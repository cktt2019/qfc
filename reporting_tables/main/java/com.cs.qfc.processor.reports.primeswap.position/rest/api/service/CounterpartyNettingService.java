package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class CounterpartyNettingService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(CounterpartyNettingService.class);
 
    @Autowired
	CounterpartyNettingRepository counterpartyNettingRepository;

	@Autowired
    CustomizedCounterpartyNettingRepository customizedCounterpartyNettingRepository;

	
	@Transactional
	public CounterpartyNetting saveCounterpartyNetting (CounterpartyNettingDTO counterpartyNettingDTO){
	 CounterpartyNetting counterpartyNetting  = DTOToCounterpartyNettingConverter.convert(counterpartyNettingDTO);
	 return counterpartyNettingRepository.save(counterpartyNetting);
	}
	
	@Transactional
	public CounterpartyNetting save (CounterpartyNetting counterpartyNetting){
	 return counterpartyNettingRepository.save(counterpartyNetting);
	}

       
       
       
  
    @Transactional
   public Collection<CounterpartyNetting> findAll (){
	   return counterpartyNettingRepository.findAll();
	}

	@Transactional
    public Collection<CounterpartyNettingDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedCounterpartyNettingRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public CounterpartyNetting findById (Integer id){
	   Optional<CounterpartyNetting> counterpartyNetting  = counterpartyNettingRepository.findById(id);
	  return (counterpartyNetting.isPresent() ? counterpartyNetting.get() : null);
	}
	
   @Transactional
   public CounterpartyNetting updateCounterpartyNetting (CounterpartyNetting counterpartyNetting,CounterpartyNettingDTO counterpartyNettingDto){
	
	counterpartyNetting.setSourceTxnId(counterpartyNettingDto.getSourceTxnId());
	counterpartyNetting.setA21AsOfDate(counterpartyNettingDto.getA21AsOfDate());
	counterpartyNetting.setA22RecordsEntityIdentifier(counterpartyNettingDto.getA22RecordsEntityIdentifier());
	counterpartyNetting.setA23NettingAgreementCounterpartyIdentifier(counterpartyNettingDto.getA23NettingAgreementCounterpartyIdentifier());
	counterpartyNetting.setA24NettingAgreementIdentifier(counterpartyNettingDto.getA24NettingAgreementIdentifier());
	counterpartyNetting.setA241UnderlyingQfcObligatorIdentifier(counterpartyNettingDto.getA241UnderlyingQfcObligatorIdentifier());
	counterpartyNetting.setA25CoveredByThirdPartyCreditEnhancement(counterpartyNettingDto.getA25CoveredByThirdPartyCreditEnhancement());
	counterpartyNetting.setA251ThirdPartyCreditEnhancementProviderIdentifier(counterpartyNettingDto.getA251ThirdPartyCreditEnhancementProviderIdentifier());
	counterpartyNetting.setA252ThirdPartyCreditEnhancementAgreementIdentifier(counterpartyNettingDto.getA252ThirdPartyCreditEnhancementAgreementIdentifier());
	counterpartyNetting.setA253CounterpartyThirdPartyCreditEnhancement(counterpartyNettingDto.getA253CounterpartyThirdPartyCreditEnhancement());
	counterpartyNetting.setA254CounterpartyThirdPartyCreditEnhancementProviderIdentifier(counterpartyNettingDto.getA254CounterpartyThirdPartyCreditEnhancementProviderIdentifier());
	counterpartyNetting.setA255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier(counterpartyNettingDto.getA255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier());
	counterpartyNetting.setA26AggregateCurrMarketValueAllPositionsUsd(counterpartyNettingDto.getA26AggregateCurrMarketValueAllPositionsUsd());
	counterpartyNetting.setA27AggregateCurrMarketValuePositivePositionsUsd(counterpartyNettingDto.getA27AggregateCurrMarketValuePositivePositionsUsd());
	counterpartyNetting.setA28AggregateCurrMarketValueNegativePositionsUsd(counterpartyNettingDto.getA28AggregateCurrMarketValueNegativePositionsUsd());
	counterpartyNetting.setA29CurrMarketValueAllCollateralRecordsEntityInUsd(counterpartyNettingDto.getA29CurrMarketValueAllCollateralRecordsEntityInUsd());
	counterpartyNetting.setA210CurrMarketValueAllCollateralCounterpartyInUsd(counterpartyNettingDto.getA210CurrMarketValueAllCollateralCounterpartyInUsd());
	counterpartyNetting.setA211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd(counterpartyNettingDto.getA211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd());
	counterpartyNetting.setA212CurrMarketValueAllCollateralCounterpartyRehypoInUsd(counterpartyNettingDto.getA212CurrMarketValueAllCollateralCounterpartyRehypoInUsd());
	counterpartyNetting.setA213RecordsEntityCollateralNet(counterpartyNettingDto.getA213RecordsEntityCollateralNet());
	counterpartyNetting.setA214CounterpartyCollateralNet(counterpartyNettingDto.getA214CounterpartyCollateralNet());
	counterpartyNetting.setA215NextMarginPaymentDate(counterpartyNettingDto.getA215NextMarginPaymentDate());
	counterpartyNetting.setA216NextMarginPaymentAmountUsd(counterpartyNettingDto.getA216NextMarginPaymentAmountUsd());
	counterpartyNetting.setA217SafekeepingAgentIdentifierRecordsEntity(counterpartyNettingDto.getA217SafekeepingAgentIdentifierRecordsEntity());
	counterpartyNetting.setA218SafekeepingAgentIdentifierCounterparty(counterpartyNettingDto.getA218SafekeepingAgentIdentifierCounterparty());
	 return counterpartyNettingRepository.save(counterpartyNetting);
	}
	
   @Transactional
   public void deleteById (Integer id){
	   counterpartyNettingRepository.deleteById(id);

	}
	
   
   

}
