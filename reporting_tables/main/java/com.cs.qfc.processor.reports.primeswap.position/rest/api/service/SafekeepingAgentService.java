package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class SafekeepingAgentService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(SafekeepingAgentService.class);
 
    @Autowired
	SafekeepingAgentRepository safekeepingAgentRepository;

	@Autowired
    CustomizedSafekeepingAgentRepository customizedSafekeepingAgentRepository;

	
	@Transactional
	public SafekeepingAgent saveSafekeepingAgent (SafekeepingAgentDTO safekeepingAgentDTO){
	 SafekeepingAgent safekeepingAgent  = DTOToSafekeepingAgentConverter.convert(safekeepingAgentDTO);
	 return safekeepingAgentRepository.save(safekeepingAgent);
	}
	
	@Transactional
	public SafekeepingAgent save (SafekeepingAgent safekeepingAgent){
	 return safekeepingAgentRepository.save(safekeepingAgent);
	}

       
       
       
  
    @Transactional
   public Collection<SafekeepingAgent> findAll (){
	   return safekeepingAgentRepository.findAll();
	}

	@Transactional
    public Collection<SafekeepingAgentDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedSafekeepingAgentRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public SafekeepingAgent findById (String id){
	   Optional<SafekeepingAgent> safekeepingAgent  = safekeepingAgentRepository.findById(id);
	  return (safekeepingAgent.isPresent() ? safekeepingAgent.get() : null);
	}
	
   @Transactional
   public SafekeepingAgent updateSafekeepingAgent (SafekeepingAgent safekeepingAgent,SafekeepingAgentDTO safekeepingAgentDto){
	
	safekeepingAgent.setSa1AsOfDate(safekeepingAgentDto.getSa1AsOfDate());
	safekeepingAgent.setSa2SafekeepingAgentIdentifier(safekeepingAgentDto.getSa2SafekeepingAgentIdentifier());
	safekeepingAgent.setSa3LegalNaleOfSafekeepingAgent(safekeepingAgentDto.getSa3LegalNaleOfSafekeepingAgent());
	safekeepingAgent.setSa4PointOfContactName(safekeepingAgentDto.getSa4PointOfContactName());
	safekeepingAgent.setSa5PointOfContactAddress(safekeepingAgentDto.getSa5PointOfContactAddress());
	safekeepingAgent.setSa6PointOfContactPhone(safekeepingAgentDto.getSa6PointOfContactPhone());
	safekeepingAgent.setSa7PointOfContactEmail(safekeepingAgentDto.getSa7PointOfContactEmail());
	 return safekeepingAgentRepository.save(safekeepingAgent);
	}
	
   @Transactional
   public void deleteById (String id){
	   safekeepingAgentRepository.deleteById(id);

	}
	
   
   

}
