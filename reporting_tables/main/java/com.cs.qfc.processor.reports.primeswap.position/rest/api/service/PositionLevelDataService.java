package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class PositionLevelDataService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(PositionLevelDataService.class);
 
    @Autowired
	PositionLevelDataRepository positionLevelDataRepository;

	@Autowired
    CustomizedPositionLevelDataRepository customizedPositionLevelDataRepository;

	
	@Transactional
	public PositionLevelData savePositionLevelData (PositionLevelDataDTO positionLevelDataDTO){
	 PositionLevelData positionLevelData  = DTOToPositionLevelDataConverter.convert(positionLevelDataDTO);
	 return positionLevelDataRepository.save(positionLevelData);
	}
	
	@Transactional
	public PositionLevelData save (PositionLevelData positionLevelData){
	 return positionLevelDataRepository.save(positionLevelData);
	}

       
       
       
  
    @Transactional
   public Collection<PositionLevelData> findAll (){
	   return positionLevelDataRepository.findAll();
	}

	@Transactional
    public Collection<PositionLevelDataDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedPositionLevelDataRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public PositionLevelData findById (String id){
	   Optional<PositionLevelData> positionLevelData  = positionLevelDataRepository.findById(id);
	  return (positionLevelData.isPresent() ? positionLevelData.get() : null);
	}
	
   @Transactional
   public PositionLevelData updatePositionLevelData (PositionLevelData positionLevelData,PositionLevelDataDTO positionLevelDataDto){
	
	positionLevelData.setA11AsOfDate(positionLevelDataDto.getA11AsOfDate());
	positionLevelData.setA12RecordsEntityIdentifier(positionLevelDataDto.getA12RecordsEntityIdentifier());
	positionLevelData.setA13PositionsIdentifier(positionLevelDataDto.getA13PositionsIdentifier());
	positionLevelData.setA15InternalBookingLocationIdentifier(positionLevelDataDto.getA15InternalBookingLocationIdentifier());
	positionLevelData.setA16UniqueBookingUnitDeskIdentifier(positionLevelDataDto.getA16UniqueBookingUnitDeskIdentifier());
	positionLevelData.setA17TypeOfQfc(positionLevelDataDto.getA17TypeOfQfc());
	positionLevelData.setA171TypeOfQfcCoveredGuarantee(positionLevelDataDto.getA171TypeOfQfcCoveredGuarantee());
	positionLevelData.setA172UnderlyingQfcObligatorIdentifier(positionLevelDataDto.getA172UnderlyingQfcObligatorIdentifier());
	positionLevelData.setA18AgreementIdentifier(positionLevelDataDto.getA18AgreementIdentifier());
	positionLevelData.setA19NettingAgreementIdrntifier(positionLevelDataDto.getA19NettingAgreementIdrntifier());
	positionLevelData.setA110NettingAgreementCounterpartyIdentifier(positionLevelDataDto.getA110NettingAgreementCounterpartyIdentifier());
	positionLevelData.setA111TradeDate(positionLevelDataDto.getA111TradeDate());
	positionLevelData.setA112TerminationDate(positionLevelDataDto.getA112TerminationDate());
	positionLevelData.setA113NextPutCallCancellationDate(positionLevelDataDto.getA113NextPutCallCancellationDate());
	positionLevelData.setA114NextPaymentDate(positionLevelDataDto.getA114NextPaymentDate());
	positionLevelData.setA115LocalCurrencyOfPosition(positionLevelDataDto.getA115LocalCurrencyOfPosition());
	positionLevelData.setA116CurrentMarketValueInLocalCurrency(positionLevelDataDto.getA116CurrentMarketValueInLocalCurrency());
	positionLevelData.setA117CurrentMarketValueInUsd(positionLevelDataDto.getA117CurrentMarketValueInUsd());
	positionLevelData.setA118AssetClassification(positionLevelDataDto.getA118AssetClassification());
	positionLevelData.setA119NotionalPrincipalAmountLocalCurrency(positionLevelDataDto.getA119NotionalPrincipalAmountLocalCurrency());
	positionLevelData.setA120NotionalPrincipalAmountUsd(positionLevelDataDto.getA120NotionalPrincipalAmountUsd());
	positionLevelData.setA121CoveredByThirdPartyCreditEnhancement(positionLevelDataDto.getA121CoveredByThirdPartyCreditEnhancement());
	positionLevelData.setA1211ThirdPartyCreditEnhancementProviderIdentifier(positionLevelDataDto.getA1211ThirdPartyCreditEnhancementProviderIdentifier());
	positionLevelData.setA1212ThirdPartyCreditEnhancementAgreementIdentifier(positionLevelDataDto.getA1212ThirdPartyCreditEnhancementAgreementIdentifier());
	positionLevelData.setA213CounterpartyThirdPartyCreditEnhancement(positionLevelDataDto.getA213CounterpartyThirdPartyCreditEnhancement());
	positionLevelData.setA1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier(positionLevelDataDto.getA1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier());
	positionLevelData.setA1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier(positionLevelDataDto.getA1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier());
	positionLevelData.setA122RelatedPositionOfRecordsEntity(positionLevelDataDto.getA122RelatedPositionOfRecordsEntity());
	positionLevelData.setA123ReferenceNumberForAnyRelatedLoan(positionLevelDataDto.getA123ReferenceNumberForAnyRelatedLoan());
	positionLevelData.setA124IdentifierOfTheLenderOfRelatedLoan(positionLevelDataDto.getA124IdentifierOfTheLenderOfRelatedLoan());
	positionLevelData.setSourceTxnId(positionLevelDataDto.getSourceTxnId());
	positionLevelData.setA14CounterpartyIdentifier(positionLevelDataDto.getA14CounterpartyIdentifier());
	 return positionLevelDataRepository.save(positionLevelData);
	}
	
   @Transactional
   public void deleteById (String id){
	   positionLevelDataRepository.deleteById(id);

	}
	
   
   

}
