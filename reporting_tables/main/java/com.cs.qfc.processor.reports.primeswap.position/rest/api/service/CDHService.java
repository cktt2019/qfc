package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class CDHService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(CDHService.class);
 
    @Autowired
	CDHRepository cDHRepository;

	@Autowired
    CustomizedCDHRepository customizedCDHRepository;

	
	@Transactional
	public CDH saveCDH (CDHDTO cDHDTO){
	 CDH cDH  = DTOToCDHConverter.convert(cDHDTO);
	 return cDHRepository.save(cDH);
	}
	
	@Transactional
	public CDH save (CDH cDH){
	 return cDHRepository.save(cDH);
	}

       
       
       
  
    @Transactional
   public Collection<CDH> findAll (){
	   return cDHRepository.findAll();
	}

	@Transactional
    public Collection<CDHDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedCDHRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public CDH findById (Integer id){
	   Optional<CDH> cDH  = cDHRepository.findById(id);
	  return (cDH.isPresent() ? cDH.get() : null);
	}
	
   @Transactional
   public CDH updateCDH (CDH cDH,CDHDTO cDHDto){
	
	cDH.setEmployeeId(cDHDto.getEmployeeId());
	cDH.setNtLoginId(cDHDto.getNtLoginId());
	cDH.setFirstName(cDHDto.getFirstName());
	cDH.setLastName(cDHDto.getLastName());
	cDH.setCountry(cDHDto.getCountry());
	cDH.setSupervisorId(cDHDto.getSupervisorId());
	cDH.setPid(cDHDto.getPid());
	cDH.setGid(cDHDto.getGid());
	cDH.setTelephoneCountry(cDHDto.getTelephoneCountry());
	cDH.setTelephoneArea(cDHDto.getTelephoneArea());
	cDH.setTalephoneNumber(cDHDto.getTalephoneNumber());
	 return cDHRepository.save(cDH);
	}
	
   @Transactional
   public void deleteById (Integer id){
	   cDHRepository.deleteById(id);

	}
	
   
   

}
