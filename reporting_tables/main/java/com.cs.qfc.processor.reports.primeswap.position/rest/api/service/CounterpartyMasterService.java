package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class CounterpartyMasterService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(CounterpartyMasterService.class);
 
    @Autowired
	CounterpartyMasterRepository counterpartyMasterRepository;

	@Autowired
    CustomizedCounterpartyMasterRepository customizedCounterpartyMasterRepository;

	
	@Transactional
	public CounterpartyMaster saveCounterpartyMaster (CounterpartyMasterDTO counterpartyMasterDTO){
	 CounterpartyMaster counterpartyMaster  = DTOToCounterpartyMasterConverter.convert(counterpartyMasterDTO);
	 return counterpartyMasterRepository.save(counterpartyMaster);
	}
	
	@Transactional
	public CounterpartyMaster save (CounterpartyMaster counterpartyMaster){
	 return counterpartyMasterRepository.save(counterpartyMaster);
	}

       
       
       
  
    @Transactional
   public Collection<CounterpartyMaster> findAll (){
	   return counterpartyMasterRepository.findAll();
	}

	@Transactional
    public Collection<CounterpartyMasterDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedCounterpartyMasterRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public CounterpartyMaster findById (String id){
	   Optional<CounterpartyMaster> counterpartyMaster  = counterpartyMasterRepository.findById(id);
	  return (counterpartyMaster.isPresent() ? counterpartyMaster.get() : null);
	}
	
   @Transactional
   public CounterpartyMaster updateCounterpartyMaster (CounterpartyMaster counterpartyMaster,CounterpartyMasterDTO counterpartyMasterDto){
	
	counterpartyMaster.setCp1AsOfDate(counterpartyMasterDto.getCp1AsOfDate());
	counterpartyMaster.setCp2CounterpartyIdentifier(counterpartyMasterDto.getCp2CounterpartyIdentifier());
	counterpartyMaster.setCp3HasLeiBeenUsedCpi(counterpartyMasterDto.getCp3HasLeiBeenUsedCpi());
	counterpartyMaster.setCp4LegalNameCounterparty(counterpartyMasterDto.getCp4LegalNameCounterparty());
	counterpartyMaster.setCp5Domicile(counterpartyMasterDto.getCp5Domicile());
	counterpartyMaster.setCp6JurisdictionUnderWhichIncorporated(counterpartyMasterDto.getCp6JurisdictionUnderWhichIncorporated());
	counterpartyMaster.setCp7ImmediateParentEntityIdentifier(counterpartyMasterDto.getCp7ImmediateParentEntityIdentifier());
	counterpartyMaster.setCp8HasLeiBeenUsedForParent(counterpartyMasterDto.getCp8HasLeiBeenUsedForParent());
	counterpartyMaster.setCp9LegalNameOfImmediateParentEntity(counterpartyMasterDto.getCp9LegalNameOfImmediateParentEntity());
	counterpartyMaster.setCp10UltimateParentEntityIdentifier(counterpartyMasterDto.getCp10UltimateParentEntityIdentifier());
	counterpartyMaster.setCp11HasLeiBeenUsedForUltimateParent(counterpartyMasterDto.getCp11HasLeiBeenUsedForUltimateParent());
	counterpartyMaster.setLegalNameOfUltimateParent(counterpartyMasterDto.getLegalNameOfUltimateParent());
	 return counterpartyMasterRepository.save(counterpartyMaster);
	}
	
   @Transactional
   public void deleteById (String id){
	   counterpartyMasterRepository.deleteById(id);

	}
	
   
   

}
