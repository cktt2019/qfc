package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class CorpOrgMasterService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(CorpOrgMasterService.class);
 
    @Autowired
	CorpOrgMasterRepository corpOrgMasterRepository;

	@Autowired
    CustomizedCorpOrgMasterRepository customizedCorpOrgMasterRepository;

	
	@Transactional
	public CorpOrgMaster saveCorpOrgMaster (CorpOrgMasterDTO corpOrgMasterDTO){
	 CorpOrgMaster corpOrgMaster  = DTOToCorpOrgMasterConverter.convert(corpOrgMasterDTO);
	 return corpOrgMasterRepository.save(corpOrgMaster);
	}
	
	@Transactional
	public CorpOrgMaster save (CorpOrgMaster corpOrgMaster){
	 return corpOrgMasterRepository.save(corpOrgMaster);
	}

       
       
       
  
    @Transactional
   public Collection<CorpOrgMaster> findAll (){
	   return corpOrgMasterRepository.findAll();
	}

	@Transactional
    public Collection<CorpOrgMasterDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedCorpOrgMasterRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public CorpOrgMaster findById (String id){
	   Optional<CorpOrgMaster> corpOrgMaster  = corpOrgMasterRepository.findById(id);
	  return (corpOrgMaster.isPresent() ? corpOrgMaster.get() : null);
	}
	
   @Transactional
   public CorpOrgMaster updateCorpOrgMaster (CorpOrgMaster corpOrgMaster,CorpOrgMasterDTO corpOrgMasterDto){
	
	corpOrgMaster.setCo1AsOfDate(corpOrgMasterDto.getCo1AsOfDate());
	corpOrgMaster.setCo2EntityIdentifier(corpOrgMasterDto.getCo2EntityIdentifier());
	corpOrgMaster.setCo3HasLeiBeenUsed(corpOrgMasterDto.getCo3HasLeiBeenUsed());
	corpOrgMaster.setCo4LegalNameOfEntity(corpOrgMasterDto.getCo4LegalNameOfEntity());
	corpOrgMaster.setCo5ImmediateParentEntityIdentifier(corpOrgMasterDto.getCo5ImmediateParentEntityIdentifier());
	corpOrgMaster.setCo6HasLeiBeenUsedForImmediateParent(corpOrgMasterDto.getCo6HasLeiBeenUsedForImmediateParent());
	corpOrgMaster.setCo7LegalNameOfImmediateParent(corpOrgMasterDto.getCo7LegalNameOfImmediateParent());
	corpOrgMaster.setCo8PercentageOwnershipOfImmediateParentInEntity(corpOrgMasterDto.getCo8PercentageOwnershipOfImmediateParentInEntity());
	corpOrgMaster.setCo9EntityType(corpOrgMasterDto.getCo9EntityType());
	corpOrgMaster.setCo10Domicile(corpOrgMasterDto.getCo10Domicile());
	corpOrgMaster.setCo11JurisdictionUnderWhichIncorporated(corpOrgMasterDto.getCo11JurisdictionUnderWhichIncorporated());
	 return corpOrgMasterRepository.save(corpOrgMaster);
	}
	
   @Transactional
   public void deleteById (String id){
	   corpOrgMasterRepository.deleteById(id);

	}
	
   
   

}
