package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class BookingLocationService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(BookingLocationService.class);
 
    @Autowired
	BookingLocationRepository bookingLocationRepository;

	@Autowired
    CustomizedBookingLocationRepository customizedBookingLocationRepository;

	
	@Transactional
	public BookingLocation saveBookingLocation (BookingLocationDTO bookingLocationDTO){
	 BookingLocation bookingLocation  = DTOToBookingLocationConverter.convert(bookingLocationDTO);
	 return bookingLocationRepository.save(bookingLocation);
	}
	
	@Transactional
	public BookingLocation save (BookingLocation bookingLocation){
	 return bookingLocationRepository.save(bookingLocation);
	}

       
       
       
  
    @Transactional
   public Collection<BookingLocation> findAll (){
	   return bookingLocationRepository.findAll();
	}

	@Transactional
    public Collection<BookingLocationDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedBookingLocationRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public BookingLocation findById (String id){
	   Optional<BookingLocation> bookingLocation  = bookingLocationRepository.findById(id);
	  return (bookingLocation.isPresent() ? bookingLocation.get() : null);
	}
	
   @Transactional
   public BookingLocation updateBookingLocation (BookingLocation bookingLocation,BookingLocationDTO bookingLocationDto){
	
	bookingLocation.setBl1AsOfDate(bookingLocationDto.getBl1AsOfDate());
	bookingLocation.setBl2RecordsEntityIdentifier(bookingLocationDto.getBl2RecordsEntityIdentifier());
	bookingLocation.setBl3InternalBookingLocationIdentifier(bookingLocationDto.getBl3InternalBookingLocationIdentifier());
	bookingLocation.setBl4UniqueBookingUnitDeskIdentifier(bookingLocationDto.getBl4UniqueBookingUnitDeskIdentifier());
	bookingLocation.setBl5UniqueBookingUnitDeskDescription(bookingLocationDto.getBl5UniqueBookingUnitDeskDescription());
	bookingLocation.setBl6BookingUnitDeskContactPhone(bookingLocationDto.getBl6BookingUnitDeskContactPhone());
	bookingLocation.setBl7BookingUnitDeskContactEmail(bookingLocationDto.getBl7BookingUnitDeskContactEmail());
	 return bookingLocationRepository.save(bookingLocation);
	}
	
   @Transactional
   public void deleteById (String id){
	   bookingLocationRepository.deleteById(id);

	}
	
   
   

}
