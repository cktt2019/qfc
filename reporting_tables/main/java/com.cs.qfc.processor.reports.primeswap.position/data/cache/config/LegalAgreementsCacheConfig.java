package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.LegalAgreements;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class LegalAgreementsCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, LegalAgreements> cacheConfiguration (){
     CacheConfiguration<Integer, LegalAgreements> cacheCfg = new CacheConfiguration<Integer, LegalAgreements> ();

     cacheCfg.setName("LegalAgreementsCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     LegalAgreementsCacheJdbcPojoStoreFactory factory = new LegalAgreementsCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("LegalAgreementsCache");
     jdbcType.setDatabaseSchema("SIERRA_QFC");
     jdbcType.setDatabaseTable("LegalAgreements");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(LegalAgreements.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"),
   	  new JdbcTypeField(Types.VARCHAR,"source_txn_id",String.class,"sourceTxnId"),
   	  new JdbcTypeField(Types.VARCHAR,"a31_as_of_date",String.class,"a31AsOfDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a32_records_entity_identifier",String.class,"a32RecordsEntityIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a33_agreement_identifier",String.class,"a33AgreementIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a34_name_of_agreement_or_governing_doc",String.class,"a34NameOfAgreementGoverningDoc"),
   	  new JdbcTypeField(Types.VARCHAR,"a35_agreement_date",String.class,"a35AgreementDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a36_agreement_counterparty_identifier",String.class,"a36AgreementCounterpartyIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a361_underlying_qfc_obligator_identifier",String.class,"a361UnderlyingQfcObligatorIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a37_agreement_governing_law",String.class,"a37AgreementGoverningLaw"),
   	  new JdbcTypeField(Types.VARCHAR,"a38_cross_default_provision",String.class,"a38CrossDefaultProvision"),
   	  new JdbcTypeField(Types.VARCHAR,"a39_identity_of_cross_default_entities",String.class,"a39IdentityOfCrossDefaultEntities"),
   	  new JdbcTypeField(Types.VARCHAR,"a310_covered_by_third_party_credit_enhancement",String.class,"a310CoveredByThirdPartyCreditEnhancement"),
   	  new JdbcTypeField(Types.VARCHAR,"a311_third_party_credit_enhancement_provider_identifier",String.class,"a311ThirdPartyCreditEnhancementProviderIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a312_associated_credit_enhancement_doc_identifier",String.class,"a312AssociatedCreditEnhancementDocIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a3121_counterparty_third_party_credit_enhancement",String.class,"a3121CounterpartyThirdPartyCreditEnhancement"),
   	  new JdbcTypeField(Types.VARCHAR,"a3122_counterparty_third_party_credit_enhancement_provider_identifier",String.class,"a3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a3123_counterparty_associated_credit_enhancement_doc_identifier",String.class,"a3123CounterpartyAssociatedCreditEnhancementDocIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a313_counterparty_contact_info_name",String.class,"a313CounterpartyContactInfoName"),
   	  new JdbcTypeField(Types.VARCHAR,"a314_counterparty_contact_info_address",String.class,"a314CounterpartyContactInfoAddress"),
   	  new JdbcTypeField(Types.VARCHAR,"a315_counterparty_contact_info_phone",String.class,"a315CounterpartyContactInfoPhone"),
   	  new JdbcTypeField(Types.VARCHAR,"a316_counterparty_contact_info_email",String.class,"a316CounterpartyContactInfoEmail")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class LegalAgreementsCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, LegalAgreements> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, LegalAgreements> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
