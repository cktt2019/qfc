package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.CounterpartyMaster;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class CounterpartyMasterCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<String, CounterpartyMaster> cacheConfiguration (){
     CacheConfiguration<String, CounterpartyMaster> cacheCfg = new CacheConfiguration<String, CounterpartyMaster> ();

     cacheCfg.setName("CounterpartyMasterCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     CounterpartyMasterCacheJdbcPojoStoreFactory factory = new CounterpartyMasterCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("CounterpartyMasterCache");
     jdbcType.setDatabaseTable("CounterpartyMaster");
     jdbcType.setKeyType(String.class);
     jdbcType.setValueType(CounterpartyMaster.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"),
   	  new JdbcTypeField(Types.VARCHAR,"cp1_as_of_date",String.class,"cp1AsOfDate"),
   	  new JdbcTypeField(Types.VARCHAR,"cp2_counterparty_identifier",String.class,"cp2CounterpartyIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"cp3_has_lei_been_used_cpi",String.class,"cp3HasLeiBeenUsedCpi"),
   	  new JdbcTypeField(Types.VARCHAR,"cp4_legal_name_counterparty",String.class,"cp4LegalNameCounterparty"),
   	  new JdbcTypeField(Types.VARCHAR,"cp5_domicile",String.class,"cp5Domicile"),
   	  new JdbcTypeField(Types.VARCHAR,"cp6_jurisdiction_under_which_incorporated",String.class,"cp6JurisdictionUnderWhichIncorporated"),
   	  new JdbcTypeField(Types.VARCHAR,"cp7_immediate_parent_entity_identifier",String.class,"cp7ImmediateParentEntityIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"cp8_has_lei_been_used_for_parent",String.class,"cp8HasLeiBeenUsedForParent"),
   	  new JdbcTypeField(Types.VARCHAR,"cp9_legal_name_of_immediate_parent_entity",String.class,"cp9LegalNameOfImmediateParentEntity"),
   	  new JdbcTypeField(Types.VARCHAR,"cp10_ultimate_parent_entity_identifier",String.class,"cp10UltimateParentEntityIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"cp11_has_lei_been_used_for_ultimate_parent",String.class,"cp11HasLeiBeenUsedForUltimateParent"),
   	  new JdbcTypeField(Types.VARCHAR,"legal_name_of_ultimate_parent",String.class,"legalNameOfUltimateParent")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class CounterpartyMasterCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<String, CounterpartyMaster> implements Serializable {
           @Override
           public CacheJdbcPojoStore<String, CounterpartyMaster> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
