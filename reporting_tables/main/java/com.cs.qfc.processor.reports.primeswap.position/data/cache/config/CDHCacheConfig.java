package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.CDH;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class CDHCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, CDH> cacheConfiguration (){
     CacheConfiguration<Integer, CDH> cacheCfg = new CacheConfiguration<Integer, CDH> ();

     cacheCfg.setName("CDHCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     CDHCacheJdbcPojoStoreFactory factory = new CDHCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("CDHCache");
     jdbcType.setDatabaseTable("CDH");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(CDH.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"),
   	  new JdbcTypeField(Types.VARCHAR,"employee_id",String.class,"employeeId"),
   	  new JdbcTypeField(Types.VARCHAR,"nt_login_id",String.class,"ntLoginId"),
   	  new JdbcTypeField(Types.VARCHAR,"first_name",String.class,"firstName"),
   	  new JdbcTypeField(Types.VARCHAR,"last_name",String.class,"lastName"),
   	  new JdbcTypeField(Types.VARCHAR,"country",String.class,"country"),
   	  new JdbcTypeField(Types.VARCHAR,"supervisor_id",String.class,"supervisorId"),
   	  new JdbcTypeField(Types.VARCHAR,"pid",String.class,"pid"),
   	  new JdbcTypeField(Types.VARCHAR,"gid",String.class,"gid"),
   	  new JdbcTypeField(Types.VARCHAR,"telephone_country",String.class,"telephoneCountry"),
   	  new JdbcTypeField(Types.VARCHAR,"telephone_area",String.class,"telephoneArea"),
   	  new JdbcTypeField(Types.VARCHAR,"talephone_number",String.class,"talephoneNumber")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class CDHCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, CDH> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, CDH> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
