package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.SafekeepingAgent;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class SafekeepingAgentCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<String, SafekeepingAgent> cacheConfiguration (){
     CacheConfiguration<String, SafekeepingAgent> cacheCfg = new CacheConfiguration<String, SafekeepingAgent> ();

     cacheCfg.setName("SafekeepingAgentCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     SafekeepingAgentCacheJdbcPojoStoreFactory factory = new SafekeepingAgentCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("SafekeepingAgentCache");
     jdbcType.setDatabaseTable("SafekeepingAgent");
     jdbcType.setKeyType(String.class);
     jdbcType.setValueType(SafekeepingAgent.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"),
   	  new JdbcTypeField(Types.VARCHAR,"sa1_as_of_date",String.class,"sa1AsOfDate"),
   	  new JdbcTypeField(Types.VARCHAR,"sa2_safekeeping_agent_identifier",String.class,"sa2SafekeepingAgentIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"sa3_legal_nale_of_safekeeping_agent",String.class,"sa3LegalNaleOfSafekeepingAgent"),
   	  new JdbcTypeField(Types.VARCHAR,"sa4_point_of_contact_name",String.class,"sa4PointOfContactName"),
   	  new JdbcTypeField(Types.VARCHAR,"sa5_point_of_contact_address",String.class,"sa5PointOfContactAddress"),
   	  new JdbcTypeField(Types.VARCHAR,"sa6_point_of_contact_phone",String.class,"sa6PointOfContactPhone"),
   	  new JdbcTypeField(Types.VARCHAR,"sa7_point_of_contact_email",String.class,"sa7PointOfContactEmail")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class SafekeepingAgentCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<String, SafekeepingAgent> implements Serializable {
           @Override
           public CacheJdbcPojoStore<String, SafekeepingAgent> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
