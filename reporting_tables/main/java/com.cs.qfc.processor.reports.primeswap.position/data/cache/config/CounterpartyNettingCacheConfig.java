package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.CounterpartyNetting;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class CounterpartyNettingCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, CounterpartyNetting> cacheConfiguration (){
     CacheConfiguration<Integer, CounterpartyNetting> cacheCfg = new CacheConfiguration<Integer, CounterpartyNetting> ();

     cacheCfg.setName("CounterpartyNettingCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     CounterpartyNettingCacheJdbcPojoStoreFactory factory = new CounterpartyNettingCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("CounterpartyNettingCache");
     jdbcType.setDatabaseSchema("SIERRA_QFC");
     jdbcType.setDatabaseTable("CounterpartyNetting");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(CounterpartyNetting.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"),
   	  new JdbcTypeField(Types.VARCHAR,"source_txn_id",String.class,"sourceTxnId"),
   	  new JdbcTypeField(Types.VARCHAR,"a21_as_of_date",String.class,"a21AsOfDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a22_records_entity_identifier",String.class,"a22RecordsEntityIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a23_netting_agreement_counterparty_identifier",String.class,"a23NettingAgreementCounterpartyIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a24_netting_agreement_identifier",String.class,"a24NettingAgreementIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a241_underlying_qfc_obligator_identifier",String.class,"a241UnderlyingQfcObligatorIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a25_covered_by_third_party_credit_enhancement",String.class,"a25CoveredByThirdPartyCreditEnhancement"),
   	  new JdbcTypeField(Types.VARCHAR,"a251_third_party_credit_enhancement_provider_identifier",String.class,"a251ThirdPartyCreditEnhancementProviderIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a252_third_party_credit_enhancement_agreement_identifier",String.class,"a252ThirdPartyCreditEnhancementAgreementIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a253_counterparty_third_party_credit_enhancement",String.class,"a253CounterpartyThirdPartyCreditEnhancement"),
   	  new JdbcTypeField(Types.VARCHAR,"a254_counterparty_third_party_credit_enhancement_provider_identifier",String.class,"a254CounterpartyThirdPartyCreditEnhancementProviderIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a255_counterparty_third_party_credit_enhancement_agreement_identifier",String.class,"a255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a26_aggregate_curr_market_value_all_positions_usd",String.class,"a26AggregateCurrMarketValueAllPositionsUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a27_aggregate_curr_market_value_positive_positions_usd",String.class,"a27AggregateCurrMarketValuePositivePositionsUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a28_aggregate_curr_market_value_negative_positions_usd",String.class,"a28AggregateCurrMarketValueNegativePositionsUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a29_curr_market_value_all_collateral_records_entity_in_usd",String.class,"a29CurrMarketValueAllCollateralRecordsEntityInUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a210_curr_market_value_all_collateral_counterparty_in_usd",String.class,"a210CurrMarketValueAllCollateralCounterpartyInUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a211_curr_market_value_all_collateral_records_entity_rehypo_in_usd",String.class,"a211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a212_curr_market_value_all_collateral_counterparty_rehypo_in_usd",String.class,"a212CurrMarketValueAllCollateralCounterpartyRehypoInUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a213_records_entity_collateral_net",String.class,"a213RecordsEntityCollateralNet"),
   	  new JdbcTypeField(Types.VARCHAR,"a214_counterparty_collateral_net",String.class,"a214CounterpartyCollateralNet"),
   	  new JdbcTypeField(Types.VARCHAR,"a215_next_margin_payment_date",String.class,"a215NextMarginPaymentDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a216_next_margin_payment_amount_usd",String.class,"a216NextMarginPaymentAmountUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a217_safekeeping_agent_identifier_records_entity",String.class,"a217SafekeepingAgentIdentifierRecordsEntity"),
   	  new JdbcTypeField(Types.VARCHAR,"a218_safekeeping_agent_identifier_counterparty",String.class,"a218SafekeepingAgentIdentifierCounterparty")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class CounterpartyNettingCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, CounterpartyNetting> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, CounterpartyNetting> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
