package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.BookingLocation;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class BookingLocationCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<String, BookingLocation> cacheConfiguration (){
     CacheConfiguration<String, BookingLocation> cacheCfg = new CacheConfiguration<String, BookingLocation> ();

     cacheCfg.setName("BookingLocationCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     BookingLocationCacheJdbcPojoStoreFactory factory = new BookingLocationCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("BookingLocationCache");
     jdbcType.setDatabaseTable("BookingLocation");
     jdbcType.setKeyType(String.class);
     jdbcType.setValueType(BookingLocation.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"),
   	  new JdbcTypeField(Types.VARCHAR,"bl1_as_of_date",String.class,"bl1AsOfDate"),
   	  new JdbcTypeField(Types.VARCHAR,"bl2_records_entity_identifier",String.class,"bl2RecordsEntityIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"bl3_internal_booking_location_identifier",String.class,"bl3InternalBookingLocationIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"bl4_unique_booking_unit_desk_identifier",String.class,"bl4UniqueBookingUnitDeskIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"bl5_unique_booking_unit_desk_description",String.class,"bl5UniqueBookingUnitDeskDescription"),
   	  new JdbcTypeField(Types.VARCHAR,"bl6_booking_unit_desk_contact_phone",String.class,"bl6BookingUnitDeskContactPhone"),
   	  new JdbcTypeField(Types.VARCHAR,"bl7_booking_unit_desk_contact_email",String.class,"bl7BookingUnitDeskContactEmail")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class BookingLocationCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<String, BookingLocation> implements Serializable {
           @Override
           public CacheJdbcPojoStore<String, BookingLocation> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
