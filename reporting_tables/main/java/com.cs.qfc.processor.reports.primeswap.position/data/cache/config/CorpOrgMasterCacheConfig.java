package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.CorpOrgMaster;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class CorpOrgMasterCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<String, CorpOrgMaster> cacheConfiguration (){
     CacheConfiguration<String, CorpOrgMaster> cacheCfg = new CacheConfiguration<String, CorpOrgMaster> ();

     cacheCfg.setName("CorpOrgMasterCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     CorpOrgMasterCacheJdbcPojoStoreFactory factory = new CorpOrgMasterCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("CorpOrgMasterCache");
     jdbcType.setDatabaseTable("CorpOrgMaster");
     jdbcType.setKeyType(String.class);
     jdbcType.setValueType(CorpOrgMaster.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"),
   	  new JdbcTypeField(Types.VARCHAR,"co1_as_of_date",String.class,"co1AsOfDate"),
   	  new JdbcTypeField(Types.VARCHAR,"co2_entity_identifier",String.class,"co2EntityIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"co3_has_lei_been_used",String.class,"co3HasLeiBeenUsed"),
   	  new JdbcTypeField(Types.VARCHAR,"co4_legal_name_of_entity",String.class,"co4LegalNameOfEntity"),
   	  new JdbcTypeField(Types.VARCHAR,"co5_immediate_parent_entity_identifier",String.class,"co5ImmediateParentEntityIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"co6_has_lei_been_used_for_immediate_parent",String.class,"co6HasLeiBeenUsedForImmediateParent"),
   	  new JdbcTypeField(Types.VARCHAR,"co7_legal_name_of_immediate_parent",String.class,"co7LegalNameOfImmediateParent"),
   	  new JdbcTypeField(Types.VARCHAR,"co8_percentage_ownership_of_immediate_parent_in_entity",String.class,"co8PercentageOwnershipOfImmediateParentInEntity"),
   	  new JdbcTypeField(Types.VARCHAR,"co9_entity_type",String.class,"co9EntityType"),
   	  new JdbcTypeField(Types.VARCHAR,"co10_domicile",String.class,"co10Domicile"),
   	  new JdbcTypeField(Types.VARCHAR,"co11_jurisdiction_under_which_incorporated",String.class,"co11JurisdictionUnderWhichIncorporated")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class CorpOrgMasterCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<String, CorpOrgMaster> implements Serializable {
           @Override
           public CacheJdbcPojoStore<String, CorpOrgMaster> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
