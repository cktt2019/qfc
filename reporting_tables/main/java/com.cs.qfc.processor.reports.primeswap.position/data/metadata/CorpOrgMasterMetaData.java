package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class CorpOrgMasterMetaData  implements MetaData {
	
	private static final String ID = "id";
	private static final String CO1ASOFDATE = "co1AsOfDate";
	private static final String CO2ENTITYIDENTIFIER = "co2EntityIdentifier";
	private static final String CO3HASLEIBEENUSED = "co3HasLeiBeenUsed";
	private static final String CO4LEGALNAMEOFENTITY = "co4LegalNameOfEntity";
	private static final String CO5IMMEDIATEPARENTENTITYIDENTIFIER = "co5ImmediateParentEntityIdentifier";
	private static final String CO6HASLEIBEENUSEDFORIMMEDIATEPARENT = "co6HasLeiBeenUsedForImmediateParent";
	private static final String CO7LEGALNAMEOFIMMEDIATEPARENT = "co7LegalNameOfImmediateParent";
	private static final String CO8PERCENTAGEOWNERSHIPOFIMMEDIATEPARENTINENTITY = "co8PercentageOwnershipOfImmediateParentInEntity";
	private static final String CO9ENTITYTYPE = "co9EntityType";
	private static final String CO10DOMICILE = "co10Domicile";
	private static final String CO11JURISDICTIONUNDERWHICHINCORPORATED = "co11JurisdictionUnderWhichIncorporated";

   
     public static final String TABLE = "CorpOrgMaster";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case ID : return "id";
	case CO1ASOFDATE : return "co1_as_of_date";
	case CO2ENTITYIDENTIFIER : return "co2_entity_identifier";
	case CO3HASLEIBEENUSED : return "co3_has_lei_been_used";
	case CO4LEGALNAMEOFENTITY : return "co4_legal_name_of_entity";
	case CO5IMMEDIATEPARENTENTITYIDENTIFIER : return "co5_immediate_parent_entity_identifier";
	case CO6HASLEIBEENUSEDFORIMMEDIATEPARENT : return "co6_has_lei_been_used_for_immediate_parent";
	case CO7LEGALNAMEOFIMMEDIATEPARENT : return "co7_legal_name_of_immediate_parent";
	case CO8PERCENTAGEOWNERSHIPOFIMMEDIATEPARENTINENTITY : return "co8_percentage_ownership_of_immediate_parent_in_entity";
	case CO9ENTITYTYPE : return "co9_entity_type";
	case CO10DOMICILE : return "co10_domicile";
	case CO11JURISDICTIONUNDERWHICHINCORPORATED : return "co11_jurisdiction_under_which_incorporated";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case ID : return "String";
	case CO1ASOFDATE : return "String";
	case CO2ENTITYIDENTIFIER : return "String";
	case CO3HASLEIBEENUSED : return "String";
	case CO4LEGALNAMEOFENTITY : return "String";
	case CO5IMMEDIATEPARENTENTITYIDENTIFIER : return "String";
	case CO6HASLEIBEENUSEDFORIMMEDIATEPARENT : return "String";
	case CO7LEGALNAMEOFIMMEDIATEPARENT : return "String";
	case CO8PERCENTAGEOWNERSHIPOFIMMEDIATEPARENTINENTITY : return "String";
	case CO9ENTITYTYPE : return "String";
	case CO10DOMICILE : return "String";
	case CO11JURISDICTIONUNDERWHICHINCORPORATED : return "String";
		}
		return null;
	}
	

}
