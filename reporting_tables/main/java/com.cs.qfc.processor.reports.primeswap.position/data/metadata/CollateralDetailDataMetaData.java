package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class CollateralDetailDataMetaData  implements MetaData {
	
	private static final String ID = "id";
	private static final String SOURCETXNID = "sourceTxnId";
	private static final String A41ASOFDATE = "a41AsOfDate";
	private static final String A42RECORDSENTITYIDENTIFIER = "a42RecordsEntityIdentifier";
	private static final String A43COLLATERAOPOSTEDRECEIVEDFLAG = "a43collateraoPostedReceivedFlag";
	private static final String A44COUNTERPARTYIDENTIFIER = "a44CounterpartyIdentifier";
	private static final String A45NETTINGAGREEMENTIDENTIFIER = "a45NettingAgreementIdentifier";
	private static final String A46UNIQUECOLLATERALITEMIDENTIFIER = "a46UniqueCollateralItemIdentifier";
	private static final String A47ORIGINALFACEAMTCOLLATERALITEMLOCALCURR = "a47OriginalFaceAmtCollateralItemLocalCurr";
	private static final String A48LOCALCURRENCYOFCOLLATERALITEM = "a48LocalCurrencyOfCollateralItem";
	private static final String A49MARKETVALUEAMOUNTOFCOLLATERALITEMINUSD = "a49MarketValueAmountOfCollateralItemInUsd";
	private static final String A410DESCRIPTIONOFCOLLATERALITEM = "a410DescriptionOfCollateralItem";
	private static final String A411ASSETCLASSIFICATION = "a411AssetClassification";
	private static final String A412COLLATERALPORTFOLIOSEGREGATIONSTATUS = "a412CollateralPortfolioSegregationStatus";
	private static final String A413COLLATERALLOCATION = "a413CollateralLocation";
	private static final String A414COLLATERALJURISDICTION = "a414CollateralJurisdiction";
	private static final String A415COLLATERALREHYPOALLOWED = "a415CollateralRehypoAllowed";

   
    public static final String TABLE = "SIERRA_QFC.CollateralDetailData";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case ID : return "id";
	case SOURCETXNID : return "source_txn_id";
	case A41ASOFDATE : return "a41_as_of_date";
	case A42RECORDSENTITYIDENTIFIER : return "a42_records_entity_identifier";
	case A43COLLATERAOPOSTEDRECEIVEDFLAG : return "a43collaterao_posted_received_flag";
	case A44COUNTERPARTYIDENTIFIER : return "a44_counterparty_identifier";
	case A45NETTINGAGREEMENTIDENTIFIER : return "a45_netting_agreement_identifier";
	case A46UNIQUECOLLATERALITEMIDENTIFIER : return "a46_unique_collateral_item_identifier";
	case A47ORIGINALFACEAMTCOLLATERALITEMLOCALCURR : return "a47_original_face_amt_collateral_item_local_curr";
	case A48LOCALCURRENCYOFCOLLATERALITEM : return "a48_local_currency_of_collateral_item";
	case A49MARKETVALUEAMOUNTOFCOLLATERALITEMINUSD : return "a49_market_value_amount_of_collateral_item_in_usd";
	case A410DESCRIPTIONOFCOLLATERALITEM : return "a410_description_of_collateral_item";
	case A411ASSETCLASSIFICATION : return "a411_asset_classification";
	case A412COLLATERALPORTFOLIOSEGREGATIONSTATUS : return "a412_collateral_portfolio_segregation_status";
	case A413COLLATERALLOCATION : return "a413_collateral_location";
	case A414COLLATERALJURISDICTION : return "a414_collateral_jurisdiction";
	case A415COLLATERALREHYPOALLOWED : return "a415_collateral_rehypo_allowed";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case ID : return "Integer";
	case SOURCETXNID : return "String";
	case A41ASOFDATE : return "String";
	case A42RECORDSENTITYIDENTIFIER : return "String";
	case A43COLLATERAOPOSTEDRECEIVEDFLAG : return "String";
	case A44COUNTERPARTYIDENTIFIER : return "String";
	case A45NETTINGAGREEMENTIDENTIFIER : return "String";
	case A46UNIQUECOLLATERALITEMIDENTIFIER : return "String";
	case A47ORIGINALFACEAMTCOLLATERALITEMLOCALCURR : return "String";
	case A48LOCALCURRENCYOFCOLLATERALITEM : return "String";
	case A49MARKETVALUEAMOUNTOFCOLLATERALITEMINUSD : return "String";
	case A410DESCRIPTIONOFCOLLATERALITEM : return "String";
	case A411ASSETCLASSIFICATION : return "String";
	case A412COLLATERALPORTFOLIOSEGREGATIONSTATUS : return "String";
	case A413COLLATERALLOCATION : return "String";
	case A414COLLATERALJURISDICTION : return "String";
	case A415COLLATERALREHYPOALLOWED : return "String";
		}
		return null;
	}
	

}
