package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class PositionLevelDataMetaData  implements MetaData {
	
	private static final String ID = "id";
	private static final String A11ASOFDATE = "a11AsOfDate";
	private static final String A12RECORDSENTITYIDENTIFIER = "a12RecordsEntityIdentifier";
	private static final String A13POSITIONSIDENTIFIER = "a13PositionsIdentifier";
	private static final String A15INTERNALBOOKINGLOCATIONIDENTIFIER = "a15InternalBookingLocationIdentifier";
	private static final String A16UNIQUEBOOKINGUNITDESKIDENTIFIER = "a16UniqueBookingUnitDeskIdentifier";
	private static final String A17TYPEOFQFC = "a17TypeOfQfc";
	private static final String A171TYPEOFQFCCOVEREDGUARANTEE = "a171TypeOfQfcCoveredGuarantee";
	private static final String A172UNDERLYINGQFCOBLIGATORIDENTIFIER = "a172UnderlyingQfcObligatorIdentifier";
	private static final String A18AGREEMENTIDENTIFIER = "a18AgreementIdentifier";
	private static final String A19NETTINGAGREEMENTIDRNTIFIER = "a19NettingAgreementIdrntifier";
	private static final String A110NETTINGAGREEMENTCOUNTERPARTYIDENTIFIER = "a110NettingAgreementCounterpartyIdentifier";
	private static final String A111TRADEDATE = "a111TradeDate";
	private static final String A112TERMINATIONDATE = "a112TerminationDate";
	private static final String A113NEXTPUTCALLCANCELLATIONDATE = "a113NextPutCallCancellationDate";
	private static final String A114NEXTPAYMENTDATE = "a114NextPaymentDate";
	private static final String A115LOCALCURRENCYOFPOSITION = "a115LocalCurrencyOfPosition";
	private static final String A116CURRENTMARKETVALUEINLOCALCURRENCY = "a116CurrentMarketValueInLocalCurrency";
	private static final String A117CURRENTMARKETVALUEINUSD = "a117CurrentMarketValueInUsd";
	private static final String A118ASSETCLASSIFICATION = "a118AssetClassification";
	private static final String A119NOTIONALPRINCIPALAMOUNTLOCALCURRENCY = "a119NotionalPrincipalAmountLocalCurrency";
	private static final String A120NOTIONALPRINCIPALAMOUNTUSD = "a120NotionalPrincipalAmountUsd";
	private static final String A121COVEREDBYTHIRDPARTYCREDITENHANCEMENT = "a121CoveredByThirdPartyCreditEnhancement";
	private static final String A1211THIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER = "a1211ThirdPartyCreditEnhancementProviderIdentifier";
	private static final String A1212THIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER = "a1212ThirdPartyCreditEnhancementAgreementIdentifier";
	private static final String A213COUNTERPARTYTHIRDPARTYCREDITENHANCEMENT = "a213CounterpartyThirdPartyCreditEnhancement";
	private static final String A1214COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER = "a1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier";
	private static final String A1215COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER = "a1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier";
	private static final String A122RELATEDPOSITIONOFRECORDSENTITY = "a122RelatedPositionOfRecordsEntity";
	private static final String A123REFERENCENUMBERFORANYRELATEDLOAN = "a123ReferenceNumberForAnyRelatedLoan";
	private static final String A124IDENTIFIEROFTHELENDEROFRELATEDLOAN = "a124IdentifierOfTheLenderOfRelatedLoan";
	private static final String SOURCETXNID = "sourceTxnId";
	private static final String A14COUNTERPARTYIDENTIFIER = "a14CounterpartyIdentifier";

   
     public static final String TABLE = "PositionLevelData";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case ID : return "id";
	case A11ASOFDATE : return "a11_as_of_date";
	case A12RECORDSENTITYIDENTIFIER : return "a12_records_entity_identifier";
	case A13POSITIONSIDENTIFIER : return "a13_positions_identifier";
	case A15INTERNALBOOKINGLOCATIONIDENTIFIER : return "a15_internal_booking_location_identifier";
	case A16UNIQUEBOOKINGUNITDESKIDENTIFIER : return "a16_unique_booking_unit_desk_identifier";
	case A17TYPEOFQFC : return "a17_type_of_qfc";
	case A171TYPEOFQFCCOVEREDGUARANTEE : return "a_171_type_of_qfc_covered_guarantee";
	case A172UNDERLYINGQFCOBLIGATORIDENTIFIER : return "a172_underlying_qfc_obligator_identifier";
	case A18AGREEMENTIDENTIFIER : return "a18_agreement_identifier";
	case A19NETTINGAGREEMENTIDRNTIFIER : return "a19_netting_agreement_idrntifier";
	case A110NETTINGAGREEMENTCOUNTERPARTYIDENTIFIER : return "a110_netting_agreement_counterparty_identifier";
	case A111TRADEDATE : return "a111_trade_date";
	case A112TERMINATIONDATE : return "a112_termination_date";
	case A113NEXTPUTCALLCANCELLATIONDATE : return "a113_next_put_call_cancellation_date";
	case A114NEXTPAYMENTDATE : return "a114_next_payment_date";
	case A115LOCALCURRENCYOFPOSITION : return "a115_local_currency_of_position";
	case A116CURRENTMARKETVALUEINLOCALCURRENCY : return "a116_current_market_value_in_local_currency";
	case A117CURRENTMARKETVALUEINUSD : return "a117_current_market_value_in_usd";
	case A118ASSETCLASSIFICATION : return "a118_asset_classification";
	case A119NOTIONALPRINCIPALAMOUNTLOCALCURRENCY : return "a119_notional_or_principal_amount_local_currency";
	case A120NOTIONALPRINCIPALAMOUNTUSD : return "a120_notional_or_principal_amount_usd";
	case A121COVEREDBYTHIRDPARTYCREDITENHANCEMENT : return "a121_covered_by_third_party_credit_enhancement";
	case A1211THIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER : return "a1211_third_party_credit_enhancement_provider_identifier";
	case A1212THIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER : return "a1212_third_party_credit_enhancement_agreement_identifier";
	case A213COUNTERPARTYTHIRDPARTYCREDITENHANCEMENT : return "a213_counterparty_third_party_credit_enhancement";
	case A1214COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER : return "a1214_counterparty_third_party_credit_enhancement_provider_identifier";
	case A1215COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER : return "a1215_counterparty_third_party_credit_enhancement_agreement_identifier";
	case A122RELATEDPOSITIONOFRECORDSENTITY : return "a122_related_position_of_records_entity";
	case A123REFERENCENUMBERFORANYRELATEDLOAN : return "a123_reference_number_for_any_related_loan";
	case A124IDENTIFIEROFTHELENDEROFRELATEDLOAN : return "a124_identifier_of_the_lender_of_related_loan";
	case SOURCETXNID : return "source_txn_id";
	case A14COUNTERPARTYIDENTIFIER : return "a14_counterparty_identifier";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case ID : return "String";
	case A11ASOFDATE : return "java.sql.Date";
	case A12RECORDSENTITYIDENTIFIER : return "String";
	case A13POSITIONSIDENTIFIER : return "String";
	case A15INTERNALBOOKINGLOCATIONIDENTIFIER : return "String";
	case A16UNIQUEBOOKINGUNITDESKIDENTIFIER : return "String";
	case A17TYPEOFQFC : return "String";
	case A171TYPEOFQFCCOVEREDGUARANTEE : return "String";
	case A172UNDERLYINGQFCOBLIGATORIDENTIFIER : return "String";
	case A18AGREEMENTIDENTIFIER : return "String";
	case A19NETTINGAGREEMENTIDRNTIFIER : return "String";
	case A110NETTINGAGREEMENTCOUNTERPARTYIDENTIFIER : return "String";
	case A111TRADEDATE : return "String";
	case A112TERMINATIONDATE : return "String";
	case A113NEXTPUTCALLCANCELLATIONDATE : return "String";
	case A114NEXTPAYMENTDATE : return "String";
	case A115LOCALCURRENCYOFPOSITION : return "String";
	case A116CURRENTMARKETVALUEINLOCALCURRENCY : return "String";
	case A117CURRENTMARKETVALUEINUSD : return "String";
	case A118ASSETCLASSIFICATION : return "String";
	case A119NOTIONALPRINCIPALAMOUNTLOCALCURRENCY : return "String";
	case A120NOTIONALPRINCIPALAMOUNTUSD : return "String";
	case A121COVEREDBYTHIRDPARTYCREDITENHANCEMENT : return "String";
	case A1211THIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER : return "String";
	case A1212THIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER : return "String";
	case A213COUNTERPARTYTHIRDPARTYCREDITENHANCEMENT : return "String";
	case A1214COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER : return "String";
	case A1215COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER : return "String";
	case A122RELATEDPOSITIONOFRECORDSENTITY : return "String";
	case A123REFERENCENUMBERFORANYRELATEDLOAN : return "String";
	case A124IDENTIFIEROFTHELENDEROFRELATEDLOAN : return "String";
	case SOURCETXNID : return "String";
	case A14COUNTERPARTYIDENTIFIER : return "String";
		}
		return null;
	}
	

}
