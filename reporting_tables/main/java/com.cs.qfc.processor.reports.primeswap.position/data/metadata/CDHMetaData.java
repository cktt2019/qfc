package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class CDHMetaData  implements MetaData {
	
	private static final String ID = "id";
	private static final String EMPLOYEEID = "employeeId";
	private static final String NTLOGINID = "ntLoginId";
	private static final String FIRSTNAME = "firstName";
	private static final String LASTNAME = "lastName";
	private static final String COUNTRY = "country";
	private static final String SUPERVISORID = "supervisorId";
	private static final String PID = "pid";
	private static final String GID = "gid";
	private static final String TELEPHONECOUNTRY = "telephoneCountry";
	private static final String TELEPHONEAREA = "telephoneArea";
	private static final String TALEPHONENUMBER = "talephoneNumber";

   
     public static final String TABLE = "CDH";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case ID : return "id";
	case EMPLOYEEID : return "employee_id";
	case NTLOGINID : return "nt_login_id";
	case FIRSTNAME : return "first_name";
	case LASTNAME : return "last_name";
	case COUNTRY : return "country";
	case SUPERVISORID : return "supervisor_id";
	case PID : return "pid";
	case GID : return "gid";
	case TELEPHONECOUNTRY : return "telephone_country";
	case TELEPHONEAREA : return "telephone_area";
	case TALEPHONENUMBER : return "talephone_number";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case ID : return "Integer";
	case EMPLOYEEID : return "String";
	case NTLOGINID : return "String";
	case FIRSTNAME : return "String";
	case LASTNAME : return "String";
	case COUNTRY : return "String";
	case SUPERVISORID : return "String";
	case PID : return "String";
	case GID : return "String";
	case TELEPHONECOUNTRY : return "String";
	case TELEPHONEAREA : return "String";
	case TALEPHONENUMBER : return "String";
		}
		return null;
	}
	

}
