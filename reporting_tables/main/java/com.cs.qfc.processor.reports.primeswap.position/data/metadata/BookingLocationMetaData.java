package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class BookingLocationMetaData  implements MetaData {
	
	private static final String ID = "id";
	private static final String BL1ASOFDATE = "bl1AsOfDate";
	private static final String BL2RECORDSENTITYIDENTIFIER = "bl2RecordsEntityIdentifier";
	private static final String BL3INTERNALBOOKINGLOCATIONIDENTIFIER = "bl3InternalBookingLocationIdentifier";
	private static final String BL4UNIQUEBOOKINGUNITDESKIDENTIFIER = "bl4UniqueBookingUnitDeskIdentifier";
	private static final String BL5UNIQUEBOOKINGUNITDESKDESCRIPTION = "bl5UniqueBookingUnitDeskDescription";
	private static final String BL6BOOKINGUNITDESKCONTACTPHONE = "bl6BookingUnitDeskContactPhone";
	private static final String BL7BOOKINGUNITDESKCONTACTEMAIL = "bl7BookingUnitDeskContactEmail";

   
     public static final String TABLE = "BookingLocation";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case ID : return "id";
	case BL1ASOFDATE : return "bl1_as_of_date";
	case BL2RECORDSENTITYIDENTIFIER : return "bl2_records_entity_identifier";
	case BL3INTERNALBOOKINGLOCATIONIDENTIFIER : return "bl3_internal_booking_location_identifier";
	case BL4UNIQUEBOOKINGUNITDESKIDENTIFIER : return "bl4_unique_booking_unit_desk_identifier";
	case BL5UNIQUEBOOKINGUNITDESKDESCRIPTION : return "bl5_unique_booking_unit_desk_description";
	case BL6BOOKINGUNITDESKCONTACTPHONE : return "bl6_booking_unit_desk_contact_phone";
	case BL7BOOKINGUNITDESKCONTACTEMAIL : return "bl7_booking_unit_desk_contact_email";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case ID : return "String";
	case BL1ASOFDATE : return "String";
	case BL2RECORDSENTITYIDENTIFIER : return "String";
	case BL3INTERNALBOOKINGLOCATIONIDENTIFIER : return "String";
	case BL4UNIQUEBOOKINGUNITDESKIDENTIFIER : return "String";
	case BL5UNIQUEBOOKINGUNITDESKDESCRIPTION : return "String";
	case BL6BOOKINGUNITDESKCONTACTPHONE : return "String";
	case BL7BOOKINGUNITDESKCONTACTEMAIL : return "String";
		}
		return null;
	}
	

}
