package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class SafekeepingAgentMetaData  implements MetaData {
	
	private static final String ID = "id";
	private static final String SA1ASOFDATE = "sa1AsOfDate";
	private static final String SA2SAFEKEEPINGAGENTIDENTIFIER = "sa2SafekeepingAgentIdentifier";
	private static final String SA3LEGALNALEOFSAFEKEEPINGAGENT = "sa3LegalNaleOfSafekeepingAgent";
	private static final String SA4POINTOFCONTACTNAME = "sa4PointOfContactName";
	private static final String SA5POINTOFCONTACTADDRESS = "sa5PointOfContactAddress";
	private static final String SA6POINTOFCONTACTPHONE = "sa6PointOfContactPhone";
	private static final String SA7POINTOFCONTACTEMAIL = "sa7PointOfContactEmail";

   
     public static final String TABLE = "SafekeepingAgent";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case ID : return "id";
	case SA1ASOFDATE : return "sa1_as_of_date";
	case SA2SAFEKEEPINGAGENTIDENTIFIER : return "sa2_safekeeping_agent_identifier";
	case SA3LEGALNALEOFSAFEKEEPINGAGENT : return "sa3_legal_nale_of_safekeeping_agent";
	case SA4POINTOFCONTACTNAME : return "sa4_point_of_contact_name";
	case SA5POINTOFCONTACTADDRESS : return "sa5_point_of_contact_address";
	case SA6POINTOFCONTACTPHONE : return "sa6_point_of_contact_phone";
	case SA7POINTOFCONTACTEMAIL : return "sa7_point_of_contact_email";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case ID : return "String";
	case SA1ASOFDATE : return "String";
	case SA2SAFEKEEPINGAGENTIDENTIFIER : return "String";
	case SA3LEGALNALEOFSAFEKEEPINGAGENT : return "String";
	case SA4POINTOFCONTACTNAME : return "String";
	case SA5POINTOFCONTACTADDRESS : return "String";
	case SA6POINTOFCONTACTPHONE : return "String";
	case SA7POINTOFCONTACTEMAIL : return "String";
		}
		return null;
	}
	

}
