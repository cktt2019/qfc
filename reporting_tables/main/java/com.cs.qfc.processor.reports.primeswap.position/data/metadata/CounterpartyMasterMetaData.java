package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class CounterpartyMasterMetaData  implements MetaData {
	
	private static final String ID = "id";
	private static final String CP1ASOFDATE = "cp1AsOfDate";
	private static final String CP2COUNTERPARTYIDENTIFIER = "cp2CounterpartyIdentifier";
	private static final String CP3HASLEIBEENUSEDCPI = "cp3HasLeiBeenUsedCpi";
	private static final String CP4LEGALNAMECOUNTERPARTY = "cp4LegalNameCounterparty";
	private static final String CP5DOMICILE = "cp5Domicile";
	private static final String CP6JURISDICTIONUNDERWHICHINCORPORATED = "cp6JurisdictionUnderWhichIncorporated";
	private static final String CP7IMMEDIATEPARENTENTITYIDENTIFIER = "cp7ImmediateParentEntityIdentifier";
	private static final String CP8HASLEIBEENUSEDFORPARENT = "cp8HasLeiBeenUsedForParent";
	private static final String CP9LEGALNAMEOFIMMEDIATEPARENTENTITY = "cp9LegalNameOfImmediateParentEntity";
	private static final String CP10ULTIMATEPARENTENTITYIDENTIFIER = "cp10UltimateParentEntityIdentifier";
	private static final String CP11HASLEIBEENUSEDFORULTIMATEPARENT = "cp11HasLeiBeenUsedForUltimateParent";
	private static final String LEGALNAMEOFULTIMATEPARENT = "legalNameOfUltimateParent";

   
     public static final String TABLE = "CounterpartyMaster";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case ID : return "id";
	case CP1ASOFDATE : return "cp1_as_of_date";
	case CP2COUNTERPARTYIDENTIFIER : return "cp2_counterparty_identifier";
	case CP3HASLEIBEENUSEDCPI : return "cp3_has_lei_been_used_cpi";
	case CP4LEGALNAMECOUNTERPARTY : return "cp4_legal_name_counterparty";
	case CP5DOMICILE : return "cp5_domicile";
	case CP6JURISDICTIONUNDERWHICHINCORPORATED : return "cp6_jurisdiction_under_which_incorporated";
	case CP7IMMEDIATEPARENTENTITYIDENTIFIER : return "cp7_immediate_parent_entity_identifier";
	case CP8HASLEIBEENUSEDFORPARENT : return "cp8_has_lei_been_used_for_parent";
	case CP9LEGALNAMEOFIMMEDIATEPARENTENTITY : return "cp9_legal_name_of_immediate_parent_entity";
	case CP10ULTIMATEPARENTENTITYIDENTIFIER : return "cp10_ultimate_parent_entity_identifier";
	case CP11HASLEIBEENUSEDFORULTIMATEPARENT : return "cp11_has_lei_been_used_for_ultimate_parent";
	case LEGALNAMEOFULTIMATEPARENT : return "legal_name_of_ultimate_parent";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case ID : return "String";
	case CP1ASOFDATE : return "String";
	case CP2COUNTERPARTYIDENTIFIER : return "String";
	case CP3HASLEIBEENUSEDCPI : return "String";
	case CP4LEGALNAMECOUNTERPARTY : return "String";
	case CP5DOMICILE : return "String";
	case CP6JURISDICTIONUNDERWHICHINCORPORATED : return "String";
	case CP7IMMEDIATEPARENTENTITYIDENTIFIER : return "String";
	case CP8HASLEIBEENUSEDFORPARENT : return "String";
	case CP9LEGALNAMEOFIMMEDIATEPARENTENTITY : return "String";
	case CP10ULTIMATEPARENTENTITYIDENTIFIER : return "String";
	case CP11HASLEIBEENUSEDFORULTIMATEPARENT : return "String";
	case LEGALNAMEOFULTIMATEPARENT : return "String";
		}
		return null;
	}
	

}
