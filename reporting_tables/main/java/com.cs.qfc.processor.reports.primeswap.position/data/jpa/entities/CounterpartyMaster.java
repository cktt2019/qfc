package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "CounterpartyMaster" )
	@Entity
	public class CounterpartyMaster implements Serializable {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "counterpartymaster_seq")
	    @SequenceGenerator(name = "counterpartymaster_seq", sequenceName = "counterpartymaster_seq")
	  @Column(name = "id"  )
	private String id ;
 
	
	  @Column(name = "cp1_as_of_date"  )
	private String cp1AsOfDate ;
 
	
	  @Column(name = "cp2_counterparty_identifier"  )
	private String cp2CounterpartyIdentifier ;
 
	
	  @Column(name = "cp3_has_lei_been_used_cpi"  )
	private String cp3HasLeiBeenUsedCpi ;
 
	
	  @Column(name = "cp4_legal_name_counterparty"  )
	private String cp4LegalNameCounterparty ;
 
	
	  @Column(name = "cp5_domicile"  )
	private String cp5Domicile ;
 
	
	  @Column(name = "cp6_jurisdiction_under_which_incorporated"  )
	private String cp6JurisdictionUnderWhichIncorporated ;
 
	
	  @Column(name = "cp7_immediate_parent_entity_identifier"  )
	private String cp7ImmediateParentEntityIdentifier ;
 
	
	  @Column(name = "cp8_has_lei_been_used_for_parent"  )
	private String cp8HasLeiBeenUsedForParent ;
 
	
	  @Column(name = "cp9_legal_name_of_immediate_parent_entity"  )
	private String cp9LegalNameOfImmediateParentEntity ;
 
	
	  @Column(name = "cp10_ultimate_parent_entity_identifier"  )
	private String cp10UltimateParentEntityIdentifier ;
 
	
	  @Column(name = "cp11_has_lei_been_used_for_ultimate_parent"  )
	private String cp11HasLeiBeenUsedForUltimateParent ;
 
	
	  @Column(name = "legal_name_of_ultimate_parent"  )
	private String legalNameOfUltimateParent ;
 
   
   
    


      
      
}
