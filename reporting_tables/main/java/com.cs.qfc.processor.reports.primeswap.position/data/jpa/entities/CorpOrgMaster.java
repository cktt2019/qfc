package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "CorpOrgMaster" )
	@Entity
	public class CorpOrgMaster implements Serializable {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "corporgmaster_seq")
	    @SequenceGenerator(name = "corporgmaster_seq", sequenceName = "corporgmaster_seq")
	  @Column(name = "id"  )
	private String id ;
 
	
	  @Column(name = "co1_as_of_date"  )
	private String co1AsOfDate ;
 
	
	  @Column(name = "co2_entity_identifier"  )
	private String co2EntityIdentifier ;
 
	
	  @Column(name = "co3_has_lei_been_used"  )
	private String co3HasLeiBeenUsed ;
 
	
	  @Column(name = "co4_legal_name_of_entity"  )
	private String co4LegalNameOfEntity ;
 
	
	  @Column(name = "co5_immediate_parent_entity_identifier"  )
	private String co5ImmediateParentEntityIdentifier ;
 
	
	  @Column(name = "co6_has_lei_been_used_for_immediate_parent"  )
	private String co6HasLeiBeenUsedForImmediateParent ;
 
	
	  @Column(name = "co7_legal_name_of_immediate_parent"  )
	private String co7LegalNameOfImmediateParent ;
 
	
	  @Column(name = "co8_percentage_ownership_of_immediate_parent_in_entity"  )
	private String co8PercentageOwnershipOfImmediateParentInEntity ;
 
	
	  @Column(name = "co9_entity_type"  )
	private String co9EntityType ;
 
	
	  @Column(name = "co10_domicile"  )
	private String co10Domicile ;
 
	
	  @Column(name = "co11_jurisdiction_under_which_incorporated"  )
	private String co11JurisdictionUnderWhichIncorporated ;
 
   
   
    


      
      
}
