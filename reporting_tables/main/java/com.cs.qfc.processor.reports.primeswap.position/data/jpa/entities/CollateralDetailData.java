package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "CollateralDetailData"  , schema ="SIERRA_QFC" )
	@Entity
	public class CollateralDetailData implements Serializable {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "collateraldetaildata_seq")
	    @SequenceGenerator(name = "collateraldetaildata_seq", sequenceName = "SIERRA_QFC.collateraldetaildata_seq")
	  @Column(name = "id"  )
	private Integer id ;
 
	
	  @Column(name = "source_txn_id"  )
	private String sourceTxnId ;
 
	
	  @Column(name = "a41_as_of_date"  )
	private String a41AsOfDate ;
 
	
	  @Column(name = "a42_records_entity_identifier"  )
	private String a42RecordsEntityIdentifier ;
 
	
	  @Column(name = "a43collaterao_posted_received_flag"  )
	private String a43collateraoPostedReceivedFlag ;
 
	
	  @Column(name = "a44_counterparty_identifier"  )
	private String a44CounterpartyIdentifier ;
 
	
	  @Column(name = "a45_netting_agreement_identifier"  )
	private String a45NettingAgreementIdentifier ;
 
	
	  @Column(name = "a46_unique_collateral_item_identifier"  )
	private String a46UniqueCollateralItemIdentifier ;
 
	
	  @Column(name = "a47_original_face_amt_collateral_item_local_curr"  )
	private String a47OriginalFaceAmtCollateralItemLocalCurr ;
 
	
	  @Column(name = "a48_local_currency_of_collateral_item"  )
	private String a48LocalCurrencyOfCollateralItem ;
 
	
	  @Column(name = "a49_market_value_amount_of_collateral_item_in_usd"  )
	private String a49MarketValueAmountOfCollateralItemInUsd ;
 
	
	  @Column(name = "a410_description_of_collateral_item"  )
	private String a410DescriptionOfCollateralItem ;
 
	
	  @Column(name = "a411_asset_classification"  )
	private String a411AssetClassification ;
 
	
	  @Column(name = "a412_collateral_portfolio_segregation_status"  )
	private String a412CollateralPortfolioSegregationStatus ;
 
	
	  @Column(name = "a413_collateral_location"  )
	private String a413CollateralLocation ;
 
	
	  @Column(name = "a414_collateral_jurisdiction"  )
	private String a414CollateralJurisdiction ;
 
	
	  @Column(name = "a415_collateral_rehypo_allowed"  )
	private String a415CollateralRehypoAllowed ;
 
   
   
    


      
      
}
