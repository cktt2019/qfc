package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "SafekeepingAgent" )
	@Entity
	public class SafekeepingAgent implements Serializable {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "safekeepingagent_seq")
	    @SequenceGenerator(name = "safekeepingagent_seq", sequenceName = "safekeepingagent_seq")
	  @Column(name = "id"  )
	private String id ;
 
	
	  @Column(name = "sa1_as_of_date"  )
	private String sa1AsOfDate ;
 
	
	  @Column(name = "sa2_safekeeping_agent_identifier"  )
	private String sa2SafekeepingAgentIdentifier ;
 
	
	  @Column(name = "sa3_legal_nale_of_safekeeping_agent"  )
	private String sa3LegalNaleOfSafekeepingAgent ;
 
	
	  @Column(name = "sa4_point_of_contact_name"  )
	private String sa4PointOfContactName ;
 
	
	  @Column(name = "sa5_point_of_contact_address"  )
	private String sa5PointOfContactAddress ;
 
	
	  @Column(name = "sa6_point_of_contact_phone"  )
	private String sa6PointOfContactPhone ;
 
	
	  @Column(name = "sa7_point_of_contact_email"  )
	private String sa7PointOfContactEmail ;
 
   
   
    


      
      
}
