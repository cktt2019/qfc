package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "BookingLocation" )
	@Entity
	public class BookingLocation implements Serializable {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bookinglocation_seq")
	    @SequenceGenerator(name = "bookinglocation_seq", sequenceName = "bookinglocation_seq")
	  @Column(name = "id"  )
	private String id ;
 
	
	  @Column(name = "bl1_as_of_date"  )
	private String bl1AsOfDate ;
 
	
	  @Column(name = "bl2_records_entity_identifier"  )
	private String bl2RecordsEntityIdentifier ;
 
	
	  @Column(name = "bl3_internal_booking_location_identifier"  )
	private String bl3InternalBookingLocationIdentifier ;
 
	
	  @Column(name = "bl4_unique_booking_unit_desk_identifier"  )
	private String bl4UniqueBookingUnitDeskIdentifier ;
 
	
	  @Column(name = "bl5_unique_booking_unit_desk_description"  )
	private String bl5UniqueBookingUnitDeskDescription ;
 
	
	  @Column(name = "bl6_booking_unit_desk_contact_phone"  )
	private String bl6BookingUnitDeskContactPhone ;
 
	
	  @Column(name = "bl7_booking_unit_desk_contact_email"  )
	private String bl7BookingUnitDeskContactEmail ;
 
   
   
    


      
      
}
