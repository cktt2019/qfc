package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "PositionLevelData" )
	@Entity
	public class PositionLevelData implements Serializable {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "positionleveldata_seq")
	    @SequenceGenerator(name = "positionleveldata_seq", sequenceName = "positionleveldata_seq")
	  @Column(name = "id"  )
	private String id ;
 
	
	  @Column(name = "a11_as_of_date"  )
	private java.sql.Date a11AsOfDate ;
 
	
	  @Column(name = "a12_records_entity_identifier"  )
	private String a12RecordsEntityIdentifier ;
 
	
	  @Column(name = "a13_positions_identifier"  )
	private String a13PositionsIdentifier ;
 
	
	  @Column(name = "a15_internal_booking_location_identifier"  )
	private String a15InternalBookingLocationIdentifier ;
 
	
	  @Column(name = "a16_unique_booking_unit_desk_identifier"  )
	private String a16UniqueBookingUnitDeskIdentifier ;
 
	
	  @Column(name = "a17_type_of_qfc"  )
	private String a17TypeOfQfc ;
 
	
	  @Column(name = "a_171_type_of_qfc_covered_guarantee"  )
	private String a171TypeOfQfcCoveredGuarantee ;
 
	
	  @Column(name = "a172_underlying_qfc_obligator_identifier"  )
	private String a172UnderlyingQfcObligatorIdentifier ;
 
	
	  @Column(name = "a18_agreement_identifier"  )
	private String a18AgreementIdentifier ;
 
	
	  @Column(name = "a19_netting_agreement_idrntifier"  )
	private String a19NettingAgreementIdrntifier ;
 
	
	  @Column(name = "a110_netting_agreement_counterparty_identifier"  )
	private String a110NettingAgreementCounterpartyIdentifier ;
 
	
	  @Column(name = "a111_trade_date"  )
	private String a111TradeDate ;
 
	
	  @Column(name = "a112_termination_date"  )
	private String a112TerminationDate ;
 
	
	  @Column(name = "a113_next_put_call_cancellation_date"  )
	private String a113NextPutCallCancellationDate ;
 
	
	  @Column(name = "a114_next_payment_date"  )
	private String a114NextPaymentDate ;
 
	
	  @Column(name = "a115_local_currency_of_position"  )
	private String a115LocalCurrencyOfPosition ;
 
	
	  @Column(name = "a116_current_market_value_in_local_currency"  )
	private String a116CurrentMarketValueInLocalCurrency ;
 
	
	  @Column(name = "a117_current_market_value_in_usd"  )
	private String a117CurrentMarketValueInUsd ;
 
	
	  @Column(name = "a118_asset_classification"  )
	private String a118AssetClassification ;
 
	
	  @Column(name = "a119_notional_or_principal_amount_local_currency"  )
	private String a119NotionalPrincipalAmountLocalCurrency ;
 
	
	  @Column(name = "a120_notional_or_principal_amount_usd"  )
	private String a120NotionalPrincipalAmountUsd ;
 
	
	  @Column(name = "a121_covered_by_third_party_credit_enhancement"  )
	private String a121CoveredByThirdPartyCreditEnhancement ;
 
	
	  @Column(name = "a1211_third_party_credit_enhancement_provider_identifier"  )
	private String a1211ThirdPartyCreditEnhancementProviderIdentifier ;
 
	
	  @Column(name = "a1212_third_party_credit_enhancement_agreement_identifier"  )
	private String a1212ThirdPartyCreditEnhancementAgreementIdentifier ;
 
	
	  @Column(name = "a213_counterparty_third_party_credit_enhancement"  )
	private String a213CounterpartyThirdPartyCreditEnhancement ;
 
	
	  @Column(name = "a1214_counterparty_third_party_credit_enhancement_provider_identifier"  )
	private String a1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier ;
 
	
	  @Column(name = "a1215_counterparty_third_party_credit_enhancement_agreement_identifier"  )
	private String a1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier ;
 
	
	  @Column(name = "a122_related_position_of_records_entity"  )
	private String a122RelatedPositionOfRecordsEntity ;
 
	
	  @Column(name = "a123_reference_number_for_any_related_loan"  )
	private String a123ReferenceNumberForAnyRelatedLoan ;
 
	
	  @Column(name = "a124_identifier_of_the_lender_of_related_loan"  )
	private String a124IdentifierOfTheLenderOfRelatedLoan ;
 
	
	  @Column(name = "source_txn_id"  )
	private String sourceTxnId ;
 
	
	  @Column(name = "a14_counterparty_identifier"  )
	private String a14CounterpartyIdentifier ;
 
   
   
    


      
      
}
