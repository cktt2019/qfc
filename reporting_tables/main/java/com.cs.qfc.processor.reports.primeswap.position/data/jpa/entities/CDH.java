package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "CDH" )
	@Entity
	public class CDH implements Serializable {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cdh_seq")
	    @SequenceGenerator(name = "cdh_seq", sequenceName = "cdh_seq")
	  @Column(name = "id"  )
	private Integer id ;
 
	
	  @Column(name = "employee_id"  )
	private String employeeId ;
 
	
	  @Column(name = "nt_login_id"  )
	private String ntLoginId ;
 
	
	  @Column(name = "first_name"  )
	private String firstName ;
 
	
	  @Column(name = "last_name"  )
	private String lastName ;
 
	
	  @Column(name = "country"  )
	private String country ;
 
	
	  @Column(name = "supervisor_id"  )
	private String supervisorId ;
 
	
	  @Column(name = "pid"  )
	private String pid ;
 
	
	  @Column(name = "gid"  )
	private String gid ;
 
	
	  @Column(name = "telephone_country"  )
	private String telephoneCountry ;
 
	
	  @Column(name = "telephone_area"  )
	private String telephoneArea ;
 
	
	  @Column(name = "talephone_number"  )
	private String talephoneNumber ;
 
   
   
    


      
      
}
