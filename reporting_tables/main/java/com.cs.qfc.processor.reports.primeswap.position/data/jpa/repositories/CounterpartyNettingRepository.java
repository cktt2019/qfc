package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.CounterpartyNetting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link CounterpartyNetting} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface CounterpartyNettingRepository extends JpaRepository<CounterpartyNetting, Integer>{


	public List<CounterpartyNetting> findBySourceTxnId (String sourceTxnId);
	public List<CounterpartyNetting> findByA21AsOfDate (String a21AsOfDate);
	public List<CounterpartyNetting> findByA22RecordsEntityIdentifier (String a22RecordsEntityIdentifier);
	public List<CounterpartyNetting> findByA23NettingAgreementCounterpartyIdentifier (String a23NettingAgreementCounterpartyIdentifier);
	public List<CounterpartyNetting> findByA24NettingAgreementIdentifier (String a24NettingAgreementIdentifier);
	public List<CounterpartyNetting> findByA241UnderlyingQfcObligatorIdentifier (String a241UnderlyingQfcObligatorIdentifier);
	public List<CounterpartyNetting> findByA25CoveredByThirdPartyCreditEnhancement (String a25CoveredByThirdPartyCreditEnhancement);
	public List<CounterpartyNetting> findByA251ThirdPartyCreditEnhancementProviderIdentifier (String a251ThirdPartyCreditEnhancementProviderIdentifier);
	public List<CounterpartyNetting> findByA252ThirdPartyCreditEnhancementAgreementIdentifier (String a252ThirdPartyCreditEnhancementAgreementIdentifier);
	public List<CounterpartyNetting> findByA253CounterpartyThirdPartyCreditEnhancement (String a253CounterpartyThirdPartyCreditEnhancement);
	public List<CounterpartyNetting> findByA254CounterpartyThirdPartyCreditEnhancementProviderIdentifier (String a254CounterpartyThirdPartyCreditEnhancementProviderIdentifier);
	public List<CounterpartyNetting> findByA255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier (String a255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier);
	public List<CounterpartyNetting> findByA26AggregateCurrMarketValueAllPositionsUsd (String a26AggregateCurrMarketValueAllPositionsUsd);
	public List<CounterpartyNetting> findByA27AggregateCurrMarketValuePositivePositionsUsd (String a27AggregateCurrMarketValuePositivePositionsUsd);
	public List<CounterpartyNetting> findByA28AggregateCurrMarketValueNegativePositionsUsd (String a28AggregateCurrMarketValueNegativePositionsUsd);
	public List<CounterpartyNetting> findByA29CurrMarketValueAllCollateralRecordsEntityInUsd (String a29CurrMarketValueAllCollateralRecordsEntityInUsd);
	public List<CounterpartyNetting> findByA210CurrMarketValueAllCollateralCounterpartyInUsd (String a210CurrMarketValueAllCollateralCounterpartyInUsd);
	public List<CounterpartyNetting> findByA211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd (String a211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd);
	public List<CounterpartyNetting> findByA212CurrMarketValueAllCollateralCounterpartyRehypoInUsd (String a212CurrMarketValueAllCollateralCounterpartyRehypoInUsd);
	public List<CounterpartyNetting> findByA213RecordsEntityCollateralNet (String a213RecordsEntityCollateralNet);
	public List<CounterpartyNetting> findByA214CounterpartyCollateralNet (String a214CounterpartyCollateralNet);
	public List<CounterpartyNetting> findByA215NextMarginPaymentDate (String a215NextMarginPaymentDate);
	public List<CounterpartyNetting> findByA216NextMarginPaymentAmountUsd (String a216NextMarginPaymentAmountUsd);
	public List<CounterpartyNetting> findByA217SafekeepingAgentIdentifierRecordsEntity (String a217SafekeepingAgentIdentifierRecordsEntity);
	public List<CounterpartyNetting> findByA218SafekeepingAgentIdentifierCounterparty (String a218SafekeepingAgentIdentifierCounterparty);
	

	
	public Page<CounterpartyNetting> findAll (Pageable page);
	
	

	

	
	}







