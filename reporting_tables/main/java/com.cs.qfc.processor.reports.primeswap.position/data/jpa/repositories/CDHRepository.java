package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.CDH;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link CDH} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface CDHRepository extends JpaRepository<CDH, Integer>{


	public List<CDH> findByEmployeeId (String employeeId);
	public List<CDH> findByNtLoginId (String ntLoginId);
	public List<CDH> findByFirstName (String firstName);
	public List<CDH> findByLastName (String lastName);
	public List<CDH> findByCountry (String country);
	public List<CDH> findBySupervisorId (String supervisorId);
	public List<CDH> findByPid (String pid);
	public List<CDH> findByGid (String gid);
	public List<CDH> findByTelephoneCountry (String telephoneCountry);
	public List<CDH> findByTelephoneArea (String telephoneArea);
	public List<CDH> findByTalephoneNumber (String talephoneNumber);
	

	
	public Page<CDH> findAll (Pageable page);
	
	

	

	
	}







