package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.CounterpartyMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link CounterpartyMaster} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface CounterpartyMasterRepository extends JpaRepository<CounterpartyMaster, String>{


	public List<CounterpartyMaster> findByCp1AsOfDate (String cp1AsOfDate);
	public List<CounterpartyMaster> findByCp2CounterpartyIdentifier (String cp2CounterpartyIdentifier);
	public List<CounterpartyMaster> findByCp3HasLeiBeenUsedCpi (String cp3HasLeiBeenUsedCpi);
	public List<CounterpartyMaster> findByCp4LegalNameCounterparty (String cp4LegalNameCounterparty);
	public List<CounterpartyMaster> findByCp5Domicile (String cp5Domicile);
	public List<CounterpartyMaster> findByCp6JurisdictionUnderWhichIncorporated (String cp6JurisdictionUnderWhichIncorporated);
	public List<CounterpartyMaster> findByCp7ImmediateParentEntityIdentifier (String cp7ImmediateParentEntityIdentifier);
	public List<CounterpartyMaster> findByCp8HasLeiBeenUsedForParent (String cp8HasLeiBeenUsedForParent);
	public List<CounterpartyMaster> findByCp9LegalNameOfImmediateParentEntity (String cp9LegalNameOfImmediateParentEntity);
	public List<CounterpartyMaster> findByCp10UltimateParentEntityIdentifier (String cp10UltimateParentEntityIdentifier);
	public List<CounterpartyMaster> findByCp11HasLeiBeenUsedForUltimateParent (String cp11HasLeiBeenUsedForUltimateParent);
	public List<CounterpartyMaster> findByLegalNameOfUltimateParent (String legalNameOfUltimateParent);
	

	
	public Page<CounterpartyMaster> findAll (Pageable page);
	
	

	

	
	}







