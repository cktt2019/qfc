package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.SafekeepingAgent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link SafekeepingAgent} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface SafekeepingAgentRepository extends JpaRepository<SafekeepingAgent, String>{


	public List<SafekeepingAgent> findBySa1AsOfDate (String sa1AsOfDate);
	public List<SafekeepingAgent> findBySa2SafekeepingAgentIdentifier (String sa2SafekeepingAgentIdentifier);
	public List<SafekeepingAgent> findBySa3LegalNaleOfSafekeepingAgent (String sa3LegalNaleOfSafekeepingAgent);
	public List<SafekeepingAgent> findBySa4PointOfContactName (String sa4PointOfContactName);
	public List<SafekeepingAgent> findBySa5PointOfContactAddress (String sa5PointOfContactAddress);
	public List<SafekeepingAgent> findBySa6PointOfContactPhone (String sa6PointOfContactPhone);
	public List<SafekeepingAgent> findBySa7PointOfContactEmail (String sa7PointOfContactEmail);
	

	
	public Page<SafekeepingAgent> findAll (Pageable page);
	
	

	

	
	}







