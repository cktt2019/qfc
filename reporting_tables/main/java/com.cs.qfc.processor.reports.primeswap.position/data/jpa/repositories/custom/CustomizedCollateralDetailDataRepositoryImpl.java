package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.CollateralDetailData;
import com.cs.qfc.data.metadata.CollateralDetailDataMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.CollateralDetailDataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link CollateralDetailData} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedCollateralDetailDataRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedCollateralDetailDataRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<CollateralDetailDataDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + CollateralDetailDataMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new CollateralDetailDataMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new CollateralDetailDataMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new CollateralDetailDataMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<CollateralDetailDataDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new CollateralDetailDataRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			CollateralDetailDataMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class CollateralDetailDataRowMapper implements RowMapper<CollateralDetailDataDTO> {
		@Override
		public CollateralDetailDataDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			CollateralDetailDataDTO ret = new CollateralDetailDataDTO();


			
           ret.setId (rs.getInt("id"));
           ret.setSourceTxnId (rs.getString("source_txn_id"));
           ret.setA41AsOfDate (rs.getString("a41_as_of_date"));
           ret.setA42RecordsEntityIdentifier (rs.getString("a42_records_entity_identifier"));
           ret.setA43collateraoPostedReceivedFlag (rs.getString("a43collaterao_posted_received_flag"));
           ret.setA44CounterpartyIdentifier (rs.getString("a44_counterparty_identifier"));
           ret.setA45NettingAgreementIdentifier (rs.getString("a45_netting_agreement_identifier"));
           ret.setA46UniqueCollateralItemIdentifier (rs.getString("a46_unique_collateral_item_identifier"));
           ret.setA47OriginalFaceAmtCollateralItemLocalCurr (rs.getString("a47_original_face_amt_collateral_item_local_curr"));
           ret.setA48LocalCurrencyOfCollateralItem (rs.getString("a48_local_currency_of_collateral_item"));
           ret.setA49MarketValueAmountOfCollateralItemInUsd (rs.getString("a49_market_value_amount_of_collateral_item_in_usd"));
           ret.setA410DescriptionOfCollateralItem (rs.getString("a410_description_of_collateral_item"));
           ret.setA411AssetClassification (rs.getString("a411_asset_classification"));
           ret.setA412CollateralPortfolioSegregationStatus (rs.getString("a412_collateral_portfolio_segregation_status"));
           ret.setA413CollateralLocation (rs.getString("a413_collateral_location"));
           ret.setA414CollateralJurisdiction (rs.getString("a414_collateral_jurisdiction"));
           ret.setA415CollateralRehypoAllowed (rs.getString("a415_collateral_rehypo_allowed"));

			return ret;
		}
	}

}
