package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.CorpOrgMaster;
import com.cs.qfc.data.metadata.CorpOrgMasterMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.CorpOrgMasterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link CorpOrgMaster} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedCorpOrgMasterRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedCorpOrgMasterRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<CorpOrgMasterDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + CorpOrgMasterMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new CorpOrgMasterMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new CorpOrgMasterMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new CorpOrgMasterMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<CorpOrgMasterDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new CorpOrgMasterRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			CorpOrgMasterMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class CorpOrgMasterRowMapper implements RowMapper<CorpOrgMasterDTO> {
		@Override
		public CorpOrgMasterDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			CorpOrgMasterDTO ret = new CorpOrgMasterDTO();


			
           ret.setId (rs.getString("id"));
           ret.setCo1AsOfDate (rs.getString("co1_as_of_date"));
           ret.setCo2EntityIdentifier (rs.getString("co2_entity_identifier"));
           ret.setCo3HasLeiBeenUsed (rs.getString("co3_has_lei_been_used"));
           ret.setCo4LegalNameOfEntity (rs.getString("co4_legal_name_of_entity"));
           ret.setCo5ImmediateParentEntityIdentifier (rs.getString("co5_immediate_parent_entity_identifier"));
           ret.setCo6HasLeiBeenUsedForImmediateParent (rs.getString("co6_has_lei_been_used_for_immediate_parent"));
           ret.setCo7LegalNameOfImmediateParent (rs.getString("co7_legal_name_of_immediate_parent"));
           ret.setCo8PercentageOwnershipOfImmediateParentInEntity (rs.getString("co8_percentage_ownership_of_immediate_parent_in_entity"));
           ret.setCo9EntityType (rs.getString("co9_entity_type"));
           ret.setCo10Domicile (rs.getString("co10_domicile"));
           ret.setCo11JurisdictionUnderWhichIncorporated (rs.getString("co11_jurisdiction_under_which_incorporated"));

			return ret;
		}
	}

}
