package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.CorpOrgMaster;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CorpOrgMasterDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link CorpOrgMaster} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedCorpOrgMasterRepository {
	
	public Collection<CorpOrgMasterDTO> findByCustomCriteria (SearchInfoDTO info);

	}







