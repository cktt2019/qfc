package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.SafekeepingAgent;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.SafekeepingAgentDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link SafekeepingAgent} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedSafekeepingAgentRepository {
	
	public Collection<SafekeepingAgentDTO> findByCustomCriteria (SearchInfoDTO info);

	}







