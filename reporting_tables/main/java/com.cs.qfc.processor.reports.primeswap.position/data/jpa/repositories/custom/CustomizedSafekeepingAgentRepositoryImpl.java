package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.SafekeepingAgent;
import com.cs.qfc.data.metadata.SafekeepingAgentMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.SafekeepingAgentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link SafekeepingAgent} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedSafekeepingAgentRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedSafekeepingAgentRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<SafekeepingAgentDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + SafekeepingAgentMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new SafekeepingAgentMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new SafekeepingAgentMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new SafekeepingAgentMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<SafekeepingAgentDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new SafekeepingAgentRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			SafekeepingAgentMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class SafekeepingAgentRowMapper implements RowMapper<SafekeepingAgentDTO> {
		@Override
		public SafekeepingAgentDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			SafekeepingAgentDTO ret = new SafekeepingAgentDTO();


			
           ret.setId (rs.getString("id"));
           ret.setSa1AsOfDate (rs.getString("sa1_as_of_date"));
           ret.setSa2SafekeepingAgentIdentifier (rs.getString("sa2_safekeeping_agent_identifier"));
           ret.setSa3LegalNaleOfSafekeepingAgent (rs.getString("sa3_legal_nale_of_safekeeping_agent"));
           ret.setSa4PointOfContactName (rs.getString("sa4_point_of_contact_name"));
           ret.setSa5PointOfContactAddress (rs.getString("sa5_point_of_contact_address"));
           ret.setSa6PointOfContactPhone (rs.getString("sa6_point_of_contact_phone"));
           ret.setSa7PointOfContactEmail (rs.getString("sa7_point_of_contact_email"));

			return ret;
		}
	}

}
