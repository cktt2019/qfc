package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.CounterpartyMaster;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CounterpartyMasterDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link CounterpartyMaster} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedCounterpartyMasterRepository {
	
	public Collection<CounterpartyMasterDTO> findByCustomCriteria (SearchInfoDTO info);

	}







