package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.CDH;
import com.cs.qfc.data.metadata.CDHMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.CDHDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link CDH} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedCDHRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedCDHRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<CDHDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + CDHMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new CDHMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new CDHMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new CDHMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<CDHDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new CDHRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			CDHMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class CDHRowMapper implements RowMapper<CDHDTO> {
		@Override
		public CDHDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			CDHDTO ret = new CDHDTO();


			
           ret.setId (rs.getInt("id"));
           ret.setEmployeeId (rs.getString("employee_id"));
           ret.setNtLoginId (rs.getString("nt_login_id"));
           ret.setFirstName (rs.getString("first_name"));
           ret.setLastName (rs.getString("last_name"));
           ret.setCountry (rs.getString("country"));
           ret.setSupervisorId (rs.getString("supervisor_id"));
           ret.setPid (rs.getString("pid"));
           ret.setGid (rs.getString("gid"));
           ret.setTelephoneCountry (rs.getString("telephone_country"));
           ret.setTelephoneArea (rs.getString("telephone_area"));
           ret.setTalephoneNumber (rs.getString("talephone_number"));

			return ret;
		}
	}

}
