package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.CounterpartyMaster;
import com.cs.qfc.data.metadata.CounterpartyMasterMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.CounterpartyMasterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link CounterpartyMaster} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedCounterpartyMasterRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedCounterpartyMasterRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<CounterpartyMasterDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + CounterpartyMasterMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new CounterpartyMasterMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new CounterpartyMasterMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new CounterpartyMasterMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<CounterpartyMasterDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new CounterpartyMasterRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			CounterpartyMasterMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class CounterpartyMasterRowMapper implements RowMapper<CounterpartyMasterDTO> {
		@Override
		public CounterpartyMasterDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			CounterpartyMasterDTO ret = new CounterpartyMasterDTO();


			
           ret.setId (rs.getString("id"));
           ret.setCp1AsOfDate (rs.getString("cp1_as_of_date"));
           ret.setCp2CounterpartyIdentifier (rs.getString("cp2_counterparty_identifier"));
           ret.setCp3HasLeiBeenUsedCpi (rs.getString("cp3_has_lei_been_used_cpi"));
           ret.setCp4LegalNameCounterparty (rs.getString("cp4_legal_name_counterparty"));
           ret.setCp5Domicile (rs.getString("cp5_domicile"));
           ret.setCp6JurisdictionUnderWhichIncorporated (rs.getString("cp6_jurisdiction_under_which_incorporated"));
           ret.setCp7ImmediateParentEntityIdentifier (rs.getString("cp7_immediate_parent_entity_identifier"));
           ret.setCp8HasLeiBeenUsedForParent (rs.getString("cp8_has_lei_been_used_for_parent"));
           ret.setCp9LegalNameOfImmediateParentEntity (rs.getString("cp9_legal_name_of_immediate_parent_entity"));
           ret.setCp10UltimateParentEntityIdentifier (rs.getString("cp10_ultimate_parent_entity_identifier"));
           ret.setCp11HasLeiBeenUsedForUltimateParent (rs.getString("cp11_has_lei_been_used_for_ultimate_parent"));
           ret.setLegalNameOfUltimateParent (rs.getString("legal_name_of_ultimate_parent"));

			return ret;
		}
	}

}
