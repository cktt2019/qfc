package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.BookingLocation;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.BookingLocationDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link BookingLocation} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedBookingLocationRepository {
	
	public Collection<BookingLocationDTO> findByCustomCriteria (SearchInfoDTO info);

	}







