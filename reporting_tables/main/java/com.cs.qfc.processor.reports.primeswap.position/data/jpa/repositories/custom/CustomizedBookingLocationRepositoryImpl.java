package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.BookingLocation;
import com.cs.qfc.data.metadata.BookingLocationMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.BookingLocationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link BookingLocation} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedBookingLocationRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedBookingLocationRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<BookingLocationDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + BookingLocationMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new BookingLocationMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new BookingLocationMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new BookingLocationMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<BookingLocationDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new BookingLocationRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			BookingLocationMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class BookingLocationRowMapper implements RowMapper<BookingLocationDTO> {
		@Override
		public BookingLocationDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			BookingLocationDTO ret = new BookingLocationDTO();


			
           ret.setId (rs.getString("id"));
           ret.setBl1AsOfDate (rs.getString("bl1_as_of_date"));
           ret.setBl2RecordsEntityIdentifier (rs.getString("bl2_records_entity_identifier"));
           ret.setBl3InternalBookingLocationIdentifier (rs.getString("bl3_internal_booking_location_identifier"));
           ret.setBl4UniqueBookingUnitDeskIdentifier (rs.getString("bl4_unique_booking_unit_desk_identifier"));
           ret.setBl5UniqueBookingUnitDeskDescription (rs.getString("bl5_unique_booking_unit_desk_description"));
           ret.setBl6BookingUnitDeskContactPhone (rs.getString("bl6_booking_unit_desk_contact_phone"));
           ret.setBl7BookingUnitDeskContactEmail (rs.getString("bl7_booking_unit_desk_contact_email"));

			return ret;
		}
	}

}
