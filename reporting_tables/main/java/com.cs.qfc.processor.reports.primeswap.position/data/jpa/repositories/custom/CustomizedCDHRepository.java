package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.CDH;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CDHDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link CDH} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedCDHRepository {
	
	public Collection<CDHDTO> findByCustomCriteria (SearchInfoDTO info);

	}







