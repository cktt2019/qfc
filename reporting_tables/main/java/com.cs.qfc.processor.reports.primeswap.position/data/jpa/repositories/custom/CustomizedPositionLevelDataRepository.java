package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.PositionLevelData;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.PositionLevelDataDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link PositionLevelData} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedPositionLevelDataRepository {
	
	public Collection<PositionLevelDataDTO> findByCustomCriteria (SearchInfoDTO info);

	}







