package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.CollateralDetailData;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CollateralDetailDataDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link CollateralDetailData} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedCollateralDetailDataRepository {
	
	public Collection<CollateralDetailDataDTO> findByCustomCriteria (SearchInfoDTO info);

	}







