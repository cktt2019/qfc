package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.CorpOrgMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link CorpOrgMaster} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface CorpOrgMasterRepository extends JpaRepository<CorpOrgMaster, String>{


	public List<CorpOrgMaster> findByCo1AsOfDate (String co1AsOfDate);
	public List<CorpOrgMaster> findByCo2EntityIdentifier (String co2EntityIdentifier);
	public List<CorpOrgMaster> findByCo3HasLeiBeenUsed (String co3HasLeiBeenUsed);
	public List<CorpOrgMaster> findByCo4LegalNameOfEntity (String co4LegalNameOfEntity);
	public List<CorpOrgMaster> findByCo5ImmediateParentEntityIdentifier (String co5ImmediateParentEntityIdentifier);
	public List<CorpOrgMaster> findByCo6HasLeiBeenUsedForImmediateParent (String co6HasLeiBeenUsedForImmediateParent);
	public List<CorpOrgMaster> findByCo7LegalNameOfImmediateParent (String co7LegalNameOfImmediateParent);
	public List<CorpOrgMaster> findByCo8PercentageOwnershipOfImmediateParentInEntity (String co8PercentageOwnershipOfImmediateParentInEntity);
	public List<CorpOrgMaster> findByCo9EntityType (String co9EntityType);
	public List<CorpOrgMaster> findByCo10Domicile (String co10Domicile);
	public List<CorpOrgMaster> findByCo11JurisdictionUnderWhichIncorporated (String co11JurisdictionUnderWhichIncorporated);
	

	
	public Page<CorpOrgMaster> findAll (Pageable page);
	
	

	

	
	}







