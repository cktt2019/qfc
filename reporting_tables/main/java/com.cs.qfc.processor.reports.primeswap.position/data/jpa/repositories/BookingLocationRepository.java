package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.BookingLocation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link BookingLocation} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface BookingLocationRepository extends JpaRepository<BookingLocation, String>{


	public List<BookingLocation> findByBl1AsOfDate (String bl1AsOfDate);
	public List<BookingLocation> findByBl2RecordsEntityIdentifier (String bl2RecordsEntityIdentifier);
	public List<BookingLocation> findByBl3InternalBookingLocationIdentifier (String bl3InternalBookingLocationIdentifier);
	public List<BookingLocation> findByBl4UniqueBookingUnitDeskIdentifier (String bl4UniqueBookingUnitDeskIdentifier);
	public List<BookingLocation> findByBl5UniqueBookingUnitDeskDescription (String bl5UniqueBookingUnitDeskDescription);
	public List<BookingLocation> findByBl6BookingUnitDeskContactPhone (String bl6BookingUnitDeskContactPhone);
	public List<BookingLocation> findByBl7BookingUnitDeskContactEmail (String bl7BookingUnitDeskContactEmail);
	

	
	public Page<BookingLocation> findAll (Pageable page);
	
	

	

	
	}







