package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CDH;
import com.cs.qfc.data.model.CDHDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class CDHToDTOConverter  {
	

	

public static CDHDTO convert(CDH cDH) {
 if (cDH != null)
  return CDHDTO.builder().id(cDH.getId())
.employeeId(cDH.getEmployeeId())
.ntLoginId(cDH.getNtLoginId())
.firstName(cDH.getFirstName())
.lastName(cDH.getLastName())
.country(cDH.getCountry())
.supervisorId(cDH.getSupervisorId())
.pid(cDH.getPid())
.gid(cDH.getGid())
.telephoneCountry(cDH.getTelephoneCountry())
.telephoneArea(cDH.getTelephoneArea())
.talephoneNumber(cDH.getTalephoneNumber())
.build();
 return null;
	}
    
}
