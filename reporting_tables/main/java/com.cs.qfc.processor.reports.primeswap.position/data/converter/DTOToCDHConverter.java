package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CDH;
import com.cs.qfc.data.model.CDHDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToCDHConverter  {
	

	

public static CDH convert(CDHDTO cDHDto) {
 if (cDHDto != null)
  return CDH.builder().id(cDHDto.getId())
.employeeId(cDHDto.getEmployeeId())
.ntLoginId(cDHDto.getNtLoginId())
.firstName(cDHDto.getFirstName())
.lastName(cDHDto.getLastName())
.country(cDHDto.getCountry())
.supervisorId(cDHDto.getSupervisorId())
.pid(cDHDto.getPid())
.gid(cDHDto.getGid())
.telephoneCountry(cDHDto.getTelephoneCountry())
.telephoneArea(cDHDto.getTelephoneArea())
.talephoneNumber(cDHDto.getTalephoneNumber())
 
.build();
 return null;
	}
    
}
