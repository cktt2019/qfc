package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CorpOrgMaster;
import com.cs.qfc.data.model.CorpOrgMasterDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class CorpOrgMasterToDTOConverter  {
	

	

public static CorpOrgMasterDTO convert(CorpOrgMaster corpOrgMaster) {
 if (corpOrgMaster != null)
  return CorpOrgMasterDTO.builder().id(corpOrgMaster.getId())
.co1AsOfDate(corpOrgMaster.getCo1AsOfDate())
.co2EntityIdentifier(corpOrgMaster.getCo2EntityIdentifier())
.co3HasLeiBeenUsed(corpOrgMaster.getCo3HasLeiBeenUsed())
.co4LegalNameOfEntity(corpOrgMaster.getCo4LegalNameOfEntity())
.co5ImmediateParentEntityIdentifier(corpOrgMaster.getCo5ImmediateParentEntityIdentifier())
.co6HasLeiBeenUsedForImmediateParent(corpOrgMaster.getCo6HasLeiBeenUsedForImmediateParent())
.co7LegalNameOfImmediateParent(corpOrgMaster.getCo7LegalNameOfImmediateParent())
.co8PercentageOwnershipOfImmediateParentInEntity(corpOrgMaster.getCo8PercentageOwnershipOfImmediateParentInEntity())
.co9EntityType(corpOrgMaster.getCo9EntityType())
.co10Domicile(corpOrgMaster.getCo10Domicile())
.co11JurisdictionUnderWhichIncorporated(corpOrgMaster.getCo11JurisdictionUnderWhichIncorporated())
.build();
 return null;
	}
    
}
