package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CorpOrgMaster;
import com.cs.qfc.data.model.CorpOrgMasterDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToCorpOrgMasterConverter  {
	

	

public static CorpOrgMaster convert(CorpOrgMasterDTO corpOrgMasterDto) {
 if (corpOrgMasterDto != null)
  return CorpOrgMaster.builder().id(corpOrgMasterDto.getId())
.co1AsOfDate(corpOrgMasterDto.getCo1AsOfDate())
.co2EntityIdentifier(corpOrgMasterDto.getCo2EntityIdentifier())
.co3HasLeiBeenUsed(corpOrgMasterDto.getCo3HasLeiBeenUsed())
.co4LegalNameOfEntity(corpOrgMasterDto.getCo4LegalNameOfEntity())
.co5ImmediateParentEntityIdentifier(corpOrgMasterDto.getCo5ImmediateParentEntityIdentifier())
.co6HasLeiBeenUsedForImmediateParent(corpOrgMasterDto.getCo6HasLeiBeenUsedForImmediateParent())
.co7LegalNameOfImmediateParent(corpOrgMasterDto.getCo7LegalNameOfImmediateParent())
.co8PercentageOwnershipOfImmediateParentInEntity(corpOrgMasterDto.getCo8PercentageOwnershipOfImmediateParentInEntity())
.co9EntityType(corpOrgMasterDto.getCo9EntityType())
.co10Domicile(corpOrgMasterDto.getCo10Domicile())
.co11JurisdictionUnderWhichIncorporated(corpOrgMasterDto.getCo11JurisdictionUnderWhichIncorporated())
 
.build();
 return null;
	}
    
}
