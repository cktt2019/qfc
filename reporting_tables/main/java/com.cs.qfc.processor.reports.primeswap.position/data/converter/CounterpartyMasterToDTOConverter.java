package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CounterpartyMaster;
import com.cs.qfc.data.model.CounterpartyMasterDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class CounterpartyMasterToDTOConverter  {
	

	

public static CounterpartyMasterDTO convert(CounterpartyMaster counterpartyMaster) {
 if (counterpartyMaster != null)
  return CounterpartyMasterDTO.builder().id(counterpartyMaster.getId())
.cp1AsOfDate(counterpartyMaster.getCp1AsOfDate())
.cp2CounterpartyIdentifier(counterpartyMaster.getCp2CounterpartyIdentifier())
.cp3HasLeiBeenUsedCpi(counterpartyMaster.getCp3HasLeiBeenUsedCpi())
.cp4LegalNameCounterparty(counterpartyMaster.getCp4LegalNameCounterparty())
.cp5Domicile(counterpartyMaster.getCp5Domicile())
.cp6JurisdictionUnderWhichIncorporated(counterpartyMaster.getCp6JurisdictionUnderWhichIncorporated())
.cp7ImmediateParentEntityIdentifier(counterpartyMaster.getCp7ImmediateParentEntityIdentifier())
.cp8HasLeiBeenUsedForParent(counterpartyMaster.getCp8HasLeiBeenUsedForParent())
.cp9LegalNameOfImmediateParentEntity(counterpartyMaster.getCp9LegalNameOfImmediateParentEntity())
.cp10UltimateParentEntityIdentifier(counterpartyMaster.getCp10UltimateParentEntityIdentifier())
.cp11HasLeiBeenUsedForUltimateParent(counterpartyMaster.getCp11HasLeiBeenUsedForUltimateParent())
.legalNameOfUltimateParent(counterpartyMaster.getLegalNameOfUltimateParent())
.build();
 return null;
	}
    
}
