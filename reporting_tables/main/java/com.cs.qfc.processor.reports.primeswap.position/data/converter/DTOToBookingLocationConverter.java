package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.BookingLocation;
import com.cs.qfc.data.model.BookingLocationDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToBookingLocationConverter  {
	

	

public static BookingLocation convert(BookingLocationDTO bookingLocationDto) {
 if (bookingLocationDto != null)
  return BookingLocation.builder().id(bookingLocationDto.getId())
.bl1AsOfDate(bookingLocationDto.getBl1AsOfDate())
.bl2RecordsEntityIdentifier(bookingLocationDto.getBl2RecordsEntityIdentifier())
.bl3InternalBookingLocationIdentifier(bookingLocationDto.getBl3InternalBookingLocationIdentifier())
.bl4UniqueBookingUnitDeskIdentifier(bookingLocationDto.getBl4UniqueBookingUnitDeskIdentifier())
.bl5UniqueBookingUnitDeskDescription(bookingLocationDto.getBl5UniqueBookingUnitDeskDescription())
.bl6BookingUnitDeskContactPhone(bookingLocationDto.getBl6BookingUnitDeskContactPhone())
.bl7BookingUnitDeskContactEmail(bookingLocationDto.getBl7BookingUnitDeskContactEmail())
 
.build();
 return null;
	}
    
}
