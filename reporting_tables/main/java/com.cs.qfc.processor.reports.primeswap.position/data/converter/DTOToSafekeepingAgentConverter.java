package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.SafekeepingAgent;
import com.cs.qfc.data.model.SafekeepingAgentDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToSafekeepingAgentConverter  {
	

	

public static SafekeepingAgent convert(SafekeepingAgentDTO safekeepingAgentDto) {
 if (safekeepingAgentDto != null)
  return SafekeepingAgent.builder().id(safekeepingAgentDto.getId())
.sa1AsOfDate(safekeepingAgentDto.getSa1AsOfDate())
.sa2SafekeepingAgentIdentifier(safekeepingAgentDto.getSa2SafekeepingAgentIdentifier())
.sa3LegalNaleOfSafekeepingAgent(safekeepingAgentDto.getSa3LegalNaleOfSafekeepingAgent())
.sa4PointOfContactName(safekeepingAgentDto.getSa4PointOfContactName())
.sa5PointOfContactAddress(safekeepingAgentDto.getSa5PointOfContactAddress())
.sa6PointOfContactPhone(safekeepingAgentDto.getSa6PointOfContactPhone())
.sa7PointOfContactEmail(safekeepingAgentDto.getSa7PointOfContactEmail())
 
.build();
 return null;
	}
    
}
