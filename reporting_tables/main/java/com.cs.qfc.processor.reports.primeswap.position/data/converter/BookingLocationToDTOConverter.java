package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.BookingLocation;
import com.cs.qfc.data.model.BookingLocationDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class BookingLocationToDTOConverter  {
	

	

public static BookingLocationDTO convert(BookingLocation bookingLocation) {
 if (bookingLocation != null)
  return BookingLocationDTO.builder().id(bookingLocation.getId())
.bl1AsOfDate(bookingLocation.getBl1AsOfDate())
.bl2RecordsEntityIdentifier(bookingLocation.getBl2RecordsEntityIdentifier())
.bl3InternalBookingLocationIdentifier(bookingLocation.getBl3InternalBookingLocationIdentifier())
.bl4UniqueBookingUnitDeskIdentifier(bookingLocation.getBl4UniqueBookingUnitDeskIdentifier())
.bl5UniqueBookingUnitDeskDescription(bookingLocation.getBl5UniqueBookingUnitDeskDescription())
.bl6BookingUnitDeskContactPhone(bookingLocation.getBl6BookingUnitDeskContactPhone())
.bl7BookingUnitDeskContactEmail(bookingLocation.getBl7BookingUnitDeskContactEmail())
.build();
 return null;
	}
    
}
