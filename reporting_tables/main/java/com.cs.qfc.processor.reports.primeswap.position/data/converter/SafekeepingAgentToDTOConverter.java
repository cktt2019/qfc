package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.SafekeepingAgent;
import com.cs.qfc.data.model.SafekeepingAgentDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class SafekeepingAgentToDTOConverter  {
	

	

public static SafekeepingAgentDTO convert(SafekeepingAgent safekeepingAgent) {
 if (safekeepingAgent != null)
  return SafekeepingAgentDTO.builder().id(safekeepingAgent.getId())
.sa1AsOfDate(safekeepingAgent.getSa1AsOfDate())
.sa2SafekeepingAgentIdentifier(safekeepingAgent.getSa2SafekeepingAgentIdentifier())
.sa3LegalNaleOfSafekeepingAgent(safekeepingAgent.getSa3LegalNaleOfSafekeepingAgent())
.sa4PointOfContactName(safekeepingAgent.getSa4PointOfContactName())
.sa5PointOfContactAddress(safekeepingAgent.getSa5PointOfContactAddress())
.sa6PointOfContactPhone(safekeepingAgent.getSa6PointOfContactPhone())
.sa7PointOfContactEmail(safekeepingAgent.getSa7PointOfContactEmail())
.build();
 return null;
	}
    
}
