package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CounterpartyMaster;
import com.cs.qfc.data.model.CounterpartyMasterDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToCounterpartyMasterConverter  {
	

	

public static CounterpartyMaster convert(CounterpartyMasterDTO counterpartyMasterDto) {
 if (counterpartyMasterDto != null)
  return CounterpartyMaster.builder().id(counterpartyMasterDto.getId())
.cp1AsOfDate(counterpartyMasterDto.getCp1AsOfDate())
.cp2CounterpartyIdentifier(counterpartyMasterDto.getCp2CounterpartyIdentifier())
.cp3HasLeiBeenUsedCpi(counterpartyMasterDto.getCp3HasLeiBeenUsedCpi())
.cp4LegalNameCounterparty(counterpartyMasterDto.getCp4LegalNameCounterparty())
.cp5Domicile(counterpartyMasterDto.getCp5Domicile())
.cp6JurisdictionUnderWhichIncorporated(counterpartyMasterDto.getCp6JurisdictionUnderWhichIncorporated())
.cp7ImmediateParentEntityIdentifier(counterpartyMasterDto.getCp7ImmediateParentEntityIdentifier())
.cp8HasLeiBeenUsedForParent(counterpartyMasterDto.getCp8HasLeiBeenUsedForParent())
.cp9LegalNameOfImmediateParentEntity(counterpartyMasterDto.getCp9LegalNameOfImmediateParentEntity())
.cp10UltimateParentEntityIdentifier(counterpartyMasterDto.getCp10UltimateParentEntityIdentifier())
.cp11HasLeiBeenUsedForUltimateParent(counterpartyMasterDto.getCp11HasLeiBeenUsedForUltimateParent())
.legalNameOfUltimateParent(counterpartyMasterDto.getLegalNameOfUltimateParent())
 
.build();
 return null;
	}
    
}
