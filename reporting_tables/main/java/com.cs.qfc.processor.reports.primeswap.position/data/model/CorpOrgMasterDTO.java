package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class CorpOrgMasterDTO  {
   
	    private String id ;
   
	    private String co1AsOfDate ;
   
	    private String co2EntityIdentifier ;
   
	    private String co3HasLeiBeenUsed ;
   
	    private String co4LegalNameOfEntity ;
   
	    private String co5ImmediateParentEntityIdentifier ;
   
	    private String co6HasLeiBeenUsedForImmediateParent ;
   
	    private String co7LegalNameOfImmediateParent ;
   
	    private String co8PercentageOwnershipOfImmediateParentInEntity ;
   
	    private String co9EntityType ;
   
	    private String co10Domicile ;
   
	    private String co11JurisdictionUnderWhichIncorporated ;
    


}
