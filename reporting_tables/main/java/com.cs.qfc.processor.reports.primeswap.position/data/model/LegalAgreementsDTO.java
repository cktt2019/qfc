package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class LegalAgreementsDTO  {
   
	    private Integer id ;
   
	    private String sourceTxnId ;
   
	    private String a31AsOfDate ;
   
	    private String a32RecordsEntityIdentifier ;
   
	    private String a33AgreementIdentifier ;
   
	    private String a34NameOfAgreementGoverningDoc ;
   
	    private String a35AgreementDate ;
   
	    private String a36AgreementCounterpartyIdentifier ;
   
	    private String a361UnderlyingQfcObligatorIdentifier ;
   
	    private String a37AgreementGoverningLaw ;
   
	    private String a38CrossDefaultProvision ;
   
	    private String a39IdentityOfCrossDefaultEntities ;
   
	    private String a310CoveredByThirdPartyCreditEnhancement ;
   
	    private String a311ThirdPartyCreditEnhancementProviderIdentifier ;
   
	    private String a312AssociatedCreditEnhancementDocIdentifier ;
   
	    private String a3121CounterpartyThirdPartyCreditEnhancement ;
   
	    private String a3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier ;
   
	    private String a3123CounterpartyAssociatedCreditEnhancementDocIdentifier ;
   
	    private String a313CounterpartyContactInfoName ;
   
	    private String a314CounterpartyContactInfoAddress ;
   
	    private String a315CounterpartyContactInfoPhone ;
   
	    private String a316CounterpartyContactInfoEmail ;
    


}
