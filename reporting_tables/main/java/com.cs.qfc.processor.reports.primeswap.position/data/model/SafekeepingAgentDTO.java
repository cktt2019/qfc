package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class SafekeepingAgentDTO  {
   
	    private String id ;
   
	    private String sa1AsOfDate ;
   
	    private String sa2SafekeepingAgentIdentifier ;
   
	    private String sa3LegalNaleOfSafekeepingAgent ;
   
	    private String sa4PointOfContactName ;
   
	    private String sa5PointOfContactAddress ;
   
	    private String sa6PointOfContactPhone ;
   
	    private String sa7PointOfContactEmail ;
    


}
