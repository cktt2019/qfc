package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class BookingLocationDTO  {
   
	    private String id ;
   
	    private String bl1AsOfDate ;
   
	    private String bl2RecordsEntityIdentifier ;
   
	    private String bl3InternalBookingLocationIdentifier ;
   
	    private String bl4UniqueBookingUnitDeskIdentifier ;
   
	    private String bl5UniqueBookingUnitDeskDescription ;
   
	    private String bl6BookingUnitDeskContactPhone ;
   
	    private String bl7BookingUnitDeskContactEmail ;
    


}
