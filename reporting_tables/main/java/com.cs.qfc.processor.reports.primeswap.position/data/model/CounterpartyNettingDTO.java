package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class CounterpartyNettingDTO  {
   
	    private Integer id ;
   
	    private String sourceTxnId ;
   
	    private String a21AsOfDate ;
   
	    private String a22RecordsEntityIdentifier ;
   
	    private String a23NettingAgreementCounterpartyIdentifier ;
   
	    private String a24NettingAgreementIdentifier ;
   
	    private String a241UnderlyingQfcObligatorIdentifier ;
   
	    private String a25CoveredByThirdPartyCreditEnhancement ;
   
	    private String a251ThirdPartyCreditEnhancementProviderIdentifier ;
   
	    private String a252ThirdPartyCreditEnhancementAgreementIdentifier ;
   
	    private String a253CounterpartyThirdPartyCreditEnhancement ;
   
	    private String a254CounterpartyThirdPartyCreditEnhancementProviderIdentifier ;
   
	    private String a255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier ;
   
	    private String a26AggregateCurrMarketValueAllPositionsUsd ;
   
	    private String a27AggregateCurrMarketValuePositivePositionsUsd ;
   
	    private String a28AggregateCurrMarketValueNegativePositionsUsd ;
   
	    private String a29CurrMarketValueAllCollateralRecordsEntityInUsd ;
   
	    private String a210CurrMarketValueAllCollateralCounterpartyInUsd ;
   
	    private String a211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd ;
   
	    private String a212CurrMarketValueAllCollateralCounterpartyRehypoInUsd ;
   
	    private String a213RecordsEntityCollateralNet ;
   
	    private String a214CounterpartyCollateralNet ;
   
	    private String a215NextMarginPaymentDate ;
   
	    private String a216NextMarginPaymentAmountUsd ;
   
	    private String a217SafekeepingAgentIdentifierRecordsEntity ;
   
	    private String a218SafekeepingAgentIdentifierCounterparty ;
    


}
