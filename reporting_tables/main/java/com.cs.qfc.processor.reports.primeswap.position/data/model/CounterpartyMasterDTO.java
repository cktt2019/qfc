package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class CounterpartyMasterDTO  {
   
	    private String id ;
   
	    private String cp1AsOfDate ;
   
	    private String cp2CounterpartyIdentifier ;
   
	    private String cp3HasLeiBeenUsedCpi ;
   
	    private String cp4LegalNameCounterparty ;
   
	    private String cp5Domicile ;
   
	    private String cp6JurisdictionUnderWhichIncorporated ;
   
	    private String cp7ImmediateParentEntityIdentifier ;
   
	    private String cp8HasLeiBeenUsedForParent ;
   
	    private String cp9LegalNameOfImmediateParentEntity ;
   
	    private String cp10UltimateParentEntityIdentifier ;
   
	    private String cp11HasLeiBeenUsedForUltimateParent ;
   
	    private String legalNameOfUltimateParent ;
    


}
