package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class CDHDTO  {
   
	    private Integer id ;
   
	    private String employeeId ;
   
	    private String ntLoginId ;
   
	    private String firstName ;
   
	    private String lastName ;
   
	    private String country ;
   
	    private String supervisorId ;
   
	    private String pid ;
   
	    private String gid ;
   
	    private String telephoneCountry ;
   
	    private String telephoneArea ;
   
	    private String talephoneNumber ;
    


}
