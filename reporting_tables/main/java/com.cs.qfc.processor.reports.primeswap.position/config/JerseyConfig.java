package com.cs.qfc.config;

import com.cs.qfc.rest.api.endpoints.RestEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(RestEndpoint.class);
		register(CorsFilter.class);
	}

}
