package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class CollateralDetailDataService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(CollateralDetailDataService.class);
 
    @Autowired
	CollateralDetailDataRepository collateralDetailDataRepository;

	@Autowired
    CustomizedCollateralDetailDataRepository customizedCollateralDetailDataRepository;

	
	@Transactional
	public CollateralDetailData saveCollateralDetailData (CollateralDetailDataDTO collateralDetailDataDTO){
	 CollateralDetailData collateralDetailData  = DTOToCollateralDetailDataConverter.convert(collateralDetailDataDTO);
	 return collateralDetailDataRepository.save(collateralDetailData);
	}
	
	@Transactional
	public CollateralDetailData save (CollateralDetailData collateralDetailData){
	 return collateralDetailDataRepository.save(collateralDetailData);
	}

       
       
       
  
    @Transactional
   public Collection<CollateralDetailData> findAll (){
	   return collateralDetailDataRepository.findAll();
	}

	@Transactional
    public Collection<CollateralDetailDataDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedCollateralDetailDataRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public CollateralDetailData findById (Integer id){
	   Optional<CollateralDetailData> collateralDetailData  = collateralDetailDataRepository.findById(id);
	  return (collateralDetailData.isPresent() ? collateralDetailData.get() : null);
	}
	
   @Transactional
   public CollateralDetailData updateCollateralDetailData (CollateralDetailData collateralDetailData,CollateralDetailDataDTO collateralDetailDataDto){
	
	collateralDetailData.setSourceTxnId(collateralDetailDataDto.getSourceTxnId());
	collateralDetailData.setA41AsOfDate(collateralDetailDataDto.getA41AsOfDate());
	collateralDetailData.setA42RecordsEntityIdentifier(collateralDetailDataDto.getA42RecordsEntityIdentifier());
	collateralDetailData.setA43collateraoPostedReceivedFlag(collateralDetailDataDto.getA43collateraoPostedReceivedFlag());
	collateralDetailData.setA44CounterpartyIdentifier(collateralDetailDataDto.getA44CounterpartyIdentifier());
	collateralDetailData.setA45NettingAgreementIdentifier(collateralDetailDataDto.getA45NettingAgreementIdentifier());
	collateralDetailData.setA46UniqueCollateralItemIdentifier(collateralDetailDataDto.getA46UniqueCollateralItemIdentifier());
	collateralDetailData.setA47OriginalFaceAmtCollateralItemLocalCurr(collateralDetailDataDto.getA47OriginalFaceAmtCollateralItemLocalCurr());
	collateralDetailData.setA48LocalCurrencyOfCollateralItem(collateralDetailDataDto.getA48LocalCurrencyOfCollateralItem());
	collateralDetailData.setA49MarketValueAmountOfCollateralItemInUsd(collateralDetailDataDto.getA49MarketValueAmountOfCollateralItemInUsd());
	collateralDetailData.setA410DescriptionOfCollateralItem(collateralDetailDataDto.getA410DescriptionOfCollateralItem());
	collateralDetailData.setA411AssetClassification(collateralDetailDataDto.getA411AssetClassification());
	collateralDetailData.setA412CollateralPortfolioSegregationStatus(collateralDetailDataDto.getA412CollateralPortfolioSegregationStatus());
	collateralDetailData.setA413CollateralLocation(collateralDetailDataDto.getA413CollateralLocation());
	collateralDetailData.setA414CollateralJurisdiction(collateralDetailDataDto.getA414CollateralJurisdiction());
	collateralDetailData.setA415CollateralRehypoAllowed(collateralDetailDataDto.getA415CollateralRehypoAllowed());
	 return collateralDetailDataRepository.save(collateralDetailData);
	}
	
   @Transactional
   public void deleteById (Integer id){
	   collateralDetailDataRepository.deleteById(id);

	}
	
   
   

}
