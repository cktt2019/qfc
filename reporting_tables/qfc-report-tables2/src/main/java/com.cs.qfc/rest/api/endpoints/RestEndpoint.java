package com.cs.qfc.rest.api.endpoints;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *  Rest enpoint for data retrieval and modifications
 *
 *
 * @author ctanyitang
 */
 	@Controller
    @Path("/api")
    @Transactional
	public class RestEndpoint {
	
     @Autowired
     PositionLevelDataService positionLevelDataService;
     
     @Autowired
     CounterpartyNettingService counterpartyNettingService;
     
     @Autowired
     LegalAgreementsService legalAgreementsService;
     
     @Autowired
     CollateralDetailDataService collateralDetailDataService;
     



    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/collateraldetaildatas/{id}")
	public CollateralDetailDataDTO getCollateralDetailDataById (@PathParam("id") Integer  id    ) {

		CollateralDetailData collateralDetailData =	collateralDetailDataService.findById(id    );
		if(collateralDetailData != null ) 
		   return CollateralDetailDataToDTOConverter.convert(collateralDetailData);
		throw new NotFoundException("No CollateralDetailData with  " +" id = " + id    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/collateraldetaildatas/{id}")
	public Response updateCollateralDetailDataById (@RequestBody CollateralDetailDataDTO collateralDetailData ) {
	    CollateralDetailData existingCollateralDetailData =	collateralDetailDataService.findById(collateralDetailData.getId()    );
		if(existingCollateralDetailData == null ) 
		throw new NotFoundException("No CollateralDetailData with  " +" collateralDetailData.getId() = " + collateralDetailData.getId()    + " exists");
		
	    collateralDetailDataService.updateCollateralDetailData(existingCollateralDetailData,collateralDetailData);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/collateraldetaildatas/{id}")
	public Response deleteCollateralDetailDataById (@PathParam("id") Integer  id    ) 
	  {
	        CollateralDetailData existingCollateralDetailData =	collateralDetailDataService.findById(id    );
		if(existingCollateralDetailData == null ) 
		throw new NotFoundException("No CollateralDetailData with  " +" id = " + id    + " exists");
	
		collateralDetailDataService.deleteById(id    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/collateraldetaildatas")
	public List<CollateralDetailDataDTO> listCollateraldetaildatas () {

		return collateralDetailDataService.findAll().stream().map(e -> CollateralDetailDataToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/collateraldetaildatas/custom")
    	public List<CollateralDetailDataDTO> createCollateralDetailData ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(collateralDetailDataService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/collateraldetaildatas")
	public Response createCollateralDetailData ( @RequestBody CollateralDetailDataDTO collateralDetailData ) {
		CollateralDetailData createdCollateralDetailData = collateralDetailDataService.saveCollateralDetailData(collateralDetailData);
		return Response.status(Response.Status.CREATED).header("Location","/collateraldetaildatas/"+createdCollateralDetailData.getId()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/counterpartynettings")
	public List<CounterpartyNettingDTO> listCounterpartynettings () {

		return counterpartyNettingService.findAll().stream().map(e -> CounterpartyNettingToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/counterpartynettings/custom")
    	public List<CounterpartyNettingDTO> createCounterpartyNetting ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(counterpartyNettingService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/counterpartynettings")
	public Response createCounterpartyNetting ( @RequestBody CounterpartyNettingDTO counterpartyNetting ) {
		CounterpartyNetting createdCounterpartyNetting = counterpartyNettingService.saveCounterpartyNetting(counterpartyNetting);
		return Response.status(Response.Status.CREATED).header("Location","/counterpartynettings/"+createdCounterpartyNetting.getId()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/positionleveldatas")
	public List<PositionLevelDataDTO> listPositionleveldatas () {

		return positionLevelDataService.findAll().stream().map(e -> PositionLevelDataToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/positionleveldatas/custom")
    	public List<PositionLevelDataDTO> createPositionLevelData ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(positionLevelDataService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/positionleveldatas")
	public Response createPositionLevelData ( @RequestBody PositionLevelDataDTO positionLevelData ) {
		PositionLevelData createdPositionLevelData = positionLevelDataService.savePositionLevelData(positionLevelData);
		return Response.status(Response.Status.CREATED).header("Location","/positionleveldatas/"+createdPositionLevelData.getId()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/counterpartynettings/{id}")
	public CounterpartyNettingDTO getCounterpartyNettingById (@PathParam("id") Integer  id    ) {

		CounterpartyNetting counterpartyNetting =	counterpartyNettingService.findById(id    );
		if(counterpartyNetting != null ) 
		   return CounterpartyNettingToDTOConverter.convert(counterpartyNetting);
		throw new NotFoundException("No CounterpartyNetting with  " +" id = " + id    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/counterpartynettings/{id}")
	public Response updateCounterpartyNettingById (@RequestBody CounterpartyNettingDTO counterpartyNetting ) {
	    CounterpartyNetting existingCounterpartyNetting =	counterpartyNettingService.findById(counterpartyNetting.getId()    );
		if(existingCounterpartyNetting == null ) 
		throw new NotFoundException("No CounterpartyNetting with  " +" counterpartyNetting.getId() = " + counterpartyNetting.getId()    + " exists");
		
	    counterpartyNettingService.updateCounterpartyNetting(existingCounterpartyNetting,counterpartyNetting);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/counterpartynettings/{id}")
	public Response deleteCounterpartyNettingById (@PathParam("id") Integer  id    ) 
	  {
	        CounterpartyNetting existingCounterpartyNetting =	counterpartyNettingService.findById(id    );
		if(existingCounterpartyNetting == null ) 
		throw new NotFoundException("No CounterpartyNetting with  " +" id = " + id    + " exists");
	
		counterpartyNettingService.deleteById(id    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/legalagreements")
	public List<LegalAgreementsDTO> listLegalagreements () {

		return legalAgreementsService.findAll().stream().map(e -> LegalAgreementsToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/legalagreements/custom")
    	public List<LegalAgreementsDTO> createLegalAgreements ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(legalAgreementsService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/legalagreements")
	public Response createLegalAgreements ( @RequestBody LegalAgreementsDTO legalAgreements ) {
		LegalAgreements createdLegalAgreements = legalAgreementsService.saveLegalAgreements(legalAgreements);
		return Response.status(Response.Status.CREATED).header("Location","/legalagreements/"+createdLegalAgreements.getId()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/legalagreements/{id}")
	public LegalAgreementsDTO getLegalAgreementsById (@PathParam("id") Integer  id    ) {

		LegalAgreements legalAgreements =	legalAgreementsService.findById(id    );
		if(legalAgreements != null ) 
		   return LegalAgreementsToDTOConverter.convert(legalAgreements);
		throw new NotFoundException("No LegalAgreements with  " +" id = " + id    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/legalagreements/{id}")
	public Response updateLegalAgreementsById (@RequestBody LegalAgreementsDTO legalAgreements ) {
	    LegalAgreements existingLegalAgreements =	legalAgreementsService.findById(legalAgreements.getId()    );
		if(existingLegalAgreements == null ) 
		throw new NotFoundException("No LegalAgreements with  " +" legalAgreements.getId() = " + legalAgreements.getId()    + " exists");
		
	    legalAgreementsService.updateLegalAgreements(existingLegalAgreements,legalAgreements);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/legalagreements/{id}")
	public Response deleteLegalAgreementsById (@PathParam("id") Integer  id    ) 
	  {
	        LegalAgreements existingLegalAgreements =	legalAgreementsService.findById(id    );
		if(existingLegalAgreements == null ) 
		throw new NotFoundException("No LegalAgreements with  " +" id = " + id    + " exists");
	
		legalAgreementsService.deleteById(id    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/positionleveldatas/{id}")
	public PositionLevelDataDTO getPositionLevelDataById (@PathParam("id") String  id    ) {

		PositionLevelData positionLevelData =	positionLevelDataService.findById(id    );
		if(positionLevelData != null ) 
		   return PositionLevelDataToDTOConverter.convert(positionLevelData);
		throw new NotFoundException("No PositionLevelData with  " +" id = " + id    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/positionleveldatas/{id}")
	public Response updatePositionLevelDataById (@RequestBody PositionLevelDataDTO positionLevelData ) {
	    PositionLevelData existingPositionLevelData =	positionLevelDataService.findById(positionLevelData.getId()    );
		if(existingPositionLevelData == null ) 
		throw new NotFoundException("No PositionLevelData with  " +" positionLevelData.getId() = " + positionLevelData.getId()    + " exists");
		
	    positionLevelDataService.updatePositionLevelData(existingPositionLevelData,positionLevelData);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/positionleveldatas/{id}")
	public Response deletePositionLevelDataById (@PathParam("id") String  id    ) 
	  {
	        PositionLevelData existingPositionLevelData =	positionLevelDataService.findById(id    );
		if(existingPositionLevelData == null ) 
		throw new NotFoundException("No PositionLevelData with  " +" id = " + id    + " exists");
	
		positionLevelDataService.deleteById(id    );

		return Response.status(Response.Status.OK).build();
		
	}


        //Extra method required for custom search (Remove if not needed)
	    @GET
        @Produces(MediaType.APPLICATION_JSON)
        @Path("/customquerieslist/{entityName}")
        public List<String> listCustomqueriesForEntity (@PathParam("entityName") String  entityName) {

            List<CustomQueries> filtered = customQueriesService.findAll().stream().filter(x -> x.getEntityName().equals(entityName)).collect(Collectors.toList());
            return
                    filtered.stream().map(x->x.getCriteriaName()).collect(Collectors.toList());

        }
	}







