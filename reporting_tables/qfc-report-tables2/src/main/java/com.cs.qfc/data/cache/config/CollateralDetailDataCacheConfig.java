package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.CollateralDetailData;
import org.apache.ignite.cache.*;
import org.apache.ignite.configuration.CacheConfiguration;









    @Component
	public class CollateralDetailDataCacheConfig implements Serializable {

    @Autowired
    OracleDataSource oracleDataSource;

    private class CollateralDetailDataCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, CollateralDetailData> implements Serializable {
       @Override
       public CacheJdbcPojoStore<Integer, CollateralDetailData> create() {
        setDataSource(oracleDataSource);
       }
    }

    public CacheConfiguration<Integer, CollateralDetailData> cacheConfiguration (){
     CacheConfiguration<Integer, CollateralDetailData> cacheCfg = new CacheConfiguration<Integer, CollateralDetailData> ();

     cacheCfg.setName("CollateralDetailDataCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     CollateralDetailDataCacheJdbcPojoStoreFactory factory = new CacheJdbcPojoStoreFactory ();

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("CollateralDetailDataCache");
     jdbcType.setDatabaseSchema("SIERRA_QFC");
     jdbcType.setDatabaseTable("CollateralDetailData");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(CollateralDetailData.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"),
   	  new JdbcTypeField(Types.VARCHAR,"source_txn_id",String.class,"sourceTxnId"),
   	  new JdbcTypeField(Types.VARCHAR,"a41_as_of_date",String.class,"a41AsOfDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a42_records_entity_identifier",String.class,"a42RecordsEntityIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a43collaterao_posted_received_flag",String.class,"a43collateraoPostedReceivedFlag"),
   	  new JdbcTypeField(Types.VARCHAR,"a44_counterparty_identifier",String.class,"a44CounterpartyIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a45_netting_agreement_identifier",String.class,"a45NettingAgreementIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a46_unique_collateral_item_identifier",String.class,"a46UniqueCollateralItemIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a47_original_face_amt_collateral_item_local_curr",String.class,"a47OriginalFaceAmtCollateralItemLocalCurr"),
   	  new JdbcTypeField(Types.VARCHAR,"a48_local_currency_of_collateral_item",String.class,"a48LocalCurrencyOfCollateralItem"),
   	  new JdbcTypeField(Types.VARCHAR,"a49_market_value_amount_of_collateral_item_in_usd",String.class,"a49MarketValueAmountOfCollateralItemInUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a410_description_of_collateral_item",String.class,"a410DescriptionOfCollateralItem"),
   	  new JdbcTypeField(Types.VARCHAR,"a411_asset_classification",String.class,"a411AssetClassification"),
   	  new JdbcTypeField(Types.VARCHAR,"a412_collateral_portfolio_segregation_status",String.class,"a412CollateralPortfolioSegregationStatus"),
   	  new JdbcTypeField(Types.VARCHAR,"a413_collateral_location",String.class,"a413CollateralLocation"),
   	  new JdbcTypeField(Types.VARCHAR,"a414_collateral_jurisdiction",String.class,"a414CollateralJurisdiction"),
   	  new JdbcTypeField(Types.VARCHAR,"a415_collateral_rehypo_allowed",String.class,"a415CollateralRehypoAllowed")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

}
