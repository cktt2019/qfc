package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.PositionLevelData;
import org.apache.ignite.cache.*;
import org.apache.ignite.configuration.CacheConfiguration;









    @Component
	public class PositionLevelDataCacheConfig implements Serializable {

    @Autowired
    OracleDataSource oracleDataSource;

    private class PositionLevelDataCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<String, PositionLevelData> implements Serializable {
       @Override
       public CacheJdbcPojoStore<String, PositionLevelData> create() {
        setDataSource(oracleDataSource);
       }
    }

    public CacheConfiguration<String, PositionLevelData> cacheConfiguration (){
     CacheConfiguration<String, PositionLevelData> cacheCfg = new CacheConfiguration<String, PositionLevelData> ();

     cacheCfg.setName("PositionLevelDataCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     PositionLevelDataCacheJdbcPojoStoreFactory factory = new CacheJdbcPojoStoreFactory ();

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("PositionLevelDataCache");
     jdbcType.setDatabaseTable("PositionLevelData");
     jdbcType.setKeyType(String.class);
     jdbcType.setValueType(PositionLevelData.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.VARCHAR,"id",String.class,"id"),
   	  new JdbcTypeField(Types.DATE,"a11_as_of_date",java.sql.Date.class,"a11AsOfDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a12_records_entity_identifier",String.class,"a12RecordsEntityIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a13_positions_identifier",String.class,"a13PositionsIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a15_internal_booking_location_identifier",String.class,"a15InternalBookingLocationIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a16_unique_booking_unit_desk_identifier",String.class,"a16UniqueBookingUnitDeskIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a17_type_of_qfc",String.class,"a17TypeOfQfc"),
   	  new JdbcTypeField(Types.VARCHAR,"a_171_type_of_qfc_covered_guarantee",String.class,"a171TypeOfQfcCoveredGuarantee"),
   	  new JdbcTypeField(Types.VARCHAR,"a172_underlying_qfc_obligator_identifier",String.class,"a172UnderlyingQfcObligatorIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a18_agreement_identifier",String.class,"a18AgreementIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a19_netting_agreement_idrntifier",String.class,"a19NettingAgreementIdrntifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a110_netting_agreement_counterparty_identifier",String.class,"a110NettingAgreementCounterpartyIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a111_trade_date",String.class,"a111TradeDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a112_termination_date",String.class,"a112TerminationDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a113_next_put_call_cancellation_date",String.class,"a113NextPutCallCancellationDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a114_next_payment_date",String.class,"a114NextPaymentDate"),
   	  new JdbcTypeField(Types.VARCHAR,"a115_local_currency_of_position",String.class,"a115LocalCurrencyOfPosition"),
   	  new JdbcTypeField(Types.VARCHAR,"a116_current_market_value_in_local_currency",String.class,"a116CurrentMarketValueInLocalCurrency"),
   	  new JdbcTypeField(Types.VARCHAR,"a117_current_market_value_in_usd",String.class,"a117CurrentMarketValueInUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a118_asset_classification",String.class,"a118AssetClassification"),
   	  new JdbcTypeField(Types.VARCHAR,"a119_notional_or_principal_amount_local_currency",String.class,"a119NotionalPrincipalAmountLocalCurrency"),
   	  new JdbcTypeField(Types.VARCHAR,"a120_notional_or_principal_amount_usd",String.class,"a120NotionalPrincipalAmountUsd"),
   	  new JdbcTypeField(Types.VARCHAR,"a121_covered_by_third_party_credit_enhancement",String.class,"a121CoveredByThirdPartyCreditEnhancement"),
   	  new JdbcTypeField(Types.VARCHAR,"a1211_third_party_credit_enhancement_provider_identifier",String.class,"a1211ThirdPartyCreditEnhancementProviderIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a1212_third_party_credit_enhancement_agreement_identifier",String.class,"a1212ThirdPartyCreditEnhancementAgreementIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a213_counterparty_third_party_credit_enhancement",String.class,"a213CounterpartyThirdPartyCreditEnhancement"),
   	  new JdbcTypeField(Types.VARCHAR,"a1214_counterparty_third_party_credit_enhancement_provider_identifier",String.class,"a1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a1215_counterparty_third_party_credit_enhancement_agreement_identifier",String.class,"a1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier"),
   	  new JdbcTypeField(Types.VARCHAR,"a122_related_position_of_records_entity",String.class,"a122RelatedPositionOfRecordsEntity"),
   	  new JdbcTypeField(Types.VARCHAR,"a123_reference_number_for_any_related_loan",String.class,"a123ReferenceNumberForAnyRelatedLoan"),
   	  new JdbcTypeField(Types.VARCHAR,"a124_identifier_of_the_lender_of_related_loan",String.class,"a124IdentifierOfTheLenderOfRelatedLoan"),
   	  new JdbcTypeField(Types.VARCHAR,"source_txn_id",String.class,"sourceTxnId"),
   	  new JdbcTypeField(Types.VARCHAR,"a14_counterparty_identifier",String.class,"a14CounterpartyIdentifier")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

}
