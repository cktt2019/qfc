package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class CounterpartyNettingMetaData  implements MetaData {
	
	private static final String ID = "id";
	private static final String SOURCETXNID = "sourceTxnId";
	private static final String A21ASOFDATE = "a21AsOfDate";
	private static final String A22RECORDSENTITYIDENTIFIER = "a22RecordsEntityIdentifier";
	private static final String A23NETTINGAGREEMENTCOUNTERPARTYIDENTIFIER = "a23NettingAgreementCounterpartyIdentifier";
	private static final String A24NETTINGAGREEMENTIDENTIFIER = "a24NettingAgreementIdentifier";
	private static final String A241UNDERLYINGQFCOBLIGATORIDENTIFIER = "a241UnderlyingQfcObligatorIdentifier";
	private static final String A25COVEREDBYTHIRDPARTYCREDITENHANCEMENT = "a25CoveredByThirdPartyCreditEnhancement";
	private static final String A251THIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER = "a251ThirdPartyCreditEnhancementProviderIdentifier";
	private static final String A252THIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER = "a252ThirdPartyCreditEnhancementAgreementIdentifier";
	private static final String A253COUNTERPARTYTHIRDPARTYCREDITENHANCEMENT = "a253CounterpartyThirdPartyCreditEnhancement";
	private static final String A254COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER = "a254CounterpartyThirdPartyCreditEnhancementProviderIdentifier";
	private static final String A255COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER = "a255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier";
	private static final String A26AGGREGATECURRMARKETVALUEALLPOSITIONSUSD = "a26AggregateCurrMarketValueAllPositionsUsd";
	private static final String A27AGGREGATECURRMARKETVALUEPOSITIVEPOSITIONSUSD = "a27AggregateCurrMarketValuePositivePositionsUsd";
	private static final String A28AGGREGATECURRMARKETVALUENEGATIVEPOSITIONSUSD = "a28AggregateCurrMarketValueNegativePositionsUsd";
	private static final String A29CURRMARKETVALUEALLCOLLATERALRECORDSENTITYINUSD = "a29CurrMarketValueAllCollateralRecordsEntityInUsd";
	private static final String A210CURRMARKETVALUEALLCOLLATERALCOUNTERPARTYINUSD = "a210CurrMarketValueAllCollateralCounterpartyInUsd";
	private static final String A211CURRMARKETVALUEALLCOLLATERALRECORDSENTITYREHYPOINUSD = "a211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd";
	private static final String A212CURRMARKETVALUEALLCOLLATERALCOUNTERPARTYREHYPOINUSD = "a212CurrMarketValueAllCollateralCounterpartyRehypoInUsd";
	private static final String A213RECORDSENTITYCOLLATERALNET = "a213RecordsEntityCollateralNet";
	private static final String A214COUNTERPARTYCOLLATERALNET = "a214CounterpartyCollateralNet";
	private static final String A215NEXTMARGINPAYMENTDATE = "a215NextMarginPaymentDate";
	private static final String A216NEXTMARGINPAYMENTAMOUNTUSD = "a216NextMarginPaymentAmountUsd";
	private static final String A217SAFEKEEPINGAGENTIDENTIFIERRECORDSENTITY = "a217SafekeepingAgentIdentifierRecordsEntity";
	private static final String A218SAFEKEEPINGAGENTIDENTIFIERCOUNTERPARTY = "a218SafekeepingAgentIdentifierCounterparty";

   
    public static final String TABLE = "SIERRA_QFC.CounterpartyNetting";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case ID : return "id";
	case SOURCETXNID : return "source_txn_id";
	case A21ASOFDATE : return "a21_as_of_date";
	case A22RECORDSENTITYIDENTIFIER : return "a22_records_entity_identifier";
	case A23NETTINGAGREEMENTCOUNTERPARTYIDENTIFIER : return "a23_netting_agreement_counterparty_identifier";
	case A24NETTINGAGREEMENTIDENTIFIER : return "a24_netting_agreement_identifier";
	case A241UNDERLYINGQFCOBLIGATORIDENTIFIER : return "a241_underlying_qfc_obligator_identifier";
	case A25COVEREDBYTHIRDPARTYCREDITENHANCEMENT : return "a25_covered_by_third_party_credit_enhancement";
	case A251THIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER : return "a251_third_party_credit_enhancement_provider_identifier";
	case A252THIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER : return "a252_third_party_credit_enhancement_agreement_identifier";
	case A253COUNTERPARTYTHIRDPARTYCREDITENHANCEMENT : return "a253_counterparty_third_party_credit_enhancement";
	case A254COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER : return "a254_counterparty_third_party_credit_enhancement_provider_identifier";
	case A255COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER : return "a255_counterparty_third_party_credit_enhancement_agreement_identifier";
	case A26AGGREGATECURRMARKETVALUEALLPOSITIONSUSD : return "a26_aggregate_curr_market_value_all_positions_usd";
	case A27AGGREGATECURRMARKETVALUEPOSITIVEPOSITIONSUSD : return "a27_aggregate_curr_market_value_positive_positions_usd";
	case A28AGGREGATECURRMARKETVALUENEGATIVEPOSITIONSUSD : return "a28_aggregate_curr_market_value_negative_positions_usd";
	case A29CURRMARKETVALUEALLCOLLATERALRECORDSENTITYINUSD : return "a29_curr_market_value_all_collateral_records_entity_in_usd";
	case A210CURRMARKETVALUEALLCOLLATERALCOUNTERPARTYINUSD : return "a210_curr_market_value_all_collateral_counterparty_in_usd";
	case A211CURRMARKETVALUEALLCOLLATERALRECORDSENTITYREHYPOINUSD : return "a211_curr_market_value_all_collateral_records_entity_rehypo_in_usd";
	case A212CURRMARKETVALUEALLCOLLATERALCOUNTERPARTYREHYPOINUSD : return "a212_curr_market_value_all_collateral_counterparty_rehypo_in_usd";
	case A213RECORDSENTITYCOLLATERALNET : return "a213_records_entity_collateral_net";
	case A214COUNTERPARTYCOLLATERALNET : return "a214_counterparty_collateral_net";
	case A215NEXTMARGINPAYMENTDATE : return "a215_next_margin_payment_date";
	case A216NEXTMARGINPAYMENTAMOUNTUSD : return "a216_next_margin_payment_amount_usd";
	case A217SAFEKEEPINGAGENTIDENTIFIERRECORDSENTITY : return "a217_safekeeping_agent_identifier_records_entity";
	case A218SAFEKEEPINGAGENTIDENTIFIERCOUNTERPARTY : return "a218_safekeeping_agent_identifier_counterparty";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case ID : return "Integer";
	case SOURCETXNID : return "String";
	case A21ASOFDATE : return "String";
	case A22RECORDSENTITYIDENTIFIER : return "String";
	case A23NETTINGAGREEMENTCOUNTERPARTYIDENTIFIER : return "String";
	case A24NETTINGAGREEMENTIDENTIFIER : return "String";
	case A241UNDERLYINGQFCOBLIGATORIDENTIFIER : return "String";
	case A25COVEREDBYTHIRDPARTYCREDITENHANCEMENT : return "String";
	case A251THIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER : return "String";
	case A252THIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER : return "String";
	case A253COUNTERPARTYTHIRDPARTYCREDITENHANCEMENT : return "String";
	case A254COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER : return "String";
	case A255COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER : return "String";
	case A26AGGREGATECURRMARKETVALUEALLPOSITIONSUSD : return "String";
	case A27AGGREGATECURRMARKETVALUEPOSITIVEPOSITIONSUSD : return "String";
	case A28AGGREGATECURRMARKETVALUENEGATIVEPOSITIONSUSD : return "String";
	case A29CURRMARKETVALUEALLCOLLATERALRECORDSENTITYINUSD : return "String";
	case A210CURRMARKETVALUEALLCOLLATERALCOUNTERPARTYINUSD : return "String";
	case A211CURRMARKETVALUEALLCOLLATERALRECORDSENTITYREHYPOINUSD : return "String";
	case A212CURRMARKETVALUEALLCOLLATERALCOUNTERPARTYREHYPOINUSD : return "String";
	case A213RECORDSENTITYCOLLATERALNET : return "String";
	case A214COUNTERPARTYCOLLATERALNET : return "String";
	case A215NEXTMARGINPAYMENTDATE : return "String";
	case A216NEXTMARGINPAYMENTAMOUNTUSD : return "String";
	case A217SAFEKEEPINGAGENTIDENTIFIERRECORDSENTITY : return "String";
	case A218SAFEKEEPINGAGENTIDENTIFIERCOUNTERPARTY : return "String";
		}
		return null;
	}
	

}
