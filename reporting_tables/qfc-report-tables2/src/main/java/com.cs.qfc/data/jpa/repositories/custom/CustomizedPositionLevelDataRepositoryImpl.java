package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.PositionLevelData;
import com.cs.qfc.data.metadata.PositionLevelDataMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.PositionLevelDataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link PositionLevelData} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedPositionLevelDataRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedPositionLevelDataRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<PositionLevelDataDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + PositionLevelDataMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new PositionLevelDataMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new PositionLevelDataMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new PositionLevelDataMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<PositionLevelDataDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new PositionLevelDataRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			PositionLevelDataMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class PositionLevelDataRowMapper implements RowMapper<PositionLevelDataDTO> {
		@Override
		public PositionLevelDataDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			PositionLevelDataDTO ret = new PositionLevelDataDTO();


			
           ret.setId (rs.getString("id"));
           ret.setA11AsOfDate (rs.getDate("a11_as_of_date"));
           ret.setA12RecordsEntityIdentifier (rs.getString("a12_records_entity_identifier"));
           ret.setA13PositionsIdentifier (rs.getString("a13_positions_identifier"));
           ret.setA15InternalBookingLocationIdentifier (rs.getString("a15_internal_booking_location_identifier"));
           ret.setA16UniqueBookingUnitDeskIdentifier (rs.getString("a16_unique_booking_unit_desk_identifier"));
           ret.setA17TypeOfQfc (rs.getString("a17_type_of_qfc"));
           ret.setA171TypeOfQfcCoveredGuarantee (rs.getString("a_171_type_of_qfc_covered_guarantee"));
           ret.setA172UnderlyingQfcObligatorIdentifier (rs.getString("a172_underlying_qfc_obligator_identifier"));
           ret.setA18AgreementIdentifier (rs.getString("a18_agreement_identifier"));
           ret.setA19NettingAgreementIdrntifier (rs.getString("a19_netting_agreement_idrntifier"));
           ret.setA110NettingAgreementCounterpartyIdentifier (rs.getString("a110_netting_agreement_counterparty_identifier"));
           ret.setA111TradeDate (rs.getString("a111_trade_date"));
           ret.setA112TerminationDate (rs.getString("a112_termination_date"));
           ret.setA113NextPutCallCancellationDate (rs.getString("a113_next_put_call_cancellation_date"));
           ret.setA114NextPaymentDate (rs.getString("a114_next_payment_date"));
           ret.setA115LocalCurrencyOfPosition (rs.getString("a115_local_currency_of_position"));
           ret.setA116CurrentMarketValueInLocalCurrency (rs.getString("a116_current_market_value_in_local_currency"));
           ret.setA117CurrentMarketValueInUsd (rs.getString("a117_current_market_value_in_usd"));
           ret.setA118AssetClassification (rs.getString("a118_asset_classification"));
           ret.setA119NotionalPrincipalAmountLocalCurrency (rs.getString("a119_notional_or_principal_amount_local_currency"));
           ret.setA120NotionalPrincipalAmountUsd (rs.getString("a120_notional_or_principal_amount_usd"));
           ret.setA121CoveredByThirdPartyCreditEnhancement (rs.getString("a121_covered_by_third_party_credit_enhancement"));
           ret.setA1211ThirdPartyCreditEnhancementProviderIdentifier (rs.getString("a1211_third_party_credit_enhancement_provider_identifier"));
           ret.setA1212ThirdPartyCreditEnhancementAgreementIdentifier (rs.getString("a1212_third_party_credit_enhancement_agreement_identifier"));
           ret.setA213CounterpartyThirdPartyCreditEnhancement (rs.getString("a213_counterparty_third_party_credit_enhancement"));
           ret.setA1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier (rs.getString("a1214_counterparty_third_party_credit_enhancement_provider_identifier"));
           ret.setA1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier (rs.getString("a1215_counterparty_third_party_credit_enhancement_agreement_identifier"));
           ret.setA122RelatedPositionOfRecordsEntity (rs.getString("a122_related_position_of_records_entity"));
           ret.setA123ReferenceNumberForAnyRelatedLoan (rs.getString("a123_reference_number_for_any_related_loan"));
           ret.setA124IdentifierOfTheLenderOfRelatedLoan (rs.getString("a124_identifier_of_the_lender_of_related_loan"));
           ret.setSourceTxnId (rs.getString("source_txn_id"));
           ret.setA14CounterpartyIdentifier (rs.getString("a14_counterparty_identifier"));

			return ret;
		}
	}

}
