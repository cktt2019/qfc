package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.CounterpartyNetting;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CounterpartyNettingDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link CounterpartyNetting} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedCounterpartyNettingRepository {
	
	public Collection<CounterpartyNettingDTO> findByCustomCriteria (SearchInfoDTO info);

	}







