package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.LegalAgreements;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link LegalAgreements} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface LegalAgreementsRepository extends JpaRepository<LegalAgreements, Integer>{


	public List<LegalAgreements> findBySourceTxnId (String sourceTxnId);
	public List<LegalAgreements> findByA31AsOfDate (String a31AsOfDate);
	public List<LegalAgreements> findByA32RecordsEntityIdentifier (String a32RecordsEntityIdentifier);
	public List<LegalAgreements> findByA33AgreementIdentifier (String a33AgreementIdentifier);
	public List<LegalAgreements> findByA34NameOfAgreementGoverningDoc (String a34NameOfAgreementGoverningDoc);
	public List<LegalAgreements> findByA35AgreementDate (String a35AgreementDate);
	public List<LegalAgreements> findByA36AgreementCounterpartyIdentifier (String a36AgreementCounterpartyIdentifier);
	public List<LegalAgreements> findByA361UnderlyingQfcObligatorIdentifier (String a361UnderlyingQfcObligatorIdentifier);
	public List<LegalAgreements> findByA37AgreementGoverningLaw (String a37AgreementGoverningLaw);
	public List<LegalAgreements> findByA38CrossDefaultProvision (String a38CrossDefaultProvision);
	public List<LegalAgreements> findByA39IdentityOfCrossDefaultEntities (String a39IdentityOfCrossDefaultEntities);
	public List<LegalAgreements> findByA310CoveredByThirdPartyCreditEnhancement (String a310CoveredByThirdPartyCreditEnhancement);
	public List<LegalAgreements> findByA311ThirdPartyCreditEnhancementProviderIdentifier (String a311ThirdPartyCreditEnhancementProviderIdentifier);
	public List<LegalAgreements> findByA312AssociatedCreditEnhancementDocIdentifier (String a312AssociatedCreditEnhancementDocIdentifier);
	public List<LegalAgreements> findByA3121CounterpartyThirdPartyCreditEnhancement (String a3121CounterpartyThirdPartyCreditEnhancement);
	public List<LegalAgreements> findByA3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier (String a3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier);
	public List<LegalAgreements> findByA3123CounterpartyAssociatedCreditEnhancementDocIdentifier (String a3123CounterpartyAssociatedCreditEnhancementDocIdentifier);
	public List<LegalAgreements> findByA313CounterpartyContactInfoName (String a313CounterpartyContactInfoName);
	public List<LegalAgreements> findByA314CounterpartyContactInfoAddress (String a314CounterpartyContactInfoAddress);
	public List<LegalAgreements> findByA315CounterpartyContactInfoPhone (String a315CounterpartyContactInfoPhone);
	public List<LegalAgreements> findByA316CounterpartyContactInfoEmail (String a316CounterpartyContactInfoEmail);
	

	
	public Page<LegalAgreements> findAll (Pageable page);
	
	

	

	
	}







