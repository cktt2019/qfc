package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.LegalAgreements;
import com.cs.qfc.data.model.LegalAgreementsDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class LegalAgreementsToDTOConverter  {
	

	

public static LegalAgreementsDTO convert(LegalAgreements legalAgreements) {
 if (legalAgreements != null)
  return LegalAgreementsDTO.builder().id(legalAgreements.getId())
.sourceTxnId(legalAgreements.getSourceTxnId())
.a31AsOfDate(legalAgreements.getA31AsOfDate())
.a32RecordsEntityIdentifier(legalAgreements.getA32RecordsEntityIdentifier())
.a33AgreementIdentifier(legalAgreements.getA33AgreementIdentifier())
.a34NameOfAgreementGoverningDoc(legalAgreements.getA34NameOfAgreementGoverningDoc())
.a35AgreementDate(legalAgreements.getA35AgreementDate())
.a36AgreementCounterpartyIdentifier(legalAgreements.getA36AgreementCounterpartyIdentifier())
.a361UnderlyingQfcObligatorIdentifier(legalAgreements.getA361UnderlyingQfcObligatorIdentifier())
.a37AgreementGoverningLaw(legalAgreements.getA37AgreementGoverningLaw())
.a38CrossDefaultProvision(legalAgreements.getA38CrossDefaultProvision())
.a39IdentityOfCrossDefaultEntities(legalAgreements.getA39IdentityOfCrossDefaultEntities())
.a310CoveredByThirdPartyCreditEnhancement(legalAgreements.getA310CoveredByThirdPartyCreditEnhancement())
.a311ThirdPartyCreditEnhancementProviderIdentifier(legalAgreements.getA311ThirdPartyCreditEnhancementProviderIdentifier())
.a312AssociatedCreditEnhancementDocIdentifier(legalAgreements.getA312AssociatedCreditEnhancementDocIdentifier())
.a3121CounterpartyThirdPartyCreditEnhancement(legalAgreements.getA3121CounterpartyThirdPartyCreditEnhancement())
.a3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier(legalAgreements.getA3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier())
.a3123CounterpartyAssociatedCreditEnhancementDocIdentifier(legalAgreements.getA3123CounterpartyAssociatedCreditEnhancementDocIdentifier())
.a313CounterpartyContactInfoName(legalAgreements.getA313CounterpartyContactInfoName())
.a314CounterpartyContactInfoAddress(legalAgreements.getA314CounterpartyContactInfoAddress())
.a315CounterpartyContactInfoPhone(legalAgreements.getA315CounterpartyContactInfoPhone())
.a316CounterpartyContactInfoEmail(legalAgreements.getA316CounterpartyContactInfoEmail())
.build();
 return null;
	}
    
}
