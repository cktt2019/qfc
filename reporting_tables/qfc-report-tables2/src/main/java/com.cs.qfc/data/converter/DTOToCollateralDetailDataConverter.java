package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CollateralDetailData;
import com.cs.qfc.data.model.CollateralDetailDataDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToCollateralDetailDataConverter  {
	

	

public static CollateralDetailData convert(CollateralDetailDataDTO collateralDetailDataDto) {
 if (collateralDetailDataDto != null)
  return CollateralDetailData.builder().id(collateralDetailDataDto.getId())
.sourceTxnId(collateralDetailDataDto.getSourceTxnId())
.a41AsOfDate(collateralDetailDataDto.getA41AsOfDate())
.a42RecordsEntityIdentifier(collateralDetailDataDto.getA42RecordsEntityIdentifier())
.a43collateraoPostedReceivedFlag(collateralDetailDataDto.getA43collateraoPostedReceivedFlag())
.a44CounterpartyIdentifier(collateralDetailDataDto.getA44CounterpartyIdentifier())
.a45NettingAgreementIdentifier(collateralDetailDataDto.getA45NettingAgreementIdentifier())
.a46UniqueCollateralItemIdentifier(collateralDetailDataDto.getA46UniqueCollateralItemIdentifier())
.a47OriginalFaceAmtCollateralItemLocalCurr(collateralDetailDataDto.getA47OriginalFaceAmtCollateralItemLocalCurr())
.a48LocalCurrencyOfCollateralItem(collateralDetailDataDto.getA48LocalCurrencyOfCollateralItem())
.a49MarketValueAmountOfCollateralItemInUsd(collateralDetailDataDto.getA49MarketValueAmountOfCollateralItemInUsd())
.a410DescriptionOfCollateralItem(collateralDetailDataDto.getA410DescriptionOfCollateralItem())
.a411AssetClassification(collateralDetailDataDto.getA411AssetClassification())
.a412CollateralPortfolioSegregationStatus(collateralDetailDataDto.getA412CollateralPortfolioSegregationStatus())
.a413CollateralLocation(collateralDetailDataDto.getA413CollateralLocation())
.a414CollateralJurisdiction(collateralDetailDataDto.getA414CollateralJurisdiction())
.a415CollateralRehypoAllowed(collateralDetailDataDto.getA415CollateralRehypoAllowed())
 
.build();
 return null;
	}
    
}
