package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.PositionLevelData;
import com.cs.qfc.data.model.PositionLevelDataDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class PositionLevelDataToDTOConverter  {
	

	

public static PositionLevelDataDTO convert(PositionLevelData positionLevelData) {
 if (positionLevelData != null)
  return PositionLevelDataDTO.builder().id(positionLevelData.getId())
.a11AsOfDate(positionLevelData.getA11AsOfDate())
.a12RecordsEntityIdentifier(positionLevelData.getA12RecordsEntityIdentifier())
.a13PositionsIdentifier(positionLevelData.getA13PositionsIdentifier())
.a15InternalBookingLocationIdentifier(positionLevelData.getA15InternalBookingLocationIdentifier())
.a16UniqueBookingUnitDeskIdentifier(positionLevelData.getA16UniqueBookingUnitDeskIdentifier())
.a17TypeOfQfc(positionLevelData.getA17TypeOfQfc())
.a171TypeOfQfcCoveredGuarantee(positionLevelData.getA171TypeOfQfcCoveredGuarantee())
.a172UnderlyingQfcObligatorIdentifier(positionLevelData.getA172UnderlyingQfcObligatorIdentifier())
.a18AgreementIdentifier(positionLevelData.getA18AgreementIdentifier())
.a19NettingAgreementIdrntifier(positionLevelData.getA19NettingAgreementIdrntifier())
.a110NettingAgreementCounterpartyIdentifier(positionLevelData.getA110NettingAgreementCounterpartyIdentifier())
.a111TradeDate(positionLevelData.getA111TradeDate())
.a112TerminationDate(positionLevelData.getA112TerminationDate())
.a113NextPutCallCancellationDate(positionLevelData.getA113NextPutCallCancellationDate())
.a114NextPaymentDate(positionLevelData.getA114NextPaymentDate())
.a115LocalCurrencyOfPosition(positionLevelData.getA115LocalCurrencyOfPosition())
.a116CurrentMarketValueInLocalCurrency(positionLevelData.getA116CurrentMarketValueInLocalCurrency())
.a117CurrentMarketValueInUsd(positionLevelData.getA117CurrentMarketValueInUsd())
.a118AssetClassification(positionLevelData.getA118AssetClassification())
.a119NotionalPrincipalAmountLocalCurrency(positionLevelData.getA119NotionalPrincipalAmountLocalCurrency())
.a120NotionalPrincipalAmountUsd(positionLevelData.getA120NotionalPrincipalAmountUsd())
.a121CoveredByThirdPartyCreditEnhancement(positionLevelData.getA121CoveredByThirdPartyCreditEnhancement())
.a1211ThirdPartyCreditEnhancementProviderIdentifier(positionLevelData.getA1211ThirdPartyCreditEnhancementProviderIdentifier())
.a1212ThirdPartyCreditEnhancementAgreementIdentifier(positionLevelData.getA1212ThirdPartyCreditEnhancementAgreementIdentifier())
.a213CounterpartyThirdPartyCreditEnhancement(positionLevelData.getA213CounterpartyThirdPartyCreditEnhancement())
.a1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier(positionLevelData.getA1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier())
.a1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier(positionLevelData.getA1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier())
.a122RelatedPositionOfRecordsEntity(positionLevelData.getA122RelatedPositionOfRecordsEntity())
.a123ReferenceNumberForAnyRelatedLoan(positionLevelData.getA123ReferenceNumberForAnyRelatedLoan())
.a124IdentifierOfTheLenderOfRelatedLoan(positionLevelData.getA124IdentifierOfTheLenderOfRelatedLoan())
.sourceTxnId(positionLevelData.getSourceTxnId())
.a14CounterpartyIdentifier(positionLevelData.getA14CounterpartyIdentifier())
.build();
 return null;
	}
    
}
