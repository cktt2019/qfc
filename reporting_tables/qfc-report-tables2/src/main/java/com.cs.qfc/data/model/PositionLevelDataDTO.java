package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class PositionLevelDataDTO  {
   
	    private String id ;
   
	    private java.sql.Date a11AsOfDate ;
   
	    private String a12RecordsEntityIdentifier ;
   
	    private String a13PositionsIdentifier ;
   
	    private String a15InternalBookingLocationIdentifier ;
   
	    private String a16UniqueBookingUnitDeskIdentifier ;
   
	    private String a17TypeOfQfc ;
   
	    private String a171TypeOfQfcCoveredGuarantee ;
   
	    private String a172UnderlyingQfcObligatorIdentifier ;
   
	    private String a18AgreementIdentifier ;
   
	    private String a19NettingAgreementIdrntifier ;
   
	    private String a110NettingAgreementCounterpartyIdentifier ;
   
	    private String a111TradeDate ;
   
	    private String a112TerminationDate ;
   
	    private String a113NextPutCallCancellationDate ;
   
	    private String a114NextPaymentDate ;
   
	    private String a115LocalCurrencyOfPosition ;
   
	    private String a116CurrentMarketValueInLocalCurrency ;
   
	    private String a117CurrentMarketValueInUsd ;
   
	    private String a118AssetClassification ;
   
	    private String a119NotionalPrincipalAmountLocalCurrency ;
   
	    private String a120NotionalPrincipalAmountUsd ;
   
	    private String a121CoveredByThirdPartyCreditEnhancement ;
   
	    private String a1211ThirdPartyCreditEnhancementProviderIdentifier ;
   
	    private String a1212ThirdPartyCreditEnhancementAgreementIdentifier ;
   
	    private String a213CounterpartyThirdPartyCreditEnhancement ;
   
	    private String a1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier ;
   
	    private String a1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier ;
   
	    private String a122RelatedPositionOfRecordsEntity ;
   
	    private String a123ReferenceNumberForAnyRelatedLoan ;
   
	    private String a124IdentifierOfTheLenderOfRelatedLoan ;
   
	    private String sourceTxnId ;
   
	    private String a14CounterpartyIdentifier ;
    


}
