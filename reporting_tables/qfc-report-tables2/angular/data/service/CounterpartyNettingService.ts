import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {CounterpartyNetting} from './CounterpartyNetting.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class CounterpartyNettingService {

    constructor(
    private http: HttpClient
    ) {}

	private CounterpartyNettingUrl = 'http://localhost:8081/api/api/counterpartynettings';

	private selectedCounterpartyNetting : CounterpartyNetting;

    counterpartyNettingDetailViewComponent : any;

      setSelectedCounterpartyNetting(selectedItem : CounterpartyNetting){
        this.selectedCounterpartyNetting = selectedItem;
        console.log('setSelectedCounterpartyNetting' + selectedItem);
      }

    getSelectedCounterpartyNetting () : CounterpartyNetting{
        return this.selectedCounterpartyNetting ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 saveCounterpartyNetting (counterpartyNetting : CounterpartyNetting ){
	              this.http.post<CounterpartyNetting>(this.CounterpartyNettingUrl, counterpartyNetting , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}


customSearch (searchCritera : SearchCriteria) : Observable<CounterpartyNetting[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<CounterpartyNetting[]>(this.CounterpartyNettingUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched CounterpartyNetting(s)')),
                   catchError(this.handleError<CounterpartyNetting[]>('findAll', []))
                   );
	}

   findAll () : Observable<CounterpartyNetting[]>{

	   const url = `this.CounterpartyNettingUrl"}`;
	   return this.http.get<CounterpartyNetting[]>(this.CounterpartyNettingUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched CounterpartyNetting(s)')),
                   catchError(this.handleError<CounterpartyNetting[]>('findAll', []))
                   );
	}
     

    findById (id : number): Observable<CounterpartyNetting>{
	  const url = `${this.CounterpartyNettingUrl}/${id}`;
      return this.http.get<CounterpartyNetting>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched CounterpartyNetting with id=${id}`)),
        catchError(this.handleError<CounterpartyNetting>(`CounterpartyNetting id=${id}`))
      );
	 }
	

   updateCounterpartyNetting (counterpartyNetting : CounterpartyNetting ) : Observable<any> {
         return this.http.put<CounterpartyNetting>(this.CounterpartyNettingUrl, counterpartyNetting , this.httpOptions).pipe(
           tap(_ => console.log(`updated CounterpartyNetting ${counterpartyNetting}`)),
           catchError(this.handleError<CounterpartyNetting>(`CounterpartyNetting =${counterpartyNetting}`))
         );
	}
	

   deleteById (id : number){
	     const url = `${this.CounterpartyNettingUrl}/${id}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
