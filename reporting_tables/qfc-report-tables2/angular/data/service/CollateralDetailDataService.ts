import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {CollateralDetailData} from './CollateralDetailData.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class CollateralDetailDataService {

    constructor(
    private http: HttpClient
    ) {}

	private CollateralDetailDataUrl = 'http://localhost:8081/api/api/collateraldetaildatas';

	private selectedCollateralDetailData : CollateralDetailData;

    collateralDetailDataDetailViewComponent : any;

      setSelectedCollateralDetailData(selectedItem : CollateralDetailData){
        this.selectedCollateralDetailData = selectedItem;
        console.log('setSelectedCollateralDetailData' + selectedItem);
      }

    getSelectedCollateralDetailData () : CollateralDetailData{
        return this.selectedCollateralDetailData ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 saveCollateralDetailData (collateralDetailData : CollateralDetailData ){
	              this.http.post<CollateralDetailData>(this.CollateralDetailDataUrl, collateralDetailData , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}


customSearch (searchCritera : SearchCriteria) : Observable<CollateralDetailData[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<CollateralDetailData[]>(this.CollateralDetailDataUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched CollateralDetailData(s)')),
                   catchError(this.handleError<CollateralDetailData[]>('findAll', []))
                   );
	}

   findAll () : Observable<CollateralDetailData[]>{

	   const url = `this.CollateralDetailDataUrl"}`;
	   return this.http.get<CollateralDetailData[]>(this.CollateralDetailDataUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched CollateralDetailData(s)')),
                   catchError(this.handleError<CollateralDetailData[]>('findAll', []))
                   );
	}
     

    findById (id : number): Observable<CollateralDetailData>{
	  const url = `${this.CollateralDetailDataUrl}/${id}`;
      return this.http.get<CollateralDetailData>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched CollateralDetailData with id=${id}`)),
        catchError(this.handleError<CollateralDetailData>(`CollateralDetailData id=${id}`))
      );
	 }
	

   updateCollateralDetailData (collateralDetailData : CollateralDetailData ) : Observable<any> {
         return this.http.put<CollateralDetailData>(this.CollateralDetailDataUrl, collateralDetailData , this.httpOptions).pipe(
           tap(_ => console.log(`updated CollateralDetailData ${collateralDetailData}`)),
           catchError(this.handleError<CollateralDetailData>(`CollateralDetailData =${collateralDetailData}`))
         );
	}
	

   deleteById (id : number){
	     const url = `${this.CollateralDetailDataUrl}/${id}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
