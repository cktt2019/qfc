import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {LegalAgreements} from './LegalAgreements.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class LegalAgreementsService {

    constructor(
    private http: HttpClient
    ) {}

	private LegalAgreementsUrl = 'http://localhost:8081/api/api/legalagreementss';

	private selectedLegalAgreements : LegalAgreements;

    legalAgreementsDetailViewComponent : any;

      setSelectedLegalAgreements(selectedItem : LegalAgreements){
        this.selectedLegalAgreements = selectedItem;
        console.log('setSelectedLegalAgreements' + selectedItem);
      }

    getSelectedLegalAgreements () : LegalAgreements{
        return this.selectedLegalAgreements ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 saveLegalAgreements (legalAgreements : LegalAgreements ){
	              this.http.post<LegalAgreements>(this.LegalAgreementsUrl, legalAgreements , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}


customSearch (searchCritera : SearchCriteria) : Observable<LegalAgreements[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<LegalAgreements[]>(this.LegalAgreementsUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched LegalAgreements(s)')),
                   catchError(this.handleError<LegalAgreements[]>('findAll', []))
                   );
	}

   findAll () : Observable<LegalAgreements[]>{

	   const url = `this.LegalAgreementsUrl"}`;
	   return this.http.get<LegalAgreements[]>(this.LegalAgreementsUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched LegalAgreements(s)')),
                   catchError(this.handleError<LegalAgreements[]>('findAll', []))
                   );
	}
     

    findById (id : number): Observable<LegalAgreements>{
	  const url = `${this.LegalAgreementsUrl}/${id}`;
      return this.http.get<LegalAgreements>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched LegalAgreements with id=${id}`)),
        catchError(this.handleError<LegalAgreements>(`LegalAgreements id=${id}`))
      );
	 }
	

   updateLegalAgreements (legalAgreements : LegalAgreements ) : Observable<any> {
         return this.http.put<LegalAgreements>(this.LegalAgreementsUrl, legalAgreements , this.httpOptions).pipe(
           tap(_ => console.log(`updated LegalAgreements ${legalAgreements}`)),
           catchError(this.handleError<LegalAgreements>(`LegalAgreements =${legalAgreements}`))
         );
	}
	

   deleteById (id : number){
	     const url = `${this.LegalAgreementsUrl}/${id}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
