import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {PositionLevelData} from './PositionLevelData.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class PositionLevelDataService {

    constructor(
    private http: HttpClient
    ) {}

	private PositionLevelDataUrl = 'http://localhost:8081/api/api/positionleveldatas';

	private selectedPositionLevelData : PositionLevelData;

    positionLevelDataDetailViewComponent : any;

      setSelectedPositionLevelData(selectedItem : PositionLevelData){
        this.selectedPositionLevelData = selectedItem;
        console.log('setSelectedPositionLevelData' + selectedItem);
      }

    getSelectedPositionLevelData () : PositionLevelData{
        return this.selectedPositionLevelData ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 savePositionLevelData (positionLevelData : PositionLevelData ){
	              this.http.post<PositionLevelData>(this.PositionLevelDataUrl, positionLevelData , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}


customSearch (searchCritera : SearchCriteria) : Observable<PositionLevelData[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<PositionLevelData[]>(this.PositionLevelDataUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched PositionLevelData(s)')),
                   catchError(this.handleError<PositionLevelData[]>('findAll', []))
                   );
	}

   findAll () : Observable<PositionLevelData[]>{

	   const url = `this.PositionLevelDataUrl"}`;
	   return this.http.get<PositionLevelData[]>(this.PositionLevelDataUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched PositionLevelData(s)')),
                   catchError(this.handleError<PositionLevelData[]>('findAll', []))
                   );
	}
     

    findById (id : string): Observable<PositionLevelData>{
	  const url = `${this.PositionLevelDataUrl}/${id}`;
      return this.http.get<PositionLevelData>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched PositionLevelData with id=${id}`)),
        catchError(this.handleError<PositionLevelData>(`PositionLevelData id=${id}`))
      );
	 }
	

   updatePositionLevelData (positionLevelData : PositionLevelData ) : Observable<any> {
         return this.http.put<PositionLevelData>(this.PositionLevelDataUrl, positionLevelData , this.httpOptions).pipe(
           tap(_ => console.log(`updated PositionLevelData ${positionLevelData}`)),
           catchError(this.handleError<PositionLevelData>(`PositionLevelData =${positionLevelData}`))
         );
	}
	

   deleteById (id : string){
	     const url = `${this.PositionLevelDataUrl}/${id}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
