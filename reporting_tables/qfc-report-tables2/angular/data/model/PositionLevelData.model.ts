

	export interface PositionLevelData  {

 	    id : string;

 	    a11AsOfDate : any;

 	    a12RecordsEntityIdentifier : string;

 	    a13PositionsIdentifier : string;

 	    a15InternalBookingLocationIdentifier : string;

 	    a16UniqueBookingUnitDeskIdentifier : string;

 	    a17TypeOfQfc : string;

 	    a171TypeOfQfcCoveredGuarantee : string;

 	    a172UnderlyingQfcObligatorIdentifier : string;

 	    a18AgreementIdentifier : string;

 	    a19NettingAgreementIdrntifier : string;

 	    a110NettingAgreementCounterpartyIdentifier : string;

 	    a111TradeDate : string;

 	    a112TerminationDate : string;

 	    a113NextPutCallCancellationDate : string;

 	    a114NextPaymentDate : string;

 	    a115LocalCurrencyOfPosition : string;

 	    a116CurrentMarketValueInLocalCurrency : string;

 	    a117CurrentMarketValueInUsd : string;

 	    a118AssetClassification : string;

 	    a119NotionalPrincipalAmountLocalCurrency : string;

 	    a120NotionalPrincipalAmountUsd : string;

 	    a121CoveredByThirdPartyCreditEnhancement : string;

 	    a1211ThirdPartyCreditEnhancementProviderIdentifier : string;

 	    a1212ThirdPartyCreditEnhancementAgreementIdentifier : string;

 	    a213CounterpartyThirdPartyCreditEnhancement : string;

 	    a1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier : string;

 	    a1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier : string;

 	    a122RelatedPositionOfRecordsEntity : string;

 	    a123ReferenceNumberForAnyRelatedLoan : string;

 	    a124IdentifierOfTheLenderOfRelatedLoan : string;

 	    sourceTxnId : string;

 	    a14CounterpartyIdentifier : string;

    

}
