

	export interface CollateralDetailData  {

 	    id : number;

 	    sourceTxnId : string;

 	    a41AsOfDate : string;

 	    a42RecordsEntityIdentifier : string;

 	    a43collateraoPostedReceivedFlag : string;

 	    a44CounterpartyIdentifier : string;

 	    a45NettingAgreementIdentifier : string;

 	    a46UniqueCollateralItemIdentifier : string;

 	    a47OriginalFaceAmtCollateralItemLocalCurr : string;

 	    a48LocalCurrencyOfCollateralItem : string;

 	    a49MarketValueAmountOfCollateralItemInUsd : string;

 	    a410DescriptionOfCollateralItem : string;

 	    a411AssetClassification : string;

 	    a412CollateralPortfolioSegregationStatus : string;

 	    a413CollateralLocation : string;

 	    a414CollateralJurisdiction : string;

 	    a415CollateralRehypoAllowed : string;

    

}
