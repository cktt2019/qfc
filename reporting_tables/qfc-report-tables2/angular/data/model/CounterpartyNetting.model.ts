

	export interface CounterpartyNetting  {

 	    id : number;

 	    sourceTxnId : string;

 	    a21AsOfDate : string;

 	    a22RecordsEntityIdentifier : string;

 	    a23NettingAgreementCounterpartyIdentifier : string;

 	    a24NettingAgreementIdentifier : string;

 	    a241UnderlyingQfcObligatorIdentifier : string;

 	    a25CoveredByThirdPartyCreditEnhancement : string;

 	    a251ThirdPartyCreditEnhancementProviderIdentifier : string;

 	    a252ThirdPartyCreditEnhancementAgreementIdentifier : string;

 	    a253CounterpartyThirdPartyCreditEnhancement : string;

 	    a254CounterpartyThirdPartyCreditEnhancementProviderIdentifier : string;

 	    a255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier : string;

 	    a26AggregateCurrMarketValueAllPositionsUsd : string;

 	    a27AggregateCurrMarketValuePositivePositionsUsd : string;

 	    a28AggregateCurrMarketValueNegativePositionsUsd : string;

 	    a29CurrMarketValueAllCollateralRecordsEntityInUsd : string;

 	    a210CurrMarketValueAllCollateralCounterpartyInUsd : string;

 	    a211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd : string;

 	    a212CurrMarketValueAllCollateralCounterpartyRehypoInUsd : string;

 	    a213RecordsEntityCollateralNet : string;

 	    a214CounterpartyCollateralNet : string;

 	    a215NextMarginPaymentDate : string;

 	    a216NextMarginPaymentAmountUsd : string;

 	    a217SafekeepingAgentIdentifierRecordsEntity : string;

 	    a218SafekeepingAgentIdentifierCounterparty : string;

    

}
