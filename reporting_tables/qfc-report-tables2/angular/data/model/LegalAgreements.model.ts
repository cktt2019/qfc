

	export interface LegalAgreements  {

 	    id : number;

 	    sourceTxnId : string;

 	    a31AsOfDate : string;

 	    a32RecordsEntityIdentifier : string;

 	    a33AgreementIdentifier : string;

 	    a34NameOfAgreementGoverningDoc : string;

 	    a35AgreementDate : string;

 	    a36AgreementCounterpartyIdentifier : string;

 	    a361UnderlyingQfcObligatorIdentifier : string;

 	    a37AgreementGoverningLaw : string;

 	    a38CrossDefaultProvision : string;

 	    a39IdentityOfCrossDefaultEntities : string;

 	    a310CoveredByThirdPartyCreditEnhancement : string;

 	    a311ThirdPartyCreditEnhancementProviderIdentifier : string;

 	    a312AssociatedCreditEnhancementDocIdentifier : string;

 	    a3121CounterpartyThirdPartyCreditEnhancement : string;

 	    a3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier : string;

 	    a3123CounterpartyAssociatedCreditEnhancementDocIdentifier : string;

 	    a313CounterpartyContactInfoName : string;

 	    a314CounterpartyContactInfoAddress : string;

 	    a315CounterpartyContactInfoPhone : string;

 	    a316CounterpartyContactInfoEmail : string;

    

}
