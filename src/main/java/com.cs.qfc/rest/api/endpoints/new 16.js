{
    "cells"
:
    [
        {
            "type": "qad.Question",
            "optionHeight": 30,
            "questionHeight": 45,
            "paddingBottom": 30,
            "minWidth": 400,
            "ports": {
                "groups": {
                    "in": {
                        "position": "left",
                        "attrs": {
                            "circle": {
                                "magnet": true,
                                "stroke": "black",
                                "fill": "transparent",
                                "r": 4
                            }
                        }
                    },
                    "out": {
                        "position": "right",
                        "attrs": {
                            "circle": {
                                "magnet": true,
                                "stroke": "black",
                                "fill": "transparent",
                                "r": 4
                            }
                        }
                    }
                },
                "items": [{
                    "group": "out",
                    "id": "option-52",
                    "args": {
                        "y": 60
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-52",
                    "args": {
                        "y": 60
                    }
                }, {
                    "group": "out",
                    "id": "option-53",
                    "args": {
                        "y": 90
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-53",
                    "args": {
                        "y": 90
                    }
                }, {
                    "group": "out",
                    "id": "option-54",
                    "args": {
                        "y": 120
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-54",
                    "args": {
                        "y": 120
                    }
                }, {
                    "group": "out",
                    "id": "option-55",
                    "args": {
                        "y": 150
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-55",
                    "args": {
                        "y": 150
                    }
                }, {
                    "group": "out",
                    "id": "option-56",
                    "args": {
                        "y": 180
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-56",
                    "args": {
                        "y": 180
                    }
                }, {
                    "group": "out",
                    "id": "option-57",
                    "args": {
                        "y": 210
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-57",
                    "args": {
                        "y": 210
                    }
                }, {
                    "group": "out",
                    "id": "option-58",
                    "args": {
                        "y": 240
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-58",
                    "args": {
                        "y": 240
                    }
                }, {
                    "group": "out",
                    "id": "option-59",
                    "args": {
                        "y": 270
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-59",
                    "args": {
                        "y": 270
                    }
                }]
            },
            "position": {
                "x": 730,
                "y": 170
            },
            "size": {
                "width": 330,
                "height": 315
            },
            "angle": 0,
            "question": "Entity-8E9AJ97",
            "schema": "",
            "linkTable": false,
            "options": [{
                "id": "option-52",
                "text": "NameYERJPHL3NCO4VV      | Varchar   | PK |     ",
                "fieldName": "NameYERJPHL3NCO4VV",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": true,
                "foreignKey": false
            }, {
                "id": "option-53",
                "text": "NameJ0RB2YV32JRJAM      | VARCHAR   |    |     ",
                "fieldName": "NameJ0RB2YV32JRJAM",
                "dataType": "VARCHAR",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-54",
                "text": "NameWB73JUPWGCWG5O      | Varchar   |    |     ",
                "fieldName": "NameWB73JUPWGCWG5O",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-55",
                "text": "NameTU94U6VTKILDIL      | Varchar   |    |     ",
                "fieldName": "NameTU94U6VTKILDIL",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-56",
                "text": "NameYXELGA1PPBVLN9      | Varchar   |    |     ",
                "fieldName": "NameYXELGA1PPBVLN9",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-57",
                "text": "NameSJ98XANUO5I9BM      | Varchar   |    |     ",
                "fieldName": "NameSJ98XANUO5I9BM",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-58",
                "text": "NameE5YO7D3ZJKOOBQ      | Varchar   |    | FK ",
                "fieldName": "NameE5YO7D3ZJKOOBQ",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": true
            }, {
                "id": "option-59",
                "text": "NameMSDJ8NHEHQGXWE      | Varchar   |    | FK ",
                "fieldName": "NameMSDJ8NHEHQGXWE",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": true
            }],
            "selectedAttribute": {
                "id": "option-59",
                "text": "NameMSDJ8NHEHQGXWE      | Varchar   |    | FK ",
                "fieldName": "NameMSDJ8NHEHQGXWE",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": true
            },
            "id": "3c173c52-c6f4-4800-896c-d3e6301edaa3",
            "z": 1,
            "currentTextPath": "question",
            "attrs": {
                ".options": {
                    "refY": 45
                },
                ".question-text": {
                    "text": "Entity-8E9AJ97"
                },
                ".option-option-52": {
                    "transform": "translate(0, 0)",
                    "dynamic": true
                },
                ".option-option-52 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-52 .option-text": {
                    "text": "NameYERJPHL3NCO4VV      | Varchar   | PK |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-53": {
                    "transform": "translate(0, 30)",
                    "dynamic": true
                },
                ".option-option-53 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-53 .option-text": {
                    "text": "NameJ0RB2YV32JRJAM      | VARCHAR   |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-54": {
                    "transform": "translate(0, 60)",
                    "dynamic": true
                },
                ".option-option-54 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-54 .option-text": {
                    "text": "NameWB73JUPWGCWG5O      | Varchar   |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-55": {
                    "transform": "translate(0, 90)",
                    "dynamic": true
                },
                ".option-option-55 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-55 .option-text": {
                    "text": "NameTU94U6VTKILDIL      | Varchar   |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-56": {
                    "transform": "translate(0, 120)",
                    "dynamic": true
                },
                ".option-option-56 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-56 .option-text": {
                    "text": "NameYXELGA1PPBVLN9      | Varchar   |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-57": {
                    "transform": "translate(0, 150)",
                    "dynamic": true
                },
                ".option-option-57 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-57 .option-text": {
                    "text": "NameSJ98XANUO5I9BM      | Varchar   |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-58": {
                    "transform": "translate(0, 180)",
                    "dynamic": true
                },
                ".option-option-58 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-58 .option-text": {
                    "text": "NameE5YO7D3ZJKOOBQ      | Varchar   |    | FK ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-59": {
                    "transform": "translate(0, 210)",
                    "dynamic": true
                },
                ".option-option-59 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-59 .option-text": {
                    "text": "NameMSDJ8NHEHQGXWE      | Varchar   |    | FK ",
                    "dynamic": true,
                    "refY": 15
                }
            }
        },
        {
            "type": "qad.Question",
            "optionHeight": 30,
            "questionHeight": 45,
            "paddingBottom": 30,
            "minWidth": 400,
            "ports": {
                "groups": {
                    "in": {
                        "position": "left",
                        "attrs": {
                            "circle": {
                                "magnet": true,
                                "stroke": "black",
                                "fill": "transparent",
                                "r": 4
                            }
                        }
                    },
                    "out": {
                        "position": "right",
                        "attrs": {
                            "circle": {
                                "magnet": true,
                                "stroke": "black",
                                "fill": "transparent",
                                "r": 4
                            }
                        }
                    }
                },
                "items": [{
                    "group": "out",
                    "id": "option-68",
                    "args": {
                        "y": 60
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-68",
                    "args": {
                        "y": 60
                    }
                }, {
                    "group": "out",
                    "id": "option-69",
                    "args": {
                        "y": 90
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-69",
                    "args": {
                        "y": 90
                    }
                }, {
                    "group": "out",
                    "id": "option-70",
                    "args": {
                        "y": 120
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-70",
                    "args": {
                        "y": 120
                    }
                }, {
                    "group": "out",
                    "id": "option-71",
                    "args": {
                        "y": 150
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-71",
                    "args": {
                        "y": 150
                    }
                }, {
                    "group": "out",
                    "id": "option-72",
                    "args": {
                        "y": 180
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-72",
                    "args": {
                        "y": 180
                    }
                }, {
                    "group": "out",
                    "id": "option-73",
                    "args": {
                        "y": 210
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-73",
                    "args": {
                        "y": 210
                    }
                }, {
                    "group": "out",
                    "id": "option-74",
                    "args": {
                        "y": 240
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-74",
                    "args": {
                        "y": 240
                    }
                }, {
                    "group": "out",
                    "id": "option-75",
                    "args": {
                        "y": 270
                    }
                }, {
                    "group": "in",
                    "id": "inputPort_option-75",
                    "args": {
                        "y": 270
                    }
                }]
            },
            "position": {
                "x": -80,
                "y": 140
            },
            "size": {
                "width": 330,
                "height": 315
            },
            "angle": 0,
            "question": "Entity-GWYAIUK",
            "schema": "",
            "linkTable": false,
            "options": [{
                "id": "option-68",
                "text": "NameJ1UGQJ15JLY2EF      | Varchar   | PK |     ",
                "fieldName": "NameJ1UGQJ15JLY2EF",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": true,
                "foreignKey": false
            }, {
                "id": "option-69",
                "text": "NameUEMPR1C2WVXL8Y      | INTEGER   | PK |     ",
                "fieldName": "NameUEMPR1C2WVXL8Y",
                "dataType": "INTEGER",
                "nullable": false,
                "primaryKey": true,
                "foreignKey": false
            }, {
                "id": "option-70",
                "text": "NameUPASSUU1IIFKWB      | DATE      |    |     ",
                "fieldName": "NameUPASSUU1IIFKWB",
                "dataType": "DATE",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-71",
                "text": "NameEVJCWOI7GG9LGL      | Varchar   |    |     ",
                "fieldName": "NameEVJCWOI7GG9LGL",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-72",
                "text": "NameUVPYCQ8LXHFF5T      | Varchar   |    |     ",
                "fieldName": "NameUVPYCQ8LXHFF5T",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-73",
                "text": "NameSMS3OG2OJQ9NZD      | Varchar   |    |     ",
                "fieldName": "NameSMS3OG2OJQ9NZD",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-74",
                "text": "NameKO6YLQJI6I9PB6      | Varchar   |    |     ",
                "fieldName": "NameKO6YLQJI6I9PB6",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }, {
                "id": "option-75",
                "text": "NameQMOZ4NOFJPBJ9Z      | Varchar   |    |     ",
                "fieldName": "NameQMOZ4NOFJPBJ9Z",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": false,
                "foreignKey": false
            }],
            "selectedAttribute": {
                "id": "option-68",
                "text": "NameJ1UGQJ15JLY2EF      | Varchar   | PK |     ",
                "fieldName": "NameJ1UGQJ15JLY2EF",
                "dataType": "Varchar",
                "nullable": false,
                "primaryKey": true,
                "foreignKey": false
            },
            "id": "83a4fb31-d077-4d8c-ad1c-b26e28bb0002",
            "z": 2,
            "currentTextPath": "question",
            "attrs": {
                ".options": {
                    "refY": 45
                },
                ".question-text": {
                    "text": "Entity-GWYAIUK"
                },
                ".option-option-68": {
                    "transform": "translate(0, 0)",
                    "dynamic": true
                },
                ".option-option-68 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-68 .option-text": {
                    "text": "NameJ1UGQJ15JLY2EF      | Varchar   | PK |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-69": {
                    "transform": "translate(0, 30)",
                    "dynamic": true
                },
                ".option-option-69 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-69 .option-text": {
                    "text": "NameUEMPR1C2WVXL8Y      | INTEGER   | PK |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-70": {
                    "transform": "translate(0, 60)",
                    "dynamic": true
                },
                ".option-option-70 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-70 .option-text": {
                    "text": "NameUPASSUU1IIFKWB      | DATE      |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-71": {
                    "transform": "translate(0, 90)",
                    "dynamic": true
                },
                ".option-option-71 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-71 .option-text": {
                    "text": "NameEVJCWOI7GG9LGL      | Varchar   |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-72": {
                    "transform": "translate(0, 120)",
                    "dynamic": true
                },
                ".option-option-72 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-72 .option-text": {
                    "text": "NameUVPYCQ8LXHFF5T      | Varchar   |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-73": {
                    "transform": "translate(0, 150)",
                    "dynamic": true
                },
                ".option-option-73 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-73 .option-text": {
                    "text": "NameSMS3OG2OJQ9NZD      | Varchar   |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-74": {
                    "transform": "translate(0, 180)",
                    "dynamic": true
                },
                ".option-option-74 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-74 .option-text": {
                    "text": "NameKO6YLQJI6I9PB6      | Varchar   |    |     ",
                    "dynamic": true,
                    "refY": 15
                },
                ".option-option-75": {
                    "transform": "translate(0, 210)",
                    "dynamic": true
                },
                ".option-option-75 .option-rect": {
                    "height": 30,
                    "dynamic": true
                },
                ".option-option-75 .option-text": {
                    "text": "NameQMOZ4NOFJPBJ9Z      | Varchar   |    |     ",
                    "dynamic": true,
                    "refY": 15
                }
            }
        }, {
        "type": "OneToMany.Link",
        "optionHeight": 0,
        "questionHeight": 0,
        "paddingBottom": 0,
        "minWidth": 0,
        "angle": 0,
        "schema": "",
        "linkTable": false,
        "relationshipName": "",
        "source": {
            "id": "83a4fb31-d077-4d8c-ad1c-b26e28bb0002",
            "magnet": "circle",
            "port": "option-68"
        },
        "target": {
            "id": "3c173c52-c6f4-4800-896c-d3e6301edaa3",
            "magnet": "circle",
            "port": "inputPort_option-58"
        },
        "router": {
            "name": "manhattan"
        },
        "connector": {
            "name": "rounded"
        },
        "id": "334875f2-3d43-4e9e-b2f2-53770779c7eb",
        "z": 3,
        "attrs": {
            "line": {
                "sourceMarker": {
                    "stroke": "black",
                    "stroke-width": 2
                },
                "targetMarker": {
                    "stroke": "black",
                    "stroke-width": 2
                }
            }
        }
    }, {
        "type": "OneToMany.Link",
        "optionHeight": 0,
        "questionHeight": 0,
        "paddingBottom": 0,
        "minWidth": 0,
        "angle": 0,
        "schema": "",
        "linkTable": false,
        "source": {
            "id": "83a4fb31-d077-4d8c-ad1c-b26e28bb0002",
            "magnet": "circle",
            "port": "option-69"
        },
        "target": {
            "id": "3c173c52-c6f4-4800-896c-d3e6301edaa3",
            "magnet": "circle",
            "port": "inputPort_option-59"
        },
        "router": {
            "name": "manhattan"
        },
        "connector": {
            "name": "rounded"
        },
        "id": "2e6f8422-e72e-4897-ad42-9c1a4e9bdc34",
        "z": 4,
        "vertices": [{
            "x": 650,
            "y": 440
        }],
        "attrs": {
            "line": {
                "sourceMarker": {
                    "stroke": "black",
                    "stroke-width": 2
                },
                "targetMarker": {
                    "stroke": "black",
                    "stroke-width": 2
                }
            }
        }
    }]
}
