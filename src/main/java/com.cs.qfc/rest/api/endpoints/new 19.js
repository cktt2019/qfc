simple : {
    "cells"
:
    [{
        "type": "qad.Question",
        "optionHeight": 30,
        "questionHeight": 45,
        "paddingBottom": 30,
        "minWidth": 400,
        "ports": {
            "groups": {
                "in": {
                    "position": "left",
                    "attrs": {
                        "circle": {
                            "magnet": true,
                            "stroke": "black",
                            "fill": "transparent",
                            "r": 4
                        }
                    }
                },
                "out": {
                    "position": "right",
                    "attrs": {
                        "circle": {
                            "magnet": true,
                            "stroke": "black",
                            "fill": "transparent",
                            "r": 4
                        }
                    }
                }
            },
            "items": [{
                "group": "out",
                "id": "option-52",
                "args": {
                    "y": 60
                }
            }, {
                "group": "in",
                "id": "inputPort_option-52",
                "args": {
                    "y": 60
                }
            }, {
                "group": "out",
                "id": "option-53",
                "args": {
                    "y": 90
                }
            }, {
                "group": "in",
                "id": "inputPort_option-53",
                "args": {
                    "y": 90
                }
            }, {
                "group": "out",
                "id": "option-54",
                "args": {
                    "y": 120
                }
            }, {
                "group": "in",
                "id": "inputPort_option-54",
                "args": {
                    "y": 120
                }
            }, {
                "group": "out",
                "id": "option-55",
                "args": {
                    "y": 150
                }
            }, {
                "group": "in",
                "id": "inputPort_option-55",
                "args": {
                    "y": 150
                }
            }, {
                "group": "out",
                "id": "option-56",
                "args": {
                    "y": 180
                }
            }, {
                "group": "in",
                "id": "inputPort_option-56",
                "args": {
                    "y": 180
                }
            }, {
                "group": "out",
                "id": "option-57",
                "args": {
                    "y": 210
                }
            }, {
                "group": "in",
                "id": "inputPort_option-57",
                "args": {
                    "y": 210
                }
            }
            ]
        },
        "position": {
            "x": -140,
            "y": 160
        },
        "size": {
            "width": 400,
            "height": 255
        },
        "angle": 0,
        "question": "Entity-HYS2FJF",
        "inPorts": [{
            "id": "in",
            "label": "In"
        }
        ],
        "options": [{
            "id": "option-52",
            "text": "Edit .... ",
            "fieldName": "NameOHGAABOSWLCYSE",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-53",
            "text": "Edit .... ",
            "fieldName": "Name2HNZGRHXEARBSH",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-54",
            "text": "Edit .... ",
            "fieldName": "NameTRKBN7TZCSHBT8",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-55",
            "text": "Edit .... ",
            "fieldName": "NameVKLZZAI2LLY9CD",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-56",
            "text": "Edit .... ",
            "fieldName": "NameMPN327XURXWHZX",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-57",
            "text": "Edit .... ",
            "fieldName": "NameV2IWUNKBFAPJDU",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }
        ],
        "selectedAttribute": {},
        "id": "d94b3593-a964-410a-b423-53e01d5ad447",
        "z": 1,
        "attrs": {
            ".options": {
                "refY": 45
            },
            ".question-text": {
                "text": "Entity-HYS2FJF"
            },
            ".option-option-52": {
                "transform": "translate(0, 0)",
                "dynamic": true
            },
            ".option-option-52 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-52 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-53": {
                "transform": "translate(0, 30)",
                "dynamic": true
            },
            ".option-option-53 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-53 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-54": {
                "transform": "translate(0, 60)",
                "dynamic": true
            },
            ".option-option-54 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-54 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-55": {
                "transform": "translate(0, 90)",
                "dynamic": true
            },
            ".option-option-55 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-55 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-56": {
                "transform": "translate(0, 120)",
                "dynamic": true
            },
            ".option-option-56 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-56 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-57": {
                "transform": "translate(0, 150)",
                "dynamic": true
            },
            ".option-option-57 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-57 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            }
        }
    }, {
        "type": "qad.Question",
        "optionHeight": 30,
        "questionHeight": 45,
        "paddingBottom": 30,
        "minWidth": 400,
        "ports": {
            "groups": {
                "in": {
                    "position": "left",
                    "attrs": {
                        "circle": {
                            "magnet": true,
                            "stroke": "black",
                            "fill": "transparent",
                            "r": 4
                        }
                    }
                },
                "out": {
                    "position": "right",
                    "attrs": {
                        "circle": {
                            "magnet": true,
                            "stroke": "black",
                            "fill": "transparent",
                            "r": 4
                        }
                    }
                }
            },
            "items": [{
                "group": "out",
                "id": "option-66",
                "args": {
                    "y": 60
                }
            }, {
                "group": "in",
                "id": "inputPort_option-66",
                "args": {
                    "y": 60
                }
            }, {
                "group": "out",
                "id": "option-67",
                "args": {
                    "y": 90
                }
            }, {
                "group": "in",
                "id": "inputPort_option-67",
                "args": {
                    "y": 90
                }
            }, {
                "group": "out",
                "id": "option-68",
                "args": {
                    "y": 120
                }
            }, {
                "group": "in",
                "id": "inputPort_option-68",
                "args": {
                    "y": 120
                }
            }, {
                "group": "out",
                "id": "option-69",
                "args": {
                    "y": 150
                }
            }, {
                "group": "in",
                "id": "inputPort_option-69",
                "args": {
                    "y": 150
                }
            }, {
                "group": "out",
                "id": "option-70",
                "args": {
                    "y": 180
                }
            }, {
                "group": "in",
                "id": "inputPort_option-70",
                "args": {
                    "y": 180
                }
            }, {
                "group": "out",
                "id": "option-71",
                "args": {
                    "y": 210
                }
            }, {
                "group": "in",
                "id": "inputPort_option-71",
                "args": {
                    "y": 210
                }
            }, {
                "group": "out",
                "id": "option-72",
                "args": {
                    "y": 240
                }
            }, {
                "group": "in",
                "id": "inputPort_option-72",
                "args": {
                    "y": 240
                }
            }, {
                "group": "out",
                "id": "option-73",
                "args": {
                    "y": 270
                }
            }, {
                "group": "in",
                "id": "inputPort_option-73",
                "args": {
                    "y": 270
                }
            }, {
                "group": "out",
                "id": "option-74",
                "args": {
                    "y": 300
                }
            }, {
                "group": "in",
                "id": "inputPort_option-74",
                "args": {
                    "y": 300
                }
            }, {
                "group": "out",
                "id": "option-75",
                "args": {
                    "y": 330
                }
            }, {
                "group": "in",
                "id": "inputPort_option-75",
                "args": {
                    "y": 330
                }
            }, {
                "group": "out",
                "id": "option-76",
                "args": {
                    "y": 360
                }
            }, {
                "group": "in",
                "id": "inputPort_option-76",
                "args": {
                    "y": 360
                }
            }, {
                "group": "out",
                "id": "option-77",
                "args": {
                    "y": 390
                }
            }, {
                "group": "in",
                "id": "inputPort_option-77",
                "args": {
                    "y": 390
                }
            }
            ]
        },
        "position": {
            "x": 550,
            "y": 70
        },
        "size": {
            "width": 400,
            "height": 435
        },
        "angle": 0,
        "question": "Entity-0URRUQR",
        "inPorts": [{
            "id": "in",
            "label": "In"
        }
        ],
        "options": [{
            "id": "option-66",
            "text": "Edit .... ",
            "fieldName": "Name65JJUNF6MOLZ2C",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-67",
            "text": "Edit .... ",
            "fieldName": "NameIV6YENZCHWB2DX",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-68",
            "text": "Edit .... ",
            "fieldName": "NameHNXECI9ITXTQVH",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-69",
            "text": "Edit .... ",
            "fieldName": "NameIBWZDNOS7VCQGI",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-70",
            "text": "Edit .... ",
            "fieldName": "NameYAZZ3FJYZE0XLL",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-71",
            "text": "Edit .... ",
            "fieldName": "NameRFCUPFHLJCUQBE",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-72",
            "text": "Edit .... ",
            "fieldName": "NameIQAGNNF1ECHSJ7",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-73",
            "text": "Edit .... ",
            "fieldName": "NameFJUHSY1NOKHORN",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-74",
            "text": "Edit .... ",
            "fieldName": "NameIHRHSLAN6QLTFR",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-75",
            "text": "Edit .... ",
            "fieldName": "NameXE4PB2IMJLM76D",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-76",
            "text": "Edit .... ",
            "fieldName": "NameFHFTWTQ5TJ1HZ9",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }, {
            "id": "option-77",
            "text": "Edit .... ",
            "fieldName": "NameIZPREH2PN2G7SG",
            "dataType": "Varchar",
            "nullable": false,
            "primaryKey": false,
            "foreignKey": false
        }
        ],
        "selectedAttribute": {},
        "id": "c2a69598-1e46-478b-89da-215a663cd877",
        "z": 2,
        "attrs": {
            ".options": {
                "refY": 45
            },
            ".question-text": {
                "text": "Entity-0URRUQR"
            },
            ".option-option-66": {
                "transform": "translate(0, 0)",
                "dynamic": true
            },
            ".option-option-66 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-66 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-67": {
                "transform": "translate(0, 30)",
                "dynamic": true
            },
            ".option-option-67 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-67 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-68": {
                "transform": "translate(0, 60)",
                "dynamic": true
            },
            ".option-option-68 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-68 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-69": {
                "transform": "translate(0, 90)",
                "dynamic": true
            },
            ".option-option-69 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-69 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-70": {
                "transform": "translate(0, 120)",
                "dynamic": true
            },
            ".option-option-70 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-70 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-71": {
                "transform": "translate(0, 150)",
                "dynamic": true
            },
            ".option-option-71 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-71 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-72": {
                "transform": "translate(0, 180)",
                "dynamic": true
            },
            ".option-option-72 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-72 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-73": {
                "transform": "translate(0, 210)",
                "dynamic": true
            },
            ".option-option-73 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-73 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-74": {
                "transform": "translate(0, 240)",
                "dynamic": true
            },
            ".option-option-74 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-74 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-75": {
                "transform": "translate(0, 270)",
                "dynamic": true
            },
            ".option-option-75 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-75 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-76": {
                "transform": "translate(0, 300)",
                "dynamic": true
            },
            ".option-option-76 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-76 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            },
            ".option-option-77": {
                "transform": "translate(0, 330)",
                "dynamic": true
            },
            ".option-option-77 .option-rect": {
                "height": 30,
                "dynamic": true
            },
            ".option-option-77 .option-text": {
                "text": "Edit .... ",
                "dynamic": true,
                "refY": 15
            }
        }
    }, {
        "type": "OneToMany.Link",
        "source": {
            "id": "d94b3593-a964-410a-b423-53e01d5ad447",
            "magnet": "circle",
            "port": "option-52"
        },
        "target": {
            "id": "c2a69598-1e46-478b-89da-215a663cd877",
            "magnet": "circle",
            "port": "inputPort_option-69"
        },
        "router": {
            "name": "manhattan"
        },
        "connector": {
            "name": "rounded"
        },
        "id": "4bc7c674-5ca2-4b2b-9158-a99b1f63ac37",
        "z": 3,
        "attrs": {
            "line": {
                "sourceMarker": {
                    "stroke": "black",
                    "stroke-width": 2
                },
                "targetMarker": {
                    "d": "M13 0 a6 6 0 1 1 0 1 M 10 0 L 0 0 M 10 0 L 0 -10 M 10 0 L 0 10",
                    "stroke": "black",
                    "stroke-width": 3
                }
            }
        }
    }
    ]
}
