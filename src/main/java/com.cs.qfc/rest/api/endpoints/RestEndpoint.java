package com.cs.qfc.rest.api.endpoints;

import com.cs.qfc.data.converter.*;
import com.cs.qfc.data.jpa.entities.*;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.Party;
import com.cs.qfc.data.model.PrimeSwap;
import com.cs.qfc.data.model.*;
import com.cs.qfc.rest.api.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Rest enpoint for data retrieval and modifications
 */
@Controller
@Path("/api")
@Transactional
public class RestEndpoint {


    @Autowired
    DepartmentService departmentService;

    @Autowired
    EmployeeRoleService employeeRoleService;

    @Autowired
    PartyService partyService;

    @Autowired
    DTPMISGBLDYTREEService dTPMISGBLDYTREEService;


    @Autowired
    BookService bookService;

    @Autowired
    BusinessUnitService businessUnitService;


    @Autowired
    DebtConvertibleBondService debtConvertibleBondService;

    @Autowired
    EquityConvertibleBondService equityConvertibleBondService;




    @Autowired
    ClientHierarchyService clientHierarchyService;

    @Autowired
    PortfolioService portfolioService;


    @Autowired
    PorfolioRelationshipService porfolioRelationshipService;

    @Autowired
    CreditSuisseLegalEntityService creditSuisseLegalEntityService;

    @Autowired
    DebtInstrumentService debtInstrumentService;

    @Autowired
    EquityInstrumentService equityInstrumentService;


    @Autowired
    DrdTradeHeaderService drdTradeHeaderService;

    @Autowired
    DrdTradeUnderlyingService drdTradeUnderlyingService;

    @Autowired
    DrdTradeLegService drdTradeLegService;

    @Autowired
    DrdTradeInstrumentService drdTradeInstrumentService;

    @Autowired
    PrimeSwapService primeSwapService;

    @Autowired
    PrimeSwapPositionService primeSwapPositionService;

    @Autowired
    VisionService visionService;

    @Autowired
    LoanIQService loanIQService;

    @Autowired
    MarnaGService marnaGService;
    @Autowired
    CustomQueriesService customQueriesService;

    @Autowired
    NTPACustodyPositionService nTPACustodyPositionService;

    @Autowired
    NTPAMargin5GService nTPAMargin5GService;

    @Autowired
    MarnaGGovtLawService marnaGGovtLawService;
    @Autowired
    AgreementTitlesScopeService agreementTitlesScopeService;

    @Autowired
    CreditSuisseCorpEntityService creditSuisseCorpEntityService;

    @Autowired
    SpecificationService specificationService;

    @Autowired
    ObjectModelService objectModelService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/creditsuissecorpentitys")
    public List<CreditSuisseCorpEntityDTO> listCreditsuissecorpentitys() {

        return creditSuisseCorpEntityService.findAll().stream().map(e -> CreditSuisseCorpEntityToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    //Custom Search

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/creditsuissecorpentitys/custom")
    public List<CreditSuisseCorpEntityDTO> creditSuisseCorpEntityCustomSearch(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(creditSuisseCorpEntityService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/creditsuissecorpentitys")
    public Response createCreditSuisseCorpEntity(@RequestBody CreditSuisseCorpEntityDTO creditSuisseCorpEntity) {
        CreditSuisseCorpEntity createdCreditSuisseCorpEntity = creditSuisseCorpEntityService.saveCreditSuisseCorpEntity(creditSuisseCorpEntity);
        return Response.status(Response.Status.CREATED).header("Location", "/creditsuissecorpentitys/" + createdCreditSuisseCorpEntity.getCount()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/creditsuissecorpentitys/{count}")
    public CreditSuisseCorpEntityDTO getCreditSuisseCorpEntityById(@PathParam("count") Integer count) {

        CreditSuisseCorpEntity creditSuisseCorpEntity = creditSuisseCorpEntityService.findById(count);
        if (creditSuisseCorpEntity != null)
            return CreditSuisseCorpEntityToDTOConverter.convert(creditSuisseCorpEntity);
        throw new NotFoundException("No CreditSuisseCorpEntity with  " + " count = " + count + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/creditsuissecorpentitys/{count}")
    public Response updateCreditSuisseCorpEntityById(@RequestBody CreditSuisseCorpEntityDTO creditSuisseCorpEntity) {
        CreditSuisseCorpEntity existingCreditSuisseCorpEntity = creditSuisseCorpEntityService.findById(creditSuisseCorpEntity.getCount());
        if (existingCreditSuisseCorpEntity == null)
            throw new NotFoundException("No CreditSuisseCorpEntity with  " + " creditSuisseCorpEntity.getCount() = " + creditSuisseCorpEntity.getCount() + " exists");

        creditSuisseCorpEntityService.updateCreditSuisseCorpEntity(existingCreditSuisseCorpEntity, creditSuisseCorpEntity);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/creditsuissecorpentitys/{count}")
    public Response deleteCreditSuisseCorpEntityById(@PathParam("count") Integer count) {
        CreditSuisseCorpEntity existingCreditSuisseCorpEntity = creditSuisseCorpEntityService.findById(count);
        if (existingCreditSuisseCorpEntity == null)
            throw new NotFoundException("No CreditSuisseCorpEntity with  " + " count = " + count + " exists");

        creditSuisseCorpEntityService.deleteById(count);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/agreementtitlesscopes")
    public List<AgreementTitlesScopeDTO> listAgreementtitlesscopes() {

        return agreementTitlesScopeService.findAll().stream().map(e -> AgreementTitlesScopeToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/agreementtitlesscopes/{count}")
    public Response updateAgreementTitlesScopeById(@RequestBody AgreementTitlesScopeDTO agreementTitlesScope) {
        AgreementTitlesScope existingAgreementTitlesScope = agreementTitlesScopeService.findById(agreementTitlesScope.getCount());
        if (existingAgreementTitlesScope == null)
            throw new NotFoundException("No AgreementTitlesScope with  " + " agreementTitlesScope.getCount() = " + agreementTitlesScope.getCount() + " exists");

        agreementTitlesScopeService.updateAgreementTitlesScope(existingAgreementTitlesScope, agreementTitlesScope);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/agreementtitlesscopes/{count}")
    public Response deleteAgreementTitlesScopeById(@PathParam("count") Integer count) {
        AgreementTitlesScope existingAgreementTitlesScope = agreementTitlesScopeService.findById(count);
        if (existingAgreementTitlesScope == null)
            throw new NotFoundException("No AgreementTitlesScope with  " + " count = " + count + " exists");

        agreementTitlesScopeService.deleteById(count);

        return Response.status(Response.Status.OK).build();

    }


    //Custom Search

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/agreementtitlesscopes/custom")
    public List<AgreementTitlesScopeDTO> agreementTitlesScopeCustomSearch(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(agreementTitlesScopeService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/agreementtitlesscopes")
    public Response createAgreementTitlesScope(@RequestBody AgreementTitlesScopeDTO agreementTitlesScope) {
        AgreementTitlesScope createdAgreementTitlesScope = agreementTitlesScopeService.saveAgreementTitlesScope(agreementTitlesScope);
        return Response.status(Response.Status.CREATED).header("Location", "/agreementtitlesscopes/" + createdAgreementTitlesScope.getCount()).build();

    }


    @Path("/ntpamargin5gs")
    public List<NTPAMargin5GDTO> listNtpamargin5gs() {

        return nTPAMargin5GService.findAll().stream().map(e -> NTPAMargin5GToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/ntpamargin5gs")
    public Response createNTPAMargin5G(@RequestBody NTPAMargin5GDTO nTPAMargin5G) {
        NTPAMargin5G createdNTPAMargin5G = nTPAMargin5GService.saveNTPAMargin5G(nTPAMargin5G);
        return Response.status(Response.Status.CREATED).header("Location", "/ntpamargin5gs/" + createdNTPAMargin5G.getId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ntpamargin5gs/{id}")
    public NTPAMargin5GDTO getNTPAMargin5GById(@PathParam("id") Integer id) {

        NTPAMargin5G nTPAMargin5G = nTPAMargin5GService.findById(id);
        if (nTPAMargin5G != null)
            return NTPAMargin5GToDTOConverter.convert(nTPAMargin5G);
        throw new NotFoundException("No NTPAMargin5G with  " + " id = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/ntpamargin5gs/{id}")
    public Response updateNTPAMargin5GById(@RequestBody NTPAMargin5GDTO nTPAMargin5G) {
        NTPAMargin5G existingNTPAMargin5G = nTPAMargin5GService.findById(nTPAMargin5G.getId());
        if (existingNTPAMargin5G == null)
            throw new NotFoundException("No NTPAMargin5G with  " + " nTPAMargin5G.getId() = " + nTPAMargin5G.getId() + " exists");

        nTPAMargin5GService.updateNTPAMargin5G(existingNTPAMargin5G, nTPAMargin5G);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/ntpamargin5gs/{id}")
    public Response deleteNTPAMargin5GById(@PathParam("id") Integer id) {
        NTPAMargin5G existingNTPAMargin5G = nTPAMargin5GService.findById(id);
        if (existingNTPAMargin5G == null)
            throw new NotFoundException("No NTPAMargin5G with  " + " id = " + id + " exists");

        nTPAMargin5GService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/marnaggovtlaws/{id}")
    public MarnaGGovtLawDTO getMarnaGGovtLawById(@PathParam("id") Integer id) {

        MarnaGGovtLaw marnaGGovtLaw = marnaGGovtLawService.findById(id);
        if (marnaGGovtLaw != null)
            return MarnaGGovtLawToDTOConverter.convert(marnaGGovtLaw);
        throw new NotFoundException("No MarnaGGovtLaw with  " + " id = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/marnaggovtlaws/{id}")
    public Response updateMarnaGGovtLawById(@RequestBody MarnaGGovtLawDTO marnaGGovtLaw) {
        MarnaGGovtLaw existingMarnaGGovtLaw = marnaGGovtLawService.findById(marnaGGovtLaw.getId());
        if (existingMarnaGGovtLaw == null)
            throw new NotFoundException("No MarnaGGovtLaw with  " + " marnaGGovtLaw.getId() = " + marnaGGovtLaw.getId() + " exists");

        marnaGGovtLawService.updateMarnaGGovtLaw(existingMarnaGGovtLaw, marnaGGovtLaw);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/marnaggovtlaws/{id}")
    public Response deleteMarnaGGovtLawById(@PathParam("id") Integer id) {
        MarnaGGovtLaw existingMarnaGGovtLaw = marnaGGovtLawService.findById(id);
        if (existingMarnaGGovtLaw == null)
            throw new NotFoundException("No MarnaGGovtLaw with  " + " id = " + id + " exists");

        marnaGGovtLawService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ntpacustodypositions")
    public List<NTPACustodyPositionDTO> listNtpacustodypositions() {

        return nTPACustodyPositionService.findAll().stream().map(e -> NTPACustodyPositionToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/ntpacustodypositions")
    public Response createNTPACustodyPosition(@RequestBody NTPACustodyPositionDTO nTPACustodyPosition) {
        NTPACustodyPosition createdNTPACustodyPosition = nTPACustodyPositionService.saveNTPACustodyPosition(nTPACustodyPosition);
        return Response.status(Response.Status.CREATED).header("Location", "/ntpacustodypositions/" + createdNTPACustodyPosition.getId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/marnaggovtlaws")
    public List<MarnaGGovtLawDTO> listMarnaggovtlaws() {

        return marnaGGovtLawService.findAll().stream().map(e -> MarnaGGovtLawToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/marnaggovtlaws")
    public Response createMarnaGGovtLaw(@RequestBody MarnaGGovtLawDTO marnaGGovtLaw) {
        MarnaGGovtLaw createdMarnaGGovtLaw = marnaGGovtLawService.saveMarnaGGovtLaw(marnaGGovtLaw);
        return Response.status(Response.Status.CREATED).header("Location", "/marnaggovtlaws/" + createdMarnaGGovtLaw.getId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ntpacustodypositions/{id}")
    public NTPACustodyPositionDTO getNTPACustodyPositionById(@PathParam("id") Integer id) {

        NTPACustodyPosition nTPACustodyPosition = nTPACustodyPositionService.findById(id);
        if (nTPACustodyPosition != null)
            return NTPACustodyPositionToDTOConverter.convert(nTPACustodyPosition);
        throw new NotFoundException("No NTPACustodyPosition with  " + " id = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/ntpacustodypositions/{id}")
    public Response updateNTPACustodyPositionById(@RequestBody NTPACustodyPositionDTO nTPACustodyPosition) {
        NTPACustodyPosition existingNTPACustodyPosition = nTPACustodyPositionService.findById(nTPACustodyPosition.getId());
        if (existingNTPACustodyPosition == null)
            throw new NotFoundException("No NTPACustodyPosition with  " + " nTPACustodyPosition.getId() = " + nTPACustodyPosition.getId() + " exists");

        nTPACustodyPositionService.updateNTPACustodyPosition(existingNTPACustodyPosition, nTPACustodyPosition);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/ntpacustodypositions/{id}")
    public Response deleteNTPACustodyPositionById(@PathParam("id") Integer id) {
        NTPACustodyPosition existingNTPACustodyPosition = nTPACustodyPositionService.findById(id);
        if (existingNTPACustodyPosition == null)
            throw new NotFoundException("No NTPACustodyPosition with  " + " id = " + id + " exists");

        nTPACustodyPositionService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/portfolios/{portfolioId}")
    public PortfolioDTO getPortfolioById(@PathParam("portfolioId") String portfolioId) {

        Portfolio portfolio = portfolioService.findById(portfolioId);
        if (portfolio != null)
            return PortfolioToDTOConverter.convert(portfolio);
        throw new NotFoundException("No Portfolio with  " + " portfolioId = " + portfolioId + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/portfolios/{portfolioId}")
    public Response updatePortfolioById(@RequestBody PortfolioDTO portfolio) {
        Portfolio existingPortfolio = portfolioService.findById(portfolio.getPortfolioId());
        if (existingPortfolio == null)
            throw new NotFoundException("No Portfolio with  " + " portfolio.getPortfolioId() = " + portfolio.getPortfolioId() + " exists");

        portfolioService.updatePortfolio(existingPortfolio, portfolio);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/portfolios/{portfolioId}")
    public Response deletePortfolioById(@PathParam("portfolioId") String portfolioId) {
        Portfolio existingPortfolio = portfolioService.findById(portfolioId);
        if (existingPortfolio == null)
            throw new NotFoundException("No Portfolio with  " + " portfolioId = " + portfolioId + " exists");

        portfolioService.deleteById(portfolioId);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/portfolios")
    public List<PortfolioDTO> listPortfolios() {

        return portfolioService.findAll().stream().map(e -> PortfolioToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/portfolios")
    public Response createPortfolio(@RequestBody PortfolioDTO portfolio) {
        Portfolio createdPortfolio = portfolioService.savePortfolio(portfolio);
        return Response.status(Response.Status.CREATED).header("Location", "/portfolios/" + createdPortfolio.getPortfolioId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/equityconvertiblebonds/{cusipId}")
    public EquityConvertibleBondDTO getEquityConvertibleBondById(@PathParam("cusipId") String cusipId) {

        EquityConvertibleBond equityConvertibleBond = equityConvertibleBondService.findById(cusipId);
        if (equityConvertibleBond != null)
            return EquityConvertibleBondToDTOConverter.convert(equityConvertibleBond);
        throw new NotFoundException("No EquityConvertibleBond with  " + " cusipId = " + cusipId + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/equityconvertiblebonds/{cusipId}")
    public Response updateEquityConvertibleBondById(@RequestBody EquityConvertibleBondDTO equityConvertibleBond) {
        EquityConvertibleBond existingEquityConvertibleBond = equityConvertibleBondService.findById(equityConvertibleBond.getCusipId());
        if (existingEquityConvertibleBond == null)
            throw new NotFoundException("No EquityConvertibleBond with  " + " equityConvertibleBond.getCusipId() = " + equityConvertibleBond.getCusipId() + " exists");

        equityConvertibleBondService.updateEquityConvertibleBond(existingEquityConvertibleBond, equityConvertibleBond);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/equityconvertiblebonds/{cusipId}")
    public Response deleteEquityConvertibleBondById(@PathParam("cusipId") String cusipId) {
        EquityConvertibleBond existingEquityConvertibleBond = equityConvertibleBondService.findById(cusipId);
        if (existingEquityConvertibleBond == null)
            throw new NotFoundException("No EquityConvertibleBond with  " + " cusipId = " + cusipId + " exists");

        equityConvertibleBondService.deleteById(cusipId);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/equityconvertiblebonds")
    public List<EquityConvertibleBondDTO> listEquityconvertiblebonds() {

        return equityConvertibleBondService.findAll().stream().map(e -> EquityConvertibleBondToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/equityconvertiblebonds")
    public Response createEquityConvertibleBond(@RequestBody EquityConvertibleBondDTO equityConvertibleBond) {
        EquityConvertibleBond createdEquityConvertibleBond = equityConvertibleBondService.saveEquityConvertibleBond(equityConvertibleBond);
        return Response.status(Response.Status.CREATED).header("Location", "/equityconvertiblebonds/" + createdEquityConvertibleBond.getCusipId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/departments")
    public List<DepartmentDTO> listDepartments() {

        return departmentService.findAll().stream().map(e -> DepartmentToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/departments")
    public Response createDepartment(@RequestBody DepartmentDTO department) {
        Department createdDepartment = departmentService.saveDepartment(department);
        return Response.status(Response.Status.CREATED).header("Location", "/departments/" + createdDepartment.getDepartmentId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/departments/{departmentId}")
    public DepartmentDTO getDepartmentById(@PathParam("departmentId") String departmentId) {

        Department department = departmentService.findById(departmentId);
        if (department != null)
            return DepartmentToDTOConverter.convert(department);
        throw new NotFoundException("No Department with  " + " departmentId = " + departmentId + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/departments/{departmentId}")
    public Response updateDepartmentById(@RequestBody DepartmentDTO department) {
        Department existingDepartment = departmentService.findById(department.getDepartmentId());
        if (existingDepartment == null)
            throw new NotFoundException("No Department with  " + " department.getDepartmentId() = " + department.getDepartmentId() + " exists");

        departmentService.updateDepartment(existingDepartment, department);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/departments/{departmentId}")
    public Response deleteDepartmentById(@PathParam("departmentId") String departmentId) {
        Department existingDepartment = departmentService.findById(departmentId);
        if (existingDepartment == null)
            throw new NotFoundException("No Department with  " + " departmentId = " + departmentId + " exists");

        departmentService.deleteById(departmentId);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/partys")
    public List<Party> listPartys() {

        return partyService.findAll().stream().map(e -> PartyToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/partys")
    public Response createParty(@RequestBody Party party) {
        com.cs.qfc.data.jpa.entities.Party createdParty = partyService.saveParty(party);
        return Response.status(Response.Status.CREATED).header("Location", "/partys/" + createdParty.getGlobalPartyId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/partys/{globalPartyId}")
    public Party getPartyById(@PathParam("globalPartyId") Long globalPartyId) {

        com.cs.qfc.data.jpa.entities.Party party = partyService.findById(globalPartyId);
        if (party != null)
            return PartyToDTOConverter.convert(party);
        throw new NotFoundException("No Party with  " + " globalPartyId = " + globalPartyId + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/partys/{globalPartyId}")
    public Response updatePartyById(@RequestBody Party party) {
        com.cs.qfc.data.jpa.entities.Party existingParty = partyService.findById(party.getGlobalPartyId());
        if (existingParty == null)
            throw new NotFoundException("No Party with  " + " party.getGlobalPartyId() = " + party.getGlobalPartyId() + " exists");

        partyService.updateParty(existingParty, party);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/partys/{globalPartyId}")
    public Response deletePartyById(@PathParam("globalPartyId") Long globalPartyId) {
        com.cs.qfc.data.jpa.entities.Party existingParty = partyService.findById(globalPartyId);
        if (existingParty == null)
            throw new NotFoundException("No Party with  " + " globalPartyId = " + globalPartyId + " exists");

        partyService.deleteById(globalPartyId);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/debtconvertiblebonds")
    public List<DebtConvertibleBondDTO> listDebtconvertiblebonds() {

        return debtConvertibleBondService.findAll().stream().map(e -> DebtConvertibleBondToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/debtconvertiblebonds")
    public Response createDebtConvertibleBond(@RequestBody DebtConvertibleBondDTO debtConvertibleBond) {
        DebtConvertibleBond createdDebtConvertibleBond = debtConvertibleBondService.saveDebtConvertibleBond(debtConvertibleBond);
        return Response.status(Response.Status.CREATED).header("Location", "/debtconvertiblebonds/" + createdDebtConvertibleBond.getSedolId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/debtconvertiblebonds/{sedolId}")
    public DebtConvertibleBondDTO getDebtConvertibleBondById(@PathParam("sedolId") String sedolId) {

        DebtConvertibleBond debtConvertibleBond = debtConvertibleBondService.findById(sedolId);
        if (debtConvertibleBond != null)
            return DebtConvertibleBondToDTOConverter.convert(debtConvertibleBond);
        throw new NotFoundException("No DebtConvertibleBond with  " + " sedolId = " + sedolId + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/debtconvertiblebonds/{sedolId}")
    public Response updateDebtConvertibleBondById(@RequestBody DebtConvertibleBondDTO debtConvertibleBond) {
        DebtConvertibleBond existingDebtConvertibleBond = debtConvertibleBondService.findById(debtConvertibleBond.getSedolId());
        if (existingDebtConvertibleBond == null)
            throw new NotFoundException("No DebtConvertibleBond with  " + " debtConvertibleBond.getSedolId() = " + debtConvertibleBond.getSedolId() + " exists");

        debtConvertibleBondService.updateDebtConvertibleBond(existingDebtConvertibleBond, debtConvertibleBond);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/debtconvertiblebonds/{sedolId}")
    public Response deleteDebtConvertibleBondById(@PathParam("sedolId") String sedolId) {
        DebtConvertibleBond existingDebtConvertibleBond = debtConvertibleBondService.findById(sedolId);
        if (existingDebtConvertibleBond == null)
            throw new NotFoundException("No DebtConvertibleBond with  " + " sedolId = " + sedolId + " exists");

        debtConvertibleBondService.deleteById(sedolId);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/employeeroles/{employeeId}")
    public EmployeeRoleDTO getEmployeeRoleById(@PathParam("employeeId") String employeeId) {

        EmployeeRole employeeRole = employeeRoleService.findById(employeeId);
        if (employeeRole != null)
            return EmployeeRoleToDTOConverter.convert(employeeRole);
        throw new NotFoundException("No EmployeeRole with  " + " employeeId = " + employeeId + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/employeeroles/{employeeId}")
    public Response updateEmployeeRoleById(@RequestBody EmployeeRoleDTO employeeRole) {
        EmployeeRole existingEmployeeRole = employeeRoleService.findById(employeeRole.getEmployeeId());
        if (existingEmployeeRole == null)
            throw new NotFoundException("No EmployeeRole with  " + " employeeRole.getEmployeeId() = " + employeeRole.getEmployeeId() + " exists");

        employeeRoleService.updateEmployeeRole(existingEmployeeRole, employeeRole);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/employeeroles/{employeeId}")
    public Response deleteEmployeeRoleById(@PathParam("employeeId") String employeeId) {
        EmployeeRole existingEmployeeRole = employeeRoleService.findById(employeeId);
        if (existingEmployeeRole == null)
            throw new NotFoundException("No EmployeeRole with  " + " employeeId = " + employeeId + " exists");

        employeeRoleService.deleteById(employeeId);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/employeeroles")
    public List<EmployeeRoleDTO> listEmployeeroles() {

        return employeeRoleService.findAll().stream().map(e -> EmployeeRoleToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/employeeroles")
    public Response createEmployeeRole(@RequestBody EmployeeRoleDTO employeeRole) {
        EmployeeRole createdEmployeeRole = employeeRoleService.saveEmployeeRole(employeeRole);
        return Response.status(Response.Status.CREATED).header("Location", "/employeeroles/" + createdEmployeeRole.getEmployeeId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/debtinstruments")
    public List<DebtInstrumentDTO> listDebtinstruments() {

        return debtInstrumentService.findAll().stream().map(e -> DebtInstrumentToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/debtinstruments")
    public Response createDebtInstrument(@RequestBody DebtInstrumentDTO debtInstrument) {
        DebtInstrument createdDebtInstrument = debtInstrumentService.saveDebtInstrument(debtInstrument);
        return Response.status(Response.Status.CREATED).header("Location", "/debtinstruments/" + createdDebtInstrument.getGpi_issue()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/debtinstruments/{id}")
    public DebtInstrumentDTO getDebtInstrumentById(@PathParam("id") String id) {

        DebtInstrument debtInstrument = debtInstrumentService.findById(id);
        if (debtInstrument != null)
            return DebtInstrumentToDTOConverter.convert(debtInstrument);
        throw new NotFoundException("No DebtInstrument with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/debtinstruments/{id}")
    public Response updateDebtInstrumentById(@RequestBody DebtInstrumentDTO debtInstrument) {
        DebtInstrument existingDebtInstrument = debtInstrumentService.findById(debtInstrument.getGpiIssue());
        if (existingDebtInstrument == null)
            throw new NotFoundException("No DebtInstrument with  " + " debtInstrument.getGpiIssue() = " + debtInstrument.getGpiIssue() + " exists");

        debtInstrumentService.updateDebtInstrument(existingDebtInstrument, debtInstrument);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/debtinstruments/{id}")
    public Response deleteDebtInstrumentById(@PathParam("id") String id) {
        DebtInstrument existingDebtInstrument = debtInstrumentService.findById(id);
        if (existingDebtInstrument == null)
            throw new NotFoundException("No DebtInstrument with  " + " gpiIssue = " + id + " exists");

        debtInstrumentService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/equityinstruments/{id}")
    public EquityInstrumentDTO getEquityInstrumentById(@PathParam("id") String id) {

        EquityInstrument equityInstrument = equityInstrumentService.findById(id);
        if (equityInstrument != null)
            return EquityInstrumentToDTOConverter.convert(equityInstrument);
        throw new NotFoundException("No EquityInstrument with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/equityinstruments/{id}")
    public Response updateEquityInstrumentById(@RequestBody EquityInstrumentDTO equityInstrument) {
        EquityInstrument existingEquityInstrument = equityInstrumentService.findById(equityInstrument.getGpiIssue());
        if (existingEquityInstrument == null)
            throw new NotFoundException("No EquityInstrument with  " + " equityInstrument.getGpiIssue() = " + equityInstrument.getGpiIssue() + " exists");

        equityInstrumentService.updateEquityInstrument(existingEquityInstrument, equityInstrument);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/equityinstruments/{id}")
    public Response deleteEquityInstrumentById(@PathParam("id") String id) {
        EquityInstrument existingEquityInstrument = equityInstrumentService.findById(id);
        if (existingEquityInstrument == null)
            throw new NotFoundException("No EquityInstrument with  " + " gpiIssue = " + id + " exists");

        equityInstrumentService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/equityinstruments")
    public List<EquityInstrumentDTO> listEquityinstruments() {

        return equityInstrumentService.findAll().stream().map(e -> EquityInstrumentToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/equityinstruments")
    public Response createEquityInstrument(@RequestBody EquityInstrumentDTO equityInstrument) {
        EquityInstrument createdEquityInstrument = equityInstrumentService.saveEquityInstrument(equityInstrument);
        return Response.status(Response.Status.CREATED).header("Location", "/equityinstruments/" + createdEquityInstrument.getGpiIssue()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/clienthierarchys")
    public List<ClientHierarchyDTO> listClienthierarchys() {

        return clientHierarchyService.findAll().stream().map(e -> ClientHierarchyToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clienthierarchys")
    public Response createClientHierarchy(@RequestBody ClientHierarchyDTO clientHierarchy) {
        ClientHierarchy createdClientHierarchy = clientHierarchyService.saveClientHierarchy(clientHierarchy);
        return Response.status(Response.Status.CREATED).header("Location", "/clienthierarchys/" + createdClientHierarchy.getLevel()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/clienthierarchys/{id}")
    public ClientHierarchyDTO getClientHierarchyById(@PathParam("id") Integer id) {

        ClientHierarchy clientHierarchy = clientHierarchyService.findById(id);
        if (clientHierarchy != null)
            return ClientHierarchyToDTOConverter.convert(clientHierarchy);
        throw new NotFoundException("No ClientHierarchy with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clienthierarchys/{id}")
    public Response updateClientHierarchyById(@RequestBody ClientHierarchyDTO clientHierarchy) {
        ClientHierarchy existingClientHierarchy = clientHierarchyService.findById(Integer.valueOf(clientHierarchy.getLevel()));
        if (existingClientHierarchy == null)
            throw new NotFoundException("No ClientHierarchy with  " + " clientHierarchy.getGpiIssue() = " + clientHierarchy.getLevel() + " exists");

        clientHierarchyService.updateClientHierarchy(existingClientHierarchy, clientHierarchy);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clienthierarchys/{id}")
    public Response deleteClientHierarchyById(@PathParam("id") Integer id) {
        ClientHierarchy existingClientHierarchy = clientHierarchyService.findById(id);
        if (existingClientHierarchy == null)
            throw new NotFoundException("No ClientHierarchy with  " + " gpiIssue = " + id + " exists");

        clientHierarchyService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/dtpmisgbldytrees")
    public List<DTPMISGBLDYTREEDTO> listDtpmisgbldytrees() {

        return dTPMISGBLDYTREEService.findAll().stream().map(e -> DTPMISGBLDYTREEToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/dtpmisgbldytrees")
    public Response createDTPMISGBLDYTREE(@RequestBody DTPMISGBLDYTREEDTO dTPMISGBLDYTREE) {
        DTPMISGBLDYTREE createdDTPMISGBLDYTREE = dTPMISGBLDYTREEService.saveDTPMISGBLDYTREE(dTPMISGBLDYTREE);
        return Response.status(Response.Status.CREATED).header("Location", "/dtpmisgbldytrees/" + createdDTPMISGBLDYTREE.getMisMemberCode()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/dtpmisgbldytrees/{misMemberCode}")
    public DTPMISGBLDYTREEDTO getDTPMISGBLDYTREEById(@PathParam("misMemberCode") String misMemberCode) {

        DTPMISGBLDYTREE dTPMISGBLDYTREE = dTPMISGBLDYTREEService.findById(misMemberCode);
        if (dTPMISGBLDYTREE != null)
            return DTPMISGBLDYTREEToDTOConverter.convert(dTPMISGBLDYTREE);
        throw new NotFoundException("No DTPMISGBLDYTREE with  " + " misMemberCode = " + misMemberCode + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/dtpmisgbldytrees/{misMemberCode}")
    public Response updateDTPMISGBLDYTREEById(@RequestBody DTPMISGBLDYTREEDTO dTPMISGBLDYTREE) {
        DTPMISGBLDYTREE existingDTPMISGBLDYTREE = dTPMISGBLDYTREEService.findById(dTPMISGBLDYTREE.getMisMemberCode());
        if (existingDTPMISGBLDYTREE == null)
            throw new NotFoundException("No DTPMISGBLDYTREE with  " + " dTPMISGBLDYTREE.getMisMemberCode() = " + dTPMISGBLDYTREE.getMisMemberCode() + " exists");

        dTPMISGBLDYTREEService.updateDTPMISGBLDYTREE(existingDTPMISGBLDYTREE, dTPMISGBLDYTREE);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/dtpmisgbldytrees/{misMemberCode}")
    public Response deleteDTPMISGBLDYTREEById(@PathParam("misMemberCode") String misMemberCode) {
        DTPMISGBLDYTREE existingDTPMISGBLDYTREE = dTPMISGBLDYTREEService.findById(misMemberCode);
        if (existingDTPMISGBLDYTREE == null)
            throw new NotFoundException("No DTPMISGBLDYTREE with  " + " misMemberCode = " + misMemberCode + " exists");

        dTPMISGBLDYTREEService.deleteById(misMemberCode);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/businessunits")
    public List<BusinessUnitDTO> listBusinessunits() {

        return businessUnitService.findAll().stream().map(e -> BusinessUnitToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/businessunits")
    public Response createBusinessUnit(@RequestBody BusinessUnitDTO businessUnit) {
        BusinessUnit createdBusinessUnit = businessUnitService.saveBusinessUnit(businessUnit);
        return Response.status(Response.Status.CREATED).header("Location", "/businessunits/" + createdBusinessUnit.getFinancialBusinessUnitCode() + "/" + createdBusinessUnit.getBusinessUnitId()).build();

    }

   /* @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/businessunits/custom")
    public List<BusinessUnitDTO>  customSearch(@RequestBody SearchInfoDTO searchInfo) {
        System.out.println("received custom request "+ searchInfo);
        return new ArrayList<>(businessUnitService.findAllCustom(searchInfo));

    }*/

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/businessunits/{financialBusinessUnitCode}/{businessUnitId}")
    public BusinessUnitDTO getBusinessUnitById(@PathParam("financialBusinessUnitCode") String financialBusinessUnitCode, @PathParam("businessUnitId") String businessUnitId) {

        BusinessUnit businessUnit = businessUnitService.findById(financialBusinessUnitCode, businessUnitId);
        if (businessUnit != null)
            return BusinessUnitToDTOConverter.convert(businessUnit);
        throw new NotFoundException("No BusinessUnit with  " + " financialBusinessUnitCode = " + financialBusinessUnitCode + " businessUnitId = " + businessUnitId + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/businessunits/{financialBusinessUnitCode}/{businessUnitId}")
    public Response updateBusinessUnitById(@RequestBody BusinessUnitDTO businessUnit) {
        BusinessUnit existingBusinessUnit = businessUnitService.findById(businessUnit.getFinancialBusinessUnitCode(), businessUnit.getBusinessUnitId());
        if (existingBusinessUnit == null)
            throw new NotFoundException("No BusinessUnit with  " + " businessUnit.getFinancialBusinessUnitCode() = " + businessUnit.getFinancialBusinessUnitCode() + " businessUnit.getBusinessUnitId() = " + businessUnit.getBusinessUnitId() + " exists");

        businessUnitService.updateBusinessUnit(existingBusinessUnit, businessUnit);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/businessunits/{financialBusinessUnitCode}/{businessUnitId}")
    public Response deleteBusinessUnitById(@PathParam("financialBusinessUnitCode") String financialBusinessUnitCode, @PathParam("businessUnitId") String businessUnitId) {
        BusinessUnit existingBusinessUnit = businessUnitService.findById(financialBusinessUnitCode, businessUnitId);
        if (existingBusinessUnit == null)
            throw new NotFoundException("No BusinessUnit with  " + " financialBusinessUnitCode = " + financialBusinessUnitCode + " businessUnitId = " + businessUnitId + " exists");

        businessUnitService.deleteById(financialBusinessUnitCode, businessUnitId);

        return Response.status(Response.Status.OK).build();

    }

/*
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/clientleis/{id}")
    public ClientLeiDTO getClientLeiById(@PathParam("id") Integer id) {

        ClientLei clientLei = clientLeiService.findById(id);
        if (clientLei != null)
            return ClientLeiToDTOConverter.convert(clientLei);
        throw new NotFoundException("No ClientLei with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clientleis/{id}")
    public Response updateClientLeiById(@RequestBody ClientLeiDTO clientLei) {
        ClientLei existingClientLei = clientLeiService.findById(clientLei.getId());
        if (existingClientLei == null)
            throw new NotFoundException("No ClientLei with  " + " clientLei.getGpiIssue() = " + clientLei.getId() + " exists");

        clientLeiService.updateClientLei(existingClientLei, clientLei);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clientleis/{id}")
    public Response deleteClientLeiById(@PathParam("id") Integer id) {
        ClientLei existingClientLei = clientLeiService.findById(id);
        if (existingClientLei == null)
            throw new NotFoundException("No ClientLei with  " + " gpiIssue = " + id + " exists");

        clientLeiService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/clientleis")
    public List<ClientLeiDTO> listClientleis() {

        return clientLeiService.findAll().stream().map(e -> ClientLeiToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clientleis")
    public Response createClientLei(@RequestBody ClientLeiDTO clientLei) {
        ClientLei createdClientLei = clientLeiService.saveClientLei(clientLei);
        return Response.status(Response.Status.CREATED).header("Location", "/clientleis/" + createdClientLei.getId()).build();

    }
    */


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/porfoliorelationships")
    public List<PorfolioRelationshipDTO> listPorfoliorelationships() {

        return porfolioRelationshipService.findAll().stream().map(e -> PorfolioRelationshipToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/porfoliorelationships")
    public Response createPorfolioRelationship(@RequestBody PorfolioRelationshipDTO porfolioRelationship) {
        PorfolioRelationship createdPorfolioRelationship = porfolioRelationshipService.savePorfolioRelationship(porfolioRelationship);
        return Response.status(Response.Status.CREATED).header("Location", "/porfoliorelationships/" + createdPorfolioRelationship.getPortfolioId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/porfoliorelationships/{portfolioId}")
    public PorfolioRelationshipDTO getPorfolioRelationshipById(@PathParam("portfolioId") String portfolioId) {

        PorfolioRelationship porfolioRelationship = porfolioRelationshipService.findById(portfolioId);
        if (porfolioRelationship != null)
            return PorfolioRelationshipToDTOConverter.convert(porfolioRelationship);
        throw new NotFoundException("No PorfolioRelationship with  " + " portfolioId = " + portfolioId + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/porfoliorelationships/{portfolioId}")
    public Response updatePorfolioRelationshipById(@RequestBody PorfolioRelationshipDTO porfolioRelationship) {
        PorfolioRelationship existingPorfolioRelationship = porfolioRelationshipService.findById(porfolioRelationship.getPortfolioId());
        if (existingPorfolioRelationship == null)
            throw new NotFoundException("No PorfolioRelationship with  " + " porfolioRelationship.getPortfolioId() = " + porfolioRelationship.getPortfolioId() + " exists");

        porfolioRelationshipService.updatePorfolioRelationship(existingPorfolioRelationship, porfolioRelationship);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/porfoliorelationships/{portfolioId}")
    public Response deletePorfolioRelationshipById(@PathParam("portfolioId") String portfolioId) {
        PorfolioRelationship existingPorfolioRelationship = porfolioRelationshipService.findById(portfolioId);
        if (existingPorfolioRelationship == null)
            throw new NotFoundException("No PorfolioRelationship with  " + " portfolioId = " + portfolioId + " exists");

        porfolioRelationshipService.deleteById(portfolioId);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/books/{globalBookId}")
    public BookDTO getBookById(@PathParam("globalBookId") String globalBookId) {

        Book book = bookService.findById(globalBookId);
        if (book != null)
            return BookToDTOConverter.convert(book);
        throw new NotFoundException("No Book with  " + " globalBookId = " + globalBookId + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/books/{globalBookId}")
    public Response updateBookById(@RequestBody BookDTO book) {
        Book existingBook = bookService.findById(book.getGlobalBookId());
        if (existingBook == null)
            throw new NotFoundException("No Book with  " + " book.getGlobalBookId() = " + book.getGlobalBookId() + " exists");

        bookService.updateBook(existingBook, book);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/books/{globalBookId}")
    public Response deleteBookById(@PathParam("globalBookId") String globalBookId) {
        Book existingBook = bookService.findById(globalBookId);
        if (existingBook == null)
            throw new NotFoundException("No Book with  " + " globalBookId = " + globalBookId + " exists");

        bookService.deleteById(globalBookId);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/books")
    public List<BookDTO> listBooks() {

        return bookService.findAll().stream().map(e -> BookToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/books")
    public Response createBook(@RequestBody BookDTO book) {
        Book createdBook = bookService.saveBook(book);
        return Response.status(Response.Status.CREATED).header("Location", "/books/" + createdBook.getGlobalBookId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/creditsuisselegalentitys")
    public List<CreditSuisseLegalEntityDTO> listCreditsuisselegalentitys() {

        return creditSuisseLegalEntityService.findAll().stream().map(e -> CreditSuisseLegalEntityToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/creditsuisselegalentitys")
    public Response createCreditSuisseLegalEntity(@RequestBody CreditSuisseLegalEntityDTO creditSuisseLegalEntity) {
        CreditSuisseLegalEntity createdCreditSuisseLegalEntity = creditSuisseLegalEntityService.saveCreditSuisseLegalEntity(creditSuisseLegalEntity);
        return Response.status(Response.Status.CREATED).header("Location", "/creditsuisselegalentitys/" + createdCreditSuisseLegalEntity.getCount()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/creditsuisselegalentitys/{count}")
    public CreditSuisseLegalEntityDTO getCreditSuisseLegalEntityById(@PathParam("count") Integer count) {

        CreditSuisseLegalEntity creditSuisseLegalEntity = creditSuisseLegalEntityService.findById(count);
        if (creditSuisseLegalEntity != null)
            return CreditSuisseLegalEntityToDTOConverter.convert(creditSuisseLegalEntity);
        throw new NotFoundException("No CreditSuisseLegalEntity with  " + " count = " + count + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/creditsuisselegalentitys/{count}")
    public Response updateCreditSuisseLegalEntityById(@RequestBody CreditSuisseLegalEntityDTO creditSuisseLegalEntity) {
        CreditSuisseLegalEntity existingCreditSuisseLegalEntity = creditSuisseLegalEntityService.findById(creditSuisseLegalEntity.getCount());
        if (existingCreditSuisseLegalEntity == null)
            throw new NotFoundException("No CreditSuisseLegalEntity with  " + " creditSuisseLegalEntity.getCount() = " + creditSuisseLegalEntity.getCount() + " exists");

        creditSuisseLegalEntityService.updateCreditSuisseLegalEntity(existingCreditSuisseLegalEntity, creditSuisseLegalEntity);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/creditsuisselegalentitys/{count}")
    public Response deleteCreditSuisseLegalEntityById(@PathParam("count") Integer count) {
        CreditSuisseLegalEntity existingCreditSuisseLegalEntity = creditSuisseLegalEntityService.findById(count);
        if (existingCreditSuisseLegalEntity == null)
            throw new NotFoundException("No CreditSuisseLegalEntity with  " + " count = " + count + " exists");

        creditSuisseLegalEntityService.deleteById(count);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/drdtradeheaders/{id}")
    public DrdTradeHeaderDTO getDrdTradeHeaderById(@PathParam("id") Integer id) {

        DrdTradeHeader drdTradeHeader = drdTradeHeaderService.findById(id);
        if (drdTradeHeader != null)
            return DrdTradeHeaderToDTOConverter.convert(drdTradeHeader);
        throw new NotFoundException("No DrdTradeHeader with  " + " gpiIssue = " + id + " exists");

    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeheaders/{id}")
    public Response updateDrdTradeHeaderById(@RequestBody DrdTradeHeaderDTO drdTradeHeader) {
        DrdTradeHeader existingDrdTradeHeader = drdTradeHeaderService.findById(drdTradeHeader.getId());
        if (existingDrdTradeHeader == null)
            throw new NotFoundException("No DrdTradeHeader with  " + " drdTradeHeader.getGpiIssue() = " + drdTradeHeader.getId() + " exists");

        drdTradeHeaderService.updateDrdTradeHeader(existingDrdTradeHeader, drdTradeHeader);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeheaders/{id}")
    public Response deleteDrdTradeHeaderById(@PathParam("id") Integer id) {
        DrdTradeHeader existingDrdTradeHeader = drdTradeHeaderService.findById(id);
        if (existingDrdTradeHeader == null)
            throw new NotFoundException("No DrdTradeHeader with  " + " gpiIssue = " + id + " exists");

        drdTradeHeaderService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/drdtradeheaders")
    public List<DrdTradeHeaderDTO> listDrdtradeheaders() {

        return drdTradeHeaderService.findAll().stream().map(e -> DrdTradeHeaderToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeheaders")
    public Response createDrdTradeHeader(@RequestBody DrdTradeHeaderDTO drdTradeHeader) {
        DrdTradeHeader createdDrdTradeHeader = drdTradeHeaderService.saveDrdTradeHeader(drdTradeHeader);
        return Response.status(Response.Status.CREATED).header("Location", "/drdtradeheaders/" + createdDrdTradeHeader.getId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/drdtradeunderlyings/{id}")
    public DrdTradeUnderlyingDTO getDrdTradeUnderlyingById(@PathParam("id") Integer id) {

        DrdTradeUnderlying drdTradeUnderlying = drdTradeUnderlyingService.findById(id);
        if (drdTradeUnderlying != null)
            return DrdTradeUnderlyingToDTOConverter.convert(drdTradeUnderlying);
        throw new NotFoundException("No DrdTradeUnderlying with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeunderlyings/{id}")
    public Response updateDrdTradeUnderlyingById(@RequestBody DrdTradeUnderlyingDTO drdTradeUnderlying) {
        DrdTradeUnderlying existingDrdTradeUnderlying = drdTradeUnderlyingService.findById(drdTradeUnderlying.getId());
        if (existingDrdTradeUnderlying == null)
            throw new NotFoundException("No DrdTradeUnderlying with  " + " drdTradeUnderlying.getGpiIssue() = " + drdTradeUnderlying.getId() + " exists");

        drdTradeUnderlyingService.updateDrdTradeUnderlying(existingDrdTradeUnderlying, drdTradeUnderlying);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeunderlyings/{id}")
    public Response deleteDrdTradeUnderlyingById(@PathParam("id") Integer id) {
        DrdTradeUnderlying existingDrdTradeUnderlying = drdTradeUnderlyingService.findById(id);
        if (existingDrdTradeUnderlying == null)
            throw new NotFoundException("No DrdTradeUnderlying with  " + " gpiIssue = " + id + " exists");

        drdTradeUnderlyingService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/drdtradeunderlyings")
    public List<DrdTradeUnderlyingDTO> listDrdtradeunderlyings() {

        return drdTradeUnderlyingService.findAll().stream().map(e -> DrdTradeUnderlyingToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeunderlyings")
    public Response createDrdTradeUnderlying(@RequestBody DrdTradeUnderlyingDTO drdTradeUnderlying) {
        DrdTradeUnderlying createdDrdTradeUnderlying = drdTradeUnderlyingService.saveDrdTradeUnderlying(drdTradeUnderlying);
        return Response.status(Response.Status.CREATED).header("Location", "/drdtradeunderlyings/" + createdDrdTradeUnderlying.getId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/primeswaps/{id}")
    public PrimeSwap getPrimeSwapById(@PathParam("id") Integer id) {

        com.cs.qfc.data.jpa.entities.PrimeSwap primeSwap = primeSwapService.findById(id);
        if (primeSwap != null)
            return PrimeSwapToDTOConverter.convert(primeSwap);
        throw new NotFoundException("No PrimeSwap with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/primeswaps/{id}")
    public Response updatePrimeSwapById(@RequestBody PrimeSwap primeSwap) {
        com.cs.qfc.data.jpa.entities.PrimeSwap existingPrimeSwap = primeSwapService.findById(primeSwap.getId());
        if (existingPrimeSwap == null)
            throw new NotFoundException("No PrimeSwap with  " + " primeSwap.getGpiIssue() = " + primeSwap.getId() + " exists");

        primeSwapService.updatePrimeSwap(existingPrimeSwap, primeSwap);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/primeswaps/{id}")
    public Response deletePrimeSwapById(@PathParam("id") Integer id) {
        com.cs.qfc.data.jpa.entities.PrimeSwap existingPrimeSwap = primeSwapService.findById(id);
        if (existingPrimeSwap == null)
            throw new NotFoundException("No PrimeSwap with  " + " gpiIssue = " + id + " exists");

        primeSwapService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/primeswaps")
    public List<PrimeSwap> listPrimeswaps() {

        return primeSwapService.findAll().stream().map(e -> PrimeSwapToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/primeswaps")
    public Response createPrimeSwap(@RequestBody PrimeSwap primeSwap) {
        com.cs.qfc.data.jpa.entities.PrimeSwap createdPrimeSwap = primeSwapService.savePrimeSwap(primeSwap);
        return Response.status(Response.Status.CREATED).header("Location", "/primeswaps/" + createdPrimeSwap.getId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/primeswappositions")
    public List<PrimeSwapPositionDTO> listPrimeswappositions() {

        return primeSwapPositionService.findAll().stream().map(e -> PrimeSwapPositionToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/primeswappositions")
    public Response createPrimeSwapPosition(@RequestBody PrimeSwapPositionDTO primeSwapPosition) {
        PrimeSwapPosition createdPrimeSwapPosition = primeSwapPositionService.savePrimeSwapPosition(primeSwapPosition);
        return Response.status(Response.Status.CREATED).header("Location", "/primeswappositions/" + createdPrimeSwapPosition.getId()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/primeswappositions/{id}")
    public PrimeSwapPositionDTO getPrimeSwapPositionById(@PathParam("id") Integer id) {

        PrimeSwapPosition primeSwapPosition = primeSwapPositionService.findById(id);
        if (primeSwapPosition != null)
            return PrimeSwapPositionToDTOConverter.convert(primeSwapPosition);
        throw new NotFoundException("No PrimeSwapPosition with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/primeswappositions/{id}")
    public Response updatePrimeSwapPositionById(@RequestBody PrimeSwapPositionDTO primeSwapPosition) {
        PrimeSwapPosition existingPrimeSwapPosition = primeSwapPositionService.findById(primeSwapPosition.getId());
        if (existingPrimeSwapPosition == null)
            throw new NotFoundException("No PrimeSwapPosition with  " + " primeSwapPosition.getGpiIssue() = " + primeSwapPosition.getId() + " exists");

        primeSwapPositionService.updatePrimeSwapPosition(existingPrimeSwapPosition, primeSwapPosition);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/primeswappositions/{id}")
    public Response deletePrimeSwapPositionById(@PathParam("id") Integer id) {
        PrimeSwapPosition existingPrimeSwapPosition = primeSwapPositionService.findById(id);
        if (existingPrimeSwapPosition == null)
            throw new NotFoundException("No PrimeSwapPosition with  " + " gpiIssue = " + id + " exists");

        primeSwapPositionService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/visions/{id}")
    public VisionDTO getVisionById(@PathParam("id") Integer id) {

        Vision vision = visionService.findById(id);
        if (vision != null)
            return VisionToDTOConverter.convert(vision);
        throw new NotFoundException("No Vision with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/visions/{id}")
    public Response updateVisionById(@RequestBody VisionDTO vision) {
        Vision existingVision = visionService.findById(vision.getId());
        if (existingVision == null)
            throw new NotFoundException("No Vision with  " + " vision.getGpiIssue() = " + vision.getId() + " exists");

        visionService.updateVision(existingVision, vision);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/visions/{id}")
    public Response deleteVisionById(@PathParam("id") Integer id) {
        Vision existingVision = visionService.findById(id);
        if (existingVision == null)
            throw new NotFoundException("No Vision with  " + " gpiIssue = " + id + " exists");

        visionService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/visions")
    public List<VisionDTO> listVisions() {

        return visionService.findAll().stream().map(e -> VisionToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/visions")
    public Response createVision(@RequestBody VisionDTO vision) {
        Vision createdVision = visionService.saveVision(vision);
        return Response.status(Response.Status.CREATED).header("Location", "/visions/" + createdVision.getId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/drdtradeinstruments/{id}")
    public DrdTradeInstrumentDTO getDrdTradeInstrumentById(@PathParam("id") Integer id) {

        DrdTradeInstrument drdTradeInstrument = drdTradeInstrumentService.findById(id);
        if (drdTradeInstrument != null)
            return DrdTradeInstrumentToDTOConverter.convert(drdTradeInstrument);
        throw new NotFoundException("No DrdTradeInstrument with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeinstruments/{id}")
    public Response updateDrdTradeInstrumentById(@RequestBody DrdTradeInstrumentDTO drdTradeInstrument) {
        DrdTradeInstrument existingDrdTradeInstrument = drdTradeInstrumentService.findById(drdTradeInstrument.getId());
        if (existingDrdTradeInstrument == null)
            throw new NotFoundException("No DrdTradeInstrument with  " + " drdTradeInstrument.getGpiIssue() = " + drdTradeInstrument.getId() + " exists");

        drdTradeInstrumentService.updateDrdTradeInstrument(existingDrdTradeInstrument, drdTradeInstrument);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeinstruments/{id}")
    public Response deleteDrdTradeInstrumentById(@PathParam("id") Integer id) {
        DrdTradeInstrument existingDrdTradeInstrument = drdTradeInstrumentService.findById(id);
        if (existingDrdTradeInstrument == null)
            throw new NotFoundException("No DrdTradeInstrument with  " + " gpiIssue = " + id + " exists");

        drdTradeInstrumentService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/drdtradeinstruments")
    public List<DrdTradeInstrumentDTO> listDrdtradeinstruments() {

        return drdTradeInstrumentService.findAll().stream().map(e -> DrdTradeInstrumentToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeinstruments")
    public Response createDrdTradeInstrument(@RequestBody DrdTradeInstrumentDTO drdTradeInstrument) {
        DrdTradeInstrument createdDrdTradeInstrument = drdTradeInstrumentService.saveDrdTradeInstrument(drdTradeInstrument);
        return Response.status(Response.Status.CREATED).header("Location", "/drdtradeinstruments/" + createdDrdTradeInstrument.getId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/drdtradelegs")
    public List<DrdTradeLegDTO> listDrdtradelegs() {

        return drdTradeLegService.findAll().stream().map(e -> DrdTradeLegToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradelegs")
    public Response createDrdTradeLeg(@RequestBody DrdTradeLegDTO drdTradeLeg) {
        DrdTradeLeg createdDrdTradeLeg = drdTradeLegService.saveDrdTradeLeg(drdTradeLeg);
        return Response.status(Response.Status.CREATED).header("Location", "/drdtradelegs/" + createdDrdTradeLeg.getId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/drdtradelegs/{id}")
    public DrdTradeLegDTO getDrdTradeLegById(@PathParam("id") Integer id) {

        DrdTradeLeg drdTradeLeg = drdTradeLegService.findById(id);
        if (drdTradeLeg != null)
            return DrdTradeLegToDTOConverter.convert(drdTradeLeg);
        throw new NotFoundException("No DrdTradeLeg with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradelegs/{id}")
    public Response updateDrdTradeLegById(@RequestBody DrdTradeLegDTO drdTradeLeg) {
        DrdTradeLeg existingDrdTradeLeg = drdTradeLegService.findById(drdTradeLeg.getId());
        if (existingDrdTradeLeg == null)
            throw new NotFoundException("No DrdTradeLeg with  " + " drdTradeLeg.getGpiIssue() = " + drdTradeLeg.getId() + " exists");

        drdTradeLegService.updateDrdTradeLeg(existingDrdTradeLeg, drdTradeLeg);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradelegs/{id}")
    public Response deleteDrdTradeLegById(@PathParam("id") Integer id) {
        DrdTradeLeg existingDrdTradeLeg = drdTradeLegService.findById(id);
        if (existingDrdTradeLeg == null)
            throw new NotFoundException("No DrdTradeLeg with  " + " gpiIssue = " + id + " exists");

        drdTradeLegService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/marnags/{cnid}")
    public MarnaGDTO getMarnaGById(@PathParam("cnid") Integer cnid) {

        MarnaG marnaG = marnaGService.findById(cnid);
        if (marnaG != null)
            return MarnaGToDTOConverter.convert(marnaG);
        throw new NotFoundException("No MarnaG with  " + " cnid = " + cnid + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/marnags/{cnid}")
    public Response updateMarnaGById(@RequestBody MarnaGDTO marnaG) {
        MarnaG existingMarnaG = marnaGService.findById(marnaG.getCnid());
        if (existingMarnaG == null)
            throw new NotFoundException("No MarnaG with  " + " marnaG.getCnid() = " + marnaG.getCnid() + " exists");

        marnaGService.updateMarnaG(existingMarnaG, marnaG);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/marnags/{cnid}")
    public Response deleteMarnaGById(@PathParam("cnid") Integer cnid) {
        MarnaG existingMarnaG = marnaGService.findById(cnid);
        if (existingMarnaG == null)
            throw new NotFoundException("No MarnaG with  " + " cnid = " + cnid + " exists");

        marnaGService.deleteById(cnid);

        return Response.status(Response.Status.OK).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/marnags")
    public List<MarnaGDTO> listMarnags() {

        return marnaGService.findAll().stream().map(e -> MarnaGToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/marnags")
    public Response createMarnaG(@RequestBody MarnaGDTO marnaG) {
        MarnaG createdMarnaG = marnaGService.saveMarnaG(marnaG);
        return Response.status(Response.Status.CREATED).header("Location", "/marnags/" + createdMarnaG.getCnid()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/loaniqs")
    public List<LoanIQDTO> listLoaniqs() {

        return loanIQService.findAll().stream().map(e -> LoanIQToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/loaniqs")
    public Response createLoanIQ(@RequestBody LoanIQDTO loanIQ) {
        LoanIQ createdLoanIQ = loanIQService.saveLoanIQ(loanIQ);
        return Response.status(Response.Status.CREATED).header("Location", "/loaniqs/" + createdLoanIQ.getId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/loaniqs/{id}")
    public LoanIQDTO getLoanIQById(@PathParam("id") Integer id) {

        LoanIQ loanIQ = loanIQService.findById(id);
        if (loanIQ != null)
            return LoanIQToDTOConverter.convert(loanIQ);
        throw new NotFoundException("No LoanIQ with  " + " gpiIssue = " + id + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/loaniqs/{id}")
    public Response updateLoanIQById(@RequestBody LoanIQDTO loanIQ) {
        LoanIQ existingLoanIQ = loanIQService.findById(loanIQ.getId());
        if (existingLoanIQ == null)
            throw new NotFoundException("No LoanIQ with  " + " loanIQ.getGpiIssue() = " + loanIQ.getId() + " exists");

        loanIQService.updateLoanIQ(existingLoanIQ, loanIQ);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/loaniqs/{id}")
    public Response deleteLoanIQById(@PathParam("id") Integer id) {
        LoanIQ existingLoanIQ = loanIQService.findById(id);
        if (existingLoanIQ == null)
            throw new NotFoundException("No LoanIQ with  " + " gpiIssue = " + id + " exists");

        loanIQService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/customqueries")
    public List<CustomQueriesDTO> listCustomqueries() {

        return customQueriesService.findAll().stream().map(e -> CustomQueriesToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/customqueries")
    public Response createCustomQueries(@RequestBody CustomQueriesDTO customQueries) {
        CustomQueries createdCustomQueries = customQueriesService.saveCustomQueries(customQueries);
        return Response.status(Response.Status.CREATED).header("Location", "/customqueries/" + createdCustomQueries.getCriteriaName()).build();

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/customquerieslist/{entityName}")
    public List<String> listCustomqueriesForEntity(@PathParam("entityName") String entityName) {
        Collection<CustomQueries> all = customQueriesService.findAll();
        List<CustomQueries> filtered = all.stream().filter(x -> x.getEntityName().equals(entityName)).collect(Collectors.toList());
        return
                filtered.stream().map(x -> x.getCriteriaName().replace(entityName + "_", "")).collect(Collectors.toList());

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/customqueries/{criteriaName}")
    public CustomQueriesDTO getCustomQueriesById(@PathParam("criteriaName") String criteriaName) {

        CustomQueries customQueries = customQueriesService.findById(criteriaName);
        if (customQueries != null)
            return CustomQueriesToDTOConverter.convert(customQueries);
        throw new NotFoundException("No CustomQueries with  " + " criteriaName = " + criteriaName + " exists");

    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/customqueries/{criteriaName}")
    public Response updateCustomQueriesById(@RequestBody CustomQueriesDTO customQueries) {
        CustomQueries existingCustomQueries = customQueriesService.findById(customQueries.getCriteriaName());
        if (existingCustomQueries == null)
            throw new NotFoundException("No CustomQueries with  " + " customQueries.getCriteriaName() = " + customQueries.getCriteriaName() + " exists");

        customQueriesService.updateCustomQueries(existingCustomQueries, customQueries);

        return Response.status(Response.Status.OK).build();


    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/customqueries/{criteriaName}")
    public Response deleteCustomQueriesById(@PathParam("criteriaName") String criteriaName) {
        CustomQueries existingCustomQueries = customQueriesService.findById(criteriaName);
        if (existingCustomQueries == null)
            throw new NotFoundException("No CustomQueries with  " + " criteriaName = " + criteriaName + " exists");

        customQueriesService.deleteById(criteriaName);

        return Response.status(Response.Status.OK).build();

    }




    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/departments/custom")
    public List<DepartmentDTO> createDepartment(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(departmentService.findAllCustom(searchInfo));
    }

    //Custom Search

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradelegs/custom")
    public List<DrdTradeLegDTO> createDrdTradeLeg(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(drdTradeLegService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/debtconvertiblebonds/custom")
    public List<DebtConvertibleBondDTO> createDebtConvertibleBond(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(debtConvertibleBondService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/partys/custom")
    public List<Party> createParty(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(partyService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/debtinstruments/custom")
    public List<DebtInstrumentDTO> createDebtInstrument(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(debtInstrumentService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/primeswaps/custom")
    public List<PrimeSwap> createPrimeSwap(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(primeSwapService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clienthierarchys/custom")
    public List<ClientHierarchyDTO> createClientHierarchy(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(clientHierarchyService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/creditsuisselegalentitys/custom")
    public List<CreditSuisseLegalEntityDTO> createCreditSuisseLegalEntity(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(creditSuisseLegalEntityService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/visions/custom")
    public List<VisionDTO> createVision(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(visionService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/books/custom")
    public List<BookDTO> createBook(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(bookService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/businessunits/custom")
    public List<BusinessUnitDTO> createBusinessUnit(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(businessUnitService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/employeeroles/custom")
    public List<EmployeeRoleDTO> createEmployeeRole(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(employeeRoleService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/portfolios/custom")
    public List<PortfolioDTO> createPortfolio(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(portfolioService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeunderlyings/custom")
    public List<DrdTradeUnderlyingDTO> createDrdTradeUnderlying(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(drdTradeUnderlyingService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/porfoliorelationships/custom")
    public List<PorfolioRelationshipDTO> createPorfolioRelationship(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(porfolioRelationshipService.findAllCustom(searchInfo));
    }

    /*@POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clientleis/custom")
    public List<ClientLeiDTO> createClientLei(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(clientLeiService.findAllCustom(searchInfo));
    }*/

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/loaniqs/custom")
    public List<LoanIQDTO> createLoanIQ(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(loanIQService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/marnags/custom")
    public List<MarnaGDTO> createMarnaG(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(marnaGService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/dtpmisgbldytrees/custom")
    public List<DTPMISGBLDYTREEDTO> createDTPMISGBLDYTREE(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(dTPMISGBLDYTREEService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/equityconvertiblebonds/custom")
    public List<EquityConvertibleBondDTO> createEquityConvertibleBond(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(equityConvertibleBondService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeheaders/custom")
    public List<DrdTradeHeaderDTO> createDrdTradeHeader(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(drdTradeHeaderService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/equityinstruments/custom")
    public List<EquityInstrumentDTO> createEquityInstrument(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(equityInstrumentService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/primeswappositions/custom")
    public List<PrimeSwapPositionDTO> createPrimeSwapPosition(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(primeSwapPositionService.findAllCustom(searchInfo));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/drdtradeinstruments/custom")
    public List<DrdTradeInstrumentDTO> createDrdTradeInstrument(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(drdTradeInstrumentService.findAllCustom(searchInfo));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/specifications")
    public List<SpecificationDTO> listSpecifications() {

        return specificationService.findAll().stream().map(e -> SpecificationToDTOConverter.convert(e)).collect(Collectors.toList());

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/objectmodels")
    public List<ObjectModelDTO> listObjectModels() {

        return objectModelService.findAll().stream().map(e -> ObjectModelToDTOConverter.convert(e)).collect(Collectors.toList());

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/specifications/custom")
    public List<SpecificationDTO> createSpecification(@RequestBody SearchInfoDTO searchInfo) {
        return new ArrayList<>(specificationService.findAllCustom(searchInfo));
    }


    //Custom Search

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/specifications")
    public Response createSpecification(@RequestBody SpecificationDTO specification) {
        Specification createdSpecification = specificationService.saveSpecification(specification);
        return Response.status(Response.Status.CREATED).header("Location", "/specifications/" + createdSpecification.getId()).build();

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/objectmodels")
    public Response createObjectModel(@RequestBody ObjectModelDTO objectmodel) {
        System.out.println("Saving boject model " + objectmodel.toString());
        ObjectModel createdObjectModel = objectModelService.saveObjectModel(objectmodel);
        return Response.status(Response.Status.CREATED).header("Location", "/objectmodels/" + createdObjectModel.getId()).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/specifications/{id}")
    public SpecificationDTO getSpecificationById(@PathParam("id") Integer id) {

        Specification specification = specificationService.findById(id);
        if (specification != null)
            return SpecificationToDTOConverter.convert(specification);
        throw new NotFoundException("No Specification with  " + " id = " + id + " exists");

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/objectmodels/{id}")
    public ObjectModelDTO getObjectModelById(@PathParam("id") Integer id) {

        ObjectModel objectModel = objectModelService.findById(id);
        if (objectModel != null)
            return ObjectModelToDTOConverter.convert(objectModel);
        throw new NotFoundException("No ObjectModel with  " + " id = " + id + " exists");

    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/specifications/{id}")
    public Response updateSpecificationById(@RequestBody SpecificationDTO specification) {
        Specification existingSpecification = specificationService.findById(specification.getId());
        if (existingSpecification == null)
            throw new NotFoundException("No Specification with  " + " specification.getId() = " + specification.getId() + " exists");

        specificationService.updateSpecification(existingSpecification, specification);

        return Response.status(Response.Status.OK).build();


    }



    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/objectmodels/{id}")
    public Response updateObjectModelById(@RequestBody ObjectModelDTO objectmodel) {
        ObjectModel existingObjectModel = objectModelService.findById(objectmodel.getId());
        if (existingObjectModel == null)
            throw new NotFoundException("No Object Model with  " + " id " + objectmodel.getId() + " exists");

        objectModelService.updateSpecification(existingObjectModel, objectmodel);

        return Response.status(Response.Status.OK).build();


    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/specifications/{id}")
    public Response deleteSpecificationById(@PathParam("id") Integer id) {
        Specification existingSpecification = specificationService.findById(id);
        if (existingSpecification == null)
            throw new NotFoundException("No Specification with  " + " id = " + id + " exists");

        specificationService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/objectmodels/{id}")
    public Response deleteObjectModelById(@PathParam("id") Integer id) {
        ObjectModel existingObjectModel = objectModelService.findById(id);
        if (existingObjectModel == null)
            throw new NotFoundException("No Specification with  " + " id = " + id + " exists");

        objectModelService.deleteById(id);

        return Response.status(Response.Status.OK).build();

    }




}







