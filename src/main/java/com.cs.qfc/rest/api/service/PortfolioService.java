package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToPortfolioConverter;
import com.cs.qfc.data.jpa.entities.Portfolio;
import com.cs.qfc.data.jpa.repositories.PortfolioRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedPortfolioRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.PortfolioDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class PortfolioService {


    private static final Logger LOG = LoggerFactory.getLogger(PortfolioService.class);

    @Autowired
    PortfolioRepository portfolioRepository;
    @Autowired
    CustomizedPortfolioRepository customizedPortfolioRepository;

    @Transactional
    public Portfolio savePortfolio(PortfolioDTO portfolioDTO) {
        Portfolio portfolio = DTOToPortfolioConverter.convert(portfolioDTO);
        return portfolioRepository.save(portfolio);
    }

    @Transactional
    public Portfolio save(Portfolio portfolio) {
        return portfolioRepository.save(portfolio);
    }


    @Transactional
    public Collection<Portfolio> findAll() {
        return portfolioRepository.findAll();
    }

    @Transactional
    public Collection<PortfolioDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedPortfolioRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public Portfolio findById(String portfolioId) {
        Optional<Portfolio> portfolio = portfolioRepository.findById(portfolioId);
        return (portfolio.isPresent() ? portfolio.get() : null);
    }

    @Transactional
    public Portfolio updatePortfolio(Portfolio portfolio, PortfolioDTO portfolioDto) {

        portfolio.setGlobalPartyId(portfolioDto.getGlobalPartyId());
        portfolio.setPartytypeDescription(portfolioDto.getPartytypeDescription());
        portfolio.setLegalName(portfolioDto.getLegalName());
        portfolio.setPartyAliasName(portfolioDto.getPartyAliasName());
        portfolio.setLegalRoleDefinitionId(portfolioDto.getLegalRoleDefinitionId());
        portfolio.setLrdRoleType(portfolioDto.getLrdRoleType());
        portfolio.setLrdRoleDescription(portfolioDto.getLrdRoleDescription());
        portfolio.setLrdRoleCode(portfolioDto.getLrdRoleCode());
        portfolio.setPartyLegalRoleId(portfolioDto.getPartyLegalRoleId());
        portfolio.setSourceId(portfolioDto.getSourceId());
        portfolio.setSourceName(portfolioDto.getSourceName());
        portfolio.setInternalPartyAltId(portfolioDto.getInternalPartyAltId());
        return portfolioRepository.save(portfolio);
    }

    @Transactional
    public void deleteById(String portfolioId) {
        portfolioRepository.deleteById(portfolioId);

    }


}
