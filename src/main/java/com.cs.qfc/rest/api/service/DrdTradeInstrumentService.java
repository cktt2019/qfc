package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToDrdTradeInstrumentConverter;
import com.cs.qfc.data.jpa.entities.DrdTradeInstrument;
import com.cs.qfc.data.jpa.repositories.DrdTradeInstrumentRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedDrdTradeInstrumentRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DrdTradeInstrumentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class DrdTradeInstrumentService {


    private static final Logger LOG = LoggerFactory.getLogger(DrdTradeInstrumentService.class);

    @Autowired
    DrdTradeInstrumentRepository drdTradeInstrumentRepository;
    @Autowired
    CustomizedDrdTradeInstrumentRepository customizedDrdTradeInstrumentRepository;

    @Transactional
    public DrdTradeInstrument saveDrdTradeInstrument(DrdTradeInstrumentDTO drdTradeInstrumentDTO) {
        DrdTradeInstrument drdTradeInstrument = DTOToDrdTradeInstrumentConverter.convert(drdTradeInstrumentDTO);
        return drdTradeInstrumentRepository.save(drdTradeInstrument);
    }

    @Transactional
    public DrdTradeInstrument save(DrdTradeInstrument drdTradeInstrument) {
        return drdTradeInstrumentRepository.save(drdTradeInstrument);
    }


    @Transactional
    public Collection<DrdTradeInstrument> findAll() {
        return drdTradeInstrumentRepository.findAll();
    }

    @Transactional
    public Collection<DrdTradeInstrumentDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedDrdTradeInstrumentRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public DrdTradeInstrument findById(Integer id) {
        Optional<DrdTradeInstrument> drdTradeInstrument = drdTradeInstrumentRepository.findById(id);
        return (drdTradeInstrument.isPresent() ? drdTradeInstrument.get() : null);
    }

    @Transactional
    public DrdTradeInstrument updateDrdTradeInstrument(DrdTradeInstrument drdTradeInstrument, DrdTradeInstrumentDTO drdTradeInstrumentDto) {

        drdTradeInstrument.setCobDate(drdTradeInstrumentDto.getCobDate());
        drdTradeInstrument.setGbmEntity(drdTradeInstrumentDto.getGbmEntity());
        drdTradeInstrument.setTradeId(drdTradeInstrumentDto.getTradeId());
        drdTradeInstrument.setCounterPartyId(drdTradeInstrumentDto.getCounterPartyId());
        drdTradeInstrument.setBookName(drdTradeInstrumentDto.getBookName());
        drdTradeInstrument.setAssetClass(drdTradeInstrumentDto.getAssetClass());
        drdTradeInstrument.setCduMasterAgreementLlk(drdTradeInstrumentDto.getCduMasterAgreementLlk());
        drdTradeInstrument.setCduCollateralAnnexLlk(drdTradeInstrumentDto.getCduCollateralAnnexLlk());
        drdTradeInstrument.setFramesoftMasterAgreementLlk(drdTradeInstrumentDto.getFramesoftMasterAgreementLlk());
        drdTradeInstrument.setAlgoCollateralAnnexLlk(drdTradeInstrumentDto.getAlgoCollateralAnnexLlk());
        drdTradeInstrument.setTradeDate(drdTradeInstrumentDto.getTradeDate());
        drdTradeInstrument.setPrimaryMaturityDate(drdTradeInstrumentDto.getPrimaryMaturityDate());
        drdTradeInstrument.setLocalPvCcyCode(drdTradeInstrumentDto.getLocalPvCcyCode());
        drdTradeInstrument.setLocalPv(drdTradeInstrumentDto.getLocalPv());
        return drdTradeInstrumentRepository.save(drdTradeInstrument);
    }

    @Transactional
    public void deleteById(Integer id) {
        drdTradeInstrumentRepository.deleteById(id);

    }


}
