package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToDTPMISGBLDYTREEConverter;
import com.cs.qfc.data.jpa.entities.DTPMISGBLDYTREE;
import com.cs.qfc.data.jpa.repositories.DTPMISGBLDYTREERepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedDTPMISGBLDYTREERepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DTPMISGBLDYTREEDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class DTPMISGBLDYTREEService {


    private static final Logger LOG = LoggerFactory.getLogger(DTPMISGBLDYTREEService.class);

    @Autowired
    DTPMISGBLDYTREERepository dTPMISGBLDYTREERepository;
    @Autowired
    CustomizedDTPMISGBLDYTREERepository customizedDTPMISGBLDYTREERepository;

    @Transactional
    public DTPMISGBLDYTREE saveDTPMISGBLDYTREE(DTPMISGBLDYTREEDTO dTPMISGBLDYTREEDTO) {
        DTPMISGBLDYTREE dTPMISGBLDYTREE = DTOToDTPMISGBLDYTREEConverter.convert(dTPMISGBLDYTREEDTO);
        return dTPMISGBLDYTREERepository.save(dTPMISGBLDYTREE);
    }

    @Transactional
    public DTPMISGBLDYTREE save(DTPMISGBLDYTREE dTPMISGBLDYTREE) {
        return dTPMISGBLDYTREERepository.save(dTPMISGBLDYTREE);
    }


    @Transactional
    public Collection<DTPMISGBLDYTREE> findAll() {
        return dTPMISGBLDYTREERepository.findAll();
    }

    @Transactional
    public Collection<DTPMISGBLDYTREEDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedDTPMISGBLDYTREERepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public DTPMISGBLDYTREE findById(String misMemberCode) {
        Optional<DTPMISGBLDYTREE> dTPMISGBLDYTREE = dTPMISGBLDYTREERepository.findById(misMemberCode);
        return (dTPMISGBLDYTREE.isPresent() ? dTPMISGBLDYTREE.get() : null);
    }

    @Transactional
    public DTPMISGBLDYTREE updateDTPMISGBLDYTREE(DTPMISGBLDYTREE dTPMISGBLDYTREE, DTPMISGBLDYTREEDTO dTPMISGBLDYTREEDto) {

        dTPMISGBLDYTREE.setAmericasProductLineHeadId(dTPMISGBLDYTREEDto.getAmericasProductLineHeadId());
        dTPMISGBLDYTREE.setAmericasClusterHeadId(dTPMISGBLDYTREEDto.getAmericasClusterHeadId());
        dTPMISGBLDYTREE.setApacClusterHeadId(dTPMISGBLDYTREEDto.getApacClusterHeadId());
        dTPMISGBLDYTREE.setApacProductLineHeadId(dTPMISGBLDYTREEDto.getApacProductLineHeadId());
        dTPMISGBLDYTREE.setBusinessControllerL2Id(dTPMISGBLDYTREEDto.getBusinessControllerL2Id());
        dTPMISGBLDYTREE.setCluster(dTPMISGBLDYTREEDto.getCluster());
        dTPMISGBLDYTREE.setClusterCode(dTPMISGBLDYTREEDto.getClusterCode());
        dTPMISGBLDYTREE.setClusterHeadId(dTPMISGBLDYTREEDto.getClusterHeadId());
        dTPMISGBLDYTREE.setDesk(dTPMISGBLDYTREEDto.getDesk());
        dTPMISGBLDYTREE.setDeskCode(dTPMISGBLDYTREEDto.getDeskCode());
        dTPMISGBLDYTREE.setDivision(dTPMISGBLDYTREEDto.getDivision());
        dTPMISGBLDYTREE.setDivisionCode(dTPMISGBLDYTREEDto.getDivisionCode());
        dTPMISGBLDYTREE.setEuroClusterHeadId(dTPMISGBLDYTREEDto.getEuroClusterHeadId());
        dTPMISGBLDYTREE.setEuroProductLineHeadId(dTPMISGBLDYTREEDto.getEuroProductLineHeadId());
        dTPMISGBLDYTREE.setGlobalProductLineHeadId(dTPMISGBLDYTREEDto.getGlobalProductLineHeadId());
        dTPMISGBLDYTREE.setHierarchyTraderId(dTPMISGBLDYTREEDto.getHierarchyTraderId());
        dTPMISGBLDYTREE.setMisHierarchySortCode(dTPMISGBLDYTREEDto.getMisHierarchySortCode());
        dTPMISGBLDYTREE.setMisMemberName(dTPMISGBLDYTREEDto.getMisMemberName());
        dTPMISGBLDYTREE.setMisMemberTypeCode(dTPMISGBLDYTREEDto.getMisMemberTypeCode());
        dTPMISGBLDYTREE.setMisProduct(dTPMISGBLDYTREEDto.getMisProduct());
        dTPMISGBLDYTREE.setMisRegionCode(dTPMISGBLDYTREEDto.getMisRegionCode());
        dTPMISGBLDYTREE.setMisTreeCode(dTPMISGBLDYTREEDto.getMisTreeCode());
        dTPMISGBLDYTREE.setParentMisMemberCode(dTPMISGBLDYTREEDto.getParentMisMemberCode());
        dTPMISGBLDYTREE.setProductControlSectionMgrId(dTPMISGBLDYTREEDto.getProductControlSectionMgrId());
        dTPMISGBLDYTREE.setProductLineCode(dTPMISGBLDYTREEDto.getProductLineCode());
        dTPMISGBLDYTREE.setProductLineName(dTPMISGBLDYTREEDto.getProductLineName());
        dTPMISGBLDYTREE.setSub3ProductLine(dTPMISGBLDYTREEDto.getSub3ProductLine());
        dTPMISGBLDYTREE.setSub3ProductLineDescription(dTPMISGBLDYTREEDto.getSub3ProductLineDescription());
        dTPMISGBLDYTREE.setSubClusterCode(dTPMISGBLDYTREEDto.getSubClusterCode());
        dTPMISGBLDYTREE.setSubClusterName(dTPMISGBLDYTREEDto.getSubClusterName());
        dTPMISGBLDYTREE.setSubDivisionCode(dTPMISGBLDYTREEDto.getSubDivisionCode());
        dTPMISGBLDYTREE.setSubDivisionDescription(dTPMISGBLDYTREEDto.getSubDivisionDescription());
        dTPMISGBLDYTREE.setSubProductDescription(dTPMISGBLDYTREEDto.getSubProductDescription());
        dTPMISGBLDYTREE.setSubProductLine(dTPMISGBLDYTREEDto.getSubProductLine());
        dTPMISGBLDYTREE.setSwissProductLineHeadId(dTPMISGBLDYTREEDto.getSwissProductLineHeadId());
        dTPMISGBLDYTREE.setTradingSupervisorId(dTPMISGBLDYTREEDto.getTradingSupervisorId());
        dTPMISGBLDYTREE.setTradingSupervisorDelegateId(dTPMISGBLDYTREEDto.getTradingSupervisorDelegateId());
        dTPMISGBLDYTREE.setMisProductCode(dTPMISGBLDYTREEDto.getMisProductCode());
        dTPMISGBLDYTREE.setMisUnitPercentOwned(dTPMISGBLDYTREEDto.getMisUnitPercentOwned());
        dTPMISGBLDYTREE.setMisUnitPercentDescription(dTPMISGBLDYTREEDto.getMisUnitPercentDescription());
        dTPMISGBLDYTREE.setDeltaFlag(dTPMISGBLDYTREEDto.getDeltaFlag());
        dTPMISGBLDYTREE.setCddsTreeNodeStatus(dTPMISGBLDYTREEDto.getCddsTreeNodeStatus());
        dTPMISGBLDYTREE.setTradingSupport(dTPMISGBLDYTREEDto.getTradingSupport());
        dTPMISGBLDYTREE.setTradeSupportManagerEmail(dTPMISGBLDYTREEDto.getTradeSupportManagerEmail());
        return dTPMISGBLDYTREERepository.save(dTPMISGBLDYTREE);
    }

    @Transactional
    public void deleteById(String misMemberCode) {
        dTPMISGBLDYTREERepository.deleteById(misMemberCode);

    }


}
