package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToPrimeSwapConverter;
import com.cs.qfc.data.jpa.repositories.PrimeSwapRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedPrimeSwapRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.PrimeSwap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class PrimeSwapService {


    private static final Logger LOG = LoggerFactory.getLogger(PrimeSwapService.class);

    @Autowired
    PrimeSwapRepository primeSwapRepository;
    @Autowired
    CustomizedPrimeSwapRepository customizedPrimeSwapRepository;

    @Transactional
    public com.cs.qfc.data.jpa.entities.PrimeSwap savePrimeSwap(PrimeSwap primeSwapDTO) {
        com.cs.qfc.data.jpa.entities.PrimeSwap primeSwap = DTOToPrimeSwapConverter.convert(primeSwapDTO);
        return primeSwapRepository.save(primeSwap);
    }

    @Transactional
    public com.cs.qfc.data.jpa.entities.PrimeSwap save(com.cs.qfc.data.jpa.entities.PrimeSwap primeSwap) {
        return primeSwapRepository.save(primeSwap);
    }


    @Transactional
    public Collection<com.cs.qfc.data.jpa.entities.PrimeSwap> findAll() {
        return primeSwapRepository.findAll();
    }

    @Transactional
    public Collection<PrimeSwap> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedPrimeSwapRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public com.cs.qfc.data.jpa.entities.PrimeSwap findById(Integer id) {
        Optional<com.cs.qfc.data.jpa.entities.PrimeSwap> primeSwap = primeSwapRepository.findById(id);
        return (primeSwap.isPresent() ? primeSwap.get() : null);
    }

    @Transactional
    public com.cs.qfc.data.jpa.entities.PrimeSwap updatePrimeSwap(com.cs.qfc.data.jpa.entities.PrimeSwap primeSwap, PrimeSwap primeSwapDto) {

        primeSwap.setBusinessDate(primeSwapDto.getBusinessDate());
        primeSwap.setLegalEntity(primeSwapDto.getLegalEntity());
        primeSwap.setSwapId(primeSwapDto.getSwapId());
        primeSwap.setSwapVersion(primeSwapDto.getSwapVersion());
        primeSwap.setCounterPartyCode(primeSwapDto.getCounterPartyCode());
        primeSwap.setCounterPartyName(primeSwapDto.getCounterPartyName());
        primeSwap.setBook(primeSwapDto.getBook());
        primeSwap.setTrader(primeSwapDto.getTrader());
        primeSwap.setSwapType(primeSwapDto.getSwapType());
        primeSwap.setFramesoftMasterAgreementLlk(primeSwapDto.getFramesoftMasterAgreementLlk());
        primeSwap.setAlgoCollateralAgreementLlk(primeSwapDto.getAlgoCollateralAgreementLlk());
        primeSwap.setCduMasterAgreementLlk(primeSwapDto.getCduMasterAgreementLlk());
        primeSwap.setCduCollateralAnnexLlk(primeSwapDto.getCduCollateralAnnexLlk());
        primeSwap.setSwapTradeDate(primeSwapDto.getSwapTradeDate());
        primeSwap.setSwapTerminationDate(primeSwapDto.getSwapTerminationDate());
        primeSwap.setNextInterestPayDate(primeSwapDto.getNextInterestPayDate());
        primeSwap.setEquityCurrency(primeSwapDto.getEquityCurrency());
        primeSwap.setTotalSwapValue(primeSwapDto.getTotalSwapValue());
        primeSwap.setCurrencyEquityNotional(primeSwapDto.getCurrencyEquityNotional());
        primeSwap.setInitMargin(primeSwapDto.getInitMargin());
        primeSwap.setInitialMarginDirection(primeSwapDto.getInitialMarginDirection());
        primeSwap.setCsid(primeSwapDto.getCsid());
        primeSwap.setGsid(primeSwapDto.getGsid());
        return primeSwapRepository.save(primeSwap);
    }

    @Transactional
    public void deleteById(Integer id) {
        primeSwapRepository.deleteById(id);

    }


}
