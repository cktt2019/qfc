package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToMarnaGConverter;
import com.cs.qfc.data.jpa.entities.MarnaG;
import com.cs.qfc.data.jpa.repositories.MarnaGRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedMarnaGRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.MarnaGDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class MarnaGService {


    private static final Logger LOG = LoggerFactory.getLogger(MarnaGService.class);

    @Autowired
    MarnaGRepository marnaGRepository;
    @Autowired
    CustomizedMarnaGRepository customizedMarnaGRepository;

    @Transactional
    public MarnaG saveMarnaG(MarnaGDTO marnaGDTO) {
        MarnaG marnaG = DTOToMarnaGConverter.convert(marnaGDTO);
        return marnaGRepository.save(marnaG);
    }

    @Transactional
    public MarnaG save(MarnaG marnaG) {
        return marnaGRepository.save(marnaG);
    }


    @Transactional
    public Collection<MarnaG> findAll() {
        return marnaGRepository.findAll();
    }

    @Transactional
    public Collection<MarnaGDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedMarnaGRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public MarnaG findById(Integer cnid) {
        Optional<MarnaG> marnaG = marnaGRepository.findById(cnid);
        return (marnaG.isPresent() ? marnaG.get() : null);
    }

    @Transactional
    public MarnaG updateMarnaG(MarnaG marnaG, MarnaGDTO marnaGDto) {

        marnaG.setAgreementNumber(marnaGDto.getAgreementNumber());
        marnaG.setStatus(marnaGDto.getStatus());
        marnaG.setSupersededByCnid(marnaGDto.getSupersededByCnid());
        marnaG.setSupersededByAgreement(marnaGDto.getSupersededByAgreement());
        marnaG.setLegalAgreementDescription(marnaGDto.getLegalAgreementDescription());
        marnaG.setCsEntityBookinggroupId(marnaGDto.getCsEntityBookinggroupId());
        marnaG.setCsEntityBookinggroupName(marnaGDto.getCsEntityBookinggroupName());
        marnaG.setNegotiator(marnaGDto.getNegotiator());
        marnaG.setNegotiatorLocation(marnaGDto.getNegotiatorLocation());
        marnaG.setCounterParty(marnaGDto.getCounterParty());
        marnaG.setCounterPartyRole(marnaGDto.getCounterPartyRole());
        marnaG.setCsid(marnaGDto.getCsid());
        marnaG.setLegalId(marnaGDto.getLegalId());
        marnaG.setFormerlyKnownAs(marnaGDto.getFormerlyKnownAs());
        marnaG.setCountryOfInc(marnaGDto.getCountryOfInc());
        marnaG.setContractType(marnaGDto.getContractType());
        marnaG.setRequestType(marnaGDto.getRequestType());
        marnaG.setAgreementTitle(marnaGDto.getAgreementTitle());
        marnaG.setContractDate(marnaGDto.getContractDate());
        marnaG.setAgency(marnaGDto.getAgency());
        marnaG.setCollateralAgreementFlag(marnaGDto.getCollateralAgreementFlag());
        marnaG.setCsaEnforceableFlag(marnaGDto.getCsaEnforceableFlag());
        marnaG.setTransactionSpecificFlag(marnaGDto.getTransactionSpecificFlag());
        marnaG.setCsEntityAgencyFlag(marnaGDto.getCsEntityAgencyFlag());
        marnaG.setDesk(marnaGDto.getDesk());
        marnaG.setIconid(marnaGDto.getIconid());
        marnaG.setCsaRegFlag(marnaGDto.getCsaRegFlag());
        return marnaGRepository.save(marnaG);
    }

    @Transactional
    public void deleteById(Integer cnid) {
        marnaGRepository.deleteById(cnid);

    }


}
