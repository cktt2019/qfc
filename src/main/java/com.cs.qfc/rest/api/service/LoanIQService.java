package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToLoanIQConverter;
import com.cs.qfc.data.jpa.entities.LoanIQ;
import com.cs.qfc.data.jpa.repositories.LoanIQRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedLoanIQRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.LoanIQDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class LoanIQService {


    private static final Logger LOG = LoggerFactory.getLogger(LoanIQService.class);

    @Autowired
    LoanIQRepository loanIQRepository;

    @Autowired
    CustomizedLoanIQRepository customizedLoanIQRepository;

    @Transactional
    public LoanIQ saveLoanIQ(LoanIQDTO loanIQDTO) {
        LoanIQ loanIQ = DTOToLoanIQConverter.convert(loanIQDTO);
        return loanIQRepository.save(loanIQ);
    }

    @Transactional
    public LoanIQ save(LoanIQ loanIQ) {
        return loanIQRepository.save(loanIQ);
    }


    @Transactional
    public Collection<LoanIQ> findAll() {
        return loanIQRepository.findAll();
    }

    @Transactional
    public Collection<LoanIQDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedLoanIQRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public LoanIQ findById(Integer id) {
        Optional<LoanIQ> loanIQ = loanIQRepository.findById(id);
        return (loanIQ.isPresent() ? loanIQ.get() : null);
    }

    @Transactional
    public LoanIQ updateLoanIQ(LoanIQ loanIQ, LoanIQDTO loanIQDto) {

        loanIQ.setCptyCsid(loanIQDto.getCptyCsid());
        loanIQ.setSdsId(loanIQDto.getSdsId());
        loanIQ.setPortfolioCode(loanIQDto.getPortfolioCode());
        loanIQ.setAgreementId(loanIQDto.getAgreementId());
        loanIQ.setTradeDate(loanIQDto.getTradeDate());
        loanIQ.setMaturityDate(loanIQDto.getMaturityDate());
        loanIQ.setCurrency(loanIQDto.getCurrency());
        loanIQ.setPresentValue(loanIQDto.getPresentValue());
        loanIQ.setTradeAmount(loanIQDto.getTradeAmount());
        loanIQ.setRecordType(loanIQDto.getRecordType());
        loanIQ.setSwapId(loanIQDto.getSwapId());
        return loanIQRepository.save(loanIQ);
    }

    @Transactional
    public void deleteById(Integer id) {
        loanIQRepository.deleteById(id);

    }


}
