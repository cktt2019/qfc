package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToCreditSuisseCorpEntityConverter;
import com.cs.qfc.data.jpa.entities.CreditSuisseCorpEntity;
import com.cs.qfc.data.jpa.repositories.CreditSuisseCorpEntityRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedCreditSuisseCorpEntityRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CreditSuisseCorpEntityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class CreditSuisseCorpEntityService {


    private static final Logger LOG = LoggerFactory.getLogger(CreditSuisseCorpEntityService.class);

    @Autowired
    CreditSuisseCorpEntityRepository creditSuisseCorpEntityRepository;

    @Autowired
    CustomizedCreditSuisseCorpEntityRepository customizedCreditSuisseCorpEntityRepository;


    @Transactional
    public CreditSuisseCorpEntity saveCreditSuisseCorpEntity(CreditSuisseCorpEntityDTO creditSuisseCorpEntityDTO) {
        CreditSuisseCorpEntity creditSuisseCorpEntity = DTOToCreditSuisseCorpEntityConverter.convert(creditSuisseCorpEntityDTO);
        return creditSuisseCorpEntityRepository.save(creditSuisseCorpEntity);
    }

    @Transactional
    public CreditSuisseCorpEntity save(CreditSuisseCorpEntity creditSuisseCorpEntity) {
        return creditSuisseCorpEntityRepository.save(creditSuisseCorpEntity);
    }


    @Transactional
    public Collection<CreditSuisseCorpEntity> findAll() {
        return creditSuisseCorpEntityRepository.findAll();
    }

    @Transactional
    public Collection<CreditSuisseCorpEntityDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedCreditSuisseCorpEntityRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public CreditSuisseCorpEntity findById(Integer count) {
        Optional<CreditSuisseCorpEntity> creditSuisseCorpEntity = creditSuisseCorpEntityRepository.findById(count);
        return (creditSuisseCorpEntity.isPresent() ? creditSuisseCorpEntity.get() : null);
    }

    @Transactional
    public CreditSuisseCorpEntity updateCreditSuisseCorpEntity(CreditSuisseCorpEntity creditSuisseCorpEntity, CreditSuisseCorpEntityDTO creditSuisseCorpEntityDto) {

        creditSuisseCorpEntity.setLegalNameOfEntity(creditSuisseCorpEntityDto.getLegalNameOfEntity());
        creditSuisseCorpEntity.setLei(creditSuisseCorpEntityDto.getLei());
        creditSuisseCorpEntity.setImmediateParentName(creditSuisseCorpEntityDto.getImmediateParentName());
        creditSuisseCorpEntity.setImmediateParentLei(creditSuisseCorpEntityDto.getImmediateParentLei());
        creditSuisseCorpEntity.setPercentageOwnership(creditSuisseCorpEntityDto.getPercentageOwnership());
        creditSuisseCorpEntity.setEntityType(creditSuisseCorpEntityDto.getEntityType());
        creditSuisseCorpEntity.setDomicile(creditSuisseCorpEntityDto.getDomicile());
        creditSuisseCorpEntity.setJurisdictionIncorp(creditSuisseCorpEntityDto.getJurisdictionIncorp());
        return creditSuisseCorpEntityRepository.save(creditSuisseCorpEntity);
    }

    @Transactional
    public void deleteById(Integer count) {
        creditSuisseCorpEntityRepository.deleteById(count);

    }


}
