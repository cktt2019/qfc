package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToPorfolioRelationshipConverter;
import com.cs.qfc.data.jpa.entities.PorfolioRelationship;
import com.cs.qfc.data.jpa.repositories.PorfolioRelationshipRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedPorfolioRelationshipRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.PorfolioRelationshipDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class PorfolioRelationshipService {


    private static final Logger LOG = LoggerFactory.getLogger(PorfolioRelationshipService.class);

    @Autowired
    PorfolioRelationshipRepository porfolioRelationshipRepository;
    @Autowired
    CustomizedPorfolioRelationshipRepository customizedPorfolioRelationshipRepository;

    @Transactional
    public PorfolioRelationship savePorfolioRelationship(PorfolioRelationshipDTO porfolioRelationshipDTO) {
        PorfolioRelationship porfolioRelationship = DTOToPorfolioRelationshipConverter.convert(porfolioRelationshipDTO);
        return porfolioRelationshipRepository.save(porfolioRelationship);
    }

    @Transactional
    public PorfolioRelationship save(PorfolioRelationship porfolioRelationship) {
        return porfolioRelationshipRepository.save(porfolioRelationship);
    }


    @Transactional
    public Collection<PorfolioRelationship> findAll() {
        return porfolioRelationshipRepository.findAll();
    }

    @Transactional
    public Collection<PorfolioRelationshipDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedPorfolioRelationshipRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public PorfolioRelationship findById(String portfolioId) {
        Optional<PorfolioRelationship> porfolioRelationship = porfolioRelationshipRepository.findById(portfolioId);
        return (porfolioRelationship.isPresent() ? porfolioRelationship.get() : null);
    }

    @Transactional
    public PorfolioRelationship updatePorfolioRelationship(PorfolioRelationship porfolioRelationship, PorfolioRelationshipDTO porfolioRelationshipDto) {

        porfolioRelationship.setGlobalPartyId(porfolioRelationshipDto.getGlobalPartyId());
        porfolioRelationship.setPartytypeDescription(porfolioRelationshipDto.getPartytypeDescription());
        porfolioRelationship.setPartyTypeCode(porfolioRelationshipDto.getPartyTypeCode());
        porfolioRelationship.setPartyLegalName(porfolioRelationshipDto.getPartyLegalName());
        porfolioRelationship.setPartyAliasName(porfolioRelationshipDto.getPartyAliasName());
        porfolioRelationship.setLegalRoleDefinitionId(porfolioRelationshipDto.getLegalRoleDefinitionId());
        porfolioRelationship.setRoleType(porfolioRelationshipDto.getRoleType());
        porfolioRelationship.setRoleDescription(porfolioRelationshipDto.getRoleDescription());
        porfolioRelationship.setRoleCode(porfolioRelationshipDto.getRoleCode());
        porfolioRelationship.setPartyLegalRoleId(porfolioRelationshipDto.getPartyLegalRoleId());
        porfolioRelationship.setSourceId(porfolioRelationshipDto.getSourceId());
        porfolioRelationship.setSourceName(porfolioRelationshipDto.getSourceName());
        porfolioRelationship.setInternalPartyAltId(porfolioRelationshipDto.getInternalPartyAltId());
        porfolioRelationship.setPortfolioName(porfolioRelationshipDto.getPortfolioName());
        porfolioRelationship.setPortfolioNumber(porfolioRelationshipDto.getPortfolioNumber());
        porfolioRelationship.setLegalEntityName(porfolioRelationshipDto.getLegalEntityName());
        porfolioRelationship.setCmsBookingId(porfolioRelationshipDto.getCmsBookingId());
        porfolioRelationship.setEntityJurisdiction(porfolioRelationshipDto.getEntityJurisdiction());
        porfolioRelationship.setLegalEntityCode(porfolioRelationshipDto.getLegalEntityCode());
        porfolioRelationship.setParentTrxnCovrg(porfolioRelationshipDto.getParentTrxnCovrg());
        porfolioRelationship.setTrxnCoverage(porfolioRelationshipDto.getTrxnCoverage());
        porfolioRelationship.setTrxnCoverageDescr(porfolioRelationshipDto.getTrxnCoverageDescr());
        porfolioRelationship.setProductId(porfolioRelationshipDto.getProductId());
        porfolioRelationship.setAccountNumber(porfolioRelationshipDto.getAccountNumber());
        porfolioRelationship.setSystemId(porfolioRelationshipDto.getSystemId());
        porfolioRelationship.setSystemName(porfolioRelationshipDto.getSystemName());
        porfolioRelationship.setMnemonicValue(porfolioRelationshipDto.getMnemonicValue());
        porfolioRelationship.setMnemonicCode(porfolioRelationshipDto.getMnemonicCode());
        return porfolioRelationshipRepository.save(porfolioRelationship);
    }

    @Transactional
    public void deleteById(String portfolioId) {
        porfolioRelationshipRepository.deleteById(portfolioId);

    }


}
