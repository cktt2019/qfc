package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToDebtConvertibleBondConverter;
import com.cs.qfc.data.jpa.entities.DebtConvertibleBond;
import com.cs.qfc.data.jpa.repositories.DebtConvertibleBondRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedDebtConvertibleBondRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DebtConvertibleBondDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class DebtConvertibleBondService {


    private static final Logger LOG = LoggerFactory.getLogger(DebtConvertibleBondService.class);

    @Autowired
    DebtConvertibleBondRepository debtConvertibleBondRepository;
    @Autowired
    CustomizedDebtConvertibleBondRepository customizedDebtConvertibleBondRepository;

    @Transactional
    public DebtConvertibleBond saveDebtConvertibleBond(DebtConvertibleBondDTO debtConvertibleBondDTO) {
        DebtConvertibleBond debtConvertibleBond = DTOToDebtConvertibleBondConverter.convert(debtConvertibleBondDTO);
        return debtConvertibleBondRepository.save(debtConvertibleBond);
    }

    @Transactional
    public DebtConvertibleBond save(DebtConvertibleBond debtConvertibleBond) {
        return debtConvertibleBondRepository.save(debtConvertibleBond);
    }


    @Transactional
    public Collection<DebtConvertibleBond> findAll() {
        return debtConvertibleBondRepository.findAll();
    }

    @Transactional
    public Collection<DebtConvertibleBondDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedDebtConvertibleBondRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public DebtConvertibleBond findById(String sedolId) {
        Optional<DebtConvertibleBond> debtConvertibleBond = debtConvertibleBondRepository.findById(sedolId);
        return (debtConvertibleBond.isPresent() ? debtConvertibleBond.get() : null);
    }

    @Transactional
    public DebtConvertibleBond updateDebtConvertibleBond(DebtConvertibleBond debtConvertibleBond, DebtConvertibleBondDTO debtConvertibleBondDto) {

        debtConvertibleBond.setSecurityDescriptionLong(debtConvertibleBondDto.getSecurityDescriptionLong());
        debtConvertibleBond.setNxtPayDate(debtConvertibleBondDto.getNxtPayDate());
        debtConvertibleBond.setNextCallDate(debtConvertibleBondDto.getNextCallDate());
        debtConvertibleBond.setNextPutDate(debtConvertibleBondDto.getNextPutDate());
        debtConvertibleBond.setAccrualCount(debtConvertibleBondDto.getAccrualCount());
        debtConvertibleBond.setAggteIssue(debtConvertibleBondDto.getAggteIssue());
        debtConvertibleBond.setBaseCpi(debtConvertibleBondDto.getBaseCpi());
        debtConvertibleBond.setClearingHouseDer(debtConvertibleBondDto.getClearingHouseDer());
        debtConvertibleBond.setBbTicker(debtConvertibleBondDto.getBbTicker());
        debtConvertibleBond.setBookEntry(debtConvertibleBondDto.getBookEntry());
        debtConvertibleBond.setBradyBusDayConvention(debtConvertibleBondDto.getBradyBusDayConvention());
        debtConvertibleBond.setBusinessCalendar(debtConvertibleBondDto.getBusinessCalendar());
        debtConvertibleBond.setCalcTypeDes(debtConvertibleBondDto.getCalcTypeDes());
        debtConvertibleBond.setCallDiscrete(debtConvertibleBondDto.getCallDiscrete());
        debtConvertibleBond.setCallFeature(debtConvertibleBondDto.getCallFeature());
        debtConvertibleBond.setCallNotice(debtConvertibleBondDto.getCallNotice());
        debtConvertibleBond.setCallDate(debtConvertibleBondDto.getCallDate());
        debtConvertibleBond.setCallPrice(debtConvertibleBondDto.getCallPrice());
        debtConvertibleBond.setCpnFre(debtConvertibleBondDto.getCpnFre());
        debtConvertibleBond.setCpnFreqDays(debtConvertibleBondDto.getCpnFreqDays());
        debtConvertibleBond.setCouponType(debtConvertibleBondDto.getCouponType());
        debtConvertibleBond.setCouponTypeReset(debtConvertibleBondDto.getCouponTypeReset());
        debtConvertibleBond.setCpnFreqYld(debtConvertibleBondDto.getCpnFreqYld());
        debtConvertibleBond.setCsManaged(debtConvertibleBondDto.getCsManaged());
        debtConvertibleBond.setIssDate(debtConvertibleBondDto.getIssDate());
        debtConvertibleBond.setDebtStatus(debtConvertibleBondDto.getDebtStatus());
        debtConvertibleBond.setDefSecurity(debtConvertibleBondDto.getDefSecurity());
        debtConvertibleBond.setDaysToSettle(debtConvertibleBondDto.getDaysToSettle());
        debtConvertibleBond.setFwEntry(debtConvertibleBondDto.getFwEntry());
        debtConvertibleBond.setFidessaZone(debtConvertibleBondDto.getFidessaZone());
        debtConvertibleBond.setFidessaCode(debtConvertibleBondDto.getFidessaCode());
        debtConvertibleBond.setFirstCallDate(debtConvertibleBondDto.getFirstCallDate());
        debtConvertibleBond.setFirstCoupon(debtConvertibleBondDto.getFirstCoupon());
        debtConvertibleBond.setFixFltDayCount(debtConvertibleBondDto.getFixFltDayCount());
        debtConvertibleBond.setFloatRst(debtConvertibleBondDto.getFloatRst());
        debtConvertibleBond.setFloatRstIndx(debtConvertibleBondDto.getFloatRstIndx());
        debtConvertibleBond.setFltDaysPrior(debtConvertibleBondDto.getFltDaysPrior());
        debtConvertibleBond.setGlossK3(debtConvertibleBondDto.getGlossK3());
        debtConvertibleBond.setGlossL9(debtConvertibleBondDto.getGlossL9());
        debtConvertibleBond.setGlossM3(debtConvertibleBondDto.getGlossM3());
        debtConvertibleBond.setIdBb(debtConvertibleBondDto.getIdBb());
        debtConvertibleBond.setIdBbUnique(debtConvertibleBondDto.getIdBbUnique());
        debtConvertibleBond.setEpicId(debtConvertibleBondDto.getEpicId());
        debtConvertibleBond.setCmuId(debtConvertibleBondDto.getCmuId());
        debtConvertibleBond.setIsinId(debtConvertibleBondDto.getIsinId());
        debtConvertibleBond.setJapanId(debtConvertibleBondDto.getJapanId());
        debtConvertibleBond.setMiscDomesticId(debtConvertibleBondDto.getMiscDomesticId());
        debtConvertibleBond.setPoetsId(debtConvertibleBondDto.getPoetsId());
        debtConvertibleBond.setRicId(debtConvertibleBondDto.getRicId());
        debtConvertibleBond.setSedol2Id(debtConvertibleBondDto.getSedol2Id());
        debtConvertibleBond.setSingId(debtConvertibleBondDto.getSingId());
        debtConvertibleBond.setIncomeCurrency(debtConvertibleBondDto.getIncomeCurrency());
        debtConvertibleBond.setIndustrySbgprIssue(debtConvertibleBondDto.getIndustrySbgprIssue());
        debtConvertibleBond.setIntRateCap(debtConvertibleBondDto.getIntRateCap());
        debtConvertibleBond.setIntRateFl(debtConvertibleBondDto.getIntRateFl());
        debtConvertibleBond.setBojId(debtConvertibleBondDto.getBojId());
        debtConvertibleBond.setFisecId(debtConvertibleBondDto.getFisecId());
        debtConvertibleBond.setHugoId(debtConvertibleBondDto.getHugoId());
        debtConvertibleBond.setIstarId(debtConvertibleBondDto.getIstarId());
        debtConvertibleBond.setNriId(debtConvertibleBondDto.getNriId());
        debtConvertibleBond.setSmuId(debtConvertibleBondDto.getSmuId());
        debtConvertibleBond.setNrtIssuerId(debtConvertibleBondDto.getNrtIssuerId());
        debtConvertibleBond.setMarginPerc(debtConvertibleBondDto.getMarginPerc());
        debtConvertibleBond.setMaturityDate(debtConvertibleBondDto.getMaturityDate());
        debtConvertibleBond.setMuniMinTax(debtConvertibleBondDto.getMuniMinTax());
        debtConvertibleBond.setMuniFedTax(debtConvertibleBondDto.getMuniFedTax());
        debtConvertibleBond.setStateCode(debtConvertibleBondDto.getStateCode());
        debtConvertibleBond.setNextReset(debtConvertibleBondDto.getNextReset());
        debtConvertibleBond.setNriIntPayClass(debtConvertibleBondDto.getNriIntPayClass());
        debtConvertibleBond.setNriCode(debtConvertibleBondDto.getNriCode());
        debtConvertibleBond.setNriSecSub(debtConvertibleBondDto.getNriSecSub());
        debtConvertibleBond.setNriSicc(debtConvertibleBondDto.getNriSicc());
        debtConvertibleBond.setOfferingTyp(debtConvertibleBondDto.getOfferingTyp());
        debtConvertibleBond.setDatedDate(debtConvertibleBondDto.getDatedDate());
        debtConvertibleBond.setPhysical(debtConvertibleBondDto.getPhysical());
        debtConvertibleBond.setPrevPayDate(debtConvertibleBondDto.getPrevPayDate());
        debtConvertibleBond.setPrvRstDate(debtConvertibleBondDto.getPrvRstDate());
        debtConvertibleBond.setPrincFac(debtConvertibleBondDto.getPrincFac());
        debtConvertibleBond.setPrivatePlacement(debtConvertibleBondDto.getPrivatePlacement());
        debtConvertibleBond.setProRataCall(debtConvertibleBondDto.getProRataCall());
        debtConvertibleBond.setPubBalOut(debtConvertibleBondDto.getPubBalOut());
        debtConvertibleBond.setPutDisc(debtConvertibleBondDto.getPutDisc());
        debtConvertibleBond.setPutFeature(debtConvertibleBondDto.getPutFeature());
        debtConvertibleBond.setPutNotice(debtConvertibleBondDto.getPutNotice());
        debtConvertibleBond.setRatingsTrigger(debtConvertibleBondDto.getRatingsTrigger());
        debtConvertibleBond.setRefixFreq(debtConvertibleBondDto.getRefixFreq());
        debtConvertibleBond.setRegs(debtConvertibleBondDto.getRegs());
        debtConvertibleBond.setRegionCode(debtConvertibleBondDto.getRegionCode());
        debtConvertibleBond.setResetIdx(debtConvertibleBondDto.getResetIdx());
        debtConvertibleBond.setRestrictCode(debtConvertibleBondDto.getRestrictCode());
        debtConvertibleBond.setSecType(debtConvertibleBondDto.getSecType());
        debtConvertibleBond.setSinkFreq(debtConvertibleBondDto.getSinkFreq());
        debtConvertibleBond.setBenchmarkSpread(debtConvertibleBondDto.getBenchmarkSpread());
        debtConvertibleBond.setTickerSymbol(debtConvertibleBondDto.getTickerSymbol());
        debtConvertibleBond.setTradingCurrency(debtConvertibleBondDto.getTradingCurrency());
        debtConvertibleBond.setMnPiece(debtConvertibleBondDto.getMnPiece());
        debtConvertibleBond.setTraxReport(debtConvertibleBondDto.getTraxReport());
        debtConvertibleBond.setUltMatDate(debtConvertibleBondDto.getUltMatDate());
        debtConvertibleBond.setDesNotes(debtConvertibleBondDto.getDesNotes());
        debtConvertibleBond.setUltPrntCompId(debtConvertibleBondDto.getUltPrntCompId());
        debtConvertibleBond.setFisecIssuerId(debtConvertibleBondDto.getFisecIssuerId());
        debtConvertibleBond.setUltPrntTickerExchange(debtConvertibleBondDto.getUltPrntTickerExchange());
        debtConvertibleBond.setCouponFrequency(debtConvertibleBondDto.getCouponFrequency());
        debtConvertibleBond.setLondonImsDescription(debtConvertibleBondDto.getLondonImsDescription());
        debtConvertibleBond.setConRatio(debtConvertibleBondDto.getConRatio());
        debtConvertibleBond.setUndlySecIsin(debtConvertibleBondDto.getUndlySecIsin());
        debtConvertibleBond.setConvPrice(debtConvertibleBondDto.getConvPrice());
        debtConvertibleBond.setPerSecSrc01(debtConvertibleBondDto.getPerSecSrc01());
        debtConvertibleBond.setIssueDate(debtConvertibleBondDto.getIssueDate());
        debtConvertibleBond.setSpIssuerId(debtConvertibleBondDto.getSpIssuerId());
        debtConvertibleBond.setNtpaInPosition(debtConvertibleBondDto.getNtpaInPosition());
        debtConvertibleBond.setIsInternationalSukuk(debtConvertibleBondDto.getIsInternationalSukuk());
        debtConvertibleBond.setIsDomesticSukuk(debtConvertibleBondDto.getIsDomesticSukuk());
        debtConvertibleBond.setSyntheticId(debtConvertibleBondDto.getSyntheticId());
        debtConvertibleBond.setPoolCusipId(debtConvertibleBondDto.getPoolCusipId());
        debtConvertibleBond.setAuctRatePfdInd(debtConvertibleBondDto.getAuctRatePfdInd());
        debtConvertibleBond.setRepoclearFlg(debtConvertibleBondDto.getRepoclearFlg());
        debtConvertibleBond.setMuniLetterCreditIssuer(debtConvertibleBondDto.getMuniLetterCreditIssuer());
        debtConvertibleBond.setMuniCorpObligator2(debtConvertibleBondDto.getMuniCorpObligator2());
        debtConvertibleBond.setInsuranceStatus(debtConvertibleBondDto.getInsuranceStatus());
        debtConvertibleBond.setObligator(debtConvertibleBondDto.getObligator());
        debtConvertibleBond.setCpn(debtConvertibleBondDto.getCpn());
        debtConvertibleBond.setTradeCntry(debtConvertibleBondDto.getTradeCntry());
        debtConvertibleBond.setCommonId(debtConvertibleBondDto.getCommonId());
        debtConvertibleBond.setSeries(debtConvertibleBondDto.getSeries());
        debtConvertibleBond.setClassCode(debtConvertibleBondDto.getClassCode());
        return debtConvertibleBondRepository.save(debtConvertibleBond);
    }

    @Transactional
    public void deleteById(String sedolId) {
        debtConvertibleBondRepository.deleteById(sedolId);

    }


}
