package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToDepartmentConverter;
import com.cs.qfc.data.jpa.entities.Department;
import com.cs.qfc.data.jpa.repositories.DepartmentRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedDepartmentRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DepartmentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class DepartmentService {


    private static final Logger LOG = LoggerFactory.getLogger(DepartmentService.class);

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    CustomizedDepartmentRepository customizedDepartmentRepository;

    @Transactional
    public Department saveDepartment(DepartmentDTO departmentDTO) {
        Department department = DTOToDepartmentConverter.convert(departmentDTO);
        return departmentRepository.save(department);
    }

    @Transactional
    public Department save(Department department) {
        return departmentRepository.save(department);
    }


    @Transactional
    public Collection<Department> findAll() {
        return departmentRepository.findAll();
    }

    @Transactional
    public Collection<DepartmentDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedDepartmentRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public Department findById(String departmentId) {
        Optional<Department> department = departmentRepository.findById(departmentId);
        return (department.isPresent() ? department.get() : null);
    }

    @Transactional
    public Department updateDepartment(Department department, DepartmentDTO departmentDto) {

        department.setBusinessControllerId(departmentDto.getBusinessControllerId());
        department.setControllerId(departmentDto.getControllerId());
        department.setDepartmentCategoryCode(departmentDto.getDepartmentCategoryCode());
        department.setDepartmentCategoryName(departmentDto.getDepartmentCategoryName());
        department.setDepartmentDormantIndicator(departmentDto.getDepartmentDormantIndicator());
        department.setDepartmentLongName(departmentDto.getDepartmentLongName());
        department.setDepartmentName(departmentDto.getDepartmentName());
        department.setDepartmentReconcilerGroupName(departmentDto.getDepartmentReconcilerGroupName());
        department.setDepartmentStatus(departmentDto.getDepartmentStatus());
        department.setDepartmentTraderId(departmentDto.getDepartmentTraderId());
        department.setDepartmentValidFromDate(departmentDto.getDepartmentValidFromDate());
        department.setDepartmentValidToDate(departmentDto.getDepartmentValidToDate());
        department.setFinancialBusinessUnitCode(departmentDto.getFinancialBusinessUnitCode());
        department.setIvrGroup(departmentDto.getIvrGroup());
        department.setL1SupervisorId(departmentDto.getL1SupervisorId());
        department.setMisOfficeCode(departmentDto.getMisOfficeCode());
        department.setMisOfficeName(departmentDto.getMisOfficeName());
        department.setOwnerL2Id(departmentDto.getOwnerL2Id());
        department.setRecAndAdjControllerId(departmentDto.getRecAndAdjControllerId());
        department.setRecAndAdjSupervisorId(departmentDto.getRecAndAdjSupervisorId());
        department.setReconcilerId(departmentDto.getReconcilerId());
        department.setRegionCode(departmentDto.getRegionCode());
        department.setRegionDescription(departmentDto.getRegionDescription());
        department.setRevenueType(departmentDto.getRevenueType());
        department.setRevenueTypeDescription(departmentDto.getRevenueTypeDescription());
        department.setRiskPortfolioId(departmentDto.getRiskPortfolioId());
        department.setSpecialisationIndicator(departmentDto.getSpecialisationIndicator());
        department.setTaxCategory(departmentDto.getTaxCategory());
        department.setVolkerRuleDescription(departmentDto.getVolkerRuleDescription());
        department.setVolkerRuleIndicator(departmentDto.getVolkerRuleIndicator());
        department.setFxCentrallyMaangedName(departmentDto.getFxCentrallyMaangedName());
        department.setVolckerSubDivision(departmentDto.getVolckerSubDivision());
        department.setVolckerSubDivisionDescription(departmentDto.getVolckerSubDivisionDescription());
        department.setIhcHcdFlag(departmentDto.getIhcHcdFlag());
        department.setCoveredDeptFlag(departmentDto.getCoveredDeptFlag());
        department.setDeltaFlag(departmentDto.getDeltaFlag());
        department.setRemittancePolicyCode(departmentDto.getRemittancePolicyCode());
        department.setFirstInstanceDeptActivated(departmentDto.getFirstInstanceDeptActivated());
        department.setFirstInstanceDeptInactivated(departmentDto.getFirstInstanceDeptInactivated());
        department.setLastInstanceDeptActivated(departmentDto.getLastInstanceDeptActivated());
        department.setLastInstanceDeptInactivated(departmentDto.getLastInstanceDeptInactivated());
        department.setRbpnlFlag(departmentDto.getRbpnlFlag());
        department.setIvrGroupLongName(departmentDto.getIvrGroupLongName());
        department.setFrtbDeptCategory(departmentDto.getFrtbDeptCategory());
        return departmentRepository.save(department);
    }

    @Transactional
    public void deleteById(String departmentId) {
        departmentRepository.deleteById(departmentId);

    }


}
