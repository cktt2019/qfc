package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToNTPAMargin5GConverter;
import com.cs.qfc.data.jpa.entities.NTPAMargin5G;
import com.cs.qfc.data.jpa.repositories.NTPAMargin5GRepository;
import com.cs.qfc.data.model.NTPAMargin5GDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class NTPAMargin5GService {


    private static final Logger LOG = LoggerFactory.getLogger(NTPAMargin5GService.class);

    @Autowired
    NTPAMargin5GRepository nTPAMargin5GRepository;

    @Transactional
    public NTPAMargin5G saveNTPAMargin5G(NTPAMargin5GDTO nTPAMargin5GDTO) {
        NTPAMargin5G nTPAMargin5G = DTOToNTPAMargin5GConverter.convert(nTPAMargin5GDTO);
        return nTPAMargin5GRepository.save(nTPAMargin5G);
    }

    @Transactional
    public NTPAMargin5G save(NTPAMargin5G nTPAMargin5G) {
        return nTPAMargin5GRepository.save(nTPAMargin5G);
    }


    @Transactional
    public Collection<NTPAMargin5G> findAll() {
        return nTPAMargin5GRepository.findAll();
    }

    @Transactional
    public NTPAMargin5G findById(Integer id) {
        Optional<NTPAMargin5G> nTPAMargin5G = nTPAMargin5GRepository.findById(id);
        return (nTPAMargin5G.isPresent() ? nTPAMargin5G.get() : null);
    }

    @Transactional
    public NTPAMargin5G updateNTPAMargin5G(NTPAMargin5G nTPAMargin5G, NTPAMargin5GDTO nTPAMargin5GDto) {

        nTPAMargin5G.setBusinessDate(nTPAMargin5GDto.getBusinessDate());
        nTPAMargin5G.setRunDate(nTPAMargin5GDto.getRunDate());
        nTPAMargin5G.setAccount(nTPAMargin5GDto.getAccount());
        nTPAMargin5G.setCurrId(nTPAMargin5GDto.getCurrId());
        nTPAMargin5G.setLiquidatingEquity(nTPAMargin5GDto.getLiquidatingEquity());
        nTPAMargin5G.setMarginEquity(nTPAMargin5GDto.getMarginEquity());
        nTPAMargin5G.setRegTRequirements(nTPAMargin5GDto.getRegTRequirements());
        nTPAMargin5G.setMaintRequirements(nTPAMargin5GDto.getMaintRequirements());
        nTPAMargin5G.setNyseRequirements(nTPAMargin5GDto.getNyseRequirements());
        nTPAMargin5G.setRegtExcessDeficit(nTPAMargin5GDto.getRegtExcessDeficit());
        nTPAMargin5G.setMaintExcessDeficit(nTPAMargin5GDto.getMaintExcessDeficit());
        nTPAMargin5G.setNyseExcessDeficit(nTPAMargin5GDto.getNyseExcessDeficit());
        nTPAMargin5G.setSma(nTPAMargin5GDto.getSma());
        nTPAMargin5G.setCashAvailable(nTPAMargin5GDto.getCashAvailable());
        nTPAMargin5G.setTotalTdBalance(nTPAMargin5GDto.getTotalTdBalance());
        nTPAMargin5G.setTotalTdLongMktValue(nTPAMargin5GDto.getTotalTdLongMktValue());
        nTPAMargin5G.setTotalTdShortMktValue(nTPAMargin5GDto.getTotalTdShortMktValue());
        nTPAMargin5G.setTotalOptionLongMktValue(nTPAMargin5GDto.getTotalOptionLongMktValue());
        nTPAMargin5G.setTotalOptionShortMktValue(nTPAMargin5GDto.getTotalOptionShortMktValue());
        nTPAMargin5G.setTotalSdBalance(nTPAMargin5GDto.getTotalSdBalance());
        nTPAMargin5G.setTotalSdLongMktValue(nTPAMargin5GDto.getTotalSdLongMktValue());
        nTPAMargin5G.setTotalSdShortMktValue(nTPAMargin5GDto.getTotalSdShortMktValue());
        nTPAMargin5G.setTotalAssessedBalance(nTPAMargin5GDto.getTotalAssessedBalance());
        nTPAMargin5G.setTotalInterestAccrual(nTPAMargin5GDto.getTotalInterestAccrual());
        nTPAMargin5G.setType(nTPAMargin5GDto.getType());
        nTPAMargin5G.setTdBalance(nTPAMargin5GDto.getTdBalance());
        nTPAMargin5G.setTdMarketValLng(nTPAMargin5GDto.getTdMarketValLng());
        nTPAMargin5G.setTdMarketValShrt(nTPAMargin5GDto.getTdMarketValShrt());
        nTPAMargin5G.setSdBalance(nTPAMargin5GDto.getSdBalance());
        nTPAMargin5G.setSdMarketValue(nTPAMargin5GDto.getSdMarketValue());
        nTPAMargin5G.setLegalEntityId(nTPAMargin5GDto.getLegalEntityId());
        nTPAMargin5G.setNtpaProductId(nTPAMargin5GDto.getNtpaProductId());
        return nTPAMargin5GRepository.save(nTPAMargin5G);
    }

    @Transactional
    public void deleteById(Integer id) {
        nTPAMargin5GRepository.deleteById(id);

    }


}
