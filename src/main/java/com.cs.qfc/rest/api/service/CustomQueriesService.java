package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToCustomQueriesConverter;
import com.cs.qfc.data.jpa.entities.CustomQueries;
import com.cs.qfc.data.jpa.repositories.CustomQueriesRepository;
import com.cs.qfc.data.model.CustomQueriesDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class CustomQueriesService {


    private static final Logger LOG = LoggerFactory.getLogger(CustomQueriesService.class);

    @Autowired
    CustomQueriesRepository customQueriesRepository;

    @Transactional
    public CustomQueries saveCustomQueries(CustomQueriesDTO customQueriesDTO) {
        CustomQueries customQueries = DTOToCustomQueriesConverter.convert(customQueriesDTO);
        return customQueriesRepository.save(customQueries);
    }

    @Transactional
    public CustomQueries save(CustomQueries customQueries) {
        return customQueriesRepository.save(customQueries);
    }


    @Transactional
    public Collection<CustomQueries> findAll() {
        return customQueriesRepository.findAll();
    }

    @Transactional
    public CustomQueries findById(String criteriaName) {
        Optional<CustomQueries> customQueries = customQueriesRepository.findById(criteriaName);
        return (customQueries.isPresent() ? customQueries.get() : null);
    }

    @Transactional
    public Collection<CustomQueries> findByEntity(String entityName) {
        return customQueriesRepository.findByEntityName(entityName);

    }


    @Transactional
    public CustomQueries updateCustomQueries(CustomQueries customQueries, CustomQueriesDTO customQueriesDto) {

        customQueries.setEntityName(customQueriesDto.getEntityName());
        customQueries.setCriteriaDescription(customQueriesDto.getCriteriaDescription());
        customQueries.setCriteriaContent(customQueriesDto.getCriteriaContent());
        return customQueriesRepository.save(customQueries);
    }

    @Transactional
    public void deleteById(String criteriaName) {
        customQueriesRepository.deleteById(criteriaName);

    }


}
