package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToVisionConverter;
import com.cs.qfc.data.jpa.entities.Vision;
import com.cs.qfc.data.jpa.repositories.VisionRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedVisionRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.VisionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class VisionService {


    private static final Logger LOG = LoggerFactory.getLogger(VisionService.class);

    @Autowired
    VisionRepository visionRepository;
    @Autowired
    CustomizedVisionRepository customizedVisionRepository;

    @Transactional
    public Vision saveVision(VisionDTO visionDTO) {
        Vision vision = DTOToVisionConverter.convert(visionDTO);
        return visionRepository.save(vision);
    }

    @Transactional
    public Vision save(Vision vision) {
        return visionRepository.save(vision);
    }


    @Transactional
    public Collection<Vision> findAll() {
        return visionRepository.findAll();
    }

    @Transactional
    public Collection<VisionDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedVisionRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public Vision findById(Integer id) {
        Optional<Vision> vision = visionRepository.findById(id);
        return (vision.isPresent() ? vision.get() : null);
    }

    @Transactional
    public Vision updateVision(Vision vision, VisionDTO visionDto) {

        vision.setBusinessDate(visionDto.getBusinessDate());
        vision.setEntity(visionDto.getEntity());
        vision.setCpty(visionDto.getCpty());
        vision.setBook(visionDto.getBook());
        vision.setProveNameProduct(visionDto.getProveNameProduct());
        vision.setAgreementId1(visionDto.getAgreementId1());
        vision.setTradeDate(visionDto.getTradeDate());
        vision.setEndDate(visionDto.getEndDate());
        vision.setBondNextCoup(visionDto.getBondNextCoup());
        vision.setCashCurr(visionDto.getCashCurr());
        vision.setPvUsd(visionDto.getPvUsd());
        vision.setDefaultFvLevel(visionDto.getDefaultFvLevel());
        vision.setFace(visionDto.getFace());
        vision.setValueOn(visionDto.getValueOn());
        vision.setRehypothecationIndicator(visionDto.getRehypothecationIndicator());
        vision.setCustodianId(visionDto.getCustodianId());
        return visionRepository.save(vision);
    }

    @Transactional
    public void deleteById(Integer id) {
        visionRepository.deleteById(id);

    }


}
