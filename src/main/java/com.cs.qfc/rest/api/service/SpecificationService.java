package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToSpecificationConverter;
import com.cs.qfc.data.jpa.entities.Specification;
import com.cs.qfc.data.jpa.repositories.SpecificationRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedSpecificationRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.SpecificationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class SpecificationService {


    private static final Logger LOG = LoggerFactory.getLogger(SpecificationService.class);

    @Autowired
    SpecificationRepository specificationRepository;

    @Autowired
    CustomizedSpecificationRepository customizedSpecificationRepository;


    @Transactional
    public Specification saveSpecification(SpecificationDTO specificationDTO) {
        Specification specification = DTOToSpecificationConverter.convert(specificationDTO);
        return specificationRepository.save(specification);
    }

    @Transactional
    public Specification save(Specification specification) {
        return specificationRepository.save(specification);
    }


    @Transactional
    public Collection<Specification> findAll() {
        return specificationRepository.findAll();
    }

    @Transactional
    public Collection<SpecificationDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedSpecificationRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public Specification findById(Integer id) {
        Optional<Specification> specification = specificationRepository.findById(id);
        return (specification.isPresent() ? specification.get() : null);
    }

    @Transactional
    public Specification updateSpecification(Specification specification, SpecificationDTO specificationDto) {

        specification.setSpecificationName(specificationDto.getSpecificationName());
        specification.setSpecificationDescription(specificationDto.getSpecificationDescription());
        specification.setSpecificationVersion(specificationDto.getSpecificationVersion());
        specification.setSpecificationContent(specificationDto.getSpecificationContent());
        return specificationRepository.save(specification);
    }

    @Transactional
    public void deleteById(Integer id) {
        specificationRepository.deleteById(id);

    }


}
