package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToDrdTradeHeaderConverter;
import com.cs.qfc.data.jpa.entities.DrdTradeHeader;
import com.cs.qfc.data.jpa.repositories.DrdTradeHeaderRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedDrdTradeHeaderRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DrdTradeHeaderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;


@Component
public class DrdTradeHeaderService {


    private static final Logger LOG = LoggerFactory.getLogger(DrdTradeHeaderService.class);

    @Autowired
    DrdTradeHeaderRepository drdTradeHeaderRepository;
    @Autowired
    CustomizedDrdTradeHeaderRepository customizedDrdTradeHeaderRepository;

    @Transactional
    public DrdTradeHeader saveDrdTradeHeader(DrdTradeHeaderDTO drdTradeHeaderDTO) {
        DrdTradeHeader drdTradeHeader = DTOToDrdTradeHeaderConverter.convert(drdTradeHeaderDTO);
        return drdTradeHeaderRepository.save(drdTradeHeader);
    }

    @Transactional
    public DrdTradeHeader save(DrdTradeHeader drdTradeHeader) {
        return drdTradeHeaderRepository.save(drdTradeHeader);
    }


    @Transactional
    public Collection<DrdTradeHeader> findAll() {
        return drdTradeHeaderRepository.findAll();
    }

    @Transactional
    public Collection<DrdTradeHeaderDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedDrdTradeHeaderRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public DrdTradeHeader findById(Integer id) {
        Optional<DrdTradeHeader> drdTradeHeader = drdTradeHeaderRepository.findById(id);
        return (drdTradeHeader.isPresent() ? drdTradeHeader.get() : null);
    }

    @Transactional
    public DrdTradeHeader updateDrdTradeHeader(DrdTradeHeader drdTradeHeader, DrdTradeHeaderDTO drdTradeHeaderDto) {

        drdTradeHeader.setCobDate(drdTradeHeaderDto.getCobDate());
        drdTradeHeader.setLoadCodDate(drdTradeHeaderDto.getLoadCodDate());
        drdTradeHeader.setBookName(drdTradeHeaderDto.getBookName());
        drdTradeHeader.setTradeLoadId(drdTradeHeaderDto.getTradeLoadId());
        drdTradeHeader.setLoadTimestamp(drdTradeHeaderDto.getLoadTimestamp());
        drdTradeHeader.setSnapshotName(drdTradeHeaderDto.getSnapshotName());
        drdTradeHeader.setSnapshotId(drdTradeHeaderDto.getSnapshotId());
        drdTradeHeader.setSnapshotCreationDatetime(drdTradeHeaderDto.getSnapshotCreationDatetime());
        drdTradeHeader.setInterfaceVersion(drdTradeHeaderDto.getInterfaceVersion());
        drdTradeHeader.setTradeId(drdTradeHeaderDto.getTradeId());
        drdTradeHeader.setTfTradeVersion(drdTradeHeaderDto.getTfTradeVersion());
        drdTradeHeader.setTradeLocationId(drdTradeHeaderDto.getTradeLocationId());
        drdTradeHeader.setPrimaryTcn(drdTradeHeaderDto.getPrimaryTcn());
        drdTradeHeader.setTradeVersionUpdateTime(drdTradeHeaderDto.getTradeVersionUpdateTime());
        drdTradeHeader.setCancellationDate(drdTradeHeaderDto.getCancellationDate());
        drdTradeHeader.setXccyIndicator(drdTradeHeaderDto.getXccyIndicator());
        drdTradeHeader.setGbmEntity(drdTradeHeaderDto.getGbmEntity());
        drdTradeHeader.setTotalLocalPv(drdTradeHeaderDto.getTotalLocalPv());
        drdTradeHeader.setTotalLocalPvCcyCode(drdTradeHeaderDto.getTotalLocalPvCcyCode());
        drdTradeHeader.setTotalPvStatus(drdTradeHeaderDto.getTotalPvStatus());
        drdTradeHeader.setTotalRpTpv(drdTradeHeaderDto.getTotalRpTpv());
        drdTradeHeader.setTradePvRegDate(drdTradeHeaderDto.getTradePvRegDate());
        drdTradeHeader.setTradePvRegDays(drdTradeHeaderDto.getTradePvRegDays());
        drdTradeHeader.setConfirmationStatus(drdTradeHeaderDto.getConfirmationStatus());
        drdTradeHeader.setDefaultFairValue(drdTradeHeaderDto.getDefaultFairValue());
        drdTradeHeader.setProveCluster(drdTradeHeaderDto.getProveCluster());
        drdTradeHeader.setProveFamily(drdTradeHeaderDto.getProveFamily());
        drdTradeHeader.setProveId(drdTradeHeaderDto.getProveId());
        drdTradeHeader.setProveProduct(drdTradeHeaderDto.getProveProduct());
        drdTradeHeader.setApprovalStatus(drdTradeHeaderDto.getApprovalStatus());
        drdTradeHeader.setEventReason(drdTradeHeaderDto.getEventReason());
        drdTradeHeader.setExternalId(drdTradeHeaderDto.getExternalId());
        drdTradeHeader.setPrimaryMaturityDate(drdTradeHeaderDto.getPrimaryMaturityDate());
        drdTradeHeader.setProductSubType(drdTradeHeaderDto.getProductSubType());
        drdTradeHeader.setProductType(drdTradeHeaderDto.getProductType());
        drdTradeHeader.setProductValuationType(drdTradeHeaderDto.getProductValuationType());
        drdTradeHeader.setRootTcn(drdTradeHeaderDto.getRootTcn());
        drdTradeHeader.setTradeStatus(drdTradeHeaderDto.getTradeStatus());
        drdTradeHeader.setCduMasterAgreementLlk(drdTradeHeaderDto.getCduMasterAgreementLlk());
        drdTradeHeader.setCduCollateralAnnexLlk(drdTradeHeaderDto.getCduCollateralAnnexLlk());
        drdTradeHeader.setFreamesoftMasterAgreementLlk(drdTradeHeaderDto.getFreamesoftMasterAgreementLlk());
        drdTradeHeader.setAlgoCollateralAnnexLlk(drdTradeHeaderDto.getAlgoCollateralAnnexLlk());
        drdTradeHeader.setAccountArea(drdTradeHeaderDto.getAccountArea());
        drdTradeHeader.setBrokerId(drdTradeHeaderDto.getBrokerId());
        drdTradeHeader.setCollIndepAmount(drdTradeHeaderDto.getCollIndepAmount());
        drdTradeHeader.setCollIndepCcy(drdTradeHeaderDto.getCollIndepCcy());
        drdTradeHeader.setCollIndepOtherMargQuote(drdTradeHeaderDto.getCollIndepOtherMargQuote());
        drdTradeHeader.setCollIndepPercent(drdTradeHeaderDto.getCollIndepPercent());
        drdTradeHeader.setCollIndepType(drdTradeHeaderDto.getCollIndepType());
        drdTradeHeader.setMarketerId(drdTradeHeaderDto.getMarketerId());
        drdTradeHeader.setRiskManagementOnlyFlag(drdTradeHeaderDto.getRiskManagementOnlyFlag());
        drdTradeHeader.setStructuredTradeFlag(drdTradeHeaderDto.getStructuredTradeFlag());
        drdTradeHeader.setTradeDate(drdTradeHeaderDto.getTradeDate());
        drdTradeHeader.setTraderName(drdTradeHeaderDto.getTraderName());
        drdTradeHeader.setTradeSourceSystem(drdTradeHeaderDto.getTradeSourceSystem());
        drdTradeHeader.setTradingPartyId(drdTradeHeaderDto.getTradingPartyId());
        drdTradeHeader.setTradingPartyIdType(drdTradeHeaderDto.getTradingPartyIdType());
        drdTradeHeader.setCity(drdTradeHeaderDto.getCity());
        drdTradeHeader.setDeal(drdTradeHeaderDto.getDeal());
        drdTradeHeader.setStripId(drdTradeHeaderDto.getStripId());
        drdTradeHeader.setCustomerNumber(drdTradeHeaderDto.getCustomerNumber());
        drdTradeHeader.setDisplayId(drdTradeHeaderDto.getDisplayId());
        drdTradeHeader.setAssetClass1(drdTradeHeaderDto.getAssetClass1());
        drdTradeHeader.setAssetClass2(drdTradeHeaderDto.getAssetClass2());
        drdTradeHeader.setAssetClass3(drdTradeHeaderDto.getAssetClass3());
        drdTradeHeader.setAssetClass4(drdTradeHeaderDto.getAssetClass4());
        drdTradeHeader.setLinkedSetType(drdTradeHeaderDto.getLinkedSetType());
        drdTradeHeader.setSysLinkId(drdTradeHeaderDto.getSysLinkId());
        drdTradeHeader.setLeadTradeId(drdTradeHeaderDto.getLeadTradeId());
        drdTradeHeader.setLeadTradeVersion(drdTradeHeaderDto.getLeadTradeVersion());
        drdTradeHeader.setLeadTradeLocationId(drdTradeHeaderDto.getLeadTradeLocationId());
        drdTradeHeader.setLeadExternalId(drdTradeHeaderDto.getLeadExternalId());
        drdTradeHeader.setLeadTradeBook(drdTradeHeaderDto.getLeadTradeBook());
        drdTradeHeader.setWashTradeId(drdTradeHeaderDto.getWashTradeId());
        drdTradeHeader.setWashTradeVersion(drdTradeHeaderDto.getWashTradeVersion());
        drdTradeHeader.setWashTradeLocationId(drdTradeHeaderDto.getWashTradeLocationId());
        drdTradeHeader.setWashExternalId(drdTradeHeaderDto.getWashExternalId());
        drdTradeHeader.setWashTradeBook(drdTradeHeaderDto.getWashTradeBook());
        drdTradeHeader.setRiskTradeId(drdTradeHeaderDto.getRiskTradeId());
        drdTradeHeader.setRiskTradeVersion(drdTradeHeaderDto.getRiskTradeVersion());
        drdTradeHeader.setRiskTradeLocationId(drdTradeHeaderDto.getRiskTradeLocationId());
        drdTradeHeader.setRiskExternalId(drdTradeHeaderDto.getRiskExternalId());
        drdTradeHeader.setRiskTradeBook(drdTradeHeaderDto.getRiskTradeBook());
        drdTradeHeader.setSyntheticTradeIndicator(drdTradeHeaderDto.getSyntheticTradeIndicator());
        drdTradeHeader.setLinkageStatus(drdTradeHeaderDto.getLinkageStatus());
        drdTradeHeader.setLinkageStatusDescription(drdTradeHeaderDto.getLinkageStatusDescription());
        drdTradeHeader.setLinkedTradeId(drdTradeHeaderDto.getLinkedTradeId());
        drdTradeHeader.setLinkedPrimaryTcn(drdTradeHeaderDto.getLinkedPrimaryTcn());
        drdTradeHeader.setLinkedTradeVersion(drdTradeHeaderDto.getLinkedTradeVersion());
        drdTradeHeader.setLinkedTradeLocationId(drdTradeHeaderDto.getLinkedTradeLocationId());
        drdTradeHeader.setLinkedTradeExternalId(drdTradeHeaderDto.getLinkedTradeExternalId());
        drdTradeHeader.setLinkedTradeBook(drdTradeHeaderDto.getLinkedTradeBook());
        drdTradeHeader.setValuationClass(drdTradeHeaderDto.getValuationClass());
        drdTradeHeader.setValuationType(drdTradeHeaderDto.getValuationType());
        drdTradeHeader.setPremiumAmount(drdTradeHeaderDto.getPremiumAmount());
        drdTradeHeader.setSwapsWireId(drdTradeHeaderDto.getSwapsWireId());
        drdTradeHeader.setApproxBooking(drdTradeHeaderDto.getApproxBooking());
        drdTradeHeader.setApproxBookingReviewDate(drdTradeHeaderDto.getApproxBookingReviewDate());
        drdTradeHeader.setDocsCounterPartyId(drdTradeHeaderDto.getDocsCounterPartyId());
        drdTradeHeader.setCloIndicator(drdTradeHeaderDto.getCloIndicator());
        drdTradeHeader.setLoanMitigationFlag(drdTradeHeaderDto.getLoanMitigationFlag());
        drdTradeHeader.setUsi(drdTradeHeaderDto.getUsi());
        drdTradeHeader.setCssi(drdTradeHeaderDto.getCssi());
        drdTradeHeader.setGmbBookId(drdTradeHeaderDto.getGmbBookId());
        drdTradeHeader.setArea(drdTradeHeaderDto.getArea());
        drdTradeHeader.setStructuredId(drdTradeHeaderDto.getStructuredId());
        drdTradeHeader.setPvLevel(drdTradeHeaderDto.getPvLevel());
        drdTradeHeader.setCounterPartyCsid(drdTradeHeaderDto.getCounterPartyCsid());
        drdTradeHeader.setCreeTradeIdentifier(drdTradeHeaderDto.getCreeTradeIdentifier());
        drdTradeHeader.setCreeTradeIdentifierVersion(drdTradeHeaderDto.getCreeTradeIdentifierVersion());
        drdTradeHeader.setLchOrigCounterParty(drdTradeHeaderDto.getLchOrigCounterParty());
        drdTradeHeader.setGbmCluster(drdTradeHeaderDto.getGbmCluster());
        drdTradeHeader.setGbmDivision(drdTradeHeaderDto.getGbmDivision());
        drdTradeHeader.setAffirmPlatformStatus(drdTradeHeaderDto.getAffirmPlatformStatus());
        drdTradeHeader.setDocsActualProcessingMethod(drdTradeHeaderDto.getDocsActualProcessingMethod());
        drdTradeHeader.setUti(drdTradeHeaderDto.getUti());
        drdTradeHeader.setSwireManualConfirmedReq(drdTradeHeaderDto.getSwireManualConfirmedReq());
        drdTradeHeader.setEntryDateTime(drdTradeHeaderDto.getEntryDateTime());
        drdTradeHeader.setFundingIndicator(drdTradeHeaderDto.getFundingIndicator());
        drdTradeHeader.setLeverageFactor(drdTradeHeaderDto.getLeverageFactor());
        drdTradeHeader.setLinkTransId(drdTradeHeaderDto.getLinkTransId());
        drdTradeHeader.setLinkType(drdTradeHeaderDto.getLinkType());
        drdTradeHeader.setMasterTradeTradeId(drdTradeHeaderDto.getMasterTradeTradeId());
        drdTradeHeader.setGbmBookRef(drdTradeHeaderDto.getGbmBookRef());
        drdTradeHeader.setLastAmmendBy(drdTradeHeaderDto.getLastAmmendBy());
        drdTradeHeader.setStructureType(drdTradeHeaderDto.getStructureType());
        drdTradeHeader.setGarantiaSysId(drdTradeHeaderDto.getGarantiaSysId());
        drdTradeHeader.setEarlyTmTermParty(drdTradeHeaderDto.getEarlyTmTermParty());
        drdTradeHeader.setEarlyTmOptStartDate(drdTradeHeaderDto.getEarlyTmOptStartDate());
        drdTradeHeader.setCounterPartyGsid(drdTradeHeaderDto.getCounterPartyGsid());
        drdTradeHeader.setEntityType(drdTradeHeaderDto.getEntityType());
        drdTradeHeader.setEvent(drdTradeHeaderDto.getEvent());
        drdTradeHeader.setEventTradeDate(drdTradeHeaderDto.getEventTradeDate());
        drdTradeHeader.setFidcPmInitialMargin(drdTradeHeaderDto.getFidcPmInitialMargin());
        drdTradeHeader.setFidcPmNettingOnly(drdTradeHeaderDto.getFidcPmNettingOnly());
        drdTradeHeader.setFidcPmVariationMargin(drdTradeHeaderDto.getFidcPmVariationMargin());
        drdTradeHeader.setLateTrdNotificationReason(drdTradeHeaderDto.getLateTrdNotificationReason());
        drdTradeHeader.setMasterAgreementType(drdTradeHeaderDto.getMasterAgreementType());
        drdTradeHeader.setNotesFrontOffice(drdTradeHeaderDto.getNotesFrontOffice());
        drdTradeHeader.setRiskAdvisor(drdTradeHeaderDto.getRiskAdvisor());
        drdTradeHeader.setRiskTrader(drdTradeHeaderDto.getRiskTrader());
        drdTradeHeader.setTradingPartyGsid(drdTradeHeaderDto.getTradingPartyGsid());
        drdTradeHeader.setFidcPmInd(drdTradeHeaderDto.getFidcPmInd());
        drdTradeHeader.setAgent(drdTradeHeaderDto.getAgent());
        drdTradeHeader.setPaymentIndicator(drdTradeHeaderDto.getPaymentIndicator());
        drdTradeHeader.setRenegotiationDate(drdTradeHeaderDto.getRenegotiationDate());
        drdTradeHeader.setAlternativeVersionId(drdTradeHeaderDto.getAlternativeVersionId());
        drdTradeHeader.setCifbu(drdTradeHeaderDto.getCifbu());
        drdTradeHeader.setDeskBook(drdTradeHeaderDto.getDeskBook());
        drdTradeHeader.setMetalInkTradeId(drdTradeHeaderDto.getMetalInkTradeId());
        drdTradeHeader.setLinkedStartDate(drdTradeHeaderDto.getLinkedStartDate());
        drdTradeHeader.setLinkedEndDate(drdTradeHeaderDto.getLinkedEndDate());
        drdTradeHeader.setSalespersonId(drdTradeHeaderDto.getSalespersonId());
        drdTradeHeader.setSourceSystemId(drdTradeHeaderDto.getSourceSystemId());
        drdTradeHeader.setSourceSystemVersion(drdTradeHeaderDto.getSourceSystemVersion());
        drdTradeHeader.setEarlyTmExerciseFreq(drdTradeHeaderDto.getEarlyTmExerciseFreq());
        drdTradeHeader.setEarlyTmOptStyle(drdTradeHeaderDto.getEarlyTmOptStyle());
        drdTradeHeader.setPaymentPackageSysLinkId(drdTradeHeaderDto.getPaymentPackageSysLinkId());
        drdTradeHeader.setTfMatchStatus(drdTradeHeaderDto.getTfMatchStatus());
        drdTradeHeader.setBestMatchTfTradeVersion(drdTradeHeaderDto.getBestMatchTfTradeVersion());
        return drdTradeHeaderRepository.save(drdTradeHeader);
    }

    @Transactional
    public void deleteById(Integer id) {
        drdTradeHeaderRepository.deleteById(id);

    }


}
