package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToAgreementTitleScopeConverter;
import com.cs.qfc.data.jpa.entities.AgreementTitleScope;
import com.cs.qfc.data.jpa.repositories.AgreementTitleScopeRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedAgreementTitleScopeRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.AgreementTitleScopeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class AgreementTitleScopeService {


    private static final Logger LOG = LoggerFactory.getLogger(AgreementTitleScopeService.class);

    @Autowired
    AgreementTitleScopeRepository agreementTitleScopeRepository;

    @Autowired
    CustomizedAgreementTitleScopeRepository customizedAgreementTitleScopeRepository;


    @Transactional
    public AgreementTitleScope saveAgreementTitleScope(AgreementTitleScopeDTO agreementTitleScopeDTO) {
        AgreementTitleScope agreementTitleScope = DTOToAgreementTitleScopeConverter.convert(agreementTitleScopeDTO);
        return agreementTitleScopeRepository.save(agreementTitleScope);
    }

    @Transactional
    public AgreementTitleScope save(AgreementTitleScope agreementTitleScope) {
        return agreementTitleScopeRepository.save(agreementTitleScope);
    }


    @Transactional
    public Collection<AgreementTitleScope> findAll() {
        return agreementTitleScopeRepository.findAll();
    }

    @Transactional
    public Collection<AgreementTitleScopeDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedAgreementTitleScopeRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public AgreementTitleScope findById(Integer count) {
        Optional<AgreementTitleScope> agreementTitleScope = agreementTitleScopeRepository.findById(count);
        return (agreementTitleScope.isPresent() ? agreementTitleScope.get() : null);
    }

    @Transactional
    public AgreementTitleScope updateAgreementTitleScope(AgreementTitleScope agreementTitleScope, AgreementTitleScopeDTO agreementTitleScopeDto) {

        agreementTitleScope.setAgreementTitle(agreementTitleScopeDto.getAgreementTitle());
        agreementTitleScope.setAgreementType(agreementTitleScopeDto.getAgreementType());
        agreementTitleScope.setInScope(agreementTitleScopeDto.getInScope());
        return agreementTitleScopeRepository.save(agreementTitleScope);
    }

    @Transactional
    public void deleteById(Integer count) {
        agreementTitleScopeRepository.deleteById(count);

    }


}
