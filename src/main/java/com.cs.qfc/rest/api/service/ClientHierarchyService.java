package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToClientHierarchyConverter;
import com.cs.qfc.data.jpa.entities.ClientHierarchy;
import com.cs.qfc.data.jpa.repositories.ClientHierarchyRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedClientHierarchyRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.ClientHierarchyDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class ClientHierarchyService {


    private static final Logger LOG = LoggerFactory.getLogger(ClientHierarchyService.class);

    @Autowired
    ClientHierarchyRepository clientHierarchyRepository;

    @Autowired
    CustomizedClientHierarchyRepository customizedClientHierarchyRepository;

    @Transactional
    public ClientHierarchy saveClientHierarchy(ClientHierarchyDTO clientHierarchyDTO) {
        ClientHierarchy clientHierarchy = DTOToClientHierarchyConverter.convert(clientHierarchyDTO);
        return clientHierarchyRepository.save(clientHierarchy);
    }

    @Transactional
    public ClientHierarchy save(ClientHierarchy clientHierarchy) {
        return clientHierarchyRepository.save(clientHierarchy);
    }


    @Transactional
    public Collection<ClientHierarchy> findAll() {
        return clientHierarchyRepository.findAll();
    }

    @Transactional
    public Collection<ClientHierarchyDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedClientHierarchyRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public ClientHierarchy findById(Integer id) {
        Optional<ClientHierarchy> clientHierarchy = clientHierarchyRepository.findById(id);
        return (clientHierarchy.isPresent() ? clientHierarchy.get() : null);
    }

    @Transactional
    public ClientHierarchy updateClientHierarchy(ClientHierarchy clientHierarchy, ClientHierarchyDTO clientHierarchyDto) {

        clientHierarchy.setCsid(clientHierarchyDto.getCsid());
        clientHierarchy.setGsid(clientHierarchyDto.getGsid());
        clientHierarchy.setCounterPartyName(clientHierarchyDto.getCounterPartyName());
        clientHierarchy.setImmediateParentCsid(clientHierarchyDto.getImmediateParentCsid());
        clientHierarchy.setImmediateParentGsid(clientHierarchyDto.getImmediateParentGsid());
        clientHierarchy.setUltimateParentCsid(clientHierarchyDto.getUltimateParentCsid());
        clientHierarchy.setUltimateParentGsid(clientHierarchyDto.getUltimateParentGsid());
        return clientHierarchyRepository.save(clientHierarchy);
    }

    @Transactional
    public void deleteById(Integer id) {
        clientHierarchyRepository.deleteById(id);

    }


}
