package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToAgreementTitlesScopeConverter;
import com.cs.qfc.data.jpa.entities.AgreementTitlesScope;
import com.cs.qfc.data.jpa.repositories.AgreementTitlesScopeRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedAgreementTitlesScopeRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.AgreementTitlesScopeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class AgreementTitlesScopeService {


    private static final Logger LOG = LoggerFactory.getLogger(AgreementTitlesScopeService.class);

    @Autowired
    AgreementTitlesScopeRepository agreementTitlesScopeRepository;

    @Autowired
    CustomizedAgreementTitlesScopeRepository customizedAgreementTitlesScopeRepository;


    @Transactional
    public AgreementTitlesScope saveAgreementTitlesScope(AgreementTitlesScopeDTO agreementTitlesScopeDTO) {
        AgreementTitlesScope agreementTitlesScope = DTOToAgreementTitlesScopeConverter.convert(agreementTitlesScopeDTO);
        return agreementTitlesScopeRepository.save(agreementTitlesScope);
    }

    @Transactional
    public AgreementTitlesScope save(AgreementTitlesScope agreementTitlesScope) {
        return agreementTitlesScopeRepository.save(agreementTitlesScope);
    }


    @Transactional
    public Collection<AgreementTitlesScope> findAll() {
        return agreementTitlesScopeRepository.findAll();
    }

    @Transactional
    public Collection<AgreementTitlesScopeDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedAgreementTitlesScopeRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public AgreementTitlesScope findById(Integer count) {
        Optional<AgreementTitlesScope> agreementTitlesScope = agreementTitlesScopeRepository.findById(count);
        return (agreementTitlesScope.isPresent() ? agreementTitlesScope.get() : null);
    }

    @Transactional
    public AgreementTitlesScope updateAgreementTitlesScope(AgreementTitlesScope agreementTitlesScope, AgreementTitlesScopeDTO agreementTitlesScopeDto) {

        agreementTitlesScope.setAgreementTitle(agreementTitlesScopeDto.getAgreementTitle());
        agreementTitlesScope.setAgreementType(agreementTitlesScopeDto.getAgreementType());
        agreementTitlesScope.setInScope(agreementTitlesScopeDto.getInScope());
        return agreementTitlesScopeRepository.save(agreementTitlesScope);
    }

    @Transactional
    public void deleteById(Integer count) {
        agreementTitlesScopeRepository.deleteById(count);

    }


}
