package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToEquityConvertibleBondConverter;
import com.cs.qfc.data.jpa.entities.EquityConvertibleBond;
import com.cs.qfc.data.jpa.repositories.EquityConvertibleBondRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedEquityConvertibleBondRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.EquityConvertibleBondDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class EquityConvertibleBondService {


    private static final Logger LOG = LoggerFactory.getLogger(EquityConvertibleBondService.class);

    @Autowired
    EquityConvertibleBondRepository equityConvertibleBondRepository;

    @Autowired
    CustomizedEquityConvertibleBondRepository customizedEquityConvertibleBondRepository;

    @Transactional
    public EquityConvertibleBond saveEquityConvertibleBond(EquityConvertibleBondDTO equityConvertibleBondDTO) {
        EquityConvertibleBond equityConvertibleBond = DTOToEquityConvertibleBondConverter.convert(equityConvertibleBondDTO);
        return equityConvertibleBondRepository.save(equityConvertibleBond);
    }

    @Transactional
    public EquityConvertibleBond save(EquityConvertibleBond equityConvertibleBond) {
        return equityConvertibleBondRepository.save(equityConvertibleBond);
    }


    @Transactional
    public Collection<EquityConvertibleBond> findAll() {
        return equityConvertibleBondRepository.findAll();
    }

    @Transactional
    public Collection<EquityConvertibleBondDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedEquityConvertibleBondRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public EquityConvertibleBond findById(String cusipId) {
        Optional<EquityConvertibleBond> equityConvertibleBond = equityConvertibleBondRepository.findById(cusipId);
        return (equityConvertibleBond.isPresent() ? equityConvertibleBond.get() : null);
    }

    @Transactional
    public EquityConvertibleBond updateEquityConvertibleBond(EquityConvertibleBond equityConvertibleBond, EquityConvertibleBondDTO equityConvertibleBondDto) {

        equityConvertibleBond.setIsinId(equityConvertibleBondDto.getIsinId());
        equityConvertibleBond.setNextCallDate(equityConvertibleBondDto.getNextCallDate());
        equityConvertibleBond.setNextPutDate(equityConvertibleBondDto.getNextPutDate());
        equityConvertibleBond.setNxtPayDate(equityConvertibleBondDto.getNxtPayDate());
        equityConvertibleBond.setSecurityDescription(equityConvertibleBondDto.getSecurityDescription());
        equityConvertibleBond.setA3c7Ind(equityConvertibleBondDto.getA3c7Ind());
        equityConvertibleBond.setBbYKet(equityConvertibleBondDto.getBbYKet());
        equityConvertibleBond.setCompExchCodeDer(equityConvertibleBondDto.getCompExchCodeDer());
        equityConvertibleBond.setConvPrice(equityConvertibleBondDto.getConvPrice());
        equityConvertibleBond.setConRatio(equityConvertibleBondDto.getConRatio());
        equityConvertibleBond.setCsManaged(equityConvertibleBondDto.getCsManaged());
        equityConvertibleBond.setEquityFloat(equityConvertibleBondDto.getEquityFloat());
        equityConvertibleBond.setExchange(equityConvertibleBondDto.getExchange());
        equityConvertibleBond.setFidesaCode(equityConvertibleBondDto.getFidesaCode());
        equityConvertibleBond.setFundObjective(equityConvertibleBondDto.getFundObjective());
        equityConvertibleBond.setGlossG5(equityConvertibleBondDto.getGlossG5());
        equityConvertibleBond.setIdBbGlobal(equityConvertibleBondDto.getIdBbGlobal());
        equityConvertibleBond.setIdBbUnique(equityConvertibleBondDto.getIdBbUnique());
        equityConvertibleBond.setItalyId(equityConvertibleBondDto.getItalyId());
        equityConvertibleBond.setJapanId(equityConvertibleBondDto.getJapanId());
        equityConvertibleBond.setRicId(equityConvertibleBondDto.getRicId());
        equityConvertibleBond.setSingId(equityConvertibleBondDto.getSingId());
        equityConvertibleBond.setValorenId(equityConvertibleBondDto.getValorenId());
        equityConvertibleBond.setWertpId(equityConvertibleBondDto.getWertpId());
        equityConvertibleBond.setHugoId(equityConvertibleBondDto.getHugoId());
        equityConvertibleBond.setNtpaLsEqyPoDt(equityConvertibleBondDto.getNtpaLsEqyPoDt());
        equityConvertibleBond.setLondonImsDescription(equityConvertibleBondDto.getLondonImsDescription());
        equityConvertibleBond.setMaturityDate(equityConvertibleBondDto.getMaturityDate());
        equityConvertibleBond.setNasdaqReportCodeDer(equityConvertibleBondDto.getNasdaqReportCodeDer());
        equityConvertibleBond.setPrimaryRic(equityConvertibleBondDto.getPrimaryRic());
        equityConvertibleBond.setQtyPerUndlyShr(equityConvertibleBondDto.getQtyPerUndlyShr());
        equityConvertibleBond.setUndlyPerAdr(equityConvertibleBondDto.getUndlyPerAdr());
        equityConvertibleBond.setRegs(equityConvertibleBondDto.getRegs());
        equityConvertibleBond.setRegionPrim(equityConvertibleBondDto.getRegionPrim());
        equityConvertibleBond.setRtrTicketSymbol(equityConvertibleBondDto.getRtrTicketSymbol());
        equityConvertibleBond.setTradeCntry(equityConvertibleBondDto.getTradeCntry());
        equityConvertibleBond.setUndlySecIsin(equityConvertibleBondDto.getUndlySecIsin());
        equityConvertibleBond.setVotingRightPerShare(equityConvertibleBondDto.getVotingRightPerShare());
        equityConvertibleBond.setCmuId(equityConvertibleBondDto.getCmuId());
        equityConvertibleBond.setUltPrntTicketExchange(equityConvertibleBondDto.getUltPrntTicketExchange());
        equityConvertibleBond.setPoetsId(equityConvertibleBondDto.getPoetsId());
        equityConvertibleBond.setNtpaSoi(equityConvertibleBondDto.getNtpaSoi());
        equityConvertibleBond.setCpn(equityConvertibleBondDto.getCpn());
        equityConvertibleBond.setSsrLiquidityIndicator(equityConvertibleBondDto.getSsrLiquidityIndicator());
        equityConvertibleBond.setReExchange(equityConvertibleBondDto.getReExchange());
        equityConvertibleBond.setGlossK3(equityConvertibleBondDto.getGlossK3());
        equityConvertibleBond.setSecType(equityConvertibleBondDto.getSecType());
        equityConvertibleBond.setFundEuroDirectUcits(equityConvertibleBondDto.getFundEuroDirectUcits());
        equityConvertibleBond.setFundAssetClassFocus(equityConvertibleBondDto.getFundAssetClassFocus());
        equityConvertibleBond.setBbCompanyId(equityConvertibleBondDto.getBbCompanyId());
        equityConvertibleBond.setSpIssueId(equityConvertibleBondDto.getSpIssueId());
        equityConvertibleBond.setFundClosedNewInv(equityConvertibleBondDto.getFundClosedNewInv());
        equityConvertibleBond.setLegalName(equityConvertibleBondDto.getLegalName());
        equityConvertibleBond.setTradingLotSize(equityConvertibleBondDto.getTradingLotSize());
        equityConvertibleBond.setReAdrUndlyRic(equityConvertibleBondDto.getReAdrUndlyRic());
        equityConvertibleBond.setRelativeIndex(equityConvertibleBondDto.getRelativeIndex());
        equityConvertibleBond.setPrimTrad(equityConvertibleBondDto.getPrimTrad());
        equityConvertibleBond.setCumulativeFlag(equityConvertibleBondDto.getCumulativeFlag());
        equityConvertibleBond.setFundCustodianName(equityConvertibleBondDto.getFundCustodianName());
        equityConvertibleBond.setInitialPubOffer(equityConvertibleBondDto.getInitialPubOffer());
        equityConvertibleBond.setGicsSectorIsr(equityConvertibleBondDto.getGicsSectorIsr());
        equityConvertibleBond.setGicsSectorNameIsr(equityConvertibleBondDto.getGicsSectorNameIsr());
        equityConvertibleBond.setGicsIndustryGroupIsr(equityConvertibleBondDto.getGicsIndustryGroupIsr());
        equityConvertibleBond.setGicsIndustryGroupNameIsr(equityConvertibleBondDto.getGicsIndustryGroupNameIsr());
        equityConvertibleBond.setGicsIndustrySr(equityConvertibleBondDto.getGicsIndustrySr());
        equityConvertibleBond.setGicsIndustryNameIsr(equityConvertibleBondDto.getGicsIndustryNameIsr());
        equityConvertibleBond.setNaicsIdIsr(equityConvertibleBondDto.getNaicsIdIsr());
        equityConvertibleBond.setNtpaCountryOfSettlement(equityConvertibleBondDto.getNtpaCountryOfSettlement());
        equityConvertibleBond.setGliRp(equityConvertibleBondDto.getGliRp());
        equityConvertibleBond.setCddsLastUpdatedTime(equityConvertibleBondDto.getCddsLastUpdatedTime());
        equityConvertibleBond.setPubliclyTraded(equityConvertibleBondDto.getPubliclyTraded());
        equityConvertibleBond.setCsSettleDays(equityConvertibleBondDto.getCsSettleDays());
        equityConvertibleBond.setBbUnderlyingSecurity(equityConvertibleBondDto.getBbUnderlyingSecurity());
        equityConvertibleBond.setBbUnderlyingTypeCode(equityConvertibleBondDto.getBbUnderlyingTypeCode());
        equityConvertibleBond.setTickSizePilotGroup(equityConvertibleBondDto.getTickSizePilotGroup());
        equityConvertibleBond.setCalledDate(equityConvertibleBondDto.getCalledDate());
        equityConvertibleBond.setCalledPrice(equityConvertibleBondDto.getCalledPrice());
        equityConvertibleBond.setCouponType(equityConvertibleBondDto.getCouponType());
        equityConvertibleBond.setReClassScheme(equityConvertibleBondDto.getReClassScheme());
        equityConvertibleBond.setLegalEntityIdentifier(equityConvertibleBondDto.getLegalEntityIdentifier());
        equityConvertibleBond.setCompanyCorpTicker(equityConvertibleBondDto.getCompanyCorpTicker());
        equityConvertibleBond.setCompanyToParent(equityConvertibleBondDto.getCompanyToParent());
        equityConvertibleBond.setBbIssuerType(equityConvertibleBondDto.getBbIssuerType());
        equityConvertibleBond.setFisecIssuerId(equityConvertibleBondDto.getFisecIssuerId());
        equityConvertibleBond.setUltPrntCompId(equityConvertibleBondDto.getUltPrntCompId());
        equityConvertibleBond.setIsArchived(equityConvertibleBondDto.getIsArchived());
        equityConvertibleBond.setLocalExchangeSymbol(equityConvertibleBondDto.getLocalExchangeSymbol());
        equityConvertibleBond.setCfiCode(equityConvertibleBondDto.getCfiCode());
        equityConvertibleBond.setFisn(equityConvertibleBondDto.getFisn());
        equityConvertibleBond.setIssDate(equityConvertibleBondDto.getIssDate());
        equityConvertibleBond.setFirstSettlementDate(equityConvertibleBondDto.getFirstSettlementDate());
        equityConvertibleBond.setWhenIssuedFlag(equityConvertibleBondDto.getWhenIssuedFlag());
        equityConvertibleBond.setMinorTradingCurrencyInd(equityConvertibleBondDto.getMinorTradingCurrencyInd());
        equityConvertibleBond.setRiskAlert(equityConvertibleBondDto.getRiskAlert());
        equityConvertibleBond.setMdsId(equityConvertibleBondDto.getMdsId());
        equityConvertibleBond.setFundGeographicFocus(equityConvertibleBondDto.getFundGeographicFocus());
        equityConvertibleBond.setFundLeverageAmount(equityConvertibleBondDto.getFundLeverageAmount());
        equityConvertibleBond.setCmsCode(equityConvertibleBondDto.getCmsCode());
        equityConvertibleBond.setIdSpi(equityConvertibleBondDto.getIdSpi());
        equityConvertibleBond.setStaticListingId(equityConvertibleBondDto.getStaticListingId());
        equityConvertibleBond.setClientType(equityConvertibleBondDto.getClientType());
        equityConvertibleBond.setMarketingDesk(equityConvertibleBondDto.getMarketingDesk());
        equityConvertibleBond.setExternalId(equityConvertibleBondDto.getExternalId());
        equityConvertibleBond.setIssueBook(equityConvertibleBondDto.getIssueBook());
        equityConvertibleBond.setDutchId(equityConvertibleBondDto.getDutchId());
        equityConvertibleBond.setBelgiumId(equityConvertibleBondDto.getBelgiumId());
        equityConvertibleBond.setIstarId(equityConvertibleBondDto.getIstarId());
        equityConvertibleBond.setIdBbSecurity(equityConvertibleBondDto.getIdBbSecurity());
        equityConvertibleBond.setOperatingMic(equityConvertibleBondDto.getOperatingMic());
        equityConvertibleBond.setBbIndustrySectorCd(equityConvertibleBondDto.getBbIndustrySectorCd());
        equityConvertibleBond.setBbIndustryGroupCd(equityConvertibleBondDto.getBbIndustryGroupCd());
        equityConvertibleBond.setBbIndustrySubGroupCd(equityConvertibleBondDto.getBbIndustrySubGroupCd());
        equityConvertibleBond.setCddsRecCreationTime(equityConvertibleBondDto.getCddsRecCreationTime());
        equityConvertibleBond.setListedExchInd(equityConvertibleBondDto.getListedExchInd());
        equityConvertibleBond.setIdMalaysian(equityConvertibleBondDto.getIdMalaysian());
        equityConvertibleBond.setRedemptionPrice(equityConvertibleBondDto.getRedemptionPrice());
        equityConvertibleBond.setSeries(equityConvertibleBondDto.getSeries());
        equityConvertibleBond.setCpnFreq(equityConvertibleBondDto.getCpnFreq());
        equityConvertibleBond.setGmSoi(equityConvertibleBondDto.getGmSoi());
        equityConvertibleBond.setCsPvtPlacementInd(equityConvertibleBondDto.getCsPvtPlacementInd());
        return equityConvertibleBondRepository.save(equityConvertibleBond);
    }

    @Transactional
    public void deleteById(String cusipId) {
        equityConvertibleBondRepository.deleteById(cusipId);

    }


}
