package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToDebtInstrumentConverter;
import com.cs.qfc.data.jpa.entities.DebtInstrument;
import com.cs.qfc.data.jpa.repositories.DebtInstrumentRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedDebtInstrumentRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DebtInstrumentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class DebtInstrumentService {


    private static final Logger LOG = LoggerFactory.getLogger(DebtInstrumentService.class);

    @Autowired
    DebtInstrumentRepository debtInstrumentRepository;
    @Autowired
    CustomizedDebtInstrumentRepository customizedDebtInstrumentRepository;

    @Transactional
    public DebtInstrument saveDebtInstrument(DebtInstrumentDTO debtInstrumentDTO) {
        DebtInstrument debtInstrument = DTOToDebtInstrumentConverter.convert(debtInstrumentDTO);
        return debtInstrumentRepository.save(debtInstrument);
    }

    @Transactional
    public DebtInstrument save(DebtInstrument debtInstrument) {
        return debtInstrumentRepository.save(debtInstrument);
    }


    @Transactional
    public Collection<DebtInstrument> findAll() {
        return debtInstrumentRepository.findAll();
    }

    @Transactional
    public Collection<DebtInstrumentDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedDebtInstrumentRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public DebtInstrument findById(String id) {
        Optional<DebtInstrument> debtInstrument = debtInstrumentRepository.findById(id);
        return (debtInstrument.isPresent() ? debtInstrument.get() : null);
    }

    @Transactional
    public DebtInstrument updateDebtInstrument(DebtInstrument debtInstrument, DebtInstrumentDTO debtInstrumentDto) {

        debtInstrument.setIsinId(debtInstrumentDto.getIsinId());
        debtInstrument.setSedolId(debtInstrumentDto.getSedolId());
        debtInstrument.setSecurityDescriptionLong(debtInstrumentDto.getSecurityDescriptionLong());
        debtInstrument.setNxtPayDate(debtInstrumentDto.getNxtPayDate());
        debtInstrument.setNextCallDate(debtInstrumentDto.getNextCallDate());
        debtInstrument.setNextPutDate(debtInstrumentDto.getNextPutDate());
        return debtInstrumentRepository.save(debtInstrument);
    }

    @Transactional
    public void deleteById(String id) {
        debtInstrumentRepository.deleteById(id);

    }


}
