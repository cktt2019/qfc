package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToBusinessUnitConverter;
import com.cs.qfc.data.jpa.entities.BusinessUnit;
import com.cs.qfc.data.jpa.entities.BusinessUnitKey;
import com.cs.qfc.data.jpa.repositories.BusinessUnitRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedBusinessUnitRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.BusinessUnitDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class BusinessUnitService {


    private static final Logger LOG = LoggerFactory.getLogger(BusinessUnitService.class);

    @Autowired
    BusinessUnitRepository businessUnitRepository;

    @Autowired
    CustomizedBusinessUnitRepository customizedBusinessUnitRepository;

    @Transactional
    public BusinessUnit saveBusinessUnit(BusinessUnitDTO businessUnitDTO) {
        BusinessUnit businessUnit = DTOToBusinessUnitConverter.convert(businessUnitDTO);
        return businessUnitRepository.save(businessUnit);
    }

    @Transactional
    public BusinessUnit save(BusinessUnit businessUnit) {
        return businessUnitRepository.save(businessUnit);
    }


    @Transactional
    public Collection<BusinessUnit> findAll() {
        return businessUnitRepository.findAll();
    }

    @Transactional
    public Collection<BusinessUnitDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedBusinessUnitRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public BusinessUnit findById(String financialBusinessUnitCode, String businessUnitId) {
        Optional<BusinessUnit> businessUnit = businessUnitRepository.findById(BusinessUnitKey.builder().financialBusinessUnitCode(financialBusinessUnitCode).businessUnitId(businessUnitId).build());
        return (businessUnit.isPresent() ? businessUnit.get() : null);
    }

    @Transactional
    public BusinessUnit updateBusinessUnit(BusinessUnit businessUnit, BusinessUnitDTO businessUnitDto) {

        businessUnit.setBaseCurrency(businessUnitDto.getBaseCurrency());
        businessUnit.setBusinessUnitActiveIndicator(businessUnitDto.getBusinessUnitActiveIndicator());
        businessUnit.setBusinessUnitDescription(businessUnitDto.getBusinessUnitDescription());
        businessUnit.setBusinessUnitEffectiveDate(businessUnitDto.getBusinessUnitEffectiveDate());
        businessUnit.setBusinessUnitGermanDescription(businessUnitDto.getBusinessUnitGermanDescription());
        businessUnit.setBusinessUnitGermanName(businessUnitDto.getBusinessUnitGermanName());
        businessUnit.setBusinessUnitLastUpdateDatetime(businessUnitDto.getBusinessUnitLastUpdateDatetime());
        businessUnit.setBusinessUnitLastUpdatedBy(businessUnitDto.getBusinessUnitLastUpdatedBy());
        businessUnit.setBusinessUnitName(businessUnitDto.getBusinessUnitName());
        businessUnit.setBusinessUnitShortDescription(businessUnitDto.getBusinessUnitShortDescription());
        businessUnit.setBusinessUnitShortGermanDescription(businessUnitDto.getBusinessUnitShortGermanDescription());
        businessUnit.setChBusinessUnitLastUpdatedBy(businessUnitDto.getChBusinessUnitLastUpdatedBy());
        businessUnit.setChBusinessUnitLastUpdatedDatetime(businessUnitDto.getChBusinessUnitLastUpdatedDatetime());
        businessUnit.setCustomerId(businessUnitDto.getCustomerId());
        businessUnit.setCustomerVendorAffiliateIndicator(businessUnitDto.getCustomerVendorAffiliateIndicator());
        businessUnit.setFinancialBusinessUnitActiveIndicator(businessUnitDto.getFinancialBusinessUnitActiveIndicator());
        businessUnit.setFinancialBusinessUnitEffectiveDate(businessUnitDto.getFinancialBusinessUnitEffectiveDate());
        businessUnit.setFinancialBusinessUnitGroupId(businessUnitDto.getFinancialBusinessUnitGroupId());
        businessUnit.setFinancialBusinessUnitGroupName(businessUnitDto.getFinancialBusinessUnitGroupName());
        businessUnit.setFinancialBusinessUnitName(businessUnitDto.getFinancialBusinessUnitName());
        businessUnit.setFirmType(businessUnitDto.getFirmType());
        businessUnit.setIsoCurrencyCode(businessUnitDto.getIsoCurrencyCode());
        businessUnit.setLcdRefNo(businessUnitDto.getLcdRefNo());
        businessUnit.setLegalEntityShortDescription(businessUnitDto.getLegalEntityShortDescription());
        businessUnit.setLegalEntityActiveIndicator(businessUnitDto.getLegalEntityActiveIndicator());
        businessUnit.setLegalEntityCode(businessUnitDto.getLegalEntityCode());
        businessUnit.setLegalEntityDescription(businessUnitDto.getLegalEntityDescription());
        businessUnit.setLegalEntityEffectiveDate(businessUnitDto.getLegalEntityEffectiveDate());
        businessUnit.setLegalEntityGermanDescription(businessUnitDto.getLegalEntityGermanDescription());
        businessUnit.setLegalEntityLastUpdatedBy(businessUnitDto.getLegalEntityLastUpdatedBy());
        businessUnit.setLegalEntityLastUpdatedDatetime(businessUnitDto.getLegalEntityLastUpdatedDatetime());
        businessUnit.setLocalGaapCurrency(businessUnitDto.getLocalGaapCurrency());
        businessUnit.setMisMemberCode(businessUnitDto.getMisMemberCode());
        businessUnit.setProcessingLocationCode(businessUnitDto.getProcessingLocationCode());
        businessUnit.setSourceId(businessUnitDto.getSourceId());
        businessUnit.setDeltaFlag(businessUnitDto.getDeltaFlag());
        businessUnit.setBuDomicile(businessUnitDto.getBuDomicile());
        return businessUnitRepository.save(businessUnit);
    }

    @Transactional
    public void deleteById(String financialBusinessUnitCode, String businessUnitId) {
        businessUnitRepository.deleteById(BusinessUnitKey.builder().financialBusinessUnitCode(financialBusinessUnitCode).businessUnitId(businessUnitId).build());

    }


}
