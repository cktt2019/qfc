package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToBookConverter;
import com.cs.qfc.data.jpa.entities.Book;
import com.cs.qfc.data.jpa.repositories.BookRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedBookRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.BookDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class BookService {


    private static final Logger LOG = LoggerFactory.getLogger(BookService.class);

    @Autowired
    BookRepository bookRepository;
    @Autowired
    CustomizedBookRepository customizedBookRepository;

    @Transactional
    public Book saveBook(BookDTO bookDTO) {
        Book book = DTOToBookConverter.convert(bookDTO);
        return bookRepository.save(book);
    }

    @Transactional
    public Book save(Book book) {
        return bookRepository.save(book);
    }


    @Transactional
    public Collection<Book> findAll() {

        return bookRepository.findAll(PageRequest.of(0, 100, Sort.unsorted())).getContent();
    }

    @Transactional
    public Collection<BookDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedBookRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public Book findById(String globalBookId) {
        Optional<Book> book = bookRepository.findById(globalBookId);
        return (book.isPresent() ? book.get() : null);
    }

    @Transactional
    public Book updateBook(Book book, BookDTO bookDto) {

        book.setAccountMethod(bookDto.getAccountMethod());
        book.setAccountingBasisIndicator(bookDto.getAccountingBasisIndicator());
        book.setBookCategoryCode(bookDto.getBookCategoryCode());
        book.setBookCategoryDescription(bookDto.getBookCategoryDescription());
        book.setBookControllerId(bookDto.getBookControllerId());
        book.setBookDormantIndicator(bookDto.getBookDormantIndicator());
        book.setBookName(bookDto.getBookName());
        book.setBookPnlSystemName(bookDto.getBookPnlSystemName());
        book.setBookRef(bookDto.getBookRef());
        book.setBookStatus(bookDto.getBookStatus());
        book.setBookSubCategoryCode(bookDto.getBookSubCategoryCode());
        book.setBookSubCategoryDescription(bookDto.getBookSubCategoryDescription());
        book.setBookTraderId(bookDto.getBookTraderId());
        book.setCnyDeliverableOutsideChinaIndicator(bookDto.getCnyDeliverableOutsideChinaIndicator());
        book.setDepartmentId(bookDto.getDepartmentId());
        book.setFrontOfficeRepresentativeId(bookDto.getFrontOfficeRepresentativeId());
        book.setGplAtomCode(bookDto.getGplAtomCode());
        book.setNhfsIndicator(bookDto.getNhfsIndicator());
        book.setRemoteBookingPolicyIndicator(bookDto.getRemoteBookingPolicyIndicator());
        book.setSplitHedgeComments(bookDto.getSplitHedgeComments());
        book.setSplitHedgeIndicator(bookDto.getSplitHedgeIndicator());
        book.setUsTaxCategoryCode(bookDto.getUsTaxCategoryCode());
        book.setBookLastUpdatedDatetime(bookDto.getBookLastUpdatedDatetime());
        book.setFinancialBusinessUnitCode(bookDto.getFinancialBusinessUnitCode());
        book.setDeltaFlag(bookDto.getDeltaFlag());
        book.setBookRequestStatusCode(bookDto.getBookRequestStatusCode());
        book.setFirstInstanceBookActivated(bookDto.getFirstInstanceBookActivated());
        book.setLastInstanceBookActivated(bookDto.getLastInstanceBookActivated());
        book.setFirstInstanceBookInactivated(bookDto.getFirstInstanceBookInactivated());
        book.setLastInstanceBook(bookDto.getLastInstanceBook());
        book.setEmailAddress(bookDto.getEmailAddress());
        book.setCavFlag(bookDto.getCavFlag());
        book.setFosFlag(bookDto.getFosFlag());
        book.setBookingIntent(bookDto.getBookingIntent());
        book.setBookRequestId(bookDto.getBookRequestId());
        book.setIparEmplIdEmailId(bookDto.getIparEmplIdEmailId());
        book.setBookTraderEmailId(bookDto.getBookTraderEmailId());
        book.setPcSupervisorEmailId(bookDto.getPcSupervisorEmailId());
        book.setBookEmpIdEmailId(bookDto.getBookEmpIdEmailId());
        book.setPctnlEmplIdEmailId(bookDto.getPctnlEmplIdEmailId());
        return bookRepository.save(book);
    }

    @Transactional
    public void deleteById(String globalBookId) {
        bookRepository.deleteById(globalBookId);

    }


}
