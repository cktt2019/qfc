package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToPrimeSwapPositionConverter;
import com.cs.qfc.data.jpa.entities.PrimeSwapPosition;
import com.cs.qfc.data.jpa.repositories.PrimeSwapPositionRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedPrimeSwapPositionRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.PrimeSwapPositionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class PrimeSwapPositionService {


    private static final Logger LOG = LoggerFactory.getLogger(PrimeSwapPositionService.class);

    @Autowired
    PrimeSwapPositionRepository primeSwapPositionRepository;
    @Autowired
    CustomizedPrimeSwapPositionRepository customizedPrimeSwapPositionRepository;


    @Transactional
    public PrimeSwapPosition savePrimeSwapPosition(PrimeSwapPositionDTO primeSwapPositionDTO) {
        PrimeSwapPosition primeSwapPosition = DTOToPrimeSwapPositionConverter.convert(primeSwapPositionDTO);
        return primeSwapPositionRepository.save(primeSwapPosition);
    }

    @Transactional
    public PrimeSwapPosition save(PrimeSwapPosition primeSwapPosition) {
        return primeSwapPositionRepository.save(primeSwapPosition);
    }


    @Transactional
    public Collection<PrimeSwapPosition> findAll() {
        return primeSwapPositionRepository.findAll();
    }

    @Transactional
    public Collection<PrimeSwapPositionDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedPrimeSwapPositionRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public PrimeSwapPosition findById(Integer id) {
        Optional<PrimeSwapPosition> primeSwapPosition = primeSwapPositionRepository.findById(id);
        return (primeSwapPosition.isPresent() ? primeSwapPosition.get() : null);
    }

    @Transactional
    public PrimeSwapPosition updatePrimeSwapPosition(PrimeSwapPosition primeSwapPosition, PrimeSwapPositionDTO primeSwapPositionDto) {

        primeSwapPosition.setBusinessDate(primeSwapPositionDto.getBusinessDate());
        primeSwapPosition.setSwapId(primeSwapPositionDto.getSwapId());
        primeSwapPosition.setSwapVersion(primeSwapPositionDto.getSwapVersion());
        primeSwapPosition.setPositionId(primeSwapPositionDto.getPositionId());
        primeSwapPosition.setPositionVersion(primeSwapPositionDto.getPositionVersion());
        primeSwapPosition.setSwapType(primeSwapPositionDto.getSwapType());
        primeSwapPosition.setCounterpartyCode(primeSwapPositionDto.getCounterpartyCode());
        primeSwapPosition.setCounterpartyName(primeSwapPositionDto.getCounterpartyName());
        primeSwapPosition.setSettledLtdCostBase(primeSwapPositionDto.getSettledLtdCostBase());
        primeSwapPosition.setSwapCcyToUsdFx(primeSwapPositionDto.getSwapCcyToUsdFx());
        return primeSwapPositionRepository.save(primeSwapPosition);
    }

    @Transactional
    public void deleteById(Integer id) {
        primeSwapPositionRepository.deleteById(id);

    }


}
