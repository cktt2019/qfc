package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToCreditSuisseLegalEntityConverter;
import com.cs.qfc.data.jpa.entities.CreditSuisseLegalEntity;
import com.cs.qfc.data.jpa.repositories.CreditSuisseLegalEntityRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedCreditSuisseLegalEntityRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CreditSuisseLegalEntityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class CreditSuisseLegalEntityService {


    private static final Logger LOG = LoggerFactory.getLogger(CreditSuisseLegalEntityService.class);

    @Autowired
    CreditSuisseLegalEntityRepository creditSuisseLegalEntityRepository;
    @Autowired
    CustomizedCreditSuisseLegalEntityRepository customizedCreditSuisseLegalEntityRepository;

    @Transactional
    public CreditSuisseLegalEntity saveCreditSuisseLegalEntity(CreditSuisseLegalEntityDTO creditSuisseLegalEntityDTO) {
        CreditSuisseLegalEntity creditSuisseLegalEntity = DTOToCreditSuisseLegalEntityConverter.convert(creditSuisseLegalEntityDTO);
        return creditSuisseLegalEntityRepository.save(creditSuisseLegalEntity);
    }

    @Transactional
    public CreditSuisseLegalEntity save(CreditSuisseLegalEntity creditSuisseLegalEntity) {
        return creditSuisseLegalEntityRepository.save(creditSuisseLegalEntity);
    }


    @Transactional
    public Collection<CreditSuisseLegalEntity> findAll() {
        return creditSuisseLegalEntityRepository.findAll();
    }

    @Transactional
    public Collection<CreditSuisseLegalEntityDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedCreditSuisseLegalEntityRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public CreditSuisseLegalEntity findById(Integer count) {
        Optional<CreditSuisseLegalEntity> creditSuisseLegalEntity = creditSuisseLegalEntityRepository.findById(count);
        return (creditSuisseLegalEntity.isPresent() ? creditSuisseLegalEntity.get() : null);
    }

    @Transactional
    public CreditSuisseLegalEntity updateCreditSuisseLegalEntity(CreditSuisseLegalEntity creditSuisseLegalEntity, CreditSuisseLegalEntityDTO creditSuisseLegalEntityDto) {

        creditSuisseLegalEntity.setEnterpriseDNumber(creditSuisseLegalEntityDto.getEnterpriseDNumber());
        creditSuisseLegalEntity.setBookingEntityNm(creditSuisseLegalEntityDto.getBookingEntityNm());
        creditSuisseLegalEntity.setPeopleSoftId(creditSuisseLegalEntityDto.getPeopleSoftId());
        creditSuisseLegalEntity.setCsidId(creditSuisseLegalEntityDto.getCsidId());
        return creditSuisseLegalEntityRepository.save(creditSuisseLegalEntity);
    }

    @Transactional
    public void deleteById(Integer count) {
        creditSuisseLegalEntityRepository.deleteById(count);

    }


}
