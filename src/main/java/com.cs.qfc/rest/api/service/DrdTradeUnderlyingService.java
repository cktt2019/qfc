package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToDrdTradeUnderlyingConverter;
import com.cs.qfc.data.jpa.entities.DrdTradeUnderlying;
import com.cs.qfc.data.jpa.repositories.DrdTradeUnderlyingRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedDrdTradeUnderlyingRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DrdTradeUnderlyingDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class DrdTradeUnderlyingService {


    private static final Logger LOG = LoggerFactory.getLogger(DrdTradeUnderlyingService.class);

    @Autowired
    DrdTradeUnderlyingRepository drdTradeUnderlyingRepository;
    @Autowired
    CustomizedDrdTradeUnderlyingRepository customizedDrdTradeUnderlyingRepository;

    @Transactional
    public DrdTradeUnderlying saveDrdTradeUnderlying(DrdTradeUnderlyingDTO drdTradeUnderlyingDTO) {
        DrdTradeUnderlying drdTradeUnderlying = DTOToDrdTradeUnderlyingConverter.convert(drdTradeUnderlyingDTO);
        return drdTradeUnderlyingRepository.save(drdTradeUnderlying);
    }

    @Transactional
    public DrdTradeUnderlying save(DrdTradeUnderlying drdTradeUnderlying) {
        return drdTradeUnderlyingRepository.save(drdTradeUnderlying);
    }


    @Transactional
    public Collection<DrdTradeUnderlying> findAll() {
        return drdTradeUnderlyingRepository.findAll();
    }

    @Transactional
    public Collection<DrdTradeUnderlyingDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedDrdTradeUnderlyingRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public DrdTradeUnderlying findById(Integer id) {
        Optional<DrdTradeUnderlying> drdTradeUnderlying = drdTradeUnderlyingRepository.findById(id);
        return (drdTradeUnderlying.isPresent() ? drdTradeUnderlying.get() : null);
    }

    @Transactional
    public DrdTradeUnderlying updateDrdTradeUnderlying(DrdTradeUnderlying drdTradeUnderlying, DrdTradeUnderlyingDTO drdTradeUnderlyingDto) {

        drdTradeUnderlying.setCobDate(drdTradeUnderlyingDto.getCobDate());
        drdTradeUnderlying.setLoadCobDate(drdTradeUnderlyingDto.getLoadCobDate());
        drdTradeUnderlying.setLoadBook(drdTradeUnderlyingDto.getLoadBook());
        drdTradeUnderlying.setBookName(drdTradeUnderlyingDto.getBookName());
        drdTradeUnderlying.setTradeLoadId(drdTradeUnderlyingDto.getTradeLoadId());
        drdTradeUnderlying.setLoadTimestamp(drdTradeUnderlyingDto.getLoadTimestamp());
        drdTradeUnderlying.setSnapshotName(drdTradeUnderlyingDto.getSnapshotName());
        drdTradeUnderlying.setSnapshotId(drdTradeUnderlyingDto.getSnapshotId());
        drdTradeUnderlying.setSnapshotCreationDatetime(drdTradeUnderlyingDto.getSnapshotCreationDatetime());
        drdTradeUnderlying.setInterfaceVersion(drdTradeUnderlyingDto.getInterfaceVersion());
        drdTradeUnderlying.setTradeId(drdTradeUnderlyingDto.getTradeId());
        drdTradeUnderlying.setTftTradeVersion(drdTradeUnderlyingDto.getTftTradeVersion());
        drdTradeUnderlying.setTradeLocationId(drdTradeUnderlyingDto.getTradeLocationId());
        drdTradeUnderlying.setPrimaryTcn(drdTradeUnderlyingDto.getPrimaryTcn());
        drdTradeUnderlying.setTcn(drdTradeUnderlyingDto.getTcn());
        drdTradeUnderlying.setTradeVersionUpdateTime(drdTradeUnderlyingDto.getTradeVersionUpdateTime());
        drdTradeUnderlying.setCancellationDate(drdTradeUnderlyingDto.getCancellationDate());
        drdTradeUnderlying.setGbmEntity(drdTradeUnderlyingDto.getGbmEntity());
        drdTradeUnderlying.setApprovalStatus(drdTradeUnderlyingDto.getApprovalStatus());
        drdTradeUnderlying.setEventReason(drdTradeUnderlyingDto.getEventReason());
        drdTradeUnderlying.setExternalId(drdTradeUnderlyingDto.getExternalId());
        drdTradeUnderlying.setPrimaryMaturityDate(drdTradeUnderlyingDto.getPrimaryMaturityDate());
        drdTradeUnderlying.setProductSubType(drdTradeUnderlyingDto.getProductSubType());
        drdTradeUnderlying.setProductType(drdTradeUnderlyingDto.getProductType());
        drdTradeUnderlying.setProductValuationType(drdTradeUnderlyingDto.getProductValuationType());
        drdTradeUnderlying.setTradeStatus(drdTradeUnderlyingDto.getTradeStatus());
        drdTradeUnderlying.setUnderlyingId(drdTradeUnderlyingDto.getUnderlyingId());
        drdTradeUnderlying.setUnderlyingIdType(drdTradeUnderlyingDto.getUnderlyingIdType());
        drdTradeUnderlying.setAccountCcy(drdTradeUnderlyingDto.getAccountCcy());
        drdTradeUnderlying.setBasketStrike(drdTradeUnderlyingDto.getBasketStrike());
        drdTradeUnderlying.setBasketWeighting(drdTradeUnderlyingDto.getBasketWeighting());
        drdTradeUnderlying.setCoupon(drdTradeUnderlyingDto.getCoupon());
        drdTradeUnderlying.setCouponFreq(drdTradeUnderlyingDto.getCouponFreq());
        drdTradeUnderlying.setFinalPrice(drdTradeUnderlyingDto.getFinalPrice());
        drdTradeUnderlying.setFloatingRateIndex(drdTradeUnderlyingDto.getFloatingRateIndex());
        drdTradeUnderlying.setFloatingRateIndexTenor(drdTradeUnderlyingDto.getFloatingRateIndexTenor());
        drdTradeUnderlying.setFloatingRateIndexType(drdTradeUnderlyingDto.getFloatingRateIndexType());
        drdTradeUnderlying.setInitialPrice(drdTradeUnderlyingDto.getInitialPrice());
        drdTradeUnderlying.setRefObligCategory(drdTradeUnderlyingDto.getRefObligCategory());
        drdTradeUnderlying.setRefPoolLongName(drdTradeUnderlyingDto.getRefPoolLongName());
        drdTradeUnderlying.setRefPoolShortName(drdTradeUnderlyingDto.getRefPoolShortName());
        drdTradeUnderlying.setSequenceId(drdTradeUnderlyingDto.getSequenceId());
        drdTradeUnderlying.setUnderlyingAssetClass(drdTradeUnderlyingDto.getUnderlyingAssetClass());
        drdTradeUnderlying.setUnderlyingCcy(drdTradeUnderlyingDto.getUnderlyingCcy());
        drdTradeUnderlying.setUnderlyingDesc(drdTradeUnderlyingDto.getUnderlyingDesc());
        drdTradeUnderlying.setUnderlyingExtId(drdTradeUnderlyingDto.getUnderlyingExtId());
        drdTradeUnderlying.setUnderlyingExtIdType(drdTradeUnderlyingDto.getUnderlyingExtIdType());
        drdTradeUnderlying.setUnderlyingMatDate(drdTradeUnderlyingDto.getUnderlyingMatDate());
        drdTradeUnderlying.setUnderlyingRiskId(drdTradeUnderlyingDto.getUnderlyingRiskId());
        drdTradeUnderlying.setPayRec(drdTradeUnderlyingDto.getPayRec());
        drdTradeUnderlying.setSettlementType(drdTradeUnderlyingDto.getSettlementType());
        drdTradeUnderlying.setBusinessGroup(drdTradeUnderlyingDto.getBusinessGroup());
        drdTradeUnderlying.setValuationClass(drdTradeUnderlyingDto.getValuationClass());
        drdTradeUnderlying.setValuationType(drdTradeUnderlyingDto.getValuationType());
        drdTradeUnderlying.setTradeSourceSystem(drdTradeUnderlyingDto.getTradeSourceSystem());
        drdTradeUnderlying.setUnderlyingPriceSource(drdTradeUnderlyingDto.getUnderlyingPriceSource());
        drdTradeUnderlying.setCduMasterAgreementLlk(drdTradeUnderlyingDto.getCduMasterAgreementLlk());
        drdTradeUnderlying.setCduCollateralAnnexLlk(drdTradeUnderlyingDto.getCduCollateralAnnexLlk());
        drdTradeUnderlying.setFramesoftMasterAgreementLlk(drdTradeUnderlyingDto.getFramesoftMasterAgreementLlk());
        drdTradeUnderlying.setAlgoCollateralAnnexLlk(drdTradeUnderlyingDto.getAlgoCollateralAnnexLlk());
        drdTradeUnderlying.setGbmBookRef(drdTradeUnderlyingDto.getGbmBookRef());
        drdTradeUnderlying.setUnderlyingProductId(drdTradeUnderlyingDto.getUnderlyingProductId());
        drdTradeUnderlying.setUnderlyingProductType(drdTradeUnderlyingDto.getUnderlyingProductType());
        drdTradeUnderlying.setUnderlyingRiskType(drdTradeUnderlyingDto.getUnderlyingRiskType());
        drdTradeUnderlying.setBankruptcy(drdTradeUnderlyingDto.getBankruptcy());
        drdTradeUnderlying.setDelivObligAcceleratedMatured(drdTradeUnderlyingDto.getDelivObligAcceleratedMatured());
        drdTradeUnderlying.setDelivObligAccruedInterest(drdTradeUnderlyingDto.getDelivObligAccruedInterest());
        drdTradeUnderlying.setDelivObligAssignableLoan(drdTradeUnderlyingDto.getDelivObligAssignableLoan());
        drdTradeUnderlying.setDelivObligCategory(drdTradeUnderlyingDto.getDelivObligCategory());
        drdTradeUnderlying.setDelivObligConsentRequiredLoan(drdTradeUnderlyingDto.getDelivObligConsentRequiredLoan());
        drdTradeUnderlying.setDelivOblgDirectLoanPrtCptn(drdTradeUnderlyingDto.getDelivOblgDirectLoanPrtCptn());
        drdTradeUnderlying.setDelivObligListed(drdTradeUnderlyingDto.getDelivObligListed());
        drdTradeUnderlying.setDelivObligMaximumMaturity(drdTradeUnderlyingDto.getDelivObligMaximumMaturity());
        drdTradeUnderlying.setDelivObligMaxMaturitySpecified(drdTradeUnderlyingDto.getDelivObligMaxMaturitySpecified());
        drdTradeUnderlying.setDelivObligNotBearer(drdTradeUnderlyingDto.getDelivObligNotBearer());
        drdTradeUnderlying.setDelivObligNotContingent(drdTradeUnderlyingDto.getDelivObligNotContingent());
        drdTradeUnderlying.setDelivObligNotDomesticCurrency(drdTradeUnderlyingDto.getDelivObligNotDomesticCurrency());
        drdTradeUnderlying.setDelivObligNotDomesticIssuance(drdTradeUnderlyingDto.getDelivObligNotDomesticIssuance());
        drdTradeUnderlying.setDelivObligNotDomesticLaw(drdTradeUnderlyingDto.getDelivObligNotDomesticLaw());
        drdTradeUnderlying.setDelivObligNotSovereignLender(drdTradeUnderlyingDto.getDelivObligNotSovereignLender());
        drdTradeUnderlying.setDelivObligPhyParipassu(drdTradeUnderlyingDto.getDelivObligPhyParipassu());
        drdTradeUnderlying.setDelivObligSpecifiedCcyList(drdTradeUnderlyingDto.getDelivObligSpecifiedCcyList());
        drdTradeUnderlying.setDelivObligSpecifiedCcyType(drdTradeUnderlyingDto.getDelivObligSpecifiedCcyType());
        drdTradeUnderlying.setDelivObligTransferable(drdTradeUnderlyingDto.getDelivObligTransferable());
        drdTradeUnderlying.setEntityType(drdTradeUnderlyingDto.getEntityType());
        drdTradeUnderlying.setPrimaryObligor(drdTradeUnderlyingDto.getPrimaryObligor());
        drdTradeUnderlying.setRecoveryValue(drdTradeUnderlyingDto.getRecoveryValue());
        drdTradeUnderlying.setReferenceEntityRedCode(drdTradeUnderlyingDto.getReferenceEntityRedCode());
        drdTradeUnderlying.setReferenceEntityUniqueEntityId(drdTradeUnderlyingDto.getReferenceEntityUniqueEntityId());
        drdTradeUnderlying.setReferenceObligationBloombergId(drdTradeUnderlyingDto.getReferenceObligationBloombergId());
        drdTradeUnderlying.setReferenceObligationCusip(drdTradeUnderlyingDto.getReferenceObligationCusip());
        drdTradeUnderlying.setReferenceObligationGuarantor(drdTradeUnderlyingDto.getReferenceObligationGuarantor());
        drdTradeUnderlying.setReferenceObligationIsin(drdTradeUnderlyingDto.getReferenceObligationIsin());
        drdTradeUnderlying.setReferencePrice(drdTradeUnderlyingDto.getReferencePrice());
        drdTradeUnderlying.setRefObligorCouponRate(drdTradeUnderlyingDto.getRefObligorCouponRate());
        drdTradeUnderlying.setRefObligorMaturityDate(drdTradeUnderlyingDto.getRefObligorMaturityDate());
        drdTradeUnderlying.setRefObligorNotionalAmt(drdTradeUnderlyingDto.getRefObligorNotionalAmt());
        drdTradeUnderlying.setRefObligorNotionalAmtCcy(drdTradeUnderlyingDto.getRefObligorNotionalAmtCcy());
        drdTradeUnderlying.setRefObligorObligationCategory(drdTradeUnderlyingDto.getRefObligorObligationCategory());
        drdTradeUnderlying.setValUnwind(drdTradeUnderlyingDto.getValUnwind());
        return drdTradeUnderlyingRepository.save(drdTradeUnderlying);
    }

    @Transactional
    public void deleteById(Integer id) {
        drdTradeUnderlyingRepository.deleteById(id);

    }


}
