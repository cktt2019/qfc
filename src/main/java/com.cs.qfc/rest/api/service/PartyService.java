package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToPartyConverter;
import com.cs.qfc.data.jpa.repositories.PartyRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedPartyRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.Party;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class PartyService {


    private static final Logger LOG = LoggerFactory.getLogger(PartyService.class);

    @Autowired
    PartyRepository partyRepository;

    @Autowired
    CustomizedPartyRepository customizedPartyRepository;

    @Transactional
    public com.cs.qfc.data.jpa.entities.Party saveParty(Party partyDTO) {
        com.cs.qfc.data.jpa.entities.Party party = DTOToPartyConverter.convert(partyDTO);
        return partyRepository.save(party);
    }

    @Transactional
    public com.cs.qfc.data.jpa.entities.Party save(com.cs.qfc.data.jpa.entities.Party party) {
        return partyRepository.save(party);
    }


    @Transactional
    public Collection<com.cs.qfc.data.jpa.entities.Party> findAll() {
        return partyRepository.findAll();
    }

    @Transactional
    public Collection<Party> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedPartyRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public com.cs.qfc.data.jpa.entities.Party findById(Long globalPartyId) {
        Optional<com.cs.qfc.data.jpa.entities.Party> party = partyRepository.findById(globalPartyId);
        return (party.isPresent() ? party.get() : null);
    }

    @Transactional
    public com.cs.qfc.data.jpa.entities.Party updateParty(com.cs.qfc.data.jpa.entities.Party party, Party partyDto) {

        party.setCsid(partyDto.getCsid());
        party.setCsidSource(partyDto.getCsidSource());
        party.setGsid(partyDto.getGsid());
        party.setGsidSource(partyDto.getGsidSource());
        party.setCountryOfDomicileIsoCountryCode(partyDto.getCountryOfDomicileIsoCountryCode());
        party.setCountryOfIncorporationIsoCountryCode(partyDto.getCountryOfIncorporationIsoCountryCode());
        party.setPartyLegalName(partyDto.getPartyLegalName());
        party.setAddressTypeCode(partyDto.getAddressTypeCode());
        party.setAddressAddressLine1(partyDto.getAddressAddressLine1());
        party.setAddressAddressLine2(partyDto.getAddressAddressLine2());
        party.setAddressAddressLine3(partyDto.getAddressAddressLine3());
        party.setAddressAddressLine4(partyDto.getAddressAddressLine4());
        party.setAddressPostalCode(partyDto.getAddressPostalCode());
        party.setAddressCountryIsoCountryName(partyDto.getAddressCountryIsoCountryName());
        party.setAddressAddressFullPhoneNumber(partyDto.getAddressAddressFullPhoneNumber());
        party.setAddressElctronicAddressId(partyDto.getAddressElctronicAddressId());
        party.setPartyLegalRoleDefinitionId(partyDto.getPartyLegalRoleDefinitionId());
        party.setPartyLegalRoleId(partyDto.getPartyLegalRoleId());
        party.setInternalPartyIdAltSourceName(partyDto.getInternalPartyIdAltSourceName());
        party.setPartyLegalRoleDescription(partyDto.getPartyLegalRoleDescription());
        party.setPartyLegalStatusType(partyDto.getPartyLegalStatusType());
        party.setPartyLegalStatusCode(partyDto.getPartyLegalStatusCode());
        party.setAddressTypeDescription(partyDto.getAddressTypeDescription());
        party.setAddressCityName(partyDto.getAddressCityName());
        party.setAddressPoBoxCity(partyDto.getAddressPoBoxCity());
        party.setExternalPartyAltIdSourceId(partyDto.getExternalPartyAltIdSourceId());
        party.setExternalPartyAltIdSourceName(partyDto.getExternalPartyAltIdSourceName());
        party.setExternalPartyAltIdSourceIdentificationNumber(partyDto.getExternalPartyAltIdSourceIdentificationNumber());
        return partyRepository.save(party);
    }

    @Transactional
    public void deleteById(Long globalPartyId) {
        partyRepository.deleteById(globalPartyId);

    }


}
