package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToEmployeeRoleConverter;
import com.cs.qfc.data.jpa.entities.EmployeeRole;
import com.cs.qfc.data.jpa.repositories.EmployeeRoleRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedEmployeeRoleRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.EmployeeRoleDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class EmployeeRoleService {


    private static final Logger LOG = LoggerFactory.getLogger(EmployeeRoleService.class);

    @Autowired
    EmployeeRoleRepository employeeRoleRepository;

    @Autowired
    CustomizedEmployeeRoleRepository customizedEmployeeRoleRepository;

    @Transactional
    public EmployeeRole saveEmployeeRole(EmployeeRoleDTO employeeRoleDTO) {
        EmployeeRole employeeRole = DTOToEmployeeRoleConverter.convert(employeeRoleDTO);
        return employeeRoleRepository.save(employeeRole);
    }

    @Transactional
    public EmployeeRole save(EmployeeRole employeeRole) {
        return employeeRoleRepository.save(employeeRole);
    }


    @Transactional
    public Collection<EmployeeRole> findAll() {
        return employeeRoleRepository.findAll();
    }

    @Transactional
    public Collection<EmployeeRoleDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedEmployeeRoleRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public EmployeeRole findById(String employeeId) {
        Optional<EmployeeRole> employeeRole = employeeRoleRepository.findById(employeeId);
        return (employeeRole.isPresent() ? employeeRole.get() : null);
    }

    @Transactional
    public EmployeeRole updateEmployeeRole(EmployeeRole employeeRole, EmployeeRoleDTO employeeRoleDto) {

        employeeRole.setEmployeeActiveIndicator(employeeRoleDto.getEmployeeActiveIndicator());
        employeeRole.setEmployeeEffectiveDate(employeeRoleDto.getEmployeeEffectiveDate());
        employeeRole.setEmployeeEmailId(employeeRoleDto.getEmployeeEmailId());
        employeeRole.setEmployeeFirstName(employeeRoleDto.getEmployeeFirstName());
        employeeRole.setEmployeeLastName(employeeRoleDto.getEmployeeLastName());
        employeeRole.setEmployeeLastUpdatedBy(employeeRoleDto.getEmployeeLastUpdatedBy());
        employeeRole.setEmployeeLastUpdatedDatetime(employeeRoleDto.getEmployeeLastUpdatedDatetime());
        employeeRole.setEmployeeLogonDomain(employeeRoleDto.getEmployeeLogonDomain());
        employeeRole.setEmployeeLogonId(employeeRoleDto.getEmployeeLogonId());
        employeeRole.setEmployeePidId(employeeRoleDto.getEmployeePidId());
        employeeRole.setCity(employeeRoleDto.getCity());
        employeeRole.setRegion(employeeRoleDto.getRegion());
        employeeRole.setCountryLongDesc(employeeRoleDto.getCountryLongDesc());
        employeeRole.setDeltaFlag(employeeRoleDto.getDeltaFlag());
        return employeeRoleRepository.save(employeeRole);
    }

    @Transactional
    public void deleteById(String employeeId) {
        employeeRoleRepository.deleteById(employeeId);

    }


}
