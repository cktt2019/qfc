package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToLegalAgreementsConverter;
import com.cs.qfc.data.jpa.entities.LegalAgreements;
import com.cs.qfc.data.jpa.repositories.LegalAgreementsRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedLegalAgreementsRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.LegalAgreementsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class LegalAgreementsService {


    private static final Logger LOG = LoggerFactory.getLogger(LegalAgreementsService.class);

    @Autowired
    LegalAgreementsRepository legalAgreementsRepository;

    @Autowired
    CustomizedLegalAgreementsRepository customizedLegalAgreementsRepository;


    @Transactional
    public LegalAgreements saveLegalAgreements(LegalAgreementsDTO legalAgreementsDTO) {
        LegalAgreements legalAgreements = DTOToLegalAgreementsConverter.convert(legalAgreementsDTO);
        return legalAgreementsRepository.save(legalAgreements);
    }

    @Transactional
    public LegalAgreements save(LegalAgreements legalAgreements) {
        return legalAgreementsRepository.save(legalAgreements);
    }


    @Transactional
    public Collection<LegalAgreements> findAll() {
        return legalAgreementsRepository.findAll();
    }

    @Transactional
    public Collection<LegalAgreementsDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedLegalAgreementsRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public LegalAgreements findById(Integer id) {
        Optional<LegalAgreements> legalAgreements = legalAgreementsRepository.findById(id);
        return (legalAgreements.isPresent() ? legalAgreements.get() : null);
    }

    @Transactional
    public LegalAgreements updateLegalAgreements(LegalAgreements legalAgreements, LegalAgreementsDTO legalAgreementsDto) {

        legalAgreements.setSourceTxnId(legalAgreementsDto.getSourceTxnId());
        legalAgreements.setA31AsOfDate(legalAgreementsDto.getA31AsOfDate());
        legalAgreements.setA32RecordsEntityIdentifier(legalAgreementsDto.getA32RecordsEntityIdentifier());
        legalAgreements.setA33AgreementIdentifier(legalAgreementsDto.getA33AgreementIdentifier());
        legalAgreements.setA34NameOfAgreementGoverningDoc(legalAgreementsDto.getA34NameOfAgreementGoverningDoc());
        legalAgreements.setA35AgreementDate(legalAgreementsDto.getA35AgreementDate());
        legalAgreements.setA36AgreementCounterpartyIdentifier(legalAgreementsDto.getA36AgreementCounterpartyIdentifier());
        legalAgreements.setA361UnderlyingQfcObligatorIdentifier(legalAgreementsDto.getA361UnderlyingQfcObligatorIdentifier());
        legalAgreements.setA37AgreementGoverningLaw(legalAgreementsDto.getA37AgreementGoverningLaw());
        legalAgreements.setA38CrossDefaultProvision(legalAgreementsDto.getA38CrossDefaultProvision());
        legalAgreements.setA39IdentityOfCrossDefaultEntities(legalAgreementsDto.getA39IdentityOfCrossDefaultEntities());
        legalAgreements.setA310CoveredByThirdPartyCreditEnhancement(legalAgreementsDto.getA310CoveredByThirdPartyCreditEnhancement());
        legalAgreements.setA311ThirdPartyCreditEnhancementProviderIdentifier(legalAgreementsDto.getA311ThirdPartyCreditEnhancementProviderIdentifier());
        legalAgreements.setA312AssociatedCreditEnhancementDocIdentifier(legalAgreementsDto.getA312AssociatedCreditEnhancementDocIdentifier());
        legalAgreements.setA3121CounterpartyThirdPartyCreditEnhancement(legalAgreementsDto.getA3121CounterpartyThirdPartyCreditEnhancement());
        legalAgreements.setA3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier(legalAgreementsDto.getA3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier());
        legalAgreements.setA3123CounterpartyAssociatedCreditEnhancementDocIdentifier(legalAgreementsDto.getA3123CounterpartyAssociatedCreditEnhancementDocIdentifier());
        legalAgreements.setA313CounterpartyContactInfoName(legalAgreementsDto.getA313CounterpartyContactInfoName());
        legalAgreements.setA314CounterpartyContactInfoAddress(legalAgreementsDto.getA314CounterpartyContactInfoAddress());
        legalAgreements.setA315CounterpartyContactInfoPhone(legalAgreementsDto.getA315CounterpartyContactInfoPhone());
        legalAgreements.setA316CounterpartyContactInfoEmail(legalAgreementsDto.getA316CounterpartyContactInfoEmail());
        return legalAgreementsRepository.save(legalAgreements);
    }

    @Transactional
    public void deleteById(Integer id) {
        legalAgreementsRepository.deleteById(id);

    }


}
