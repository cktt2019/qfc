package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToNTPACustodyPositionConverter;
import com.cs.qfc.data.jpa.entities.NTPACustodyPosition;
import com.cs.qfc.data.jpa.repositories.NTPACustodyPositionRepository;
import com.cs.qfc.data.model.NTPACustodyPositionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class NTPACustodyPositionService {


    private static final Logger LOG = LoggerFactory.getLogger(NTPACustodyPositionService.class);

    @Autowired
    NTPACustodyPositionRepository nTPACustodyPositionRepository;

    @Transactional
    public NTPACustodyPosition saveNTPACustodyPosition(NTPACustodyPositionDTO nTPACustodyPositionDTO) {
        NTPACustodyPosition nTPACustodyPosition = DTOToNTPACustodyPositionConverter.convert(nTPACustodyPositionDTO);
        return nTPACustodyPositionRepository.save(nTPACustodyPosition);
    }

    @Transactional
    public NTPACustodyPosition save(NTPACustodyPosition nTPACustodyPosition) {
        return nTPACustodyPositionRepository.save(nTPACustodyPosition);
    }


    @Transactional
    public Collection<NTPACustodyPosition> findAll() {
        return nTPACustodyPositionRepository.findAll();
    }

    @Transactional
    public NTPACustodyPosition findById(Integer id) {
        Optional<NTPACustodyPosition> nTPACustodyPosition = nTPACustodyPositionRepository.findById(id);
        return (nTPACustodyPosition.isPresent() ? nTPACustodyPosition.get() : null);
    }

    @Transactional
    public NTPACustodyPosition updateNTPACustodyPosition(NTPACustodyPosition nTPACustodyPosition, NTPACustodyPositionDTO nTPACustodyPositionDto) {

        nTPACustodyPosition.setBusinessDate(nTPACustodyPositionDto.getBusinessDate());
        nTPACustodyPosition.setCompIdC(nTPACustodyPositionDto.getCompIdC());
        nTPACustodyPosition.setPIdC(nTPACustodyPositionDto.getPIdC());
        nTPACustodyPosition.setCAcIdC(nTPACustodyPositionDto.getCAcIdC());
        nTPACustodyPosition.setCAcSetlmtTypC(nTPACustodyPositionDto.getCAcSetlmtTypC());
        nTPACustodyPosition.setCurIdC(nTPACustodyPositionDto.getCurIdC());
        nTPACustodyPosition.setPnbPosD(nTPACustodyPositionDto.getPnbPosD());
        nTPACustodyPosition.setTNatvCurIdC(nTPACustodyPositionDto.getTNatvCurIdC());
        nTPACustodyPosition.setTBaseCurIdC(nTPACustodyPositionDto.getTBaseCurIdC());
        nTPACustodyPosition.setTConCurIdC(nTPACustodyPositionDto.getTConCurIdC());
        nTPACustodyPosition.setPnbMktValM(nTPACustodyPositionDto.getPnbMktValM());
        nTPACustodyPosition.setPnbMktValBaseM(nTPACustodyPositionDto.getPnbMktValBaseM());
        nTPACustodyPosition.setPnbMktValConM(nTPACustodyPositionDto.getPnbMktValConM());
        nTPACustodyPosition.setPPxClM(nTPACustodyPositionDto.getPPxClM());
        nTPACustodyPosition.setPnbActvyLastId(nTPACustodyPositionDto.getPnbActvyLastId());
        nTPACustodyPosition.setCIdC(nTPACustodyPositionDto.getCIdC());
        nTPACustodyPosition.setCN(nTPACustodyPositionDto.getCN());
        nTPACustodyPosition.setCAcTypC(nTPACustodyPositionDto.getCAcTypC());
        nTPACustodyPosition.setCAcEfD(nTPACustodyPositionDto.getCAcEfD());
        nTPACustodyPosition.setPMatyD(nTPACustodyPositionDto.getPMatyD());
        nTPACustodyPosition.setPStdT(nTPACustodyPositionDto.getPStdT());
        nTPACustodyPosition.setPIsinIdC(nTPACustodyPositionDto.getPIsinIdC());
        nTPACustodyPosition.setPSedolIdC(nTPACustodyPositionDto.getPSedolIdC());
        nTPACustodyPosition.setPFileTypC(nTPACustodyPositionDto.getPFileTypC());
        nTPACustodyPosition.setPnbAcctTypC(nTPACustodyPositionDto.getPnbAcctTypC());
        return nTPACustodyPositionRepository.save(nTPACustodyPosition);
    }

    @Transactional
    public void deleteById(Integer id) {
        nTPACustodyPositionRepository.deleteById(id);

    }


}
