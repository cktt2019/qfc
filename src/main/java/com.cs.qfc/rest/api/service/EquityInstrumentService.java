package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToEquityInstrumentConverter;
import com.cs.qfc.data.jpa.entities.EquityInstrument;
import com.cs.qfc.data.jpa.repositories.EquityInstrumentRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedEquityInstrumentRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.EquityInstrumentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class EquityInstrumentService {


    private static final Logger LOG = LoggerFactory.getLogger(EquityInstrumentService.class);

    @Autowired
    EquityInstrumentRepository equityInstrumentRepository;

    @Autowired
    CustomizedEquityInstrumentRepository customizedEquityInstrumentRepository;

    @Transactional
    public EquityInstrument saveEquityInstrument(EquityInstrumentDTO equityInstrumentDTO) {
        EquityInstrument equityInstrument = DTOToEquityInstrumentConverter.convert(equityInstrumentDTO);
        return equityInstrumentRepository.save(equityInstrument);
    }

    @Transactional
    public EquityInstrument save(EquityInstrument equityInstrument) {
        return equityInstrumentRepository.save(equityInstrument);
    }


    @Transactional
    public Collection<EquityInstrument> findAll() {
        return equityInstrumentRepository.findAll();
    }

    @Transactional
    public Collection<EquityInstrumentDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedEquityInstrumentRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public EquityInstrument findById(String id) {
        Optional<EquityInstrument> equityInstrument = equityInstrumentRepository.findById(id);
        return (equityInstrument.isPresent() ? equityInstrument.get() : null);
    }

    @Transactional
    public EquityInstrument updateEquityInstrument(EquityInstrument equityInstrument, EquityInstrumentDTO equityInstrumentDto) {

        equityInstrument.setIsinId(equityInstrumentDto.getIsinId());
        equityInstrument.setSedolId(equityInstrumentDto.getSedolId());
        equityInstrument.setCusipId(equityInstrumentDto.getCusipId());
        equityInstrument.setSecurityDescriptionLong(equityInstrumentDto.getSecurityDescriptionLong());
        return equityInstrumentRepository.save(equityInstrument);
    }

    @Transactional
    public void deleteById(String id) {
        equityInstrumentRepository.deleteById(id);

    }


}
