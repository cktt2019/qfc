package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToObjectModelConverter;
import com.cs.qfc.data.jpa.entities.ObjectModel;
import com.cs.qfc.data.jpa.repositories.ObjectModelRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedSpecificationRepository;
import com.cs.qfc.data.model.ObjectModelDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class ObjectModelService {


    private static final Logger LOG = LoggerFactory.getLogger(ObjectModelService.class);

    @Autowired
    ObjectModelRepository objectModelRepository;

    @Autowired
    CustomizedSpecificationRepository customizedSpecificationRepository;

    @Transactional
    public ObjectModel saveObjectModel(ObjectModelDTO objectModelDTO) {
        ObjectModel objectmodel = DTOToObjectModelConverter.convert(objectModelDTO);
        return objectModelRepository.save(objectmodel);
    }

    @Transactional
    public ObjectModel save(ObjectModel objectModel) {
        return objectModelRepository.save(objectModel);
    }


    @Transactional
    public Collection<ObjectModel> findAll() {
        return objectModelRepository.findAll();
    }


    @Transactional
    public ObjectModel findById(Integer id) {
        Optional<ObjectModel> objectModel = objectModelRepository.findById(id);
        return (objectModel.isPresent() ? objectModel.get() : null);
    }

    @Transactional
    public ObjectModel updateSpecification(ObjectModel objectModel, ObjectModelDTO objectModelDto) {

        objectModel.setObjectName(objectModelDto.getObjectName());
        objectModel.setObjectDescription(objectModelDto.getObjectDescription());
        objectModel.setAccountId(objectModelDto.getAccountId());
        objectModel.setObjectVersion(objectModelDto.getObjectVersion());
        objectModel.setObjectContent(objectModelDto.getObjectContent());
        return objectModelRepository.save(objectModel);
    }

    @Transactional
    public void deleteById(Integer id) {
        objectModelRepository.deleteById(id);

    }


}
