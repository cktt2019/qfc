package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.converter.DTOToDrdTradeLegConverter;
import com.cs.qfc.data.jpa.entities.DrdTradeLeg;
import com.cs.qfc.data.jpa.repositories.DrdTradeLegRepository;
import com.cs.qfc.data.jpa.repositories.custom.CustomizedDrdTradeLegRepository;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DrdTradeLegDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class DrdTradeLegService {


    private static final Logger LOG = LoggerFactory.getLogger(DrdTradeLegService.class);

    @Autowired
    DrdTradeLegRepository drdTradeLegRepository;
    @Autowired
    CustomizedDrdTradeLegRepository customizedDrdTradeLegRepository;

    @Transactional
    public DrdTradeLeg saveDrdTradeLeg(DrdTradeLegDTO drdTradeLegDTO) {
        DrdTradeLeg drdTradeLeg = DTOToDrdTradeLegConverter.convert(drdTradeLegDTO);
        return drdTradeLegRepository.save(drdTradeLeg);
    }

    @Transactional
    public DrdTradeLeg save(DrdTradeLeg drdTradeLeg) {
        return drdTradeLegRepository.save(drdTradeLeg);
    }


    @Transactional
    public Collection<DrdTradeLeg> findAll() {
        return drdTradeLegRepository.findAll();
    }

    @Transactional
    public Collection<DrdTradeLegDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedDrdTradeLegRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public DrdTradeLeg findById(Integer id) {
        Optional<DrdTradeLeg> drdTradeLeg = drdTradeLegRepository.findById(id);
        return (drdTradeLeg.isPresent() ? drdTradeLeg.get() : null);
    }

    @Transactional
    public DrdTradeLeg updateDrdTradeLeg(DrdTradeLeg drdTradeLeg, DrdTradeLegDTO drdTradeLegDto) {

        drdTradeLeg.setCobDate(drdTradeLegDto.getCobDate());
        drdTradeLeg.setGbmEntity(drdTradeLegDto.getGbmEntity());
        drdTradeLeg.setTradeId(drdTradeLegDto.getTradeId());
        drdTradeLeg.setCounterPartyId(drdTradeLegDto.getCounterPartyId());
        drdTradeLeg.setBookName(drdTradeLegDto.getBookName());
        drdTradeLeg.setCduMasterAgreementLlk(drdTradeLegDto.getCduMasterAgreementLlk());
        drdTradeLeg.setCduCollateralAnnexLlk(drdTradeLegDto.getCduCollateralAnnexLlk());
        drdTradeLeg.setFramesoftMasterAgreementLlk(drdTradeLegDto.getFramesoftMasterAgreementLlk());
        drdTradeLeg.setAlgoCollateralAnnexLlk(drdTradeLegDto.getAlgoCollateralAnnexLlk());
        drdTradeLeg.setTradeDate(drdTradeLegDto.getTradeDate());
        drdTradeLeg.setPrimaryMaturityDate(drdTradeLegDto.getPrimaryMaturityDate());
        drdTradeLeg.setLocalPvCcyCode(drdTradeLegDto.getLocalPvCcyCode());
        drdTradeLeg.setLocalPv(drdTradeLegDto.getLocalPv());
        drdTradeLeg.setPvNotional(drdTradeLegDto.getPvNotional());
        return drdTradeLegRepository.save(drdTradeLeg);
    }

    @Transactional
    public void deleteById(Integer id) {
        drdTradeLegRepository.deleteById(id);

    }


}
