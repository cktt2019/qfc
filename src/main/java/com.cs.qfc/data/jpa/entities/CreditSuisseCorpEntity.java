package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CreditSuisseCorpEntity")
@Entity
public class CreditSuisseCorpEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "creditsuissecorpentity_seq")
    @SequenceGenerator(name = "creditsuissecorpentity_seq", sequenceName = "creditsuissecorpentity_seq")
    @Column(name = "count")
    private Integer count;


    @Column(name = "legal_name_of_entity")
    private String legalNameOfEntity;


    @Column(name = "lei")
    private String lei;


    @Column(name = "immediate_parent_name")
    private String immediateParentName;


    @Column(name = "immediate_parent_lei")
    private String immediateParentLei;


    @Column(name = "percentage_ownership")
    private String percentageOwnership;


    @Column(name = "entity_type")
    private String entityType;


    @Column(name = "domicile")
    private String domicile;


    @Column(name = "jurisdiction_incorp")
    private String jurisdictionIncorp;


}
