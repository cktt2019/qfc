package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PrimeSwap")
@Entity
public class PrimeSwap implements Serializable {

    @Column(name = "business_date")
    private java.sql.Date businessDate;


    @Column(name = "legal_entity")
    private String legalEntity;


    @Column(name = "swap_id")
    private String swapId;


    @Column(name = "swap_version")
    private String swapVersion;


    @Column(name = "counter_party_code")
    private String counterPartyCode;


    @Column(name = "counter_party_name")
    private String counterPartyName;


    @Column(name = "book")
    private String book;


    @Column(name = "trader")
    private String trader;


    @Column(name = "swap_type")
    private String swapType;


    @Column(name = "framesoft_master_agreement_llk")
    private String framesoftMasterAgreementLlk;


    @Column(name = "algo_collateral_agreement_llk")
    private String algoCollateralAgreementLlk;


    @Column(name = "cdu_master_agreement_llk")
    private String cduMasterAgreementLlk;


    @Column(name = "cdu_collateral_annex_llk")
    private String cduCollateralAnnexLlk;


    @Column(name = "swap_trade_date")
    private java.sql.Date swapTradeDate;


    @Column(name = "swap_termination_date")
    private java.sql.Date swapTerminationDate;


    @Column(name = "next_interest_pay_date")
    private java.sql.Date nextInterestPayDate;


    @Column(name = "equity_currency")
    private String equityCurrency;


    @Column(name = "total_swap_value")
    private Float totalSwapValue;


    @Column(name = "currency_equity_notional")
    private String currencyEquityNotional;


    @Column(name = "init_margin")
    private Float initMargin;


    @Column(name = "initial_margin_direction")
    private String initialMarginDirection;


    @Column(name = "csid")
    private String csid;


    @Column(name = "gsid")
    private String gsid;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "primeswap_seq")
    @SequenceGenerator(name = "primeswap_seq", sequenceName = "primeswap_seq")
    @Column(name = "id")
    private Integer id;


}
