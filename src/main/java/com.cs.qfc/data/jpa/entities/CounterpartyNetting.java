package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CounterpartyNetting", schema = "SIERRA_QFC")
@Entity
public class CounterpartyNetting implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "counterpartynetting_seq")
    @SequenceGenerator(name = "counterpartynetting_seq", sequenceName = "SIERRA_QFC.counterpartynetting_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "source_txn_id")
    private String sourceTxnId;


    @Column(name = "a21_as_of_date")
    private String a21AsOfDate;


    @Column(name = "a22_records_entity_identifier")
    private String a22RecordsEntityIdentifier;


    @Column(name = "a23_netting_agreement_counterparty_identifier")
    private String a23NettingAgreementCounterpartyIdentifier;


    @Column(name = "a24_netting_agreement_identifier")
    private String a24NettingAgreementIdentifier;


    @Column(name = "a241_underlying_qfc_obligator_identifier")
    private String a241UnderlyingQfcObligatorIdentifier;


    @Column(name = "a25_covered_by_third_party_credit_enhancement")
    private String a25CoveredByThirdPartyCreditEnhancement;


    @Column(name = "a251_third_party_credit_enhancement_provider_identifier")
    private String a251ThirdPartyCreditEnhancementProviderIdentifier;


    @Column(name = "a252_third_party_credit_enhancement_agreement_identifier")
    private String a252ThirdPartyCreditEnhancementAgreementIdentifier;


    @Column(name = "a253_counterparty_third_party_credit_enhancement")
    private String a253CounterpartyThirdPartyCreditEnhancement;


    @Column(name = "a254_counterparty_third_party_credit_enhancement_provider_identifier")
    private String a254CounterpartyThirdPartyCreditEnhancementProviderIdentifier;


    @Column(name = "a255_counterparty_third_party_credit_enhancement_agreement_identifier")
    private String a255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier;


    @Column(name = "a26_aggregate_curr_market_value_all_positions_usd")
    private String a26AggregateCurrMarketValueAllPositionsUsd;


    @Column(name = "a27_aggregate_curr_market_value_positive_positions_usd")
    private String a27AggregateCurrMarketValuePositivePositionsUsd;


    @Column(name = "a28_aggregate_curr_market_value_negative_positions_usd")
    private String a28AggregateCurrMarketValueNegativePositionsUsd;


    @Column(name = "a29_curr_market_value_all_collateral_records_entity_in_usd")
    private String a29CurrMarketValueAllCollateralRecordsEntityInUsd;


    @Column(name = "a210_curr_market_value_all_collateral_counterparty_in_usd")
    private String a210CurrMarketValueAllCollateralCounterpartyInUsd;


    @Column(name = "a211_curr_market_value_all_collateral_records_entity_rehypo_in_usd")
    private String a211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd;


    @Column(name = "a212_curr_market_value_all_collateral_counterparty_rehypo_in_usd")
    private String a212CurrMarketValueAllCollateralCounterpartyRehypoInUsd;


    @Column(name = "a213_records_entity_collateral_net")
    private String a213RecordsEntityCollateralNet;


    @Column(name = "a214_counterparty_collateral_net")
    private String a214CounterpartyCollateralNet;


    @Column(name = "a215_next_margin_payment_date")
    private String a215NextMarginPaymentDate;


    @Column(name = "a216_next_margin_payment_amount_usd")
    private String a216NextMarginPaymentAmountUsd;


    @Column(name = "a217_safekeeping_agent_identifier_records_entity")
    private String a217SafekeepingAgentIdentifierRecordsEntity;


    @Column(name = "a218_safekeeping_agent_identifier_counterparty")
    private String a218SafekeepingAgentIdentifierCounterparty;


}
