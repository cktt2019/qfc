package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DTPMISGBLDYTREE")
@Entity
public class DTPMISGBLDYTREE implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dtpmisgbldytree_seq")
    @SequenceGenerator(name = "dtpmisgbldytree_seq", sequenceName = "dtpmisgbldytree_seq")
    @Column(name = "mis_member_code")
    private String misMemberCode;


    @Column(name = "americas_product_line_head_id")
    private String americasProductLineHeadId;


    @Column(name = "americas_cluster_head_id")
    private String americasClusterHeadId;


    @Column(name = "apac_cluster_head_id")
    private String apacClusterHeadId;


    @Column(name = "apac_product_line_head_id")
    private String apacProductLineHeadId;


    @Column(name = "business_controller_l2_id")
    private String businessControllerL2Id;


    @Column(name = "cluster")
    private String cluster;


    @Column(name = "cluster_code")
    private String clusterCode;


    @Column(name = "cluster_head_id")
    private String clusterHeadId;


    @Column(name = "desk")
    private String desk;


    @Column(name = "desk_code")
    private String deskCode;


    @Column(name = "division")
    private String division;


    @Column(name = "division_code")
    private String divisionCode;


    @Column(name = "euro_cluster_head_id")
    private String euroClusterHeadId;


    @Column(name = "euro_product_line_head_id")
    private String euroProductLineHeadId;


    @Column(name = "global_product_line_head_id")
    private String globalProductLineHeadId;


    @Column(name = "hierarchy_trader_id")
    private String hierarchyTraderId;


    @Column(name = "mis_hierarchy_sort_code")
    private String misHierarchySortCode;


    @Column(name = "mis_member_name")
    private String misMemberName;


    @Column(name = "mis_member_type_code")
    private String misMemberTypeCode;


    @Column(name = "mis_product")
    private String misProduct;


    @Column(name = "mis_region_code")
    private String misRegionCode;


    @Column(name = "mis_tree_code")
    private String misTreeCode;


    @Column(name = "parent_mis_member_code")
    private String parentMisMemberCode;


    @Column(name = "product_control_section_mgr_id")
    private String productControlSectionMgrId;


    @Column(name = "product_line_code")
    private String productLineCode;


    @Column(name = "product_line_name")
    private String productLineName;


    @Column(name = "sub_3_product_line")
    private String sub3ProductLine;


    @Column(name = "sub_3_product_line_description")
    private String sub3ProductLineDescription;


    @Column(name = "sub_cluster_code")
    private String subClusterCode;


    @Column(name = "sub_cluster_name")
    private String subClusterName;


    @Column(name = "sub_division_code")
    private String subDivisionCode;


    @Column(name = "sub_division_description")
    private String subDivisionDescription;


    @Column(name = "sub_product_description")
    private String subProductDescription;


    @Column(name = "sub_product_line")
    private String subProductLine;


    @Column(name = "swiss_product_line_head_id")
    private String swissProductLineHeadId;


    @Column(name = "trading_supervisor_id")
    private String tradingSupervisorId;


    @Column(name = "trading_supervisor_delegate_id")
    private String tradingSupervisorDelegateId;


    @Column(name = "mis_product_code")
    private String misProductCode;


    @Column(name = "mis_unit_percent_owned")
    private String misUnitPercentOwned;


    @Column(name = "mis_unit_percent_description")
    private String misUnitPercentDescription;


    @Column(name = "delta_flag")
    private String deltaFlag;


    @Column(name = "cdds_tree_node_status")
    private String cddsTreeNodeStatus;


    @Column(name = "trading_support")
    private String tradingSupport;


    @Column(name = "trade_support_manager_email")
    private String tradeSupportManagerEmail;


}
