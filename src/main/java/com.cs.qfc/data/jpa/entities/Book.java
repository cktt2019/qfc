package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Book")
@Entity
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_seq")
    @SequenceGenerator(name = "book_seq", sequenceName = "book_seq")
    @Column(name = "global_book_id")
    private String globalBookId;


    @Column(name = "account_method")
    private String accountMethod;


    @Column(name = "accounting_basis_indicator")
    private String accountingBasisIndicator;


    @Column(name = "book_category_code")
    private String bookCategoryCode;


    @Column(name = "book_category_description")
    private String bookCategoryDescription;


    @Column(name = "book_controller_id")
    private String bookControllerId;


    @Column(name = "book_dormant_indicator")
    private String bookDormantIndicator;


    @Column(name = "book_name")
    private String bookName;


    @Column(name = "book_pnl_system_name")
    private String bookPnlSystemName;


    @Column(name = "book_ref")
    private String bookRef;


    @Column(name = "book_status")
    private String bookStatus;


    @Column(name = "book_sub_category_code")
    private String bookSubCategoryCode;


    @Column(name = "book_sub_category_description")
    private String bookSubCategoryDescription;


    @Column(name = "book_trader_id")
    private String bookTraderId;


    @Column(name = "cny_deliverable_outside_china_indicator")
    private String cnyDeliverableOutsideChinaIndicator;


    @Column(name = "department_id", insertable = false, updatable = false)
    private String departmentId;


    @Column(name = "front_office_representative_id")
    private String frontOfficeRepresentativeId;


    @Column(name = "gpl_atom_code")
    private String gplAtomCode;


    @Column(name = "nhfs_indicator")
    private String nhfsIndicator;


    @Column(name = "remote_booking_policy_indicator")
    private String remoteBookingPolicyIndicator;


    @Column(name = "split_hedge_comments")
    private String splitHedgeComments;


    @Column(name = "split_hedge_indicator")
    private String splitHedgeIndicator;


    @Column(name = "us_tax_category_code")
    private String usTaxCategoryCode;


    @Column(name = "book_last_updated_datetime")
    private java.sql.Timestamp bookLastUpdatedDatetime;


    @Column(name = "financial_business_unit_code")
    private String financialBusinessUnitCode;


    @Column(name = "delta_flag")
    private String deltaFlag;


    @Column(name = "book_request_status_code")
    private String bookRequestStatusCode;


    @Column(name = "first_instance_book_activated")
    private java.sql.Date firstInstanceBookActivated;


    @Column(name = "last_instance_book_activated")
    private java.sql.Date lastInstanceBookActivated;


    @Column(name = "first_instance_book_inactivated")
    private java.sql.Date firstInstanceBookInactivated;


    @Column(name = "last_instance_book_")
    private java.sql.Date lastInstanceBook;


    @Column(name = "email_address")
    private String emailAddress;


    @Column(name = "cav_flag")
    private String cavFlag;


    @Column(name = "fos_flag")
    private String fosFlag;


    @Column(name = "booking_intent")
    private String bookingIntent;


    @Column(name = "book_request_id")
    private String bookRequestId;


    @Column(name = "ipar_empl_id_email_id")
    private String iparEmplIdEmailId;


    @Column(name = "book_trader_email_id")
    private String bookTraderEmailId;


    @Column(name = "pc_supervisor_email_id")
    private String pcSupervisorEmailId;


    @Column(name = "book_emp_id_email_id")
    private String bookEmpIdEmailId;


    @Column(name = "pctnl_empl_id_email_id")
    private String pctnlEmplIdEmailId;


}
