package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MarnaG")
@Entity
public class MarnaG implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marnag_seq")
    @SequenceGenerator(name = "marnag_seq", sequenceName = "marnag_seq")
    @Column(name = "cnid")
    private Integer cnid;


    @Column(name = "agreement_number")
    private String agreementNumber;


    @Column(name = "status")
    private String status;


    @Column(name = "superseded_by_cnid")
    private String supersededByCnid;


    @Column(name = "superseded_by_agreement")
    private String supersededByAgreement;


    @Column(name = "legal_agreement_description")
    private String legalAgreementDescription;


    @Column(name = "cs_entity_bookinggroup_id")
    private String csEntityBookinggroupId;


    @Column(name = "cs_entity_bookinggroup_name")
    private String csEntityBookinggroupName;


    @Column(name = "negotiator")
    private String negotiator;


    @Column(name = "negotiator_location")
    private String negotiatorLocation;


    @Column(name = "counter_party")
    private String counterParty;


    @Column(name = "counter_party_role")
    private String counterPartyRole;


    @Column(name = "csid")
    private String csid;


    @Column(name = "legal_id")
    private String legalId;


    @Column(name = "formerly_known_as")
    private String formerlyKnownAs;


    @Column(name = "country_of_inc")
    private String countryOfInc;


    @Column(name = "contract_type")
    private String contractType;


    @Column(name = "request_type")
    private String requestType;


    @Column(name = "agreement_title")
    private String agreementTitle;


    @Column(name = "contract_date")
    private java.sql.Date contractDate;


    @Column(name = "agency")
    private String agency;


    @Column(name = "collateral_agreement_flag")
    private String collateralAgreementFlag;


    @Column(name = "csa_enforceable_flag")
    private String csaEnforceableFlag;


    @Column(name = "transaction_specific_flag")
    private String transactionSpecificFlag;


    @Column(name = "cs_entity_agency_flag")
    private String csEntityAgencyFlag;


    @Column(name = "desk")
    private String desk;


    @Column(name = "iconid")
    private String iconid;


    @Column(name = "csa_reg_flag")
    private String csaRegFlag;


}
