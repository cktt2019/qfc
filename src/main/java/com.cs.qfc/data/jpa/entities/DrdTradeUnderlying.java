package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DrdTradeUnderlying")
@Entity
public class DrdTradeUnderlying implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drdtradeunderlying_seq")
    @SequenceGenerator(name = "drdtradeunderlying_seq", sequenceName = "drdtradeunderlying_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "cob_date")
    private java.sql.Date cobDate;


    @Column(name = "load_cob_date")
    private java.sql.Date loadCobDate;


    @Column(name = "load_book")
    private String loadBook;


    @Column(name = "book_name")
    private String bookName;


    @Column(name = "trade_load_id")
    private Integer tradeLoadId;


    @Column(name = "load_timestamp")
    private Date loadTimestamp;


    @Column(name = "snapshot_name")
    private String snapshotName;


    @Column(name = "snapshot_id")
    private Integer snapshotId;


    @Column(name = "snapshot_creation_datetime")
    private Date snapshotCreationDatetime;


    @Column(name = "interface_version")
    private String interfaceVersion;


    @Column(name = "trade_id")
    private Integer tradeId;


    @Column(name = "tft_trade_version")
    private Integer tftTradeVersion;


    @Column(name = "trade_location_id")
    private Integer tradeLocationId;


    @Column(name = "primary_tcn")
    private String primaryTcn;


    @Column(name = "tcn")
    private String tcn;


    @Column(name = "trade_version_update_time")
    private Date tradeVersionUpdateTime;


    @Column(name = "cancellation_date")
    private String cancellationDate;


    @Column(name = "gbm_entity")
    private String gbmEntity;


    @Column(name = "approval_status")
    private String approvalStatus;


    @Column(name = "event_reason")
    private String eventReason;


    @Column(name = "external_id")
    private String externalId;


    @Column(name = "primary_maturity_date")
    private java.sql.Date primaryMaturityDate;


    @Column(name = "product_sub_type")
    private String productSubType;


    @Column(name = "product_type")
    private String productType;


    @Column(name = "product_valuation_type")
    private String productValuationType;


    @Column(name = "trade_status")
    private String tradeStatus;


    @Column(name = "underlying_id")
    private String underlyingId;


    @Column(name = "underlying_id_type")
    private String underlyingIdType;


    @Column(name = "account_ccy")
    private String accountCcy;


    @Column(name = "basket_strike")
    private Float basketStrike;


    @Column(name = "basket_weighting")
    private String basketWeighting;


    @Column(name = "coupon")
    private Float coupon;


    @Column(name = "coupon_freq")
    private String couponFreq;


    @Column(name = "final_price")
    private Float finalPrice;


    @Column(name = "floating_rate_index")
    private String floatingRateIndex;


    @Column(name = "floating_rate_index_tenor")
    private String floatingRateIndexTenor;


    @Column(name = "floating_rate_index_type")
    private String floatingRateIndexType;


    @Column(name = "initial_price")
    private Float initialPrice;


    @Column(name = "ref_oblig_category")
    private String refObligCategory;


    @Column(name = "ref_pool_long_name")
    private String refPoolLongName;


    @Column(name = "ref_pool_short_name")
    private String refPoolShortName;


    @Column(name = "sequence_id")
    private Integer sequenceId;


    @Column(name = "underlying_asset_class")
    private String underlyingAssetClass;


    @Column(name = "underlying_ccy")
    private String underlyingCcy;


    @Column(name = "underlying_desc")
    private String underlyingDesc;


    @Column(name = "underlying_ext_id")
    private String underlyingExtId;


    @Column(name = "underlying_ext_id_type")
    private String underlyingExtIdType;


    @Column(name = "underlying_mat_date")
    private java.sql.Date underlyingMatDate;


    @Column(name = "underlying_risk_id")
    private String underlyingRiskId;


    @Column(name = "pay_rec")
    private String payRec;


    @Column(name = "settlement_type")
    private String settlementType;


    @Column(name = "business_group")
    private String businessGroup;


    @Column(name = "valuation_class")
    private String valuationClass;


    @Column(name = "valuation_type")
    private String valuationType;


    @Column(name = "trade_source_system")
    private String tradeSourceSystem;


    @Column(name = "underlying_price_source")
    private String underlyingPriceSource;


    @Column(name = "cdu_master_agreement_llk")
    private String cduMasterAgreementLlk;


    @Column(name = "cdu_collateral_annex_llk")
    private String cduCollateralAnnexLlk;


    @Column(name = "framesoft_master_agreement_llk")
    private Integer framesoftMasterAgreementLlk;


    @Column(name = "algo_collateral_annex_llk")
    private Integer algoCollateralAnnexLlk;


    @Column(name = "gbm_book_ref")
    private String gbmBookRef;


    @Column(name = "underlying_product_id")
    private String underlyingProductId;


    @Column(name = "underlying_product_type")
    private String underlyingProductType;


    @Column(name = "underlying_risk_type")
    private String underlyingRiskType;


    @Column(name = "bankruptcy")
    private String bankruptcy;


    @Column(name = "deliv_oblig_accelerated_or_matured")
    private String delivObligAcceleratedMatured;


    @Column(name = "deliv_oblig_accrued_interest")
    private String delivObligAccruedInterest;


    @Column(name = "deliv_oblig_assignable_loan")
    private String delivObligAssignableLoan;


    @Column(name = "deliv_oblig_category")
    private String delivObligCategory;


    @Column(name = "deliv_oblig_consent_required_loan")
    private String delivObligConsentRequiredLoan;


    @Column(name = "deliv_oblg_direct_loan_prt_cptn")
    private String delivOblgDirectLoanPrtCptn;


    @Column(name = "deliv_oblig_listed")
    private String delivObligListed;


    @Column(name = "deliv_oblig_maximum_maturity")
    private Integer delivObligMaximumMaturity;


    @Column(name = "deliv_oblig_max_maturity_specified")
    private String delivObligMaxMaturitySpecified;


    @Column(name = "deliv_oblig_not_bearer")
    private String delivObligNotBearer;


    @Column(name = "deliv_oblig_not_contingent")
    private String delivObligNotContingent;


    @Column(name = "deliv_oblig_not_domestic_currency")
    private String delivObligNotDomesticCurrency;


    @Column(name = "deliv_oblig_not_domestic_issuance")
    private String delivObligNotDomesticIssuance;


    @Column(name = "deliv_oblig_not_domestic_law")
    private String delivObligNotDomesticLaw;


    @Column(name = "deliv_oblig_not_sovereign_lender")
    private String delivObligNotSovereignLender;


    @Column(name = "deliv_oblig_phy_paripassu")
    private String delivObligPhyParipassu;


    @Column(name = "deliv_oblig_specified_ccy_list")
    private String delivObligSpecifiedCcyList;


    @Column(name = "deliv_oblig_specified_ccy_type")
    private String delivObligSpecifiedCcyType;


    @Column(name = "deliv_oblig_transferable")
    private String delivObligTransferable;


    @Column(name = "entity_type")
    private String entityType;


    @Column(name = "primary_obligor")
    private String primaryObligor;


    @Column(name = "recovery_value")
    private Float recoveryValue;


    @Column(name = "reference_entity_red_code")
    private String referenceEntityRedCode;


    @Column(name = "reference_entity_unique_entity_id")
    private String referenceEntityUniqueEntityId;


    @Column(name = "reference_obligation_bloomberg_id")
    private String referenceObligationBloombergId;


    @Column(name = "reference_obligation_cusip")
    private String referenceObligationCusip;


    @Column(name = "reference_obligation_guarantor")
    private String referenceObligationGuarantor;


    @Column(name = "reference_obligation_isin")
    private String referenceObligationIsin;


    @Column(name = "reference_price")
    private Float referencePrice;


    @Column(name = "ref_obligor_coupon_rate")
    private String refObligorCouponRate;


    @Column(name = "ref_obligor_maturity_date")
    private java.sql.Date refObligorMaturityDate;


    @Column(name = "ref_obligor_notional_amt")
    private Float refObligorNotionalAmt;


    @Column(name = "ref_obligor_notional_amt_ccy")
    private String refObligorNotionalAmtCcy;


    @Column(name = "ref_obligor_obligation_category")
    private String refObligorObligationCategory;


    @Column(name = "val_unwind")
    private Float valUnwind;


}
