package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CreditSuisseLegalEntity")
@Entity
public class CreditSuisseLegalEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "creditsuisselegalentity_seq")
    @SequenceGenerator(name = "creditsuisselegalentity_seq", sequenceName = "creditsuisselegalentity_seq")
    @Column(name = "Count")
    private Integer count;


    @Column(name = "enterprise_d_number")
    private Integer enterpriseDNumber;


    @Column(name = "booking_entity_nm")
    private String bookingEntityNm;


    @Column(name = "people_soft_id")
    private String peopleSoftId;


    @Column(name = "csid_id")
    private String csidId;


}
