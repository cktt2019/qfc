package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "LegalAgreements", schema = "SIERRA_QFC")
@Entity
public class LegalAgreements implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "legalagreements_seq")
    @SequenceGenerator(name = "legalagreements_seq", sequenceName = "SIERRA_QFC.legalagreements_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "source_txn_id")
    private String sourceTxnId;


    @Column(name = "a31_as_of_date")
    private String a31AsOfDate;


    @Column(name = "a32_records_entity_identifier")
    private String a32RecordsEntityIdentifier;


    @Column(name = "a33_agreement_identifier")
    private String a33AgreementIdentifier;


    @Column(name = "a34_name_of_agreement_or_governing_doc")
    private String a34NameOfAgreementGoverningDoc;


    @Column(name = "a35_agreement_date")
    private String a35AgreementDate;


    @Column(name = "a36_agreement_counterparty_identifier")
    private String a36AgreementCounterpartyIdentifier;


    @Column(name = "a361_underlying_qfc_obligator_identifier")
    private String a361UnderlyingQfcObligatorIdentifier;


    @Column(name = "a37_agreement_governing_law")
    private String a37AgreementGoverningLaw;


    @Column(name = "a38_cross_default_provision")
    private String a38CrossDefaultProvision;


    @Column(name = "a39_identity_of_cross_default_entities")
    private String a39IdentityOfCrossDefaultEntities;


    @Column(name = "a310_covered_by_third_party_credit_enhancement")
    private String a310CoveredByThirdPartyCreditEnhancement;


    @Column(name = "a311_third_party_credit_enhancement_provider_identifier")
    private String a311ThirdPartyCreditEnhancementProviderIdentifier;


    @Column(name = "a312_associated_credit_enhancement_doc_identifier")
    private String a312AssociatedCreditEnhancementDocIdentifier;


    @Column(name = "a3121_counterparty_third_party_credit_enhancement")
    private String a3121CounterpartyThirdPartyCreditEnhancement;


    @Column(name = "a3122_counterparty_third_party_credit_enhancement_provider_identifier")
    private String a3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier;


    @Column(name = "a3123_counterparty_associated_credit_enhancement_doc_identifier")
    private String a3123CounterpartyAssociatedCreditEnhancementDocIdentifier;


    @Column(name = "a313_counterparty_contact_info_name")
    private String a313CounterpartyContactInfoName;


    @Column(name = "a314_counterparty_contact_info_address")
    private String a314CounterpartyContactInfoAddress;


    @Column(name = "a315_counterparty_contact_info_phone")
    private String a315CounterpartyContactInfoPhone;


    @Column(name = "a316_counterparty_contact_info_email")
    private String a316CounterpartyContactInfoEmail;


}
