package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DebtInstrument")
@Entity
public class DebtInstrument implements Serializable {

    @Id
    @Column(name = "gpi_issue")
    private String gpi_issue;


    @Column(name = "isin_id")
    private String isinId;


    @Column(name = "sedol_id")
    private String sedolId;


    @Column(name = "security_description_long")
    private String securityDescriptionLong;


    @Column(name = "nxt_pay_date")
    private java.sql.Date nxtPayDate;


    @Column(name = "next_call_date")
    private java.sql.Date nextCallDate;


    @Column(name = "next_put_date")
    private java.sql.Date nextPutDate;


}
