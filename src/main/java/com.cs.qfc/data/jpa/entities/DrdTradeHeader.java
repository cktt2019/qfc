package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DrdTradeHeader")
@Entity
public class DrdTradeHeader implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drdtradeheader_seq")
    @SequenceGenerator(name = "drdtradeheader_seq", sequenceName = "drdtradeheader_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "cob_date")
    private java.sql.Date cobDate;


    @Column(name = "load_cod_date")
    private java.sql.Date loadCodDate;


    @Column(name = "book_name")
    private String bookName;


    @Column(name = "trade_load_id")
    private String tradeLoadId;


    @Column(name = "load_timestamp")
    private Date loadTimestamp;


    @Column(name = "snapshot_name")
    private String snapshotName;


    @Column(name = "snapshot_id")
    private Integer snapshotId;


    @Column(name = "snapshot_creation_datetime")
    private Date snapshotCreationDatetime;


    @Column(name = "interface_version")
    private String interfaceVersion;


    @Column(name = "trade_id")
    private Integer tradeId;


    @Column(name = "tf_trade_version")
    private Integer tfTradeVersion;


    @Column(name = "trade_location_id")
    private Integer tradeLocationId;


    @Column(name = "primary_tcn")
    private String primaryTcn;


    @Column(name = "trade_version_update_time")
    private Date tradeVersionUpdateTime;


    @Column(name = "cancellation_date")
    private String cancellationDate;


    @Column(name = "xccy_indicator")
    private String xccyIndicator;


    @Column(name = "gbm_entity")
    private String gbmEntity;


    @Column(name = "total_local_pv")
    private Integer totalLocalPv;


    @Column(name = "total_local_pv_ccy_code")
    private String totalLocalPvCcyCode;


    @Column(name = "total_pv_status")
    private String totalPvStatus;


    @Column(name = "total_rp_tpv")
    private Integer totalRpTpv;


    @Column(name = "trade_pv_reg_date")
    private java.sql.Date tradePvRegDate;


    @Column(name = "trade_pv_reg_days")
    private Integer tradePvRegDays;


    @Column(name = "confirmation_status")
    private String confirmationStatus;


    @Column(name = "default_fair_value")
    private Integer defaultFairValue;


    @Column(name = "prove_cluster")
    private String proveCluster;


    @Column(name = "prove_family")
    private String proveFamily;


    @Column(name = "prove_id")
    private String proveId;


    @Column(name = "prove_product")
    private String proveProduct;


    @Column(name = "approval_status")
    private String approvalStatus;


    @Column(name = "event_reason")
    private String eventReason;


    @Column(name = "external_id")
    private String externalId;


    @Column(name = "primary_maturity_date")
    private java.sql.Date primaryMaturityDate;


    @Column(name = "product_sub_type")
    private String productSubType;


    @Column(name = "product_type")
    private String productType;


    @Column(name = "product_valuation_type")
    private String productValuationType;


    @Column(name = "root_tcn")
    private String rootTcn;


    @Column(name = "trade_status")
    private String tradeStatus;


    @Column(name = "cdu_master_agreement_llk")
    private String cduMasterAgreementLlk;


    @Column(name = "cdu_collateral_annex_llk")
    private String cduCollateralAnnexLlk;


    @Column(name = "freamesoft_master_agreement_llk")
    private Integer freamesoftMasterAgreementLlk;


    @Column(name = "algo_collateral_annex_llk")
    private Integer algoCollateralAnnexLlk;


    @Column(name = "account_area")
    private String accountArea;


    @Column(name = "broker_id")
    private String brokerId;


    @Column(name = "coll_indep_amount")
    private Float collIndepAmount;


    @Column(name = "coll_indep_ccy")
    private String collIndepCcy;


    @Column(name = "coll_indep_other_marg_quote")
    private String collIndepOtherMargQuote;


    @Column(name = "coll_indep_percent")
    private Float collIndepPercent;


    @Column(name = "coll_indep_type")
    private String collIndepType;


    @Column(name = "marketer_id")
    private String marketerId;


    @Column(name = "risk_management_only_flag")
    private String riskManagementOnlyFlag;


    @Column(name = "structured_trade_flag")
    private String structuredTradeFlag;


    @Column(name = "trade_date")
    private java.sql.Date tradeDate;


    @Column(name = "trader_name")
    private String traderName;


    @Column(name = "trade_source_system")
    private String tradeSourceSystem;


    @Column(name = "trading_party_id")
    private String tradingPartyId;


    @Column(name = "trading_party_id_type")
    private String tradingPartyIdType;


    @Column(name = "city")
    private String city;


    @Column(name = "deal")
    private String deal;


    @Column(name = "strip_id")
    private String stripId;


    @Column(name = "customer_number")
    private String customerNumber;


    @Column(name = "display_id")
    private String displayId;


    @Column(name = "asset_class1")
    private String assetClass1;


    @Column(name = "asset_class2")
    private String assetClass2;


    @Column(name = "asset_class3")
    private String assetClass3;


    @Column(name = "asset_class4")
    private String assetClass4;


    @Column(name = "linked_set_type")
    private String linkedSetType;


    @Column(name = "sys_link_id")
    private String sysLinkId;


    @Column(name = "lead_trade_id")
    private Integer leadTradeId;


    @Column(name = "lead_trade_version")
    private Integer leadTradeVersion;


    @Column(name = "lead_trade_location_id")
    private Integer leadTradeLocationId;


    @Column(name = "lead_external_id")
    private String leadExternalId;


    @Column(name = "lead_trade_book")
    private String leadTradeBook;


    @Column(name = "wash_trade_id")
    private Integer washTradeId;


    @Column(name = "wash_trade_version")
    private Integer washTradeVersion;


    @Column(name = "wash_trade_location_id")
    private Integer washTradeLocationId;


    @Column(name = "wash_external_id")
    private String washExternalId;


    @Column(name = "wash_trade_book")
    private String washTradeBook;


    @Column(name = "risk_trade_id")
    private Integer riskTradeId;


    @Column(name = "risk_trade_version")
    private Integer riskTradeVersion;


    @Column(name = "risk_trade_location_id")
    private Integer riskTradeLocationId;


    @Column(name = "risk_external_id")
    private String riskExternalId;


    @Column(name = "risk_trade_book")
    private String riskTradeBook;


    @Column(name = "synthetic_trade_indicator")
    private String syntheticTradeIndicator;


    @Column(name = "linkage_status")
    private String linkageStatus;


    @Column(name = "linkage_status_description")
    private String linkageStatusDescription;


    @Column(name = "linked_trade_id")
    private Integer linkedTradeId;


    @Column(name = "linked_primary_tcn")
    private String linkedPrimaryTcn;


    @Column(name = "linked_trade_version")
    private Integer linkedTradeVersion;


    @Column(name = "linked_trade_location_id")
    private String linkedTradeLocationId;


    @Column(name = "linked_trade_external_id")
    private String linkedTradeExternalId;


    @Column(name = "linked_trade_book")
    private String linkedTradeBook;


    @Column(name = "valuation_class")
    private String valuationClass;


    @Column(name = "valuation_type")
    private String valuationType;


    @Column(name = "premium_amount")
    private Float premiumAmount;


    @Column(name = "swaps_wire_id")
    private String swapsWireId;


    @Column(name = "approx_booking")
    private String approxBooking;


    @Column(name = "approx_booking_review_date")
    private java.sql.Date approxBookingReviewDate;


    @Column(name = "docs_counter_party_id")
    private String docsCounterPartyId;


    @Column(name = "clo_indicator")
    private String cloIndicator;


    @Column(name = "loan_mitigation_flag")
    private String loanMitigationFlag;


    @Column(name = "usi")
    private String usi;


    @Column(name = "cssi")
    private String cssi;


    @Column(name = "gmb_book_id")
    private Integer gmbBookId;


    @Column(name = "area")
    private String area;


    @Column(name = "structured_id")
    private String structuredId;


    @Column(name = "pv_level")
    private String pvLevel;


    @Column(name = "counter_party_csid")
    private String counterPartyCsid;


    @Column(name = "cree_trade_identifier")
    private String creeTradeIdentifier;


    @Column(name = "cree_trade_identifier_version")
    private String creeTradeIdentifierVersion;


    @Column(name = "lch_orig_counter_party")
    private String lchOrigCounterParty;


    @Column(name = "gbm_cluster")
    private String gbmCluster;


    @Column(name = "gbm_division")
    private String gbmDivision;


    @Column(name = "affirm_platform_status")
    private String affirmPlatformStatus;


    @Column(name = "docs_actual_processing_method")
    private String docsActualProcessingMethod;


    @Column(name = "uti")
    private String uti;


    @Column(name = "swire_manual_confirmed_req")
    private String swireManualConfirmedReq;


    @Column(name = "entry_date_time")
    private java.sql.Date entryDateTime;


    @Column(name = "funding_indicator")
    private String fundingIndicator;


    @Column(name = "leverage_factor")
    private Float leverageFactor;


    @Column(name = "link_trans_id")
    private String linkTransId;


    @Column(name = "link_type")
    private String linkType;


    @Column(name = "master_trade_trade_id")
    private String masterTradeTradeId;


    @Column(name = "gbm_book_ref")
    private String gbmBookRef;


    @Column(name = "last_ammend_by")
    private String lastAmmendBy;


    @Column(name = "structure_type")
    private String structureType;


    @Column(name = "garantia_sys_id")
    private String garantiaSysId;


    @Column(name = "early_tm_term_party")
    private String earlyTmTermParty;


    @Column(name = "early_tm_opt_start_date")
    private java.sql.Date earlyTmOptStartDate;


    @Column(name = "counter_party_gsid")
    private String counterPartyGsid;


    @Column(name = "entity_type")
    private String entityType;


    @Column(name = "event")
    private String event;


    @Column(name = "event_trade_date")
    private java.sql.Date eventTradeDate;


    @Column(name = "fidc_pm_initial_margin")
    private String fidcPmInitialMargin;


    @Column(name = "fidc_pm_netting_only")
    private String fidcPmNettingOnly;


    @Column(name = "fidc_pm_variation_margin")
    private String fidcPmVariationMargin;


    @Column(name = "late_trd_notification_reason")
    private String lateTrdNotificationReason;


    @Column(name = "master_agreement_type")
    private Integer masterAgreementType;


    @Column(name = "notes_front_office")
    private String notesFrontOffice;


    @Column(name = "risk_advisor")
    private String riskAdvisor;


    @Column(name = "risk_trader")
    private String riskTrader;


    @Column(name = "trading_party_gsid")
    private String tradingPartyGsid;


    @Column(name = "fidc_pm_ind")
    private String fidcPmInd;


    @Column(name = "agent")
    private String agent;


    @Column(name = "payment_indicator")
    private String paymentIndicator;


    @Column(name = "renegotiation_date")
    private java.sql.Date renegotiationDate;


    @Column(name = "alternative_version_id")
    private Integer alternativeVersionId;


    @Column(name = "cifbu")
    private String cifbu;


    @Column(name = "desk_book")
    private String deskBook;


    @Column(name = "metal_ink_trade_id")
    private String metalInkTradeId;


    @Column(name = "linked_start_date")
    private java.sql.Date linkedStartDate;


    @Column(name = "linked_end_date")
    private java.sql.Date linkedEndDate;


    @Column(name = "salesperson_id")
    private String salespersonId;


    @Column(name = "source_system_id")
    private String sourceSystemId;


    @Column(name = "source_system_version")
    private Integer sourceSystemVersion;


    @Column(name = "early_tm_exercise_freq")
    private String earlyTmExerciseFreq;


    @Column(name = "early_tm_opt_style")
    private String earlyTmOptStyle;


    @Column(name = "payment_package_sys_link_id")
    private String paymentPackageSysLinkId;


    @Column(name = "tf_match_status")
    private String tfMatchStatus;


    @Column(name = "best_match_tf_trade_version")
    private Integer bestMatchTfTradeVersion;


}
