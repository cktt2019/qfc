package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ObjectModel")
@Entity
public class ObjectModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "objectmodel_seq")
    @SequenceGenerator(name = "objectmodel_seq", sequenceName = "objectmodel_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "account_id")
    private String accountId;

    @Column(name = "object_name")
    private String objectName;


    @Column(name = "objectDescription")
    private String objectDescription;


    @Column(name = "objectVersion")
    private String objectVersion;

    @Lob
    @Column(name = "objectContent")
    private String objectContent;


}
