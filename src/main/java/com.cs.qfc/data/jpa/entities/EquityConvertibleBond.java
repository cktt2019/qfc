package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EquityConvertibleBond")
@Entity
public class EquityConvertibleBond implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "equityconvertiblebond_seq")
    @SequenceGenerator(name = "equityconvertiblebond_seq", sequenceName = "equityconvertiblebond_seq")
    @Column(name = "cusip_id")
    private String cusipId;


    @Column(name = "isin_id")
    private String isinId;


    @Column(name = "next_call_date")
    private java.sql.Date nextCallDate;


    @Column(name = "next_put_date")
    private String nextPutDate;


    @Column(name = "nxt_pay_date")
    private java.sql.Date nxtPayDate;


    @Column(name = "security_description")
    private String securityDescription;

    @Column(name = "security_description_long")
    private String securityDescriptionLong;


    @Column(name = "a_3c7_ind")
    private String a3c7Ind;


    @Column(name = "bb_y_ket")
    private String bbYKet;


    @Column(name = "comp_exch_code_der")
    private String compExchCodeDer;


    @Column(name = "conv_price")
    private Float convPrice;


    @Column(name = "con_ratio")
    private Float conRatio;


    @Column(name = "cs_managed")
    private String csManaged;


    @Column(name = "equity_float")
    private Float equityFloat;


    @Column(name = "exchange")
    private String exchange;


    @Column(name = "fidesa_code")
    private String fidesaCode;


    @Column(name = "fund_objective")
    private String fundObjective;


    @Column(name = "gloss_g5")
    private String glossG5;


    @Column(name = "id_bb_global")
    private String idBbGlobal;


    @Column(name = "id_bb_unique")
    private String idBbUnique;


    @Column(name = "italy_id")
    private String italyId;


    @Column(name = "japan_id")
    private String japanId;


    @Column(name = "ric_id")
    private String ricId;


    @Column(name = "sing_id")
    private String singId;


    @Column(name = "valoren_id")
    private String valorenId;


    @Column(name = "wertp_id")
    private String wertpId;


    @Column(name = "hugo_id")
    private String hugoId;


    @Column(name = "ntpa_ls_eqy_po_dt")
    private String ntpaLsEqyPoDt;


    @Column(name = "london_ims_description")
    private String londonImsDescription;


    @Column(name = "maturity_date")
    private java.sql.Date maturityDate;


    @Column(name = "nasdaq_report_code_der")
    private String nasdaqReportCodeDer;


    @Column(name = "primary_ric")
    private String primaryRic;


    @Column(name = "qty_per_undly_shr")
    private Float qtyPerUndlyShr;


    @Column(name = "undly_per_adr")
    private String undlyPerAdr;


    @Column(name = "regs")
    private String regs;


    @Column(name = "region_prim")
    private String regionPrim;


    @Column(name = "rtr_ticket_symbol")
    private String rtrTicketSymbol;


    @Column(name = "trade_cntry")
    private String tradeCntry;


    @Column(name = "undly_sec_isin")
    private String undlySecIsin;


    @Column(name = "voting_right_per_share")
    private Float votingRightPerShare;


    @Column(name = "cmu_id")
    private String cmuId;


    @Column(name = "ult_prnt_ticket_exchange")
    private String ultPrntTicketExchange;


    @Column(name = "poets_id")
    private String poetsId;


    @Column(name = "ntpa_soi")
    private String ntpaSoi;


    @Column(name = "cpn")
    private Float cpn;


    @Column(name = "ssr_liquidity_indicator")
    private String ssrLiquidityIndicator;


    @Column(name = "re_exchange")
    private String reExchange;


    @Column(name = "gloss_k3")
    private String glossK3;


    @Column(name = "sec_type")
    private String secType;


    @Column(name = "fund_euro_direct_ucits")
    private String fundEuroDirectUcits;


    @Column(name = "fund_asset_class_focus")
    private String fundAssetClassFocus;


    @Column(name = "bb_company_id")
    private String bbCompanyId;


    @Column(name = "sp_issue_id")
    private String spIssueId;


    @Column(name = "fund_closed_new_inv")
    private String fundClosedNewInv;


    @Column(name = "legal_name")
    private String legalName;


    @Column(name = "trading_lot_size")
    private Float tradingLotSize;


    @Column(name = "re_adr_undly_ric")
    private String reAdrUndlyRic;


    @Column(name = "relative_index")
    private String relativeIndex;


    @Column(name = "prim_trad")
    private String primTrad;


    @Column(name = "cumulative_flag")
    private String cumulativeFlag;


    @Column(name = "fund_custodian_name")
    private String fundCustodianName;


    @Column(name = "initial_pub_offer")
    private java.sql.Date initialPubOffer;


    @Column(name = "gics_sector_isr")
    private String gicsSectorIsr;


    @Column(name = "gics_sector_name_isr")
    private String gicsSectorNameIsr;


    @Column(name = "gics_industry_group_isr")
    private String gicsIndustryGroupIsr;


    @Column(name = "gics_industry_group_name_isr")
    private String gicsIndustryGroupNameIsr;


    @Column(name = "gics_industry_sr")
    private String gicsIndustrySr;


    @Column(name = "gics_industry_name_isr")
    private String gicsIndustryNameIsr;


    @Column(name = "naics_id_isr")
    private String naicsIdIsr;


    @Column(name = "ntpa_country_of_settlement")
    private String ntpaCountryOfSettlement;


    @Column(name = "gli_rp")
    private String gliRp;


    @Column(name = "cdds_last_updated_time")
    private String cddsLastUpdatedTime;


    @Column(name = "publicly_traded")
    private String publiclyTraded;


    @Column(name = "cs_settle_days")
    private String csSettleDays;


    @Column(name = "bb_underlying_security")
    private String bbUnderlyingSecurity;


    @Column(name = "bb_underlying_type_code")
    private String bbUnderlyingTypeCode;


    @Column(name = "tick_size_pilot_group")
    private String tickSizePilotGroup;


    @Column(name = "called_date")
    private java.sql.Date calledDate;


    @Column(name = "called_price")
    private Float calledPrice;


    @Column(name = "coupon_type")
    private String couponType;


    @Column(name = "re_class_scheme")
    private String reClassScheme;


    @Column(name = "legal_entity_identifier")
    private String legalEntityIdentifier;


    @Column(name = "company_corp_ticker")
    private String companyCorpTicker;


    @Column(name = "company_to_parent")
    private String companyToParent;


    @Column(name = "bb_issuer_type")
    private String bbIssuerType;


    @Column(name = "fisec_issuer_id")
    private String fisecIssuerId;


    @Column(name = "ult_prnt_comp_id")
    private String ultPrntCompId;


    @Column(name = "is_archived")
    private Integer isArchived;


    @Column(name = "local_exchange_symbol")
    private String localExchangeSymbol;


    @Column(name = "cfi_code")
    private String cfiCode;


    @Column(name = "fisn")
    private String fisn;


    @Column(name = "iss_date")
    private java.sql.Date issDate;


    @Column(name = "first_settlement_date")
    private java.sql.Date firstSettlementDate;


    @Column(name = "when_issued_flag")
    private String whenIssuedFlag;


    @Column(name = "minor_trading_currency_ind")
    private String minorTradingCurrencyInd;


    @Column(name = "risk_alert")
    private String riskAlert;


    @Column(name = "mds_id")
    private String mdsId;


    @Column(name = "fund_geographic_focus")
    private String fundGeographicFocus;


    @Column(name = "fund_leverage_amount")
    private String fundLeverageAmount;


    @Column(name = "cms_code")
    private String cmsCode;


    @Column(name = "id_spi")
    private String idSpi;


    @Column(name = "static_listing_id")
    private String staticListingId;


    @Column(name = "client_type")
    private String clientType;


    @Column(name = "marketing_desk")
    private String marketingDesk;


    @Column(name = "external_id")
    private String externalId;


    @Column(name = "issue_book")
    private String issueBook;


    @Column(name = "dutch_id")
    private String dutchId;


    @Column(name = "belgium_id")
    private String belgiumId;


    @Column(name = "istar_id")
    private String istarId;


    @Column(name = "id_bb_security")
    private Float idBbSecurity;


    @Column(name = "operating_mic")
    private String operatingMic;


    @Column(name = "bb_industry_sector_cd")
    private Integer bbIndustrySectorCd;


    @Column(name = "bb_industry_group_cd")
    private Integer bbIndustryGroupCd;


    @Column(name = "bb_industry_sub_group_cd")
    private Integer bbIndustrySubGroupCd;


    @Column(name = "cdds_rec_creation_time")
    private java.sql.Timestamp cddsRecCreationTime;


    @Column(name = "listed_exch_ind")
    private String listedExchInd;


    @Column(name = "id_malaysian")
    private String idMalaysian;


    @Column(name = "redemption_price")
    private Float redemptionPrice;


    @Column(name = "series")
    private String series;


    @Column(name = "cpn_freq")
    private String cpnFreq;


    @Column(name = "gm_soi")
    private String gmSoi;


    @Column(name = "cs_pvt_placement_ind")
    private String csPvtPlacementInd;


}
