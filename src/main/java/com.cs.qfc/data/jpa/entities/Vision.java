package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Vision")
@Entity
public class Vision implements Serializable {

    @Column(name = "business_date")
    private java.sql.Date businessDate;


    @Column(name = "entity")
    private String entity;


    @Column(name = "cpty")
    private String cpty;


    @Column(name = "book")
    private String book;


    @Column(name = "prove_name_product")
    private String proveNameProduct;


    @Column(name = "agreement_id1")
    private String agreementId1;


    @Column(name = "trade_date")
    private java.sql.Date tradeDate;


    @Column(name = "end_date")
    private java.sql.Date endDate;


    @Column(name = "bond_next_coup")
    private String bondNextCoup;


    @Column(name = "cash_curr")
    private String cashCurr;


    @Column(name = "pv_usd")
    private String pvUsd;


    @Column(name = "default_fv_level")
    private String defaultFvLevel;


    @Column(name = "face")
    private String face;


    @Column(name = "value_on")
    private String valueOn;


    @Column(name = "rehypothecation_indicator")
    private String rehypothecationIndicator;


    @Column(name = "custodian_id")
    private String custodianId;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vision_seq")
    @SequenceGenerator(name = "vision_seq", sequenceName = "vision_seq")
    @Column(name = "id")
    private Integer id;


}
