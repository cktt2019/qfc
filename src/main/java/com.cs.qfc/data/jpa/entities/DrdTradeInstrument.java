package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DrdTradeInstrument")
@Entity
public class DrdTradeInstrument implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drdtradeinstrument_seq")
    @SequenceGenerator(name = "drdtradeinstrument_seq", sequenceName = "drdtradeinstrument_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "cob_date")
    private java.sql.Date cobDate;


    @Column(name = "gbm_entity")
    private String gbmEntity;


    @Column(name = "trade_id")
    private Integer tradeId;


    @Column(name = "counter_party_id")
    private String counterPartyId;


    @Column(name = "book_name")
    private String bookName;


    @Column(name = "asset_class")
    private String assetClass;


    @Column(name = "cdu_master_agreement_llk")
    private String cduMasterAgreementLlk;


    @Column(name = "cdu_collateral_annex_llk")
    private String cduCollateralAnnexLlk;


    @Column(name = "framesoft_master_agreement_llk")
    private String framesoftMasterAgreementLlk;


    @Column(name = "algo_collateral_annex_llk")
    private String algoCollateralAnnexLlk;


    @Column(name = "trade_date")
    private java.sql.Date tradeDate;


    @Column(name = "primary_maturity_date")
    private java.sql.Date primaryMaturityDate;


    @Column(name = "local_pv_ccy_code")
    private String localPvCcyCode;


    @Column(name = "local_pv")
    private Integer localPv;


}
