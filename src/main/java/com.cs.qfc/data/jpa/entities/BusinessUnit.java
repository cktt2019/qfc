package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "BusinessUnit")
@Entity
@IdClass(BusinessUnitKey.class)
public class BusinessUnit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "businessunit_seq")
    @SequenceGenerator(name = "businessunit_seq", sequenceName = "businessunit_seq")
    @Column(name = "financial_business_unit_code")
    private String financialBusinessUnitCode;


    @Column(name = "base_currency")
    private String baseCurrency;


    @Column(name = "business_unit_active_indicator")
    private String businessUnitActiveIndicator;


    @Column(name = "business_unit_description")
    private String businessUnitDescription;


    @Column(name = "business_unit_effective_date")
    private java.sql.Date businessUnitEffectiveDate;


    @Column(name = "business_unit_german_description")
    private String businessUnitGermanDescription;


    @Column(name = "business_unit_german_name")
    private String businessUnitGermanName;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "businessunit_seq")
    @SequenceGenerator(name = "businessunit_seq", sequenceName = "businessunit_seq")
    @Column(name = "business_unit_id")
    private String businessUnitId;


    @Column(name = "business_unit_last_update_datetime")
    private java.sql.Timestamp businessUnitLastUpdateDatetime;


    @Column(name = "business_unit_last_updated_by")
    private String businessUnitLastUpdatedBy;


    @Column(name = "business_unit_name")
    private String businessUnitName;


    @Column(name = "business_unit_short_description")
    private String businessUnitShortDescription;


    @Column(name = "business_unit_short_german_description")
    private String businessUnitShortGermanDescription;


    @Column(name = "ch_business_unit_last_updated_by")
    private String chBusinessUnitLastUpdatedBy;


    @Column(name = "ch_business_unit_last_updated_datetime")
    private java.sql.Timestamp chBusinessUnitLastUpdatedDatetime;


    @Column(name = "customer_id")
    private String customerId;


    @Column(name = "customer_vendor_affiliate_indicator")
    private String customerVendorAffiliateIndicator;


    @Column(name = "financial_business_unit_active_indicator")
    private String financialBusinessUnitActiveIndicator;


    @Column(name = "financial_business_unit_effective_date")
    private java.sql.Date financialBusinessUnitEffectiveDate;


    @Column(name = "financial_business_unit_group_id")
    private String financialBusinessUnitGroupId;


    @Column(name = "financial_business_unit_group_name")
    private String financialBusinessUnitGroupName;


    @Column(name = "financial_business_unit_name")
    private String financialBusinessUnitName;


    @Column(name = "firm_type")
    private String firmType;


    @Column(name = "iso_currency_code")
    private String isoCurrencyCode;


    @Column(name = "lcd_ref_no")
    private String lcdRefNo;


    @Column(name = "legal_entity_short_description")
    private String legalEntityShortDescription;


    @Column(name = "legal_entity_active_indicator")
    private String legalEntityActiveIndicator;


    @Column(name = "legal_entity_code")
    private String legalEntityCode;


    @Column(name = "legal_entity_description")
    private String legalEntityDescription;


    @Column(name = "legal_entity_effective_date")
    private java.sql.Date legalEntityEffectiveDate;


    @Column(name = "legal_entity_german_description")
    private String legalEntityGermanDescription;


    @Column(name = "legal_entity_last_updated_by")
    private String legalEntityLastUpdatedBy;


    @Column(name = "legal_entity_last_updated_datetime")
    private java.sql.Timestamp legalEntityLastUpdatedDatetime;


    @Column(name = "local_gaap_currency")
    private String localGaapCurrency;


    @Column(name = "mis_member_code")
    private String misMemberCode;


    @Column(name = "processing_location_code")
    private String processingLocationCode;


    @Column(name = "source_id")
    private String sourceId;


    @Column(name = "delta_flag")
    private String deltaFlag;


    @Column(name = "bu_domicile")
    private String buDomicile;


}
