package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PorfolioRelationship")
@Entity
public class PorfolioRelationship implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "porfoliorelationship_seq")
    @SequenceGenerator(name = "porfoliorelationship_seq", sequenceName = "porfoliorelationship_seq")
    @Column(name = "portfolio_id")
    private String portfolioId;


    @Column(name = "global_party_id")
    private String globalPartyId;


    @Column(name = "partytype_description")
    private String partytypeDescription;


    @Column(name = "party_type_code")
    private String partyTypeCode;


    @Column(name = "party_legal_name")
    private String partyLegalName;


    @Column(name = "party_alias_name")
    private String partyAliasName;


    @Column(name = "legal_role_definition_id")
    private String legalRoleDefinitionId;


    @Column(name = "role_type")
    private String roleType;


    @Column(name = "role_description")
    private String roleDescription;


    @Column(name = "role_code")
    private String roleCode;


    @Column(name = "party_legal_role_id")
    private String partyLegalRoleId;


    @Column(name = "source_id")
    private String sourceId;


    @Column(name = "source_name")
    private String sourceName;


    @Column(name = "internal_party_alt_id")
    private String internalPartyAltId;


    @Column(name = "portfolio_name")
    private String portfolioName;


    @Column(name = "portfolio_number")
    private String portfolioNumber;


    @Column(name = "legal_entity_name")
    private String legalEntityName;


    @Column(name = "cms_booking_id")
    private String cmsBookingId;


    @Column(name = "entity_jurisdiction")
    private String entityJurisdiction;


    @Column(name = "legal_entity_code")
    private String legalEntityCode;


    @Column(name = "parent_trxn_covrg")
    private String parentTrxnCovrg;


    @Column(name = "trxn_coverage")
    private String trxnCoverage;


    @Column(name = "trxn_coverage_descr")
    private String trxnCoverageDescr;


    @Column(name = "product_id")
    private String productId;


    @Column(name = "account_number")
    private String accountNumber;


    @Column(name = "system_id")
    private String systemId;


    @Column(name = "system_name")
    private String systemName;


    @Column(name = "mnemonic_value")
    private String mnemonicValue;


    @Column(name = "mnemonic_code")
    private String mnemonicCode;


}
