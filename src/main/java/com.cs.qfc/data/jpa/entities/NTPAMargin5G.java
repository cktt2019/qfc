package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "NTPAMargin5G")
@Entity
public class NTPAMargin5G implements Serializable {

    @Column(name = "business_date")
    private java.sql.Date businessDate;


    @Column(name = "run_date")
    private String runDate;


    @Column(name = "account")
    private String account;


    @Column(name = "curr_id")
    private String currId;


    @Column(name = "liquidating_equity")
    private String liquidatingEquity;


    @Column(name = "margin_equity")
    private String marginEquity;


    @Column(name = "reg_t_requirements")
    private String regTRequirements;


    @Column(name = "maint_requirements")
    private String maintRequirements;


    @Column(name = "nyse_requirements")
    private String nyseRequirements;


    @Column(name = "regt_excess_deficit")
    private String regtExcessDeficit;


    @Column(name = "maint_excess_deficit")
    private String maintExcessDeficit;


    @Column(name = "nyse_excess_deficit")
    private String nyseExcessDeficit;


    @Column(name = "sma")
    private String sma;


    @Column(name = "cash_available")
    private String cashAvailable;


    @Column(name = "total_td_balance")
    private String totalTdBalance;


    @Column(name = "total_td_long_mkt_value")
    private String totalTdLongMktValue;


    @Column(name = "total_td_short_mkt_value")
    private String totalTdShortMktValue;


    @Column(name = "total_option_long_mkt_value")
    private String totalOptionLongMktValue;


    @Column(name = "total_option_short_mkt_value")
    private String totalOptionShortMktValue;


    @Column(name = "total_sd_balance")
    private String totalSdBalance;


    @Column(name = "total_sd_long_mkt_value")
    private String totalSdLongMktValue;


    @Column(name = "total_sd_short_mkt_value")
    private String totalSdShortMktValue;


    @Column(name = "total_assessed_balance")
    private String totalAssessedBalance;


    @Column(name = "total_interest_accrual")
    private String totalInterestAccrual;


    @Column(name = "type")
    private String type;


    @Column(name = "td_balance")
    private String tdBalance;


    @Column(name = "td_market_val_lng")
    private String tdMarketValLng;


    @Column(name = "td_market_val_shrt")
    private String tdMarketValShrt;


    @Column(name = "sd_balance")
    private String sdBalance;


    @Column(name = "sd_market_value")
    private String sdMarketValue;


    @Column(name = "legal_entity_id")
    private String legalEntityId;


    @Column(name = "ntpa_product_id")
    private String ntpaProductId;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ntpamargin5g_seq")
    @SequenceGenerator(name = "ntpamargin5g_seq", sequenceName = "ntpamargin5g_seq")
    @Column(name = "id")
    private Integer id;


}
