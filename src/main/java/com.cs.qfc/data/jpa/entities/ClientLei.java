package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ClientLei")
@Entity
public class ClientLei implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "clientlei_seq")
    @SequenceGenerator(name = "clientlei_seq", sequenceName = "clientlei_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "counter_party_csid")
    private String counterPartyCsid;


    @Column(name = "counter_party_name")
    private String counterPartyName;


    @Column(name = "counter_party_lei")
    private String counterPartyLei;


    @Column(name = "ultimate_parent_csid")
    private String ultimateParentCsid;


    @Column(name = "ultimate_parent_name")
    private String ultimateParentName;


    @Column(name = "ultimate_parent_lei")
    private String ultimateParentLei;


}
