package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "LoanIQ")
@Entity
public class LoanIQ implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "loaniq_seq")
    @SequenceGenerator(name = "loaniq_seq", sequenceName = "loaniq_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "cpty_csid")
    private String cptyCsid;


    @Column(name = "sds_id")
    private String sdsId;


    @Column(name = "portfolio_code")
    private String portfolioCode;


    @Column(name = "agreement_id")
    private String agreementId;


    @Column(name = "trade_date")
    private java.sql.Date tradeDate;


    @Column(name = "maturity_date")
    private String maturityDate;


    @Column(name = "currency")
    private String currency;


    @Column(name = "present_value")
    private Float presentValue;


    @Column(name = "trade_amount")
    private Float tradeAmount;


    @Column(name = "record_type")
    private String recordType;


    @Column(name = "swap_id")
    private String swapId;


}
