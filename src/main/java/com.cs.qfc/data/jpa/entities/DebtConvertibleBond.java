package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DebtConvertibleBond")
@Entity
public class DebtConvertibleBond implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "debtconvertiblebond_seq")
    @SequenceGenerator(name = "debtconvertiblebond_seq", sequenceName = "debtconvertiblebond_seq")
    @Column(name = "sedol_id")
    private String sedolId;


    @Column(name = "security_description_long")
    private String securityDescriptionLong;


    @Column(name = "nxt_pay_date")
    private java.sql.Date nxtPayDate;


    @Column(name = "next_call_date")
    private String nextCallDate;


    @Column(name = "next_put_date")
    private String nextPutDate;


    @Column(name = "accrual_count")
    private String accrualCount;


    @Column(name = "aggte_issue")
    private String aggteIssue;


    @Column(name = "base_cpi")
    private Float baseCpi;


    @Column(name = "clearing_house_der")
    private String clearingHouseDer;


    @Column(name = "bb_ticker")
    private String bbTicker;


    @Column(name = "book_entry")
    private String bookEntry;


    @Column(name = "brady_bus_day_convention")
    private String bradyBusDayConvention;


    @Column(name = "business_calendar")
    private String businessCalendar;


    @Column(name = "calc_type_des")
    private String calcTypeDes;


    @Column(name = "call_discrete")
    private String callDiscrete;


    @Column(name = "call_feature")
    private String callFeature;


    @Column(name = "call_notice")
    private String callNotice;


    @Column(name = "call_date")
    private java.sql.Date callDate;


    @Column(name = "call_price")
    private Float callPrice;


    @Column(name = "cpn_fre")
    private String cpnFre;


    @Column(name = "cpn_freq_days")
    private Float cpnFreqDays;


    @Column(name = "coupon_type")
    private String couponType;


    @Column(name = "coupon_type_reset")
    private String couponTypeReset;


    @Column(name = "cpn_freq_yld")
    private String cpnFreqYld;


    @Column(name = "cs_managed")
    private String csManaged;


    @Column(name = "iss_date")
    private java.sql.Date issDate;


    @Column(name = "debt_status")
    private String debtStatus;


    @Column(name = "def_security")
    private String defSecurity;


    @Column(name = "days_to_settle")
    private Float daysToSettle;


    @Column(name = "fw_entry")
    private String fwEntry;


    @Column(name = "fidessa_zone")
    private String fidessaZone;


    @Column(name = "fidessa_code")
    private String fidessaCode;


    @Column(name = "first_call_date")
    private java.sql.Date firstCallDate;


    @Column(name = "first_coupon")
    private java.sql.Date firstCoupon;


    @Column(name = "fix_flt_day_count")
    private Float fixFltDayCount;


    @Column(name = "float_rst")
    private String floatRst;


    @Column(name = "float_rst_indx")
    private Float floatRstIndx;


    @Column(name = "flt_days_prior")
    private Float fltDaysPrior;


    @Column(name = "gloss_k3")
    private String glossK3;


    @Column(name = "gloss_l9")
    private String glossL9;


    @Column(name = "gloss_m3")
    private String glossM3;


    @Column(name = "id_bb")
    private String idBb;


    @Column(name = "id_bb_unique")
    private String idBbUnique;


    @Column(name = "epic_id")
    private String epicId;


    @Column(name = "cmu_id")
    private String cmuId;


    @Column(name = "isin_id")
    private String isinId;


    @Column(name = "japan_id")
    private String japanId;


    @Column(name = "misc_domestic_id")
    private String miscDomesticId;


    @Column(name = "poets_id")
    private String poetsId;


    @Column(name = "ric_id")
    private String ricId;


    @Column(name = "sedol2_id")
    private String sedol2Id;


    @Column(name = "sing_id")
    private String singId;


    @Column(name = "income_currency")
    private String incomeCurrency;


    @Column(name = "industry_sbgpr_issue")
    private String industrySbgprIssue;


    @Column(name = "int_rate_cap")
    private Float intRateCap;


    @Column(name = "int_rate_fl")
    private String intRateFl;


    @Column(name = "boj_id")
    private String bojId;


    @Column(name = "fisec_id")
    private String fisecId;


    @Column(name = "hugo_id")
    private String hugoId;


    @Column(name = "istar_id")
    private String istarId;


    @Column(name = "nri_id")
    private String nriId;


    @Column(name = "smu_id")
    private String smuId;


    @Column(name = "nrt_issuer_id")
    private String nrtIssuerId;


    @Column(name = "margin_perc")
    private Float marginPerc;


    @Column(name = "maturity_date")
    private java.sql.Date maturityDate;


    @Column(name = "muni_min_tax")
    private String muniMinTax;


    @Column(name = "muni_fed_tax")
    private String muniFedTax;


    @Column(name = "state_code")
    private String stateCode;


    @Column(name = "next_reset")
    private java.sql.Date nextReset;


    @Column(name = "nri_int_pay_class")
    private String nriIntPayClass;


    @Column(name = "nri_code")
    private String nriCode;


    @Column(name = "nri_sec_sub")
    private String nriSecSub;


    @Column(name = "nri_sicc")
    private String nriSicc;


    @Column(name = "offering_typ")
    private String offeringTyp;


    @Column(name = "dated_date")
    private java.sql.Date datedDate;


    @Column(name = "physical")
    private String physical;


    @Column(name = "prev_pay_date")
    private java.sql.Date prevPayDate;


    @Column(name = "prv_rst_date")
    private java.sql.Date prvRstDate;


    @Column(name = "princ_fac")
    private Float princFac;


    @Column(name = "private_placement")
    private String privatePlacement;


    @Column(name = "pro_rata_call")
    private String proRataCall;


    @Column(name = "pub_bal_out")
    private Float pubBalOut;


    @Column(name = "put_disc")
    private String putDisc;


    @Column(name = "put_feature")
    private String putFeature;


    @Column(name = "put_notice")
    private Integer putNotice;


    @Column(name = "ratings_trigger")
    private String ratingsTrigger;


    @Column(name = "refix_freq")
    private String refixFreq;


    @Column(name = "regs")
    private String regs;


    @Column(name = "region_code")
    private String regionCode;


    @Column(name = "reset_idx")
    private String resetIdx;


    @Column(name = "restrict_code")
    private String restrictCode;


    @Column(name = "sec_type")
    private String secType;


    @Column(name = "sink_freq")
    private String sinkFreq;


    @Column(name = "benchmark_spread")
    private Integer benchmarkSpread;


    @Column(name = "ticker_symbol")
    private String tickerSymbol;


    @Column(name = "trading_currency")
    private String tradingCurrency;


    @Column(name = "mn_piece")
    private Float mnPiece;


    @Column(name = "trax_report")
    private String traxReport;


    @Column(name = "ult_mat_date")
    private java.sql.Date ultMatDate;


    @Column(name = "des_notes")
    private String desNotes;


    @Column(name = "ult_prnt_comp_id")
    private String ultPrntCompId;


    @Column(name = "fisec_issuer_id")
    private String fisecIssuerId;


    @Column(name = "ult_prnt_ticker_exchange")
    private String ultPrntTickerExchange;


    @Column(name = "coupon_frequency")
    private String couponFrequency;


    @Column(name = "london_ims_description")
    private String londonImsDescription;


    @Column(name = "con_ratio")
    private Integer conRatio;


    @Column(name = "undly_sec_isin")
    private String undlySecIsin;


    @Column(name = "conv_price")
    private Float convPrice;


    @Column(name = "per_sec_src01")
    private String perSecSrc01;


    @Column(name = "issue_date")
    private java.sql.Date issueDate;


    @Column(name = "sp_issuer_id")
    private String spIssuerId;


    @Column(name = "ntpa_in_position")
    private String ntpaInPosition;


    @Column(name = "is_international_sukuk")
    private String isInternationalSukuk;


    @Column(name = "is_domestic_sukuk")
    private String isDomesticSukuk;


    @Column(name = "synthetic_id")
    private String syntheticId;


    @Column(name = "pool_cusip_id")
    private String poolCusipId;


    @Column(name = "auct_rate_pfd_ind")
    private String auctRatePfdInd;


    @Column(name = "repoclear_flg")
    private String repoclearFlg;


    @Column(name = "muni_letter_credit_issuer")
    private String muniLetterCreditIssuer;


    @Column(name = "muni_corp_obligator_2")
    private String muniCorpObligator2;


    @Column(name = "insurance_status")
    private String insuranceStatus;


    @Column(name = "obligator")
    private String obligator;


    @Column(name = "cpn")
    private Float cpn;


    @Column(name = "trade_cntry")
    private String tradeCntry;


    @Column(name = "common_id")
    private String commonId;


    @Column(name = "series")
    private String series;


    @Column(name = "class_code")
    private String classCode;


}
