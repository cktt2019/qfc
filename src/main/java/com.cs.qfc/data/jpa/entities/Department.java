package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Department")
@Entity
public class Department implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "department_seq")
    @SequenceGenerator(name = "department_seq", sequenceName = "department_seq")
    @Column(name = "department_id")
    private String departmentId;


    @Column(name = "business_controller_id")
    private String businessControllerId;


    @Column(name = "controller_id")
    private String controllerId;


    @Column(name = "department_category_code")
    private String departmentCategoryCode;


    @Column(name = "department_category_name")
    private String departmentCategoryName;


    @Column(name = "department_dormant_indicator")
    private String departmentDormantIndicator;


    @Column(name = "department_long_name")
    private String departmentLongName;


    @Column(name = "department_name")
    private String departmentName;


    @Column(name = "department_reconciler_group_name")
    private String departmentReconcilerGroupName;


    @Column(name = "department_status")
    private String departmentStatus;


    @Column(name = "department_trader_id")
    private String departmentTraderId;


    @Column(name = "department_valid_from_date")
    private java.sql.Date departmentValidFromDate;


    @Column(name = "department_valid_to_date")
    private java.sql.Date departmentValidToDate;


    @Column(name = "financial_business_unit_code")
    private String financialBusinessUnitCode;


    @Column(name = "ivr_group")
    private String ivrGroup;


    @Column(name = "l1_supervisor_id")
    private String l1SupervisorId;


    @Column(name = "mis_office_code")
    private String misOfficeCode;


    @Column(name = "mis_office_name")
    private String misOfficeName;


    @Column(name = "owner_l2_id")
    private String ownerL2Id;


    @Column(name = "rec_and_adj_controller_id")
    private String recAndAdjControllerId;


    @Column(name = "rec_and_adj_supervisor_id")
    private String recAndAdjSupervisorId;


    @Column(name = "reconciler_id")
    private String reconcilerId;


    @Column(name = "region_code")
    private String regionCode;


    @Column(name = "region_description")
    private String regionDescription;


    @Column(name = "revenue_type")
    private String revenueType;


    @Column(name = "revenue_type_description")
    private String revenueTypeDescription;


    @Column(name = "risk_portfolio_id")
    private String riskPortfolioId;


    @Column(name = "specialisation_indicator")
    private String specialisationIndicator;


    @Column(name = "tax_category")
    private String taxCategory;


    @Column(name = "volker_rule_description")
    private String volkerRuleDescription;


    @Column(name = "volker_rule_indicator")
    private String volkerRuleIndicator;


    @Column(name = "fx_centrally_maanged_name")
    private String fxCentrallyMaangedName;


    @Column(name = "volcker_sub_division")
    private String volckerSubDivision;


    @Column(name = "volcker_sub_division_description")
    private String volckerSubDivisionDescription;


    @Column(name = "ihc_hcd_flag")
    private String ihcHcdFlag;


    @Column(name = "covered_dept_flag")
    private String coveredDeptFlag;


    @Column(name = "delta_flag")
    private String deltaFlag;


    @Column(name = "remittance_policy_code")
    private String remittancePolicyCode;


    @Column(name = "first_instance_dept_activated")
    private String firstInstanceDeptActivated;


    @Column(name = "first_instance_dept_inactivated")
    private String firstInstanceDeptInactivated;


    @Column(name = "last_instance_dept_activated")
    private String lastInstanceDeptActivated;


    @Column(name = "last_instance_dept_inactivated")
    private String lastInstanceDeptInactivated;


    @Column(name = "rbpnl_flag")
    private String rbpnlFlag;


    @Column(name = "ivr_group_long_name")
    private String ivrGroupLongName;


    @Column(name = "frtb_dept_category")
    private String frtbDeptCategory;


}
