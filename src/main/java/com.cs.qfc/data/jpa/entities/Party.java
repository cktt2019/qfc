package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Party")
@Entity
public class Party implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "party_seq")
    @SequenceGenerator(name = "party_seq", sequenceName = "party_seq")
    @Column(name = "global_party_id")
    private Long globalPartyId;


    @Column(name = "csid")
    private String csid;


    @Column(name = "csid_source")
    private String csidSource;


    @Column(name = "gsid")
    private String gsid;


    @Column(name = "gsid_source")
    private String gsidSource;


    @Column(name = "country_of_domicile_iso_country_code")
    private String countryOfDomicileIsoCountryCode;


    @Column(name = "country_of_incorporation_iso_country_code")
    private String countryOfIncorporationIsoCountryCode;


    @Column(name = "party_legal_name")
    private String partyLegalName;


    @Column(name = "address_type_code")
    private String addressTypeCode;


    @Column(name = "address_address_line1")
    private String addressAddressLine1;


    @Column(name = "address_address_line2")
    private String addressAddressLine2;


    @Column(name = "address_address_line3")
    private String addressAddressLine3;


    @Column(name = "address_address_line4")
    private String addressAddressLine4;


    @Column(name = "address_postal_code")
    private String addressPostalCode;


    @Column(name = "address_country_iso_country_name")
    private String addressCountryIsoCountryName;


    @Column(name = "address_address_full_phone_number")
    private String addressAddressFullPhoneNumber;


    @Column(name = "address_elctronic_address_id")
    private String addressElctronicAddressId;


    @Column(name = "party_legal_role_definition_id")
    private String partyLegalRoleDefinitionId;


    @Column(name = "party_legal_role_id")
    private String partyLegalRoleId;


    @Column(name = "internal_party_id_alt_source_name")
    private String internalPartyIdAltSourceName;


    @Column(name = "party_legal_role_description")
    private String partyLegalRoleDescription;


    @Column(name = "party_legal_status_type")
    private String partyLegalStatusType;


    @Column(name = "party_legal_status_code")
    private String partyLegalStatusCode;


    @Column(name = "address_type_description")
    private String addressTypeDescription;


    @Column(name = "address_city_name")
    private String addressCityName;


    @Column(name = "address_po_box_city")
    private String addressPoBoxCity;


    @Column(name = "external_party_alt_id_source_id")
    private String externalPartyAltIdSourceId;


    @Column(name = "external_party_alt_id_source_name")
    private String externalPartyAltIdSourceName;


    @Column(name = "external_party_alt_id_source_identification_number")
    private String externalPartyAltIdSourceIdentificationNumber;


}
