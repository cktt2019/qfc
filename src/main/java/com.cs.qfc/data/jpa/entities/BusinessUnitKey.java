package com.cs.qfc.data.jpa.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BusinessUnitKey implements Serializable {

    private String financialBusinessUnitCode;

    private String businessUnitId;

}
