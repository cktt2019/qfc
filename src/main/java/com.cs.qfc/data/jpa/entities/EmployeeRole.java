package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EmployeeRole")
@Entity
public class EmployeeRole implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employeerole_seq")
    @SequenceGenerator(name = "employeerole_seq", sequenceName = "employeerole_seq")
    @Column(name = "employee_id")
    private String employeeId;


    @Column(name = "employee_active_indicator")
    private String employeeActiveIndicator;


    @Column(name = "employee_effective_date")
    private java.sql.Date employeeEffectiveDate;


    @Column(name = "employee_email_id")
    private String employeeEmailId;


    @Column(name = "employee_first_name")
    private String employeeFirstName;


    @Column(name = "employee_last_name")
    private String employeeLastName;


    @Column(name = "employee_last_updated_by")
    private String employeeLastUpdatedBy;


    @Column(name = "employee_last_updated_datetime")
    private String employeeLastUpdatedDatetime;


    @Column(name = "employee_logon_domain")
    private String employeeLogonDomain;


    @Column(name = "employee_logon_id")
    private String employeeLogonId;


    @Column(name = "employee_pid_id")
    private String employeePidId;


    @Column(name = "city")
    private String city;


    @Column(name = "region")
    private String region;


    @Column(name = "country_long_desc")
    private String countryLongDesc;


    @Column(name = "delta_flag")
    private String deltaFlag;


}
