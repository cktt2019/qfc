package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Portfolio")
@Entity
public class Portfolio implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "portfolio_seq")
    @SequenceGenerator(name = "portfolio_seq", sequenceName = "portfolio_seq")
    @Column(name = "portfolio_id")
    private String portfolioId;


    @Column(name = "global_party_id")
    private String globalPartyId;


    @Column(name = "partytype_description")
    private String partytypeDescription;


    @Column(name = "legal_name")
    private String legalName;


    @Column(name = "party_alias_name")
    private String partyAliasName;


    @Column(name = "legal_role_definition_id")
    private String legalRoleDefinitionId;


    @Column(name = "lrd_role_type")
    private String lrdRoleType;


    @Column(name = "lrd_role_description")
    private String lrdRoleDescription;


    @Column(name = "lrd_role_code")
    private String lrdRoleCode;


    @Column(name = "party_legal_role_id")
    private String partyLegalRoleId;


    @Column(name = "source_id")
    private String sourceId;


    @Column(name = "source_name")
    private String sourceName;


    @Column(name = "internal_party_alt_id")
    private String internalPartyAltId;


}
