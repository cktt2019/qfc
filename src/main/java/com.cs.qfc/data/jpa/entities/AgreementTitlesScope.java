package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "AgreementTitlesScope")
@Entity
public class AgreementTitlesScope implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "agreementtitlesscope_seq")
    @SequenceGenerator(name = "agreementtitlesscope_seq", sequenceName = "agreementtitlesscope_seq")
    @Column(name = "count")
    private Integer count;


    @Column(name = "agreement_title")
    private String agreementTitle;


    @Column(name = "agreement_type")
    private String agreementType;


    @Column(name = "in_scope")
    private Boolean inScope;


}
