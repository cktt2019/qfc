package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PrimeSwapPosition")
@Entity
public class PrimeSwapPosition implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "primeswapposition_seq")
    @SequenceGenerator(name = "primeswapposition_seq", sequenceName = "primeswapposition_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "business_date")
    private java.sql.Date businessDate;


    @Column(name = "swap_id")
    private String swapId;


    @Column(name = "swap_version")
    private String swapVersion;


    @Column(name = "position_id")
    private String positionId;


    @Column(name = "position_version")
    private String positionVersion;


    @Column(name = "swap_type")
    private String swapType;


    @Column(name = "counterparty_code")
    private String counterpartyCode;


    @Column(name = "counterparty_name")
    private String counterpartyName;


    @Column(name = "settled_ltd_cost_base")
    private String settledLtdCostBase;


    @Column(name = "swap_ccy_to_usd_fx")
    private String swapCcyToUsdFx;


}
