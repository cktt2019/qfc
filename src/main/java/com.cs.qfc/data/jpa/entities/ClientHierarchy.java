package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ClientHierarchy")
@Entity
@IdClass(ClientHierarchyKey.class)
public class ClientHierarchy implements Serializable {

    @Id
    @Column(name = "csid ")
    private String csid;


    @Column(name = " gsid ")
    private String gsid;


    @Column(name = " counter_party_name ")
    private String counterPartyName;


    @Column(name = " immediate_parent_csid ")
    private String immediateParentCsid;


    @Column(name = " immediate_parent_gsid ")
    private String immediateParentGsid;


    @Column(name = " ultimate_parent_csid ")
    private String ultimateParentCsid;


    @Column(name = " ultimate_parent_gsid ")
    private String ultimateParentGsid;


    @Id
    @Column(name = " level ")
    private String level;


}
