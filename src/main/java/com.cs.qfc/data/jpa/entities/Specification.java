package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Specification")
@Entity
public class Specification implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "specification_seq")
    @SequenceGenerator(name = "specification_seq", sequenceName = "specification_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "specification_name")
    private String specificationName;


    @Column(name = "specificationDescription")
    private String specificationDescription;


    @Column(name = "specificationVersion")
    private String specificationVersion;

    @Lob
    @Column(name = "specificationContent")
    private String specificationContent;


}
