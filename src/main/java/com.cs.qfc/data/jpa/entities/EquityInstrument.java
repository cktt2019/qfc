package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EquityInstrument")
@Entity
public class EquityInstrument implements Serializable {

    @Id
    @Column(name = "gpi_issue")
    private String gpiIssue;


    @Column(name = "isin_id")
    private String isinId;


    @Column(name = "sedol_id")
    private String sedolId;


    @Column(name = "cusip_id")
    private String cusipId;


    @Column(name = "security_description_long")
    private String securityDescriptionLong;


}
