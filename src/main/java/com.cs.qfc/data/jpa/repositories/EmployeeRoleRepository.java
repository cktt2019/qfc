package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.EmployeeRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link EmployeeRole} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface EmployeeRoleRepository extends JpaRepository<EmployeeRole, String> {


    public List<EmployeeRole> findByEmployeeActiveIndicator(String employeeActiveIndicator);

    public List<EmployeeRole> findByEmployeeEffectiveDate(java.sql.Date employeeEffectiveDate);

    public List<EmployeeRole> findByEmployeeEmailId(String employeeEmailId);

    public List<EmployeeRole> findByEmployeeFirstName(String employeeFirstName);

    public List<EmployeeRole> findByEmployeeLastName(String employeeLastName);

    public List<EmployeeRole> findByEmployeeLastUpdatedBy(String employeeLastUpdatedBy);

    public List<EmployeeRole> findByEmployeeLastUpdatedDatetime(String employeeLastUpdatedDatetime);

    public List<EmployeeRole> findByEmployeeLogonDomain(String employeeLogonDomain);

    public List<EmployeeRole> findByEmployeeLogonId(String employeeLogonId);

    public List<EmployeeRole> findByEmployeePidId(String employeePidId);

    public List<EmployeeRole> findByCity(String city);

    public List<EmployeeRole> findByRegion(String region);

    public List<EmployeeRole> findByCountryLongDesc(String countryLongDesc);

    public List<EmployeeRole> findByDeltaFlag(String deltaFlag);


    public Page<EmployeeRole> findAll(Pageable page);


}







