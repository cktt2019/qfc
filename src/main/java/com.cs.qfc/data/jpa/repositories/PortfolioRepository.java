package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.Portfolio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link Portfolio} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface PortfolioRepository extends JpaRepository<Portfolio, String> {


    public List<Portfolio> findByGlobalPartyId(String globalPartyId);

    public List<Portfolio> findByPartytypeDescription(String partytypeDescription);

    public List<Portfolio> findByLegalName(String legalName);

    public List<Portfolio> findByPartyAliasName(String partyAliasName);

    public List<Portfolio> findByLegalRoleDefinitionId(String legalRoleDefinitionId);

    public List<Portfolio> findByLrdRoleType(String lrdRoleType);

    public List<Portfolio> findByLrdRoleDescription(String lrdRoleDescription);

    public List<Portfolio> findByLrdRoleCode(String lrdRoleCode);

    public List<Portfolio> findByPartyLegalRoleId(String partyLegalRoleId);

    public List<Portfolio> findBySourceId(String sourceId);

    public List<Portfolio> findBySourceName(String sourceName);

    public List<Portfolio> findByInternalPartyAltId(String internalPartyAltId);


    public Page<Portfolio> findAll(Pageable page);


}







