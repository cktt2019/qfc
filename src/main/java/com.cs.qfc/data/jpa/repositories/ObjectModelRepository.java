package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.ObjectModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link ObjectModel} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface ObjectModelRepository extends JpaRepository<ObjectModel, Integer> {


    public List<ObjectModel> findByObjectName(String specificationName);

    public List<ObjectModel> findByObjectDescription(String specificationDescription);

    public List<ObjectModel> findByObjectVersion(String specificationVersion);

    public List<ObjectModel> findByObjectContent(java.sql.Clob specificationContent);


    public Page<ObjectModel> findAll(Pageable page);


}







