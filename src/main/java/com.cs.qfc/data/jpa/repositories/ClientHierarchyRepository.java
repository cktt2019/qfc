package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.ClientHierarchy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link ClientHierarchy} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface ClientHierarchyRepository extends JpaRepository<ClientHierarchy, Integer> {


    public List<ClientHierarchy> findByCsid(String csid);

    public List<ClientHierarchy> findByGsid(String gsid);

    public List<ClientHierarchy> findByCounterPartyName(String counterPartyName);

    public List<ClientHierarchy> findByImmediateParentCsid(String immediateParentCsid);

    public List<ClientHierarchy> findByImmediateParentGsid(String immediateParentGsid);

    public List<ClientHierarchy> findByUltimateParentCsid(String ultimateParentCsid);

    public List<ClientHierarchy> findByUltimateParentGsid(String ultimateParentGsid);


    public Page<ClientHierarchy> findAll(Pageable page);


}







