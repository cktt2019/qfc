package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.Specification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link Specification} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface SpecificationRepository extends JpaRepository<Specification, Integer> {


    public List<Specification> findBySpecificationName(String specificationName);

    public List<Specification> findBySpecificationDescription(String specificationDescription);

    public List<Specification> findBySpecificationVersion(String specificationVersion);

    public List<Specification> findBySpecificationContent(java.sql.Clob specificationContent);


    public Page<Specification> findAll(Pageable page);


}







