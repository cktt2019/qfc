package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.PorfolioRelationship;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link PorfolioRelationship} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface PorfolioRelationshipRepository extends JpaRepository<PorfolioRelationship, String> {


    public List<PorfolioRelationship> findByGlobalPartyId(String globalPartyId);

    public List<PorfolioRelationship> findByPartytypeDescription(String partytypeDescription);

    public List<PorfolioRelationship> findByPartyTypeCode(String partyTypeCode);

    public List<PorfolioRelationship> findByPartyLegalName(String partyLegalName);

    public List<PorfolioRelationship> findByPartyAliasName(String partyAliasName);

    public List<PorfolioRelationship> findByLegalRoleDefinitionId(String legalRoleDefinitionId);

    public List<PorfolioRelationship> findByRoleType(String roleType);

    public List<PorfolioRelationship> findByRoleDescription(String roleDescription);

    public List<PorfolioRelationship> findByRoleCode(String roleCode);

    public List<PorfolioRelationship> findByPartyLegalRoleId(String partyLegalRoleId);

    public List<PorfolioRelationship> findBySourceId(String sourceId);

    public List<PorfolioRelationship> findBySourceName(String sourceName);

    public List<PorfolioRelationship> findByInternalPartyAltId(String internalPartyAltId);

    public List<PorfolioRelationship> findByPortfolioName(String portfolioName);

    public List<PorfolioRelationship> findByPortfolioNumber(String portfolioNumber);

    public List<PorfolioRelationship> findByLegalEntityName(String legalEntityName);

    public List<PorfolioRelationship> findByCmsBookingId(String cmsBookingId);

    public List<PorfolioRelationship> findByEntityJurisdiction(String entityJurisdiction);

    public List<PorfolioRelationship> findByLegalEntityCode(String legalEntityCode);

    public List<PorfolioRelationship> findByParentTrxnCovrg(String parentTrxnCovrg);

    public List<PorfolioRelationship> findByTrxnCoverage(String trxnCoverage);

    public List<PorfolioRelationship> findByTrxnCoverageDescr(String trxnCoverageDescr);

    public List<PorfolioRelationship> findByProductId(String productId);

    public List<PorfolioRelationship> findByAccountNumber(String accountNumber);

    public List<PorfolioRelationship> findBySystemId(String systemId);

    public List<PorfolioRelationship> findBySystemName(String systemName);

    public List<PorfolioRelationship> findByMnemonicValue(String mnemonicValue);

    public List<PorfolioRelationship> findByMnemonicCode(String mnemonicCode);


    public Page<PorfolioRelationship> findAll(Pageable page);


}







