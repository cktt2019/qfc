package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.DebtInstrument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Service interface with operations for {@link DebtInstrument} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface DebtInstrumentRepository extends JpaRepository<DebtInstrument, String> {


    public List<DebtInstrument> findByIsinId(String isinId);

    public List<DebtInstrument> findBySedolId(String sedolId);

    public List<DebtInstrument> findBySecurityDescriptionLong(String securityDescriptionLong);

    public List<DebtInstrument> findByNxtPayDate(java.sql.Date nxtPayDate);

    public List<DebtInstrument> findByNextCallDate(java.sql.Date nextCallDate);

    public List<DebtInstrument> findByNextPutDate(java.sql.Date nextPutDate);


    public Page<DebtInstrument> findAll(Pageable page);


}
