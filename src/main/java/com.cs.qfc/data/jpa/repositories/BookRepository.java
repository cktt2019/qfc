package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link Book} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface BookRepository extends JpaRepository<Book, String> {


    public List<Book> findByAccountMethod(String accountMethod);

    public List<Book> findByAccountingBasisIndicator(String accountingBasisIndicator);

    public List<Book> findByBookCategoryCode(String bookCategoryCode);

    public List<Book> findByBookCategoryDescription(String bookCategoryDescription);

    public List<Book> findByBookControllerId(String bookControllerId);

    public List<Book> findByBookDormantIndicator(String bookDormantIndicator);

    public List<Book> findByBookName(String bookName);

    public List<Book> findByBookPnlSystemName(String bookPnlSystemName);

    public List<Book> findByBookRef(String bookRef);

    public List<Book> findByBookStatus(String bookStatus);

    public List<Book> findByBookSubCategoryCode(String bookSubCategoryCode);

    public List<Book> findByBookSubCategoryDescription(String bookSubCategoryDescription);

    public List<Book> findByBookTraderId(String bookTraderId);

    public List<Book> findByCnyDeliverableOutsideChinaIndicator(String cnyDeliverableOutsideChinaIndicator);

    public List<Book> findByDepartmentId(String departmentId);

    public List<Book> findByFrontOfficeRepresentativeId(String frontOfficeRepresentativeId);

    public List<Book> findByGplAtomCode(String gplAtomCode);

    public List<Book> findByNhfsIndicator(String nhfsIndicator);

    public List<Book> findByRemoteBookingPolicyIndicator(String remoteBookingPolicyIndicator);

    public List<Book> findBySplitHedgeComments(String splitHedgeComments);

    public List<Book> findBySplitHedgeIndicator(String splitHedgeIndicator);

    public List<Book> findByUsTaxCategoryCode(String usTaxCategoryCode);

    public List<Book> findByBookLastUpdatedDatetime(java.sql.Timestamp bookLastUpdatedDatetime);

    public List<Book> findByFinancialBusinessUnitCode(String financialBusinessUnitCode);

    public List<Book> findByDeltaFlag(String deltaFlag);

    public List<Book> findByBookRequestStatusCode(String bookRequestStatusCode);

    public List<Book> findByFirstInstanceBookActivated(java.sql.Date firstInstanceBookActivated);

    public List<Book> findByLastInstanceBookActivated(java.sql.Date lastInstanceBookActivated);

    public List<Book> findByFirstInstanceBookInactivated(java.sql.Date firstInstanceBookInactivated);

    public List<Book> findByLastInstanceBook(java.sql.Date lastInstanceBook);

    public List<Book> findByEmailAddress(String emailAddress);

    public List<Book> findByCavFlag(String cavFlag);

    public List<Book> findByFosFlag(String fosFlag);

    public List<Book> findByBookingIntent(String bookingIntent);

    public List<Book> findByBookRequestId(String bookRequestId);

    public List<Book> findByIparEmplIdEmailId(String iparEmplIdEmailId);

    public List<Book> findByBookTraderEmailId(String bookTraderEmailId);

    public List<Book> findByPcSupervisorEmailId(String pcSupervisorEmailId);

    public List<Book> findByBookEmpIdEmailId(String bookEmpIdEmailId);

    public List<Book> findByPctnlEmplIdEmailId(String pctnlEmplIdEmailId);


    public Page<Book> findAll(Pageable page);


}







