package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.Vision;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link Vision} persistence.
 */
@Repository
public interface VisionRepository extends JpaRepository<Vision, Integer> {


    public List<Vision> findByBusinessDate(java.sql.Date businessDate);

    public List<Vision> findByEntity(String entity);

    public List<Vision> findByCpty(String cpty);

    public List<Vision> findByBook(String book);

    public List<Vision> findByProveNameProduct(String proveNameProduct);

    public List<Vision> findByAgreementId1(String agreementId1);

    public List<Vision> findByTradeDate(java.sql.Date tradeDate);

    public List<Vision> findByEndDate(java.sql.Date endDate);

    public List<Vision> findByBondNextCoup(String bondNextCoup);

    public List<Vision> findByCashCurr(String cashCurr);

    public List<Vision> findByPvUsd(String pvUsd);

    public List<Vision> findByDefaultFvLevel(String defaultFvLevel);

    public List<Vision> findByFace(String face);

    public List<Vision> findByValueOn(String valueOn);

    public List<Vision> findByRehypothecationIndicator(String rehypothecationIndicator);

    public List<Vision> findByCustodianId(String custodianId);


    public Page<Vision> findAll(Pageable page);


}







