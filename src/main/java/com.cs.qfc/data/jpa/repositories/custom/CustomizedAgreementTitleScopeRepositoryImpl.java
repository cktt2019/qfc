package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.AgreementTitleScope;
import com.cs.qfc.data.metadata.AgreementTitleScopeMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.AgreementTitleScopeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link AgreementTitleScope} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedAgreementTitleScopeRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedAgreementTitleScopeRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<AgreementTitleScopeDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + AgreementTitleScopeMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new AgreementTitleScopeMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new AgreementTitleScopeMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new AgreementTitleScopeMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<AgreementTitleScopeDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new AgreementTitleScopeRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     AgreementTitleScopeMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class AgreementTitleScopeRowMapper implements RowMapper<AgreementTitleScopeDTO> {
        @Override
        public AgreementTitleScopeDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            AgreementTitleScopeDTO ret = new AgreementTitleScopeDTO();


            ret.setCount(rs.getInt("count"));
            ret.setAgreementTitle(rs.getString("agreement_title"));
            ret.setAgreementType(rs.getString("agreement_type"));
            ret.setInScope(rs.getInt("in_scope"));

            return ret;
        }
    }

}
