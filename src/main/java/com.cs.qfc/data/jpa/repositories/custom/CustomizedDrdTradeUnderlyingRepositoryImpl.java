package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.DrdTradeUnderlying;
import com.cs.qfc.data.metadata.DrdTradeUnderlyingMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.DrdTradeUnderlyingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link DrdTradeUnderlying} .
 */

@Component
public class CustomizedDrdTradeUnderlyingRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedDrdTradeUnderlyingRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<DrdTradeUnderlyingDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + DrdTradeUnderlyingMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new DrdTradeUnderlyingMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new DrdTradeUnderlyingMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new DrdTradeUnderlyingMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<DrdTradeUnderlyingDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new DrdTradeUnderlyingRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     DrdTradeUnderlyingMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class DrdTradeUnderlyingRowMapper implements RowMapper<DrdTradeUnderlyingDTO> {
        @Override
        public DrdTradeUnderlyingDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            DrdTradeUnderlyingDTO ret = new DrdTradeUnderlyingDTO();


            ret.setId(rs.getInt("id"));
            ret.setCobDate(rs.getDate("cob_date"));
            ret.setLoadCobDate(rs.getDate("load_cob_date"));
            ret.setLoadBook(rs.getString("load_book"));
            ret.setBookName(rs.getString("book_name"));
            ret.setTradeLoadId(rs.getInt("trade_load_id"));
            ret.setLoadTimestamp(rs.getDate("load_timestamp"));
            ret.setSnapshotName(rs.getString("snapshot_name"));
            ret.setSnapshotId(rs.getInt("snapshot_id"));
            ret.setSnapshotCreationDatetime(rs.getDate("snapshot_creation_datetime"));
            ret.setInterfaceVersion(rs.getString("interface_version"));
            ret.setTradeId(rs.getInt("trade_id"));
            ret.setTftTradeVersion(rs.getInt("tft_trade_version"));
            ret.setTradeLocationId(rs.getInt("trade_location_id"));
            ret.setPrimaryTcn(rs.getString("primary_tcn"));
            ret.setTcn(rs.getString("tcn"));
            ret.setTradeVersionUpdateTime(rs.getDate("trade_version_update_time"));
            ret.setCancellationDate(rs.getString("cancellation_date"));
            ret.setGbmEntity(rs.getString("gbm_entity"));
            ret.setApprovalStatus(rs.getString("approval_status"));
            ret.setEventReason(rs.getString("event_reason"));
            ret.setExternalId(rs.getString("external_id"));
            ret.setPrimaryMaturityDate(rs.getDate("primary_maturity_date"));
            ret.setProductSubType(rs.getString("product_sub_type"));
            ret.setProductType(rs.getString("product_type"));
            ret.setProductValuationType(rs.getString("product_valuation_type"));
            ret.setTradeStatus(rs.getString("trade_status"));
            ret.setUnderlyingId(rs.getString("underlying_id"));
            ret.setUnderlyingIdType(rs.getString("underlying_id_type"));
            ret.setAccountCcy(rs.getString("account_ccy"));
            ret.setBasketStrike(rs.getFloat("basket_strike"));
            ret.setBasketWeighting(rs.getString("basket_weighting"));
            ret.setCoupon(rs.getFloat("coupon"));
            ret.setCouponFreq(rs.getString("coupon_freq"));
            ret.setFinalPrice(rs.getFloat("final_price"));
            ret.setFloatingRateIndex(rs.getString("floating_rate_index"));
            ret.setFloatingRateIndexTenor(rs.getString("floating_rate_index_tenor"));
            ret.setFloatingRateIndexType(rs.getString("floating_rate_index_type"));
            ret.setInitialPrice(rs.getFloat("initial_price"));
            ret.setRefObligCategory(rs.getString("ref_oblig_category"));
            ret.setRefPoolLongName(rs.getString("ref_pool_long_name"));
            ret.setRefPoolShortName(rs.getString("ref_pool_short_name"));
            ret.setSequenceId(rs.getInt("sequence_id"));
            ret.setUnderlyingAssetClass(rs.getString("underlying_asset_class"));
            ret.setUnderlyingCcy(rs.getString("underlying_ccy"));
            ret.setUnderlyingDesc(rs.getString("underlying_desc"));
            ret.setUnderlyingExtId(rs.getString("underlying_ext_id"));
            ret.setUnderlyingExtIdType(rs.getString("underlying_ext_id_type"));
            ret.setUnderlyingMatDate(rs.getDate("underlying_mat_date"));
            ret.setUnderlyingRiskId(rs.getString("underlying_risk_id"));
            ret.setPayRec(rs.getString("pay_rec"));
            ret.setSettlementType(rs.getString("settlement_type"));
            ret.setBusinessGroup(rs.getString("business_group"));
            ret.setValuationClass(rs.getString("valuation_class"));
            ret.setValuationType(rs.getString("valuation_type"));
            ret.setTradeSourceSystem(rs.getString("trade_source_system"));
            ret.setUnderlyingPriceSource(rs.getString("underlying_price_source"));
            ret.setCduMasterAgreementLlk(rs.getString("cdu_master_agreement_llk"));
            ret.setCduCollateralAnnexLlk(rs.getString("cdu_collateral_annex_llk"));
            ret.setFramesoftMasterAgreementLlk(rs.getInt("framesoft_master_agreement_llk"));
            ret.setAlgoCollateralAnnexLlk(rs.getInt("algo_collateral_annex_llk"));
            ret.setGbmBookRef(rs.getString("gbm_book_ref"));
            ret.setUnderlyingProductId(rs.getString("underlying_product_id"));
            ret.setUnderlyingProductType(rs.getString("underlying_product_type"));
            ret.setUnderlyingRiskType(rs.getString("underlying_risk_type"));
            ret.setBankruptcy(rs.getString("bankruptcy"));
            ret.setDelivObligAcceleratedMatured(rs.getString("deliv_oblig_accelerated_or_matured"));
            ret.setDelivObligAccruedInterest(rs.getString("deliv_oblig_accrued_interest"));
            ret.setDelivObligAssignableLoan(rs.getString("deliv_oblig_assignable_loan"));
            ret.setDelivObligCategory(rs.getString("deliv_oblig_category"));
            ret.setDelivObligConsentRequiredLoan(rs.getString("deliv_oblig_consent_required_loan"));
            ret.setDelivOblgDirectLoanPrtCptn(rs.getString("deliv_oblg_direct_loan_prt_cptn"));
            ret.setDelivObligListed(rs.getString("deliv_oblig_listed"));
            ret.setDelivObligMaximumMaturity(rs.getInt("deliv_oblig_maximum_maturity"));
            ret.setDelivObligMaxMaturitySpecified(rs.getString("deliv_oblig_max_maturity_specified"));
            ret.setDelivObligNotBearer(rs.getString("deliv_oblig_not_bearer"));
            ret.setDelivObligNotContingent(rs.getString("deliv_oblig_not_contingent"));
            ret.setDelivObligNotDomesticCurrency(rs.getString("deliv_oblig_not_domestic_currency"));
            ret.setDelivObligNotDomesticIssuance(rs.getString("deliv_oblig_not_domestic_issuance"));
            ret.setDelivObligNotDomesticLaw(rs.getString("deliv_oblig_not_domestic_law"));
            ret.setDelivObligNotSovereignLender(rs.getString("deliv_oblig_not_sovereign_lender"));
            ret.setDelivObligPhyParipassu(rs.getString("deliv_oblig_phy_paripassu"));
            ret.setDelivObligSpecifiedCcyList(rs.getString("deliv_oblig_specified_ccy_list"));
            ret.setDelivObligSpecifiedCcyType(rs.getString("deliv_oblig_specified_ccy_type"));
            ret.setDelivObligTransferable(rs.getString("deliv_oblig_transferable"));
            ret.setEntityType(rs.getString("entity_type"));
            ret.setPrimaryObligor(rs.getString("primary_obligor"));
            ret.setRecoveryValue(rs.getFloat("recovery_value"));
            ret.setReferenceEntityRedCode(rs.getString("reference_entity_red_code"));
            ret.setReferenceEntityUniqueEntityId(rs.getString("reference_entity_unique_entity_id"));
            ret.setReferenceObligationBloombergId(rs.getString("reference_obligation_bloomberg_id"));
            ret.setReferenceObligationCusip(rs.getString("reference_obligation_cusip"));
            ret.setReferenceObligationGuarantor(rs.getString("reference_obligation_guarantor"));
            ret.setReferenceObligationIsin(rs.getString("reference_obligation_isin"));
            ret.setReferencePrice(rs.getFloat("reference_price"));
            ret.setRefObligorCouponRate(rs.getString("ref_obligor_coupon_rate"));
            ret.setRefObligorMaturityDate(rs.getDate("ref_obligor_maturity_date"));
            ret.setRefObligorNotionalAmt(rs.getFloat("ref_obligor_notional_amt"));
            ret.setRefObligorNotionalAmtCcy(rs.getString("ref_obligor_notional_amt_ccy"));
            ret.setRefObligorObligationCategory(rs.getString("ref_obligor_obligation_category"));
            ret.setValUnwind(rs.getFloat("val_unwind"));

            return ret;
        }
    }

}
