package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.Book;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.BookDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link Book} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedBookRepository {

    public Collection<BookDTO> findByCustomCriteria(SearchInfoDTO info);

}







