package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.CreditSuisseLegalEntity;
import com.cs.qfc.data.metadata.CreditSuisseLegalEntityMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.CreditSuisseLegalEntityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link CreditSuisseLegalEntity} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedCreditSuisseLegalEntityRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedCreditSuisseLegalEntityRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<CreditSuisseLegalEntityDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + CreditSuisseLegalEntityMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new CreditSuisseLegalEntityMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new CreditSuisseLegalEntityMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new CreditSuisseLegalEntityMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<CreditSuisseLegalEntityDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new CreditSuisseLegalEntityRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     CreditSuisseLegalEntityMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class CreditSuisseLegalEntityRowMapper implements RowMapper<CreditSuisseLegalEntityDTO> {
        @Override
        public CreditSuisseLegalEntityDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            CreditSuisseLegalEntityDTO ret = new CreditSuisseLegalEntityDTO();


            ret.setCount(rs.getInt("Count"));
            ret.setEnterpriseDNumber(rs.getInt("enterprise_d_number"));
            ret.setBookingEntityNm(rs.getString("booking_entity_nm"));
            ret.setPeopleSoftId(rs.getString("people_soft_id"));
            ret.setCsidId(rs.getString("csid_id"));

            return ret;
        }
    }

}
