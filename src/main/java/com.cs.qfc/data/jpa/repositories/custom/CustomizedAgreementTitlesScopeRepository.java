package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.AgreementTitlesScope;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.AgreementTitlesScopeDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link AgreementTitlesScope} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedAgreementTitlesScopeRepository {

    public Collection<AgreementTitlesScopeDTO> findByCustomCriteria(SearchInfoDTO info);

}







