package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.PorfolioRelationship;
import com.cs.qfc.data.metadata.PorfolioRelationshipMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.PorfolioRelationshipDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link PorfolioRelationship} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedPorfolioRelationshipRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedPorfolioRelationshipRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<PorfolioRelationshipDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + PorfolioRelationshipMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new PorfolioRelationshipMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new PorfolioRelationshipMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new PorfolioRelationshipMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<PorfolioRelationshipDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new PorfolioRelationshipRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     PorfolioRelationshipMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class PorfolioRelationshipRowMapper implements RowMapper<PorfolioRelationshipDTO> {
        @Override
        public PorfolioRelationshipDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            PorfolioRelationshipDTO ret = new PorfolioRelationshipDTO();


            ret.setPortfolioId(rs.getString("portfolio_id"));
            ret.setGlobalPartyId(rs.getString("global_party_id"));
            ret.setPartytypeDescription(rs.getString("partytype_description"));
            ret.setPartyTypeCode(rs.getString("party_type_code"));
            ret.setPartyLegalName(rs.getString("party_legal_name"));
            ret.setPartyAliasName(rs.getString("party_alias_name"));
            ret.setLegalRoleDefinitionId(rs.getString("legal_role_definition_id"));
            ret.setRoleType(rs.getString("role_type"));
            ret.setRoleDescription(rs.getString("role_description"));
            ret.setRoleCode(rs.getString("role_code"));
            ret.setPartyLegalRoleId(rs.getString("party_legal_role_id"));
            ret.setSourceId(rs.getString("source_id"));
            ret.setSourceName(rs.getString("source_name"));
            ret.setInternalPartyAltId(rs.getString("internal_party_alt_id"));
            ret.setPortfolioName(rs.getString("portfolio_name"));
            ret.setPortfolioNumber(rs.getString("portfolio_number"));
            ret.setLegalEntityName(rs.getString("legal_entity_name"));
            ret.setCmsBookingId(rs.getString("cms_booking_id"));
            ret.setEntityJurisdiction(rs.getString("entity_jurisdiction"));
            ret.setLegalEntityCode(rs.getString("legal_entity_code"));
            ret.setParentTrxnCovrg(rs.getString("parent_trxn_covrg"));
            ret.setTrxnCoverage(rs.getString("trxn_coverage"));
            ret.setTrxnCoverageDescr(rs.getString("trxn_coverage_descr"));
            ret.setProductId(rs.getString("product_id"));
            ret.setAccountNumber(rs.getString("account_number"));
            ret.setSystemId(rs.getString("system_id"));
            ret.setSystemName(rs.getString("system_name"));
            ret.setMnemonicValue(rs.getString("mnemonic_value"));
            ret.setMnemonicCode(rs.getString("mnemonic_code"));

            return ret;
        }
    }

}
