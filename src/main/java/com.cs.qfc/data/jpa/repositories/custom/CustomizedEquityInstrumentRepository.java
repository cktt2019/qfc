package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.EquityInstrument;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.EquityInstrumentDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link EquityInstrument} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedEquityInstrumentRepository {

    public Collection<EquityInstrumentDTO> findByCustomCriteria(SearchInfoDTO info);

}







