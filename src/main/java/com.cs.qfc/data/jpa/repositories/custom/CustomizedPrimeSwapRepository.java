package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.PrimeSwap;

import java.util.Collection;

/**
 * Service interface with operations for {@link com.cs.qfc.data.jpa.entities.PrimeSwap} persistence.
 */

public interface CustomizedPrimeSwapRepository {

    public Collection<PrimeSwap> findByCustomCriteria(SearchInfoDTO info);

}







