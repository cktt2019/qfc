package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.CreditSuisseCorpEntity;
import com.cs.qfc.data.metadata.CreditSuisseCorpEntityMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.CreditSuisseCorpEntityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link CreditSuisseCorpEntity} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedCreditSuisseCorpEntityRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedCreditSuisseCorpEntityRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<CreditSuisseCorpEntityDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + CreditSuisseCorpEntityMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new CreditSuisseCorpEntityMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new CreditSuisseCorpEntityMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();
        System.out.println("executing query " + sql);
        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new CreditSuisseCorpEntityMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<CreditSuisseCorpEntityDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new CreditSuisseCorpEntityRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     CreditSuisseCorpEntityMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class CreditSuisseCorpEntityRowMapper implements RowMapper<CreditSuisseCorpEntityDTO> {
        @Override
        public CreditSuisseCorpEntityDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            CreditSuisseCorpEntityDTO ret = new CreditSuisseCorpEntityDTO();


            ret.setCount(rs.getInt("count"));
            ret.setLegalNameOfEntity(rs.getString("legal_name_of_entity"));
            ret.setLei(rs.getString("lei"));
            ret.setImmediateParentName(rs.getString("immediate_parent_name"));
            ret.setImmediateParentLei(rs.getString("immediate_parent_lei"));
            ret.setPercentageOwnership(rs.getString("percentage_ownership"));
            ret.setEntityType(rs.getString("entity_type"));
            ret.setDomicile(rs.getString("domicile"));
            ret.setJurisdictionIncorp(rs.getString("jurisdiction_incorp"));

            return ret;
        }
    }

}
