package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.EmployeeRole;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.EmployeeRoleDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link EmployeeRole} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedEmployeeRoleRepository {

    public Collection<EmployeeRoleDTO> findByCustomCriteria(SearchInfoDTO info);

}







