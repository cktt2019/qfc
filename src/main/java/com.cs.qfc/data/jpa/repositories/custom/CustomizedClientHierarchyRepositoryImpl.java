package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.ClientHierarchy;
import com.cs.qfc.data.metadata.ClientHierarchyMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.ClientHierarchyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link ClientHierarchy} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedClientHierarchyRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedClientHierarchyRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<ClientHierarchyDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + ClientHierarchyMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new ClientHierarchyMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new ClientHierarchyMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new ClientHierarchyMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<ClientHierarchyDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new ClientHierarchyRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     ClientHierarchyMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class ClientHierarchyRowMapper implements RowMapper<ClientHierarchyDTO> {
        @Override
        public ClientHierarchyDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            ClientHierarchyDTO ret = new ClientHierarchyDTO();


            ret.setCsid(rs.getString("csid"));
            ret.setGsid(rs.getString("gsid"));
            ret.setCounterPartyName(rs.getString("counter_party_name"));
            ret.setImmediateParentCsid(rs.getString("immediate_parent_csid"));
            ret.setImmediateParentGsid(rs.getString("immediate_parent_gsid"));
            ret.setUltimateParentCsid(rs.getString("ultimate_parent_csid"));
            ret.setUltimateParentGsid(rs.getString("ultimate_parent_gsid"));
            ret.setLevel(rs.getString("level"));

            return ret;
        }
    }

}
