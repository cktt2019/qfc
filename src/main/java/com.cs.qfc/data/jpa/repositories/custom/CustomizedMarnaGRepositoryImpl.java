package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.MarnaG;
import com.cs.qfc.data.metadata.MarnaGMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.MarnaGDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link MarnaG} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedMarnaGRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedMarnaGRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<MarnaGDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + MarnaGMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new MarnaGMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new MarnaGMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new MarnaGMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<MarnaGDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new MarnaGRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     MarnaGMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class MarnaGRowMapper implements RowMapper<MarnaGDTO> {
        @Override
        public MarnaGDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            MarnaGDTO ret = new MarnaGDTO();


            ret.setCnid(rs.getInt("cnid"));
            ret.setAgreementNumber(rs.getString("agreement_number"));
            ret.setStatus(rs.getString("status"));
            ret.setSupersededByCnid(rs.getString("superseded_by_cnid"));
            ret.setSupersededByAgreement(rs.getString("superseded_by_agreement"));
            ret.setLegalAgreementDescription(rs.getString("legal_agreement_description"));
            ret.setCsEntityBookinggroupId(rs.getString("cs_entity_bookinggroup_id"));
            ret.setCsEntityBookinggroupName(rs.getString("cs_entity_bookinggroup_name"));
            ret.setNegotiator(rs.getString("negotiator"));
            ret.setNegotiatorLocation(rs.getString("negotiator_location"));
            ret.setCounterParty(rs.getString("counter_party"));
            ret.setCounterPartyRole(rs.getString("counter_party_role"));
            ret.setCsid(rs.getString("csid"));
            ret.setLegalId(rs.getString("legal_id"));
            ret.setFormerlyKnownAs(rs.getString("formerly_known_as"));
            ret.setCountryOfInc(rs.getString("country_of_inc"));
            ret.setContractType(rs.getString("contract_type"));
            ret.setRequestType(rs.getString("request_type"));
            ret.setAgreementTitle(rs.getString("agreement_title"));
            ret.setContractDate(rs.getDate("contract_date"));
            ret.setAgency(rs.getString("agency"));
            ret.setCollateralAgreementFlag(rs.getString("collateral_agreement_flag"));
            ret.setCsaEnforceableFlag(rs.getString("csa_enforceable_flag"));
            ret.setTransactionSpecificFlag(rs.getString("transaction_specific_flag"));
            ret.setCsEntityAgencyFlag(rs.getString("cs_entity_agency_flag"));
            ret.setDesk(rs.getString("desk"));
            ret.setIconid(rs.getString("iconid"));
            ret.setCsaRegFlag(rs.getString("csa_reg_flag"));

            return ret;
        }
    }

}
