package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.Vision;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.VisionDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link Vision} persistence.
 */

public interface CustomizedVisionRepository {

    public Collection<VisionDTO> findByCustomCriteria(SearchInfoDTO info);

}







