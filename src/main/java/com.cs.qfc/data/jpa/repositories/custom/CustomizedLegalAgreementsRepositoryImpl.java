package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.LegalAgreements;
import com.cs.qfc.data.metadata.LegalAgreementsMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.LegalAgreementsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link LegalAgreements} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedLegalAgreementsRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedLegalAgreementsRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<LegalAgreementsDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + LegalAgreementsMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new LegalAgreementsMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new LegalAgreementsMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new LegalAgreementsMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<LegalAgreementsDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new LegalAgreementsRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     LegalAgreementsMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class LegalAgreementsRowMapper implements RowMapper<LegalAgreementsDTO> {
        @Override
        public LegalAgreementsDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            LegalAgreementsDTO ret = new LegalAgreementsDTO();


            ret.setId(rs.getInt("id"));
            ret.setSourceTxnId(rs.getString("source_txn_id"));
            ret.setA31AsOfDate(rs.getString("a31_as_of_date"));
            ret.setA32RecordsEntityIdentifier(rs.getString("a32_records_entity_identifier"));
            ret.setA33AgreementIdentifier(rs.getString("a33_agreement_identifier"));
            ret.setA34NameOfAgreementGoverningDoc(rs.getString("a34_name_of_agreement_or_governing_doc"));
            ret.setA35AgreementDate(rs.getString("a35_agreement_date"));
            ret.setA36AgreementCounterpartyIdentifier(rs.getString("a36_agreement_counterparty_identifier"));
            ret.setA361UnderlyingQfcObligatorIdentifier(rs.getString("a361_underlying_qfc_obligator_identifier"));
            ret.setA37AgreementGoverningLaw(rs.getString("a37_agreement_governing_law"));
            ret.setA38CrossDefaultProvision(rs.getString("a38_cross_default_provision"));
            ret.setA39IdentityOfCrossDefaultEntities(rs.getString("a39_identity_of_cross_default_entities"));
            ret.setA310CoveredByThirdPartyCreditEnhancement(rs.getString("a310_covered_by_third_party_credit_enhancement"));
            ret.setA311ThirdPartyCreditEnhancementProviderIdentifier(rs.getString("a311_third_party_credit_enhancement_provider_identifier"));
            ret.setA312AssociatedCreditEnhancementDocIdentifier(rs.getString("a312_associated_credit_enhancement_doc_identifier"));
            ret.setA3121CounterpartyThirdPartyCreditEnhancement(rs.getString("a3121_counterparty_third_party_credit_enhancement"));
            ret.setA3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier(rs.getString("a3122_counterparty_third_party_credit_enhancement_provider_identifier"));
            ret.setA3123CounterpartyAssociatedCreditEnhancementDocIdentifier(rs.getString("a3123_counterparty_associated_credit_enhancement_doc_identifier"));
            ret.setA313CounterpartyContactInfoName(rs.getString("a313_counterparty_contact_info_name"));
            ret.setA314CounterpartyContactInfoAddress(rs.getString("a314_counterparty_contact_info_address"));
            ret.setA315CounterpartyContactInfoPhone(rs.getString("a315_counterparty_contact_info_phone"));
            ret.setA316CounterpartyContactInfoEmail(rs.getString("a316_counterparty_contact_info_email"));

            return ret;
        }
    }

}
