package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.MarnaGGovtLaw;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.MarnaGGovtLawDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link MarnaGGovtLaw} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedMarnaGGovtLawRepository {

    public Collection<MarnaGGovtLawDTO> findByCustomCriteria(SearchInfoDTO info);

}







