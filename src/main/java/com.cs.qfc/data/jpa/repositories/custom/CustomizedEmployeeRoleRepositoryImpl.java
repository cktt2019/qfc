package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.EmployeeRole;
import com.cs.qfc.data.metadata.EmployeeRoleMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.EmployeeRoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link EmployeeRole} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedEmployeeRoleRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedEmployeeRoleRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<EmployeeRoleDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + EmployeeRoleMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new EmployeeRoleMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new EmployeeRoleMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new EmployeeRoleMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<EmployeeRoleDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new EmployeeRoleRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     EmployeeRoleMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class EmployeeRoleRowMapper implements RowMapper<EmployeeRoleDTO> {
        @Override
        public EmployeeRoleDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            EmployeeRoleDTO ret = new EmployeeRoleDTO();


            ret.setEmployeeId(rs.getString("employee_id"));
            ret.setEmployeeActiveIndicator(rs.getString("employee_active_indicator"));
            ret.setEmployeeEffectiveDate(rs.getDate("employee_effective_date"));
            ret.setEmployeeEmailId(rs.getString("employee_email_id"));
            ret.setEmployeeFirstName(rs.getString("employee_first_name"));
            ret.setEmployeeLastName(rs.getString("employee_last_name"));
            ret.setEmployeeLastUpdatedBy(rs.getString("employee_last_updated_by"));
            ret.setEmployeeLastUpdatedDatetime(rs.getString("employee_last_updated_datetime"));
            ret.setEmployeeLogonDomain(rs.getString("employee_logon_domain"));
            ret.setEmployeeLogonId(rs.getString("employee_logon_id"));
            ret.setEmployeePidId(rs.getString("employee_pid_id"));
            ret.setCity(rs.getString("city"));
            ret.setRegion(rs.getString("region"));
            ret.setCountryLongDesc(rs.getString("country_long_desc"));
            ret.setDeltaFlag(rs.getString("delta_flag"));

            return ret;
        }
    }

}
