package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.Department;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DepartmentDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link Department} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedDepartmentRepository {

    public Collection<DepartmentDTO> findByCustomCriteria(SearchInfoDTO info);

}







