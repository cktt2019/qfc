package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.EquityConvertibleBond;
import com.cs.qfc.data.metadata.EquityConvertibleBondMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.EquityConvertibleBondDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link EquityConvertibleBond} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedEquityConvertibleBondRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedEquityConvertibleBondRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<EquityConvertibleBondDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + EquityConvertibleBondMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new EquityConvertibleBondMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new EquityConvertibleBondMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new EquityConvertibleBondMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<EquityConvertibleBondDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new EquityConvertibleBondRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     EquityConvertibleBondMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class EquityConvertibleBondRowMapper implements RowMapper<EquityConvertibleBondDTO> {
        @Override
        public EquityConvertibleBondDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            EquityConvertibleBondDTO ret = new EquityConvertibleBondDTO();


            ret.setCusipId(rs.getString("cusip_id"));
            ret.setIsinId(rs.getString("isin_id"));
            ret.setNextCallDate(rs.getDate("next_call_date"));
            ret.setNextPutDate(rs.getString("next_put_date"));
            ret.setNxtPayDate(rs.getDate("nxt_pay_date"));
            ret.setSecurityDescription(rs.getString("security_description"));
            ret.setA3c7Ind(rs.getString("a_3c7_ind"));
            ret.setBbYKet(rs.getString("bb_y_ket"));
            ret.setCompExchCodeDer(rs.getString("comp_exch_code_der"));
            ret.setConvPrice(rs.getFloat("conv_price"));
            ret.setConRatio(rs.getFloat("con_ratio"));
            ret.setCsManaged(rs.getString("cs_managed"));
            ret.setEquityFloat(rs.getFloat("equity_float"));
            ret.setExchange(rs.getString("exchange"));
            ret.setFidesaCode(rs.getString("fidesa_code"));
            ret.setFundObjective(rs.getString("fund_objective"));
            ret.setGlossG5(rs.getString("gloss_g5"));
            ret.setIdBbGlobal(rs.getString("id_bb_global"));
            ret.setIdBbUnique(rs.getString("id_bb_unique"));
            ret.setItalyId(rs.getString("italy_id"));
            ret.setJapanId(rs.getString("japan_id"));
            ret.setRicId(rs.getString("ric_id"));
            ret.setSingId(rs.getString("sing_id"));
            ret.setValorenId(rs.getString("valoren_id"));
            ret.setWertpId(rs.getString("wertp_id"));
            ret.setHugoId(rs.getString("hugo_id"));
            ret.setNtpaLsEqyPoDt(rs.getString("ntpa_ls_eqy_po_dt"));
            ret.setLondonImsDescription(rs.getString("london_ims_description"));
            ret.setMaturityDate(rs.getDate("maturity_date"));
            ret.setNasdaqReportCodeDer(rs.getString("nasdaq_report_code_der"));
            ret.setPrimaryRic(rs.getString("primary_ric"));
            ret.setQtyPerUndlyShr(rs.getFloat("qty_per_undly_shr"));
            ret.setUndlyPerAdr(rs.getString("undly_per_adr"));
            ret.setRegs(rs.getString("regs"));
            ret.setRegionPrim(rs.getString("region_prim"));
            ret.setRtrTicketSymbol(rs.getString("rtr_ticket_symbol"));
            ret.setTradeCntry(rs.getString("trade_cntry"));
            ret.setUndlySecIsin(rs.getString("undly_sec_isin"));
            ret.setVotingRightPerShare(rs.getFloat("voting_right_per_share"));
            ret.setCmuId(rs.getString("cmu_id"));
            ret.setUltPrntTicketExchange(rs.getString("ult_prnt_ticket_exchange"));
            ret.setPoetsId(rs.getString("poets_id"));
            ret.setNtpaSoi(rs.getString("ntpa_soi"));
            ret.setCpn(rs.getFloat("cpn"));
            ret.setSsrLiquidityIndicator(rs.getString("ssr_liquidity_indicator"));
            ret.setReExchange(rs.getString("re_exchange"));
            ret.setGlossK3(rs.getString("gloss_k3"));
            ret.setSecType(rs.getString("sec_type"));
            ret.setFundEuroDirectUcits(rs.getString("fund_euro_direct_ucits"));
            ret.setFundAssetClassFocus(rs.getString("fund_asset_class_focus"));
            ret.setBbCompanyId(rs.getString("bb_company_id"));
            ret.setSpIssueId(rs.getString("sp_issue_id"));
            ret.setFundClosedNewInv(rs.getString("fund_closed_new_inv"));
            ret.setLegalName(rs.getString("legal_name"));
            ret.setTradingLotSize(rs.getFloat("trading_lot_size"));
            ret.setReAdrUndlyRic(rs.getString("re_adr_undly_ric"));
            ret.setRelativeIndex(rs.getString("relative_index"));
            ret.setPrimTrad(rs.getString("prim_trad"));
            ret.setCumulativeFlag(rs.getString("cumulative_flag"));
            ret.setFundCustodianName(rs.getString("fund_custodian_name"));
            ret.setInitialPubOffer(rs.getDate("initial_pub_offer"));
            ret.setGicsSectorIsr(rs.getString("gics_sector_isr"));
            ret.setGicsSectorNameIsr(rs.getString("gics_sector_name_isr"));
            ret.setGicsIndustryGroupIsr(rs.getString("gics_industry_group_isr"));
            ret.setGicsIndustryGroupNameIsr(rs.getString("gics_industry_group_name_isr"));
            ret.setGicsIndustrySr(rs.getString("gics_industry_sr"));
            ret.setGicsIndustryNameIsr(rs.getString("gics_industry_name_isr"));
            ret.setNaicsIdIsr(rs.getString("naics_id_isr"));
            ret.setNtpaCountryOfSettlement(rs.getString("ntpa_country_of_settlement"));
            ret.setGliRp(rs.getString("gli_rp"));
            ret.setCddsLastUpdatedTime(rs.getString("cdds_last_updated_time"));
            ret.setPubliclyTraded(rs.getString("publicly_traded"));
            ret.setCsSettleDays(rs.getString("cs_settle_days"));
            ret.setBbUnderlyingSecurity(rs.getString("bb_underlying_security"));
            ret.setBbUnderlyingTypeCode(rs.getString("bb_underlying_type_code"));
            ret.setTickSizePilotGroup(rs.getString("tick_size_pilot_group"));
            ret.setCalledDate(rs.getDate("called_date"));
            ret.setCalledPrice(rs.getFloat("called_price"));
            ret.setCouponType(rs.getString("coupon_type"));
            ret.setReClassScheme(rs.getString("re_class_scheme"));
            ret.setLegalEntityIdentifier(rs.getString("legal_entity_identifier"));
            ret.setCompanyCorpTicker(rs.getString("company_corp_ticker"));
            ret.setCompanyToParent(rs.getString("company_to_parent"));
            ret.setBbIssuerType(rs.getString("bb_issuer_type"));
            ret.setFisecIssuerId(rs.getString("fisec_issuer_id"));
            ret.setUltPrntCompId(rs.getString("ult_prnt_comp_id"));
            ret.setIsArchived(rs.getInt("is_archived"));
            ret.setLocalExchangeSymbol(rs.getString("local_exchange_symbol"));
            ret.setCfiCode(rs.getString("cfi_code"));
            ret.setFisn(rs.getString("fisn"));
            ret.setIssDate(rs.getDate("iss_date"));
            ret.setFirstSettlementDate(rs.getDate("first_settlement_date"));
            ret.setWhenIssuedFlag(rs.getString("when_issued_flag"));
            ret.setMinorTradingCurrencyInd(rs.getString("minor_trading_currency_ind"));
            ret.setRiskAlert(rs.getString("risk_alert"));
            ret.setMdsId(rs.getString("mds_id"));
            ret.setFundGeographicFocus(rs.getString("fund_geographic_focus"));
            ret.setFundLeverageAmount(rs.getString("fund_leverage_amount"));
            ret.setCmsCode(rs.getString("cms_code"));
            ret.setIdSpi(rs.getString("id_spi"));
            ret.setStaticListingId(rs.getString("static_listing_id"));
            ret.setClientType(rs.getString("client_type"));
            ret.setMarketingDesk(rs.getString("marketing_desk"));
            ret.setExternalId(rs.getString("external_id"));
            ret.setIssueBook(rs.getString("issue_book"));
            ret.setDutchId(rs.getString("dutch_id"));
            ret.setBelgiumId(rs.getString("belgium_id"));
            ret.setIstarId(rs.getString("istar_id"));
            ret.setIdBbSecurity(rs.getFloat("id_bb_security"));
            ret.setOperatingMic(rs.getString("operating_mic"));
            ret.setBbIndustrySectorCd(rs.getInt("bb_industry_sector_cd"));
            ret.setBbIndustryGroupCd(rs.getInt("bb_industry_group_cd"));
            ret.setBbIndustrySubGroupCd(rs.getInt("bb_industry_sub_group_cd"));
            ret.setCddsRecCreationTime(rs.getTimestamp("cdds_rec_creation_time"));
            ret.setListedExchInd(rs.getString("listed_exch_ind"));
            ret.setIdMalaysian(rs.getString("id_malaysian"));
            ret.setRedemptionPrice(rs.getFloat("redemption_price"));
            ret.setSeries(rs.getString("series"));
            ret.setCpnFreq(rs.getString("cpn_freq"));
            ret.setGmSoi(rs.getString("gm_soi"));
            ret.setCsPvtPlacementInd(rs.getString("cs_pvt_placement_ind"));

            return ret;
        }
    }

}
