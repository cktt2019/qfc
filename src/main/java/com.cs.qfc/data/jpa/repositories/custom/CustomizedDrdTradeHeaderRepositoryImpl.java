package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.DrdTradeHeader;
import com.cs.qfc.data.metadata.DrdTradeHeaderMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.DrdTradeHeaderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link DrdTradeHeader} .
 */

@Component
public class CustomizedDrdTradeHeaderRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedDrdTradeHeaderRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<DrdTradeHeaderDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + DrdTradeHeaderMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new DrdTradeHeaderMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new DrdTradeHeaderMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new DrdTradeHeaderMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<DrdTradeHeaderDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new DrdTradeHeaderRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     DrdTradeHeaderMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class DrdTradeHeaderRowMapper implements RowMapper<DrdTradeHeaderDTO> {
        @Override
        public DrdTradeHeaderDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            DrdTradeHeaderDTO ret = new DrdTradeHeaderDTO();


            ret.setId(rs.getInt("id"));
            ret.setCobDate(rs.getDate("cob_date"));
            ret.setLoadCodDate(rs.getDate("load_cod_date"));
            ret.setBookName(rs.getString("book_name"));
            ret.setTradeLoadId(rs.getString("trade_load_id"));
            ret.setLoadTimestamp(rs.getDate("load_timestamp"));
            ret.setSnapshotName(rs.getString("snapshot_name"));
            ret.setSnapshotId(rs.getInt("snapshot_id"));
            ret.setSnapshotCreationDatetime(rs.getDate("snapshot_creation_datetime"));
            ret.setInterfaceVersion(rs.getString("interface_version"));
            ret.setTradeId(rs.getInt("trade_id"));
            ret.setTfTradeVersion(rs.getInt("tf_trade_version"));
            ret.setTradeLocationId(rs.getInt("trade_location_id"));
            ret.setPrimaryTcn(rs.getString("primary_tcn"));
            ret.setTradeVersionUpdateTime(rs.getDate("trade_version_update_time"));
            ret.setCancellationDate(rs.getString("cancellation_date"));
            ret.setXccyIndicator(rs.getString("xccy_indicator"));
            ret.setGbmEntity(rs.getString("gbm_entity"));
            ret.setTotalLocalPv(rs.getInt("total_local_pv"));
            ret.setTotalLocalPvCcyCode(rs.getString("total_local_pv_ccy_code"));
            ret.setTotalPvStatus(rs.getString("total_pv_status"));
            ret.setTotalRpTpv(rs.getInt("total_rp_tpv"));
            ret.setTradePvRegDate(rs.getDate("trade_pv_reg_date"));
            ret.setTradePvRegDays(rs.getInt("trade_pv_reg_days"));
            ret.setConfirmationStatus(rs.getString("confirmation_status"));
            ret.setDefaultFairValue(rs.getInt("default_fair_value"));
            ret.setProveCluster(rs.getString("prove_cluster"));
            ret.setProveFamily(rs.getString("prove_family"));
            ret.setProveId(rs.getString("prove_id"));
            ret.setProveProduct(rs.getString("prove_product"));
            ret.setApprovalStatus(rs.getString("approval_status"));
            ret.setEventReason(rs.getString("event_reason"));
            ret.setExternalId(rs.getString("external_id"));
            ret.setPrimaryMaturityDate(rs.getDate("primary_maturity_date"));
            ret.setProductSubType(rs.getString("product_sub_type"));
            ret.setProductType(rs.getString("product_type"));
            ret.setProductValuationType(rs.getString("product_valuation_type"));
            ret.setRootTcn(rs.getString("root_tcn"));
            ret.setTradeStatus(rs.getString("trade_status"));
            ret.setCduMasterAgreementLlk(rs.getString("cdu_master_agreement_llk"));
            ret.setCduCollateralAnnexLlk(rs.getString("cdu_collateral_annex_llk"));
            ret.setFreamesoftMasterAgreementLlk(rs.getInt("freamesoft_master_agreement_llk"));
            ret.setAlgoCollateralAnnexLlk(rs.getInt("algo_collateral_annex_llk"));
            ret.setAccountArea(rs.getString("account_area"));
            ret.setBrokerId(rs.getString("broker_id"));
            ret.setCollIndepAmount(rs.getFloat("coll_indep_amount"));
            ret.setCollIndepCcy(rs.getString("coll_indep_ccy"));
            ret.setCollIndepOtherMargQuote(rs.getString("coll_indep_other_marg_quote"));
            ret.setCollIndepPercent(rs.getFloat("coll_indep_percent"));
            ret.setCollIndepType(rs.getString("coll_indep_type"));
            ret.setMarketerId(rs.getString("marketer_id"));
            ret.setRiskManagementOnlyFlag(rs.getString("risk_management_only_flag"));
            ret.setStructuredTradeFlag(rs.getString("structured_trade_flag"));
            ret.setTradeDate(rs.getDate("trade_date"));
            ret.setTraderName(rs.getString("trader_name"));
            ret.setTradeSourceSystem(rs.getString("trade_source_system"));
            ret.setTradingPartyId(rs.getString("trading_party_id"));
            ret.setTradingPartyIdType(rs.getString("trading_party_id_type"));
            ret.setCity(rs.getString("city"));
            ret.setDeal(rs.getString("deal"));
            ret.setStripId(rs.getString("strip_id"));
            ret.setCustomerNumber(rs.getString("customer_number"));
            ret.setDisplayId(rs.getString("display_id"));
            ret.setAssetClass1(rs.getString("asset_class1"));
            ret.setAssetClass2(rs.getString("asset_class2"));
            ret.setAssetClass3(rs.getString("asset_class3"));
            ret.setAssetClass4(rs.getString("asset_class4"));
            ret.setLinkedSetType(rs.getString("linked_set_type"));
            ret.setSysLinkId(rs.getString("sys_link_id"));
            ret.setLeadTradeId(rs.getInt("lead_trade_id"));
            ret.setLeadTradeVersion(rs.getInt("lead_trade_version"));
            ret.setLeadTradeLocationId(rs.getInt("lead_trade_location_id"));
            ret.setLeadExternalId(rs.getString("lead_external_id"));
            ret.setLeadTradeBook(rs.getString("lead_trade_book"));
            ret.setWashTradeId(rs.getInt("wash_trade_id"));
            ret.setWashTradeVersion(rs.getInt("wash_trade_version"));
            ret.setWashTradeLocationId(rs.getInt("wash_trade_location_id"));
            ret.setWashExternalId(rs.getString("wash_external_id"));
            ret.setWashTradeBook(rs.getString("wash_trade_book"));
            ret.setRiskTradeId(rs.getInt("risk_trade_id"));
            ret.setRiskTradeVersion(rs.getInt("risk_trade_version"));
            ret.setRiskTradeLocationId(rs.getInt("risk_trade_location_id"));
            ret.setRiskExternalId(rs.getString("risk_external_id"));
            ret.setRiskTradeBook(rs.getString("risk_trade_book"));
            ret.setSyntheticTradeIndicator(rs.getString("synthetic_trade_indicator"));
            ret.setLinkageStatus(rs.getString("linkage_status"));
            ret.setLinkageStatusDescription(rs.getString("linkage_status_description"));
            ret.setLinkedTradeId(rs.getInt("linked_trade_id"));
            ret.setLinkedPrimaryTcn(rs.getString("linked_primary_tcn"));
            ret.setLinkedTradeVersion(rs.getInt("linked_trade_version"));
            ret.setLinkedTradeLocationId(rs.getString("linked_trade_location_id"));
            ret.setLinkedTradeExternalId(rs.getString("linked_trade_external_id"));
            ret.setLinkedTradeBook(rs.getString("linked_trade_book"));
            ret.setValuationClass(rs.getString("valuation_class"));
            ret.setValuationType(rs.getString("valuation_type"));
            ret.setPremiumAmount(rs.getFloat("premium_amount"));
            ret.setSwapsWireId(rs.getString("swaps_wire_id"));
            ret.setApproxBooking(rs.getString("approx_booking"));
            ret.setApproxBookingReviewDate(rs.getDate("approx_booking_review_date"));
            ret.setDocsCounterPartyId(rs.getString("docs_counter_party_id"));
            ret.setCloIndicator(rs.getString("clo_indicator"));
            ret.setLoanMitigationFlag(rs.getString("loan_mitigation_flag"));
            ret.setUsi(rs.getString("usi"));
            ret.setCssi(rs.getString("cssi"));
            ret.setGmbBookId(rs.getInt("gmb_book_id"));
            ret.setArea(rs.getString("area"));
            ret.setStructuredId(rs.getString("structured_id"));
            ret.setPvLevel(rs.getString("pv_level"));
            ret.setCounterPartyCsid(rs.getString("counter_party_csid"));
            ret.setCreeTradeIdentifier(rs.getString("cree_trade_identifier"));
            ret.setCreeTradeIdentifierVersion(rs.getString("cree_trade_identifier_version"));
            ret.setLchOrigCounterParty(rs.getString("lch_orig_counter_party"));
            ret.setGbmCluster(rs.getString("gbm_cluster"));
            ret.setGbmDivision(rs.getString("gbm_division"));
            ret.setAffirmPlatformStatus(rs.getString("affirm_platform_status"));
            ret.setDocsActualProcessingMethod(rs.getString("docs_actual_processing_method"));
            ret.setUti(rs.getString("uti"));
            ret.setSwireManualConfirmedReq(rs.getString("swire_manual_confirmed_req"));
            ret.setEntryDateTime(rs.getDate("entry_date_time"));
            ret.setFundingIndicator(rs.getString("funding_indicator"));
            ret.setLeverageFactor(rs.getFloat("leverage_factor"));
            ret.setLinkTransId(rs.getString("link_trans_id"));
            ret.setLinkType(rs.getString("link_type"));
            ret.setMasterTradeTradeId(rs.getString("master_trade_trade_id"));
            ret.setGbmBookRef(rs.getString("gbm_book_ref"));
            ret.setLastAmmendBy(rs.getString("last_ammend_by"));
            ret.setStructureType(rs.getString("structure_type"));
            ret.setGarantiaSysId(rs.getString("garantia_sys_id"));
            ret.setEarlyTmTermParty(rs.getString("early_tm_term_party"));
            ret.setEarlyTmOptStartDate(rs.getDate("early_tm_opt_start_date"));
            ret.setCounterPartyGsid(rs.getString("counter_party_gsid"));
            ret.setEntityType(rs.getString("entity_type"));
            ret.setEvent(rs.getString("event"));
            ret.setEventTradeDate(rs.getDate("event_trade_date"));
            ret.setFidcPmInitialMargin(rs.getString("fidc_pm_initial_margin"));
            ret.setFidcPmNettingOnly(rs.getString("fidc_pm_netting_only"));
            ret.setFidcPmVariationMargin(rs.getString("fidc_pm_variation_margin"));
            ret.setLateTrdNotificationReason(rs.getString("late_trd_notification_reason"));
            ret.setMasterAgreementType(rs.getInt("master_agreement_type"));
            ret.setNotesFrontOffice(rs.getString("notes_front_office"));
            ret.setRiskAdvisor(rs.getString("risk_advisor"));
            ret.setRiskTrader(rs.getString("risk_trader"));
            ret.setTradingPartyGsid(rs.getString("trading_party_gsid"));
            ret.setFidcPmInd(rs.getString("fidc_pm_ind"));
            ret.setAgent(rs.getString("agent"));
            ret.setPaymentIndicator(rs.getString("payment_indicator"));
            ret.setRenegotiationDate(rs.getDate("renegotiation_date"));
            ret.setAlternativeVersionId(rs.getInt("alternative_version_id"));
            ret.setCifbu(rs.getString("cifbu"));
            ret.setDeskBook(rs.getString("desk_book"));
            ret.setMetalInkTradeId(rs.getString("metal_ink_trade_id"));
            ret.setLinkedStartDate(rs.getDate("linked_start_date"));
            ret.setLinkedEndDate(rs.getDate("linked_end_date"));
            ret.setSalespersonId(rs.getString("salesperson_id"));
            ret.setSourceSystemId(rs.getString("source_system_id"));
            ret.setSourceSystemVersion(rs.getInt("source_system_version"));
            ret.setEarlyTmExerciseFreq(rs.getString("early_tm_exercise_freq"));
            ret.setEarlyTmOptStyle(rs.getString("early_tm_opt_style"));
            ret.setPaymentPackageSysLinkId(rs.getString("payment_package_sys_link_id"));
            ret.setTfMatchStatus(rs.getString("tf_match_status"));
            ret.setBestMatchTfTradeVersion(rs.getInt("best_match_tf_trade_version"));

            return ret;
        }
    }

}
