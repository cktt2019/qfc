package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.Vision;
import com.cs.qfc.data.metadata.VisionMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.VisionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link Vision} .
 */

@Component
public class CustomizedVisionRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedVisionRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<VisionDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + VisionMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new VisionMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new VisionMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new VisionMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

        System.out.println("executing query " + sql);
        List<VisionDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new VisionRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     VisionMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class VisionRowMapper implements RowMapper<VisionDTO> {
        @Override
        public VisionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            VisionDTO ret = new VisionDTO();


            ret.setBusinessDate(rs.getDate("business_date"));
            ret.setEntity(rs.getString("entity"));
            ret.setCpty(rs.getString("cpty"));
            ret.setBook(rs.getString("book"));
            ret.setProveNameProduct(rs.getString("prove_name_product"));
            ret.setAgreementId1(rs.getString("agreement_id1"));
            ret.setTradeDate(rs.getDate("trade_date"));
            ret.setEndDate(rs.getDate("end_date"));
            ret.setBondNextCoup(rs.getString("bond_next_coup"));
            ret.setCashCurr(rs.getString("cash_curr"));
            ret.setPvUsd(rs.getString("pv_usd"));
            ret.setDefaultFvLevel(rs.getString("default_fv_level"));
            ret.setFace(rs.getString("face"));
            ret.setValueOn(rs.getString("value_on"));
            ret.setRehypothecationIndicator(rs.getString("rehypothecation_indicator"));
            ret.setCustodianId(rs.getString("custodian_id"));
            ret.setId(rs.getInt("id"));

            return ret;
        }
    }

}
