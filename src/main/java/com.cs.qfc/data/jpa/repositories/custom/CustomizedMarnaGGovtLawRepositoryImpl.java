package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.MarnaGGovtLaw;
import com.cs.qfc.data.metadata.MarnaGGovtLawMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.MarnaGGovtLawDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link MarnaGGovtLaw} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedMarnaGGovtLawRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedMarnaGGovtLawRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<MarnaGGovtLawDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + MarnaGGovtLawMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new MarnaGGovtLawMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new MarnaGGovtLawMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new MarnaGGovtLawMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<MarnaGGovtLawDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new MarnaGGovtLawRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     MarnaGGovtLawMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class MarnaGGovtLawRowMapper implements RowMapper<MarnaGGovtLawDTO> {
        @Override
        public MarnaGGovtLawDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            MarnaGGovtLawDTO ret = new MarnaGGovtLawDTO();


            ret.setBusinessDate(rs.getDate("business_date"));
            ret.setAgmtNo(rs.getString("agmt_no"));
            ret.setCnid(rs.getString("cnid"));
            ret.setAgmtDate(rs.getString("agmt_date"));
            ret.setCounterparty(rs.getString("counterparty"));
            ret.setCsid(rs.getString("csid"));
            ret.setAgmtType(rs.getString("agmt_type"));
            ret.setAgmtTitle(rs.getString("agmt_title"));
            ret.setCoi(rs.getString("coi"));
            ret.setGoverningLaw(rs.getString("governing_law"));
            ret.setId(rs.getInt("id"));

            return ret;
        }
    }

}
