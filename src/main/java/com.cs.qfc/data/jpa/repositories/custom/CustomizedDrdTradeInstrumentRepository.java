package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.DrdTradeInstrument;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DrdTradeInstrumentDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link DrdTradeInstrument} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedDrdTradeInstrumentRepository {

    public Collection<DrdTradeInstrumentDTO> findByCustomCriteria(SearchInfoDTO info);

}







