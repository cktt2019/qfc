package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.CounterpartyNetting;
import com.cs.qfc.data.metadata.CounterpartyNettingMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.CounterpartyNettingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link CounterpartyNetting} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedCounterpartyNettingRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedCounterpartyNettingRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<CounterpartyNettingDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + CounterpartyNettingMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new CounterpartyNettingMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new CounterpartyNettingMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new CounterpartyNettingMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<CounterpartyNettingDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new CounterpartyNettingRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     CounterpartyNettingMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class CounterpartyNettingRowMapper implements RowMapper<CounterpartyNettingDTO> {
        @Override
        public CounterpartyNettingDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            CounterpartyNettingDTO ret = new CounterpartyNettingDTO();


            ret.setId(rs.getInt("id"));
            ret.setSourceTxnId(rs.getString("source_txn_id"));
            ret.setA21AsOfDate(rs.getString("a21_as_of_date"));
            ret.setA22RecordsEntityIdentifier(rs.getString("a22_records_entity_identifier"));
            ret.setA23NettingAgreementCounterpartyIdentifier(rs.getString("a23_netting_agreement_counterparty_identifier"));
            ret.setA24NettingAgreementIdentifier(rs.getString("a24_netting_agreement_identifier"));
            ret.setA241UnderlyingQfcObligatorIdentifier(rs.getString("a241_underlying_qfc_obligator_identifier"));
            ret.setA25CoveredByThirdPartyCreditEnhancement(rs.getString("a25_covered_by_third_party_credit_enhancement"));
            ret.setA251ThirdPartyCreditEnhancementProviderIdentifier(rs.getString("a251_third_party_credit_enhancement_provider_identifier"));
            ret.setA252ThirdPartyCreditEnhancementAgreementIdentifier(rs.getString("a252_third_party_credit_enhancement_agreement_identifier"));
            ret.setA253CounterpartyThirdPartyCreditEnhancement(rs.getString("a253_counterparty_third_party_credit_enhancement"));
            ret.setA254CounterpartyThirdPartyCreditEnhancementProviderIdentifier(rs.getString("a254_counterparty_third_party_credit_enhancement_provider_identifier"));
            ret.setA255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier(rs.getString("a255_counterparty_third_party_credit_enhancement_agreement_identifier"));
            ret.setA26AggregateCurrMarketValueAllPositionsUsd(rs.getString("a26_aggregate_curr_market_value_all_positions_usd"));
            ret.setA27AggregateCurrMarketValuePositivePositionsUsd(rs.getString("a27_aggregate_curr_market_value_positive_positions_usd"));
            ret.setA28AggregateCurrMarketValueNegativePositionsUsd(rs.getString("a28_aggregate_curr_market_value_negative_positions_usd"));
            ret.setA29CurrMarketValueAllCollateralRecordsEntityInUsd(rs.getString("a29_curr_market_value_all_collateral_records_entity_in_usd"));
            ret.setA210CurrMarketValueAllCollateralCounterpartyInUsd(rs.getString("a210_curr_market_value_all_collateral_counterparty_in_usd"));
            ret.setA211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd(rs.getString("a211_curr_market_value_all_collateral_records_entity_rehypo_in_usd"));
            ret.setA212CurrMarketValueAllCollateralCounterpartyRehypoInUsd(rs.getString("a212_curr_market_value_all_collateral_counterparty_rehypo_in_usd"));
            ret.setA213RecordsEntityCollateralNet(rs.getString("a213_records_entity_collateral_net"));
            ret.setA214CounterpartyCollateralNet(rs.getString("a214_counterparty_collateral_net"));
            ret.setA215NextMarginPaymentDate(rs.getString("a215_next_margin_payment_date"));
            ret.setA216NextMarginPaymentAmountUsd(rs.getString("a216_next_margin_payment_amount_usd"));
            ret.setA217SafekeepingAgentIdentifierRecordsEntity(rs.getString("a217_safekeeping_agent_identifier_records_entity"));
            ret.setA218SafekeepingAgentIdentifierCounterparty(rs.getString("a218_safekeeping_agent_identifier_counterparty"));

            return ret;
        }
    }

}
