package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.AgreementTitleScope;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.AgreementTitleScopeDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link AgreementTitleScope} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedAgreementTitleScopeRepository {

    public Collection<AgreementTitleScopeDTO> findByCustomCriteria(SearchInfoDTO info);

}







