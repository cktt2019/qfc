package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.DrdTradeHeader;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DrdTradeHeaderDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link DrdTradeHeader} persistence.
 */

public interface CustomizedDrdTradeHeaderRepository {

    public Collection<DrdTradeHeaderDTO> findByCustomCriteria(SearchInfoDTO info);

}







