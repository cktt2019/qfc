package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.DTPMISGBLDYTREE;
import com.cs.qfc.data.metadata.DTPMISGBLDYTREEMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.DTPMISGBLDYTREEDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link DTPMISGBLDYTREE} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedDTPMISGBLDYTREERepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedDTPMISGBLDYTREERepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<DTPMISGBLDYTREEDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + DTPMISGBLDYTREEMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new DTPMISGBLDYTREEMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new DTPMISGBLDYTREEMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new DTPMISGBLDYTREEMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<DTPMISGBLDYTREEDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new DTPMISGBLDYTREERowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     DTPMISGBLDYTREEMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class DTPMISGBLDYTREERowMapper implements RowMapper<DTPMISGBLDYTREEDTO> {
        @Override
        public DTPMISGBLDYTREEDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            DTPMISGBLDYTREEDTO ret = new DTPMISGBLDYTREEDTO();


            ret.setMisMemberCode(rs.getString("mis_member_code"));
            ret.setAmericasProductLineHeadId(rs.getString("americas_product_line_head_id"));
            ret.setAmericasClusterHeadId(rs.getString("americas_cluster_head_id"));
            ret.setApacClusterHeadId(rs.getString("apac_cluster_head_id"));
            ret.setApacProductLineHeadId(rs.getString("apac_product_line_head_id"));
            ret.setBusinessControllerL2Id(rs.getString("business_controller_l2_id"));
            ret.setCluster(rs.getString("cluster"));
            ret.setClusterCode(rs.getString("cluster_code"));
            ret.setClusterHeadId(rs.getString("cluster_head_id"));
            ret.setDesk(rs.getString("desk"));
            ret.setDeskCode(rs.getString("desk_code"));
            ret.setDivision(rs.getString("division"));
            ret.setDivisionCode(rs.getString("division_code"));
            ret.setEuroClusterHeadId(rs.getString("euro_cluster_head_id"));
            ret.setEuroProductLineHeadId(rs.getString("euro_product_line_head_id"));
            ret.setGlobalProductLineHeadId(rs.getString("global_product_line_head_id"));
            ret.setHierarchyTraderId(rs.getString("hierarchy_trader_id"));
            ret.setMisHierarchySortCode(rs.getString("mis_hierarchy_sort_code"));
            ret.setMisMemberName(rs.getString("mis_member_name"));
            ret.setMisMemberTypeCode(rs.getString("mis_member_type_code"));
            ret.setMisProduct(rs.getString("mis_product"));
            ret.setMisRegionCode(rs.getString("mis_region_code"));
            ret.setMisTreeCode(rs.getString("mis_tree_code"));
            ret.setParentMisMemberCode(rs.getString("parent_mis_member_code"));
            ret.setProductControlSectionMgrId(rs.getString("product_control_section_mgr_id"));
            ret.setProductLineCode(rs.getString("product_line_code"));
            ret.setProductLineName(rs.getString("product_line_name"));
            ret.setSub3ProductLine(rs.getString("sub_3_product_line"));
            ret.setSub3ProductLineDescription(rs.getString("sub_3_product_line_description"));
            ret.setSubClusterCode(rs.getString("sub_cluster_code"));
            ret.setSubClusterName(rs.getString("sub_cluster_name"));
            ret.setSubDivisionCode(rs.getString("sub_division_code"));
            ret.setSubDivisionDescription(rs.getString("sub_division_description"));
            ret.setSubProductDescription(rs.getString("sub_product_description"));
            ret.setSubProductLine(rs.getString("sub_product_line"));
            ret.setSwissProductLineHeadId(rs.getString("swiss_product_line_head_id"));
            ret.setTradingSupervisorId(rs.getString("trading_supervisor_id"));
            ret.setTradingSupervisorDelegateId(rs.getString("trading_supervisor_delegate_id"));
            ret.setMisProductCode(rs.getString("mis_product_code"));
            ret.setMisUnitPercentOwned(rs.getString("mis_unit_percent_owned"));
            ret.setMisUnitPercentDescription(rs.getString("mis_unit_percent_description"));
            ret.setDeltaFlag(rs.getString("delta_flag"));
            ret.setCddsTreeNodeStatus(rs.getString("cdds_tree_node_status"));
            ret.setTradingSupport(rs.getString("trading_support"));
            ret.setTradeSupportManagerEmail(rs.getString("trade_support_manager_email"));

            return ret;
        }
    }

}
