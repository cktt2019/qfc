package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.LoanIQ;
import com.cs.qfc.data.metadata.LoanIQMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.LoanIQDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link LoanIQ} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedLoanIQRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedLoanIQRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<LoanIQDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + LoanIQMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new LoanIQMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new LoanIQMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new LoanIQMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<LoanIQDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new LoanIQRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     LoanIQMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class LoanIQRowMapper implements RowMapper<LoanIQDTO> {
        @Override
        public LoanIQDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            LoanIQDTO ret = new LoanIQDTO();


            ret.setId(rs.getInt("id"));
            ret.setCptyCsid(rs.getString("cpty_csid"));
            ret.setSdsId(rs.getString("sds_id"));
            ret.setPortfolioCode(rs.getString("portfolio_code"));
            ret.setAgreementId(rs.getString("agreement_id"));
            ret.setTradeDate(rs.getDate("trade_date"));
            ret.setMaturityDate(rs.getString("maturity_date"));
            ret.setCurrency(rs.getString("currency"));
            ret.setPresentValue(rs.getFloat("present_value"));
            ret.setTradeAmount(rs.getFloat("trade_amount"));
            ret.setRecordType(rs.getString("record_type"));
            ret.setSwapId(rs.getString("swap_id"));

            return ret;
        }
    }

}
