package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.DebtInstrument;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DebtInstrumentDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link DebtInstrument} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedDebtInstrumentRepository {

    public Collection<DebtInstrumentDTO> findByCustomCriteria(SearchInfoDTO info);

}







