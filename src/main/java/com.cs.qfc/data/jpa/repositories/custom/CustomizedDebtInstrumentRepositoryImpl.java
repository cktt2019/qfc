package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.DebtInstrument;
import com.cs.qfc.data.metadata.DebtInstrumentMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.DebtInstrumentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link DebtInstrument} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedDebtInstrumentRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedDebtInstrumentRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<DebtInstrumentDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + DebtInstrumentMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new DebtInstrumentMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new DebtInstrumentMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new DebtInstrumentMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<DebtInstrumentDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new DebtInstrumentRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     DebtInstrumentMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class DebtInstrumentRowMapper implements RowMapper<DebtInstrumentDTO> {
        @Override
        public DebtInstrumentDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            DebtInstrumentDTO ret = new DebtInstrumentDTO();


            ret.setGpiIssue(rs.getString("gpi_issue"));
            ret.setIsinId(rs.getString("isin_id"));
            ret.setSedolId(rs.getString("sedol_id"));
            ret.setSecurityDescriptionLong(rs.getString("security_description_long"));
            ret.setNxtPayDate(rs.getDate("nxt_pay_date"));
            ret.setNextCallDate(rs.getDate("next_call_date"));
            ret.setNextPutDate(rs.getDate("next_put_date"));

            return ret;
        }
    }

}
