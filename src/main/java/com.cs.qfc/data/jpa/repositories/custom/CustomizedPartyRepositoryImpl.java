package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.metadata.PartyMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.Party;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link com.cs.qfc.data.jpa.entities.Party} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedPartyRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedPartyRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<Party> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + PartyMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new PartyMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new PartyMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new PartyMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<Party> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new PartyRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     PartyMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class PartyRowMapper implements RowMapper<Party> {
        @Override
        public Party mapRow(ResultSet rs, int rowNum) throws SQLException {
            Party ret = new Party();


            ret.setGlobalPartyId(rs.getLong("global_party_id"));
            ret.setCsid(rs.getString("csid"));
            ret.setCsidSource(rs.getString("csid_source"));
            ret.setGsid(rs.getString("gsid"));
            ret.setGsidSource(rs.getString("gsid_source"));
            ret.setCountryOfDomicileIsoCountryCode(rs.getString("country_of_domicile_iso_country_code"));
            ret.setCountryOfIncorporationIsoCountryCode(rs.getString("country_of_incorporation_iso_country_code"));
            ret.setPartyLegalName(rs.getString("party_legal_name"));
            ret.setAddressTypeCode(rs.getString("address_type_code"));
            ret.setAddressAddressLine1(rs.getString("address_address_line1"));
            ret.setAddressAddressLine2(rs.getString("address_address_line2"));
            ret.setAddressAddressLine3(rs.getString("address_address_line3"));
            ret.setAddressAddressLine4(rs.getString("address_address_line4"));
            ret.setAddressPostalCode(rs.getString("address_postal_code"));
            ret.setAddressCountryIsoCountryName(rs.getString("address_country_iso_country_name"));
            ret.setAddressAddressFullPhoneNumber(rs.getString("address_address_full_phone_number"));
            ret.setAddressElctronicAddressId(rs.getString("address_elctronic_address_id"));
            ret.setPartyLegalRoleDefinitionId(rs.getString("party_legal_role_definition_id"));
            ret.setPartyLegalRoleId(rs.getString("party_legal_role_id"));
            ret.setInternalPartyIdAltSourceName(rs.getString("internal_party_id_alt_source_name"));
            ret.setPartyLegalRoleDescription(rs.getString("party_legal_role_description"));
            ret.setPartyLegalStatusType(rs.getString("party_legal_status_type"));
            ret.setPartyLegalStatusCode(rs.getString("party_legal_status_code"));
            ret.setAddressTypeDescription(rs.getString("address_type_description"));
            ret.setAddressCityName(rs.getString("address_city_name"));
            ret.setAddressPoBoxCity(rs.getString("address_po_box_city"));
            ret.setExternalPartyAltIdSourceId(rs.getString("external_party_alt_id_source_id"));
            ret.setExternalPartyAltIdSourceName(rs.getString("external_party_alt_id_source_name"));
            ret.setExternalPartyAltIdSourceIdentificationNumber(rs.getString("external_party_alt_id_source_identification_number"));

            return ret;
        }
    }

}
