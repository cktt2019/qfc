package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.Book;
import com.cs.qfc.data.metadata.BookMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.BookDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link Book} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedBookRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedBookRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<BookDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + BookMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new BookMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new BookMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new BookMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

        System.out.println("executing query " + sql);
        List<BookDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new BookRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     BookMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class BookRowMapper implements RowMapper<BookDTO> {
        @Override
        public BookDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            BookDTO ret = new BookDTO();


            ret.setGlobalBookId(rs.getString("global_book_id"));
            ret.setAccountMethod(rs.getString("account_method"));
            ret.setAccountingBasisIndicator(rs.getString("accounting_basis_indicator"));
            ret.setBookCategoryCode(rs.getString("book_category_code"));
            ret.setBookCategoryDescription(rs.getString("book_category_description"));
            ret.setBookControllerId(rs.getString("book_controller_id"));
            ret.setBookDormantIndicator(rs.getString("book_dormant_indicator"));
            ret.setBookName(rs.getString("book_name"));
            ret.setBookPnlSystemName(rs.getString("book_pnl_system_name"));
            ret.setBookRef(rs.getString("book_ref"));
            ret.setBookStatus(rs.getString("book_status"));
            ret.setBookSubCategoryCode(rs.getString("book_sub_category_code"));
            ret.setBookSubCategoryDescription(rs.getString("book_sub_category_description"));
            ret.setBookTraderId(rs.getString("book_trader_id"));
            ret.setCnyDeliverableOutsideChinaIndicator(rs.getString("cny_deliverable_outside_china_indicator"));
            ret.setDepartmentId(rs.getString("department_id"));
            ret.setFrontOfficeRepresentativeId(rs.getString("front_office_representative_id"));
            ret.setGplAtomCode(rs.getString("gpl_atom_code"));
            ret.setNhfsIndicator(rs.getString("nhfs_indicator"));
            ret.setRemoteBookingPolicyIndicator(rs.getString("remote_booking_policy_indicator"));
            ret.setSplitHedgeComments(rs.getString("split_hedge_comments"));
            ret.setSplitHedgeIndicator(rs.getString("split_hedge_indicator"));
            ret.setUsTaxCategoryCode(rs.getString("us_tax_category_code"));
            ret.setBookLastUpdatedDatetime(rs.getTimestamp("book_last_updated_datetime"));
            ret.setFinancialBusinessUnitCode(rs.getString("financial_business_unit_code"));
            ret.setDeltaFlag(rs.getString("delta_flag"));
            ret.setBookRequestStatusCode(rs.getString("book_request_status_code"));
            ret.setFirstInstanceBookActivated(rs.getDate("first_instance_book_activated"));
            ret.setLastInstanceBookActivated(rs.getDate("last_instance_book_activated"));
            ret.setFirstInstanceBookInactivated(rs.getDate("first_instance_book_inactivated"));
            ret.setLastInstanceBook(rs.getDate("last_instance_book_"));
            ret.setEmailAddress(rs.getString("email_address"));
            ret.setCavFlag(rs.getString("cav_flag"));
            ret.setFosFlag(rs.getString("fos_flag"));
            ret.setBookingIntent(rs.getString("booking_intent"));
            ret.setBookRequestId(rs.getString("book_request_id"));
            ret.setIparEmplIdEmailId(rs.getString("ipar_empl_id_email_id"));
            ret.setBookTraderEmailId(rs.getString("book_trader_email_id"));
            ret.setPcSupervisorEmailId(rs.getString("pc_supervisor_email_id"));
            ret.setBookEmpIdEmailId(rs.getString("book_emp_id_email_id"));
            ret.setPctnlEmplIdEmailId(rs.getString("pctnl_empl_id_email_id"));

            return ret;
        }
    }

}
