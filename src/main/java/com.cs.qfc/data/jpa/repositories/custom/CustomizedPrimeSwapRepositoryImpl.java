package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.metadata.PrimeSwapMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.PrimeSwap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link com.cs.qfc.data.jpa.entities.PrimeSwap} .
 */

@Component
public class CustomizedPrimeSwapRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedPrimeSwapRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<PrimeSwap> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + PrimeSwapMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new PrimeSwapMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new PrimeSwapMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new PrimeSwapMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<PrimeSwap> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new PrimeSwapRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     PrimeSwapMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class PrimeSwapRowMapper implements RowMapper<PrimeSwap> {
        @Override
        public PrimeSwap mapRow(ResultSet rs, int rowNum) throws SQLException {
            PrimeSwap ret = new PrimeSwap();


            ret.setBusinessDate(rs.getDate("business_date"));
            ret.setLegalEntity(rs.getString("legal_entity"));
            ret.setSwapId(rs.getString("swap_id"));
            ret.setSwapVersion(rs.getString("swap_version"));
            ret.setCounterPartyCode(rs.getString("counter_party_code"));
            ret.setCounterPartyName(rs.getString("counter_party_name"));
            ret.setBook(rs.getString("book"));
            ret.setTrader(rs.getString("trader"));
            ret.setSwapType(rs.getString("swap_type"));
            ret.setFramesoftMasterAgreementLlk(rs.getString("framesoft_master_agreement_llk"));
            ret.setAlgoCollateralAgreementLlk(rs.getString("algo_collateral_agreement_llk"));
            ret.setCduMasterAgreementLlk(rs.getString("cdu_master_agreement_llk"));
            ret.setCduCollateralAnnexLlk(rs.getString("cdu_collateral_annex_llk"));
            ret.setSwapTradeDate(rs.getDate("swap_trade_date"));
            ret.setSwapTerminationDate(rs.getDate("swap_termination_date"));
            ret.setNextInterestPayDate(rs.getDate("next_interest_pay_date"));
            ret.setEquityCurrency(rs.getString("equity_currency"));
            ret.setTotalSwapValue(rs.getFloat("total_swap_value"));
            ret.setCurrencyEquityNotional(rs.getString("currency_equity_notional"));
            ret.setInitMargin(rs.getFloat("init_margin"));
            ret.setInitialMarginDirection(rs.getString("initial_margin_direction"));
            ret.setCsid(rs.getString("csid"));
            ret.setGsid(rs.getString("gsid"));
            ret.setId(rs.getInt("id"));

            return ret;
        }
    }

}
