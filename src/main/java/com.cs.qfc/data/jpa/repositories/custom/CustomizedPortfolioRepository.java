package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.Portfolio;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.PortfolioDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link Portfolio} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedPortfolioRepository {

    public Collection<PortfolioDTO> findByCustomCriteria(SearchInfoDTO info);

}







