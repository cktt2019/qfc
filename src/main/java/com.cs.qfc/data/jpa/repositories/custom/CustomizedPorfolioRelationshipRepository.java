package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.PorfolioRelationship;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.PorfolioRelationshipDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link PorfolioRelationship} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedPorfolioRelationshipRepository {

    public Collection<PorfolioRelationshipDTO> findByCustomCriteria(SearchInfoDTO info);

}







