package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.Department;
import com.cs.qfc.data.metadata.DepartmentMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.DepartmentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link Department} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedDepartmentRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedDepartmentRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<DepartmentDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + DepartmentMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new DepartmentMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new DepartmentMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new DepartmentMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<DepartmentDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new DepartmentRowMapper());

        return resp;

    }

    //For oracle : OFFSET N ROWS FETCH Y ROWS ONLY
    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     DepartmentMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class DepartmentRowMapper implements RowMapper<DepartmentDTO> {
        @Override
        public DepartmentDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            DepartmentDTO ret = new DepartmentDTO();


            ret.setDepartmentId(rs.getString("department_id"));
            ret.setBusinessControllerId(rs.getString("business_controller_id"));
            ret.setControllerId(rs.getString("controller_id"));
            ret.setDepartmentCategoryCode(rs.getString("department_category_code"));
            ret.setDepartmentCategoryName(rs.getString("department_category_name"));
            ret.setDepartmentDormantIndicator(rs.getString("department_dormant_indicator"));
            ret.setDepartmentLongName(rs.getString("department_long_name"));
            ret.setDepartmentName(rs.getString("department_name"));
            ret.setDepartmentReconcilerGroupName(rs.getString("department_reconciler_group_name"));
            ret.setDepartmentStatus(rs.getString("department_status"));
            ret.setDepartmentTraderId(rs.getString("department_trader_id"));
            ret.setDepartmentValidFromDate(rs.getDate("department_valid_from_date"));
            ret.setDepartmentValidToDate(rs.getDate("department_valid_to_date"));
            ret.setFinancialBusinessUnitCode(rs.getString("financial_business_unit_code"));
            ret.setIvrGroup(rs.getString("ivr_group"));
            ret.setL1SupervisorId(rs.getString("l1_supervisor_id"));
            ret.setMisOfficeCode(rs.getString("mis_office_code"));
            ret.setMisOfficeName(rs.getString("mis_office_name"));
            ret.setOwnerL2Id(rs.getString("owner_l2_id"));
            ret.setRecAndAdjControllerId(rs.getString("rec_and_adj_controller_id"));
            ret.setRecAndAdjSupervisorId(rs.getString("rec_and_adj_supervisor_id"));
            ret.setReconcilerId(rs.getString("reconciler_id"));
            ret.setRegionCode(rs.getString("region_code"));
            ret.setRegionDescription(rs.getString("region_description"));
            ret.setRevenueType(rs.getString("revenue_type"));
            ret.setRevenueTypeDescription(rs.getString("revenue_type_description"));
            ret.setRiskPortfolioId(rs.getString("risk_portfolio_id"));
            ret.setSpecialisationIndicator(rs.getString("specialisation_indicator"));
            ret.setTaxCategory(rs.getString("tax_category"));
            ret.setVolkerRuleDescription(rs.getString("volker_rule_description"));
            ret.setVolkerRuleIndicator(rs.getString("volker_rule_indicator"));
            ret.setFxCentrallyMaangedName(rs.getString("fx_centrally_maanged_name"));
            ret.setVolckerSubDivision(rs.getString("volcker_sub_division"));
            ret.setVolckerSubDivisionDescription(rs.getString("volcker_sub_division_description"));
            ret.setIhcHcdFlag(rs.getString("ihc_hcd_flag"));
            ret.setCoveredDeptFlag(rs.getString("covered_dept_flag"));
            ret.setDeltaFlag(rs.getString("delta_flag"));
            ret.setRemittancePolicyCode(rs.getString("remittance_policy_code"));
            ret.setFirstInstanceDeptActivated(rs.getString("first_instance_dept_activated"));
            ret.setFirstInstanceDeptInactivated(rs.getString("first_instance_dept_inactivated"));
            ret.setLastInstanceDeptActivated(rs.getString("last_instance_dept_activated"));
            ret.setLastInstanceDeptInactivated(rs.getString("last_instance_dept_inactivated"));
            ret.setRbpnlFlag(rs.getString("rbpnl_flag"));
            ret.setIvrGroupLongName(rs.getString("ivr_group_long_name"));
            ret.setFrtbDeptCategory(rs.getString("frtb_dept_category"));

            return ret;
        }
    }

}
