package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.PrimeSwapPosition;
import com.cs.qfc.data.metadata.PrimeSwapPositionMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.PrimeSwapPositionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link PrimeSwapPosition} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedPrimeSwapPositionRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedPrimeSwapPositionRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<PrimeSwapPositionDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + PrimeSwapPositionMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new PrimeSwapPositionMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new PrimeSwapPositionMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new PrimeSwapPositionMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<PrimeSwapPositionDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new PrimeSwapPositionRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     PrimeSwapPositionMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class PrimeSwapPositionRowMapper implements RowMapper<PrimeSwapPositionDTO> {
        @Override
        public PrimeSwapPositionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            PrimeSwapPositionDTO ret = new PrimeSwapPositionDTO();


            ret.setId(rs.getInt("id"));
            ret.setBusinessDate(rs.getDate("business_date"));
            ret.setSwapId(rs.getString("swap_id"));
            ret.setSwapVersion(rs.getString("swap_version"));
            ret.setPositionId(rs.getString("position_id"));
            ret.setPositionVersion(rs.getString("position_version"));
            ret.setSwapType(rs.getString("swap_type"));
            ret.setCounterpartyCode(rs.getString("counterparty_code"));
            ret.setCounterpartyName(rs.getString("counterparty_name"));
            ret.setSettledLtdCostBase(rs.getString("settled_ltd_cost_base"));
            ret.setSwapCcyToUsdFx(rs.getString("swap_ccy_to_usd_fx"));

            return ret;
        }
    }

}
