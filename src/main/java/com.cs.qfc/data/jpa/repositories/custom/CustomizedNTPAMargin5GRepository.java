package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.NTPAMargin5G;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.NTPAMargin5GDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link NTPAMargin5G} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedNTPAMargin5GRepository {

    public Collection<NTPAMargin5GDTO> findByCustomCriteria(SearchInfoDTO info);

}







