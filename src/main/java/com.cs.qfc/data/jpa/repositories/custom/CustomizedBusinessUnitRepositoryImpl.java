package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.BusinessUnit;
import com.cs.qfc.data.metadata.BusinessUnitMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.BusinessUnitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link BusinessUnit} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedBusinessUnitRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedBusinessUnitRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<BusinessUnitDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + BusinessUnitMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new BusinessUnitMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new BusinessUnitMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new BusinessUnitMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<BusinessUnitDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new BusinessUnitRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     BusinessUnitMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class BusinessUnitRowMapper implements RowMapper<BusinessUnitDTO> {
        @Override
        public BusinessUnitDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            BusinessUnitDTO ret = new BusinessUnitDTO();


            ret.setFinancialBusinessUnitCode(rs.getString("financial_business_unit_code"));
            ret.setBaseCurrency(rs.getString("base_currency"));
            ret.setBusinessUnitActiveIndicator(rs.getString("business_unit_active_indicator"));
            ret.setBusinessUnitDescription(rs.getString("business_unit_description"));
            ret.setBusinessUnitEffectiveDate(rs.getDate("business_unit_effective_date"));
            ret.setBusinessUnitGermanDescription(rs.getString("business_unit_german_description"));
            ret.setBusinessUnitGermanName(rs.getString("business_unit_german_name"));
            ret.setBusinessUnitId(rs.getString("business_unit_id"));
            ret.setBusinessUnitLastUpdateDatetime(rs.getTimestamp("business_unit_last_update_datetime"));
            ret.setBusinessUnitLastUpdatedBy(rs.getString("business_unit_last_updated_by"));
            ret.setBusinessUnitName(rs.getString("business_unit_name"));
            ret.setBusinessUnitShortDescription(rs.getString("business_unit_short_description"));
            ret.setBusinessUnitShortGermanDescription(rs.getString("business_unit_short_german_description"));
            ret.setChBusinessUnitLastUpdatedBy(rs.getString("ch_business_unit_last_updated_by"));
            ret.setChBusinessUnitLastUpdatedDatetime(rs.getTimestamp("ch_business_unit_last_updated_datetime"));
            ret.setCustomerId(rs.getString("customer_id"));
            ret.setCustomerVendorAffiliateIndicator(rs.getString("customer_vendor_affiliate_indicator"));
            ret.setFinancialBusinessUnitActiveIndicator(rs.getString("financial_business_unit_active_indicator"));
            ret.setFinancialBusinessUnitEffectiveDate(rs.getDate("financial_business_unit_effective_date"));
            ret.setFinancialBusinessUnitGroupId(rs.getString("financial_business_unit_group_id"));
            ret.setFinancialBusinessUnitGroupName(rs.getString("financial_business_unit_group_name"));
            ret.setFinancialBusinessUnitName(rs.getString("financial_business_unit_name"));
            ret.setFirmType(rs.getString("firm_type"));
            ret.setIsoCurrencyCode(rs.getString("iso_currency_code"));
            ret.setLcdRefNo(rs.getString("lcd_ref_no"));
            ret.setLegalEntityShortDescription(rs.getString("legal_entity_short_description"));
            ret.setLegalEntityActiveIndicator(rs.getString("legal_entity_active_indicator"));
            ret.setLegalEntityCode(rs.getString("legal_entity_code"));
            ret.setLegalEntityDescription(rs.getString("legal_entity_description"));
            ret.setLegalEntityEffectiveDate(rs.getDate("legal_entity_effective_date"));
            ret.setLegalEntityGermanDescription(rs.getString("legal_entity_german_description"));
            ret.setLegalEntityLastUpdatedBy(rs.getString("legal_entity_last_updated_by"));
            ret.setLegalEntityLastUpdatedDatetime(rs.getTimestamp("legal_entity_last_updated_datetime"));
            ret.setLocalGaapCurrency(rs.getString("local_gaap_currency"));
            ret.setMisMemberCode(rs.getString("mis_member_code"));
            ret.setProcessingLocationCode(rs.getString("processing_location_code"));
            ret.setSourceId(rs.getString("source_id"));
            ret.setDeltaFlag(rs.getString("delta_flag"));
            ret.setBuDomicile(rs.getString("bu_domicile"));

            return ret;
        }
    }

}
