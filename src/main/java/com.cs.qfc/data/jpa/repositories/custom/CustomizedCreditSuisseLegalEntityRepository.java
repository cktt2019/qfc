package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.CreditSuisseLegalEntity;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CreditSuisseLegalEntityDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link CreditSuisseLegalEntity} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedCreditSuisseLegalEntityRepository {

    public Collection<CreditSuisseLegalEntityDTO> findByCustomCriteria(SearchInfoDTO info);

}







