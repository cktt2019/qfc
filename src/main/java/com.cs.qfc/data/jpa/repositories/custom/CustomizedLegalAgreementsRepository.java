package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.LegalAgreements;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.LegalAgreementsDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link LegalAgreements} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedLegalAgreementsRepository {

    public Collection<LegalAgreementsDTO> findByCustomCriteria(SearchInfoDTO info);

}







