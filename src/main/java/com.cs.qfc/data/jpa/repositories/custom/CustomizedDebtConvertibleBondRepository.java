package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.DebtConvertibleBond;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DebtConvertibleBondDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link DebtConvertibleBond} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedDebtConvertibleBondRepository {

    public Collection<DebtConvertibleBondDTO> findByCustomCriteria(SearchInfoDTO info);

}







