package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.MarnaG;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.MarnaGDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link MarnaG} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedMarnaGRepository {

    public Collection<MarnaGDTO> findByCustomCriteria(SearchInfoDTO info);

}







