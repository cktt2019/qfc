package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.DebtConvertibleBond;
import com.cs.qfc.data.metadata.DebtConvertibleBondMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.DebtConvertibleBondDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link DebtConvertibleBond} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedDebtConvertibleBondRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedDebtConvertibleBondRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<DebtConvertibleBondDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + DebtConvertibleBondMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new DebtConvertibleBondMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new DebtConvertibleBondMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new DebtConvertibleBondMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<DebtConvertibleBondDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new DebtConvertibleBondRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     DebtConvertibleBondMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class DebtConvertibleBondRowMapper implements RowMapper<DebtConvertibleBondDTO> {
        @Override
        public DebtConvertibleBondDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            DebtConvertibleBondDTO ret = new DebtConvertibleBondDTO();


            ret.setSedolId(rs.getString("sedol_id"));
            ret.setSecurityDescriptionLong(rs.getString("security_description_long"));
            ret.setNxtPayDate(rs.getDate("nxt_pay_date"));
            ret.setNextCallDate(rs.getString("next_call_date"));
            ret.setNextPutDate(rs.getString("next_put_date"));
            ret.setAccrualCount(rs.getString("accrual_count"));
            ret.setAggteIssue(rs.getString("aggte_issue"));
            ret.setBaseCpi(rs.getFloat("base_cpi"));
            ret.setClearingHouseDer(rs.getString("clearing_house_der"));
            ret.setBbTicker(rs.getString("bb_ticker"));
            ret.setBookEntry(rs.getString("book_entry"));
            ret.setBradyBusDayConvention(rs.getString("brady_bus_day_convention"));
            ret.setBusinessCalendar(rs.getString("business_calendar"));
            ret.setCalcTypeDes(rs.getString("calc_type_des"));
            ret.setCallDiscrete(rs.getString("call_discrete"));
            ret.setCallFeature(rs.getString("call_feature"));
            ret.setCallNotice(rs.getString("call_notice"));
            ret.setCallDate(rs.getDate("call_date"));
            ret.setCallPrice(rs.getFloat("call_price"));
            ret.setCpnFre(rs.getString("cpn_fre"));
            ret.setCpnFreqDays(rs.getFloat("cpn_freq_days"));
            ret.setCouponType(rs.getString("coupon_type"));
            ret.setCouponTypeReset(rs.getString("coupon_type_reset"));
            ret.setCpnFreqYld(rs.getString("cpn_freq_yld"));
            ret.setCsManaged(rs.getString("cs_managed"));
            ret.setIssDate(rs.getDate("iss_date"));
            ret.setDebtStatus(rs.getString("debt_status"));
            ret.setDefSecurity(rs.getString("def_security"));
            ret.setDaysToSettle(rs.getFloat("days_to_settle"));
            ret.setFwEntry(rs.getString("fw_entry"));
            ret.setFidessaZone(rs.getString("fidessa_zone"));
            ret.setFidessaCode(rs.getString("fidessa_code"));
            ret.setFirstCallDate(rs.getDate("first_call_date"));
            ret.setFirstCoupon(rs.getDate("first_coupon"));
            ret.setFixFltDayCount(rs.getFloat("fix_flt_day_count"));
            ret.setFloatRst(rs.getString("float_rst"));
            ret.setFloatRstIndx(rs.getFloat("float_rst_indx"));
            ret.setFltDaysPrior(rs.getFloat("flt_days_prior"));
            ret.setGlossK3(rs.getString("gloss_k3"));
            ret.setGlossL9(rs.getString("gloss_l9"));
            ret.setGlossM3(rs.getString("gloss_m3"));
            ret.setIdBb(rs.getString("id_bb"));
            ret.setIdBbUnique(rs.getString("id_bb_unique"));
            ret.setEpicId(rs.getString("epic_id"));
            ret.setCmuId(rs.getString("cmu_id"));
            ret.setIsinId(rs.getString("isin_id"));
            ret.setJapanId(rs.getString("japan_id"));
            ret.setMiscDomesticId(rs.getString("misc_domestic_id"));
            ret.setPoetsId(rs.getString("poets_id"));
            ret.setRicId(rs.getString("ric_id"));
            ret.setSedol2Id(rs.getString("sedol2_id"));
            ret.setSingId(rs.getString("sing_id"));
            ret.setIncomeCurrency(rs.getString("income_currency"));
            ret.setIndustrySbgprIssue(rs.getString("industry_sbgpr_issue"));
            ret.setIntRateCap(rs.getFloat("int_rate_cap"));
            ret.setIntRateFl(rs.getString("int_rate_fl"));
            ret.setBojId(rs.getString("boj_id"));
            ret.setFisecId(rs.getString("fisec_id"));
            ret.setHugoId(rs.getString("hugo_id"));
            ret.setIstarId(rs.getString("istar_id"));
            ret.setNriId(rs.getString("nri_id"));
            ret.setSmuId(rs.getString("smu_id"));
            ret.setNrtIssuerId(rs.getString("nrt_issuer_id"));
            ret.setMarginPerc(rs.getFloat("margin_perc"));
            ret.setMaturityDate(rs.getDate("maturity_date"));
            ret.setMuniMinTax(rs.getString("muni_min_tax"));
            ret.setMuniFedTax(rs.getString("muni_fed_tax"));
            ret.setStateCode(rs.getString("state_code"));
            ret.setNextReset(rs.getDate("next_reset"));
            ret.setNriIntPayClass(rs.getString("nri_int_pay_class"));
            ret.setNriCode(rs.getString("nri_code"));
            ret.setNriSecSub(rs.getString("nri_sec_sub"));
            ret.setNriSicc(rs.getString("nri_sicc"));
            ret.setOfferingTyp(rs.getString("offering_typ"));
            ret.setDatedDate(rs.getDate("dated_date"));
            ret.setPhysical(rs.getString("physical"));
            ret.setPrevPayDate(rs.getDate("prev_pay_date"));
            ret.setPrvRstDate(rs.getDate("prv_rst_date"));
            ret.setPrincFac(rs.getFloat("princ_fac"));
            ret.setPrivatePlacement(rs.getString("private_placement"));
            ret.setProRataCall(rs.getString("pro_rata_call"));
            ret.setPubBalOut(rs.getFloat("pub_bal_out"));
            ret.setPutDisc(rs.getString("put_disc"));
            ret.setPutFeature(rs.getString("put_feature"));
            ret.setPutNotice(rs.getInt("put_notice"));
            ret.setRatingsTrigger(rs.getString("ratings_trigger"));
            ret.setRefixFreq(rs.getString("refix_freq"));
            ret.setRegs(rs.getString("regs"));
            ret.setRegionCode(rs.getString("region_code"));
            ret.setResetIdx(rs.getString("reset_idx"));
            ret.setRestrictCode(rs.getString("restrict_code"));
            ret.setSecType(rs.getString("sec_type"));
            ret.setSinkFreq(rs.getString("sink_freq"));
            ret.setBenchmarkSpread(rs.getInt("benchmark_spread"));
            ret.setTickerSymbol(rs.getString("ticker_symbol"));
            ret.setTradingCurrency(rs.getString("trading_currency"));
            ret.setMnPiece(rs.getFloat("mn_piece"));
            ret.setTraxReport(rs.getString("trax_report"));
            ret.setUltMatDate(rs.getDate("ult_mat_date"));
            ret.setDesNotes(rs.getString("des_notes"));
            ret.setUltPrntCompId(rs.getString("ult_prnt_comp_id"));
            ret.setFisecIssuerId(rs.getString("fisec_issuer_id"));
            ret.setUltPrntTickerExchange(rs.getString("ult_prnt_ticker_exchange"));
            ret.setCouponFrequency(rs.getString("coupon_frequency"));
            ret.setLondonImsDescription(rs.getString("london_ims_description"));
            ret.setConRatio(rs.getInt("con_ratio"));
            ret.setUndlySecIsin(rs.getString("undly_sec_isin"));
            ret.setConvPrice(rs.getFloat("conv_price"));
            ret.setPerSecSrc01(rs.getString("per_sec_src01"));
            ret.setIssueDate(rs.getDate("issue_date"));
            ret.setSpIssuerId(rs.getString("sp_issuer_id"));
            ret.setNtpaInPosition(rs.getString("ntpa_in_position"));
            ret.setIsInternationalSukuk(rs.getString("is_international_sukuk"));
            ret.setIsDomesticSukuk(rs.getString("is_domestic_sukuk"));
            ret.setSyntheticId(rs.getString("synthetic_id"));
            ret.setPoolCusipId(rs.getString("pool_cusip_id"));
            ret.setAuctRatePfdInd(rs.getString("auct_rate_pfd_ind"));
            ret.setRepoclearFlg(rs.getString("repoclear_flg"));
            ret.setMuniLetterCreditIssuer(rs.getString("muni_letter_credit_issuer"));
            ret.setMuniCorpObligator2(rs.getString("muni_corp_obligator_2"));
            ret.setInsuranceStatus(rs.getString("insurance_status"));
            ret.setObligator(rs.getString("obligator"));
            ret.setCpn(rs.getFloat("cpn"));
            ret.setTradeCntry(rs.getString("trade_cntry"));
            ret.setCommonId(rs.getString("common_id"));
            ret.setSeries(rs.getString("series"));
            ret.setClassCode(rs.getString("class_code"));

            return ret;
        }
    }

}
