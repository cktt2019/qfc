package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.DTPMISGBLDYTREE;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DTPMISGBLDYTREEDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link DTPMISGBLDYTREE} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedDTPMISGBLDYTREERepository {

    public Collection<DTPMISGBLDYTREEDTO> findByCustomCriteria(SearchInfoDTO info);

}







