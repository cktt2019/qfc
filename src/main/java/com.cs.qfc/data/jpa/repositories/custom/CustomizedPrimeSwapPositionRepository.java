package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.PrimeSwapPosition;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.PrimeSwapPositionDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link PrimeSwapPosition} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedPrimeSwapPositionRepository {

    public Collection<PrimeSwapPositionDTO> findByCustomCriteria(SearchInfoDTO info);

}







