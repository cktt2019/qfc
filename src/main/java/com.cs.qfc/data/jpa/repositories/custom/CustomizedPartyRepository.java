package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.Party;

import java.util.Collection;

/**
 * Service interface with operations for {@link com.cs.qfc.data.jpa.entities.Party} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedPartyRepository {

    public Collection<Party> findByCustomCriteria(SearchInfoDTO info);

}







