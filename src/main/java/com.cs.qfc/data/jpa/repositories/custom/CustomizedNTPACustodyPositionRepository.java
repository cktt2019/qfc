package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.NTPACustodyPosition;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.NTPACustodyPositionDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link NTPACustodyPosition} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedNTPACustodyPositionRepository {

    public Collection<NTPACustodyPositionDTO> findByCustomCriteria(SearchInfoDTO info);

}







