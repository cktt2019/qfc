package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.LoanIQ;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.LoanIQDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link LoanIQ} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedLoanIQRepository {

    public Collection<LoanIQDTO> findByCustomCriteria(SearchInfoDTO info);

}







