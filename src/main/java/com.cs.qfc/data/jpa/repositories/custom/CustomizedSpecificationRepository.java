package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.Specification;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.SpecificationDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link Specification} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedSpecificationRepository {

    public Collection<SpecificationDTO> findByCustomCriteria(SearchInfoDTO info);

}







