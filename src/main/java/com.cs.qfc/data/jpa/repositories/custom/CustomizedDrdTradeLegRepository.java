package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.DrdTradeLeg;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DrdTradeLegDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link DrdTradeLeg} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedDrdTradeLegRepository {

    public Collection<DrdTradeLegDTO> findByCustomCriteria(SearchInfoDTO info);

}







