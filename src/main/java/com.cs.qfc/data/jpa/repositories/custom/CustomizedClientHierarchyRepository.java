package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.ClientHierarchy;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.ClientHierarchyDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link ClientHierarchy} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedClientHierarchyRepository {

    public Collection<ClientHierarchyDTO> findByCustomCriteria(SearchInfoDTO info);

}







