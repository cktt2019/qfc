package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.DrdTradeUnderlying;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.DrdTradeUnderlyingDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link DrdTradeUnderlying} persistence.
 */

public interface CustomizedDrdTradeUnderlyingRepository {

    public Collection<DrdTradeUnderlyingDTO> findByCustomCriteria(SearchInfoDTO info);

}







