package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.EquityConvertibleBond;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.EquityConvertibleBondDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link EquityConvertibleBond} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedEquityConvertibleBondRepository {

    public Collection<EquityConvertibleBondDTO> findByCustomCriteria(SearchInfoDTO info);

}







