package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.DrdTradeInstrument;
import com.cs.qfc.data.metadata.DrdTradeInstrumentMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.DrdTradeInstrumentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link DrdTradeInstrument} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedDrdTradeInstrumentRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedDrdTradeInstrumentRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<DrdTradeInstrumentDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + DrdTradeInstrumentMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new DrdTradeInstrumentMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new DrdTradeInstrumentMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new DrdTradeInstrumentMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<DrdTradeInstrumentDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new DrdTradeInstrumentRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     DrdTradeInstrumentMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class DrdTradeInstrumentRowMapper implements RowMapper<DrdTradeInstrumentDTO> {
        @Override
        public DrdTradeInstrumentDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            DrdTradeInstrumentDTO ret = new DrdTradeInstrumentDTO();


            ret.setId(rs.getInt("id"));
            ret.setCobDate(rs.getDate("cob_date"));
            ret.setGbmEntity(rs.getString("gbm_entity"));
            ret.setTradeId(rs.getInt("trade_id"));
            ret.setCounterPartyId(rs.getString("counter_party_id"));
            ret.setBookName(rs.getString("book_name"));
            ret.setAssetClass(rs.getString("asset_class"));
            ret.setCduMasterAgreementLlk(rs.getString("cdu_master_agreement_llk"));
            ret.setCduCollateralAnnexLlk(rs.getString("cdu_collateral_annex_llk"));
            ret.setFramesoftMasterAgreementLlk(rs.getString("framesoft_master_agreement_llk"));
            ret.setAlgoCollateralAnnexLlk(rs.getString("algo_collateral_annex_llk"));
            ret.setTradeDate(rs.getDate("trade_date"));
            ret.setPrimaryMaturityDate(rs.getDate("primary_maturity_date"));
            ret.setLocalPvCcyCode(rs.getString("local_pv_ccy_code"));
            ret.setLocalPv(rs.getInt("local_pv"));

            return ret;
        }
    }

}
