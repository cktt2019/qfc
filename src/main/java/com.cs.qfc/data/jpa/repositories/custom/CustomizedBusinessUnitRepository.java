package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.BusinessUnit;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.BusinessUnitDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link BusinessUnit} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedBusinessUnitRepository {

    public Collection<BusinessUnitDTO> findByCustomCriteria(SearchInfoDTO info);

}







