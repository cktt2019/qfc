package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.Portfolio;
import com.cs.qfc.data.metadata.PortfolioMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.PortfolioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link Portfolio} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedPortfolioRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedPortfolioRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<PortfolioDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + PortfolioMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new PortfolioMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new PortfolioMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new PortfolioMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<PortfolioDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new PortfolioRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     PortfolioMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class PortfolioRowMapper implements RowMapper<PortfolioDTO> {
        @Override
        public PortfolioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            PortfolioDTO ret = new PortfolioDTO();


            ret.setPortfolioId(rs.getString("portfolio_id"));
            ret.setGlobalPartyId(rs.getString("global_party_id"));
            ret.setPartytypeDescription(rs.getString("partytype_description"));
            ret.setLegalName(rs.getString("legal_name"));
            ret.setPartyAliasName(rs.getString("party_alias_name"));
            ret.setLegalRoleDefinitionId(rs.getString("legal_role_definition_id"));
            ret.setLrdRoleType(rs.getString("lrd_role_type"));
            ret.setLrdRoleDescription(rs.getString("lrd_role_description"));
            ret.setLrdRoleCode(rs.getString("lrd_role_code"));
            ret.setPartyLegalRoleId(rs.getString("party_legal_role_id"));
            ret.setSourceId(rs.getString("source_id"));
            ret.setSourceName(rs.getString("source_name"));
            ret.setInternalPartyAltId(rs.getString("internal_party_alt_id"));

            return ret;
        }
    }

}
