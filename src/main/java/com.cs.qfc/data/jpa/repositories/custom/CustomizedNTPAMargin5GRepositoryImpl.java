package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.NTPAMargin5G;
import com.cs.qfc.data.metadata.NTPAMargin5GMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.NTPAMargin5GDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link NTPAMargin5G} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedNTPAMargin5GRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedNTPAMargin5GRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<NTPAMargin5GDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + NTPAMargin5GMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new NTPAMargin5GMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new NTPAMargin5GMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new NTPAMargin5GMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<NTPAMargin5GDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new NTPAMargin5GRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     NTPAMargin5GMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class NTPAMargin5GRowMapper implements RowMapper<NTPAMargin5GDTO> {
        @Override
        public NTPAMargin5GDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            NTPAMargin5GDTO ret = new NTPAMargin5GDTO();


            ret.setBusinessDate(rs.getDate("business_date"));
            ret.setRunDate(rs.getString("run_date"));
            ret.setAccount(rs.getString("account"));
            ret.setCurrId(rs.getString("curr_id"));
            ret.setLiquidatingEquity(rs.getString("liquidating_equity"));
            ret.setMarginEquity(rs.getString("margin_equity"));
            ret.setRegTRequirements(rs.getString("reg_t_requirements"));
            ret.setMaintRequirements(rs.getString("maint_requirements"));
            ret.setNyseRequirements(rs.getString("nyse_requirements"));
            ret.setRegtExcessDeficit(rs.getString("regt_excess_deficit"));
            ret.setMaintExcessDeficit(rs.getString("maint_excess_deficit"));
            ret.setNyseExcessDeficit(rs.getString("nyse_excess_deficit"));
            ret.setSma(rs.getString("sma"));
            ret.setCashAvailable(rs.getString("cash_available"));
            ret.setTotalTdBalance(rs.getString("total_td_balance"));
            ret.setTotalTdLongMktValue(rs.getString("total_td_long_mkt_value"));
            ret.setTotalTdShortMktValue(rs.getString("total_td_short_mkt_value"));
            ret.setTotalOptionLongMktValue(rs.getString("total_option_long_mkt_value"));
            ret.setTotalOptionShortMktValue(rs.getString("total_option_short_mkt_value"));
            ret.setTotalSdBalance(rs.getString("total_sd_balance"));
            ret.setTotalSdLongMktValue(rs.getString("total_sd_long_mkt_value"));
            ret.setTotalSdShortMktValue(rs.getString("total_sd_short_mkt_value"));
            ret.setTotalAssessedBalance(rs.getString("total_assessed_balance"));
            ret.setTotalInterestAccrual(rs.getString("total_interest_accrual"));
            ret.setType(rs.getString("type"));
            ret.setTdBalance(rs.getString("td_balance"));
            ret.setTdMarketValLng(rs.getString("td_market_val_lng"));
            ret.setTdMarketValShrt(rs.getString("td_market_val_shrt"));
            ret.setSdBalance(rs.getString("sd_balance"));
            ret.setSdMarketValue(rs.getString("sd_market_value"));
            ret.setLegalEntityId(rs.getString("legal_entity_id"));
            ret.setNtpaProductId(rs.getString("ntpa_product_id"));
            ret.setId(rs.getInt("id"));

            return ret;
        }
    }

}
