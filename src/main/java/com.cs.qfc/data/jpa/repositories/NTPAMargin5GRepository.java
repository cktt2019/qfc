package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.NTPAMargin5G;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link NTPAMargin5G} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface NTPAMargin5GRepository extends JpaRepository<NTPAMargin5G, Integer> {


    public List<NTPAMargin5G> findByBusinessDate(java.sql.Date businessDate);

    public List<NTPAMargin5G> findByRunDate(String runDate);

    public List<NTPAMargin5G> findByAccount(String account);

    public List<NTPAMargin5G> findByCurrId(String currId);

    public List<NTPAMargin5G> findByLiquidatingEquity(String liquidatingEquity);

    public List<NTPAMargin5G> findByMarginEquity(String marginEquity);

    public List<NTPAMargin5G> findByRegTRequirements(String regTRequirements);

    public List<NTPAMargin5G> findByMaintRequirements(String maintRequirements);

    public List<NTPAMargin5G> findByNyseRequirements(String nyseRequirements);

    public List<NTPAMargin5G> findByRegtExcessDeficit(String regtExcessDeficit);

    public List<NTPAMargin5G> findByMaintExcessDeficit(String maintExcessDeficit);

    public List<NTPAMargin5G> findByNyseExcessDeficit(String nyseExcessDeficit);

    public List<NTPAMargin5G> findBySma(String sma);

    public List<NTPAMargin5G> findByCashAvailable(String cashAvailable);

    public List<NTPAMargin5G> findByTotalTdBalance(String totalTdBalance);

    public List<NTPAMargin5G> findByTotalTdLongMktValue(String totalTdLongMktValue);

    public List<NTPAMargin5G> findByTotalTdShortMktValue(String totalTdShortMktValue);

    public List<NTPAMargin5G> findByTotalOptionLongMktValue(String totalOptionLongMktValue);

    public List<NTPAMargin5G> findByTotalOptionShortMktValue(String totalOptionShortMktValue);

    public List<NTPAMargin5G> findByTotalSdBalance(String totalSdBalance);

    public List<NTPAMargin5G> findByTotalSdLongMktValue(String totalSdLongMktValue);

    public List<NTPAMargin5G> findByTotalSdShortMktValue(String totalSdShortMktValue);

    public List<NTPAMargin5G> findByTotalAssessedBalance(String totalAssessedBalance);

    public List<NTPAMargin5G> findByTotalInterestAccrual(String totalInterestAccrual);

    public List<NTPAMargin5G> findByType(String type);

    public List<NTPAMargin5G> findByTdBalance(String tdBalance);

    public List<NTPAMargin5G> findByTdMarketValLng(String tdMarketValLng);

    public List<NTPAMargin5G> findByTdMarketValShrt(String tdMarketValShrt);

    public List<NTPAMargin5G> findBySdBalance(String sdBalance);

    public List<NTPAMargin5G> findBySdMarketValue(String sdMarketValue);

    public List<NTPAMargin5G> findByLegalEntityId(String legalEntityId);

    public List<NTPAMargin5G> findByNtpaProductId(String ntpaProductId);


    public Page<NTPAMargin5G> findAll(Pageable page);


}







