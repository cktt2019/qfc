package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link Department} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface DepartmentRepository extends JpaRepository<Department, String> {


    public List<Department> findByBusinessControllerId(String businessControllerId);

    public List<Department> findByControllerId(String controllerId);

    public List<Department> findByDepartmentCategoryCode(String departmentCategoryCode);

    public List<Department> findByDepartmentCategoryName(String departmentCategoryName);

    public List<Department> findByDepartmentDormantIndicator(String departmentDormantIndicator);

    public List<Department> findByDepartmentLongName(String departmentLongName);

    public List<Department> findByDepartmentName(String departmentName);

    public List<Department> findByDepartmentReconcilerGroupName(String departmentReconcilerGroupName);

    public List<Department> findByDepartmentStatus(String departmentStatus);

    public List<Department> findByDepartmentTraderId(String departmentTraderId);

    public List<Department> findByDepartmentValidFromDate(java.sql.Date departmentValidFromDate);

    public List<Department> findByDepartmentValidToDate(java.sql.Date departmentValidToDate);

    public List<Department> findByFinancialBusinessUnitCode(String financialBusinessUnitCode);

    public List<Department> findByIvrGroup(String ivrGroup);

    public List<Department> findByL1SupervisorId(String l1SupervisorId);

    public List<Department> findByMisOfficeCode(String misOfficeCode);

    public List<Department> findByMisOfficeName(String misOfficeName);

    public List<Department> findByOwnerL2Id(String ownerL2Id);

    //public List<Department> findByRecAndAdjControllerId (String recAndAdjControllerId);
    //public List<Department> findByRecAndAdjSupervisorId (String recAndAdjSupervisorId);
    public List<Department> findByReconcilerId(String reconcilerId);

    public List<Department> findByRegionCode(String regionCode);

    public List<Department> findByRegionDescription(String regionDescription);

    public List<Department> findByRevenueType(String revenueType);

    public List<Department> findByRevenueTypeDescription(String revenueTypeDescription);

    public List<Department> findByRiskPortfolioId(String riskPortfolioId);

    public List<Department> findBySpecialisationIndicator(String specialisationIndicator);

    public List<Department> findByTaxCategory(String taxCategory);

    public List<Department> findByVolkerRuleDescription(String volkerRuleDescription);

    public List<Department> findByVolkerRuleIndicator(String volkerRuleIndicator);

    public List<Department> findByFxCentrallyMaangedName(String fxCentrallyMaangedName);

    public List<Department> findByVolckerSubDivision(String volckerSubDivision);

    public List<Department> findByVolckerSubDivisionDescription(String volckerSubDivisionDescription);

    public List<Department> findByIhcHcdFlag(String ihcHcdFlag);

    public List<Department> findByCoveredDeptFlag(String coveredDeptFlag);

    public List<Department> findByDeltaFlag(String deltaFlag);

    public List<Department> findByRemittancePolicyCode(String remittancePolicyCode);

    public List<Department> findByFirstInstanceDeptActivated(String firstInstanceDeptActivated);

    public List<Department> findByFirstInstanceDeptInactivated(String firstInstanceDeptInactivated);

    public List<Department> findByLastInstanceDeptActivated(String lastInstanceDeptActivated);

    public List<Department> findByLastInstanceDeptInactivated(String lastInstanceDeptInactivated);

    public List<Department> findByRbpnlFlag(String rbpnlFlag);

    public List<Department> findByIvrGroupLongName(String ivrGroupLongName);

    public List<Department> findByFrtbDeptCategory(String frtbDeptCategory);


    public Page<Department> findAll(Pageable page);


}







