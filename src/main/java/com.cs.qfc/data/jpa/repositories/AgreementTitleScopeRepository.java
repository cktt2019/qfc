package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.AgreementTitleScope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link AgreementTitleScope} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface AgreementTitleScopeRepository extends JpaRepository<AgreementTitleScope, Integer> {


    public List<AgreementTitleScope> findByAgreementTitle(String agreementTitle);

    public List<AgreementTitleScope> findByAgreementType(String agreementType);

    public List<AgreementTitleScope> findByInScope(Integer inScope);


    public Page<AgreementTitleScope> findAll(Pageable page);


}







