package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.DrdTradeLeg;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link DrdTradeLeg} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface DrdTradeLegRepository extends JpaRepository<DrdTradeLeg, Integer> {


    public List<DrdTradeLeg> findByCobDate(java.sql.Date cobDate);

    public List<DrdTradeLeg> findByGbmEntity(String gbmEntity);

    public List<DrdTradeLeg> findByTradeId(Integer tradeId);

    public List<DrdTradeLeg> findByCounterPartyId(String counterPartyId);

    public List<DrdTradeLeg> findByBookName(String bookName);

    public List<DrdTradeLeg> findByCduMasterAgreementLlk(String cduMasterAgreementLlk);

    public List<DrdTradeLeg> findByCduCollateralAnnexLlk(String cduCollateralAnnexLlk);

    public List<DrdTradeLeg> findByFramesoftMasterAgreementLlk(String framesoftMasterAgreementLlk);

    public List<DrdTradeLeg> findByAlgoCollateralAnnexLlk(String algoCollateralAnnexLlk);

    public List<DrdTradeLeg> findByTradeDate(java.sql.Date tradeDate);

    public List<DrdTradeLeg> findByPrimaryMaturityDate(java.sql.Date primaryMaturityDate);

    public List<DrdTradeLeg> findByLocalPvCcyCode(String localPvCcyCode);

    public List<DrdTradeLeg> findByLocalPv(Integer localPv);

    public List<DrdTradeLeg> findByPvNotional(Float pvNotional);


    public Page<DrdTradeLeg> findAll(Pageable page);


}







