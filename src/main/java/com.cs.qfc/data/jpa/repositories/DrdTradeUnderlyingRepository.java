package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.DrdTradeUnderlying;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Service interface with operations for {@link DrdTradeUnderlying} persistence.
 */
@Repository
public interface DrdTradeUnderlyingRepository extends JpaRepository<DrdTradeUnderlying, Integer> {


    public List<DrdTradeUnderlying> findByCobDate(java.sql.Date cobDate);

    public List<DrdTradeUnderlying> findByLoadCobDate(java.sql.Date loadCobDate);

    public List<DrdTradeUnderlying> findByLoadBook(String loadBook);

    public List<DrdTradeUnderlying> findByBookName(String bookName);

    public List<DrdTradeUnderlying> findByTradeLoadId(Integer tradeLoadId);

    public List<DrdTradeUnderlying> findByLoadTimestamp(Date loadTimestamp);

    public List<DrdTradeUnderlying> findBySnapshotName(String snapshotName);

    public List<DrdTradeUnderlying> findBySnapshotId(Integer snapshotId);

    public List<DrdTradeUnderlying> findBySnapshotCreationDatetime(Date snapshotCreationDatetime);

    public List<DrdTradeUnderlying> findByInterfaceVersion(String interfaceVersion);

    public List<DrdTradeUnderlying> findByTradeId(Integer tradeId);

    public List<DrdTradeUnderlying> findByTftTradeVersion(Integer tftTradeVersion);

    public List<DrdTradeUnderlying> findByTradeLocationId(Integer tradeLocationId);

    public List<DrdTradeUnderlying> findByPrimaryTcn(String primaryTcn);

    public List<DrdTradeUnderlying> findByTcn(String tcn);

    public List<DrdTradeUnderlying> findByTradeVersionUpdateTime(Date tradeVersionUpdateTime);

    public List<DrdTradeUnderlying> findByCancellationDate(String cancellationDate);

    public List<DrdTradeUnderlying> findByGbmEntity(String gbmEntity);

    public List<DrdTradeUnderlying> findByApprovalStatus(String approvalStatus);

    public List<DrdTradeUnderlying> findByEventReason(String eventReason);

    public List<DrdTradeUnderlying> findByExternalId(String externalId);

    public List<DrdTradeUnderlying> findByPrimaryMaturityDate(java.sql.Date primaryMaturityDate);

    public List<DrdTradeUnderlying> findByProductSubType(String productSubType);

    public List<DrdTradeUnderlying> findByProductType(String productType);

    public List<DrdTradeUnderlying> findByProductValuationType(String productValuationType);

    public List<DrdTradeUnderlying> findByTradeStatus(String tradeStatus);

    public List<DrdTradeUnderlying> findByUnderlyingId(String underlyingId);

    public List<DrdTradeUnderlying> findByUnderlyingIdType(String underlyingIdType);

    public List<DrdTradeUnderlying> findByAccountCcy(String accountCcy);

    public List<DrdTradeUnderlying> findByBasketStrike(Float basketStrike);

    public List<DrdTradeUnderlying> findByBasketWeighting(String basketWeighting);

    public List<DrdTradeUnderlying> findByCoupon(Float coupon);

    public List<DrdTradeUnderlying> findByCouponFreq(String couponFreq);

    public List<DrdTradeUnderlying> findByFinalPrice(Float finalPrice);

    public List<DrdTradeUnderlying> findByFloatingRateIndex(String floatingRateIndex);

    public List<DrdTradeUnderlying> findByFloatingRateIndexTenor(String floatingRateIndexTenor);

    public List<DrdTradeUnderlying> findByFloatingRateIndexType(String floatingRateIndexType);

    public List<DrdTradeUnderlying> findByInitialPrice(Float initialPrice);

    public List<DrdTradeUnderlying> findByRefObligCategory(String refObligCategory);

    public List<DrdTradeUnderlying> findByRefPoolLongName(String refPoolLongName);

    public List<DrdTradeUnderlying> findByRefPoolShortName(String refPoolShortName);

    public List<DrdTradeUnderlying> findBySequenceId(Integer sequenceId);

    public List<DrdTradeUnderlying> findByUnderlyingAssetClass(String underlyingAssetClass);

    public List<DrdTradeUnderlying> findByUnderlyingCcy(String underlyingCcy);

    public List<DrdTradeUnderlying> findByUnderlyingDesc(String underlyingDesc);

    public List<DrdTradeUnderlying> findByUnderlyingExtId(String underlyingExtId);

    public List<DrdTradeUnderlying> findByUnderlyingExtIdType(String underlyingExtIdType);

    public List<DrdTradeUnderlying> findByUnderlyingMatDate(java.sql.Date underlyingMatDate);

    public List<DrdTradeUnderlying> findByUnderlyingRiskId(String underlyingRiskId);

    public List<DrdTradeUnderlying> findByPayRec(String payRec);

    public List<DrdTradeUnderlying> findBySettlementType(String settlementType);

    public List<DrdTradeUnderlying> findByBusinessGroup(String businessGroup);

    public List<DrdTradeUnderlying> findByValuationClass(String valuationClass);

    public List<DrdTradeUnderlying> findByValuationType(String valuationType);

    public List<DrdTradeUnderlying> findByTradeSourceSystem(String tradeSourceSystem);

    public List<DrdTradeUnderlying> findByUnderlyingPriceSource(String underlyingPriceSource);

    public List<DrdTradeUnderlying> findByCduMasterAgreementLlk(String cduMasterAgreementLlk);

    public List<DrdTradeUnderlying> findByCduCollateralAnnexLlk(String cduCollateralAnnexLlk);

    public List<DrdTradeUnderlying> findByFramesoftMasterAgreementLlk(Integer framesoftMasterAgreementLlk);

    public List<DrdTradeUnderlying> findByAlgoCollateralAnnexLlk(Integer algoCollateralAnnexLlk);

    public List<DrdTradeUnderlying> findByGbmBookRef(String gbmBookRef);

    public List<DrdTradeUnderlying> findByUnderlyingProductId(String underlyingProductId);

    public List<DrdTradeUnderlying> findByUnderlyingProductType(String underlyingProductType);

    public List<DrdTradeUnderlying> findByUnderlyingRiskType(String underlyingRiskType);

    public List<DrdTradeUnderlying> findByBankruptcy(String bankruptcy);

    public List<DrdTradeUnderlying> findByDelivObligAcceleratedMatured(String delivObligAcceleratedMatured);

    public List<DrdTradeUnderlying> findByDelivObligAccruedInterest(String delivObligAccruedInterest);

    public List<DrdTradeUnderlying> findByDelivObligAssignableLoan(String delivObligAssignableLoan);

    public List<DrdTradeUnderlying> findByDelivObligCategory(String delivObligCategory);

    public List<DrdTradeUnderlying> findByDelivObligConsentRequiredLoan(String delivObligConsentRequiredLoan);

    public List<DrdTradeUnderlying> findByDelivOblgDirectLoanPrtCptn(String delivOblgDirectLoanPrtCptn);

    public List<DrdTradeUnderlying> findByDelivObligListed(String delivObligListed);

    public List<DrdTradeUnderlying> findByDelivObligMaximumMaturity(Integer delivObligMaximumMaturity);

    public List<DrdTradeUnderlying> findByDelivObligMaxMaturitySpecified(String delivObligMaxMaturitySpecified);

    public List<DrdTradeUnderlying> findByDelivObligNotBearer(String delivObligNotBearer);

    public List<DrdTradeUnderlying> findByDelivObligNotContingent(String delivObligNotContingent);

    public List<DrdTradeUnderlying> findByDelivObligNotDomesticCurrency(String delivObligNotDomesticCurrency);

    public List<DrdTradeUnderlying> findByDelivObligNotDomesticIssuance(String delivObligNotDomesticIssuance);

    public List<DrdTradeUnderlying> findByDelivObligNotDomesticLaw(String delivObligNotDomesticLaw);

    public List<DrdTradeUnderlying> findByDelivObligNotSovereignLender(String delivObligNotSovereignLender);

    public List<DrdTradeUnderlying> findByDelivObligPhyParipassu(String delivObligPhyParipassu);

    public List<DrdTradeUnderlying> findByDelivObligSpecifiedCcyList(String delivObligSpecifiedCcyList);

    public List<DrdTradeUnderlying> findByDelivObligSpecifiedCcyType(String delivObligSpecifiedCcyType);

    public List<DrdTradeUnderlying> findByDelivObligTransferable(String delivObligTransferable);

    public List<DrdTradeUnderlying> findByEntityType(String entityType);

    public List<DrdTradeUnderlying> findByPrimaryObligor(String primaryObligor);

    public List<DrdTradeUnderlying> findByRecoveryValue(Float recoveryValue);

    public List<DrdTradeUnderlying> findByReferenceEntityRedCode(String referenceEntityRedCode);

    public List<DrdTradeUnderlying> findByReferenceEntityUniqueEntityId(String referenceEntityUniqueEntityId);

    public List<DrdTradeUnderlying> findByReferenceObligationBloombergId(String referenceObligationBloombergId);

    public List<DrdTradeUnderlying> findByReferenceObligationCusip(String referenceObligationCusip);

    public List<DrdTradeUnderlying> findByReferenceObligationGuarantor(String referenceObligationGuarantor);

    public List<DrdTradeUnderlying> findByReferenceObligationIsin(String referenceObligationIsin);

    public List<DrdTradeUnderlying> findByReferencePrice(Float referencePrice);

    public List<DrdTradeUnderlying> findByRefObligorCouponRate(String refObligorCouponRate);

    public List<DrdTradeUnderlying> findByRefObligorMaturityDate(java.sql.Date refObligorMaturityDate);

    public List<DrdTradeUnderlying> findByRefObligorNotionalAmt(Float refObligorNotionalAmt);

    public List<DrdTradeUnderlying> findByRefObligorNotionalAmtCcy(String refObligorNotionalAmtCcy);

    public List<DrdTradeUnderlying> findByRefObligorObligationCategory(String refObligorObligationCategory);

    public List<DrdTradeUnderlying> findByValUnwind(Float valUnwind);


    public Page<DrdTradeUnderlying> findAll(Pageable page);


}







