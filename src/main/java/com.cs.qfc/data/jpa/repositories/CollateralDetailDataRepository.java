package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.CollateralDetailData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link CollateralDetailData} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface CollateralDetailDataRepository extends JpaRepository<CollateralDetailData, Integer> {


    public List<CollateralDetailData> findBySourceTxnId(String sourceTxnId);

    public List<CollateralDetailData> findByA41AsOfDate(String a41AsOfDate);

    public List<CollateralDetailData> findByA42RecordsEntityIdentifier(String a42RecordsEntityIdentifier);

    public List<CollateralDetailData> findByA43collateraoPostedReceivedFlag(String a43collateraoPostedReceivedFlag);

    public List<CollateralDetailData> findByA44CounterpartyIdentifier(String a44CounterpartyIdentifier);

    public List<CollateralDetailData> findByA45NettingAgreementIdentifier(String a45NettingAgreementIdentifier);

    public List<CollateralDetailData> findByA46UniqueCollateralItemIdentifier(String a46UniqueCollateralItemIdentifier);

    public List<CollateralDetailData> findByA47OriginalFaceAmtCollateralItemLocalCurr(String a47OriginalFaceAmtCollateralItemLocalCurr);

    public List<CollateralDetailData> findByA48LocalCurrencyOfCollateralItem(String a48LocalCurrencyOfCollateralItem);

    public List<CollateralDetailData> findByA49MarketValueAmountOfCollateralItemInUsd(String a49MarketValueAmountOfCollateralItemInUsd);

    public List<CollateralDetailData> findByA410DescriptionOfCollateralItem(String a410DescriptionOfCollateralItem);

    public List<CollateralDetailData> findByA411AssetClassification(String a411AssetClassification);

    public List<CollateralDetailData> findByA412CollateralPortfolioSegregationStatus(String a412CollateralPortfolioSegregationStatus);

    public List<CollateralDetailData> findByA413CollateralLocation(String a413CollateralLocation);

    public List<CollateralDetailData> findByA414CollateralJurisdiction(String a414CollateralJurisdiction);

    public List<CollateralDetailData> findByA415CollateralRehypoAllowed(String a415CollateralRehypoAllowed);


    public Page<CollateralDetailData> findAll(Pageable page);


}







