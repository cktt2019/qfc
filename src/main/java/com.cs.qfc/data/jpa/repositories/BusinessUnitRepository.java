package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.BusinessUnit;
import com.cs.qfc.data.jpa.entities.BusinessUnitKey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link BusinessUnit} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface BusinessUnitRepository extends JpaRepository<BusinessUnit, BusinessUnitKey> {


    public List<BusinessUnit> findByBaseCurrency(String baseCurrency);

    public List<BusinessUnit> findByBusinessUnitActiveIndicator(String businessUnitActiveIndicator);

    public List<BusinessUnit> findByBusinessUnitDescription(String businessUnitDescription);

    public List<BusinessUnit> findByBusinessUnitEffectiveDate(java.sql.Date businessUnitEffectiveDate);

    public List<BusinessUnit> findByBusinessUnitGermanDescription(String businessUnitGermanDescription);

    public List<BusinessUnit> findByBusinessUnitGermanName(String businessUnitGermanName);

    public List<BusinessUnit> findByBusinessUnitLastUpdateDatetime(java.sql.Timestamp businessUnitLastUpdateDatetime);

    public List<BusinessUnit> findByBusinessUnitLastUpdatedBy(String businessUnitLastUpdatedBy);

    public List<BusinessUnit> findByBusinessUnitName(String businessUnitName);

    public List<BusinessUnit> findByBusinessUnitShortDescription(String businessUnitShortDescription);

    public List<BusinessUnit> findByBusinessUnitShortGermanDescription(String businessUnitShortGermanDescription);

    public List<BusinessUnit> findByChBusinessUnitLastUpdatedBy(String chBusinessUnitLastUpdatedBy);

    public List<BusinessUnit> findByChBusinessUnitLastUpdatedDatetime(java.sql.Timestamp chBusinessUnitLastUpdatedDatetime);

    public List<BusinessUnit> findByCustomerId(String customerId);

    public List<BusinessUnit> findByCustomerVendorAffiliateIndicator(String customerVendorAffiliateIndicator);

    public List<BusinessUnit> findByFinancialBusinessUnitActiveIndicator(String financialBusinessUnitActiveIndicator);

    public List<BusinessUnit> findByFinancialBusinessUnitEffectiveDate(java.sql.Date financialBusinessUnitEffectiveDate);

    public List<BusinessUnit> findByFinancialBusinessUnitGroupId(String financialBusinessUnitGroupId);

    public List<BusinessUnit> findByFinancialBusinessUnitGroupName(String financialBusinessUnitGroupName);

    public List<BusinessUnit> findByFinancialBusinessUnitName(String financialBusinessUnitName);

    public List<BusinessUnit> findByFirmType(String firmType);

    public List<BusinessUnit> findByIsoCurrencyCode(String isoCurrencyCode);

    public List<BusinessUnit> findByLcdRefNo(String lcdRefNo);

    public List<BusinessUnit> findByLegalEntityShortDescription(String legalEntityShortDescription);

    public List<BusinessUnit> findByLegalEntityActiveIndicator(String legalEntityActiveIndicator);

    public List<BusinessUnit> findByLegalEntityCode(String legalEntityCode);

    public List<BusinessUnit> findByLegalEntityDescription(String legalEntityDescription);

    public List<BusinessUnit> findByLegalEntityEffectiveDate(java.sql.Date legalEntityEffectiveDate);

    public List<BusinessUnit> findByLegalEntityGermanDescription(String legalEntityGermanDescription);

    public List<BusinessUnit> findByLegalEntityLastUpdatedBy(String legalEntityLastUpdatedBy);

    public List<BusinessUnit> findByLegalEntityLastUpdatedDatetime(java.sql.Timestamp legalEntityLastUpdatedDatetime);

    public List<BusinessUnit> findByLocalGaapCurrency(String localGaapCurrency);

    public List<BusinessUnit> findByMisMemberCode(String misMemberCode);

    public List<BusinessUnit> findByProcessingLocationCode(String processingLocationCode);

    public List<BusinessUnit> findBySourceId(String sourceId);

    public List<BusinessUnit> findByDeltaFlag(String deltaFlag);

    public List<BusinessUnit> findByBuDomicile(String buDomicile);


    public Page<BusinessUnit> findAll(Pageable page);


}







