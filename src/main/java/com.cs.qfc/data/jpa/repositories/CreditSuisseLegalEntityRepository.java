package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.CreditSuisseLegalEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link CreditSuisseLegalEntity} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface CreditSuisseLegalEntityRepository extends JpaRepository<CreditSuisseLegalEntity, Integer> {


    public List<CreditSuisseLegalEntity> findByEnterpriseDNumber(Integer enterpriseDNumber);

    public List<CreditSuisseLegalEntity> findByBookingEntityNm(String bookingEntityNm);

    public List<CreditSuisseLegalEntity> findByPeopleSoftId(String peopleSoftId);

    public List<CreditSuisseLegalEntity> findByCsidId(String csidId);


    public Page<CreditSuisseLegalEntity> findAll(Pageable page);


}







