package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.LoanIQ;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link LoanIQ} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface LoanIQRepository extends JpaRepository<LoanIQ, Integer> {


    public List<LoanIQ> findByCptyCsid(String cptyCsid);

    public List<LoanIQ> findBySdsId(String sdsId);

    public List<LoanIQ> findByPortfolioCode(String portfolioCode);

    public List<LoanIQ> findByAgreementId(String agreementId);

    public List<LoanIQ> findByTradeDate(java.sql.Date tradeDate);

    public List<LoanIQ> findByMaturityDate(String maturityDate);

    public List<LoanIQ> findByCurrency(String currency);

    public List<LoanIQ> findByPresentValue(Float presentValue);

    public List<LoanIQ> findByTradeAmount(Float tradeAmount);

    public List<LoanIQ> findByRecordType(String recordType);

    public List<LoanIQ> findBySwapId(String swapId);


    public Page<LoanIQ> findAll(Pageable page);


}







