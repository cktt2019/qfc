package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.EquityInstrument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link EquityInstrument} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface EquityInstrumentRepository extends JpaRepository<EquityInstrument, String> {


    public List<EquityInstrument> findByIsinId(String isinId);

    public List<EquityInstrument> findBySedolId(String sedolId);

    public List<EquityInstrument> findByCusipId(String cusipId);

    public List<EquityInstrument> findBySecurityDescriptionLong(String securityDescriptionLong);


    public Page<EquityInstrument> findAll(Pageable page);


}







