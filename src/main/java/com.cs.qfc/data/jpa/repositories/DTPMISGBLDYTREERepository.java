package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.DTPMISGBLDYTREE;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link DTPMISGBLDYTREE} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface DTPMISGBLDYTREERepository extends JpaRepository<DTPMISGBLDYTREE, String> {


    public List<DTPMISGBLDYTREE> findByAmericasProductLineHeadId(String americasProductLineHeadId);

    public List<DTPMISGBLDYTREE> findByAmericasClusterHeadId(String americasClusterHeadId);

    public List<DTPMISGBLDYTREE> findByApacClusterHeadId(String apacClusterHeadId);

    public List<DTPMISGBLDYTREE> findByApacProductLineHeadId(String apacProductLineHeadId);

    public List<DTPMISGBLDYTREE> findByBusinessControllerL2Id(String businessControllerL2Id);

    public List<DTPMISGBLDYTREE> findByCluster(String cluster);

    public List<DTPMISGBLDYTREE> findByClusterCode(String clusterCode);

    public List<DTPMISGBLDYTREE> findByClusterHeadId(String clusterHeadId);

    public List<DTPMISGBLDYTREE> findByDesk(String desk);

    public List<DTPMISGBLDYTREE> findByDeskCode(String deskCode);

    public List<DTPMISGBLDYTREE> findByDivision(String division);

    public List<DTPMISGBLDYTREE> findByDivisionCode(String divisionCode);

    public List<DTPMISGBLDYTREE> findByEuroClusterHeadId(String euroClusterHeadId);

    public List<DTPMISGBLDYTREE> findByEuroProductLineHeadId(String euroProductLineHeadId);

    public List<DTPMISGBLDYTREE> findByGlobalProductLineHeadId(String globalProductLineHeadId);

    public List<DTPMISGBLDYTREE> findByHierarchyTraderId(String hierarchyTraderId);

    public List<DTPMISGBLDYTREE> findByMisHierarchySortCode(String misHierarchySortCode);

    public List<DTPMISGBLDYTREE> findByMisMemberName(String misMemberName);

    public List<DTPMISGBLDYTREE> findByMisMemberTypeCode(String misMemberTypeCode);

    public List<DTPMISGBLDYTREE> findByMisProduct(String misProduct);

    public List<DTPMISGBLDYTREE> findByMisRegionCode(String misRegionCode);

    public List<DTPMISGBLDYTREE> findByMisTreeCode(String misTreeCode);

    public List<DTPMISGBLDYTREE> findByParentMisMemberCode(String parentMisMemberCode);

    public List<DTPMISGBLDYTREE> findByProductControlSectionMgrId(String productControlSectionMgrId);

    public List<DTPMISGBLDYTREE> findByProductLineCode(String productLineCode);

    public List<DTPMISGBLDYTREE> findByProductLineName(String productLineName);

    public List<DTPMISGBLDYTREE> findBySub3ProductLine(String sub3ProductLine);

    public List<DTPMISGBLDYTREE> findBySub3ProductLineDescription(String sub3ProductLineDescription);

    public List<DTPMISGBLDYTREE> findBySubClusterCode(String subClusterCode);

    public List<DTPMISGBLDYTREE> findBySubClusterName(String subClusterName);

    public List<DTPMISGBLDYTREE> findBySubDivisionCode(String subDivisionCode);

    public List<DTPMISGBLDYTREE> findBySubDivisionDescription(String subDivisionDescription);

    public List<DTPMISGBLDYTREE> findBySubProductDescription(String subProductDescription);

    public List<DTPMISGBLDYTREE> findBySubProductLine(String subProductLine);

    public List<DTPMISGBLDYTREE> findBySwissProductLineHeadId(String swissProductLineHeadId);

    public List<DTPMISGBLDYTREE> findByTradingSupervisorId(String tradingSupervisorId);

    public List<DTPMISGBLDYTREE> findByTradingSupervisorDelegateId(String tradingSupervisorDelegateId);

    public List<DTPMISGBLDYTREE> findByMisProductCode(String misProductCode);

    public List<DTPMISGBLDYTREE> findByMisUnitPercentOwned(String misUnitPercentOwned);

    public List<DTPMISGBLDYTREE> findByMisUnitPercentDescription(String misUnitPercentDescription);

    public List<DTPMISGBLDYTREE> findByDeltaFlag(String deltaFlag);

    public List<DTPMISGBLDYTREE> findByCddsTreeNodeStatus(String cddsTreeNodeStatus);

    public List<DTPMISGBLDYTREE> findByTradingSupport(String tradingSupport);

    public List<DTPMISGBLDYTREE> findByTradeSupportManagerEmail(String tradeSupportManagerEmail);


    public Page<DTPMISGBLDYTREE> findAll(Pageable page);


}







