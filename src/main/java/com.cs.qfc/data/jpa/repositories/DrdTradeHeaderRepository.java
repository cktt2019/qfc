package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.DrdTradeHeader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Service interface with operations for {@link DrdTradeHeader} persistence.
 */
@Repository
public interface DrdTradeHeaderRepository extends JpaRepository<DrdTradeHeader, Integer> {


    public List<DrdTradeHeader> findByCobDate(java.sql.Date cobDate);

    public List<DrdTradeHeader> findByLoadCodDate(java.sql.Date loadCodDate);

    public List<DrdTradeHeader> findByBookName(String bookName);

    public List<DrdTradeHeader> findByTradeLoadId(String tradeLoadId);

    public List<DrdTradeHeader> findByLoadTimestamp(Date loadTimestamp);

    public List<DrdTradeHeader> findBySnapshotName(String snapshotName);

    public List<DrdTradeHeader> findBySnapshotId(Integer snapshotId);

    public List<DrdTradeHeader> findBySnapshotCreationDatetime(Date snapshotCreationDatetime);

    public List<DrdTradeHeader> findByInterfaceVersion(String interfaceVersion);

    public List<DrdTradeHeader> findByTradeId(Integer tradeId);

    public List<DrdTradeHeader> findByTfTradeVersion(Integer tfTradeVersion);

    public List<DrdTradeHeader> findByTradeLocationId(Integer tradeLocationId);

    public List<DrdTradeHeader> findByPrimaryTcn(String primaryTcn);

    public List<DrdTradeHeader> findByTradeVersionUpdateTime(Date tradeVersionUpdateTime);

    public List<DrdTradeHeader> findByCancellationDate(String cancellationDate);

    public List<DrdTradeHeader> findByXccyIndicator(String xccyIndicator);

    public List<DrdTradeHeader> findByGbmEntity(String gbmEntity);

    public List<DrdTradeHeader> findByTotalLocalPv(Integer totalLocalPv);

    public List<DrdTradeHeader> findByTotalLocalPvCcyCode(String totalLocalPvCcyCode);

    public List<DrdTradeHeader> findByTotalPvStatus(String totalPvStatus);

    public List<DrdTradeHeader> findByTotalRpTpv(Integer totalRpTpv);

    public List<DrdTradeHeader> findByTradePvRegDate(java.sql.Date tradePvRegDate);

    public List<DrdTradeHeader> findByTradePvRegDays(Integer tradePvRegDays);

    public List<DrdTradeHeader> findByConfirmationStatus(String confirmationStatus);

    public List<DrdTradeHeader> findByDefaultFairValue(Integer defaultFairValue);

    public List<DrdTradeHeader> findByProveCluster(String proveCluster);

    public List<DrdTradeHeader> findByProveFamily(String proveFamily);

    public List<DrdTradeHeader> findByProveId(String proveId);

    public List<DrdTradeHeader> findByProveProduct(String proveProduct);

    public List<DrdTradeHeader> findByApprovalStatus(String approvalStatus);

    public List<DrdTradeHeader> findByEventReason(String eventReason);

    public List<DrdTradeHeader> findByExternalId(String externalId);

    public List<DrdTradeHeader> findByPrimaryMaturityDate(java.sql.Date primaryMaturityDate);

    public List<DrdTradeHeader> findByProductSubType(String productSubType);

    public List<DrdTradeHeader> findByProductType(String productType);

    public List<DrdTradeHeader> findByProductValuationType(String productValuationType);

    public List<DrdTradeHeader> findByRootTcn(String rootTcn);

    public List<DrdTradeHeader> findByTradeStatus(String tradeStatus);

    public List<DrdTradeHeader> findByCduMasterAgreementLlk(String cduMasterAgreementLlk);

    public List<DrdTradeHeader> findByCduCollateralAnnexLlk(String cduCollateralAnnexLlk);

    public List<DrdTradeHeader> findByFreamesoftMasterAgreementLlk(Integer freamesoftMasterAgreementLlk);

    public List<DrdTradeHeader> findByAlgoCollateralAnnexLlk(Integer algoCollateralAnnexLlk);

    public List<DrdTradeHeader> findByAccountArea(String accountArea);

    public List<DrdTradeHeader> findByBrokerId(String brokerId);

    public List<DrdTradeHeader> findByCollIndepAmount(Float collIndepAmount);

    public List<DrdTradeHeader> findByCollIndepCcy(String collIndepCcy);

    public List<DrdTradeHeader> findByCollIndepOtherMargQuote(String collIndepOtherMargQuote);

    public List<DrdTradeHeader> findByCollIndepPercent(Float collIndepPercent);

    public List<DrdTradeHeader> findByCollIndepType(String collIndepType);

    public List<DrdTradeHeader> findByMarketerId(String marketerId);

    public List<DrdTradeHeader> findByRiskManagementOnlyFlag(String riskManagementOnlyFlag);

    public List<DrdTradeHeader> findByStructuredTradeFlag(String structuredTradeFlag);

    public List<DrdTradeHeader> findByTradeDate(java.sql.Date tradeDate);

    public List<DrdTradeHeader> findByTraderName(String traderName);

    public List<DrdTradeHeader> findByTradeSourceSystem(String tradeSourceSystem);

    public List<DrdTradeHeader> findByTradingPartyId(String tradingPartyId);

    public List<DrdTradeHeader> findByTradingPartyIdType(String tradingPartyIdType);

    public List<DrdTradeHeader> findByCity(String city);

    public List<DrdTradeHeader> findByDeal(String deal);

    public List<DrdTradeHeader> findByStripId(String stripId);

    public List<DrdTradeHeader> findByCustomerNumber(String customerNumber);

    public List<DrdTradeHeader> findByDisplayId(String displayId);

    public List<DrdTradeHeader> findByAssetClass1(String assetClass1);

    public List<DrdTradeHeader> findByAssetClass2(String assetClass2);

    public List<DrdTradeHeader> findByAssetClass3(String assetClass3);

    public List<DrdTradeHeader> findByAssetClass4(String assetClass4);

    public List<DrdTradeHeader> findByLinkedSetType(String linkedSetType);

    public List<DrdTradeHeader> findBySysLinkId(String sysLinkId);

    public List<DrdTradeHeader> findByLeadTradeId(Integer leadTradeId);

    public List<DrdTradeHeader> findByLeadTradeVersion(Integer leadTradeVersion);

    public List<DrdTradeHeader> findByLeadTradeLocationId(Integer leadTradeLocationId);

    public List<DrdTradeHeader> findByLeadExternalId(String leadExternalId);

    public List<DrdTradeHeader> findByLeadTradeBook(String leadTradeBook);

    public List<DrdTradeHeader> findByWashTradeId(Integer washTradeId);

    public List<DrdTradeHeader> findByWashTradeVersion(Integer washTradeVersion);

    public List<DrdTradeHeader> findByWashTradeLocationId(Integer washTradeLocationId);

    public List<DrdTradeHeader> findByWashExternalId(String washExternalId);

    public List<DrdTradeHeader> findByWashTradeBook(String washTradeBook);

    public List<DrdTradeHeader> findByRiskTradeId(Integer riskTradeId);

    public List<DrdTradeHeader> findByRiskTradeVersion(Integer riskTradeVersion);

    public List<DrdTradeHeader> findByRiskTradeLocationId(Integer riskTradeLocationId);

    public List<DrdTradeHeader> findByRiskExternalId(String riskExternalId);

    public List<DrdTradeHeader> findByRiskTradeBook(String riskTradeBook);

    public List<DrdTradeHeader> findBySyntheticTradeIndicator(String syntheticTradeIndicator);

    public List<DrdTradeHeader> findByLinkageStatus(String linkageStatus);

    public List<DrdTradeHeader> findByLinkageStatusDescription(String linkageStatusDescription);

    public List<DrdTradeHeader> findByLinkedTradeId(Integer linkedTradeId);

    public List<DrdTradeHeader> findByLinkedPrimaryTcn(String linkedPrimaryTcn);

    public List<DrdTradeHeader> findByLinkedTradeVersion(Integer linkedTradeVersion);

    public List<DrdTradeHeader> findByLinkedTradeLocationId(String linkedTradeLocationId);

    public List<DrdTradeHeader> findByLinkedTradeExternalId(String linkedTradeExternalId);

    public List<DrdTradeHeader> findByLinkedTradeBook(String linkedTradeBook);

    public List<DrdTradeHeader> findByValuationClass(String valuationClass);

    public List<DrdTradeHeader> findByValuationType(String valuationType);

    public List<DrdTradeHeader> findByPremiumAmount(Float premiumAmount);

    public List<DrdTradeHeader> findBySwapsWireId(String swapsWireId);

    public List<DrdTradeHeader> findByApproxBooking(String approxBooking);

    public List<DrdTradeHeader> findByApproxBookingReviewDate(java.sql.Date approxBookingReviewDate);

    public List<DrdTradeHeader> findByDocsCounterPartyId(String docsCounterPartyId);

    public List<DrdTradeHeader> findByCloIndicator(String cloIndicator);

    public List<DrdTradeHeader> findByLoanMitigationFlag(String loanMitigationFlag);

    public List<DrdTradeHeader> findByUsi(String usi);

    public List<DrdTradeHeader> findByCssi(String cssi);

    public List<DrdTradeHeader> findByGmbBookId(Integer gmbBookId);

    public List<DrdTradeHeader> findByArea(String area);

    public List<DrdTradeHeader> findByStructuredId(String structuredId);

    public List<DrdTradeHeader> findByPvLevel(String pvLevel);

    public List<DrdTradeHeader> findByCounterPartyCsid(String counterPartyCsid);

    public List<DrdTradeHeader> findByCreeTradeIdentifier(String creeTradeIdentifier);

    public List<DrdTradeHeader> findByCreeTradeIdentifierVersion(String creeTradeIdentifierVersion);

    public List<DrdTradeHeader> findByLchOrigCounterParty(String lchOrigCounterParty);

    public List<DrdTradeHeader> findByGbmCluster(String gbmCluster);

    public List<DrdTradeHeader> findByGbmDivision(String gbmDivision);

    public List<DrdTradeHeader> findByAffirmPlatformStatus(String affirmPlatformStatus);

    public List<DrdTradeHeader> findByDocsActualProcessingMethod(String docsActualProcessingMethod);

    public List<DrdTradeHeader> findByUti(String uti);

    public List<DrdTradeHeader> findBySwireManualConfirmedReq(String swireManualConfirmedReq);

    public List<DrdTradeHeader> findByEntryDateTime(java.sql.Date entryDateTime);

    public List<DrdTradeHeader> findByFundingIndicator(String fundingIndicator);

    public List<DrdTradeHeader> findByLeverageFactor(Float leverageFactor);

    public List<DrdTradeHeader> findByLinkTransId(String linkTransId);

    public List<DrdTradeHeader> findByLinkType(String linkType);

    public List<DrdTradeHeader> findByMasterTradeTradeId(String masterTradeTradeId);

    public List<DrdTradeHeader> findByGbmBookRef(String gbmBookRef);

    public List<DrdTradeHeader> findByLastAmmendBy(String lastAmmendBy);

    public List<DrdTradeHeader> findByStructureType(String structureType);

    public List<DrdTradeHeader> findByGarantiaSysId(String garantiaSysId);

    public List<DrdTradeHeader> findByEarlyTmTermParty(String earlyTmTermParty);

    public List<DrdTradeHeader> findByEarlyTmOptStartDate(java.sql.Date earlyTmOptStartDate);

    public List<DrdTradeHeader> findByCounterPartyGsid(String counterPartyGsid);

    public List<DrdTradeHeader> findByEntityType(String entityType);

    public List<DrdTradeHeader> findByEvent(String event);

    public List<DrdTradeHeader> findByEventTradeDate(java.sql.Date eventTradeDate);

    public List<DrdTradeHeader> findByFidcPmInitialMargin(String fidcPmInitialMargin);

    public List<DrdTradeHeader> findByFidcPmNettingOnly(String fidcPmNettingOnly);

    public List<DrdTradeHeader> findByFidcPmVariationMargin(String fidcPmVariationMargin);

    public List<DrdTradeHeader> findByLateTrdNotificationReason(String lateTrdNotificationReason);

    public List<DrdTradeHeader> findByMasterAgreementType(Integer masterAgreementType);

    public List<DrdTradeHeader> findByNotesFrontOffice(String notesFrontOffice);

    public List<DrdTradeHeader> findByRiskAdvisor(String riskAdvisor);

    public List<DrdTradeHeader> findByRiskTrader(String riskTrader);

    public List<DrdTradeHeader> findByTradingPartyGsid(String tradingPartyGsid);

    public List<DrdTradeHeader> findByFidcPmInd(String fidcPmInd);

    public List<DrdTradeHeader> findByAgent(String agent);

    public List<DrdTradeHeader> findByPaymentIndicator(String paymentIndicator);

    public List<DrdTradeHeader> findByRenegotiationDate(java.sql.Date renegotiationDate);

    public List<DrdTradeHeader> findByAlternativeVersionId(Integer alternativeVersionId);

    public List<DrdTradeHeader> findByCifbu(String cifbu);

    public List<DrdTradeHeader> findByDeskBook(String deskBook);

    public List<DrdTradeHeader> findByMetalInkTradeId(String metalInkTradeId);

    public List<DrdTradeHeader> findByLinkedStartDate(java.sql.Date linkedStartDate);

    public List<DrdTradeHeader> findByLinkedEndDate(java.sql.Date linkedEndDate);

    public List<DrdTradeHeader> findBySalespersonId(String salespersonId);

    public List<DrdTradeHeader> findBySourceSystemId(String sourceSystemId);

    public List<DrdTradeHeader> findBySourceSystemVersion(Integer sourceSystemVersion);

    public List<DrdTradeHeader> findByEarlyTmExerciseFreq(String earlyTmExerciseFreq);

    public List<DrdTradeHeader> findByEarlyTmOptStyle(String earlyTmOptStyle);

    public List<DrdTradeHeader> findByPaymentPackageSysLinkId(String paymentPackageSysLinkId);

    public List<DrdTradeHeader> findByTfMatchStatus(String tfMatchStatus);

    public List<DrdTradeHeader> findByBestMatchTfTradeVersion(Integer bestMatchTfTradeVersion);


    public Page<DrdTradeHeader> findAll(Pageable page);


}







