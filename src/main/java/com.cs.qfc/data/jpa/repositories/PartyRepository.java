package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.Party;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link Party} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface PartyRepository extends JpaRepository<Party, Long> {


    public List<Party> findByCsid(String csid);

    public List<Party> findByCsidSource(String csidSource);

    public List<Party> findByGsid(String gsid);

    public List<Party> findByGsidSource(String gsidSource);

    public List<Party> findByCountryOfDomicileIsoCountryCode(String countryOfDomicileIsoCountryCode);

    public List<Party> findByCountryOfIncorporationIsoCountryCode(String countryOfIncorporationIsoCountryCode);

    public List<Party> findByPartyLegalName(String partyLegalName);

    public List<Party> findByAddressTypeCode(String addressTypeCode);

    public List<Party> findByAddressAddressLine1(String addressAddressLine1);

    public List<Party> findByAddressAddressLine2(String addressAddressLine2);

    public List<Party> findByAddressAddressLine3(String addressAddressLine3);

    public List<Party> findByAddressAddressLine4(String addressAddressLine4);

    public List<Party> findByAddressPostalCode(String addressPostalCode);

    public List<Party> findByAddressCountryIsoCountryName(String addressCountryIsoCountryName);

    public List<Party> findByAddressAddressFullPhoneNumber(String addressAddressFullPhoneNumber);

    public List<Party> findByAddressElctronicAddressId(String addressElctronicAddressId);

    public List<Party> findByPartyLegalRoleDefinitionId(String partyLegalRoleDefinitionId);

    public List<Party> findByPartyLegalRoleId(String partyLegalRoleId);

    public List<Party> findByInternalPartyIdAltSourceName(String internalPartyIdAltSourceName);

    public List<Party> findByPartyLegalRoleDescription(String partyLegalRoleDescription);

    public List<Party> findByPartyLegalStatusType(String partyLegalStatusType);

    public List<Party> findByPartyLegalStatusCode(String partyLegalStatusCode);

    public List<Party> findByAddressTypeDescription(String addressTypeDescription);

    public List<Party> findByAddressCityName(String addressCityName);

    public List<Party> findByAddressPoBoxCity(String addressPoBoxCity);

    public List<Party> findByExternalPartyAltIdSourceId(String externalPartyAltIdSourceId);

    public List<Party> findByExternalPartyAltIdSourceName(String externalPartyAltIdSourceName);

    public List<Party> findByExternalPartyAltIdSourceIdentificationNumber(String externalPartyAltIdSourceIdentificationNumber);


    public Page<Party> findAll(Pageable page);


}







