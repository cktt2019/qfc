package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.DebtConvertibleBond;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link DebtConvertibleBond} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface DebtConvertibleBondRepository extends JpaRepository<DebtConvertibleBond, String> {


    public List<DebtConvertibleBond> findBySecurityDescriptionLong(String securityDescriptionLong);

    public List<DebtConvertibleBond> findByNxtPayDate(java.sql.Date nxtPayDate);

    public List<DebtConvertibleBond> findByNextCallDate(String nextCallDate);

    public List<DebtConvertibleBond> findByNextPutDate(String nextPutDate);

    public List<DebtConvertibleBond> findByAccrualCount(String accrualCount);

    public List<DebtConvertibleBond> findByAggteIssue(String aggteIssue);

    public List<DebtConvertibleBond> findByBaseCpi(Float baseCpi);

    public List<DebtConvertibleBond> findByClearingHouseDer(String clearingHouseDer);

    public List<DebtConvertibleBond> findByBbTicker(String bbTicker);

    public List<DebtConvertibleBond> findByBookEntry(String bookEntry);

    public List<DebtConvertibleBond> findByBradyBusDayConvention(String bradyBusDayConvention);

    public List<DebtConvertibleBond> findByBusinessCalendar(String businessCalendar);

    public List<DebtConvertibleBond> findByCalcTypeDes(String calcTypeDes);

    public List<DebtConvertibleBond> findByCallDiscrete(String callDiscrete);

    public List<DebtConvertibleBond> findByCallFeature(String callFeature);

    public List<DebtConvertibleBond> findByCallNotice(String callNotice);

    public List<DebtConvertibleBond> findByCallDate(java.sql.Date callDate);

    public List<DebtConvertibleBond> findByCallPrice(Float callPrice);

    public List<DebtConvertibleBond> findByCpnFre(String cpnFre);

    public List<DebtConvertibleBond> findByCpnFreqDays(Float cpnFreqDays);

    public List<DebtConvertibleBond> findByCouponType(String couponType);

    public List<DebtConvertibleBond> findByCouponTypeReset(String couponTypeReset);

    public List<DebtConvertibleBond> findByCpnFreqYld(String cpnFreqYld);

    public List<DebtConvertibleBond> findByCsManaged(String csManaged);

    public List<DebtConvertibleBond> findByIssDate(java.sql.Date issDate);

    public List<DebtConvertibleBond> findByDebtStatus(String debtStatus);

    public List<DebtConvertibleBond> findByDefSecurity(String defSecurity);

    public List<DebtConvertibleBond> findByDaysToSettle(Float daysToSettle);

    public List<DebtConvertibleBond> findByFwEntry(String fwEntry);

    public List<DebtConvertibleBond> findByFidessaZone(String fidessaZone);

    public List<DebtConvertibleBond> findByFidessaCode(String fidessaCode);

    public List<DebtConvertibleBond> findByFirstCallDate(java.sql.Date firstCallDate);

    public List<DebtConvertibleBond> findByFirstCoupon(java.sql.Date firstCoupon);

    public List<DebtConvertibleBond> findByFixFltDayCount(Float fixFltDayCount);

    public List<DebtConvertibleBond> findByFloatRst(String floatRst);

    public List<DebtConvertibleBond> findByFloatRstIndx(Float floatRstIndx);

    public List<DebtConvertibleBond> findByFltDaysPrior(Float fltDaysPrior);

    public List<DebtConvertibleBond> findByGlossK3(String glossK3);

    public List<DebtConvertibleBond> findByGlossL9(String glossL9);

    public List<DebtConvertibleBond> findByGlossM3(String glossM3);

    public List<DebtConvertibleBond> findByIdBb(String idBb);

    public List<DebtConvertibleBond> findByIdBbUnique(String idBbUnique);

    public List<DebtConvertibleBond> findByEpicId(String epicId);

    public List<DebtConvertibleBond> findByCmuId(String cmuId);

    public List<DebtConvertibleBond> findByIsinId(String isinId);

    public List<DebtConvertibleBond> findByJapanId(String japanId);

    public List<DebtConvertibleBond> findByMiscDomesticId(String miscDomesticId);

    public List<DebtConvertibleBond> findByPoetsId(String poetsId);

    public List<DebtConvertibleBond> findByRicId(String ricId);

    public List<DebtConvertibleBond> findBySedol2Id(String sedol2Id);

    public List<DebtConvertibleBond> findBySingId(String singId);

    public List<DebtConvertibleBond> findByIncomeCurrency(String incomeCurrency);

    public List<DebtConvertibleBond> findByIndustrySbgprIssue(String industrySbgprIssue);

    public List<DebtConvertibleBond> findByIntRateCap(Float intRateCap);

    public List<DebtConvertibleBond> findByIntRateFl(String intRateFl);

    public List<DebtConvertibleBond> findByBojId(String bojId);

    public List<DebtConvertibleBond> findByFisecId(String fisecId);

    public List<DebtConvertibleBond> findByHugoId(String hugoId);

    public List<DebtConvertibleBond> findByIstarId(String istarId);

    public List<DebtConvertibleBond> findByNriId(String nriId);

    public List<DebtConvertibleBond> findBySmuId(String smuId);

    public List<DebtConvertibleBond> findByNrtIssuerId(String nrtIssuerId);

    public List<DebtConvertibleBond> findByMarginPerc(Float marginPerc);

    public List<DebtConvertibleBond> findByMaturityDate(java.sql.Date maturityDate);

    public List<DebtConvertibleBond> findByMuniMinTax(String muniMinTax);

    public List<DebtConvertibleBond> findByMuniFedTax(String muniFedTax);

    public List<DebtConvertibleBond> findByStateCode(String stateCode);

    public List<DebtConvertibleBond> findByNextReset(java.sql.Date nextReset);

    public List<DebtConvertibleBond> findByNriIntPayClass(String nriIntPayClass);

    public List<DebtConvertibleBond> findByNriCode(String nriCode);

    public List<DebtConvertibleBond> findByNriSecSub(String nriSecSub);

    public List<DebtConvertibleBond> findByNriSicc(String nriSicc);

    public List<DebtConvertibleBond> findByOfferingTyp(String offeringTyp);

    public List<DebtConvertibleBond> findByDatedDate(java.sql.Date datedDate);

    public List<DebtConvertibleBond> findByPhysical(String physical);

    public List<DebtConvertibleBond> findByPrevPayDate(java.sql.Date prevPayDate);

    public List<DebtConvertibleBond> findByPrvRstDate(java.sql.Date prvRstDate);

    public List<DebtConvertibleBond> findByPrincFac(Float princFac);

    public List<DebtConvertibleBond> findByPrivatePlacement(String privatePlacement);

    public List<DebtConvertibleBond> findByProRataCall(String proRataCall);

    public List<DebtConvertibleBond> findByPubBalOut(Float pubBalOut);

    public List<DebtConvertibleBond> findByPutDisc(String putDisc);

    public List<DebtConvertibleBond> findByPutFeature(String putFeature);

    public List<DebtConvertibleBond> findByPutNotice(Integer putNotice);

    public List<DebtConvertibleBond> findByRatingsTrigger(String ratingsTrigger);

    public List<DebtConvertibleBond> findByRefixFreq(String refixFreq);

    public List<DebtConvertibleBond> findByRegs(String regs);

    public List<DebtConvertibleBond> findByRegionCode(String regionCode);

    public List<DebtConvertibleBond> findByResetIdx(String resetIdx);

    public List<DebtConvertibleBond> findByRestrictCode(String restrictCode);

    public List<DebtConvertibleBond> findBySecType(String secType);

    public List<DebtConvertibleBond> findBySinkFreq(String sinkFreq);

    public List<DebtConvertibleBond> findByBenchmarkSpread(Integer benchmarkSpread);

    public List<DebtConvertibleBond> findByTickerSymbol(String tickerSymbol);

    public List<DebtConvertibleBond> findByTradingCurrency(String tradingCurrency);

    public List<DebtConvertibleBond> findByMnPiece(Float mnPiece);

    public List<DebtConvertibleBond> findByTraxReport(String traxReport);

    public List<DebtConvertibleBond> findByUltMatDate(java.sql.Date ultMatDate);

    public List<DebtConvertibleBond> findByDesNotes(String desNotes);

    public List<DebtConvertibleBond> findByUltPrntCompId(String ultPrntCompId);

    public List<DebtConvertibleBond> findByFisecIssuerId(String fisecIssuerId);

    public List<DebtConvertibleBond> findByUltPrntTickerExchange(String ultPrntTickerExchange);

    public List<DebtConvertibleBond> findByCouponFrequency(String couponFrequency);

    public List<DebtConvertibleBond> findByLondonImsDescription(String londonImsDescription);

    public List<DebtConvertibleBond> findByConRatio(Integer conRatio);

    public List<DebtConvertibleBond> findByUndlySecIsin(String undlySecIsin);

    public List<DebtConvertibleBond> findByConvPrice(Float convPrice);

    public List<DebtConvertibleBond> findByPerSecSrc01(String perSecSrc01);

    public List<DebtConvertibleBond> findByIssueDate(java.sql.Date issueDate);

    public List<DebtConvertibleBond> findBySpIssuerId(String spIssuerId);

    public List<DebtConvertibleBond> findByNtpaInPosition(String ntpaInPosition);

    public List<DebtConvertibleBond> findByIsInternationalSukuk(String isInternationalSukuk);

    public List<DebtConvertibleBond> findByIsDomesticSukuk(String isDomesticSukuk);

    public List<DebtConvertibleBond> findBySyntheticId(String syntheticId);

    public List<DebtConvertibleBond> findByPoolCusipId(String poolCusipId);

    public List<DebtConvertibleBond> findByAuctRatePfdInd(String auctRatePfdInd);

    public List<DebtConvertibleBond> findByRepoclearFlg(String repoclearFlg);

    public List<DebtConvertibleBond> findByMuniLetterCreditIssuer(String muniLetterCreditIssuer);

    public List<DebtConvertibleBond> findByMuniCorpObligator2(String muniCorpObligator2);

    public List<DebtConvertibleBond> findByInsuranceStatus(String insuranceStatus);

    public List<DebtConvertibleBond> findByObligator(String obligator);

    public List<DebtConvertibleBond> findByCpn(Float cpn);

    public List<DebtConvertibleBond> findByTradeCntry(String tradeCntry);

    public List<DebtConvertibleBond> findByCommonId(String commonId);

    public List<DebtConvertibleBond> findBySeries(String series);

    public List<DebtConvertibleBond> findByClassCode(String classCode);


    public Page<DebtConvertibleBond> findAll(Pageable page);


}







