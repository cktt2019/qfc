package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.ClientLei;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link ClientLei} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface ClientLeiRepository extends JpaRepository<ClientLei, Integer> {


    public List<ClientLei> findByCounterPartyCsid(String counterPartyCsid);

    public List<ClientLei> findByCounterPartyName(String counterPartyName);

    public List<ClientLei> findByCounterPartyLei(String counterPartyLei);

    public List<ClientLei> findByUltimateParentCsid(String ultimateParentCsid);

    public List<ClientLei> findByUltimateParentName(String ultimateParentName);

    public List<ClientLei> findByUltimateParentLei(String ultimateParentLei);


    public Page<ClientLei> findAll(Pageable page);


}







