package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.MarnaG;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link MarnaG} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface MarnaGRepository extends JpaRepository<MarnaG, Integer> {


    public List<MarnaG> findByAgreementNumber(String agreementNumber);

    public List<MarnaG> findByStatus(String status);

    public List<MarnaG> findBySupersededByCnid(String supersededByCnid);

    public List<MarnaG> findBySupersededByAgreement(String supersededByAgreement);

    public List<MarnaG> findByLegalAgreementDescription(String legalAgreementDescription);

    public List<MarnaG> findByCsEntityBookinggroupId(String csEntityBookinggroupId);

    public List<MarnaG> findByCsEntityBookinggroupName(String csEntityBookinggroupName);

    public List<MarnaG> findByNegotiator(String negotiator);

    public List<MarnaG> findByNegotiatorLocation(String negotiatorLocation);

    public List<MarnaG> findByCounterParty(String counterParty);

    public List<MarnaG> findByCounterPartyRole(String counterPartyRole);

    public List<MarnaG> findByCsid(String csid);

    public List<MarnaG> findByLegalId(String legalId);

    public List<MarnaG> findByFormerlyKnownAs(String formerlyKnownAs);

    public List<MarnaG> findByCountryOfInc(String countryOfInc);

    public List<MarnaG> findByContractType(String contractType);

    public List<MarnaG> findByRequestType(String requestType);

    public List<MarnaG> findByAgreementTitle(String agreementTitle);

    public List<MarnaG> findByContractDate(java.sql.Date contractDate);

    public List<MarnaG> findByAgency(String agency);

    public List<MarnaG> findByCollateralAgreementFlag(String collateralAgreementFlag);

    public List<MarnaG> findByCsaEnforceableFlag(String csaEnforceableFlag);

    public List<MarnaG> findByTransactionSpecificFlag(String transactionSpecificFlag);

    public List<MarnaG> findByCsEntityAgencyFlag(String csEntityAgencyFlag);

    public List<MarnaG> findByDesk(String desk);

    public List<MarnaG> findByIconid(String iconid);

    public List<MarnaG> findByCsaRegFlag(String csaRegFlag);


    public Page<MarnaG> findAll(Pageable page);


}







