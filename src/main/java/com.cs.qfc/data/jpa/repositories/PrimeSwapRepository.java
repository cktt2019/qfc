package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.PrimeSwap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link PrimeSwap} persistence.
 */
@Repository
public interface PrimeSwapRepository extends JpaRepository<PrimeSwap, Integer> {


    public List<PrimeSwap> findByBusinessDate(java.sql.Date businessDate);

    public List<PrimeSwap> findByLegalEntity(String legalEntity);

    public List<PrimeSwap> findBySwapId(String swapId);

    public List<PrimeSwap> findBySwapVersion(String swapVersion);

    public List<PrimeSwap> findByCounterPartyCode(String counterPartyCode);

    public List<PrimeSwap> findByCounterPartyName(String counterPartyName);

    public List<PrimeSwap> findByBook(String book);

    public List<PrimeSwap> findByTrader(String trader);

    public List<PrimeSwap> findBySwapType(String swapType);

    public List<PrimeSwap> findByFramesoftMasterAgreementLlk(String framesoftMasterAgreementLlk);

    public List<PrimeSwap> findByAlgoCollateralAgreementLlk(String algoCollateralAgreementLlk);

    public List<PrimeSwap> findByCduMasterAgreementLlk(String cduMasterAgreementLlk);

    public List<PrimeSwap> findByCduCollateralAnnexLlk(String cduCollateralAnnexLlk);

    public List<PrimeSwap> findBySwapTradeDate(java.sql.Date swapTradeDate);

    public List<PrimeSwap> findBySwapTerminationDate(java.sql.Date swapTerminationDate);

    public List<PrimeSwap> findByNextInterestPayDate(java.sql.Date nextInterestPayDate);

    public List<PrimeSwap> findByEquityCurrency(String equityCurrency);

    public List<PrimeSwap> findByTotalSwapValue(Float totalSwapValue);

    public List<PrimeSwap> findByCurrencyEquityNotional(String currencyEquityNotional);

    public List<PrimeSwap> findByInitMargin(Float initMargin);

    public List<PrimeSwap> findByInitialMarginDirection(String initialMarginDirection);

    public List<PrimeSwap> findByCsid(String csid);

    public List<PrimeSwap> findByGsid(String gsid);


    public Page<PrimeSwap> findAll(Pageable page);


}







