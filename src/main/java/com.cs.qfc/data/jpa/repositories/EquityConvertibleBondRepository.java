package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.EquityConvertibleBond;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link EquityConvertibleBond} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface EquityConvertibleBondRepository extends JpaRepository<EquityConvertibleBond, String> {


    public List<EquityConvertibleBond> findByIsinId(String isinId);

    public List<EquityConvertibleBond> findByNextCallDate(java.sql.Date nextCallDate);

    public List<EquityConvertibleBond> findByNextPutDate(String nextPutDate);

    public List<EquityConvertibleBond> findByNxtPayDate(java.sql.Date nxtPayDate);

    public List<EquityConvertibleBond> findBySecurityDescription(String securityDescription);

    public List<EquityConvertibleBond> findByA3c7Ind(String a3c7Ind);

    public List<EquityConvertibleBond> findByBbYKet(String bbYKet);

    public List<EquityConvertibleBond> findByCompExchCodeDer(String compExchCodeDer);

    public List<EquityConvertibleBond> findByConvPrice(Float convPrice);

    public List<EquityConvertibleBond> findByConRatio(Float conRatio);

    public List<EquityConvertibleBond> findByCsManaged(String csManaged);

    public List<EquityConvertibleBond> findByEquityFloat(Float equityFloat);

    public List<EquityConvertibleBond> findByExchange(String exchange);

    public List<EquityConvertibleBond> findByFidesaCode(String fidesaCode);

    public List<EquityConvertibleBond> findByFundObjective(String fundObjective);

    public List<EquityConvertibleBond> findByGlossG5(String glossG5);

    public List<EquityConvertibleBond> findByIdBbGlobal(String idBbGlobal);

    public List<EquityConvertibleBond> findByIdBbUnique(String idBbUnique);

    public List<EquityConvertibleBond> findByItalyId(String italyId);

    public List<EquityConvertibleBond> findByJapanId(String japanId);

    public List<EquityConvertibleBond> findByRicId(String ricId);

    public List<EquityConvertibleBond> findBySingId(String singId);

    public List<EquityConvertibleBond> findByValorenId(String valorenId);

    public List<EquityConvertibleBond> findByWertpId(String wertpId);

    public List<EquityConvertibleBond> findByHugoId(String hugoId);

    public List<EquityConvertibleBond> findByNtpaLsEqyPoDt(String ntpaLsEqyPoDt);

    public List<EquityConvertibleBond> findByLondonImsDescription(String londonImsDescription);

    public List<EquityConvertibleBond> findByMaturityDate(java.sql.Date maturityDate);

    public List<EquityConvertibleBond> findByNasdaqReportCodeDer(String nasdaqReportCodeDer);

    public List<EquityConvertibleBond> findByPrimaryRic(String primaryRic);

    public List<EquityConvertibleBond> findByQtyPerUndlyShr(Float qtyPerUndlyShr);

    public List<EquityConvertibleBond> findByUndlyPerAdr(String undlyPerAdr);

    public List<EquityConvertibleBond> findByRegs(String regs);

    public List<EquityConvertibleBond> findByRegionPrim(String regionPrim);

    public List<EquityConvertibleBond> findByRtrTicketSymbol(String rtrTicketSymbol);

    public List<EquityConvertibleBond> findByTradeCntry(String tradeCntry);

    public List<EquityConvertibleBond> findByUndlySecIsin(String undlySecIsin);

    public List<EquityConvertibleBond> findByVotingRightPerShare(Float votingRightPerShare);

    public List<EquityConvertibleBond> findByCmuId(String cmuId);

    public List<EquityConvertibleBond> findByUltPrntTicketExchange(String ultPrntTicketExchange);

    public List<EquityConvertibleBond> findByPoetsId(String poetsId);

    public List<EquityConvertibleBond> findByNtpaSoi(String ntpaSoi);

    public List<EquityConvertibleBond> findByCpn(Float cpn);

    public List<EquityConvertibleBond> findBySsrLiquidityIndicator(String ssrLiquidityIndicator);

    public List<EquityConvertibleBond> findByReExchange(String reExchange);

    public List<EquityConvertibleBond> findByGlossK3(String glossK3);

    public List<EquityConvertibleBond> findBySecType(String secType);

    public List<EquityConvertibleBond> findByFundEuroDirectUcits(String fundEuroDirectUcits);

    public List<EquityConvertibleBond> findByFundAssetClassFocus(String fundAssetClassFocus);

    public List<EquityConvertibleBond> findByBbCompanyId(String bbCompanyId);

    public List<EquityConvertibleBond> findBySpIssueId(String spIssueId);

    public List<EquityConvertibleBond> findByFundClosedNewInv(String fundClosedNewInv);

    public List<EquityConvertibleBond> findByLegalName(String legalName);

    public List<EquityConvertibleBond> findByTradingLotSize(Float tradingLotSize);

    public List<EquityConvertibleBond> findByReAdrUndlyRic(String reAdrUndlyRic);

    public List<EquityConvertibleBond> findByRelativeIndex(String relativeIndex);

    public List<EquityConvertibleBond> findByPrimTrad(String primTrad);

    public List<EquityConvertibleBond> findByCumulativeFlag(String cumulativeFlag);

    public List<EquityConvertibleBond> findByFundCustodianName(String fundCustodianName);

    public List<EquityConvertibleBond> findByInitialPubOffer(java.sql.Date initialPubOffer);

    public List<EquityConvertibleBond> findByGicsSectorIsr(String gicsSectorIsr);

    public List<EquityConvertibleBond> findByGicsSectorNameIsr(String gicsSectorNameIsr);

    public List<EquityConvertibleBond> findByGicsIndustryGroupIsr(String gicsIndustryGroupIsr);

    public List<EquityConvertibleBond> findByGicsIndustryGroupNameIsr(String gicsIndustryGroupNameIsr);

    public List<EquityConvertibleBond> findByGicsIndustrySr(String gicsIndustrySr);

    public List<EquityConvertibleBond> findByGicsIndustryNameIsr(String gicsIndustryNameIsr);

    public List<EquityConvertibleBond> findByNaicsIdIsr(String naicsIdIsr);

    public List<EquityConvertibleBond> findByNtpaCountryOfSettlement(String ntpaCountryOfSettlement);

    public List<EquityConvertibleBond> findByGliRp(String gliRp);

    public List<EquityConvertibleBond> findByCddsLastUpdatedTime(String cddsLastUpdatedTime);

    public List<EquityConvertibleBond> findByPubliclyTraded(String publiclyTraded);

    public List<EquityConvertibleBond> findByCsSettleDays(String csSettleDays);

    public List<EquityConvertibleBond> findByBbUnderlyingSecurity(String bbUnderlyingSecurity);

    public List<EquityConvertibleBond> findByBbUnderlyingTypeCode(String bbUnderlyingTypeCode);

    public List<EquityConvertibleBond> findByTickSizePilotGroup(String tickSizePilotGroup);

    public List<EquityConvertibleBond> findByCalledDate(java.sql.Date calledDate);

    public List<EquityConvertibleBond> findByCalledPrice(Float calledPrice);

    public List<EquityConvertibleBond> findByCouponType(String couponType);

    public List<EquityConvertibleBond> findByReClassScheme(String reClassScheme);

    public List<EquityConvertibleBond> findByLegalEntityIdentifier(String legalEntityIdentifier);

    public List<EquityConvertibleBond> findByCompanyCorpTicker(String companyCorpTicker);

    public List<EquityConvertibleBond> findByCompanyToParent(String companyToParent);

    public List<EquityConvertibleBond> findByBbIssuerType(String bbIssuerType);

    public List<EquityConvertibleBond> findByFisecIssuerId(String fisecIssuerId);

    public List<EquityConvertibleBond> findByUltPrntCompId(String ultPrntCompId);

    public List<EquityConvertibleBond> findByIsArchived(Integer isArchived);

    public List<EquityConvertibleBond> findByLocalExchangeSymbol(String localExchangeSymbol);

    public List<EquityConvertibleBond> findByCfiCode(String cfiCode);

    public List<EquityConvertibleBond> findByFisn(String fisn);

    public List<EquityConvertibleBond> findByIssDate(java.sql.Date issDate);

    public List<EquityConvertibleBond> findByFirstSettlementDate(java.sql.Date firstSettlementDate);

    public List<EquityConvertibleBond> findByWhenIssuedFlag(String whenIssuedFlag);

    public List<EquityConvertibleBond> findByMinorTradingCurrencyInd(String minorTradingCurrencyInd);

    public List<EquityConvertibleBond> findByRiskAlert(String riskAlert);

    public List<EquityConvertibleBond> findByMdsId(String mdsId);

    public List<EquityConvertibleBond> findByFundGeographicFocus(String fundGeographicFocus);

    public List<EquityConvertibleBond> findByFundLeverageAmount(String fundLeverageAmount);

    public List<EquityConvertibleBond> findByCmsCode(String cmsCode);

    public List<EquityConvertibleBond> findByIdSpi(String idSpi);

    public List<EquityConvertibleBond> findByStaticListingId(String staticListingId);

    public List<EquityConvertibleBond> findByClientType(String clientType);

    public List<EquityConvertibleBond> findByMarketingDesk(String marketingDesk);

    public List<EquityConvertibleBond> findByExternalId(String externalId);

    public List<EquityConvertibleBond> findByIssueBook(String issueBook);

    public List<EquityConvertibleBond> findByDutchId(String dutchId);

    public List<EquityConvertibleBond> findByBelgiumId(String belgiumId);

    public List<EquityConvertibleBond> findByIstarId(String istarId);

    public List<EquityConvertibleBond> findByIdBbSecurity(Float idBbSecurity);

    public List<EquityConvertibleBond> findByOperatingMic(String operatingMic);

    public List<EquityConvertibleBond> findByBbIndustrySectorCd(Integer bbIndustrySectorCd);

    public List<EquityConvertibleBond> findByBbIndustryGroupCd(Integer bbIndustryGroupCd);

    public List<EquityConvertibleBond> findByBbIndustrySubGroupCd(Integer bbIndustrySubGroupCd);

    public List<EquityConvertibleBond> findByCddsRecCreationTime(java.sql.Timestamp cddsRecCreationTime);

    public List<EquityConvertibleBond> findByListedExchInd(String listedExchInd);

    public List<EquityConvertibleBond> findByIdMalaysian(String idMalaysian);

    public List<EquityConvertibleBond> findByRedemptionPrice(Float redemptionPrice);

    public List<EquityConvertibleBond> findBySeries(String series);

    public List<EquityConvertibleBond> findByCpnFreq(String cpnFreq);

    public List<EquityConvertibleBond> findByGmSoi(String gmSoi);

    public List<EquityConvertibleBond> findByCsPvtPlacementInd(String csPvtPlacementInd);


    public Page<EquityConvertibleBond> findAll(Pageable page);


}







