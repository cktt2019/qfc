package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.PrimeSwapPosition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link PrimeSwapPosition} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface PrimeSwapPositionRepository extends JpaRepository<PrimeSwapPosition, Integer> {


    public List<PrimeSwapPosition> findByBusinessDate(java.sql.Date businessDate);

    public List<PrimeSwapPosition> findBySwapId(String swapId);

    public List<PrimeSwapPosition> findBySwapVersion(String swapVersion);

    public List<PrimeSwapPosition> findByPositionId(String positionId);

    public List<PrimeSwapPosition> findByPositionVersion(String positionVersion);

    public List<PrimeSwapPosition> findBySwapType(String swapType);

    public List<PrimeSwapPosition> findByCounterpartyCode(String counterpartyCode);

    public List<PrimeSwapPosition> findByCounterpartyName(String counterpartyName);

    public List<PrimeSwapPosition> findBySettledLtdCostBase(String settledLtdCostBase);

    public List<PrimeSwapPosition> findBySwapCcyToUsdFx(String swapCcyToUsdFx);


    public Page<PrimeSwapPosition> findAll(Pageable page);


}







