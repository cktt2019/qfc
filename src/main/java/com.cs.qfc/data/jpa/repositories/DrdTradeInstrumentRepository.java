package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.DrdTradeInstrument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link DrdTradeInstrument} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface DrdTradeInstrumentRepository extends JpaRepository<DrdTradeInstrument, Integer> {


    public List<DrdTradeInstrument> findByCobDate(java.sql.Date cobDate);

    public List<DrdTradeInstrument> findByGbmEntity(String gbmEntity);

    public List<DrdTradeInstrument> findByTradeId(Integer tradeId);

    public List<DrdTradeInstrument> findByCounterPartyId(String counterPartyId);

    public List<DrdTradeInstrument> findByBookName(String bookName);

    public List<DrdTradeInstrument> findByAssetClass(String assetClass);

    public List<DrdTradeInstrument> findByCduMasterAgreementLlk(String cduMasterAgreementLlk);

    public List<DrdTradeInstrument> findByCduCollateralAnnexLlk(String cduCollateralAnnexLlk);

    public List<DrdTradeInstrument> findByFramesoftMasterAgreementLlk(String framesoftMasterAgreementLlk);

    public List<DrdTradeInstrument> findByAlgoCollateralAnnexLlk(String algoCollateralAnnexLlk);

    public List<DrdTradeInstrument> findByTradeDate(java.sql.Date tradeDate);

    public List<DrdTradeInstrument> findByPrimaryMaturityDate(java.sql.Date primaryMaturityDate);

    public List<DrdTradeInstrument> findByLocalPvCcyCode(String localPvCcyCode);

    public List<DrdTradeInstrument> findByLocalPv(Integer localPv);


    public Page<DrdTradeInstrument> findAll(Pageable page);


}







