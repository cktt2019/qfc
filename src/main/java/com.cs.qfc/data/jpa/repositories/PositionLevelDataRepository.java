package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.PositionLevelData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link PositionLevelData} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface PositionLevelDataRepository extends JpaRepository<PositionLevelData, String> {


    public List<PositionLevelData> findByA11AsOfDate(java.sql.Date a11AsOfDate);

    public List<PositionLevelData> findByA12RecordsEntityIdentifier(String a12RecordsEntityIdentifier);

    public List<PositionLevelData> findByA13PositionsIdentifier(String a13PositionsIdentifier);

    public List<PositionLevelData> findByA15InternalBookingLocationIdentifier(String a15InternalBookingLocationIdentifier);

    public List<PositionLevelData> findByA16UniqueBookingUnitDeskIdentifier(String a16UniqueBookingUnitDeskIdentifier);

    public List<PositionLevelData> findByA17TypeOfQfc(String a17TypeOfQfc);

    public List<PositionLevelData> findByA171TypeOfQfcCoveredGuarantee(String a171TypeOfQfcCoveredGuarantee);

    public List<PositionLevelData> findByA172UnderlyingQfcObligatorIdentifier(String a172UnderlyingQfcObligatorIdentifier);

    public List<PositionLevelData> findByA18AgreementIdentifier(String a18AgreementIdentifier);

    public List<PositionLevelData> findByA19NettingAgreementIdrntifier(String a19NettingAgreementIdrntifier);

    public List<PositionLevelData> findByA110NettingAgreementCounterpartyIdentifier(String a110NettingAgreementCounterpartyIdentifier);

    public List<PositionLevelData> findByA111TradeDate(String a111TradeDate);

    public List<PositionLevelData> findByA112TerminationDate(String a112TerminationDate);

    public List<PositionLevelData> findByA113NextPutCallCancellationDate(String a113NextPutCallCancellationDate);

    public List<PositionLevelData> findByA114NextPaymentDate(String a114NextPaymentDate);

    public List<PositionLevelData> findByA115LocalCurrencyOfPosition(String a115LocalCurrencyOfPosition);

    public List<PositionLevelData> findByA116CurrentMarketValueInLocalCurrency(String a116CurrentMarketValueInLocalCurrency);

    public List<PositionLevelData> findByA117CurrentMarketValueInUsd(String a117CurrentMarketValueInUsd);

    public List<PositionLevelData> findByA118AssetClassification(String a118AssetClassification);

    public List<PositionLevelData> findByA119NotionalPrincipalAmountLocalCurrency(String a119NotionalPrincipalAmountLocalCurrency);

    public List<PositionLevelData> findByA120NotionalPrincipalAmountUsd(String a120NotionalPrincipalAmountUsd);

    public List<PositionLevelData> findByA121CoveredByThirdPartyCreditEnhancement(String a121CoveredByThirdPartyCreditEnhancement);

    public List<PositionLevelData> findByA1211ThirdPartyCreditEnhancementProviderIdentifier(String a1211ThirdPartyCreditEnhancementProviderIdentifier);

    public List<PositionLevelData> findByA1212ThirdPartyCreditEnhancementAgreementIdentifier(String a1212ThirdPartyCreditEnhancementAgreementIdentifier);

    public List<PositionLevelData> findByA213CounterpartyThirdPartyCreditEnhancement(String a213CounterpartyThirdPartyCreditEnhancement);

    public List<PositionLevelData> findByA1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier(String a1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier);

    public List<PositionLevelData> findByA1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier(String a1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier);

    public List<PositionLevelData> findByA122RelatedPositionOfRecordsEntity(String a122RelatedPositionOfRecordsEntity);

    public List<PositionLevelData> findByA123ReferenceNumberForAnyRelatedLoan(String a123ReferenceNumberForAnyRelatedLoan);

    public List<PositionLevelData> findByA124IdentifierOfTheLenderOfRelatedLoan(String a124IdentifierOfTheLenderOfRelatedLoan);

    public List<PositionLevelData> findBySourceTxnId(String sourceTxnId);

    public List<PositionLevelData> findByA14CounterpartyIdentifier(String a14CounterpartyIdentifier);


    public Page<PositionLevelData> findAll(Pageable page);


}







