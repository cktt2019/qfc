package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Department;
import com.cs.qfc.data.model.DepartmentDTO;


/**
 * @author london-databases
 */


public class DepartmentToDTOConverter {


    public static DepartmentDTO convert(Department department) {
        if (department != null)
            return DepartmentDTO.builder().departmentId(department.getDepartmentId())
                    .businessControllerId(department.getBusinessControllerId())
                    .controllerId(department.getControllerId())
                    .departmentCategoryCode(department.getDepartmentCategoryCode())
                    .departmentCategoryName(department.getDepartmentCategoryName())
                    .departmentDormantIndicator(department.getDepartmentDormantIndicator())
                    .departmentLongName(department.getDepartmentLongName())
                    .departmentName(department.getDepartmentName())
                    .departmentReconcilerGroupName(department.getDepartmentReconcilerGroupName())
                    .departmentStatus(department.getDepartmentStatus())
                    .departmentTraderId(department.getDepartmentTraderId())
                    .departmentValidFromDate(department.getDepartmentValidFromDate())
                    .departmentValidToDate(department.getDepartmentValidToDate())
                    .financialBusinessUnitCode(department.getFinancialBusinessUnitCode())
                    .ivrGroup(department.getIvrGroup())
                    .l1SupervisorId(department.getL1SupervisorId())
                    .misOfficeCode(department.getMisOfficeCode())
                    .misOfficeName(department.getMisOfficeName())
                    .ownerL2Id(department.getOwnerL2Id())
                    .recAndAdjControllerId(department.getRecAndAdjControllerId())
                    .recAndAdjSupervisorId(department.getRecAndAdjSupervisorId())
                    .reconcilerId(department.getReconcilerId())
                    .regionCode(department.getRegionCode())
                    .regionDescription(department.getRegionDescription())
                    .revenueType(department.getRevenueType())
                    .revenueTypeDescription(department.getRevenueTypeDescription())
                    .riskPortfolioId(department.getRiskPortfolioId())
                    .specialisationIndicator(department.getSpecialisationIndicator())
                    .taxCategory(department.getTaxCategory())
                    .volkerRuleDescription(department.getVolkerRuleDescription())
                    .volkerRuleIndicator(department.getVolkerRuleIndicator())
                    .fxCentrallyMaangedName(department.getFxCentrallyMaangedName())
                    .volckerSubDivision(department.getVolckerSubDivision())
                    .volckerSubDivisionDescription(department.getVolckerSubDivisionDescription())
                    .ihcHcdFlag(department.getIhcHcdFlag())
                    .coveredDeptFlag(department.getCoveredDeptFlag())
                    .deltaFlag(department.getDeltaFlag())
                    .remittancePolicyCode(department.getRemittancePolicyCode())
                    .firstInstanceDeptActivated(department.getFirstInstanceDeptActivated())
                    .firstInstanceDeptInactivated(department.getFirstInstanceDeptInactivated())
                    .lastInstanceDeptActivated(department.getLastInstanceDeptActivated())
                    .lastInstanceDeptInactivated(department.getLastInstanceDeptInactivated())
                    .rbpnlFlag(department.getRbpnlFlag())
                    .ivrGroupLongName(department.getIvrGroupLongName())
                    .frtbDeptCategory(department.getFrtbDeptCategory())
                    .build();
        return null;
    }

}
