package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.PrimeSwapPosition;
import com.cs.qfc.data.model.PrimeSwapPositionDTO;


/**
 * @author london-databases
 */


public class DTOToPrimeSwapPositionConverter {


    public static PrimeSwapPosition convert(PrimeSwapPositionDTO primeSwapPositionDto) {
        if (primeSwapPositionDto != null)
            return PrimeSwapPosition.builder().id(primeSwapPositionDto.getId())
                    .businessDate(primeSwapPositionDto.getBusinessDate())
                    .swapId(primeSwapPositionDto.getSwapId())
                    .swapVersion(primeSwapPositionDto.getSwapVersion())
                    .positionId(primeSwapPositionDto.getPositionId())
                    .positionVersion(primeSwapPositionDto.getPositionVersion())
                    .swapType(primeSwapPositionDto.getSwapType())
                    .counterpartyCode(primeSwapPositionDto.getCounterpartyCode())
                    .counterpartyName(primeSwapPositionDto.getCounterpartyName())
                    .settledLtdCostBase(primeSwapPositionDto.getSettledLtdCostBase())
                    .swapCcyToUsdFx(primeSwapPositionDto.getSwapCcyToUsdFx())

                    .build();
        return null;
    }

}
