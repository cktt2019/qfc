package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Portfolio;
import com.cs.qfc.data.model.PortfolioDTO;


/**
 * @author london-databases
 */


public class DTOToPortfolioConverter {


    public static Portfolio convert(PortfolioDTO portfolioDto) {
        if (portfolioDto != null)
            return Portfolio.builder().portfolioId(portfolioDto.getPortfolioId())
                    .globalPartyId(portfolioDto.getGlobalPartyId())
                    .partytypeDescription(portfolioDto.getPartytypeDescription())
                    .legalName(portfolioDto.getLegalName())
                    .partyAliasName(portfolioDto.getPartyAliasName())
                    .legalRoleDefinitionId(portfolioDto.getLegalRoleDefinitionId())
                    .lrdRoleType(portfolioDto.getLrdRoleType())
                    .lrdRoleDescription(portfolioDto.getLrdRoleDescription())
                    .lrdRoleCode(portfolioDto.getLrdRoleCode())
                    .partyLegalRoleId(portfolioDto.getPartyLegalRoleId())
                    .sourceId(portfolioDto.getSourceId())
                    .sourceName(portfolioDto.getSourceName())
                    .internalPartyAltId(portfolioDto.getInternalPartyAltId())

                    .build();
        return null;
    }

}
