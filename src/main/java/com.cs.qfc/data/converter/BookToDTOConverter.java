package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Book;
import com.cs.qfc.data.model.BookDTO;


/**
 * @author london-databases
 */


public class BookToDTOConverter {


    public static BookDTO convert(Book book) {
        if (book != null)
            return BookDTO.builder().globalBookId(book.getGlobalBookId())
                    .accountMethod(book.getAccountMethod())
                    .accountingBasisIndicator(book.getAccountingBasisIndicator())
                    .bookCategoryCode(book.getBookCategoryCode())
                    .bookCategoryDescription(book.getBookCategoryDescription())
                    .bookControllerId(book.getBookControllerId())
                    .bookDormantIndicator(book.getBookDormantIndicator())
                    .bookName(book.getBookName())
                    .bookPnlSystemName(book.getBookPnlSystemName())
                    .bookRef(book.getBookRef())
                    .bookStatus(book.getBookStatus())
                    .bookSubCategoryCode(book.getBookSubCategoryCode())
                    .bookSubCategoryDescription(book.getBookSubCategoryDescription())
                    .bookTraderId(book.getBookTraderId())
                    .cnyDeliverableOutsideChinaIndicator(book.getCnyDeliverableOutsideChinaIndicator())
                    .departmentId(book.getDepartmentId())
                    .frontOfficeRepresentativeId(book.getFrontOfficeRepresentativeId())
                    .gplAtomCode(book.getGplAtomCode())
                    .nhfsIndicator(book.getNhfsIndicator())
                    .remoteBookingPolicyIndicator(book.getRemoteBookingPolicyIndicator())
                    .splitHedgeComments(book.getSplitHedgeComments())
                    .splitHedgeIndicator(book.getSplitHedgeIndicator())
                    .usTaxCategoryCode(book.getUsTaxCategoryCode())
                    .bookLastUpdatedDatetime(book.getBookLastUpdatedDatetime())
                    .financialBusinessUnitCode(book.getFinancialBusinessUnitCode())
                    .deltaFlag(book.getDeltaFlag())
                    .bookRequestStatusCode(book.getBookRequestStatusCode())
                    .firstInstanceBookActivated(book.getFirstInstanceBookActivated())
                    .lastInstanceBookActivated(book.getLastInstanceBookActivated())
                    .firstInstanceBookInactivated(book.getFirstInstanceBookInactivated())
                    .lastInstanceBook(book.getLastInstanceBook())
                    .emailAddress(book.getEmailAddress())
                    .cavFlag(book.getCavFlag())
                    .fosFlag(book.getFosFlag())
                    .bookingIntent(book.getBookingIntent())
                    .bookRequestId(book.getBookRequestId())
                    .iparEmplIdEmailId(book.getIparEmplIdEmailId())
                    .bookTraderEmailId(book.getBookTraderEmailId())
                    .pcSupervisorEmailId(book.getPcSupervisorEmailId())
                    .bookEmpIdEmailId(book.getBookEmpIdEmailId())
                    .pctnlEmplIdEmailId(book.getPctnlEmplIdEmailId())
                    .build();
        return null;
    }

}
