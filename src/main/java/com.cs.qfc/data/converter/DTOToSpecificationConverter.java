package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Specification;
import com.cs.qfc.data.model.SpecificationDTO;


/**
 * @author london-databases
 */


public class DTOToSpecificationConverter {


    public static Specification convert(SpecificationDTO specificationDto) {
        if (specificationDto != null)
            return Specification.builder().id(specificationDto.getId())
                    .specificationName(specificationDto.getSpecificationName())
                    .specificationDescription(specificationDto.getSpecificationDescription())
                    .specificationVersion(specificationDto.getSpecificationVersion())
                    .specificationContent(specificationDto.getSpecificationContent())

                    .build();
        return null;
    }

}
