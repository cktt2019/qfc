package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CollateralDetailData;
import com.cs.qfc.data.model.CollateralDetailDataDTO;


/**
 * @author london-databases
 */


public class CollateralDetailDataToDTOConverter {


    public static CollateralDetailDataDTO convert(CollateralDetailData collateralDetailData) {
        if (collateralDetailData != null)
            return CollateralDetailDataDTO.builder().id(collateralDetailData.getId())
                    .sourceTxnId(collateralDetailData.getSourceTxnId())
                    .a41AsOfDate(collateralDetailData.getA41AsOfDate())
                    .a42RecordsEntityIdentifier(collateralDetailData.getA42RecordsEntityIdentifier())
                    .a43collateraoPostedReceivedFlag(collateralDetailData.getA43collateraoPostedReceivedFlag())
                    .a44CounterpartyIdentifier(collateralDetailData.getA44CounterpartyIdentifier())
                    .a45NettingAgreementIdentifier(collateralDetailData.getA45NettingAgreementIdentifier())
                    .a46UniqueCollateralItemIdentifier(collateralDetailData.getA46UniqueCollateralItemIdentifier())
                    .a47OriginalFaceAmtCollateralItemLocalCurr(collateralDetailData.getA47OriginalFaceAmtCollateralItemLocalCurr())
                    .a48LocalCurrencyOfCollateralItem(collateralDetailData.getA48LocalCurrencyOfCollateralItem())
                    .a49MarketValueAmountOfCollateralItemInUsd(collateralDetailData.getA49MarketValueAmountOfCollateralItemInUsd())
                    .a410DescriptionOfCollateralItem(collateralDetailData.getA410DescriptionOfCollateralItem())
                    .a411AssetClassification(collateralDetailData.getA411AssetClassification())
                    .a412CollateralPortfolioSegregationStatus(collateralDetailData.getA412CollateralPortfolioSegregationStatus())
                    .a413CollateralLocation(collateralDetailData.getA413CollateralLocation())
                    .a414CollateralJurisdiction(collateralDetailData.getA414CollateralJurisdiction())
                    .a415CollateralRehypoAllowed(collateralDetailData.getA415CollateralRehypoAllowed())
                    .build();
        return null;
    }

}
