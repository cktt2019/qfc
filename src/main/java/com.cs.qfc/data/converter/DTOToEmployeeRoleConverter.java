package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.EmployeeRole;
import com.cs.qfc.data.model.EmployeeRoleDTO;


/**
 * @author london-databases
 */


public class DTOToEmployeeRoleConverter {


    public static EmployeeRole convert(EmployeeRoleDTO employeeRoleDto) {
        if (employeeRoleDto != null)
            return EmployeeRole.builder().employeeId(employeeRoleDto.getEmployeeId())
                    .employeeActiveIndicator(employeeRoleDto.getEmployeeActiveIndicator())
                    .employeeEffectiveDate(employeeRoleDto.getEmployeeEffectiveDate())
                    .employeeEmailId(employeeRoleDto.getEmployeeEmailId())
                    .employeeFirstName(employeeRoleDto.getEmployeeFirstName())
                    .employeeLastName(employeeRoleDto.getEmployeeLastName())
                    .employeeLastUpdatedBy(employeeRoleDto.getEmployeeLastUpdatedBy())
                    .employeeLastUpdatedDatetime(employeeRoleDto.getEmployeeLastUpdatedDatetime())
                    .employeeLogonDomain(employeeRoleDto.getEmployeeLogonDomain())
                    .employeeLogonId(employeeRoleDto.getEmployeeLogonId())
                    .employeePidId(employeeRoleDto.getEmployeePidId())
                    .city(employeeRoleDto.getCity())
                    .region(employeeRoleDto.getRegion())
                    .countryLongDesc(employeeRoleDto.getCountryLongDesc())
                    .deltaFlag(employeeRoleDto.getDeltaFlag())

                    .build();
        return null;
    }

}
