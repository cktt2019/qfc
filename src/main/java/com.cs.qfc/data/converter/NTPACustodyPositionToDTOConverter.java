package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.NTPACustodyPosition;
import com.cs.qfc.data.model.NTPACustodyPositionDTO;


/**
 * @author london-databases
 */


public class NTPACustodyPositionToDTOConverter {


    public static NTPACustodyPositionDTO convert(NTPACustodyPosition nTPACustodyPosition) {
        if (nTPACustodyPosition != null)
            return NTPACustodyPositionDTO.builder().businessDate(nTPACustodyPosition.getBusinessDate())
                    .compIdC(nTPACustodyPosition.getCompIdC())
                    .pIdC(nTPACustodyPosition.getPIdC())
                    .cAcIdC(nTPACustodyPosition.getCAcIdC())
                    .cAcSetlmtTypC(nTPACustodyPosition.getCAcSetlmtTypC())
                    .curIdC(nTPACustodyPosition.getCurIdC())
                    .pnbPosD(nTPACustodyPosition.getPnbPosD())
                    .tNatvCurIdC(nTPACustodyPosition.getTNatvCurIdC())
                    .tBaseCurIdC(nTPACustodyPosition.getTBaseCurIdC())
                    .tConCurIdC(nTPACustodyPosition.getTConCurIdC())
                    .pnbMktValM(nTPACustodyPosition.getPnbMktValM())
                    .pnbMktValBaseM(nTPACustodyPosition.getPnbMktValBaseM())
                    .pnbMktValConM(nTPACustodyPosition.getPnbMktValConM())
                    .pPxClM(nTPACustodyPosition.getPPxClM())
                    .pnbActvyLastId(nTPACustodyPosition.getPnbActvyLastId())
                    .cIdC(nTPACustodyPosition.getCIdC())
                    .cN(nTPACustodyPosition.getCN())
                    .cAcTypC(nTPACustodyPosition.getCAcTypC())
                    .cAcEfD(nTPACustodyPosition.getCAcEfD())
                    .pMatyD(nTPACustodyPosition.getPMatyD())
                    .pStdT(nTPACustodyPosition.getPStdT())
                    .pIsinIdC(nTPACustodyPosition.getPIsinIdC())
                    .pSedolIdC(nTPACustodyPosition.getPSedolIdC())
                    .pFileTypC(nTPACustodyPosition.getPFileTypC())
                    .pnbAcctTypC(nTPACustodyPosition.getPnbAcctTypC())
                    .id(nTPACustodyPosition.getId())
                    .build();
        return null;
    }

}
