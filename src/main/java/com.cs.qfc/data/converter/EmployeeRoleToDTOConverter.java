package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.EmployeeRole;
import com.cs.qfc.data.model.EmployeeRoleDTO;


/**
 * @author london-databases
 */


public class EmployeeRoleToDTOConverter {


    public static EmployeeRoleDTO convert(EmployeeRole employeeRole) {
        if (employeeRole != null)
            return EmployeeRoleDTO.builder().employeeId(employeeRole.getEmployeeId())
                    .employeeActiveIndicator(employeeRole.getEmployeeActiveIndicator())
                    .employeeEffectiveDate(employeeRole.getEmployeeEffectiveDate())
                    .employeeEmailId(employeeRole.getEmployeeEmailId())
                    .employeeFirstName(employeeRole.getEmployeeFirstName())
                    .employeeLastName(employeeRole.getEmployeeLastName())
                    .employeeLastUpdatedBy(employeeRole.getEmployeeLastUpdatedBy())
                    .employeeLastUpdatedDatetime(employeeRole.getEmployeeLastUpdatedDatetime())
                    .employeeLogonDomain(employeeRole.getEmployeeLogonDomain())
                    .employeeLogonId(employeeRole.getEmployeeLogonId())
                    .employeePidId(employeeRole.getEmployeePidId())
                    .city(employeeRole.getCity())
                    .region(employeeRole.getRegion())
                    .countryLongDesc(employeeRole.getCountryLongDesc())
                    .deltaFlag(employeeRole.getDeltaFlag())
                    .build();
        return null;
    }

}
