package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Book;
import com.cs.qfc.data.model.BookDTO;


/**
 * @author london-databases
 */


public class DTOToBookConverter {


    public static Book convert(BookDTO bookDto) {
        if (bookDto != null)
            return Book.builder().globalBookId(bookDto.getGlobalBookId())
                    .accountMethod(bookDto.getAccountMethod())
                    .accountingBasisIndicator(bookDto.getAccountingBasisIndicator())
                    .bookCategoryCode(bookDto.getBookCategoryCode())
                    .bookCategoryDescription(bookDto.getBookCategoryDescription())
                    .bookControllerId(bookDto.getBookControllerId())
                    .bookDormantIndicator(bookDto.getBookDormantIndicator())
                    .bookName(bookDto.getBookName())
                    .bookPnlSystemName(bookDto.getBookPnlSystemName())
                    .bookRef(bookDto.getBookRef())
                    .bookStatus(bookDto.getBookStatus())
                    .bookSubCategoryCode(bookDto.getBookSubCategoryCode())
                    .bookSubCategoryDescription(bookDto.getBookSubCategoryDescription())
                    .bookTraderId(bookDto.getBookTraderId())
                    .cnyDeliverableOutsideChinaIndicator(bookDto.getCnyDeliverableOutsideChinaIndicator())
                    .departmentId(bookDto.getDepartmentId())
                    .frontOfficeRepresentativeId(bookDto.getFrontOfficeRepresentativeId())
                    .gplAtomCode(bookDto.getGplAtomCode())
                    .nhfsIndicator(bookDto.getNhfsIndicator())
                    .remoteBookingPolicyIndicator(bookDto.getRemoteBookingPolicyIndicator())
                    .splitHedgeComments(bookDto.getSplitHedgeComments())
                    .splitHedgeIndicator(bookDto.getSplitHedgeIndicator())
                    .usTaxCategoryCode(bookDto.getUsTaxCategoryCode())
                    .bookLastUpdatedDatetime(bookDto.getBookLastUpdatedDatetime())
                    .financialBusinessUnitCode(bookDto.getFinancialBusinessUnitCode())
                    .deltaFlag(bookDto.getDeltaFlag())
                    .bookRequestStatusCode(bookDto.getBookRequestStatusCode())
                    .firstInstanceBookActivated(bookDto.getFirstInstanceBookActivated())
                    .lastInstanceBookActivated(bookDto.getLastInstanceBookActivated())
                    .firstInstanceBookInactivated(bookDto.getFirstInstanceBookInactivated())
                    .lastInstanceBook(bookDto.getLastInstanceBook())
                    .emailAddress(bookDto.getEmailAddress())
                    .cavFlag(bookDto.getCavFlag())
                    .fosFlag(bookDto.getFosFlag())
                    .bookingIntent(bookDto.getBookingIntent())
                    .bookRequestId(bookDto.getBookRequestId())
                    .iparEmplIdEmailId(bookDto.getIparEmplIdEmailId())
                    .bookTraderEmailId(bookDto.getBookTraderEmailId())
                    .pcSupervisorEmailId(bookDto.getPcSupervisorEmailId())
                    .bookEmpIdEmailId(bookDto.getBookEmpIdEmailId())
                    .pctnlEmplIdEmailId(bookDto.getPctnlEmplIdEmailId())

                    .build();
        return null;
    }

}
