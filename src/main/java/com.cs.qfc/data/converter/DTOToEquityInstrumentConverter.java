package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.EquityInstrument;
import com.cs.qfc.data.model.EquityInstrumentDTO;


/**
 * @author london-databases
 */


public class DTOToEquityInstrumentConverter {


    public static EquityInstrument convert(EquityInstrumentDTO equityInstrumentDto) {
        if (equityInstrumentDto != null)
            return EquityInstrument.builder().gpiIssue(equityInstrumentDto.getGpiIssue())
                    .isinId(equityInstrumentDto.getIsinId())
                    .sedolId(equityInstrumentDto.getSedolId())
                    .cusipId(equityInstrumentDto.getCusipId())
                    .securityDescriptionLong(equityInstrumentDto.getSecurityDescriptionLong())

                    .build();
        return null;
    }

}
