package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.ObjectModel;
import com.cs.qfc.data.model.ObjectModelDTO;


/**
 * @author london-databases
 */


public class DTOToObjectModelConverter {


    public static ObjectModel convert(ObjectModelDTO objectModelDTO) {
        if (objectModelDTO != null)
            return ObjectModel.builder().id(objectModelDTO.getId())
                    .accountId(objectModelDTO.getAccountId())
                    .objectName(objectModelDTO.getObjectName())
                    .objectDescription(objectModelDTO.getObjectDescription())
                    .objectVersion(objectModelDTO.getObjectVersion())
                    .objectContent(objectModelDTO.getObjectContent())

                    .build();
        return null;
    }

}
