package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.MarnaG;
import com.cs.qfc.data.model.MarnaGDTO;


/**
 * @author london-databases
 */


public class DTOToMarnaGConverter {


    public static MarnaG convert(MarnaGDTO marnaGDto) {
        if (marnaGDto != null)
            return MarnaG.builder().cnid(marnaGDto.getCnid())
                    .agreementNumber(marnaGDto.getAgreementNumber())
                    .status(marnaGDto.getStatus())
                    .supersededByCnid(marnaGDto.getSupersededByCnid())
                    .supersededByAgreement(marnaGDto.getSupersededByAgreement())
                    .legalAgreementDescription(marnaGDto.getLegalAgreementDescription())
                    .csEntityBookinggroupId(marnaGDto.getCsEntityBookinggroupId())
                    .csEntityBookinggroupName(marnaGDto.getCsEntityBookinggroupName())
                    .negotiator(marnaGDto.getNegotiator())
                    .negotiatorLocation(marnaGDto.getNegotiatorLocation())
                    .counterParty(marnaGDto.getCounterParty())
                    .counterPartyRole(marnaGDto.getCounterPartyRole())
                    .csid(marnaGDto.getCsid())
                    .legalId(marnaGDto.getLegalId())
                    .formerlyKnownAs(marnaGDto.getFormerlyKnownAs())
                    .countryOfInc(marnaGDto.getCountryOfInc())
                    .contractType(marnaGDto.getContractType())
                    .requestType(marnaGDto.getRequestType())
                    .agreementTitle(marnaGDto.getAgreementTitle())
                    .contractDate(marnaGDto.getContractDate())
                    .agency(marnaGDto.getAgency())
                    .collateralAgreementFlag(marnaGDto.getCollateralAgreementFlag())
                    .csaEnforceableFlag(marnaGDto.getCsaEnforceableFlag())
                    .transactionSpecificFlag(marnaGDto.getTransactionSpecificFlag())
                    .csEntityAgencyFlag(marnaGDto.getCsEntityAgencyFlag())
                    .desk(marnaGDto.getDesk())
                    .iconid(marnaGDto.getIconid())
                    .csaRegFlag(marnaGDto.getCsaRegFlag())

                    .build();
        return null;
    }

}
