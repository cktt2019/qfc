package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.ClientHierarchy;
import com.cs.qfc.data.model.ClientHierarchyDTO;


/**
 * @author london-databases
 */


public class ClientHierarchyToDTOConverter {


    public static ClientHierarchyDTO convert(ClientHierarchy clientHierarchy) {
        if (clientHierarchy != null)
            return ClientHierarchyDTO.builder().csid(clientHierarchy.getCsid())
                    .gsid(clientHierarchy.getGsid())
                    .counterPartyName(clientHierarchy.getCounterPartyName())
                    .immediateParentCsid(clientHierarchy.getImmediateParentCsid())
                    .immediateParentGsid(clientHierarchy.getImmediateParentGsid())
                    .ultimateParentCsid(clientHierarchy.getUltimateParentCsid())
                    .ultimateParentGsid(clientHierarchy.getUltimateParentGsid())
                    .level(clientHierarchy.getLevel())
                    .build();
        return null;
    }

}
