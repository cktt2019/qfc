package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.PositionLevelData;
import com.cs.qfc.data.model.PositionLevelDataDTO;


/**
 * @author london-databases
 */


public class DTOToPositionLevelDataConverter {


    public static PositionLevelData convert(PositionLevelDataDTO positionLevelDataDto) {
        if (positionLevelDataDto != null)
            return PositionLevelData.builder().id(positionLevelDataDto.getId())
                    .a11AsOfDate(positionLevelDataDto.getA11AsOfDate())
                    .a12RecordsEntityIdentifier(positionLevelDataDto.getA12RecordsEntityIdentifier())
                    .a13PositionsIdentifier(positionLevelDataDto.getA13PositionsIdentifier())
                    .a15InternalBookingLocationIdentifier(positionLevelDataDto.getA15InternalBookingLocationIdentifier())
                    .a16UniqueBookingUnitDeskIdentifier(positionLevelDataDto.getA16UniqueBookingUnitDeskIdentifier())
                    .a17TypeOfQfc(positionLevelDataDto.getA17TypeOfQfc())
                    .a171TypeOfQfcCoveredGuarantee(positionLevelDataDto.getA171TypeOfQfcCoveredGuarantee())
                    .a172UnderlyingQfcObligatorIdentifier(positionLevelDataDto.getA172UnderlyingQfcObligatorIdentifier())
                    .a18AgreementIdentifier(positionLevelDataDto.getA18AgreementIdentifier())
                    .a19NettingAgreementIdrntifier(positionLevelDataDto.getA19NettingAgreementIdrntifier())
                    .a110NettingAgreementCounterpartyIdentifier(positionLevelDataDto.getA110NettingAgreementCounterpartyIdentifier())
                    .a111TradeDate(positionLevelDataDto.getA111TradeDate())
                    .a112TerminationDate(positionLevelDataDto.getA112TerminationDate())
                    .a113NextPutCallCancellationDate(positionLevelDataDto.getA113NextPutCallCancellationDate())
                    .a114NextPaymentDate(positionLevelDataDto.getA114NextPaymentDate())
                    .a115LocalCurrencyOfPosition(positionLevelDataDto.getA115LocalCurrencyOfPosition())
                    .a116CurrentMarketValueInLocalCurrency(positionLevelDataDto.getA116CurrentMarketValueInLocalCurrency())
                    .a117CurrentMarketValueInUsd(positionLevelDataDto.getA117CurrentMarketValueInUsd())
                    .a118AssetClassification(positionLevelDataDto.getA118AssetClassification())
                    .a119NotionalPrincipalAmountLocalCurrency(positionLevelDataDto.getA119NotionalPrincipalAmountLocalCurrency())
                    .a120NotionalPrincipalAmountUsd(positionLevelDataDto.getA120NotionalPrincipalAmountUsd())
                    .a121CoveredByThirdPartyCreditEnhancement(positionLevelDataDto.getA121CoveredByThirdPartyCreditEnhancement())
                    .a1211ThirdPartyCreditEnhancementProviderIdentifier(positionLevelDataDto.getA1211ThirdPartyCreditEnhancementProviderIdentifier())
                    .a1212ThirdPartyCreditEnhancementAgreementIdentifier(positionLevelDataDto.getA1212ThirdPartyCreditEnhancementAgreementIdentifier())
                    .a213CounterpartyThirdPartyCreditEnhancement(positionLevelDataDto.getA213CounterpartyThirdPartyCreditEnhancement())
                    .a1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier(positionLevelDataDto.getA1214CounterpartyThirdPartyCreditEnhancementProviderIdentifier())
                    .a1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier(positionLevelDataDto.getA1215CounterpartyThirdPartyCreditEnhancementAgreementIdentifier())
                    .a122RelatedPositionOfRecordsEntity(positionLevelDataDto.getA122RelatedPositionOfRecordsEntity())
                    .a123ReferenceNumberForAnyRelatedLoan(positionLevelDataDto.getA123ReferenceNumberForAnyRelatedLoan())
                    .a124IdentifierOfTheLenderOfRelatedLoan(positionLevelDataDto.getA124IdentifierOfTheLenderOfRelatedLoan())
                    .sourceTxnId(positionLevelDataDto.getSourceTxnId())
                    .a14CounterpartyIdentifier(positionLevelDataDto.getA14CounterpartyIdentifier())

                    .build();
        return null;
    }

}
