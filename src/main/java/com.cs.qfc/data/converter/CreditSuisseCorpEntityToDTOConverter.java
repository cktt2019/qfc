package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CreditSuisseCorpEntity;
import com.cs.qfc.data.model.CreditSuisseCorpEntityDTO;


/**
 * @author london-databases
 */


public class CreditSuisseCorpEntityToDTOConverter {


    public static CreditSuisseCorpEntityDTO convert(CreditSuisseCorpEntity creditSuisseCorpEntity) {
        if (creditSuisseCorpEntity != null)
            return CreditSuisseCorpEntityDTO.builder().count(creditSuisseCorpEntity.getCount())
                    .legalNameOfEntity(creditSuisseCorpEntity.getLegalNameOfEntity())
                    .lei(creditSuisseCorpEntity.getLei())
                    .immediateParentName(creditSuisseCorpEntity.getImmediateParentName())
                    .immediateParentLei(creditSuisseCorpEntity.getImmediateParentLei())
                    .percentageOwnership(creditSuisseCorpEntity.getPercentageOwnership())
                    .entityType(creditSuisseCorpEntity.getEntityType())
                    .domicile(creditSuisseCorpEntity.getDomicile())
                    .jurisdictionIncorp(creditSuisseCorpEntity.getJurisdictionIncorp())
                    .build();
        return null;
    }

}
