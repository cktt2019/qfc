package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Specification;
import com.cs.qfc.data.model.SpecificationDTO;


/**
 * @author london-databases
 */


public class SpecificationToDTOConverter {


    public static SpecificationDTO convert(Specification specification) {
        if (specification != null)
            return SpecificationDTO.builder().id(specification.getId())
                    .specificationName(specification.getSpecificationName())
                    .specificationDescription(specification.getSpecificationDescription())
                    .specificationVersion(specification.getSpecificationVersion())
                    .specificationContent(specification.getSpecificationContent())
                    .build();
        return null;
    }

}
