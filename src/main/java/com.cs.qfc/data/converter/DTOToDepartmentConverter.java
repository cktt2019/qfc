package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Department;
import com.cs.qfc.data.model.DepartmentDTO;


/**
 * @author london-databases
 */


public class DTOToDepartmentConverter {


    public static Department convert(DepartmentDTO departmentDto) {
        if (departmentDto != null)
            return Department.builder().departmentId(departmentDto.getDepartmentId())
                    .businessControllerId(departmentDto.getBusinessControllerId())
                    .controllerId(departmentDto.getControllerId())
                    .departmentCategoryCode(departmentDto.getDepartmentCategoryCode())
                    .departmentCategoryName(departmentDto.getDepartmentCategoryName())
                    .departmentDormantIndicator(departmentDto.getDepartmentDormantIndicator())
                    .departmentLongName(departmentDto.getDepartmentLongName())
                    .departmentName(departmentDto.getDepartmentName())
                    .departmentReconcilerGroupName(departmentDto.getDepartmentReconcilerGroupName())
                    .departmentStatus(departmentDto.getDepartmentStatus())
                    .departmentTraderId(departmentDto.getDepartmentTraderId())
                    .departmentValidFromDate(departmentDto.getDepartmentValidFromDate())
                    .departmentValidToDate(departmentDto.getDepartmentValidToDate())
                    .financialBusinessUnitCode(departmentDto.getFinancialBusinessUnitCode())
                    .ivrGroup(departmentDto.getIvrGroup())
                    .l1SupervisorId(departmentDto.getL1SupervisorId())
                    .misOfficeCode(departmentDto.getMisOfficeCode())
                    .misOfficeName(departmentDto.getMisOfficeName())
                    .ownerL2Id(departmentDto.getOwnerL2Id())
                    .recAndAdjControllerId(departmentDto.getRecAndAdjControllerId())
                    .recAndAdjSupervisorId(departmentDto.getRecAndAdjSupervisorId())
                    .reconcilerId(departmentDto.getReconcilerId())
                    .regionCode(departmentDto.getRegionCode())
                    .regionDescription(departmentDto.getRegionDescription())
                    .revenueType(departmentDto.getRevenueType())
                    .revenueTypeDescription(departmentDto.getRevenueTypeDescription())
                    .riskPortfolioId(departmentDto.getRiskPortfolioId())
                    .specialisationIndicator(departmentDto.getSpecialisationIndicator())
                    .taxCategory(departmentDto.getTaxCategory())
                    .volkerRuleDescription(departmentDto.getVolkerRuleDescription())
                    .volkerRuleIndicator(departmentDto.getVolkerRuleIndicator())
                    .fxCentrallyMaangedName(departmentDto.getFxCentrallyMaangedName())
                    .volckerSubDivision(departmentDto.getVolckerSubDivision())
                    .volckerSubDivisionDescription(departmentDto.getVolckerSubDivisionDescription())
                    .ihcHcdFlag(departmentDto.getIhcHcdFlag())
                    .coveredDeptFlag(departmentDto.getCoveredDeptFlag())
                    .deltaFlag(departmentDto.getDeltaFlag())
                    .remittancePolicyCode(departmentDto.getRemittancePolicyCode())
                    .firstInstanceDeptActivated(departmentDto.getFirstInstanceDeptActivated())
                    .firstInstanceDeptInactivated(departmentDto.getFirstInstanceDeptInactivated())
                    .lastInstanceDeptActivated(departmentDto.getLastInstanceDeptActivated())
                    .lastInstanceDeptInactivated(departmentDto.getLastInstanceDeptInactivated())
                    .rbpnlFlag(departmentDto.getRbpnlFlag())
                    .ivrGroupLongName(departmentDto.getIvrGroupLongName())
                    .frtbDeptCategory(departmentDto.getFrtbDeptCategory())

                    .build();
        return null;
    }

}
