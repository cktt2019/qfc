package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.DrdTradeInstrument;
import com.cs.qfc.data.model.DrdTradeInstrumentDTO;


/**
 * @author london-databases
 */


public class DrdTradeInstrumentToDTOConverter {


    public static DrdTradeInstrumentDTO convert(DrdTradeInstrument drdTradeInstrument) {
        if (drdTradeInstrument != null)
            return DrdTradeInstrumentDTO.builder().id(drdTradeInstrument.getId())
                    .cobDate(drdTradeInstrument.getCobDate())
                    .gbmEntity(drdTradeInstrument.getGbmEntity())
                    .tradeId(drdTradeInstrument.getTradeId())
                    .counterPartyId(drdTradeInstrument.getCounterPartyId())
                    .bookName(drdTradeInstrument.getBookName())
                    .assetClass(drdTradeInstrument.getAssetClass())
                    .cduMasterAgreementLlk(drdTradeInstrument.getCduMasterAgreementLlk())
                    .cduCollateralAnnexLlk(drdTradeInstrument.getCduCollateralAnnexLlk())
                    .framesoftMasterAgreementLlk(drdTradeInstrument.getFramesoftMasterAgreementLlk())
                    .algoCollateralAnnexLlk(drdTradeInstrument.getAlgoCollateralAnnexLlk())
                    .tradeDate(drdTradeInstrument.getTradeDate())
                    .primaryMaturityDate(drdTradeInstrument.getPrimaryMaturityDate())
                    .localPvCcyCode(drdTradeInstrument.getLocalPvCcyCode())
                    .localPv(drdTradeInstrument.getLocalPv())
                    .build();
        return null;
    }

}
