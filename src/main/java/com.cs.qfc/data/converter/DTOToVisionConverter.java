package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Vision;
import com.cs.qfc.data.model.VisionDTO;


public class DTOToVisionConverter {


    public static Vision convert(VisionDTO visionDto) {
        if (visionDto != null)
            return Vision.builder().businessDate(visionDto.getBusinessDate())
                    .entity(visionDto.getEntity())
                    .cpty(visionDto.getCpty())
                    .book(visionDto.getBook())
                    .proveNameProduct(visionDto.getProveNameProduct())
                    .agreementId1(visionDto.getAgreementId1())
                    .tradeDate(visionDto.getTradeDate())
                    .endDate(visionDto.getEndDate())
                    .bondNextCoup(visionDto.getBondNextCoup())
                    .cashCurr(visionDto.getCashCurr())
                    .pvUsd(visionDto.getPvUsd())
                    .defaultFvLevel(visionDto.getDefaultFvLevel())
                    .face(visionDto.getFace())
                    .valueOn(visionDto.getValueOn())
                    .rehypothecationIndicator(visionDto.getRehypothecationIndicator())
                    .custodianId(visionDto.getCustodianId())
                    .id(visionDto.getId())

                    .build();
        return null;
    }

}
