package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.DebtInstrument;
import com.cs.qfc.data.model.DebtInstrumentDTO;


/**
 * @author london-databases
 */


public class DTOToDebtInstrumentConverter {


    public static DebtInstrument convert(DebtInstrumentDTO debtInstrumentDto) {
        if (debtInstrumentDto != null)
            return DebtInstrument.builder().gpi_issue(debtInstrumentDto.getGpiIssue())
                    .isinId(debtInstrumentDto.getIsinId())
                    .sedolId(debtInstrumentDto.getSedolId())
                    .securityDescriptionLong(debtInstrumentDto.getSecurityDescriptionLong())
                    .nxtPayDate(debtInstrumentDto.getNxtPayDate())
                    .nextCallDate(debtInstrumentDto.getNextCallDate())
                    .nextPutDate(debtInstrumentDto.getNextPutDate())

                    .build();
        return null;
    }

}
