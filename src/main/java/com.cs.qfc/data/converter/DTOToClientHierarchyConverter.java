package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.ClientHierarchy;
import com.cs.qfc.data.model.ClientHierarchyDTO;


/**
 * @author london-databases
 */


public class DTOToClientHierarchyConverter {


    public static ClientHierarchy convert(ClientHierarchyDTO clientHierarchyDto) {
        if (clientHierarchyDto != null)
            return ClientHierarchy.builder().csid(clientHierarchyDto.getCsid())
                    .gsid(clientHierarchyDto.getGsid())
                    .counterPartyName(clientHierarchyDto.getCounterPartyName())
                    .immediateParentCsid(clientHierarchyDto.getImmediateParentCsid())
                    .immediateParentGsid(clientHierarchyDto.getImmediateParentGsid())
                    .ultimateParentCsid(clientHierarchyDto.getUltimateParentCsid())
                    .ultimateParentGsid(clientHierarchyDto.getUltimateParentGsid())
                    .level(clientHierarchyDto.getLevel())

                    .build();
        return null;
    }

}
