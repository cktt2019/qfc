package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Portfolio;
import com.cs.qfc.data.model.PortfolioDTO;


/**
 * @author london-databases
 */


public class PortfolioToDTOConverter {


    public static PortfolioDTO convert(Portfolio portfolio) {
        if (portfolio != null)
            return PortfolioDTO.builder().portfolioId(portfolio.getPortfolioId())
                    .globalPartyId(portfolio.getGlobalPartyId())
                    .partytypeDescription(portfolio.getPartytypeDescription())
                    .legalName(portfolio.getLegalName())
                    .partyAliasName(portfolio.getPartyAliasName())
                    .legalRoleDefinitionId(portfolio.getLegalRoleDefinitionId())
                    .lrdRoleType(portfolio.getLrdRoleType())
                    .lrdRoleDescription(portfolio.getLrdRoleDescription())
                    .lrdRoleCode(portfolio.getLrdRoleCode())
                    .partyLegalRoleId(portfolio.getPartyLegalRoleId())
                    .sourceId(portfolio.getSourceId())
                    .sourceName(portfolio.getSourceName())
                    .internalPartyAltId(portfolio.getInternalPartyAltId())
                    .build();
        return null;
    }

}
