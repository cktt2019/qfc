package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CreditSuisseLegalEntity;
import com.cs.qfc.data.model.CreditSuisseLegalEntityDTO;


/**
 * @author london-databases
 */


public class DTOToCreditSuisseLegalEntityConverter {


    public static CreditSuisseLegalEntity convert(CreditSuisseLegalEntityDTO creditSuisseLegalEntityDto) {
        if (creditSuisseLegalEntityDto != null)
            return CreditSuisseLegalEntity.builder().count(creditSuisseLegalEntityDto.getCount())
                    .enterpriseDNumber(creditSuisseLegalEntityDto.getEnterpriseDNumber())
                    .bookingEntityNm(creditSuisseLegalEntityDto.getBookingEntityNm())
                    .peopleSoftId(creditSuisseLegalEntityDto.getPeopleSoftId())
                    .csidId(creditSuisseLegalEntityDto.getCsidId())

                    .build();
        return null;
    }

}
