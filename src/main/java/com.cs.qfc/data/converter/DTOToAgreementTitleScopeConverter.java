package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.AgreementTitleScope;
import com.cs.qfc.data.model.AgreementTitleScopeDTO;


/**
 * @author london-databases
 */


public class DTOToAgreementTitleScopeConverter {


    public static AgreementTitleScope convert(AgreementTitleScopeDTO agreementTitleScopeDto) {
        if (agreementTitleScopeDto != null)
            return AgreementTitleScope.builder().count(agreementTitleScopeDto.getCount())
                    .agreementTitle(agreementTitleScopeDto.getAgreementTitle())
                    .agreementType(agreementTitleScopeDto.getAgreementType())
                    .inScope(agreementTitleScopeDto.getInScope())

                    .build();
        return null;
    }

}
