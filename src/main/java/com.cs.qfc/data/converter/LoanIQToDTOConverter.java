package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.LoanIQ;
import com.cs.qfc.data.model.LoanIQDTO;


/**
 * @author london-databases
 */


public class LoanIQToDTOConverter {


    public static LoanIQDTO convert(LoanIQ loanIQ) {
        if (loanIQ != null)
            return LoanIQDTO.builder().id(loanIQ.getId())
                    .cptyCsid(loanIQ.getCptyCsid())
                    .sdsId(loanIQ.getSdsId())
                    .portfolioCode(loanIQ.getPortfolioCode())
                    .agreementId(loanIQ.getAgreementId())
                    .tradeDate(loanIQ.getTradeDate())
                    .maturityDate(loanIQ.getMaturityDate())
                    .currency(loanIQ.getCurrency())
                    .presentValue(loanIQ.getPresentValue())
                    .tradeAmount(loanIQ.getTradeAmount())
                    .recordType(loanIQ.getRecordType())
                    .swapId(loanIQ.getSwapId())
                    .build();
        return null;
    }

}
