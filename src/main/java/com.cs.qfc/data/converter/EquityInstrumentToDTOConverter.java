package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.EquityInstrument;
import com.cs.qfc.data.model.EquityInstrumentDTO;


/**
 * @author london-databases
 */


public class EquityInstrumentToDTOConverter {


    public static EquityInstrumentDTO convert(EquityInstrument equityInstrument) {
        if (equityInstrument != null)
            return EquityInstrumentDTO.builder().gpiIssue(equityInstrument.getGpiIssue())
                    .isinId(equityInstrument.getIsinId())
                    .sedolId(equityInstrument.getSedolId())
                    .cusipId(equityInstrument.getCusipId())
                    .securityDescriptionLong(equityInstrument.getSecurityDescriptionLong())
                    .build();
        return null;
    }

}
