package com.cs.qfc.data.converter;


import com.cs.qfc.data.model.PrimeSwap;


public class DTOToPrimeSwapConverter {


    public static com.cs.qfc.data.jpa.entities.PrimeSwap convert(PrimeSwap primeSwap) {
        if (primeSwap != null)
            return com.cs.qfc.data.jpa.entities.PrimeSwap.builder().businessDate(primeSwap.getBusinessDate())
                    .legalEntity(primeSwap.getLegalEntity())
                    .swapId(primeSwap.getSwapId())
                    .swapVersion(primeSwap.getSwapVersion())
                    .counterPartyCode(primeSwap.getCounterPartyCode())
                    .counterPartyName(primeSwap.getCounterPartyName())
                    .book(primeSwap.getBook())
                    .trader(primeSwap.getTrader())
                    .swapType(primeSwap.getSwapType())
                    .framesoftMasterAgreementLlk(primeSwap.getFramesoftMasterAgreementLlk())
                    .algoCollateralAgreementLlk(primeSwap.getAlgoCollateralAgreementLlk())
                    .cduMasterAgreementLlk(primeSwap.getCduMasterAgreementLlk())
                    .cduCollateralAnnexLlk(primeSwap.getCduCollateralAnnexLlk())
                    .swapTradeDate(primeSwap.getSwapTradeDate())
                    .swapTerminationDate(primeSwap.getSwapTerminationDate())
                    .nextInterestPayDate(primeSwap.getNextInterestPayDate())
                    .equityCurrency(primeSwap.getEquityCurrency())
                    .totalSwapValue(primeSwap.getTotalSwapValue())
                    .currencyEquityNotional(primeSwap.getCurrencyEquityNotional())
                    .initMargin(primeSwap.getInitMargin())
                    .initialMarginDirection(primeSwap.getInitialMarginDirection())
                    .csid(primeSwap.getCsid())
                    .gsid(primeSwap.getGsid())
                    .id(primeSwap.getId())

                    .build();
        return null;
    }

}
