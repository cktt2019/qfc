package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CounterpartyNetting;
import com.cs.qfc.data.model.CounterpartyNettingDTO;


/**
 * @author london-databases
 */


public class CounterpartyNettingToDTOConverter {


    public static CounterpartyNettingDTO convert(CounterpartyNetting counterpartyNetting) {
        if (counterpartyNetting != null)
            return CounterpartyNettingDTO.builder().id(counterpartyNetting.getId())
                    .sourceTxnId(counterpartyNetting.getSourceTxnId())
                    .a21AsOfDate(counterpartyNetting.getA21AsOfDate())
                    .a22RecordsEntityIdentifier(counterpartyNetting.getA22RecordsEntityIdentifier())
                    .a23NettingAgreementCounterpartyIdentifier(counterpartyNetting.getA23NettingAgreementCounterpartyIdentifier())
                    .a24NettingAgreementIdentifier(counterpartyNetting.getA24NettingAgreementIdentifier())
                    .a241UnderlyingQfcObligatorIdentifier(counterpartyNetting.getA241UnderlyingQfcObligatorIdentifier())
                    .a25CoveredByThirdPartyCreditEnhancement(counterpartyNetting.getA25CoveredByThirdPartyCreditEnhancement())
                    .a251ThirdPartyCreditEnhancementProviderIdentifier(counterpartyNetting.getA251ThirdPartyCreditEnhancementProviderIdentifier())
                    .a252ThirdPartyCreditEnhancementAgreementIdentifier(counterpartyNetting.getA252ThirdPartyCreditEnhancementAgreementIdentifier())
                    .a253CounterpartyThirdPartyCreditEnhancement(counterpartyNetting.getA253CounterpartyThirdPartyCreditEnhancement())
                    .a254CounterpartyThirdPartyCreditEnhancementProviderIdentifier(counterpartyNetting.getA254CounterpartyThirdPartyCreditEnhancementProviderIdentifier())
                    .a255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier(counterpartyNetting.getA255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier())
                    .a26AggregateCurrMarketValueAllPositionsUsd(counterpartyNetting.getA26AggregateCurrMarketValueAllPositionsUsd())
                    .a27AggregateCurrMarketValuePositivePositionsUsd(counterpartyNetting.getA27AggregateCurrMarketValuePositivePositionsUsd())
                    .a28AggregateCurrMarketValueNegativePositionsUsd(counterpartyNetting.getA28AggregateCurrMarketValueNegativePositionsUsd())
                    .a29CurrMarketValueAllCollateralRecordsEntityInUsd(counterpartyNetting.getA29CurrMarketValueAllCollateralRecordsEntityInUsd())
                    .a210CurrMarketValueAllCollateralCounterpartyInUsd(counterpartyNetting.getA210CurrMarketValueAllCollateralCounterpartyInUsd())
                    .a211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd(counterpartyNetting.getA211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd())
                    .a212CurrMarketValueAllCollateralCounterpartyRehypoInUsd(counterpartyNetting.getA212CurrMarketValueAllCollateralCounterpartyRehypoInUsd())
                    .a213RecordsEntityCollateralNet(counterpartyNetting.getA213RecordsEntityCollateralNet())
                    .a214CounterpartyCollateralNet(counterpartyNetting.getA214CounterpartyCollateralNet())
                    .a215NextMarginPaymentDate(counterpartyNetting.getA215NextMarginPaymentDate())
                    .a216NextMarginPaymentAmountUsd(counterpartyNetting.getA216NextMarginPaymentAmountUsd())
                    .a217SafekeepingAgentIdentifierRecordsEntity(counterpartyNetting.getA217SafekeepingAgentIdentifierRecordsEntity())
                    .a218SafekeepingAgentIdentifierCounterparty(counterpartyNetting.getA218SafekeepingAgentIdentifierCounterparty())
                    .build();
        return null;
    }

}
