package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.MarnaG;
import com.cs.qfc.data.model.MarnaGDTO;


/**
 * @author london-databases
 */


public class MarnaGToDTOConverter {


    public static MarnaGDTO convert(MarnaG marnaG) {
        if (marnaG != null)
            return MarnaGDTO.builder().cnid(marnaG.getCnid())
                    .agreementNumber(marnaG.getAgreementNumber())
                    .status(marnaG.getStatus())
                    .supersededByCnid(marnaG.getSupersededByCnid())
                    .supersededByAgreement(marnaG.getSupersededByAgreement())
                    .legalAgreementDescription(marnaG.getLegalAgreementDescription())
                    .csEntityBookinggroupId(marnaG.getCsEntityBookinggroupId())
                    .csEntityBookinggroupName(marnaG.getCsEntityBookinggroupName())
                    .negotiator(marnaG.getNegotiator())
                    .negotiatorLocation(marnaG.getNegotiatorLocation())
                    .counterParty(marnaG.getCounterParty())
                    .counterPartyRole(marnaG.getCounterPartyRole())
                    .csid(marnaG.getCsid())
                    .legalId(marnaG.getLegalId())
                    .formerlyKnownAs(marnaG.getFormerlyKnownAs())
                    .countryOfInc(marnaG.getCountryOfInc())
                    .contractType(marnaG.getContractType())
                    .requestType(marnaG.getRequestType())
                    .agreementTitle(marnaG.getAgreementTitle())
                    .contractDate(marnaG.getContractDate())
                    .agency(marnaG.getAgency())
                    .collateralAgreementFlag(marnaG.getCollateralAgreementFlag())
                    .csaEnforceableFlag(marnaG.getCsaEnforceableFlag())
                    .transactionSpecificFlag(marnaG.getTransactionSpecificFlag())
                    .csEntityAgencyFlag(marnaG.getCsEntityAgencyFlag())
                    .desk(marnaG.getDesk())
                    .iconid(marnaG.getIconid())
                    .csaRegFlag(marnaG.getCsaRegFlag())
                    .build();
        return null;
    }

}
