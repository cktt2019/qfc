package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.BusinessUnit;
import com.cs.qfc.data.model.BusinessUnitDTO;


/**
 * @author london-databases
 */


public class BusinessUnitToDTOConverter {


    public static BusinessUnitDTO convert(BusinessUnit businessUnit) {
        if (businessUnit != null)
            return BusinessUnitDTO.builder().financialBusinessUnitCode(businessUnit.getFinancialBusinessUnitCode())
                    .baseCurrency(businessUnit.getBaseCurrency())
                    .businessUnitActiveIndicator(businessUnit.getBusinessUnitActiveIndicator())
                    .businessUnitDescription(businessUnit.getBusinessUnitDescription())
                    .businessUnitEffectiveDate(businessUnit.getBusinessUnitEffectiveDate())
                    .businessUnitGermanDescription(businessUnit.getBusinessUnitGermanDescription())
                    .businessUnitGermanName(businessUnit.getBusinessUnitGermanName())
                    .businessUnitId(businessUnit.getBusinessUnitId())
                    .businessUnitLastUpdateDatetime(businessUnit.getBusinessUnitLastUpdateDatetime())
                    .businessUnitLastUpdatedBy(businessUnit.getBusinessUnitLastUpdatedBy())
                    .businessUnitName(businessUnit.getBusinessUnitName())
                    .businessUnitShortDescription(businessUnit.getBusinessUnitShortDescription())
                    .businessUnitShortGermanDescription(businessUnit.getBusinessUnitShortGermanDescription())
                    .chBusinessUnitLastUpdatedBy(businessUnit.getChBusinessUnitLastUpdatedBy())
                    .chBusinessUnitLastUpdatedDatetime(businessUnit.getChBusinessUnitLastUpdatedDatetime())
                    .customerId(businessUnit.getCustomerId())
                    .customerVendorAffiliateIndicator(businessUnit.getCustomerVendorAffiliateIndicator())
                    .financialBusinessUnitActiveIndicator(businessUnit.getFinancialBusinessUnitActiveIndicator())
                    .financialBusinessUnitEffectiveDate(businessUnit.getFinancialBusinessUnitEffectiveDate())
                    .financialBusinessUnitGroupId(businessUnit.getFinancialBusinessUnitGroupId())
                    .financialBusinessUnitGroupName(businessUnit.getFinancialBusinessUnitGroupName())
                    .financialBusinessUnitName(businessUnit.getFinancialBusinessUnitName())
                    .firmType(businessUnit.getFirmType())
                    .isoCurrencyCode(businessUnit.getIsoCurrencyCode())
                    .lcdRefNo(businessUnit.getLcdRefNo())
                    .legalEntityShortDescription(businessUnit.getLegalEntityShortDescription())
                    .legalEntityActiveIndicator(businessUnit.getLegalEntityActiveIndicator())
                    .legalEntityCode(businessUnit.getLegalEntityCode())
                    .legalEntityDescription(businessUnit.getLegalEntityDescription())
                    .legalEntityEffectiveDate(businessUnit.getLegalEntityEffectiveDate())
                    .legalEntityGermanDescription(businessUnit.getLegalEntityGermanDescription())
                    .legalEntityLastUpdatedBy(businessUnit.getLegalEntityLastUpdatedBy())
                    .legalEntityLastUpdatedDatetime(businessUnit.getLegalEntityLastUpdatedDatetime())
                    .localGaapCurrency(businessUnit.getLocalGaapCurrency())
                    .misMemberCode(businessUnit.getMisMemberCode())
                    .processingLocationCode(businessUnit.getProcessingLocationCode())
                    .sourceId(businessUnit.getSourceId())
                    .deltaFlag(businessUnit.getDeltaFlag())
                    .buDomicile(businessUnit.getBuDomicile())
                    .build();
        return null;
    }

}
