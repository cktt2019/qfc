package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.ObjectModel;
import com.cs.qfc.data.model.ObjectModelDTO;


/**
 * @author london-databases
 */


public class ObjectModelToDTOConverter {


    public static ObjectModelDTO convert(ObjectModel objectModel) {
        if (objectModel != null)
            return ObjectModelDTO.builder().id(objectModel.getId())
                    .accountId(objectModel.getAccountId())
                    .objectName(objectModel.getObjectName())
                    .objectDescription(objectModel.getObjectDescription())
                    .objectVersion(objectModel.getObjectVersion())
                    .objectContent(objectModel.getObjectContent())
                    .build();
        return null;
    }

}
