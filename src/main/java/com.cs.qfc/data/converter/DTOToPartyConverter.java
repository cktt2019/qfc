package com.cs.qfc.data.converter;


import com.cs.qfc.data.model.Party;


/**
 * @author london-databases
 */


public class DTOToPartyConverter {


    public static com.cs.qfc.data.jpa.entities.Party convert(Party party) {
        if (party != null)
            return com.cs.qfc.data.jpa.entities.Party.builder().globalPartyId(party.getGlobalPartyId())
                    .csid(party.getCsid())
                    .csidSource(party.getCsidSource())
                    .gsid(party.getGsid())
                    .gsidSource(party.getGsidSource())
                    .countryOfDomicileIsoCountryCode(party.getCountryOfDomicileIsoCountryCode())
                    .countryOfIncorporationIsoCountryCode(party.getCountryOfIncorporationIsoCountryCode())
                    .partyLegalName(party.getPartyLegalName())
                    .addressTypeCode(party.getAddressTypeCode())
                    .addressAddressLine1(party.getAddressAddressLine1())
                    .addressAddressLine2(party.getAddressAddressLine2())
                    .addressAddressLine3(party.getAddressAddressLine3())
                    .addressAddressLine4(party.getAddressAddressLine4())
                    .addressPostalCode(party.getAddressPostalCode())
                    .addressCountryIsoCountryName(party.getAddressCountryIsoCountryName())
                    .addressAddressFullPhoneNumber(party.getAddressAddressFullPhoneNumber())
                    .addressElctronicAddressId(party.getAddressElctronicAddressId())
                    .partyLegalRoleDefinitionId(party.getPartyLegalRoleDefinitionId())
                    .partyLegalRoleId(party.getPartyLegalRoleId())
                    .internalPartyIdAltSourceName(party.getInternalPartyIdAltSourceName())
                    .partyLegalRoleDescription(party.getPartyLegalRoleDescription())
                    .partyLegalStatusType(party.getPartyLegalStatusType())
                    .partyLegalStatusCode(party.getPartyLegalStatusCode())
                    .addressTypeDescription(party.getAddressTypeDescription())
                    .addressCityName(party.getAddressCityName())
                    .addressPoBoxCity(party.getAddressPoBoxCity())
                    .externalPartyAltIdSourceId(party.getExternalPartyAltIdSourceId())
                    .externalPartyAltIdSourceName(party.getExternalPartyAltIdSourceName())
                    .externalPartyAltIdSourceIdentificationNumber(party.getExternalPartyAltIdSourceIdentificationNumber())

                    .build();
        return null;
    }

}
