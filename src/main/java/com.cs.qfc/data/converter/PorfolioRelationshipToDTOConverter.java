package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.PorfolioRelationship;
import com.cs.qfc.data.model.PorfolioRelationshipDTO;


/**
 * @author london-databases
 */


public class PorfolioRelationshipToDTOConverter {


    public static PorfolioRelationshipDTO convert(PorfolioRelationship porfolioRelationship) {
        if (porfolioRelationship != null)
            return PorfolioRelationshipDTO.builder().portfolioId(porfolioRelationship.getPortfolioId())
                    .globalPartyId(porfolioRelationship.getGlobalPartyId())
                    .partytypeDescription(porfolioRelationship.getPartytypeDescription())
                    .partyTypeCode(porfolioRelationship.getPartyTypeCode())
                    .partyLegalName(porfolioRelationship.getPartyLegalName())
                    .partyAliasName(porfolioRelationship.getPartyAliasName())
                    .legalRoleDefinitionId(porfolioRelationship.getLegalRoleDefinitionId())
                    .roleType(porfolioRelationship.getRoleType())
                    .roleDescription(porfolioRelationship.getRoleDescription())
                    .roleCode(porfolioRelationship.getRoleCode())
                    .partyLegalRoleId(porfolioRelationship.getPartyLegalRoleId())
                    .sourceId(porfolioRelationship.getSourceId())
                    .sourceName(porfolioRelationship.getSourceName())
                    .internalPartyAltId(porfolioRelationship.getInternalPartyAltId())
                    .portfolioName(porfolioRelationship.getPortfolioName())
                    .portfolioNumber(porfolioRelationship.getPortfolioNumber())
                    .legalEntityName(porfolioRelationship.getLegalEntityName())
                    .cmsBookingId(porfolioRelationship.getCmsBookingId())
                    .entityJurisdiction(porfolioRelationship.getEntityJurisdiction())
                    .legalEntityCode(porfolioRelationship.getLegalEntityCode())
                    .parentTrxnCovrg(porfolioRelationship.getParentTrxnCovrg())
                    .trxnCoverage(porfolioRelationship.getTrxnCoverage())
                    .trxnCoverageDescr(porfolioRelationship.getTrxnCoverageDescr())
                    .productId(porfolioRelationship.getProductId())
                    .accountNumber(porfolioRelationship.getAccountNumber())
                    .systemId(porfolioRelationship.getSystemId())
                    .systemName(porfolioRelationship.getSystemName())
                    .mnemonicValue(porfolioRelationship.getMnemonicValue())
                    .mnemonicCode(porfolioRelationship.getMnemonicCode())
                    .build();
        return null;
    }

}
