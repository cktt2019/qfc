package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.DrdTradeLeg;
import com.cs.qfc.data.model.DrdTradeLegDTO;


/**
 * @author london-databases
 */


public class DTOToDrdTradeLegConverter {


    public static DrdTradeLeg convert(DrdTradeLegDTO drdTradeLegDto) {
        if (drdTradeLegDto != null)
            return DrdTradeLeg.builder().id(drdTradeLegDto.getId())
                    .cobDate(drdTradeLegDto.getCobDate())
                    .gbmEntity(drdTradeLegDto.getGbmEntity())
                    .tradeId(drdTradeLegDto.getTradeId())
                    .counterPartyId(drdTradeLegDto.getCounterPartyId())
                    .bookName(drdTradeLegDto.getBookName())
                    .cduMasterAgreementLlk(drdTradeLegDto.getCduMasterAgreementLlk())
                    .cduCollateralAnnexLlk(drdTradeLegDto.getCduCollateralAnnexLlk())
                    .framesoftMasterAgreementLlk(drdTradeLegDto.getFramesoftMasterAgreementLlk())
                    .algoCollateralAnnexLlk(drdTradeLegDto.getAlgoCollateralAnnexLlk())
                    .tradeDate(drdTradeLegDto.getTradeDate())
                    .primaryMaturityDate(drdTradeLegDto.getPrimaryMaturityDate())
                    .localPvCcyCode(drdTradeLegDto.getLocalPvCcyCode())
                    .localPv(drdTradeLegDto.getLocalPv())
                    .pvNotional(drdTradeLegDto.getPvNotional())

                    .build();
        return null;
    }

}
