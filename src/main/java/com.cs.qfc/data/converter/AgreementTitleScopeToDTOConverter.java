package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.AgreementTitleScope;
import com.cs.qfc.data.model.AgreementTitleScopeDTO;


/**
 * @author london-databases
 */


public class AgreementTitleScopeToDTOConverter {


    public static AgreementTitleScopeDTO convert(AgreementTitleScope agreementTitleScope) {
        if (agreementTitleScope != null)
            return AgreementTitleScopeDTO.builder().count(agreementTitleScope.getCount())
                    .agreementTitle(agreementTitleScope.getAgreementTitle())
                    .agreementType(agreementTitleScope.getAgreementType())
                    .inScope(agreementTitleScope.getInScope())
                    .build();
        return null;
    }

}
