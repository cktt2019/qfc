package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.DTPMISGBLDYTREE;
import com.cs.qfc.data.model.DTPMISGBLDYTREEDTO;


/**
 * @author london-databases
 */


public class DTOToDTPMISGBLDYTREEConverter {


    public static DTPMISGBLDYTREE convert(DTPMISGBLDYTREEDTO dTPMISGBLDYTREEDto) {
        if (dTPMISGBLDYTREEDto != null)
            return DTPMISGBLDYTREE.builder().misMemberCode(dTPMISGBLDYTREEDto.getMisMemberCode())
                    .americasProductLineHeadId(dTPMISGBLDYTREEDto.getAmericasProductLineHeadId())
                    .americasClusterHeadId(dTPMISGBLDYTREEDto.getAmericasClusterHeadId())
                    .apacClusterHeadId(dTPMISGBLDYTREEDto.getApacClusterHeadId())
                    .apacProductLineHeadId(dTPMISGBLDYTREEDto.getApacProductLineHeadId())
                    .businessControllerL2Id(dTPMISGBLDYTREEDto.getBusinessControllerL2Id())
                    .cluster(dTPMISGBLDYTREEDto.getCluster())
                    .clusterCode(dTPMISGBLDYTREEDto.getClusterCode())
                    .clusterHeadId(dTPMISGBLDYTREEDto.getClusterHeadId())
                    .desk(dTPMISGBLDYTREEDto.getDesk())
                    .deskCode(dTPMISGBLDYTREEDto.getDeskCode())
                    .division(dTPMISGBLDYTREEDto.getDivision())
                    .divisionCode(dTPMISGBLDYTREEDto.getDivisionCode())
                    .euroClusterHeadId(dTPMISGBLDYTREEDto.getEuroClusterHeadId())
                    .euroProductLineHeadId(dTPMISGBLDYTREEDto.getEuroProductLineHeadId())
                    .globalProductLineHeadId(dTPMISGBLDYTREEDto.getGlobalProductLineHeadId())
                    .hierarchyTraderId(dTPMISGBLDYTREEDto.getHierarchyTraderId())
                    .misHierarchySortCode(dTPMISGBLDYTREEDto.getMisHierarchySortCode())
                    .misMemberName(dTPMISGBLDYTREEDto.getMisMemberName())
                    .misMemberTypeCode(dTPMISGBLDYTREEDto.getMisMemberTypeCode())
                    .misProduct(dTPMISGBLDYTREEDto.getMisProduct())
                    .misRegionCode(dTPMISGBLDYTREEDto.getMisRegionCode())
                    .misTreeCode(dTPMISGBLDYTREEDto.getMisTreeCode())
                    .parentMisMemberCode(dTPMISGBLDYTREEDto.getParentMisMemberCode())
                    .productControlSectionMgrId(dTPMISGBLDYTREEDto.getProductControlSectionMgrId())
                    .productLineCode(dTPMISGBLDYTREEDto.getProductLineCode())
                    .productLineName(dTPMISGBLDYTREEDto.getProductLineName())
                    .sub3ProductLine(dTPMISGBLDYTREEDto.getSub3ProductLine())
                    .sub3ProductLineDescription(dTPMISGBLDYTREEDto.getSub3ProductLineDescription())
                    .subClusterCode(dTPMISGBLDYTREEDto.getSubClusterCode())
                    .subClusterName(dTPMISGBLDYTREEDto.getSubClusterName())
                    .subDivisionCode(dTPMISGBLDYTREEDto.getSubDivisionCode())
                    .subDivisionDescription(dTPMISGBLDYTREEDto.getSubDivisionDescription())
                    .subProductDescription(dTPMISGBLDYTREEDto.getSubProductDescription())
                    .subProductLine(dTPMISGBLDYTREEDto.getSubProductLine())
                    .swissProductLineHeadId(dTPMISGBLDYTREEDto.getSwissProductLineHeadId())
                    .tradingSupervisorId(dTPMISGBLDYTREEDto.getTradingSupervisorId())
                    .tradingSupervisorDelegateId(dTPMISGBLDYTREEDto.getTradingSupervisorDelegateId())
                    .misProductCode(dTPMISGBLDYTREEDto.getMisProductCode())
                    .misUnitPercentOwned(dTPMISGBLDYTREEDto.getMisUnitPercentOwned())
                    .misUnitPercentDescription(dTPMISGBLDYTREEDto.getMisUnitPercentDescription())
                    .deltaFlag(dTPMISGBLDYTREEDto.getDeltaFlag())
                    .cddsTreeNodeStatus(dTPMISGBLDYTREEDto.getCddsTreeNodeStatus())
                    .tradingSupport(dTPMISGBLDYTREEDto.getTradingSupport())
                    .tradeSupportManagerEmail(dTPMISGBLDYTREEDto.getTradeSupportManagerEmail())

                    .build();
        return null;
    }

}
