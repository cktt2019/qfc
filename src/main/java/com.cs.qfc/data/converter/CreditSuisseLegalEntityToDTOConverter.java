package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CreditSuisseLegalEntity;
import com.cs.qfc.data.model.CreditSuisseLegalEntityDTO;


/**
 * @author london-databases
 */


public class CreditSuisseLegalEntityToDTOConverter {


    public static CreditSuisseLegalEntityDTO convert(CreditSuisseLegalEntity creditSuisseLegalEntity) {
        if (creditSuisseLegalEntity != null)
            return CreditSuisseLegalEntityDTO.builder().count(creditSuisseLegalEntity.getCount())
                    .enterpriseDNumber(creditSuisseLegalEntity.getEnterpriseDNumber())
                    .bookingEntityNm(creditSuisseLegalEntity.getBookingEntityNm())
                    .peopleSoftId(creditSuisseLegalEntity.getPeopleSoftId())
                    .csidId(creditSuisseLegalEntity.getCsidId())
                    .build();
        return null;
    }

}
