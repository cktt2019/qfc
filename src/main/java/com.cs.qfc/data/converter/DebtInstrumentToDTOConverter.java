package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.DebtInstrument;
import com.cs.qfc.data.model.DebtInstrumentDTO;


/**
 * @author london-databases
 */


public class DebtInstrumentToDTOConverter {


    public static DebtInstrumentDTO convert(DebtInstrument debtInstrument) {
        if (debtInstrument != null)
            return DebtInstrumentDTO.builder().gpiIssue(debtInstrument.getGpi_issue())
                    .isinId(debtInstrument.getIsinId())
                    .sedolId(debtInstrument.getSedolId())
                    .securityDescriptionLong(debtInstrument.getSecurityDescriptionLong())
                    .nxtPayDate(debtInstrument.getNxtPayDate())
                    .nextCallDate(debtInstrument.getNextCallDate())
                    .nextPutDate(debtInstrument.getNextPutDate())
                    .build();
        return null;
    }

}
