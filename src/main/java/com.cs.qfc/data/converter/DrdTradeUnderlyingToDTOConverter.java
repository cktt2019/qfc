package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.DrdTradeUnderlying;
import com.cs.qfc.data.model.DrdTradeUnderlyingDTO;


public class DrdTradeUnderlyingToDTOConverter {


    public static DrdTradeUnderlyingDTO convert(DrdTradeUnderlying drdTradeUnderlying) {
        if (drdTradeUnderlying != null)
            return DrdTradeUnderlyingDTO.builder().id(drdTradeUnderlying.getId())
                    .cobDate(drdTradeUnderlying.getCobDate())
                    .loadCobDate(drdTradeUnderlying.getLoadCobDate())
                    .loadBook(drdTradeUnderlying.getLoadBook())
                    .bookName(drdTradeUnderlying.getBookName())
                    .tradeLoadId(drdTradeUnderlying.getTradeLoadId())
                    .loadTimestamp(drdTradeUnderlying.getLoadTimestamp())
                    .snapshotName(drdTradeUnderlying.getSnapshotName())
                    .snapshotId(drdTradeUnderlying.getSnapshotId())
                    .snapshotCreationDatetime(drdTradeUnderlying.getSnapshotCreationDatetime())
                    .interfaceVersion(drdTradeUnderlying.getInterfaceVersion())
                    .tradeId(drdTradeUnderlying.getTradeId())
                    .tftTradeVersion(drdTradeUnderlying.getTftTradeVersion())
                    .tradeLocationId(drdTradeUnderlying.getTradeLocationId())
                    .primaryTcn(drdTradeUnderlying.getPrimaryTcn())
                    .tcn(drdTradeUnderlying.getTcn())
                    .tradeVersionUpdateTime(drdTradeUnderlying.getTradeVersionUpdateTime())
                    .cancellationDate(drdTradeUnderlying.getCancellationDate())
                    .gbmEntity(drdTradeUnderlying.getGbmEntity())
                    .approvalStatus(drdTradeUnderlying.getApprovalStatus())
                    .eventReason(drdTradeUnderlying.getEventReason())
                    .externalId(drdTradeUnderlying.getExternalId())
                    .primaryMaturityDate(drdTradeUnderlying.getPrimaryMaturityDate())
                    .productSubType(drdTradeUnderlying.getProductSubType())
                    .productType(drdTradeUnderlying.getProductType())
                    .productValuationType(drdTradeUnderlying.getProductValuationType())
                    .tradeStatus(drdTradeUnderlying.getTradeStatus())
                    .underlyingId(drdTradeUnderlying.getUnderlyingId())
                    .underlyingIdType(drdTradeUnderlying.getUnderlyingIdType())
                    .accountCcy(drdTradeUnderlying.getAccountCcy())
                    .basketStrike(drdTradeUnderlying.getBasketStrike())
                    .basketWeighting(drdTradeUnderlying.getBasketWeighting())
                    .coupon(drdTradeUnderlying.getCoupon())
                    .couponFreq(drdTradeUnderlying.getCouponFreq())
                    .finalPrice(drdTradeUnderlying.getFinalPrice())
                    .floatingRateIndex(drdTradeUnderlying.getFloatingRateIndex())
                    .floatingRateIndexTenor(drdTradeUnderlying.getFloatingRateIndexTenor())
                    .floatingRateIndexType(drdTradeUnderlying.getFloatingRateIndexType())
                    .initialPrice(drdTradeUnderlying.getInitialPrice())
                    .refObligCategory(drdTradeUnderlying.getRefObligCategory())
                    .refPoolLongName(drdTradeUnderlying.getRefPoolLongName())
                    .refPoolShortName(drdTradeUnderlying.getRefPoolShortName())
                    .sequenceId(drdTradeUnderlying.getSequenceId())
                    .underlyingAssetClass(drdTradeUnderlying.getUnderlyingAssetClass())
                    .underlyingCcy(drdTradeUnderlying.getUnderlyingCcy())
                    .underlyingDesc(drdTradeUnderlying.getUnderlyingDesc())
                    .underlyingExtId(drdTradeUnderlying.getUnderlyingExtId())
                    .underlyingExtIdType(drdTradeUnderlying.getUnderlyingExtIdType())
                    .underlyingMatDate(drdTradeUnderlying.getUnderlyingMatDate())
                    .underlyingRiskId(drdTradeUnderlying.getUnderlyingRiskId())
                    .payRec(drdTradeUnderlying.getPayRec())
                    .settlementType(drdTradeUnderlying.getSettlementType())
                    .businessGroup(drdTradeUnderlying.getBusinessGroup())
                    .valuationClass(drdTradeUnderlying.getValuationClass())
                    .valuationType(drdTradeUnderlying.getValuationType())
                    .tradeSourceSystem(drdTradeUnderlying.getTradeSourceSystem())
                    .underlyingPriceSource(drdTradeUnderlying.getUnderlyingPriceSource())
                    .cduMasterAgreementLlk(drdTradeUnderlying.getCduMasterAgreementLlk())
                    .cduCollateralAnnexLlk(drdTradeUnderlying.getCduCollateralAnnexLlk())
                    .framesoftMasterAgreementLlk(drdTradeUnderlying.getFramesoftMasterAgreementLlk())
                    .algoCollateralAnnexLlk(drdTradeUnderlying.getAlgoCollateralAnnexLlk())
                    .gbmBookRef(drdTradeUnderlying.getGbmBookRef())
                    .underlyingProductId(drdTradeUnderlying.getUnderlyingProductId())
                    .underlyingProductType(drdTradeUnderlying.getUnderlyingProductType())
                    .underlyingRiskType(drdTradeUnderlying.getUnderlyingRiskType())
                    .bankruptcy(drdTradeUnderlying.getBankruptcy())
                    .delivObligAcceleratedMatured(drdTradeUnderlying.getDelivObligAcceleratedMatured())
                    .delivObligAccruedInterest(drdTradeUnderlying.getDelivObligAccruedInterest())
                    .delivObligAssignableLoan(drdTradeUnderlying.getDelivObligAssignableLoan())
                    .delivObligCategory(drdTradeUnderlying.getDelivObligCategory())
                    .delivObligConsentRequiredLoan(drdTradeUnderlying.getDelivObligConsentRequiredLoan())
                    .delivOblgDirectLoanPrtCptn(drdTradeUnderlying.getDelivOblgDirectLoanPrtCptn())
                    .delivObligListed(drdTradeUnderlying.getDelivObligListed())
                    .delivObligMaximumMaturity(drdTradeUnderlying.getDelivObligMaximumMaturity())
                    .delivObligMaxMaturitySpecified(drdTradeUnderlying.getDelivObligMaxMaturitySpecified())
                    .delivObligNotBearer(drdTradeUnderlying.getDelivObligNotBearer())
                    .delivObligNotContingent(drdTradeUnderlying.getDelivObligNotContingent())
                    .delivObligNotDomesticCurrency(drdTradeUnderlying.getDelivObligNotDomesticCurrency())
                    .delivObligNotDomesticIssuance(drdTradeUnderlying.getDelivObligNotDomesticIssuance())
                    .delivObligNotDomesticLaw(drdTradeUnderlying.getDelivObligNotDomesticLaw())
                    .delivObligNotSovereignLender(drdTradeUnderlying.getDelivObligNotSovereignLender())
                    .delivObligPhyParipassu(drdTradeUnderlying.getDelivObligPhyParipassu())
                    .delivObligSpecifiedCcyList(drdTradeUnderlying.getDelivObligSpecifiedCcyList())
                    .delivObligSpecifiedCcyType(drdTradeUnderlying.getDelivObligSpecifiedCcyType())
                    .delivObligTransferable(drdTradeUnderlying.getDelivObligTransferable())
                    .entityType(drdTradeUnderlying.getEntityType())
                    .primaryObligor(drdTradeUnderlying.getPrimaryObligor())
                    .recoveryValue(drdTradeUnderlying.getRecoveryValue())
                    .referenceEntityRedCode(drdTradeUnderlying.getReferenceEntityRedCode())
                    .referenceEntityUniqueEntityId(drdTradeUnderlying.getReferenceEntityUniqueEntityId())
                    .referenceObligationBloombergId(drdTradeUnderlying.getReferenceObligationBloombergId())
                    .referenceObligationCusip(drdTradeUnderlying.getReferenceObligationCusip())
                    .referenceObligationGuarantor(drdTradeUnderlying.getReferenceObligationGuarantor())
                    .referenceObligationIsin(drdTradeUnderlying.getReferenceObligationIsin())
                    .referencePrice(drdTradeUnderlying.getReferencePrice())
                    .refObligorCouponRate(drdTradeUnderlying.getRefObligorCouponRate())
                    .refObligorMaturityDate(drdTradeUnderlying.getRefObligorMaturityDate())
                    .refObligorNotionalAmt(drdTradeUnderlying.getRefObligorNotionalAmt())
                    .refObligorNotionalAmtCcy(drdTradeUnderlying.getRefObligorNotionalAmtCcy())
                    .refObligorObligationCategory(drdTradeUnderlying.getRefObligorObligationCategory())
                    .valUnwind(drdTradeUnderlying.getValUnwind())
                    .build();
        return null;
    }

}
