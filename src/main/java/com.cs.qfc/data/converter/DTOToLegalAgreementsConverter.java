package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.LegalAgreements;
import com.cs.qfc.data.model.LegalAgreementsDTO;


/**
 * @author london-databases
 */


public class DTOToLegalAgreementsConverter {


    public static LegalAgreements convert(LegalAgreementsDTO legalAgreementsDto) {
        if (legalAgreementsDto != null)
            return LegalAgreements.builder().id(legalAgreementsDto.getId())
                    .sourceTxnId(legalAgreementsDto.getSourceTxnId())
                    .a31AsOfDate(legalAgreementsDto.getA31AsOfDate())
                    .a32RecordsEntityIdentifier(legalAgreementsDto.getA32RecordsEntityIdentifier())
                    .a33AgreementIdentifier(legalAgreementsDto.getA33AgreementIdentifier())
                    .a34NameOfAgreementGoverningDoc(legalAgreementsDto.getA34NameOfAgreementGoverningDoc())
                    .a35AgreementDate(legalAgreementsDto.getA35AgreementDate())
                    .a36AgreementCounterpartyIdentifier(legalAgreementsDto.getA36AgreementCounterpartyIdentifier())
                    .a361UnderlyingQfcObligatorIdentifier(legalAgreementsDto.getA361UnderlyingQfcObligatorIdentifier())
                    .a37AgreementGoverningLaw(legalAgreementsDto.getA37AgreementGoverningLaw())
                    .a38CrossDefaultProvision(legalAgreementsDto.getA38CrossDefaultProvision())
                    .a39IdentityOfCrossDefaultEntities(legalAgreementsDto.getA39IdentityOfCrossDefaultEntities())
                    .a310CoveredByThirdPartyCreditEnhancement(legalAgreementsDto.getA310CoveredByThirdPartyCreditEnhancement())
                    .a311ThirdPartyCreditEnhancementProviderIdentifier(legalAgreementsDto.getA311ThirdPartyCreditEnhancementProviderIdentifier())
                    .a312AssociatedCreditEnhancementDocIdentifier(legalAgreementsDto.getA312AssociatedCreditEnhancementDocIdentifier())
                    .a3121CounterpartyThirdPartyCreditEnhancement(legalAgreementsDto.getA3121CounterpartyThirdPartyCreditEnhancement())
                    .a3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier(legalAgreementsDto.getA3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier())
                    .a3123CounterpartyAssociatedCreditEnhancementDocIdentifier(legalAgreementsDto.getA3123CounterpartyAssociatedCreditEnhancementDocIdentifier())
                    .a313CounterpartyContactInfoName(legalAgreementsDto.getA313CounterpartyContactInfoName())
                    .a314CounterpartyContactInfoAddress(legalAgreementsDto.getA314CounterpartyContactInfoAddress())
                    .a315CounterpartyContactInfoPhone(legalAgreementsDto.getA315CounterpartyContactInfoPhone())
                    .a316CounterpartyContactInfoEmail(legalAgreementsDto.getA316CounterpartyContactInfoEmail())

                    .build();
        return null;
    }

}
