package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.DTPMISGBLDYTREE;
import com.cs.qfc.data.model.DTPMISGBLDYTREEDTO;


/**
 * @author london-databases
 */


public class DTPMISGBLDYTREEToDTOConverter {


    public static DTPMISGBLDYTREEDTO convert(DTPMISGBLDYTREE dTPMISGBLDYTREE) {
        if (dTPMISGBLDYTREE != null)
            return DTPMISGBLDYTREEDTO.builder().misMemberCode(dTPMISGBLDYTREE.getMisMemberCode())
                    .americasProductLineHeadId(dTPMISGBLDYTREE.getAmericasProductLineHeadId())
                    .americasClusterHeadId(dTPMISGBLDYTREE.getAmericasClusterHeadId())
                    .apacClusterHeadId(dTPMISGBLDYTREE.getApacClusterHeadId())
                    .apacProductLineHeadId(dTPMISGBLDYTREE.getApacProductLineHeadId())
                    .businessControllerL2Id(dTPMISGBLDYTREE.getBusinessControllerL2Id())
                    .cluster(dTPMISGBLDYTREE.getCluster())
                    .clusterCode(dTPMISGBLDYTREE.getClusterCode())
                    .clusterHeadId(dTPMISGBLDYTREE.getClusterHeadId())
                    .desk(dTPMISGBLDYTREE.getDesk())
                    .deskCode(dTPMISGBLDYTREE.getDeskCode())
                    .division(dTPMISGBLDYTREE.getDivision())
                    .divisionCode(dTPMISGBLDYTREE.getDivisionCode())
                    .euroClusterHeadId(dTPMISGBLDYTREE.getEuroClusterHeadId())
                    .euroProductLineHeadId(dTPMISGBLDYTREE.getEuroProductLineHeadId())
                    .globalProductLineHeadId(dTPMISGBLDYTREE.getGlobalProductLineHeadId())
                    .hierarchyTraderId(dTPMISGBLDYTREE.getHierarchyTraderId())
                    .misHierarchySortCode(dTPMISGBLDYTREE.getMisHierarchySortCode())
                    .misMemberName(dTPMISGBLDYTREE.getMisMemberName())
                    .misMemberTypeCode(dTPMISGBLDYTREE.getMisMemberTypeCode())
                    .misProduct(dTPMISGBLDYTREE.getMisProduct())
                    .misRegionCode(dTPMISGBLDYTREE.getMisRegionCode())
                    .misTreeCode(dTPMISGBLDYTREE.getMisTreeCode())
                    .parentMisMemberCode(dTPMISGBLDYTREE.getParentMisMemberCode())
                    .productControlSectionMgrId(dTPMISGBLDYTREE.getProductControlSectionMgrId())
                    .productLineCode(dTPMISGBLDYTREE.getProductLineCode())
                    .productLineName(dTPMISGBLDYTREE.getProductLineName())
                    .sub3ProductLine(dTPMISGBLDYTREE.getSub3ProductLine())
                    .sub3ProductLineDescription(dTPMISGBLDYTREE.getSub3ProductLineDescription())
                    .subClusterCode(dTPMISGBLDYTREE.getSubClusterCode())
                    .subClusterName(dTPMISGBLDYTREE.getSubClusterName())
                    .subDivisionCode(dTPMISGBLDYTREE.getSubDivisionCode())
                    .subDivisionDescription(dTPMISGBLDYTREE.getSubDivisionDescription())
                    .subProductDescription(dTPMISGBLDYTREE.getSubProductDescription())
                    .subProductLine(dTPMISGBLDYTREE.getSubProductLine())
                    .swissProductLineHeadId(dTPMISGBLDYTREE.getSwissProductLineHeadId())
                    .tradingSupervisorId(dTPMISGBLDYTREE.getTradingSupervisorId())
                    .tradingSupervisorDelegateId(dTPMISGBLDYTREE.getTradingSupervisorDelegateId())
                    .misProductCode(dTPMISGBLDYTREE.getMisProductCode())
                    .misUnitPercentOwned(dTPMISGBLDYTREE.getMisUnitPercentOwned())
                    .misUnitPercentDescription(dTPMISGBLDYTREE.getMisUnitPercentDescription())
                    .deltaFlag(dTPMISGBLDYTREE.getDeltaFlag())
                    .cddsTreeNodeStatus(dTPMISGBLDYTREE.getCddsTreeNodeStatus())
                    .tradingSupport(dTPMISGBLDYTREE.getTradingSupport())
                    .tradeSupportManagerEmail(dTPMISGBLDYTREE.getTradeSupportManagerEmail())
                    .build();
        return null;
    }

}
