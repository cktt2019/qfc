package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.NTPACustodyPosition;
import com.cs.qfc.data.model.NTPACustodyPositionDTO;


/**
 * @author london-databases
 */


public class DTOToNTPACustodyPositionConverter {


    public static NTPACustodyPosition convert(NTPACustodyPositionDTO nTPACustodyPositionDto) {
        if (nTPACustodyPositionDto != null)
            return NTPACustodyPosition.builder().businessDate(nTPACustodyPositionDto.getBusinessDate())
                    .compIdC(nTPACustodyPositionDto.getCompIdC())
                    .pIdC(nTPACustodyPositionDto.getPIdC())
                    .cAcIdC(nTPACustodyPositionDto.getCAcIdC())
                    .cAcSetlmtTypC(nTPACustodyPositionDto.getCAcSetlmtTypC())
                    .curIdC(nTPACustodyPositionDto.getCurIdC())
                    .pnbPosD(nTPACustodyPositionDto.getPnbPosD())
                    .tNatvCurIdC(nTPACustodyPositionDto.getTNatvCurIdC())
                    .tBaseCurIdC(nTPACustodyPositionDto.getTBaseCurIdC())
                    .tConCurIdC(nTPACustodyPositionDto.getTConCurIdC())
                    .pnbMktValM(nTPACustodyPositionDto.getPnbMktValM())
                    .pnbMktValBaseM(nTPACustodyPositionDto.getPnbMktValBaseM())
                    .pnbMktValConM(nTPACustodyPositionDto.getPnbMktValConM())
                    .pPxClM(nTPACustodyPositionDto.getPPxClM())
                    .pnbActvyLastId(nTPACustodyPositionDto.getPnbActvyLastId())
                    .cIdC(nTPACustodyPositionDto.getCIdC())
                    .cN(nTPACustodyPositionDto.getCN())
                    .cAcTypC(nTPACustodyPositionDto.getCAcTypC())
                    .cAcEfD(nTPACustodyPositionDto.getCAcEfD())
                    .pMatyD(nTPACustodyPositionDto.getPMatyD())
                    .pStdT(nTPACustodyPositionDto.getPStdT())
                    .pIsinIdC(nTPACustodyPositionDto.getPIsinIdC())
                    .pSedolIdC(nTPACustodyPositionDto.getPSedolIdC())
                    .pFileTypC(nTPACustodyPositionDto.getPFileTypC())
                    .pnbAcctTypC(nTPACustodyPositionDto.getPnbAcctTypC())
                    .id(nTPACustodyPositionDto.getId())

                    .build();
        return null;
    }

}
