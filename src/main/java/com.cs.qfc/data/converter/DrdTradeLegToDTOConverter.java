package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.DrdTradeLeg;
import com.cs.qfc.data.model.DrdTradeLegDTO;


/**
 * @author london-databases
 */


public class DrdTradeLegToDTOConverter {


    public static DrdTradeLegDTO convert(DrdTradeLeg drdTradeLeg) {
        if (drdTradeLeg != null)
            return DrdTradeLegDTO.builder().id(drdTradeLeg.getId())
                    .cobDate(drdTradeLeg.getCobDate())
                    .gbmEntity(drdTradeLeg.getGbmEntity())
                    .tradeId(drdTradeLeg.getTradeId())
                    .counterPartyId(drdTradeLeg.getCounterPartyId())
                    .bookName(drdTradeLeg.getBookName())
                    .cduMasterAgreementLlk(drdTradeLeg.getCduMasterAgreementLlk())
                    .cduCollateralAnnexLlk(drdTradeLeg.getCduCollateralAnnexLlk())
                    .framesoftMasterAgreementLlk(drdTradeLeg.getFramesoftMasterAgreementLlk())
                    .algoCollateralAnnexLlk(drdTradeLeg.getAlgoCollateralAnnexLlk())
                    .tradeDate(drdTradeLeg.getTradeDate())
                    .primaryMaturityDate(drdTradeLeg.getPrimaryMaturityDate())
                    .localPvCcyCode(drdTradeLeg.getLocalPvCcyCode())
                    .localPv(drdTradeLeg.getLocalPv())
                    .pvNotional(drdTradeLeg.getPvNotional())
                    .build();
        return null;
    }

}
