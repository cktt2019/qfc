package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.AgreementTitlesScope;
import com.cs.qfc.data.model.AgreementTitlesScopeDTO;


/**
 * @author london-databases
 */


public class AgreementTitlesScopeToDTOConverter {


    public static AgreementTitlesScopeDTO convert(AgreementTitlesScope agreementTitlesScope) {
        if (agreementTitlesScope != null)
            return AgreementTitlesScopeDTO.builder().count(agreementTitlesScope.getCount())
                    .agreementTitle(agreementTitlesScope.getAgreementTitle())
                    .agreementType(agreementTitlesScope.getAgreementType())
                    .inScope(agreementTitlesScope.getInScope())
                    .build();
        return null;
    }

}
