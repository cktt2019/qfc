package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.LoanIQ;
import com.cs.qfc.data.model.LoanIQDTO;


/**
 * @author london-databases
 */


public class DTOToLoanIQConverter {


    public static LoanIQ convert(LoanIQDTO loanIQDto) {
        if (loanIQDto != null)
            return LoanIQ.builder().id(loanIQDto.getId())
                    .cptyCsid(loanIQDto.getCptyCsid())
                    .sdsId(loanIQDto.getSdsId())
                    .portfolioCode(loanIQDto.getPortfolioCode())
                    .agreementId(loanIQDto.getAgreementId())
                    .tradeDate(loanIQDto.getTradeDate())
                    .maturityDate(loanIQDto.getMaturityDate())
                    .currency(loanIQDto.getCurrency())
                    .presentValue(loanIQDto.getPresentValue())
                    .tradeAmount(loanIQDto.getTradeAmount())
                    .recordType(loanIQDto.getRecordType())
                    .swapId(loanIQDto.getSwapId())

                    .build();
        return null;
    }

}
