package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.PrimeSwapPosition;
import com.cs.qfc.data.model.PrimeSwapPositionDTO;


/**
 * @author london-databases
 */


public class PrimeSwapPositionToDTOConverter {


    public static PrimeSwapPositionDTO convert(PrimeSwapPosition primeSwapPosition) {
        if (primeSwapPosition != null)
            return PrimeSwapPositionDTO.builder().id(primeSwapPosition.getId())
                    .businessDate(primeSwapPosition.getBusinessDate())
                    .swapId(primeSwapPosition.getSwapId())
                    .swapVersion(primeSwapPosition.getSwapVersion())
                    .positionId(primeSwapPosition.getPositionId())
                    .positionVersion(primeSwapPosition.getPositionVersion())
                    .swapType(primeSwapPosition.getSwapType())
                    .counterpartyCode(primeSwapPosition.getCounterpartyCode())
                    .counterpartyName(primeSwapPosition.getCounterpartyName())
                    .settledLtdCostBase(primeSwapPosition.getSettledLtdCostBase())
                    .swapCcyToUsdFx(primeSwapPosition.getSwapCcyToUsdFx())
                    .build();
        return null;
    }

}
