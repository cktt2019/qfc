package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.MarnaGGovtLaw;
import com.cs.qfc.data.model.MarnaGGovtLawDTO;


/**
 * @author london-databases
 */


public class MarnaGGovtLawToDTOConverter {


    public static MarnaGGovtLawDTO convert(MarnaGGovtLaw marnaGGovtLaw) {
        if (marnaGGovtLaw != null)
            return MarnaGGovtLawDTO.builder().businessDate(marnaGGovtLaw.getBusinessDate())
                    .agmtNo(marnaGGovtLaw.getAgmtNo())
                    .cnid(marnaGGovtLaw.getCnid())
                    .agmtDate(marnaGGovtLaw.getAgmtDate())
                    .counterparty(marnaGGovtLaw.getCounterparty())
                    .csid(marnaGGovtLaw.getCsid())
                    .agmtType(marnaGGovtLaw.getAgmtType())
                    .agmtTitle(marnaGGovtLaw.getAgmtTitle())
                    .coi(marnaGGovtLaw.getCoi())
                    .governingLaw(marnaGGovtLaw.getGoverningLaw())
                    .id(marnaGGovtLaw.getId())
                    .build();
        return null;
    }

}
