package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.PorfolioRelationship;
import com.cs.qfc.data.model.PorfolioRelationshipDTO;


/**
 * @author london-databases
 */


public class DTOToPorfolioRelationshipConverter {


    public static PorfolioRelationship convert(PorfolioRelationshipDTO porfolioRelationshipDto) {
        if (porfolioRelationshipDto != null)
            return PorfolioRelationship.builder().portfolioId(porfolioRelationshipDto.getPortfolioId())
                    .globalPartyId(porfolioRelationshipDto.getGlobalPartyId())
                    .partytypeDescription(porfolioRelationshipDto.getPartytypeDescription())
                    .partyTypeCode(porfolioRelationshipDto.getPartyTypeCode())
                    .partyLegalName(porfolioRelationshipDto.getPartyLegalName())
                    .partyAliasName(porfolioRelationshipDto.getPartyAliasName())
                    .legalRoleDefinitionId(porfolioRelationshipDto.getLegalRoleDefinitionId())
                    .roleType(porfolioRelationshipDto.getRoleType())
                    .roleDescription(porfolioRelationshipDto.getRoleDescription())
                    .roleCode(porfolioRelationshipDto.getRoleCode())
                    .partyLegalRoleId(porfolioRelationshipDto.getPartyLegalRoleId())
                    .sourceId(porfolioRelationshipDto.getSourceId())
                    .sourceName(porfolioRelationshipDto.getSourceName())
                    .internalPartyAltId(porfolioRelationshipDto.getInternalPartyAltId())
                    .portfolioName(porfolioRelationshipDto.getPortfolioName())
                    .portfolioNumber(porfolioRelationshipDto.getPortfolioNumber())
                    .legalEntityName(porfolioRelationshipDto.getLegalEntityName())
                    .cmsBookingId(porfolioRelationshipDto.getCmsBookingId())
                    .entityJurisdiction(porfolioRelationshipDto.getEntityJurisdiction())
                    .legalEntityCode(porfolioRelationshipDto.getLegalEntityCode())
                    .parentTrxnCovrg(porfolioRelationshipDto.getParentTrxnCovrg())
                    .trxnCoverage(porfolioRelationshipDto.getTrxnCoverage())
                    .trxnCoverageDescr(porfolioRelationshipDto.getTrxnCoverageDescr())
                    .productId(porfolioRelationshipDto.getProductId())
                    .accountNumber(porfolioRelationshipDto.getAccountNumber())
                    .systemId(porfolioRelationshipDto.getSystemId())
                    .systemName(porfolioRelationshipDto.getSystemName())
                    .mnemonicValue(porfolioRelationshipDto.getMnemonicValue())
                    .mnemonicCode(porfolioRelationshipDto.getMnemonicCode())

                    .build();
        return null;
    }

}
