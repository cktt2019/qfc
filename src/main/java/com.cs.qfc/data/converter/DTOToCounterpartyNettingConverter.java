package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CounterpartyNetting;
import com.cs.qfc.data.model.CounterpartyNettingDTO;


/**
 * @author london-databases
 */


public class DTOToCounterpartyNettingConverter {


    public static CounterpartyNetting convert(CounterpartyNettingDTO counterpartyNettingDto) {
        if (counterpartyNettingDto != null)
            return CounterpartyNetting.builder().id(counterpartyNettingDto.getId())
                    .sourceTxnId(counterpartyNettingDto.getSourceTxnId())
                    .a21AsOfDate(counterpartyNettingDto.getA21AsOfDate())
                    .a22RecordsEntityIdentifier(counterpartyNettingDto.getA22RecordsEntityIdentifier())
                    .a23NettingAgreementCounterpartyIdentifier(counterpartyNettingDto.getA23NettingAgreementCounterpartyIdentifier())
                    .a24NettingAgreementIdentifier(counterpartyNettingDto.getA24NettingAgreementIdentifier())
                    .a241UnderlyingQfcObligatorIdentifier(counterpartyNettingDto.getA241UnderlyingQfcObligatorIdentifier())
                    .a25CoveredByThirdPartyCreditEnhancement(counterpartyNettingDto.getA25CoveredByThirdPartyCreditEnhancement())
                    .a251ThirdPartyCreditEnhancementProviderIdentifier(counterpartyNettingDto.getA251ThirdPartyCreditEnhancementProviderIdentifier())
                    .a252ThirdPartyCreditEnhancementAgreementIdentifier(counterpartyNettingDto.getA252ThirdPartyCreditEnhancementAgreementIdentifier())
                    .a253CounterpartyThirdPartyCreditEnhancement(counterpartyNettingDto.getA253CounterpartyThirdPartyCreditEnhancement())
                    .a254CounterpartyThirdPartyCreditEnhancementProviderIdentifier(counterpartyNettingDto.getA254CounterpartyThirdPartyCreditEnhancementProviderIdentifier())
                    .a255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier(counterpartyNettingDto.getA255CounterpartyThirdPartyCreditEnhancementAgreementIdentifier())
                    .a26AggregateCurrMarketValueAllPositionsUsd(counterpartyNettingDto.getA26AggregateCurrMarketValueAllPositionsUsd())
                    .a27AggregateCurrMarketValuePositivePositionsUsd(counterpartyNettingDto.getA27AggregateCurrMarketValuePositivePositionsUsd())
                    .a28AggregateCurrMarketValueNegativePositionsUsd(counterpartyNettingDto.getA28AggregateCurrMarketValueNegativePositionsUsd())
                    .a29CurrMarketValueAllCollateralRecordsEntityInUsd(counterpartyNettingDto.getA29CurrMarketValueAllCollateralRecordsEntityInUsd())
                    .a210CurrMarketValueAllCollateralCounterpartyInUsd(counterpartyNettingDto.getA210CurrMarketValueAllCollateralCounterpartyInUsd())
                    .a211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd(counterpartyNettingDto.getA211CurrMarketValueAllCollateralRecordsEntityRehypoInUsd())
                    .a212CurrMarketValueAllCollateralCounterpartyRehypoInUsd(counterpartyNettingDto.getA212CurrMarketValueAllCollateralCounterpartyRehypoInUsd())
                    .a213RecordsEntityCollateralNet(counterpartyNettingDto.getA213RecordsEntityCollateralNet())
                    .a214CounterpartyCollateralNet(counterpartyNettingDto.getA214CounterpartyCollateralNet())
                    .a215NextMarginPaymentDate(counterpartyNettingDto.getA215NextMarginPaymentDate())
                    .a216NextMarginPaymentAmountUsd(counterpartyNettingDto.getA216NextMarginPaymentAmountUsd())
                    .a217SafekeepingAgentIdentifierRecordsEntity(counterpartyNettingDto.getA217SafekeepingAgentIdentifierRecordsEntity())
                    .a218SafekeepingAgentIdentifierCounterparty(counterpartyNettingDto.getA218SafekeepingAgentIdentifierCounterparty())

                    .build();
        return null;
    }

}
