package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.DrdTradeInstrument;
import com.cs.qfc.data.model.DrdTradeInstrumentDTO;


/**
 * @author london-databases
 */


public class DTOToDrdTradeInstrumentConverter {


    public static DrdTradeInstrument convert(DrdTradeInstrumentDTO drdTradeInstrumentDto) {
        if (drdTradeInstrumentDto != null)
            return DrdTradeInstrument.builder().id(drdTradeInstrumentDto.getId())
                    .cobDate(drdTradeInstrumentDto.getCobDate())
                    .gbmEntity(drdTradeInstrumentDto.getGbmEntity())
                    .tradeId(drdTradeInstrumentDto.getTradeId())
                    .counterPartyId(drdTradeInstrumentDto.getCounterPartyId())
                    .bookName(drdTradeInstrumentDto.getBookName())
                    .assetClass(drdTradeInstrumentDto.getAssetClass())
                    .cduMasterAgreementLlk(drdTradeInstrumentDto.getCduMasterAgreementLlk())
                    .cduCollateralAnnexLlk(drdTradeInstrumentDto.getCduCollateralAnnexLlk())
                    .framesoftMasterAgreementLlk(drdTradeInstrumentDto.getFramesoftMasterAgreementLlk())
                    .algoCollateralAnnexLlk(drdTradeInstrumentDto.getAlgoCollateralAnnexLlk())
                    .tradeDate(drdTradeInstrumentDto.getTradeDate())
                    .primaryMaturityDate(drdTradeInstrumentDto.getPrimaryMaturityDate())
                    .localPvCcyCode(drdTradeInstrumentDto.getLocalPvCcyCode())
                    .localPv(drdTradeInstrumentDto.getLocalPv())

                    .build();
        return null;
    }

}
