package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.EquityConvertibleBond;
import com.cs.qfc.data.model.EquityConvertibleBondDTO;


/**
 * @author london-databases
 */


public class DTOToEquityConvertibleBondConverter {


    public static EquityConvertibleBond convert(EquityConvertibleBondDTO equityConvertibleBondDto) {
        if (equityConvertibleBondDto != null)
            return EquityConvertibleBond.builder().cusipId(equityConvertibleBondDto.getCusipId())
                    .isinId(equityConvertibleBondDto.getIsinId())
                    .nextCallDate(equityConvertibleBondDto.getNextCallDate())
                    .nextPutDate(equityConvertibleBondDto.getNextPutDate())
                    .nxtPayDate(equityConvertibleBondDto.getNxtPayDate())
                    .securityDescription(equityConvertibleBondDto.getSecurityDescription())
                    .a3c7Ind(equityConvertibleBondDto.getA3c7Ind())
                    .bbYKet(equityConvertibleBondDto.getBbYKet())
                    .compExchCodeDer(equityConvertibleBondDto.getCompExchCodeDer())
                    .convPrice(equityConvertibleBondDto.getConvPrice())
                    .conRatio(equityConvertibleBondDto.getConRatio())
                    .csManaged(equityConvertibleBondDto.getCsManaged())
                    .equityFloat(equityConvertibleBondDto.getEquityFloat())
                    .exchange(equityConvertibleBondDto.getExchange())
                    .fidesaCode(equityConvertibleBondDto.getFidesaCode())
                    .fundObjective(equityConvertibleBondDto.getFundObjective())
                    .glossG5(equityConvertibleBondDto.getGlossG5())
                    .idBbGlobal(equityConvertibleBondDto.getIdBbGlobal())
                    .idBbUnique(equityConvertibleBondDto.getIdBbUnique())
                    .italyId(equityConvertibleBondDto.getItalyId())
                    .japanId(equityConvertibleBondDto.getJapanId())
                    .ricId(equityConvertibleBondDto.getRicId())
                    .singId(equityConvertibleBondDto.getSingId())
                    .valorenId(equityConvertibleBondDto.getValorenId())
                    .wertpId(equityConvertibleBondDto.getWertpId())
                    .hugoId(equityConvertibleBondDto.getHugoId())
                    .ntpaLsEqyPoDt(equityConvertibleBondDto.getNtpaLsEqyPoDt())
                    .londonImsDescription(equityConvertibleBondDto.getLondonImsDescription())
                    .maturityDate(equityConvertibleBondDto.getMaturityDate())
                    .nasdaqReportCodeDer(equityConvertibleBondDto.getNasdaqReportCodeDer())
                    .primaryRic(equityConvertibleBondDto.getPrimaryRic())
                    .qtyPerUndlyShr(equityConvertibleBondDto.getQtyPerUndlyShr())
                    .undlyPerAdr(equityConvertibleBondDto.getUndlyPerAdr())
                    .regs(equityConvertibleBondDto.getRegs())
                    .regionPrim(equityConvertibleBondDto.getRegionPrim())
                    .rtrTicketSymbol(equityConvertibleBondDto.getRtrTicketSymbol())
                    .tradeCntry(equityConvertibleBondDto.getTradeCntry())
                    .undlySecIsin(equityConvertibleBondDto.getUndlySecIsin())
                    .votingRightPerShare(equityConvertibleBondDto.getVotingRightPerShare())
                    .cmuId(equityConvertibleBondDto.getCmuId())
                    .ultPrntTicketExchange(equityConvertibleBondDto.getUltPrntTicketExchange())
                    .poetsId(equityConvertibleBondDto.getPoetsId())
                    .ntpaSoi(equityConvertibleBondDto.getNtpaSoi())
                    .cpn(equityConvertibleBondDto.getCpn())
                    .ssrLiquidityIndicator(equityConvertibleBondDto.getSsrLiquidityIndicator())
                    .reExchange(equityConvertibleBondDto.getReExchange())
                    .glossK3(equityConvertibleBondDto.getGlossK3())
                    .secType(equityConvertibleBondDto.getSecType())
                    .fundEuroDirectUcits(equityConvertibleBondDto.getFundEuroDirectUcits())
                    .fundAssetClassFocus(equityConvertibleBondDto.getFundAssetClassFocus())
                    .bbCompanyId(equityConvertibleBondDto.getBbCompanyId())
                    .spIssueId(equityConvertibleBondDto.getSpIssueId())
                    .fundClosedNewInv(equityConvertibleBondDto.getFundClosedNewInv())
                    .legalName(equityConvertibleBondDto.getLegalName())
                    .tradingLotSize(equityConvertibleBondDto.getTradingLotSize())
                    .reAdrUndlyRic(equityConvertibleBondDto.getReAdrUndlyRic())
                    .relativeIndex(equityConvertibleBondDto.getRelativeIndex())
                    .primTrad(equityConvertibleBondDto.getPrimTrad())
                    .cumulativeFlag(equityConvertibleBondDto.getCumulativeFlag())
                    .fundCustodianName(equityConvertibleBondDto.getFundCustodianName())
                    .initialPubOffer(equityConvertibleBondDto.getInitialPubOffer())
                    .gicsSectorIsr(equityConvertibleBondDto.getGicsSectorIsr())
                    .gicsSectorNameIsr(equityConvertibleBondDto.getGicsSectorNameIsr())
                    .gicsIndustryGroupIsr(equityConvertibleBondDto.getGicsIndustryGroupIsr())
                    .gicsIndustryGroupNameIsr(equityConvertibleBondDto.getGicsIndustryGroupNameIsr())
                    .gicsIndustrySr(equityConvertibleBondDto.getGicsIndustrySr())
                    .gicsIndustryNameIsr(equityConvertibleBondDto.getGicsIndustryNameIsr())
                    .naicsIdIsr(equityConvertibleBondDto.getNaicsIdIsr())
                    .ntpaCountryOfSettlement(equityConvertibleBondDto.getNtpaCountryOfSettlement())
                    .gliRp(equityConvertibleBondDto.getGliRp())
                    .cddsLastUpdatedTime(equityConvertibleBondDto.getCddsLastUpdatedTime())
                    .publiclyTraded(equityConvertibleBondDto.getPubliclyTraded())
                    .csSettleDays(equityConvertibleBondDto.getCsSettleDays())
                    .bbUnderlyingSecurity(equityConvertibleBondDto.getBbUnderlyingSecurity())
                    .bbUnderlyingTypeCode(equityConvertibleBondDto.getBbUnderlyingTypeCode())
                    .tickSizePilotGroup(equityConvertibleBondDto.getTickSizePilotGroup())
                    .calledDate(equityConvertibleBondDto.getCalledDate())
                    .calledPrice(equityConvertibleBondDto.getCalledPrice())
                    .couponType(equityConvertibleBondDto.getCouponType())
                    .reClassScheme(equityConvertibleBondDto.getReClassScheme())
                    .legalEntityIdentifier(equityConvertibleBondDto.getLegalEntityIdentifier())
                    .companyCorpTicker(equityConvertibleBondDto.getCompanyCorpTicker())
                    .companyToParent(equityConvertibleBondDto.getCompanyToParent())
                    .bbIssuerType(equityConvertibleBondDto.getBbIssuerType())
                    .fisecIssuerId(equityConvertibleBondDto.getFisecIssuerId())
                    .ultPrntCompId(equityConvertibleBondDto.getUltPrntCompId())
                    .isArchived(equityConvertibleBondDto.getIsArchived())
                    .localExchangeSymbol(equityConvertibleBondDto.getLocalExchangeSymbol())
                    .cfiCode(equityConvertibleBondDto.getCfiCode())
                    .fisn(equityConvertibleBondDto.getFisn())
                    .issDate(equityConvertibleBondDto.getIssDate())
                    .firstSettlementDate(equityConvertibleBondDto.getFirstSettlementDate())
                    .whenIssuedFlag(equityConvertibleBondDto.getWhenIssuedFlag())
                    .minorTradingCurrencyInd(equityConvertibleBondDto.getMinorTradingCurrencyInd())
                    .riskAlert(equityConvertibleBondDto.getRiskAlert())
                    .mdsId(equityConvertibleBondDto.getMdsId())
                    .fundGeographicFocus(equityConvertibleBondDto.getFundGeographicFocus())
                    .fundLeverageAmount(equityConvertibleBondDto.getFundLeverageAmount())
                    .cmsCode(equityConvertibleBondDto.getCmsCode())
                    .idSpi(equityConvertibleBondDto.getIdSpi())
                    .staticListingId(equityConvertibleBondDto.getStaticListingId())
                    .clientType(equityConvertibleBondDto.getClientType())
                    .marketingDesk(equityConvertibleBondDto.getMarketingDesk())
                    .externalId(equityConvertibleBondDto.getExternalId())
                    .issueBook(equityConvertibleBondDto.getIssueBook())
                    .dutchId(equityConvertibleBondDto.getDutchId())
                    .belgiumId(equityConvertibleBondDto.getBelgiumId())
                    .istarId(equityConvertibleBondDto.getIstarId())
                    .idBbSecurity(equityConvertibleBondDto.getIdBbSecurity())
                    .operatingMic(equityConvertibleBondDto.getOperatingMic())
                    .bbIndustrySectorCd(equityConvertibleBondDto.getBbIndustrySectorCd())
                    .bbIndustryGroupCd(equityConvertibleBondDto.getBbIndustryGroupCd())
                    .bbIndustrySubGroupCd(equityConvertibleBondDto.getBbIndustrySubGroupCd())
                    .cddsRecCreationTime(equityConvertibleBondDto.getCddsRecCreationTime())
                    .listedExchInd(equityConvertibleBondDto.getListedExchInd())
                    .idMalaysian(equityConvertibleBondDto.getIdMalaysian())
                    .redemptionPrice(equityConvertibleBondDto.getRedemptionPrice())
                    .series(equityConvertibleBondDto.getSeries())
                    .cpnFreq(equityConvertibleBondDto.getCpnFreq())
                    .gmSoi(equityConvertibleBondDto.getGmSoi())
                    .csPvtPlacementInd(equityConvertibleBondDto.getCsPvtPlacementInd())

                    .build();
        return null;
    }

}
