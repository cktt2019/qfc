package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.Vision;
import com.cs.qfc.data.model.VisionDTO;


public class VisionToDTOConverter {


    public static VisionDTO convert(Vision vision) {
        if (vision != null)
            return VisionDTO.builder().businessDate(vision.getBusinessDate())
                    .entity(vision.getEntity())
                    .cpty(vision.getCpty())
                    .book(vision.getBook())
                    .proveNameProduct(vision.getProveNameProduct())
                    .agreementId1(vision.getAgreementId1())
                    .tradeDate(vision.getTradeDate())
                    .endDate(vision.getEndDate())
                    .bondNextCoup(vision.getBondNextCoup())
                    .cashCurr(vision.getCashCurr())
                    .pvUsd(vision.getPvUsd())
                    .defaultFvLevel(vision.getDefaultFvLevel())
                    .face(vision.getFace())
                    .valueOn(vision.getValueOn())
                    .rehypothecationIndicator(vision.getRehypothecationIndicator())
                    .custodianId(vision.getCustodianId())
                    .id(vision.getId())
                    .build();
        return null;
    }

}
