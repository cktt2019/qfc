package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.BusinessUnit;
import com.cs.qfc.data.model.BusinessUnitDTO;


/**
 * @author london-databases
 */


public class DTOToBusinessUnitConverter {


    public static BusinessUnit convert(BusinessUnitDTO businessUnitDto) {
        if (businessUnitDto != null)
            return BusinessUnit.builder().financialBusinessUnitCode(businessUnitDto.getFinancialBusinessUnitCode())
                    .baseCurrency(businessUnitDto.getBaseCurrency())
                    .businessUnitActiveIndicator(businessUnitDto.getBusinessUnitActiveIndicator())
                    .businessUnitDescription(businessUnitDto.getBusinessUnitDescription())
                    .businessUnitEffectiveDate(businessUnitDto.getBusinessUnitEffectiveDate())
                    .businessUnitGermanDescription(businessUnitDto.getBusinessUnitGermanDescription())
                    .businessUnitGermanName(businessUnitDto.getBusinessUnitGermanName())
                    .businessUnitId(businessUnitDto.getBusinessUnitId())
                    .businessUnitLastUpdateDatetime(businessUnitDto.getBusinessUnitLastUpdateDatetime())
                    .businessUnitLastUpdatedBy(businessUnitDto.getBusinessUnitLastUpdatedBy())
                    .businessUnitName(businessUnitDto.getBusinessUnitName())
                    .businessUnitShortDescription(businessUnitDto.getBusinessUnitShortDescription())
                    .businessUnitShortGermanDescription(businessUnitDto.getBusinessUnitShortGermanDescription())
                    .chBusinessUnitLastUpdatedBy(businessUnitDto.getChBusinessUnitLastUpdatedBy())
                    .chBusinessUnitLastUpdatedDatetime(businessUnitDto.getChBusinessUnitLastUpdatedDatetime())
                    .customerId(businessUnitDto.getCustomerId())
                    .customerVendorAffiliateIndicator(businessUnitDto.getCustomerVendorAffiliateIndicator())
                    .financialBusinessUnitActiveIndicator(businessUnitDto.getFinancialBusinessUnitActiveIndicator())
                    .financialBusinessUnitEffectiveDate(businessUnitDto.getFinancialBusinessUnitEffectiveDate())
                    .financialBusinessUnitGroupId(businessUnitDto.getFinancialBusinessUnitGroupId())
                    .financialBusinessUnitGroupName(businessUnitDto.getFinancialBusinessUnitGroupName())
                    .financialBusinessUnitName(businessUnitDto.getFinancialBusinessUnitName())
                    .firmType(businessUnitDto.getFirmType())
                    .isoCurrencyCode(businessUnitDto.getIsoCurrencyCode())
                    .lcdRefNo(businessUnitDto.getLcdRefNo())
                    .legalEntityShortDescription(businessUnitDto.getLegalEntityShortDescription())
                    .legalEntityActiveIndicator(businessUnitDto.getLegalEntityActiveIndicator())
                    .legalEntityCode(businessUnitDto.getLegalEntityCode())
                    .legalEntityDescription(businessUnitDto.getLegalEntityDescription())
                    .legalEntityEffectiveDate(businessUnitDto.getLegalEntityEffectiveDate())
                    .legalEntityGermanDescription(businessUnitDto.getLegalEntityGermanDescription())
                    .legalEntityLastUpdatedBy(businessUnitDto.getLegalEntityLastUpdatedBy())
                    .legalEntityLastUpdatedDatetime(businessUnitDto.getLegalEntityLastUpdatedDatetime())
                    .localGaapCurrency(businessUnitDto.getLocalGaapCurrency())
                    .misMemberCode(businessUnitDto.getMisMemberCode())
                    .processingLocationCode(businessUnitDto.getProcessingLocationCode())
                    .sourceId(businessUnitDto.getSourceId())
                    .deltaFlag(businessUnitDto.getDeltaFlag())
                    .buDomicile(businessUnitDto.getBuDomicile())

                    .build();
        return null;
    }

}
