package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class LegalAgreementsMetaData implements MetaData {

    public static final String TABLE = "LegalAgreements";
    private static final String ID = "id";
    private static final String SOURCETXNID = "sourceTxnId";
    private static final String A31ASOFDATE = "a31AsOfDate";
    private static final String A32RECORDSENTITYIDENTIFIER = "a32RecordsEntityIdentifier";
    private static final String A33AGREEMENTIDENTIFIER = "a33AgreementIdentifier";
    private static final String A34NAMEOFAGREEMENTGOVERNINGDOC = "a34NameOfAgreementGoverningDoc";
    private static final String A35AGREEMENTDATE = "a35AgreementDate";
    private static final String A36AGREEMENTCOUNTERPARTYIDENTIFIER = "a36AgreementCounterpartyIdentifier";
    private static final String A361UNDERLYINGQFCOBLIGATORIDENTIFIER = "a361UnderlyingQfcObligatorIdentifier";
    private static final String A37AGREEMENTGOVERNINGLAW = "a37AgreementGoverningLaw";
    private static final String A38CROSSDEFAULTPROVISION = "a38CrossDefaultProvision";
    private static final String A39IDENTITYOFCROSSDEFAULTENTITIES = "a39IdentityOfCrossDefaultEntities";
    private static final String A310COVEREDBYTHIRDPARTYCREDITENHANCEMENT = "a310CoveredByThirdPartyCreditEnhancement";
    private static final String A311THIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER = "a311ThirdPartyCreditEnhancementProviderIdentifier";
    private static final String A312ASSOCIATEDCREDITENHANCEMENTDOCIDENTIFIER = "a312AssociatedCreditEnhancementDocIdentifier";
    private static final String A3121COUNTERPARTYTHIRDPARTYCREDITENHANCEMENT = "a3121CounterpartyThirdPartyCreditEnhancement";
    private static final String A3122COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER = "a3122CounterpartyThirdPartyCreditEnhancementProviderIdentifier";
    private static final String A3123COUNTERPARTYASSOCIATEDCREDITENHANCEMENTDOCIDENTIFIER = "a3123CounterpartyAssociatedCreditEnhancementDocIdentifier";
    private static final String A313COUNTERPARTYCONTACTINFONAME = "a313CounterpartyContactInfoName";
    private static final String A314COUNTERPARTYCONTACTINFOADDRESS = "a314CounterpartyContactInfoAddress";
    private static final String A315COUNTERPARTYCONTACTINFOPHONE = "a315CounterpartyContactInfoPhone";
    private static final String A316COUNTERPARTYCONTACTINFOEMAIL = "a316CounterpartyContactInfoEmail";

    @Override
    public String getColumnName(String property) {
        switch (property) {
            case ID:
                return "id";
            case SOURCETXNID:
                return "source_txn_id";
            case A31ASOFDATE:
                return "a31_as_of_date";
            case A32RECORDSENTITYIDENTIFIER:
                return "a32_records_entity_identifier";
            case A33AGREEMENTIDENTIFIER:
                return "a33_agreement_identifier";
            case A34NAMEOFAGREEMENTGOVERNINGDOC:
                return "a34_name_of_agreement_or_governing_doc";
            case A35AGREEMENTDATE:
                return "a35_agreement_date";
            case A36AGREEMENTCOUNTERPARTYIDENTIFIER:
                return "a36_agreement_counterparty_identifier";
            case A361UNDERLYINGQFCOBLIGATORIDENTIFIER:
                return "a361_underlying_qfc_obligator_identifier";
            case A37AGREEMENTGOVERNINGLAW:
                return "a37_agreement_governing_law";
            case A38CROSSDEFAULTPROVISION:
                return "a38_cross_default_provision";
            case A39IDENTITYOFCROSSDEFAULTENTITIES:
                return "a39_identity_of_cross_default_entities";
            case A310COVEREDBYTHIRDPARTYCREDITENHANCEMENT:
                return "a310_covered_by_third_party_credit_enhancement";
            case A311THIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER:
                return "a311_third_party_credit_enhancement_provider_identifier";
            case A312ASSOCIATEDCREDITENHANCEMENTDOCIDENTIFIER:
                return "a312_associated_credit_enhancement_doc_identifier";
            case A3121COUNTERPARTYTHIRDPARTYCREDITENHANCEMENT:
                return "a3121_counterparty_third_party_credit_enhancement";
            case A3122COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER:
                return "a3122_counterparty_third_party_credit_enhancement_provider_identifier";
            case A3123COUNTERPARTYASSOCIATEDCREDITENHANCEMENTDOCIDENTIFIER:
                return "a3123_counterparty_associated_credit_enhancement_doc_identifier";
            case A313COUNTERPARTYCONTACTINFONAME:
                return "a313_counterparty_contact_info_name";
            case A314COUNTERPARTYCONTACTINFOADDRESS:
                return "a314_counterparty_contact_info_address";
            case A315COUNTERPARTYCONTACTINFOPHONE:
                return "a315_counterparty_contact_info_phone";
            case A316COUNTERPARTYCONTACTINFOEMAIL:
                return "a316_counterparty_contact_info_email";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (property) {
            case ID:
                return "Integer";
            case SOURCETXNID:
                return "String";
            case A31ASOFDATE:
                return "String";
            case A32RECORDSENTITYIDENTIFIER:
                return "String";
            case A33AGREEMENTIDENTIFIER:
                return "String";
            case A34NAMEOFAGREEMENTGOVERNINGDOC:
                return "String";
            case A35AGREEMENTDATE:
                return "String";
            case A36AGREEMENTCOUNTERPARTYIDENTIFIER:
                return "String";
            case A361UNDERLYINGQFCOBLIGATORIDENTIFIER:
                return "String";
            case A37AGREEMENTGOVERNINGLAW:
                return "String";
            case A38CROSSDEFAULTPROVISION:
                return "String";
            case A39IDENTITYOFCROSSDEFAULTENTITIES:
                return "String";
            case A310COVEREDBYTHIRDPARTYCREDITENHANCEMENT:
                return "String";
            case A311THIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER:
                return "String";
            case A312ASSOCIATEDCREDITENHANCEMENTDOCIDENTIFIER:
                return "String";
            case A3121COUNTERPARTYTHIRDPARTYCREDITENHANCEMENT:
                return "String";
            case A3122COUNTERPARTYTHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER:
                return "String";
            case A3123COUNTERPARTYASSOCIATEDCREDITENHANCEMENTDOCIDENTIFIER:
                return "String";
            case A313COUNTERPARTYCONTACTINFONAME:
                return "String";
            case A314COUNTERPARTYCONTACTINFOADDRESS:
                return "String";
            case A315COUNTERPARTYCONTACTINFOPHONE:
                return "String";
            case A316COUNTERPARTYCONTACTINFOEMAIL:
                return "String";
        }
        return null;
    }


}
