package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class CustomQueriesMetaData implements MetaData {

    public static final String TABLE = "6abdfb62-8785-41bf-887b-7391dec385a7";
    private static final String ENTITYNAME = "entityName";
    private static final String CRITERIANAME = "criteriaName";
    private static final String CRITERIADESCRIPTION = "criteriaDescription";
    private static final String CRITERIACONTENT = "criteriaContent";

    @Override
    public String getColumnName(String property) {
        switch (property) {
            case ENTITYNAME:
                return "entity_name";
            case CRITERIANAME:
                return "criteria_name";
            case CRITERIADESCRIPTION:
                return "criteria_description";
            case CRITERIACONTENT:
                return "criteria_content";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (property + "a") {
            case ENTITYNAME:
                return "String";
            case CRITERIANAME:
                return "String";
            case CRITERIADESCRIPTION:
                return "String";
            case CRITERIACONTENT:
                return "String";
        }
        return "String";
    }


}
