package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class DebtConvertibleBondMetaData implements MetaData {

    public static final String TABLE = "DEBTCONVERTIBLEBOND";
    private static final String SEDOLID = "sedolId";
    private static final String SECURITYDESCRIPTIONLONG = "securityDescriptionLong";
    private static final String NXTPAYDATE = "nxtPayDate";
    private static final String NEXTCALLDATE = "nextCallDate";
    private static final String NEXTPUTDATE = "nextPutDate";
    private static final String ACCRUALCOUNT = "accrualCount";
    private static final String AGGTEISSUE = "aggteIssue";
    private static final String BASECPI = "baseCpi";
    private static final String CLEARINGHOUSEDER = "clearingHouseDer";
    private static final String BBTICKER = "bbTicker";
    private static final String BOOKENTRY = "bookEntry";
    private static final String BRADYBUSDAYCONVENTION = "bradyBusDayConvention";
    private static final String BUSINESSCALENDAR = "businessCalendar";
    private static final String CALCTYPEDES = "calcTypeDes";
    private static final String CALLDISCRETE = "callDiscrete";
    private static final String CALLFEATURE = "callFeature";
    private static final String CALLNOTICE = "callNotice";
    private static final String CALLDATE = "callDate";
    private static final String CALLPRICE = "callPrice";
    private static final String CPNFRE = "cpnFre";
    private static final String CPNFREQDAYS = "cpnFreqDays";
    private static final String COUPONTYPE = "couponType";
    private static final String COUPONTYPERESET = "couponTypeReset";
    private static final String CPNFREQYLD = "cpnFreqYld";
    private static final String CSMANAGED = "csManaged";
    private static final String ISSDATE = "issDate";
    private static final String DEBTSTATUS = "debtStatus";
    private static final String DEFSECURITY = "defSecurity";
    private static final String DAYSTOSETTLE = "daysToSettle";
    private static final String FWENTRY = "fwEntry";
    private static final String FIDESSAZONE = "fidessaZone";
    private static final String FIDESSACODE = "fidessaCode";
    private static final String FIRSTCALLDATE = "firstCallDate";
    private static final String FIRSTCOUPON = "firstCoupon";
    private static final String FIXFLTDAYCOUNT = "fixFltDayCount";
    private static final String FLOATRST = "floatRst";
    private static final String FLOATRSTINDX = "floatRstIndx";
    private static final String FLTDAYSPRIOR = "fltDaysPrior";
    private static final String GLOSSK3 = "glossK3";
    private static final String GLOSSL9 = "glossL9";
    private static final String GLOSSM3 = "glossM3";
    private static final String IDBB = "idBb";
    private static final String IDBBUNIQUE = "idBbUnique";
    private static final String EPICID = "epicId";
    private static final String CMUID = "cmuId";
    private static final String ISINID = "isinId";
    private static final String JAPANID = "japanId";
    private static final String MISCDOMESTICID = "miscDomesticId";
    private static final String POETSID = "poetsId";
    private static final String RICID = "ricId";
    private static final String SEDOL2ID = "sedol2Id";
    private static final String SINGID = "singId";
    private static final String INCOMECURRENCY = "incomeCurrency";
    private static final String INDUSTRYSBGPRISSUE = "industrySbgprIssue";
    private static final String INTRATECAP = "intRateCap";
    private static final String INTRATEFL = "intRateFl";
    private static final String BOJID = "bojId";
    private static final String FISECID = "fisecId";
    private static final String HUGOID = "hugoId";
    private static final String ISTARID = "istarId";
    private static final String NRIID = "nriId";
    private static final String SMUID = "smuId";
    private static final String NRTISSUERID = "nrtIssuerId";
    private static final String MARGINPERC = "marginPerc";
    private static final String MATURITYDATE = "maturityDate";
    private static final String MUNIMINTAX = "muniMinTax";
    private static final String MUNIFEDTAX = "muniFedTax";
    private static final String STATECODE = "stateCode";
    private static final String NEXTRESET = "nextReset";
    private static final String NRIINTPAYCLASS = "nriIntPayClass";
    private static final String NRICODE = "nriCode";
    private static final String NRISECSUB = "nriSecSub";
    private static final String NRISICC = "nriSicc";
    private static final String OFFERINGTYP = "offeringTyp";
    private static final String DATEDDATE = "datedDate";
    private static final String PHYSICAL = "physical";
    private static final String PREVPAYDATE = "prevPayDate";
    private static final String PRVRSTDATE = "prvRstDate";
    private static final String PRINCFAC = "princFac";
    private static final String PRIVATEPLACEMENT = "privatePlacement";
    private static final String PRORATACALL = "proRataCall";
    private static final String PUBBALOUT = "pubBalOut";
    private static final String PUTDISC = "putDisc";
    private static final String PUTFEATURE = "putFeature";
    private static final String PUTNOTICE = "putNotice";
    private static final String RATINGSTRIGGER = "ratingsTrigger";
    private static final String REFIXFREQ = "refixFreq";
    private static final String REGS = "regs";
    private static final String REGIONCODE = "regionCode";
    private static final String RESETIDX = "resetIdx";
    private static final String RESTRICTCODE = "restrictCode";
    private static final String SECTYPE = "secType";
    private static final String SINKFREQ = "sinkFreq";
    private static final String BENCHMARKSPREAD = "benchmarkSpread";
    private static final String TICKERSYMBOL = "tickerSymbol";
    private static final String TRADINGCURRENCY = "tradingCurrency";
    private static final String MNPIECE = "mnPiece";
    private static final String TRAXREPORT = "traxReport";
    private static final String ULTMATDATE = "ultMatDate";
    private static final String DESNOTES = "desNotes";
    private static final String ULTPRNTCOMPID = "ultPrntCompId";
    private static final String FISECISSUERID = "fisecIssuerId";
    private static final String ULTPRNTTICKEREXCHANGE = "ultPrntTickerExchange";
    private static final String COUPONFREQUENCY = "couponFrequency";
    private static final String LONDONIMSDESCRIPTION = "londonImsDescription";
    private static final String CONRATIO = "conRatio";
    private static final String UNDLYSECISIN = "undlySecIsin";
    private static final String CONVPRICE = "convPrice";
    private static final String PERSECSRC01 = "perSecSrc01";
    private static final String ISSUEDATE = "issueDate";
    private static final String SPISSUERID = "spIssuerId";
    private static final String NTPAINPOSITION = "ntpaInPosition";
    private static final String ISINTERNATIONALSUKUK = "isInternationalSukuk";
    private static final String ISDOMESTICSUKUK = "isDomesticSukuk";
    private static final String SYNTHETICID = "syntheticId";
    private static final String POOLCUSIPID = "poolCusipId";
    private static final String AUCTRATEPFDIND = "auctRatePfdInd";
    private static final String REPOCLEARFLG = "repoclearFlg";
    private static final String MUNILETTERCREDITISSUER = "muniLetterCreditIssuer";
    private static final String MUNICORPOBLIGATOR2 = "muniCorpObligator2";
    private static final String INSURANCESTATUS = "insuranceStatus";
    private static final String OBLIGATOR = "obligator";
    private static final String CPN = "cpn";
    private static final String TRADECNTRY = "tradeCntry";
    private static final String COMMONID = "commonId";
    private static final String SERIES = "series";
    private static final String CLASSCODE = "classCode";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case SEDOLID:
                return "sedol_id";
            case SECURITYDESCRIPTIONLONG:
                return "security_description_long";
            case NXTPAYDATE:
                return "nxt_pay_date";
            case NEXTCALLDATE:
                return "next_call_date";
            case NEXTPUTDATE:
                return "next_put_date";
            case ACCRUALCOUNT:
                return "accrual_count";
            case AGGTEISSUE:
                return "aggte_issue";
            case BASECPI:
                return "base_cpi";
            case CLEARINGHOUSEDER:
                return "clearing_house_der";
            case BBTICKER:
                return "bb_ticker";
            case BOOKENTRY:
                return "book_entry";
            case BRADYBUSDAYCONVENTION:
                return "brady_bus_day_convention";
            case BUSINESSCALENDAR:
                return "business_calendar";
            case CALCTYPEDES:
                return "calc_type_des";
            case CALLDISCRETE:
                return "call_discrete";
            case CALLFEATURE:
                return "call_feature";
            case CALLNOTICE:
                return "call_notice";
            case CALLDATE:
                return "call_date";
            case CALLPRICE:
                return "call_price";
            case CPNFRE:
                return "cpn_fre";
            case CPNFREQDAYS:
                return "cpn_freq_days";
            case COUPONTYPE:
                return "coupon_type";
            case COUPONTYPERESET:
                return "coupon_type_reset";
            case CPNFREQYLD:
                return "cpn_freq_yld";
            case CSMANAGED:
                return "cs_managed";
            case ISSDATE:
                return "iss_date";
            case DEBTSTATUS:
                return "debt_status";
            case DEFSECURITY:
                return "def_security";
            case DAYSTOSETTLE:
                return "days_to_settle";
            case FWENTRY:
                return "fw_entry";
            case FIDESSAZONE:
                return "fidessa_zone";
            case FIDESSACODE:
                return "fidessa_code";
            case FIRSTCALLDATE:
                return "first_call_date";
            case FIRSTCOUPON:
                return "first_coupon";
            case FIXFLTDAYCOUNT:
                return "fix_flt_day_count";
            case FLOATRST:
                return "float_rst";
            case FLOATRSTINDX:
                return "float_rst_indx";
            case FLTDAYSPRIOR:
                return "flt_days_prior";
            case GLOSSK3:
                return "gloss_k3";
            case GLOSSL9:
                return "gloss_l9";
            case GLOSSM3:
                return "gloss_m3";
            case IDBB:
                return "id_bb";
            case IDBBUNIQUE:
                return "id_bb_unique";
            case EPICID:
                return "epic_id";
            case CMUID:
                return "cmu_id";
            case ISINID:
                return "isin_id";
            case JAPANID:
                return "japan_id";
            case MISCDOMESTICID:
                return "misc_domestic_id";
            case POETSID:
                return "poets_id";
            case RICID:
                return "ric_id";
            case SEDOL2ID:
                return "sedol2_id";
            case SINGID:
                return "sing_id";
            case INCOMECURRENCY:
                return "income_currency";
            case INDUSTRYSBGPRISSUE:
                return "industry_sbgpr_issue";
            case INTRATECAP:
                return "int_rate_cap";
            case INTRATEFL:
                return "int_rate_fl";
            case BOJID:
                return "boj_id";
            case FISECID:
                return "fisec_id";
            case HUGOID:
                return "hugo_id";
            case ISTARID:
                return "istar_id";
            case NRIID:
                return "nri_id";
            case SMUID:
                return "smu_id";
            case NRTISSUERID:
                return "nrt_issuer_id";
            case MARGINPERC:
                return "margin_perc";
            case MATURITYDATE:
                return "maturity_date";
            case MUNIMINTAX:
                return "muni_min_tax";
            case MUNIFEDTAX:
                return "muni_fed_tax";
            case STATECODE:
                return "state_code";
            case NEXTRESET:
                return "next_reset";
            case NRIINTPAYCLASS:
                return "nri_int_pay_class";
            case NRICODE:
                return "nri_code";
            case NRISECSUB:
                return "nri_sec_sub";
            case NRISICC:
                return "nri_sicc";
            case OFFERINGTYP:
                return "offering_typ";
            case DATEDDATE:
                return "dated_date";
            case PHYSICAL:
                return "physical";
            case PREVPAYDATE:
                return "prev_pay_date";
            case PRVRSTDATE:
                return "prv_rst_date";
            case PRINCFAC:
                return "princ_fac";
            case PRIVATEPLACEMENT:
                return "private_placement";
            case PRORATACALL:
                return "pro_rata_call";
            case PUBBALOUT:
                return "pub_bal_out";
            case PUTDISC:
                return "put_disc";
            case PUTFEATURE:
                return "put_feature";
            case PUTNOTICE:
                return "put_notice";
            case RATINGSTRIGGER:
                return "ratings_trigger";
            case REFIXFREQ:
                return "refix_freq";
            case REGS:
                return "regs";
            case REGIONCODE:
                return "region_code";
            case RESETIDX:
                return "reset_idx";
            case RESTRICTCODE:
                return "restrict_code";
            case SECTYPE:
                return "sec_type";
            case SINKFREQ:
                return "sink_freq";
            case BENCHMARKSPREAD:
                return "benchmark_spread";
            case TICKERSYMBOL:
                return "ticker_symbol";
            case TRADINGCURRENCY:
                return "trading_currency";
            case MNPIECE:
                return "mn_piece";
            case TRAXREPORT:
                return "trax_report";
            case ULTMATDATE:
                return "ult_mat_date";
            case DESNOTES:
                return "des_notes";
            case ULTPRNTCOMPID:
                return "ult_prnt_comp_id";
            case FISECISSUERID:
                return "fisec_issuer_id";
            case ULTPRNTTICKEREXCHANGE:
                return "ult_prnt_ticker_exchange";
            case COUPONFREQUENCY:
                return "coupon_frequency";
            case LONDONIMSDESCRIPTION:
                return "london_ims_description";
            case CONRATIO:
                return "con_ratio";
            case UNDLYSECISIN:
                return "undly_sec_isin";
            case CONVPRICE:
                return "conv_price";
            case PERSECSRC01:
                return "per_sec_src01";
            case ISSUEDATE:
                return "issue_date";
            case SPISSUERID:
                return "sp_issuer_id";
            case NTPAINPOSITION:
                return "ntpa_in_position";
            case ISINTERNATIONALSUKUK:
                return "is_international_sukuk";
            case ISDOMESTICSUKUK:
                return "is_domestic_sukuk";
            case SYNTHETICID:
                return "synthetic_id";
            case POOLCUSIPID:
                return "pool_cusip_id";
            case AUCTRATEPFDIND:
                return "auct_rate_pfd_ind";
            case REPOCLEARFLG:
                return "repoclear_flg";
            case MUNILETTERCREDITISSUER:
                return "muni_letter_credit_issuer";
            case MUNICORPOBLIGATOR2:
                return "muni_corp_obligator_2";
            case INSURANCESTATUS:
                return "insurance_status";
            case OBLIGATOR:
                return "obligator";
            case CPN:
                return "cpn";
            case TRADECNTRY:
                return "trade_cntry";
            case COMMONID:
                return "common_id";
            case SERIES:
                return "series";
            case CLASSCODE:
                return "class_code";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case SEDOLID:
                return "String";
            case SECURITYDESCRIPTIONLONG:
                return "String";
            case NXTPAYDATE:
                return "java.sql.Date";
            case NEXTCALLDATE:
                return "String";
            case NEXTPUTDATE:
                return "String";
            case ACCRUALCOUNT:
                return "String";
            case AGGTEISSUE:
                return "String";
            case BASECPI:
                return "Float";
            case CLEARINGHOUSEDER:
                return "String";
            case BBTICKER:
                return "String";
            case BOOKENTRY:
                return "String";
            case BRADYBUSDAYCONVENTION:
                return "String";
            case BUSINESSCALENDAR:
                return "String";
            case CALCTYPEDES:
                return "String";
            case CALLDISCRETE:
                return "String";
            case CALLFEATURE:
                return "String";
            case CALLNOTICE:
                return "String";
            case CALLDATE:
                return "java.sql.Date";
            case CALLPRICE:
                return "Float";
            case CPNFRE:
                return "String";
            case CPNFREQDAYS:
                return "Float";
            case COUPONTYPE:
                return "String";
            case COUPONTYPERESET:
                return "String";
            case CPNFREQYLD:
                return "String";
            case CSMANAGED:
                return "String";
            case ISSDATE:
                return "java.sql.Date";
            case DEBTSTATUS:
                return "String";
            case DEFSECURITY:
                return "String";
            case DAYSTOSETTLE:
                return "Float";
            case FWENTRY:
                return "String";
            case FIDESSAZONE:
                return "String";
            case FIDESSACODE:
                return "String";
            case FIRSTCALLDATE:
                return "java.sql.Date";
            case FIRSTCOUPON:
                return "java.sql.Date";
            case FIXFLTDAYCOUNT:
                return "Float";
            case FLOATRST:
                return "String";
            case FLOATRSTINDX:
                return "Float";
            case FLTDAYSPRIOR:
                return "Float";
            case GLOSSK3:
                return "String";
            case GLOSSL9:
                return "String";
            case GLOSSM3:
                return "String";
            case IDBB:
                return "String";
            case IDBBUNIQUE:
                return "String";
            case EPICID:
                return "String";
            case CMUID:
                return "String";
            case ISINID:
                return "String";
            case JAPANID:
                return "String";
            case MISCDOMESTICID:
                return "String";
            case POETSID:
                return "String";
            case RICID:
                return "String";
            case SEDOL2ID:
                return "String";
            case SINGID:
                return "String";
            case INCOMECURRENCY:
                return "String";
            case INDUSTRYSBGPRISSUE:
                return "String";
            case INTRATECAP:
                return "Float";
            case INTRATEFL:
                return "String";
            case BOJID:
                return "String";
            case FISECID:
                return "String";
            case HUGOID:
                return "String";
            case ISTARID:
                return "String";
            case NRIID:
                return "String";
            case SMUID:
                return "String";
            case NRTISSUERID:
                return "String";
            case MARGINPERC:
                return "Float";
            case MATURITYDATE:
                return "java.sql.Date";
            case MUNIMINTAX:
                return "String";
            case MUNIFEDTAX:
                return "String";
            case STATECODE:
                return "String";
            case NEXTRESET:
                return "java.sql.Date";
            case NRIINTPAYCLASS:
                return "String";
            case NRICODE:
                return "String";
            case NRISECSUB:
                return "String";
            case NRISICC:
                return "String";
            case OFFERINGTYP:
                return "String";
            case DATEDDATE:
                return "java.sql.Date";
            case PHYSICAL:
                return "String";
            case PREVPAYDATE:
                return "java.sql.Date";
            case PRVRSTDATE:
                return "java.sql.Date";
            case PRINCFAC:
                return "Float";
            case PRIVATEPLACEMENT:
                return "String";
            case PRORATACALL:
                return "String";
            case PUBBALOUT:
                return "Float";
            case PUTDISC:
                return "String";
            case PUTFEATURE:
                return "String";
            case PUTNOTICE:
                return "Integer";
            case RATINGSTRIGGER:
                return "String";
            case REFIXFREQ:
                return "String";
            case REGS:
                return "String";
            case REGIONCODE:
                return "String";
            case RESETIDX:
                return "String";
            case RESTRICTCODE:
                return "String";
            case SECTYPE:
                return "String";
            case SINKFREQ:
                return "String";
            case BENCHMARKSPREAD:
                return "Integer";
            case TICKERSYMBOL:
                return "String";
            case TRADINGCURRENCY:
                return "String";
            case MNPIECE:
                return "Float";
            case TRAXREPORT:
                return "String";
            case ULTMATDATE:
                return "java.sql.Date";
            case DESNOTES:
                return "String";
            case ULTPRNTCOMPID:
                return "String";
            case FISECISSUERID:
                return "String";
            case ULTPRNTTICKEREXCHANGE:
                return "String";
            case COUPONFREQUENCY:
                return "String";
            case LONDONIMSDESCRIPTION:
                return "String";
            case CONRATIO:
                return "Integer";
            case UNDLYSECISIN:
                return "String";
            case CONVPRICE:
                return "Float";
            case PERSECSRC01:
                return "String";
            case ISSUEDATE:
                return "java.sql.Date";
            case SPISSUERID:
                return "String";
            case NTPAINPOSITION:
                return "String";
            case ISINTERNATIONALSUKUK:
                return "String";
            case ISDOMESTICSUKUK:
                return "String";
            case SYNTHETICID:
                return "String";
            case POOLCUSIPID:
                return "String";
            case AUCTRATEPFDIND:
                return "String";
            case REPOCLEARFLG:
                return "String";
            case MUNILETTERCREDITISSUER:
                return "String";
            case MUNICORPOBLIGATOR2:
                return "String";
            case INSURANCESTATUS:
                return "String";
            case OBLIGATOR:
                return "String";
            case CPN:
                return "Float";
            case TRADECNTRY:
                return "String";
            case COMMONID:
                return "String";
            case SERIES:
                return "String";
            case CLASSCODE:
                return "String";
        }
        return null;
    }


}
