package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class VisionMetaData implements MetaData {

    public static final String TABLE = "VISION";
    private static final String BUSINESSDATE = "businessDate";
    private static final String ENTITY = "entity";
    private static final String CPTY = "cpty";
    private static final String BOOK = "book";
    private static final String PROVENAMEPRODUCT = "proveNameProduct";
    private static final String AGREEMENTID1 = "agreementId1";
    private static final String TRADEDATE = "tradeDate";
    private static final String ENDDATE = "endDate";
    private static final String BONDNEXTCOUP = "bondNextCoup";
    private static final String CASHCURR = "cashCurr";
    private static final String PVUSD = "pvUsd";
    private static final String DEFAULTFVLEVEL = "defaultFvLevel";
    private static final String FACE = "face";
    private static final String VALUEON = "valueOn";
    private static final String REHYPOTHECATIONINDICATOR = "rehypothecationIndicator";
    private static final String CUSTODIANID = "custodianId";
    private static final String ID = "id";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case BUSINESSDATE:
                return "business_date";
            case ENTITY:
                return "entity";
            case CPTY:
                return "cpty";
            case BOOK:
                return "book";
            case PROVENAMEPRODUCT:
                return "prove_name_product";
            case AGREEMENTID1:
                return "agreement_id1";
            case TRADEDATE:
                return "trade_date";
            case ENDDATE:
                return "end_date";
            case BONDNEXTCOUP:
                return "bond_next_coup";
            case CASHCURR:
                return "cash_curr";
            case PVUSD:
                return "pv_usd";
            case DEFAULTFVLEVEL:
                return "default_fv_level";
            case FACE:
                return "face";
            case VALUEON:
                return "value_on";
            case REHYPOTHECATIONINDICATOR:
                return "rehypothecation_indicator";
            case CUSTODIANID:
                return "custodian_id";
            case ID:
                return "id";
        }
        System.out.println("getColumnName returning null for " + property);
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case BUSINESSDATE:
                return "java.sql.Date";
            case ENTITY:
                return "String";
            case CPTY:
                return "String";
            case BOOK:
                return "String";
            case PROVENAMEPRODUCT:
                return "String";
            case AGREEMENTID1:
                return "String";
            case TRADEDATE:
                return "java.sql.Date";
            case ENDDATE:
                return "java.sql.Date";
            case BONDNEXTCOUP:
                return "String";
            case CASHCURR:
                return "String";
            case PVUSD:
                return "String";
            case DEFAULTFVLEVEL:
                return "String";
            case FACE:
                return "String";
            case VALUEON:
                return "String";
            case REHYPOTHECATIONINDICATOR:
                return "String";
            case CUSTODIANID:
                return "String";
            case ID:
                return "Integer";
        }
        return null;
    }


}
