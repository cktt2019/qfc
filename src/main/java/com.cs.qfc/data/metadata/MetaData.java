package com.cs.qfc.data.metadata;

public interface MetaData {

    public String getColumnName(String property);

    public String getColumnDataType(String property);

    default public String snakeToCamel(String str) {
        String orig = str;
        int count = 0;
        int length = str.length();
        try {
            str = str.substring(0, 1).toLowerCase() + str.substring(1);
            // Run a loop till string string contains underscore
            while (str.contains("_")) {
                count++;
                // Replace the first occurrence of letter that present after the underscore, to capitalize form of next letter of underscore
                str = str.replaceFirst("_[a-zA-Z0-9]", String.valueOf(Character.toUpperCase(str.charAt(str.indexOf("_") + 1))));
                if (count > length) break;
            }
        } catch (Throwable e) {
            // e.printStackTrace();
        }
        System.out.println("snakeToCamel from " + orig + " to " + str);
        return str;
    }

}
