package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class SpecificationMetaData implements MetaData {

    public static final String TABLE = "Specification";
    private static final String ID = "id";
    private static final String SPECIFICATIONNAME = "specificationName";
    private static final String SPECIFICATIONDESCRIPTION = "specificationDescription";
    private static final String SPECIFICATIONVERSION = "specificationVersion";
    private static final String SPECIFICATIONCONTENT = "specificationContent";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "id";
            case SPECIFICATIONNAME:
                return "specification_name";
            case SPECIFICATIONDESCRIPTION:
                return "specificationDescription";
            case SPECIFICATIONVERSION:
                return "specificationVersion";
            case SPECIFICATIONCONTENT:
                return "specificationContent";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
		/*switch (property) {
	case ID : return "Integer";
	case SPECIFICATIONNAME : return "String";
	case SPECIFICATIONDESCRIPTION : return "String";
	case SPECIFICATIONVERSION : return "String";
	case SPECIFICATIONCONTENT : return "java.sql.Clob";
		}*/
        return "String";
    }


}
