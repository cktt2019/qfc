package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class DepartmentMetaData implements MetaData {

    public static final String TABLE = "DEPARTMENT";
    private static final String DEPARTMENTID = "departmentId";
    private static final String BUSINESSCONTROLLERID = "businessControllerId";
    private static final String CONTROLLERID = "controllerId";
    private static final String DEPARTMENTCATEGORYCODE = "departmentCategoryCode";
    private static final String DEPARTMENTCATEGORYNAME = "departmentCategoryName";
    private static final String DEPARTMENTDORMANTINDICATOR = "departmentDormantIndicator";
    private static final String DEPARTMENTLONGNAME = "departmentLongName";
    private static final String DEPARTMENTNAME = "departmentName";
    private static final String DEPARTMENTRECONCILERGROUPNAME = "departmentReconcilerGroupName";
    private static final String DEPARTMENTSTATUS = "departmentStatus";
    private static final String DEPARTMENTTRADERID = "departmentTraderId";
    private static final String DEPARTMENTVALIDFROMDATE = "departmentValidFromDate";
    private static final String DEPARTMENTVALIDTODATE = "departmentValidToDate";
    private static final String FINANCIALBUSINESSUNITCODE = "financialBusinessUnitCode";
    private static final String IVRGROUP = "ivrGroup";
    private static final String L1SUPERVISORID = "l1SupervisorId";
    private static final String MISOFFICECODE = "misOfficeCode";
    private static final String MISOFFICENAME = "misOfficeName";
    private static final String OWNERL2ID = "ownerL2Id";
    private static final String RECANDADJCONTROLLERID = "recAndAdjControllerId";
    private static final String RECANDADJSUPERVISORID = "recAndAdjSupervisorId";
    private static final String RECONCILERID = "reconcilerId";
    private static final String REGIONCODE = "regionCode";
    private static final String REGIONDESCRIPTION = "regionDescription";
    private static final String REVENUETYPE = "revenueType";
    private static final String REVENUETYPEDESCRIPTION = "revenueTypeDescription";
    private static final String RISKPORTFOLIOID = "riskPortfolioId";
    private static final String SPECIALISATIONINDICATOR = "specialisationIndicator";
    private static final String TAXCATEGORY = "taxCategory";
    private static final String VOLKERRULEDESCRIPTION = "volkerRuleDescription";
    private static final String VOLKERRULEINDICATOR = "volkerRuleIndicator";
    private static final String FXCENTRALLYMAANGEDNAME = "fxCentrallyMaangedName";
    private static final String VOLCKERSUBDIVISION = "volckerSubDivision";
    private static final String VOLCKERSUBDIVISIONDESCRIPTION = "volckerSubDivisionDescription";
    private static final String IHCHCDFLAG = "ihcHcdFlag";
    private static final String COVEREDDEPTFLAG = "coveredDeptFlag";
    private static final String DELTAFLAG = "deltaFlag";
    private static final String REMITTANCEPOLICYCODE = "remittancePolicyCode";
    private static final String FIRSTINSTANCEDEPTACTIVATED = "firstInstanceDeptActivated";
    private static final String FIRSTINSTANCEDEPTINACTIVATED = "firstInstanceDeptInactivated";
    private static final String LASTINSTANCEDEPTACTIVATED = "lastInstanceDeptActivated";
    private static final String LASTINSTANCEDEPTINACTIVATED = "lastInstanceDeptInactivated";
    private static final String RBPNLFLAG = "rbpnlFlag";
    private static final String IVRGROUPLONGNAME = "ivrGroupLongName";
    private static final String FRTBDEPTCATEGORY = "frtbDeptCategory";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case DEPARTMENTID:
                return "department_id";
            case BUSINESSCONTROLLERID:
                return "business_controller_id";
            case CONTROLLERID:
                return "controller_id";
            case DEPARTMENTCATEGORYCODE:
                return "department_category_code";
            case DEPARTMENTCATEGORYNAME:
                return "department_category_name";
            case DEPARTMENTDORMANTINDICATOR:
                return "department_dormant_indicator";
            case DEPARTMENTLONGNAME:
                return "department_long_name";
            case DEPARTMENTNAME:
                return "department_name";
            case DEPARTMENTRECONCILERGROUPNAME:
                return "department_reconciler_group_name";
            case DEPARTMENTSTATUS:
                return "department_status";
            case DEPARTMENTTRADERID:
                return "department_trader_id";
            case DEPARTMENTVALIDFROMDATE:
                return "department_valid_from_date";
            case DEPARTMENTVALIDTODATE:
                return "department_valid_to_date";
            case FINANCIALBUSINESSUNITCODE:
                return "financial_business_unit_code";
            case IVRGROUP:
                return "ivr_group";
            case L1SUPERVISORID:
                return "l1_supervisor_id";
            case MISOFFICECODE:
                return "mis_office_code";
            case MISOFFICENAME:
                return "mis_office_name";
            case OWNERL2ID:
                return "owner_l2_id";
            case RECANDADJCONTROLLERID:
                return "rec_and_adj_controller_id";
            case RECANDADJSUPERVISORID:
                return "rec_and_adj_supervisor_id";
            case RECONCILERID:
                return "reconciler_id";
            case REGIONCODE:
                return "region_code";
            case REGIONDESCRIPTION:
                return "region_description";
            case REVENUETYPE:
                return "revenue_type";
            case REVENUETYPEDESCRIPTION:
                return "revenue_type_description";
            case RISKPORTFOLIOID:
                return "risk_portfolio_id";
            case SPECIALISATIONINDICATOR:
                return "specialisation_indicator";
            case TAXCATEGORY:
                return "tax_category";
            case VOLKERRULEDESCRIPTION:
                return "volker_rule_description";
            case VOLKERRULEINDICATOR:
                return "volker_rule_indicator";
            case FXCENTRALLYMAANGEDNAME:
                return "fx_centrally_maanged_name";
            case VOLCKERSUBDIVISION:
                return "volcker_sub_division";
            case VOLCKERSUBDIVISIONDESCRIPTION:
                return "volcker_sub_division_description";
            case IHCHCDFLAG:
                return "ihc_hcd_flag";
            case COVEREDDEPTFLAG:
                return "covered_dept_flag";
            case DELTAFLAG:
                return "delta_flag";
            case REMITTANCEPOLICYCODE:
                return "remittance_policy_code";
            case FIRSTINSTANCEDEPTACTIVATED:
                return "first_instance_dept_activated";
            case FIRSTINSTANCEDEPTINACTIVATED:
                return "first_instance_dept_inactivated";
            case LASTINSTANCEDEPTACTIVATED:
                return "last_instance_dept_activated";
            case LASTINSTANCEDEPTINACTIVATED:
                return "last_instance_dept_inactivated";
            case RBPNLFLAG:
                return "rbpnl_flag";
            case IVRGROUPLONGNAME:
                return "ivr_group_long_name";
            case FRTBDEPTCATEGORY:
                return "frtb_dept_category";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case DEPARTMENTID:
                return "String";
            case BUSINESSCONTROLLERID:
                return "String";
            case CONTROLLERID:
                return "String";
            case DEPARTMENTCATEGORYCODE:
                return "String";
            case DEPARTMENTCATEGORYNAME:
                return "String";
            case DEPARTMENTDORMANTINDICATOR:
                return "String";
            case DEPARTMENTLONGNAME:
                return "String";
            case DEPARTMENTNAME:
                return "String";
            case DEPARTMENTRECONCILERGROUPNAME:
                return "String";
            case DEPARTMENTSTATUS:
                return "String";
            case DEPARTMENTTRADERID:
                return "String";
            case DEPARTMENTVALIDFROMDATE:
                return "java.sql.Date";
            case DEPARTMENTVALIDTODATE:
                return "java.sql.Date";
            case FINANCIALBUSINESSUNITCODE:
                return "String";
            case IVRGROUP:
                return "String";
            case L1SUPERVISORID:
                return "String";
            case MISOFFICECODE:
                return "String";
            case MISOFFICENAME:
                return "String";
            case OWNERL2ID:
                return "String";
            case RECANDADJCONTROLLERID:
                return "String";
            case RECANDADJSUPERVISORID:
                return "String";
            case RECONCILERID:
                return "String";
            case REGIONCODE:
                return "String";
            case REGIONDESCRIPTION:
                return "String";
            case REVENUETYPE:
                return "String";
            case REVENUETYPEDESCRIPTION:
                return "String";
            case RISKPORTFOLIOID:
                return "String";
            case SPECIALISATIONINDICATOR:
                return "String";
            case TAXCATEGORY:
                return "String";
            case VOLKERRULEDESCRIPTION:
                return "String";
            case VOLKERRULEINDICATOR:
                return "String";
            case FXCENTRALLYMAANGEDNAME:
                return "String";
            case VOLCKERSUBDIVISION:
                return "String";
            case VOLCKERSUBDIVISIONDESCRIPTION:
                return "String";
            case IHCHCDFLAG:
                return "String";
            case COVEREDDEPTFLAG:
                return "String";
            case DELTAFLAG:
                return "String";
            case REMITTANCEPOLICYCODE:
                return "String";
            case FIRSTINSTANCEDEPTACTIVATED:
                return "String";
            case FIRSTINSTANCEDEPTINACTIVATED:
                return "String";
            case LASTINSTANCEDEPTACTIVATED:
                return "String";
            case LASTINSTANCEDEPTINACTIVATED:
                return "String";
            case RBPNLFLAG:
                return "String";
            case IVRGROUPLONGNAME:
                return "String";
            case FRTBDEPTCATEGORY:
                return "String";
        }
        return null;
    }


}
