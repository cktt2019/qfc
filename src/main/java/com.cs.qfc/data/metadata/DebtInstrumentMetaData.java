package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class DebtInstrumentMetaData implements MetaData {

    public static final String TABLE = "DEBTINSTRUMENT";
    private static final String ID = "gpiIssue";
    private static final String ISINID = "isinId";
    private static final String SEDOLID = "sedolId";
    private static final String SECURITYDESCRIPTIONLONG = "securityDescriptionLong";
    private static final String NXTPAYDATE = "nxtPayDate";
    private static final String NEXTCALLDATE = "nextCallDate";
    private static final String NEXTPUTDATE = "nextPutDate";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "gpi_issue";
            case ISINID:
                return "isin_id";
            case SEDOLID:
                return "sedol_id";
            case SECURITYDESCRIPTIONLONG:
                return "security_description_long";
            case NXTPAYDATE:
                return "nxt_pay_date";
            case NEXTCALLDATE:
                return "next_call_date";
            case NEXTPUTDATE:
                return "next_put_date";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "Integer";
            case ISINID:
                return "String";
            case SEDOLID:
                return "String";
            case SECURITYDESCRIPTIONLONG:
                return "String";
            case NXTPAYDATE:
                return "java.sql.Date";
            case NEXTCALLDATE:
                return "java.sql.Date";
            case NEXTPUTDATE:
                return "java.sql.Date";
        }
        return null;
    }


}
