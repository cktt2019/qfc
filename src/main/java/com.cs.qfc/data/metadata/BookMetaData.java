package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class BookMetaData implements MetaData {

    public static final String TABLE = "BOOK";
    private static final String GLOBALBOOKID = "globalBookId";
    private static final String ACCOUNTMETHOD = "accountMethod";
    private static final String ACCOUNTINGBASISINDICATOR = "accountingBasisIndicator";
    private static final String BOOKCATEGORYCODE = "bookCategoryCode";
    private static final String BOOKCATEGORYDESCRIPTION = "bookCategoryDescription";
    private static final String BOOKCONTROLLERID = "bookControllerId";
    private static final String BOOKDORMANTINDICATOR = "bookDormantIndicator";
    private static final String BOOKNAME = "bookName";
    private static final String BOOKPNLSYSTEMNAME = "bookPnlSystemName";
    private static final String BOOKREF = "bookRef";
    private static final String BOOKSTATUS = "bookStatus";
    private static final String BOOKSUBCATEGORYCODE = "bookSubCategoryCode";
    private static final String BOOKSUBCATEGORYDESCRIPTION = "bookSubCategoryDescription";
    private static final String BOOKTRADERID = "bookTraderId";
    private static final String CNYDELIVERABLEOUTSIDECHINAINDICATOR = "cnyDeliverableOutsideChinaIndicator";
    private static final String DEPARTMENTID = "departmentId";
    private static final String FRONTOFFICEREPRESENTATIVEID = "frontOfficeRepresentativeId";
    private static final String GPLATOMCODE = "gplAtomCode";
    private static final String NHFSINDICATOR = "nhfsIndicator";
    private static final String REMOTEBOOKINGPOLICYINDICATOR = "remoteBookingPolicyIndicator";
    private static final String SPLITHEDGECOMMENTS = "splitHedgeComments";
    private static final String SPLITHEDGEINDICATOR = "splitHedgeIndicator";
    private static final String USTAXCATEGORYCODE = "usTaxCategoryCode";
    private static final String BOOKLASTUPDATEDDATETIME = "bookLastUpdatedDatetime";
    private static final String FINANCIALBUSINESSUNITCODE = "financialBusinessUnitCode";
    private static final String DELTAFLAG = "deltaFlag";
    private static final String BOOKREQUESTSTATUSCODE = "bookRequestStatusCode";
    private static final String FIRSTINSTANCEBOOKACTIVATED = "firstInstanceBookActivated";
    private static final String LASTINSTANCEBOOKACTIVATED = "lastInstanceBookActivated";
    private static final String FIRSTINSTANCEBOOKINACTIVATED = "firstInstanceBookInactivated";
    private static final String LASTINSTANCEBOOK = "lastInstanceBook";
    private static final String EMAILADDRESS = "emailAddress";
    private static final String CAVFLAG = "cavFlag";
    private static final String FOSFLAG = "fosFlag";
    private static final String BOOKINGINTENT = "bookingIntent";
    private static final String BOOKREQUESTID = "bookRequestId";
    private static final String IPAREMPLIDEMAILID = "iparEmplIdEmailId";
    private static final String BOOKTRADEREMAILID = "bookTraderEmailId";
    private static final String PCSUPERVISOREMAILID = "pcSupervisorEmailId";
    private static final String BOOKEMPIDEMAILID = "bookEmpIdEmailId";
    private static final String PCTNLEMPLIDEMAILID = "pctnlEmplIdEmailId";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case GLOBALBOOKID:
                return "global_book_id";
            case ACCOUNTMETHOD:
                return "account_method";
            case ACCOUNTINGBASISINDICATOR:
                return "accounting_basis_indicator";
            case BOOKCATEGORYCODE:
                return "book_category_code";
            case BOOKCATEGORYDESCRIPTION:
                return "book_category_description";
            case BOOKCONTROLLERID:
                return "book_controller_id";
            case BOOKDORMANTINDICATOR:
                return "book_dormant_indicator";
            case BOOKNAME:
                return "book_name";
            case BOOKPNLSYSTEMNAME:
                return "book_pnl_system_name";
            case BOOKREF:
                return "book_ref";
            case BOOKSTATUS:
                return "book_status";
            case BOOKSUBCATEGORYCODE:
                return "book_sub_category_code";
            case BOOKSUBCATEGORYDESCRIPTION:
                return "book_sub_category_description";
            case BOOKTRADERID:
                return "book_trader_id";
            case CNYDELIVERABLEOUTSIDECHINAINDICATOR:
                return "cny_deliverable_outside_china_indicator";
            case DEPARTMENTID:
                return "department_id";
            case FRONTOFFICEREPRESENTATIVEID:
                return "front_office_representative_id";
            case GPLATOMCODE:
                return "gpl_atom_code";
            case NHFSINDICATOR:
                return "nhfs_indicator";
            case REMOTEBOOKINGPOLICYINDICATOR:
                return "remote_booking_policy_indicator";
            case SPLITHEDGECOMMENTS:
                return "split_hedge_comments";
            case SPLITHEDGEINDICATOR:
                return "split_hedge_indicator";
            case USTAXCATEGORYCODE:
                return "us_tax_category_code";
            case BOOKLASTUPDATEDDATETIME:
                return "book_last_updated_datetime";
            case FINANCIALBUSINESSUNITCODE:
                return "financial_business_unit_code";
            case DELTAFLAG:
                return "delta_flag";
            case BOOKREQUESTSTATUSCODE:
                return "book_request_status_code";
            case FIRSTINSTANCEBOOKACTIVATED:
                return "first_instance_book_activated";
            case LASTINSTANCEBOOKACTIVATED:
                return "last_instance_book_activated";
            case FIRSTINSTANCEBOOKINACTIVATED:
                return "first_instance_book_inactivated";
            case LASTINSTANCEBOOK:
                return "last_instance_book_";
            case EMAILADDRESS:
                return "email_address";
            case CAVFLAG:
                return "cav_flag";
            case FOSFLAG:
                return "fos_flag";
            case BOOKINGINTENT:
                return "booking_intent";
            case BOOKREQUESTID:
                return "book_request_id";
            case IPAREMPLIDEMAILID:
                return "ipar_empl_id_email_id";
            case BOOKTRADEREMAILID:
                return "book_trader_email_id";
            case PCSUPERVISOREMAILID:
                return "pc_supervisor_email_id";
            case BOOKEMPIDEMAILID:
                return "book_emp_id_email_id";
            case PCTNLEMPLIDEMAILID:
                return "pctnl_empl_id_email_id";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (property + "a") {
            case GLOBALBOOKID:
                return "String";
            case ACCOUNTMETHOD:
                return "String";
            case ACCOUNTINGBASISINDICATOR:
                return "String";
            case BOOKCATEGORYCODE:
                return "String";
            case BOOKCATEGORYDESCRIPTION:
                return "String";
            case BOOKCONTROLLERID:
                return "String";
            case BOOKDORMANTINDICATOR:
                return "String";
            case BOOKNAME:
                return "String";
            case BOOKPNLSYSTEMNAME:
                return "String";
            case BOOKREF:
                return "String";
            case BOOKSTATUS:
                return "String";
            case BOOKSUBCATEGORYCODE:
                return "String";
            case BOOKSUBCATEGORYDESCRIPTION:
                return "String";
            case BOOKTRADERID:
                return "String";
            case CNYDELIVERABLEOUTSIDECHINAINDICATOR:
                return "String";
            case DEPARTMENTID:
                return "String";
            case FRONTOFFICEREPRESENTATIVEID:
                return "String";
            case GPLATOMCODE:
                return "String";
            case NHFSINDICATOR:
                return "String";
            case REMOTEBOOKINGPOLICYINDICATOR:
                return "String";
            case SPLITHEDGECOMMENTS:
                return "String";
            case SPLITHEDGEINDICATOR:
                return "String";
            case USTAXCATEGORYCODE:
                return "String";
            case BOOKLASTUPDATEDDATETIME:
                return "java.sql.Timestamp";
            case FINANCIALBUSINESSUNITCODE:
                return "String";
            case DELTAFLAG:
                return "String";
            case BOOKREQUESTSTATUSCODE:
                return "String";
            case FIRSTINSTANCEBOOKACTIVATED:
                return "java.sql.Date";
            case LASTINSTANCEBOOKACTIVATED:
                return "java.sql.Date";
            case FIRSTINSTANCEBOOKINACTIVATED:
                return "java.sql.Date";
            case LASTINSTANCEBOOK:
                return "java.sql.Date";
            case EMAILADDRESS:
                return "String";
            case CAVFLAG:
                return "String";
            case FOSFLAG:
                return "String";
            case BOOKINGINTENT:
                return "String";
            case BOOKREQUESTID:
                return "String";
            case IPAREMPLIDEMAILID:
                return "String";
            case BOOKTRADEREMAILID:
                return "String";
            case PCSUPERVISOREMAILID:
                return "String";
            case BOOKEMPIDEMAILID:
                return "String";
            case PCTNLEMPLIDEMAILID:
                return "String";
        }
        return "String";
    }


}
