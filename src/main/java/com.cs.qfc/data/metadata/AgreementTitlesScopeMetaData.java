package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class AgreementTitlesScopeMetaData implements MetaData {

    public static final String TABLE = "AgreementTitlesScope";
    private static final String COUNT = "count";
    private static final String AGREEMENTTITLE = "agreementTitle";
    private static final String AGREEMENTTYPE = "agreementType";
    private static final String INSCOPE = "inScope";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case COUNT:
                return "count";
            case AGREEMENTTITLE:
                return "agreement_title";
            case AGREEMENTTYPE:
                return "agreement_type";
            case INSCOPE:
                return "in_scope";
        }
        return property;
    }


    @Override
    public String getColumnDataType(String property) {

        return "String";
    }


}
