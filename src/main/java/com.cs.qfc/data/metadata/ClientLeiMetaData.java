package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class ClientLeiMetaData implements MetaData {

    public static final String TABLE = "CLIENTLEI";
    private static final String ID = "id";
    private static final String COUNTERPARTYCSID = "counterPartyCsid";
    private static final String COUNTERPARTYNAME = "counterPartyName";
    private static final String COUNTERPARTYLEI = "counterPartyLei";
    private static final String ULTIMATEPARENTCSID = "ultimateParentCsid";
    private static final String ULTIMATEPARENTNAME = "ultimateParentName";
    private static final String ULTIMATEPARENTLEI = "ultimateParentLei";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property + "a")) {
            case ID:
                return "id";
            case COUNTERPARTYCSID:
                return "counter_party_csid";
            case COUNTERPARTYNAME:
                return "counter_party_name";
            case COUNTERPARTYLEI:
                return "counter_party_lei";
            case ULTIMATEPARENTCSID:
                return "ultimate_parent_csid";
            case ULTIMATEPARENTNAME:
                return "ultimate_parent_name";
            case ULTIMATEPARENTLEI:
                return "ultimate_parent_lei";
        }
        return "String";
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "Integer";
            case COUNTERPARTYCSID:
                return "String";
            case COUNTERPARTYNAME:
                return "String";
            case COUNTERPARTYLEI:
                return "String";
            case ULTIMATEPARENTCSID:
                return "String";
            case ULTIMATEPARENTNAME:
                return "String";
            case ULTIMATEPARENTLEI:
                return "String";
        }
        return null;
    }


}
