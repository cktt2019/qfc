package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class PrimeSwapPositionMetaData implements MetaData {

    public static final String TABLE = "PRIMESWAPPOSITION";
    private static final String ID = "id";
    private static final String BUSINESSDATE = "businessDate";
    private static final String SWAPID = "swapId";
    private static final String SWAPVERSION = "swapVersion";
    private static final String POSITIONID = "positionId";
    private static final String POSITIONVERSION = "positionVersion";
    private static final String SWAPTYPE = "swapType";
    private static final String COUNTERPARTYCODE = "counterpartyCode";
    private static final String COUNTERPARTYNAME = "counterpartyName";
    private static final String SETTLEDLTDCOSTBASE = "settledLtdCostBase";
    private static final String SWAPCCYTOUSDFX = "swapCcyToUsdFx";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "id";
            case BUSINESSDATE:
                return "business_date";
            case SWAPID:
                return "swap_id";
            case SWAPVERSION:
                return "swap_version";
            case POSITIONID:
                return "position_id";
            case POSITIONVERSION:
                return "position_version";
            case SWAPTYPE:
                return "swap_type";
            case COUNTERPARTYCODE:
                return "counterparty_code";
            case COUNTERPARTYNAME:
                return "counterparty_name";
            case SETTLEDLTDCOSTBASE:
                return "settled_ltd_cost_base";
            case SWAPCCYTOUSDFX:
                return "swap_ccy_to_usd_fx";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
	/*	switch (property) {
	case ID : return "Integer";
	case BUSINESSDATE : return "java.sql.Date";
	case SWAPID : return "String";
	case SWAPVERSION : return "String";
	case POSITIONID : return "String";
	case POSITIONVERSION : return "String";
	case SWAPTYPE : return "String";
	case COUNTERPARTYCODE : return "String";
	case COUNTERPARTYNAME : return "String";
	case SETTLEDLTDCOSTBASE : return "String";
	case SWAPCCYTOUSDFX : return "String";
		}*/
        return "String";
    }


}
