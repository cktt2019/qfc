package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class PorfolioRelationshipMetaData implements MetaData {

    public static final String TABLE = "PORFOLIORELATIONSHIP";
    private static final String PORTFOLIOID = "portfolioId";
    private static final String GLOBALPARTYID = "globalPartyId";
    private static final String PARTYTYPEDESCRIPTION = "partytypeDescription";
    private static final String PARTYTYPECODE = "partyTypeCode";
    private static final String PARTYLEGALNAME = "partyLegalName";
    private static final String PARTYALIASNAME = "partyAliasName";
    private static final String LEGALROLEDEFINITIONID = "legalRoleDefinitionId";
    private static final String ROLETYPE = "roleType";
    private static final String ROLEDESCRIPTION = "roleDescription";
    private static final String ROLECODE = "roleCode";
    private static final String PARTYLEGALROLEID = "partyLegalRoleId";
    private static final String SOURCEID = "sourceId";
    private static final String SOURCENAME = "sourceName";
    private static final String INTERNALPARTYALTID = "internalPartyAltId";
    private static final String PORTFOLIONAME = "portfolioName";
    private static final String PORTFOLIONUMBER = "portfolioNumber";
    private static final String LEGALENTITYNAME = "legalEntityName";
    private static final String CMSBOOKINGID = "cmsBookingId";
    private static final String ENTITYJURISDICTION = "entityJurisdiction";
    private static final String LEGALENTITYCODE = "legalEntityCode";
    private static final String PARENTTRXNCOVRG = "parentTrxnCovrg";
    private static final String TRXNCOVERAGE = "trxnCoverage";
    private static final String TRXNCOVERAGEDESCR = "trxnCoverageDescr";
    private static final String PRODUCTID = "productId";
    private static final String ACCOUNTNUMBER = "accountNumber";
    private static final String SYSTEMID = "systemId";
    private static final String SYSTEMNAME = "systemName";
    private static final String MNEMONICVALUE = "mnemonicValue";
    private static final String MNEMONICCODE = "mnemonicCode";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case PORTFOLIOID:
                return "portfolio_id";
            case GLOBALPARTYID:
                return "global_party_id";
            case PARTYTYPEDESCRIPTION:
                return "partytype_description";
            case PARTYTYPECODE:
                return "party_type_code";
            case PARTYLEGALNAME:
                return "party_legal_name";
            case PARTYALIASNAME:
                return "party_alias_name";
            case LEGALROLEDEFINITIONID:
                return "legal_role_definition_id";
            case ROLETYPE:
                return "role_type";
            case ROLEDESCRIPTION:
                return "role_description";
            case ROLECODE:
                return "role_code";
            case PARTYLEGALROLEID:
                return "party_legal_role_id";
            case SOURCEID:
                return "source_id";
            case SOURCENAME:
                return "source_name";
            case INTERNALPARTYALTID:
                return "internal_party_alt_id";
            case PORTFOLIONAME:
                return "portfolio_name";
            case PORTFOLIONUMBER:
                return "portfolio_number";
            case LEGALENTITYNAME:
                return "legal_entity_name";
            case CMSBOOKINGID:
                return "cms_booking_id";
            case ENTITYJURISDICTION:
                return "entity_jurisdiction";
            case LEGALENTITYCODE:
                return "legal_entity_code";
            case PARENTTRXNCOVRG:
                return "parent_trxn_covrg";
            case TRXNCOVERAGE:
                return "trxn_coverage";
            case TRXNCOVERAGEDESCR:
                return "trxn_coverage_descr";
            case PRODUCTID:
                return "product_id";
            case ACCOUNTNUMBER:
                return "account_number";
            case SYSTEMID:
                return "system_id";
            case SYSTEMNAME:
                return "system_name";
            case MNEMONICVALUE:
                return "mnemonic_value";
            case MNEMONICCODE:
                return "mnemonic_code";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case PORTFOLIOID:
                return "String";
            case GLOBALPARTYID:
                return "String";
            case PARTYTYPEDESCRIPTION:
                return "String";
            case PARTYTYPECODE:
                return "String";
            case PARTYLEGALNAME:
                return "String";
            case PARTYALIASNAME:
                return "String";
            case LEGALROLEDEFINITIONID:
                return "String";
            case ROLETYPE:
                return "String";
            case ROLEDESCRIPTION:
                return "String";
            case ROLECODE:
                return "String";
            case PARTYLEGALROLEID:
                return "String";
            case SOURCEID:
                return "String";
            case SOURCENAME:
                return "String";
            case INTERNALPARTYALTID:
                return "String";
            case PORTFOLIONAME:
                return "String";
            case PORTFOLIONUMBER:
                return "String";
            case LEGALENTITYNAME:
                return "String";
            case CMSBOOKINGID:
                return "String";
            case ENTITYJURISDICTION:
                return "String";
            case LEGALENTITYCODE:
                return "String";
            case PARENTTRXNCOVRG:
                return "String";
            case TRXNCOVERAGE:
                return "String";
            case TRXNCOVERAGEDESCR:
                return "String";
            case PRODUCTID:
                return "String";
            case ACCOUNTNUMBER:
                return "String";
            case SYSTEMID:
                return "String";
            case SYSTEMNAME:
                return "String";
            case MNEMONICVALUE:
                return "String";
            case MNEMONICCODE:
                return "String";
        }
        return null;
    }


}
