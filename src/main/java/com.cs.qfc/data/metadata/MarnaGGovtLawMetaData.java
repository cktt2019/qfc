package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class MarnaGGovtLawMetaData implements MetaData {

    public static final String TABLE = "a9b3562d-f93e-44a4-98f2-057f57e86227";
    private static final String BUSINESSDATE = "businessDate";
    private static final String AGMTNO = "agmtNo";
    private static final String CNID = "cnid";
    private static final String AGMTDATE = "agmtDate";
    private static final String COUNTERPARTY = "counterparty";
    private static final String CSID = "csid";
    private static final String AGMTTYPE = "agmtType";
    private static final String AGMTTITLE = "agmtTitle";
    private static final String COI = "coi";
    private static final String GOVERNINGLAW = "governingLaw";
    private static final String ID = "id";

    @Override
    public String getColumnName(String property) {
        switch (property) {
            case BUSINESSDATE:
                return "business_date";
            case AGMTNO:
                return "agmt_no";
            case CNID:
                return "cnid";
            case AGMTDATE:
                return "agmt_date";
            case COUNTERPARTY:
                return "counterparty";
            case CSID:
                return "csid";
            case AGMTTYPE:
                return "agmt_type";
            case AGMTTITLE:
                return "agmt_title";
            case COI:
                return "coi";
            case GOVERNINGLAW:
                return "governing_law";
            case ID:
                return "id";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (property) {
            case BUSINESSDATE:
                return "java.sql.Date";
            case AGMTNO:
                return "String";
            case CNID:
                return "String";
            case AGMTDATE:
                return "String";
            case COUNTERPARTY:
                return "String";
            case CSID:
                return "String";
            case AGMTTYPE:
                return "String";
            case AGMTTITLE:
                return "String";
            case COI:
                return "String";
            case GOVERNINGLAW:
                return "String";
            case ID:
                return "Integer";
        }
        return null;
    }


}
