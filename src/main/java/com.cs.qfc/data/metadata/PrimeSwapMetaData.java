package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class PrimeSwapMetaData implements MetaData {

    public static final String TABLE = "PRIMESWAP";
    private static final String BUSINESSDATE = "businessDate";
    private static final String LEGALENTITY = "legalEntity";
    private static final String SWAPID = "swapId";
    private static final String SWAPVERSION = "swapVersion";
    private static final String COUNTERPARTYCODE = "counterPartyCode";
    private static final String COUNTERPARTYNAME = "counterPartyName";
    private static final String BOOK = "book";
    private static final String TRADER = "trader";
    private static final String SWAPTYPE = "swapType";
    private static final String FRAMESOFTMASTERAGREEMENTLLK = "framesoftMasterAgreementLlk";
    private static final String ALGOCOLLATERALAGREEMENTLLK = "algoCollateralAgreementLlk";
    private static final String CDUMASTERAGREEMENTLLK = "cduMasterAgreementLlk";
    private static final String CDUCOLLATERALANNEXLLK = "cduCollateralAnnexLlk";
    private static final String SWAPTRADEDATE = "swapTradeDate";
    private static final String SWAPTERMINATIONDATE = "swapTerminationDate";
    private static final String NEXTINTERESTPAYDATE = "nextInterestPayDate";
    private static final String EQUITYCURRENCY = "equityCurrency";
    private static final String TOTALSWAPVALUE = "totalSwapValue";
    private static final String CURRENCYEQUITYNOTIONAL = "currencyEquityNotional";
    private static final String INITMARGIN = "initMargin";
    private static final String INITIALMARGINDIRECTION = "initialMarginDirection";
    private static final String CSID = "csid";
    private static final String GSID = "gsid";
    private static final String ID = "id";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case BUSINESSDATE:
                return "business_date";
            case LEGALENTITY:
                return "legal_entity";
            case SWAPID:
                return "swap_id";
            case SWAPVERSION:
                return "swap_version";
            case COUNTERPARTYCODE:
                return "counter_party_code";
            case COUNTERPARTYNAME:
                return "counter_party_name";
            case BOOK:
                return "book";
            case TRADER:
                return "trader";
            case SWAPTYPE:
                return "swap_type";
            case FRAMESOFTMASTERAGREEMENTLLK:
                return "framesoft_master_agreement_llk";
            case ALGOCOLLATERALAGREEMENTLLK:
                return "algo_collateral_agreement_llk";
            case CDUMASTERAGREEMENTLLK:
                return "cdu_master_agreement_llk";
            case CDUCOLLATERALANNEXLLK:
                return "cdu_collateral_annex_llk";
            case SWAPTRADEDATE:
                return "swap_trade_date";
            case SWAPTERMINATIONDATE:
                return "swap_termination_date";
            case NEXTINTERESTPAYDATE:
                return "next_interest_pay_date";
            case EQUITYCURRENCY:
                return "equity_currency";
            case TOTALSWAPVALUE:
                return "total_swap_value";
            case CURRENCYEQUITYNOTIONAL:
                return "currency_equity_notional";
            case INITMARGIN:
                return "init_margin";
            case INITIALMARGINDIRECTION:
                return "initial_margin_direction";
            case CSID:
                return "csid";
            case GSID:
                return "gsid";
            case ID:
                return "id";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (property) {
            case BUSINESSDATE:
                return "java.sql.Date";
            case LEGALENTITY:
                return "String";
            case SWAPID:
                return "String";
            case SWAPVERSION:
                return "String";
            case COUNTERPARTYCODE:
                return "String";
            case COUNTERPARTYNAME:
                return "String";
            case BOOK:
                return "String";
            case TRADER:
                return "String";
            case SWAPTYPE:
                return "String";
            case FRAMESOFTMASTERAGREEMENTLLK:
                return "String";
            case ALGOCOLLATERALAGREEMENTLLK:
                return "String";
            case CDUMASTERAGREEMENTLLK:
                return "String";
            case CDUCOLLATERALANNEXLLK:
                return "String";
            case SWAPTRADEDATE:
                return "java.sql.Date";
            case SWAPTERMINATIONDATE:
                return "java.sql.Date";
            case NEXTINTERESTPAYDATE:
                return "java.sql.Date";
            case EQUITYCURRENCY:
                return "String";
            case TOTALSWAPVALUE:
                return "Float";
            case CURRENCYEQUITYNOTIONAL:
                return "String";
            case INITMARGIN:
                return "Float";
            case INITIALMARGINDIRECTION:
                return "String";
            case CSID:
                return "String";
            case GSID:
                return "String";
            case ID:
                return "Integer";
        }
        return "String";
    }


}
