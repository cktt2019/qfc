package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class DrdTradeHeaderMetaData implements MetaData {

    public static final String TABLE = "DRDTRADEHEADER";
    private static final String ID = "id";
    private static final String COBDATE = "cobDate";
    private static final String LOADCODDATE = "loadCodDate";
    private static final String BOOKNAME = "bookName";
    private static final String TRADELOADID = "tradeLoadId";
    private static final String LOADTIMESTAMP = "loadTimestamp";
    private static final String SNAPSHOTNAME = "snapshotName";
    private static final String SNAPSHOTID = "snapshotId";
    private static final String SNAPSHOTCREATIONDATETIME = "snapshotCreationDatetime";
    private static final String INTERFACEVERSION = "interfaceVersion";
    private static final String TRADEID = "tradeId";
    private static final String TFTRADEVERSION = "tfTradeVersion";
    private static final String TRADELOCATIONID = "tradeLocationId";
    private static final String PRIMARYTCN = "primaryTcn";
    private static final String TRADEVERSIONUPDATETIME = "tradeVersionUpdateTime";
    private static final String CANCELLATIONDATE = "cancellationDate";
    private static final String XCCYINDICATOR = "xccyIndicator";
    private static final String GBMENTITY = "gbmEntity";
    private static final String TOTALLOCALPV = "totalLocalPv";
    private static final String TOTALLOCALPVCCYCODE = "totalLocalPvCcyCode";
    private static final String TOTALPVSTATUS = "totalPvStatus";
    private static final String TOTALRPTPV = "totalRpTpv";
    private static final String TRADEPVREGDATE = "tradePvRegDate";
    private static final String TRADEPVREGDAYS = "tradePvRegDays";
    private static final String CONFIRMATIONSTATUS = "confirmationStatus";
    private static final String DEFAULTFAIRVALUE = "defaultFairValue";
    private static final String PROVECLUSTER = "proveCluster";
    private static final String PROVEFAMILY = "proveFamily";
    private static final String PROVEID = "proveId";
    private static final String PROVEPRODUCT = "proveProduct";
    private static final String APPROVALSTATUS = "approvalStatus";
    private static final String EVENTREASON = "eventReason";
    private static final String EXTERNALID = "externalId";
    private static final String PRIMARYMATURITYDATE = "primaryMaturityDate";
    private static final String PRODUCTSUBTYPE = "productSubType";
    private static final String PRODUCTTYPE = "productType";
    private static final String PRODUCTVALUATIONTYPE = "productValuationType";
    private static final String ROOTTCN = "rootTcn";
    private static final String TRADESTATUS = "tradeStatus";
    private static final String CDUMASTERAGREEMENTLLK = "cduMasterAgreementLlk";
    private static final String CDUCOLLATERALANNEXLLK = "cduCollateralAnnexLlk";
    private static final String FREAMESOFTMASTERAGREEMENTLLK = "freamesoftMasterAgreementLlk";
    private static final String ALGOCOLLATERALANNEXLLK = "algoCollateralAnnexLlk";
    private static final String ACCOUNTAREA = "accountArea";
    private static final String BROKERID = "brokerId";
    private static final String COLLINDEPAMOUNT = "collIndepAmount";
    private static final String COLLINDEPCCY = "collIndepCcy";
    private static final String COLLINDEPOTHERMARGQUOTE = "collIndepOtherMargQuote";
    private static final String COLLINDEPPERCENT = "collIndepPercent";
    private static final String COLLINDEPTYPE = "collIndepType";
    private static final String MARKETERID = "marketerId";
    private static final String RISKMANAGEMENTONLYFLAG = "riskManagementOnlyFlag";
    private static final String STRUCTUREDTRADEFLAG = "structuredTradeFlag";
    private static final String TRADEDATE = "tradeDate";
    private static final String TRADERNAME = "traderName";
    private static final String TRADESOURCESYSTEM = "tradeSourceSystem";
    private static final String TRADINGPARTYID = "tradingPartyId";
    private static final String TRADINGPARTYIDTYPE = "tradingPartyIdType";
    private static final String CITY = "city";
    private static final String DEAL = "deal";
    private static final String STRIPID = "stripId";
    private static final String CUSTOMERNUMBER = "customerNumber";
    private static final String DISPLAYID = "displayId";
    private static final String ASSETCLASS1 = "assetClass1";
    private static final String ASSETCLASS2 = "assetClass2";
    private static final String ASSETCLASS3 = "assetClass3";
    private static final String ASSETCLASS4 = "assetClass4";
    private static final String LINKEDSETTYPE = "linkedSetType";
    private static final String SYSLINKID = "sysLinkId";
    private static final String LEADTRADEID = "leadTradeId";
    private static final String LEADTRADEVERSION = "leadTradeVersion";
    private static final String LEADTRADELOCATIONID = "leadTradeLocationId";
    private static final String LEADEXTERNALID = "leadExternalId";
    private static final String LEADTRADEBOOK = "leadTradeBook";
    private static final String WASHTRADEID = "washTradeId";
    private static final String WASHTRADEVERSION = "washTradeVersion";
    private static final String WASHTRADELOCATIONID = "washTradeLocationId";
    private static final String WASHEXTERNALID = "washExternalId";
    private static final String WASHTRADEBOOK = "washTradeBook";
    private static final String RISKTRADEID = "riskTradeId";
    private static final String RISKTRADEVERSION = "riskTradeVersion";
    private static final String RISKTRADELOCATIONID = "riskTradeLocationId";
    private static final String RISKEXTERNALID = "riskExternalId";
    private static final String RISKTRADEBOOK = "riskTradeBook";
    private static final String SYNTHETICTRADEINDICATOR = "syntheticTradeIndicator";
    private static final String LINKAGESTATUS = "linkageStatus";
    private static final String LINKAGESTATUSDESCRIPTION = "linkageStatusDescription";
    private static final String LINKEDTRADEID = "linkedTradeId";
    private static final String LINKEDPRIMARYTCN = "linkedPrimaryTcn";
    private static final String LINKEDTRADEVERSION = "linkedTradeVersion";
    private static final String LINKEDTRADELOCATIONID = "linkedTradeLocationId";
    private static final String LINKEDTRADEEXTERNALID = "linkedTradeExternalId";
    private static final String LINKEDTRADEBOOK = "linkedTradeBook";
    private static final String VALUATIONCLASS = "valuationClass";
    private static final String VALUATIONTYPE = "valuationType";
    private static final String PREMIUMAMOUNT = "premiumAmount";
    private static final String SWAPSWIREID = "swapsWireId";
    private static final String APPROXBOOKING = "approxBooking";
    private static final String APPROXBOOKINGREVIEWDATE = "approxBookingReviewDate";
    private static final String DOCSCOUNTERPARTYID = "docsCounterPartyId";
    private static final String CLOINDICATOR = "cloIndicator";
    private static final String LOANMITIGATIONFLAG = "loanMitigationFlag";
    private static final String USI = "usi";
    private static final String CSSI = "cssi";
    private static final String GMBBOOKID = "gmbBookId";
    private static final String AREA = "area";
    private static final String STRUCTUREDID = "structuredId";
    private static final String PVLEVEL = "pvLevel";
    private static final String COUNTERPARTYCSID = "counterPartyCsid";
    private static final String CREETRADEIDENTIFIER = "creeTradeIdentifier";
    private static final String CREETRADEIDENTIFIERVERSION = "creeTradeIdentifierVersion";
    private static final String LCHORIGCOUNTERPARTY = "lchOrigCounterParty";
    private static final String GBMCLUSTER = "gbmCluster";
    private static final String GBMDIVISION = "gbmDivision";
    private static final String AFFIRMPLATFORMSTATUS = "affirmPlatformStatus";
    private static final String DOCSACTUALPROCESSINGMETHOD = "docsActualProcessingMethod";
    private static final String UTI = "uti";
    private static final String SWIREMANUALCONFIRMEDREQ = "swireManualConfirmedReq";
    private static final String ENTRYDATETIME = "entryDateTime";
    private static final String FUNDINGINDICATOR = "fundingIndicator";
    private static final String LEVERAGEFACTOR = "leverageFactor";
    private static final String LINKTRANSID = "linkTransId";
    private static final String LINKTYPE = "linkType";
    private static final String MASTERTRADETRADEID = "masterTradeTradeId";
    private static final String GBMBOOKREF = "gbmBookRef";
    private static final String LASTAMMENDBY = "lastAmmendBy";
    private static final String STRUCTURETYPE = "structureType";
    private static final String GARANTIASYSID = "garantiaSysId";
    private static final String EARLYTMTERMPARTY = "earlyTmTermParty";
    private static final String EARLYTMOPTSTARTDATE = "earlyTmOptStartDate";
    private static final String COUNTERPARTYGSID = "counterPartyGsid";
    private static final String ENTITYTYPE = "entityType";
    private static final String EVENT = "event";
    private static final String EVENTTRADEDATE = "eventTradeDate";
    private static final String FIDCPMINITIALMARGIN = "fidcPmInitialMargin";
    private static final String FIDCPMNETTINGONLY = "fidcPmNettingOnly";
    private static final String FIDCPMVARIATIONMARGIN = "fidcPmVariationMargin";
    private static final String LATETRDNOTIFICATIONREASON = "lateTrdNotificationReason";
    private static final String MASTERAGREEMENTTYPE = "masterAgreementType";
    private static final String NOTESFRONTOFFICE = "notesFrontOffice";
    private static final String RISKADVISOR = "riskAdvisor";
    private static final String RISKTRADER = "riskTrader";
    private static final String TRADINGPARTYGSID = "tradingPartyGsid";
    private static final String FIDCPMIND = "fidcPmInd";
    private static final String AGENT = "agent";
    private static final String PAYMENTINDICATOR = "paymentIndicator";
    private static final String RENEGOTIATIONDATE = "renegotiationDate";
    private static final String ALTERNATIVEVERSIONID = "alternativeVersionId";
    private static final String CIFBU = "cifbu";
    private static final String DESKBOOK = "deskBook";
    private static final String METALINKTRADEID = "metalInkTradeId";
    private static final String LINKEDSTARTDATE = "linkedStartDate";
    private static final String LINKEDENDDATE = "linkedEndDate";
    private static final String SALESPERSONID = "salespersonId";
    private static final String SOURCESYSTEMID = "sourceSystemId";
    private static final String SOURCESYSTEMVERSION = "sourceSystemVersion";
    private static final String EARLYTMEXERCISEFREQ = "earlyTmExerciseFreq";
    private static final String EARLYTMOPTSTYLE = "earlyTmOptStyle";
    private static final String PAYMENTPACKAGESYSLINKID = "paymentPackageSysLinkId";
    private static final String TFMATCHSTATUS = "tfMatchStatus";
    private static final String BESTMATCHTFTRADEVERSION = "bestMatchTfTradeVersion";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "id";
            case COBDATE:
                return "cob_date";
            case LOADCODDATE:
                return "load_cod_date";
            case BOOKNAME:
                return "book_name";
            case TRADELOADID:
                return "trade_load_id";
            case LOADTIMESTAMP:
                return "load_timestamp";
            case SNAPSHOTNAME:
                return "snapshot_name";
            case SNAPSHOTID:
                return "snapshot_id";
            case SNAPSHOTCREATIONDATETIME:
                return "snapshot_creation_datetime";
            case INTERFACEVERSION:
                return "interface_version";
            case TRADEID:
                return "trade_id";
            case TFTRADEVERSION:
                return "tf_trade_version";
            case TRADELOCATIONID:
                return "trade_location_id";
            case PRIMARYTCN:
                return "primary_tcn";
            case TRADEVERSIONUPDATETIME:
                return "trade_version_update_time";
            case CANCELLATIONDATE:
                return "cancellation_date";
            case XCCYINDICATOR:
                return "xccy_indicator";
            case GBMENTITY:
                return "gbm_entity";
            case TOTALLOCALPV:
                return "total_local_pv";
            case TOTALLOCALPVCCYCODE:
                return "total_local_pv_ccy_code";
            case TOTALPVSTATUS:
                return "total_pv_status";
            case TOTALRPTPV:
                return "total_rp_tpv";
            case TRADEPVREGDATE:
                return "trade_pv_reg_date";
            case TRADEPVREGDAYS:
                return "trade_pv_reg_days";
            case CONFIRMATIONSTATUS:
                return "confirmation_status";
            case DEFAULTFAIRVALUE:
                return "default_fair_value";
            case PROVECLUSTER:
                return "prove_cluster";
            case PROVEFAMILY:
                return "prove_family";
            case PROVEID:
                return "prove_id";
            case PROVEPRODUCT:
                return "prove_product";
            case APPROVALSTATUS:
                return "approval_status";
            case EVENTREASON:
                return "event_reason";
            case EXTERNALID:
                return "external_id";
            case PRIMARYMATURITYDATE:
                return "primary_maturity_date";
            case PRODUCTSUBTYPE:
                return "product_sub_type";
            case PRODUCTTYPE:
                return "product_type";
            case PRODUCTVALUATIONTYPE:
                return "product_valuation_type";
            case ROOTTCN:
                return "root_tcn";
            case TRADESTATUS:
                return "trade_status";
            case CDUMASTERAGREEMENTLLK:
                return "cdu_master_agreement_llk";
            case CDUCOLLATERALANNEXLLK:
                return "cdu_collateral_annex_llk";
            case FREAMESOFTMASTERAGREEMENTLLK:
                return "freamesoft_master_agreement_llk";
            case ALGOCOLLATERALANNEXLLK:
                return "algo_collateral_annex_llk";
            case ACCOUNTAREA:
                return "account_area";
            case BROKERID:
                return "broker_id";
            case COLLINDEPAMOUNT:
                return "coll_indep_amount";
            case COLLINDEPCCY:
                return "coll_indep_ccy";
            case COLLINDEPOTHERMARGQUOTE:
                return "coll_indep_other_marg_quote";
            case COLLINDEPPERCENT:
                return "coll_indep_percent";
            case COLLINDEPTYPE:
                return "coll_indep_type";
            case MARKETERID:
                return "marketer_id";
            case RISKMANAGEMENTONLYFLAG:
                return "risk_management_only_flag";
            case STRUCTUREDTRADEFLAG:
                return "structured_trade_flag";
            case TRADEDATE:
                return "trade_date";
            case TRADERNAME:
                return "trader_name";
            case TRADESOURCESYSTEM:
                return "trade_source_system";
            case TRADINGPARTYID:
                return "trading_party_id";
            case TRADINGPARTYIDTYPE:
                return "trading_party_id_type";
            case CITY:
                return "city";
            case DEAL:
                return "deal";
            case STRIPID:
                return "strip_id";
            case CUSTOMERNUMBER:
                return "customer_number";
            case DISPLAYID:
                return "display_id";
            case ASSETCLASS1:
                return "asset_class1";
            case ASSETCLASS2:
                return "asset_class2";
            case ASSETCLASS3:
                return "asset_class3";
            case ASSETCLASS4:
                return "asset_class4";
            case LINKEDSETTYPE:
                return "linked_set_type";
            case SYSLINKID:
                return "sys_link_id";
            case LEADTRADEID:
                return "lead_trade_id";
            case LEADTRADEVERSION:
                return "lead_trade_version";
            case LEADTRADELOCATIONID:
                return "lead_trade_location_id";
            case LEADEXTERNALID:
                return "lead_external_id";
            case LEADTRADEBOOK:
                return "lead_trade_book";
            case WASHTRADEID:
                return "wash_trade_id";
            case WASHTRADEVERSION:
                return "wash_trade_version";
            case WASHTRADELOCATIONID:
                return "wash_trade_location_id";
            case WASHEXTERNALID:
                return "wash_external_id";
            case WASHTRADEBOOK:
                return "wash_trade_book";
            case RISKTRADEID:
                return "risk_trade_id";
            case RISKTRADEVERSION:
                return "risk_trade_version";
            case RISKTRADELOCATIONID:
                return "risk_trade_location_id";
            case RISKEXTERNALID:
                return "risk_external_id";
            case RISKTRADEBOOK:
                return "risk_trade_book";
            case SYNTHETICTRADEINDICATOR:
                return "synthetic_trade_indicator";
            case LINKAGESTATUS:
                return "linkage_status";
            case LINKAGESTATUSDESCRIPTION:
                return "linkage_status_description";
            case LINKEDTRADEID:
                return "linked_trade_id";
            case LINKEDPRIMARYTCN:
                return "linked_primary_tcn";
            case LINKEDTRADEVERSION:
                return "linked_trade_version";
            case LINKEDTRADELOCATIONID:
                return "linked_trade_location_id";
            case LINKEDTRADEEXTERNALID:
                return "linked_trade_external_id";
            case LINKEDTRADEBOOK:
                return "linked_trade_book";
            case VALUATIONCLASS:
                return "valuation_class";
            case VALUATIONTYPE:
                return "valuation_type";
            case PREMIUMAMOUNT:
                return "premium_amount";
            case SWAPSWIREID:
                return "swaps_wire_id";
            case APPROXBOOKING:
                return "approx_booking";
            case APPROXBOOKINGREVIEWDATE:
                return "approx_booking_review_date";
            case DOCSCOUNTERPARTYID:
                return "docs_counter_party_id";
            case CLOINDICATOR:
                return "clo_indicator";
            case LOANMITIGATIONFLAG:
                return "loan_mitigation_flag";
            case USI:
                return "usi";
            case CSSI:
                return "cssi";
            case GMBBOOKID:
                return "gmb_book_id";
            case AREA:
                return "area";
            case STRUCTUREDID:
                return "structured_id";
            case PVLEVEL:
                return "pv_level";
            case COUNTERPARTYCSID:
                return "counter_party_csid";
            case CREETRADEIDENTIFIER:
                return "cree_trade_identifier";
            case CREETRADEIDENTIFIERVERSION:
                return "cree_trade_identifier_version";
            case LCHORIGCOUNTERPARTY:
                return "lch_orig_counter_party";
            case GBMCLUSTER:
                return "gbm_cluster";
            case GBMDIVISION:
                return "gbm_division";
            case AFFIRMPLATFORMSTATUS:
                return "affirm_platform_status";
            case DOCSACTUALPROCESSINGMETHOD:
                return "docs_actual_processing_method";
            case UTI:
                return "uti";
            case SWIREMANUALCONFIRMEDREQ:
                return "swire_manual_confirmed_req";
            case ENTRYDATETIME:
                return "entry_date_time";
            case FUNDINGINDICATOR:
                return "funding_indicator";
            case LEVERAGEFACTOR:
                return "leverage_factor";
            case LINKTRANSID:
                return "link_trans_id";
            case LINKTYPE:
                return "link_type";
            case MASTERTRADETRADEID:
                return "master_trade_trade_id";
            case GBMBOOKREF:
                return "gbm_book_ref";
            case LASTAMMENDBY:
                return "last_ammend_by";
            case STRUCTURETYPE:
                return "structure_type";
            case GARANTIASYSID:
                return "garantia_sys_id";
            case EARLYTMTERMPARTY:
                return "early_tm_term_party";
            case EARLYTMOPTSTARTDATE:
                return "early_tm_opt_start_date";
            case COUNTERPARTYGSID:
                return "counter_party_gsid";
            case ENTITYTYPE:
                return "entity_type";
            case EVENT:
                return "event";
            case EVENTTRADEDATE:
                return "event_trade_date";
            case FIDCPMINITIALMARGIN:
                return "fidc_pm_initial_margin";
            case FIDCPMNETTINGONLY:
                return "fidc_pm_netting_only";
            case FIDCPMVARIATIONMARGIN:
                return "fidc_pm_variation_margin";
            case LATETRDNOTIFICATIONREASON:
                return "late_trd_notification_reason";
            case MASTERAGREEMENTTYPE:
                return "master_agreement_type";
            case NOTESFRONTOFFICE:
                return "notes_front_office";
            case RISKADVISOR:
                return "risk_advisor";
            case RISKTRADER:
                return "risk_trader";
            case TRADINGPARTYGSID:
                return "trading_party_gsid";
            case FIDCPMIND:
                return "fidc_pm_ind";
            case AGENT:
                return "agent";
            case PAYMENTINDICATOR:
                return "payment_indicator";
            case RENEGOTIATIONDATE:
                return "renegotiation_date";
            case ALTERNATIVEVERSIONID:
                return "alternative_version_id";
            case CIFBU:
                return "cifbu";
            case DESKBOOK:
                return "desk_book";
            case METALINKTRADEID:
                return "metal_ink_trade_id";
            case LINKEDSTARTDATE:
                return "linked_start_date";
            case LINKEDENDDATE:
                return "linked_end_date";
            case SALESPERSONID:
                return "salesperson_id";
            case SOURCESYSTEMID:
                return "source_system_id";
            case SOURCESYSTEMVERSION:
                return "source_system_version";
            case EARLYTMEXERCISEFREQ:
                return "early_tm_exercise_freq";
            case EARLYTMOPTSTYLE:
                return "early_tm_opt_style";
            case PAYMENTPACKAGESYSLINKID:
                return "payment_package_sys_link_id";
            case TFMATCHSTATUS:
                return "tf_match_status";
            case BESTMATCHTFTRADEVERSION:
                return "best_match_tf_trade_version";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "Integer";
            case COBDATE:
                return "java.sql.Date";
            case LOADCODDATE:
                return "java.sql.Date";
            case BOOKNAME:
                return "String";
            case TRADELOADID:
                return "String";
            case LOADTIMESTAMP:
                return "Date";
            case SNAPSHOTNAME:
                return "String";
            case SNAPSHOTID:
                return "Integer";
            case SNAPSHOTCREATIONDATETIME:
                return "Date";
            case INTERFACEVERSION:
                return "String";
            case TRADEID:
                return "Integer";
            case TFTRADEVERSION:
                return "Integer";
            case TRADELOCATIONID:
                return "Integer";
            case PRIMARYTCN:
                return "String";
            case TRADEVERSIONUPDATETIME:
                return "Date";
            case CANCELLATIONDATE:
                return "String";
            case XCCYINDICATOR:
                return "String";
            case GBMENTITY:
                return "String";
            case TOTALLOCALPV:
                return "Integer";
            case TOTALLOCALPVCCYCODE:
                return "String";
            case TOTALPVSTATUS:
                return "String";
            case TOTALRPTPV:
                return "Integer";
            case TRADEPVREGDATE:
                return "java.sql.Date";
            case TRADEPVREGDAYS:
                return "Integer";
            case CONFIRMATIONSTATUS:
                return "String";
            case DEFAULTFAIRVALUE:
                return "Integer";
            case PROVECLUSTER:
                return "String";
            case PROVEFAMILY:
                return "String";
            case PROVEID:
                return "String";
            case PROVEPRODUCT:
                return "String";
            case APPROVALSTATUS:
                return "String";
            case EVENTREASON:
                return "String";
            case EXTERNALID:
                return "String";
            case PRIMARYMATURITYDATE:
                return "java.sql.Date";
            case PRODUCTSUBTYPE:
                return "String";
            case PRODUCTTYPE:
                return "String";
            case PRODUCTVALUATIONTYPE:
                return "String";
            case ROOTTCN:
                return "String";
            case TRADESTATUS:
                return "String";
            case CDUMASTERAGREEMENTLLK:
                return "String";
            case CDUCOLLATERALANNEXLLK:
                return "String";
            case FREAMESOFTMASTERAGREEMENTLLK:
                return "Integer";
            case ALGOCOLLATERALANNEXLLK:
                return "Integer";
            case ACCOUNTAREA:
                return "String";
            case BROKERID:
                return "String";
            case COLLINDEPAMOUNT:
                return "Float";
            case COLLINDEPCCY:
                return "String";
            case COLLINDEPOTHERMARGQUOTE:
                return "String";
            case COLLINDEPPERCENT:
                return "Float";
            case COLLINDEPTYPE:
                return "String";
            case MARKETERID:
                return "String";
            case RISKMANAGEMENTONLYFLAG:
                return "String";
            case STRUCTUREDTRADEFLAG:
                return "String";
            case TRADEDATE:
                return "java.sql.Date";
            case TRADERNAME:
                return "String";
            case TRADESOURCESYSTEM:
                return "String";
            case TRADINGPARTYID:
                return "String";
            case TRADINGPARTYIDTYPE:
                return "String";
            case CITY:
                return "String";
            case DEAL:
                return "String";
            case STRIPID:
                return "String";
            case CUSTOMERNUMBER:
                return "String";
            case DISPLAYID:
                return "String";
            case ASSETCLASS1:
                return "String";
            case ASSETCLASS2:
                return "String";
            case ASSETCLASS3:
                return "String";
            case ASSETCLASS4:
                return "String";
            case LINKEDSETTYPE:
                return "String";
            case SYSLINKID:
                return "String";
            case LEADTRADEID:
                return "Integer";
            case LEADTRADEVERSION:
                return "Integer";
            case LEADTRADELOCATIONID:
                return "Integer";
            case LEADEXTERNALID:
                return "String";
            case LEADTRADEBOOK:
                return "String";
            case WASHTRADEID:
                return "Integer";
            case WASHTRADEVERSION:
                return "Integer";
            case WASHTRADELOCATIONID:
                return "Integer";
            case WASHEXTERNALID:
                return "String";
            case WASHTRADEBOOK:
                return "String";
            case RISKTRADEID:
                return "Integer";
            case RISKTRADEVERSION:
                return "Integer";
            case RISKTRADELOCATIONID:
                return "Integer";
            case RISKEXTERNALID:
                return "String";
            case RISKTRADEBOOK:
                return "String";
            case SYNTHETICTRADEINDICATOR:
                return "String";
            case LINKAGESTATUS:
                return "String";
            case LINKAGESTATUSDESCRIPTION:
                return "String";
            case LINKEDTRADEID:
                return "Integer";
            case LINKEDPRIMARYTCN:
                return "String";
            case LINKEDTRADEVERSION:
                return "Integer";
            case LINKEDTRADELOCATIONID:
                return "String";
            case LINKEDTRADEEXTERNALID:
                return "String";
            case LINKEDTRADEBOOK:
                return "String";
            case VALUATIONCLASS:
                return "String";
            case VALUATIONTYPE:
                return "String";
            case PREMIUMAMOUNT:
                return "Float";
            case SWAPSWIREID:
                return "String";
            case APPROXBOOKING:
                return "String";
            case APPROXBOOKINGREVIEWDATE:
                return "java.sql.Date";
            case DOCSCOUNTERPARTYID:
                return "String";
            case CLOINDICATOR:
                return "String";
            case LOANMITIGATIONFLAG:
                return "String";
            case USI:
                return "String";
            case CSSI:
                return "String";
            case GMBBOOKID:
                return "Integer";
            case AREA:
                return "String";
            case STRUCTUREDID:
                return "String";
            case PVLEVEL:
                return "String";
            case COUNTERPARTYCSID:
                return "String";
            case CREETRADEIDENTIFIER:
                return "String";
            case CREETRADEIDENTIFIERVERSION:
                return "String";
            case LCHORIGCOUNTERPARTY:
                return "String";
            case GBMCLUSTER:
                return "String";
            case GBMDIVISION:
                return "String";
            case AFFIRMPLATFORMSTATUS:
                return "String";
            case DOCSACTUALPROCESSINGMETHOD:
                return "String";
            case UTI:
                return "String";
            case SWIREMANUALCONFIRMEDREQ:
                return "String";
            case ENTRYDATETIME:
                return "java.sql.Date";
            case FUNDINGINDICATOR:
                return "String";
            case LEVERAGEFACTOR:
                return "Float";
            case LINKTRANSID:
                return "String";
            case LINKTYPE:
                return "String";
            case MASTERTRADETRADEID:
                return "String";
            case GBMBOOKREF:
                return "String";
            case LASTAMMENDBY:
                return "String";
            case STRUCTURETYPE:
                return "String";
            case GARANTIASYSID:
                return "String";
            case EARLYTMTERMPARTY:
                return "String";
            case EARLYTMOPTSTARTDATE:
                return "java.sql.Date";
            case COUNTERPARTYGSID:
                return "String";
            case ENTITYTYPE:
                return "String";
            case EVENT:
                return "String";
            case EVENTTRADEDATE:
                return "java.sql.Date";
            case FIDCPMINITIALMARGIN:
                return "String";
            case FIDCPMNETTINGONLY:
                return "String";
            case FIDCPMVARIATIONMARGIN:
                return "String";
            case LATETRDNOTIFICATIONREASON:
                return "String";
            case MASTERAGREEMENTTYPE:
                return "Integer";
            case NOTESFRONTOFFICE:
                return "String";
            case RISKADVISOR:
                return "String";
            case RISKTRADER:
                return "String";
            case TRADINGPARTYGSID:
                return "String";
            case FIDCPMIND:
                return "String";
            case AGENT:
                return "String";
            case PAYMENTINDICATOR:
                return "String";
            case RENEGOTIATIONDATE:
                return "java.sql.Date";
            case ALTERNATIVEVERSIONID:
                return "Integer";
            case CIFBU:
                return "String";
            case DESKBOOK:
                return "String";
            case METALINKTRADEID:
                return "String";
            case LINKEDSTARTDATE:
                return "java.sql.Date";
            case LINKEDENDDATE:
                return "java.sql.Date";
            case SALESPERSONID:
                return "String";
            case SOURCESYSTEMID:
                return "String";
            case SOURCESYSTEMVERSION:
                return "Integer";
            case EARLYTMEXERCISEFREQ:
                return "String";
            case EARLYTMOPTSTYLE:
                return "String";
            case PAYMENTPACKAGESYSLINKID:
                return "String";
            case TFMATCHSTATUS:
                return "String";
            case BESTMATCHTFTRADEVERSION:
                return "Integer";
        }
        return null;
    }


}
