package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class EquityInstrumentMetaData implements MetaData {

    public static final String TABLE = "EQUITYINSTRUMENT";
    private static final String ID = "gpiIssue";
    private static final String ISINID = "isinId";
    private static final String SEDOLID = "sedolId";
    private static final String CUSIPID = "cusipId";
    private static final String SECURITYDESCRIPTIONLONG = "securityDescriptionLong";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "gpiIssue";
            case ISINID:
                return "isin_id";
            case SEDOLID:
                return "sedol_id";
            case CUSIPID:
                return "cusip_id";
            case SECURITYDESCRIPTIONLONG:
                return "security_description_long";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "Integer";
            case ISINID:
                return "String";
            case SEDOLID:
                return "String";
            case CUSIPID:
                return "String";
            case SECURITYDESCRIPTIONLONG:
                return "String";
        }
        return null;
    }


}
