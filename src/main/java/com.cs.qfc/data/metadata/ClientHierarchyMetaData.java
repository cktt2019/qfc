package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class ClientHierarchyMetaData implements MetaData {

    public static final String TABLE = "CLIENTHIERARCHY";
    private static final String CSID = "csid";
    private static final String GSID = "gsid";
    private static final String COUNTERPARTYNAME = "counterPartyName";
    private static final String IMMEDIATEPARENTCSID = "immediateParentCsid";
    private static final String IMMEDIATEPARENTGSID = "immediateParentGsid";
    private static final String ULTIMATEPARENTCSID = "ultimateParentCsid";
    private static final String ULTIMATEPARENTGSID = "ultimateParentGsid";
    private static final String ID = "gpiIssue";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case CSID:
                return "csid";
            case GSID:
                return "gsid";
            case COUNTERPARTYNAME:
                return "counter_party_name";
            case IMMEDIATEPARENTCSID:
                return "immediate_parent_csid";
            case IMMEDIATEPARENTGSID:
                return "immediate_parent_gsid";
            case ULTIMATEPARENTCSID:
                return "ultimate_parent_csid";
            case ULTIMATEPARENTGSID:
                return "ultimate_parent_gsid";
            case ID:
                return "gpiIssue";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property + "a")) {
            case CSID:
                return "String";
            case GSID:
                return "String";
            case COUNTERPARTYNAME:
                return "String";
            case IMMEDIATEPARENTCSID:
                return "String";
            case IMMEDIATEPARENTGSID:
                return "String";
            case ULTIMATEPARENTCSID:
                return "String";
            case ULTIMATEPARENTGSID:
                return "String";
            case ID:
                return "Integer";
        }
        return "String";
    }


}
