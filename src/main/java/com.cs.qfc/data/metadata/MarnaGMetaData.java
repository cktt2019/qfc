package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class MarnaGMetaData implements MetaData {

    public static final String TABLE = "MARNAG";
    private static final String CNID = "cnid";
    private static final String AGREEMENTNUMBER = "agreementNumber";
    private static final String STATUS = "status";
    private static final String SUPERSEDEDBYCNID = "supersededByCnid";
    private static final String SUPERSEDEDBYAGREEMENT = "supersededByAgreement";
    private static final String LEGALAGREEMENTDESCRIPTION = "legalAgreementDescription";
    private static final String CSENTITYBOOKINGGROUPID = "csEntityBookinggroupId";
    private static final String CSENTITYBOOKINGGROUPNAME = "csEntityBookinggroupName";
    private static final String NEGOTIATOR = "negotiator";
    private static final String NEGOTIATORLOCATION = "negotiatorLocation";
    private static final String COUNTERPARTY = "counterParty";
    private static final String COUNTERPARTYROLE = "counterPartyRole";
    private static final String CSID = "csid";
    private static final String LEGALID = "legalId";
    private static final String FORMERLYKNOWNAS = "formerlyKnownAs";
    private static final String COUNTRYOFINC = "countryOfInc";
    private static final String CONTRACTTYPE = "contractType";
    private static final String REQUESTTYPE = "requestType";
    private static final String AGREEMENTTITLE = "agreementTitle";
    private static final String CONTRACTDATE = "contractDate";
    private static final String AGENCY = "agency";
    private static final String COLLATERALAGREEMENTFLAG = "collateralAgreementFlag";
    private static final String CSAENFORCEABLEFLAG = "csaEnforceableFlag";
    private static final String TRANSACTIONSPECIFICFLAG = "transactionSpecificFlag";
    private static final String CSENTITYAGENCYFLAG = "csEntityAgencyFlag";
    private static final String DESK = "desk";
    private static final String ICONID = "iconid";
    private static final String CSAREGFLAG = "csaRegFlag";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case CNID:
                return "cnid";
            case AGREEMENTNUMBER:
                return "agreement_number";
            case STATUS:
                return "status";
            case SUPERSEDEDBYCNID:
                return "superseded_by_cnid";
            case SUPERSEDEDBYAGREEMENT:
                return "superseded_by_agreement";
            case LEGALAGREEMENTDESCRIPTION:
                return "legal_agreement_description";
            case CSENTITYBOOKINGGROUPID:
                return "cs_entity_bookinggroup_id";
            case CSENTITYBOOKINGGROUPNAME:
                return "cs_entity_bookinggroup_name";
            case NEGOTIATOR:
                return "negotiator";
            case NEGOTIATORLOCATION:
                return "negotiator_location";
            case COUNTERPARTY:
                return "counter_party";
            case COUNTERPARTYROLE:
                return "counter_party_role";
            case CSID:
                return "csid";
            case LEGALID:
                return "legal_id";
            case FORMERLYKNOWNAS:
                return "formerly_known_as";
            case COUNTRYOFINC:
                return "country_of_inc";
            case CONTRACTTYPE:
                return "contract_type";
            case REQUESTTYPE:
                return "request_type";
            case AGREEMENTTITLE:
                return "agreement_title";
            case CONTRACTDATE:
                return "contract_date";
            case AGENCY:
                return "agency";
            case COLLATERALAGREEMENTFLAG:
                return "collateral_agreement_flag";
            case CSAENFORCEABLEFLAG:
                return "csa_enforceable_flag";
            case TRANSACTIONSPECIFICFLAG:
                return "transaction_specific_flag";
            case CSENTITYAGENCYFLAG:
                return "cs_entity_agency_flag";
            case DESK:
                return "desk";
            case ICONID:
                return "iconid";
            case CSAREGFLAG:
                return "csa_reg_flag";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case CNID:
                return "Integer";
            case AGREEMENTNUMBER:
                return "String";
            case STATUS:
                return "String";
            case SUPERSEDEDBYCNID:
                return "String";
            case SUPERSEDEDBYAGREEMENT:
                return "String";
            case LEGALAGREEMENTDESCRIPTION:
                return "String";
            case CSENTITYBOOKINGGROUPID:
                return "String";
            case CSENTITYBOOKINGGROUPNAME:
                return "String";
            case NEGOTIATOR:
                return "String";
            case NEGOTIATORLOCATION:
                return "String";
            case COUNTERPARTY:
                return "String";
            case COUNTERPARTYROLE:
                return "String";
            case CSID:
                return "String";
            case LEGALID:
                return "String";
            case FORMERLYKNOWNAS:
                return "String";
            case COUNTRYOFINC:
                return "String";
            case CONTRACTTYPE:
                return "String";
            case REQUESTTYPE:
                return "String";
            case AGREEMENTTITLE:
                return "String";
            case CONTRACTDATE:
                return "java.sql.Date";
            case AGENCY:
                return "String";
            case COLLATERALAGREEMENTFLAG:
                return "String";
            case CSAENFORCEABLEFLAG:
                return "String";
            case TRANSACTIONSPECIFICFLAG:
                return "String";
            case CSENTITYAGENCYFLAG:
                return "String";
            case DESK:
                return "String";
            case ICONID:
                return "String";
            case CSAREGFLAG:
                return "String";
        }
        return null;
    }


}
