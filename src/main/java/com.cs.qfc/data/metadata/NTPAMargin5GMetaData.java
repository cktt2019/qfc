package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class NTPAMargin5GMetaData implements MetaData {

    public static final String TABLE = "60f03b25-e4a6-469d-9c7d-20a4ecf23d3e";
    private static final String BUSINESSDATE = "businessDate";
    private static final String RUNDATE = "runDate";
    private static final String ACCOUNT = "account";
    private static final String CURRID = "currId";
    private static final String LIQUIDATINGEQUITY = "liquidatingEquity";
    private static final String MARGINEQUITY = "marginEquity";
    private static final String REGTREQUIREMENTS = "regTRequirements";
    private static final String MAINTREQUIREMENTS = "maintRequirements";
    private static final String NYSEREQUIREMENTS = "nyseRequirements";
    private static final String REGTEXCESSDEFICIT = "regtExcessDeficit";
    private static final String MAINTEXCESSDEFICIT = "maintExcessDeficit";
    private static final String NYSEEXCESSDEFICIT = "nyseExcessDeficit";
    private static final String SMA = "sma";
    private static final String CASHAVAILABLE = "cashAvailable";
    private static final String TOTALTDBALANCE = "totalTdBalance";
    private static final String TOTALTDLONGMKTVALUE = "totalTdLongMktValue";
    private static final String TOTALTDSHORTMKTVALUE = "totalTdShortMktValue";
    private static final String TOTALOPTIONLONGMKTVALUE = "totalOptionLongMktValue";
    private static final String TOTALOPTIONSHORTMKTVALUE = "totalOptionShortMktValue";
    private static final String TOTALSDBALANCE = "totalSdBalance";
    private static final String TOTALSDLONGMKTVALUE = "totalSdLongMktValue";
    private static final String TOTALSDSHORTMKTVALUE = "totalSdShortMktValue";
    private static final String TOTALASSESSEDBALANCE = "totalAssessedBalance";
    private static final String TOTALINTERESTACCRUAL = "totalInterestAccrual";
    private static final String TYPE = "type";
    private static final String TDBALANCE = "tdBalance";
    private static final String TDMARKETVALLNG = "tdMarketValLng";
    private static final String TDMARKETVALSHRT = "tdMarketValShrt";
    private static final String SDBALANCE = "sdBalance";
    private static final String SDMARKETVALUE = "sdMarketValue";
    private static final String LEGALENTITYID = "legalEntityId";
    private static final String NTPAPRODUCTID = "ntpaProductId";
    private static final String ID = "id";

    @Override
    public String getColumnName(String property) {
        switch (property) {
            case BUSINESSDATE:
                return "business_date";
            case RUNDATE:
                return "run_date";
            case ACCOUNT:
                return "account";
            case CURRID:
                return "curr_id";
            case LIQUIDATINGEQUITY:
                return "liquidating_equity";
            case MARGINEQUITY:
                return "margin_equity";
            case REGTREQUIREMENTS:
                return "reg_t_requirements";
            case MAINTREQUIREMENTS:
                return "maint_requirements";
            case NYSEREQUIREMENTS:
                return "nyse_requirements";
            case REGTEXCESSDEFICIT:
                return "regt_excess_deficit";
            case MAINTEXCESSDEFICIT:
                return "maint_excess_deficit";
            case NYSEEXCESSDEFICIT:
                return "nyse_excess_deficit";
            case SMA:
                return "sma";
            case CASHAVAILABLE:
                return "cash_available";
            case TOTALTDBALANCE:
                return "total_td_balance";
            case TOTALTDLONGMKTVALUE:
                return "total_td_long_mkt_value";
            case TOTALTDSHORTMKTVALUE:
                return "total_td_short_mkt_value";
            case TOTALOPTIONLONGMKTVALUE:
                return "total_option_long_mkt_value";
            case TOTALOPTIONSHORTMKTVALUE:
                return "total_option_short_mkt_value";
            case TOTALSDBALANCE:
                return "total_sd_balance";
            case TOTALSDLONGMKTVALUE:
                return "total_sd_long_mkt_value";
            case TOTALSDSHORTMKTVALUE:
                return "total_sd_short_mkt_value";
            case TOTALASSESSEDBALANCE:
                return "total_assessed_balance";
            case TOTALINTERESTACCRUAL:
                return "total_interest_accrual";
            case TYPE:
                return "type";
            case TDBALANCE:
                return "td_balance";
            case TDMARKETVALLNG:
                return "td_market_val_lng";
            case TDMARKETVALSHRT:
                return "td_market_val_shrt";
            case SDBALANCE:
                return "sd_balance";
            case SDMARKETVALUE:
                return "sd_market_value";
            case LEGALENTITYID:
                return "legal_entity_id";
            case NTPAPRODUCTID:
                return "ntpa_product_id";
            case ID:
                return "id";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (property) {
            case BUSINESSDATE:
                return "java.sql.Date";
            case RUNDATE:
                return "String";
            case ACCOUNT:
                return "String";
            case CURRID:
                return "String";
            case LIQUIDATINGEQUITY:
                return "String";
            case MARGINEQUITY:
                return "String";
            case REGTREQUIREMENTS:
                return "String";
            case MAINTREQUIREMENTS:
                return "String";
            case NYSEREQUIREMENTS:
                return "String";
            case REGTEXCESSDEFICIT:
                return "String";
            case MAINTEXCESSDEFICIT:
                return "String";
            case NYSEEXCESSDEFICIT:
                return "String";
            case SMA:
                return "String";
            case CASHAVAILABLE:
                return "String";
            case TOTALTDBALANCE:
                return "String";
            case TOTALTDLONGMKTVALUE:
                return "String";
            case TOTALTDSHORTMKTVALUE:
                return "String";
            case TOTALOPTIONLONGMKTVALUE:
                return "String";
            case TOTALOPTIONSHORTMKTVALUE:
                return "String";
            case TOTALSDBALANCE:
                return "String";
            case TOTALSDLONGMKTVALUE:
                return "String";
            case TOTALSDSHORTMKTVALUE:
                return "String";
            case TOTALASSESSEDBALANCE:
                return "String";
            case TOTALINTERESTACCRUAL:
                return "String";
            case TYPE:
                return "String";
            case TDBALANCE:
                return "String";
            case TDMARKETVALLNG:
                return "String";
            case TDMARKETVALSHRT:
                return "String";
            case SDBALANCE:
                return "String";
            case SDMARKETVALUE:
                return "String";
            case LEGALENTITYID:
                return "String";
            case NTPAPRODUCTID:
                return "String";
            case ID:
                return "Integer";
        }
        return null;
    }


}
