package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class PositionLevelDataMetaData implements MetaData {

    public static final String TABLE = "PositionLevelData";
    private static final String ASOFDATE = "asOfDate";
    private static final String RECORDSENTITYIDENTIFIER = "recordsEntityIdentifier";
    private static final String POSITIONIDENTIFIER = "positionIdentifier";
    private static final String COUNTERPARTYIDENTIFIER = "counterpartyIdentifier";
    private static final String INTERNALBOOKINGLOCATIONIDENTIFIER = "internalBookingLocationIdentifier";
    private static final String UNIQUEBOOKINGUNITDESKIDENTIFIER = "uniqueBookingUnitDeskIdentifier";
    private static final String TYPEOFQFC = "typeOfQfc";
    private static final String COVEREDBYGUARANTEECREDITENHANCEMENT = "coveredByGuaranteeCreditEnhancement";
    private static final String UNDERLYINGQFCOBLIGATORIDENTIFIER = "underlyingQfcObligatorIdentifier";
    private static final String AGREEMENTIDENTIFIER = "agreementIdentifier";
    private static final String NETTINGAGREEMENTIDENTIFIER = "nettingAgreementIdentifier";
    private static final String TRADEDATE = "tradeDate";
    private static final String TERMINATIONDATE = "terminationDate";
    private static final String NEXTPUTCALLCANCELLATIONDATE = "nextPutCallCancellationDate";
    private static final String NEXTPAYMENTDATE = "nextPaymentDate";
    private static final String LOCALCURRENCYOFPOSITION = "localCurrencyOfPosition";
    private static final String CURRENTMARKETVALUELOCALCURRENCY = "currentMarketValueLocalCurrency";
    private static final String CURRENTMARKETVALUEUSD = "currentMarketValueUsd";
    private static final String ASSETCLASSIFICATION = "assetClassification";
    private static final String NOTIONALPRINCIPALAMOUNTLOCALCURRENCY = "notionalPrincipalAmountLocalCurrency";
    private static final String NOTIONALPRINCIPALAMOUNTUSD = "notionalPrincipalAmountUsd";
    private static final String RECORDENTITYTHIRDPARTYCREDITENHANCEMENTCOVER = "recordEntityThirdPartyCreditEnhancementCover";
    private static final String RETHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER = "reThirdPartyCreditEnhancementProviderIdentifier";
    private static final String RETHIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER = "reThirdPartyCreditEnhancementAgreementIdentifier";
    private static final String RELATEDPOSITIONOFRECORDSENTITY = "relatedPositionOfRecordsEntity";
    private static final String REFERENCENUMBERFORANYRELATEDLOAN = "referenceNumberForAnyRelatedLoan";
    private static final String IDENTIFIEROFTHELENDEROFTHERELATEDLOAN = "identifierOfTheLenderOfTheRelatedLoan";
    private static final String COUNTERPARTYTHIRDPARTYCOVER = "counterPartyThirdPartyCover";
    private static final String COUNTERPARTYTHIRDPARTYENHANCEMENTPROVIDERIDENTIFIER = "counterPartyThirdPartyEnhancementProviderIdentifier";
    private static final String COUNTERPARTYTHIRDPARTYENHANCEMENTAGREEMENTIDENTIFIER = "counterPartyThirdPartyEnhancementAgreementIdentifier";
    private static final String NETTINGAGREEMENTCOUNTERPARTYIDENTIFIER = "nettingAgreementCounterPartyIdentifier";
    private static final String ID = "id";

    @Override
    public String getColumnName(String property) {
        switch (property) {
            case ASOFDATE:
                return "as_of_date";
            case RECORDSENTITYIDENTIFIER:
                return "records_entity_identifier";
            case POSITIONIDENTIFIER:
                return "position_identifier";
            case COUNTERPARTYIDENTIFIER:
                return "counterparty_identifier";
            case INTERNALBOOKINGLOCATIONIDENTIFIER:
                return "internal_booking_location_identifier";
            case UNIQUEBOOKINGUNITDESKIDENTIFIER:
                return "unique_booking_unit_or_desk_identifier";
            case TYPEOFQFC:
                return "type_of_qfc";
            case COVEREDBYGUARANTEECREDITENHANCEMENT:
                return "covered_by_guarantee_or_credit_enhancement";
            case UNDERLYINGQFCOBLIGATORIDENTIFIER:
                return "underlying_qfc_obligator_identifier";
            case AGREEMENTIDENTIFIER:
                return "agreement_identifier";
            case NETTINGAGREEMENTIDENTIFIER:
                return "netting_agreement_identifier";
            case TRADEDATE:
                return "trade_date";
            case TERMINATIONDATE:
                return "termination_date";
            case NEXTPUTCALLCANCELLATIONDATE:
                return "next_put_call_cancellation_date";
            case NEXTPAYMENTDATE:
                return "next_payment_date";
            case LOCALCURRENCYOFPOSITION:
                return "local_currency_of_position";
            case CURRENTMARKETVALUELOCALCURRENCY:
                return "current_market_value_local_currency";
            case CURRENTMARKETVALUEUSD:
                return "current_market_value_usd";
            case ASSETCLASSIFICATION:
                return "asset_classification";
            case NOTIONALPRINCIPALAMOUNTLOCALCURRENCY:
                return "notional_principal_amount_local_currency";
            case NOTIONALPRINCIPALAMOUNTUSD:
                return "notional_principal_amount_usd";
            case RECORDENTITYTHIRDPARTYCREDITENHANCEMENTCOVER:
                return "record_entity_third_party_credit_enhancement_cover";
            case RETHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER:
                return "re_third_party_credit_enhancement_provider_identifier";
            case RETHIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER:
                return "re_third_party_credit_enhancement_agreement_identifier";
            case RELATEDPOSITIONOFRECORDSENTITY:
                return "related_position_of_records_entity";
            case REFERENCENUMBERFORANYRELATEDLOAN:
                return "reference_number_for_any_related_loan";
            case IDENTIFIEROFTHELENDEROFTHERELATEDLOAN:
                return "identifier_of_the_lender_of_the_related_loan";
            case COUNTERPARTYTHIRDPARTYCOVER:
                return "counter_party_third_party_cover";
            case COUNTERPARTYTHIRDPARTYENHANCEMENTPROVIDERIDENTIFIER:
                return "counter_party_third_party_enhancement_provider_identifier";
            case COUNTERPARTYTHIRDPARTYENHANCEMENTAGREEMENTIDENTIFIER:
                return "counter_party_third_party_enhancement_agreement_identifier";
            case NETTINGAGREEMENTCOUNTERPARTYIDENTIFIER:
                return "netting_agreement_counter_party_identifier";
            case ID:
                return "id";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (property) {
            case ASOFDATE:
                return "java.sql.Date";
            case RECORDSENTITYIDENTIFIER:
                return "String";
            case POSITIONIDENTIFIER:
                return "String";
            case COUNTERPARTYIDENTIFIER:
                return "String";
            case INTERNALBOOKINGLOCATIONIDENTIFIER:
                return "String";
            case UNIQUEBOOKINGUNITDESKIDENTIFIER:
                return "String";
            case TYPEOFQFC:
                return "String";
            case COVEREDBYGUARANTEECREDITENHANCEMENT:
                return "String";
            case UNDERLYINGQFCOBLIGATORIDENTIFIER:
                return "String";
            case AGREEMENTIDENTIFIER:
                return "String";
            case NETTINGAGREEMENTIDENTIFIER:
                return "String";
            case TRADEDATE:
                return "java.sql.Date";
            case TERMINATIONDATE:
                return "java.sql.Date";
            case NEXTPUTCALLCANCELLATIONDATE:
                return "java.sql.Date";
            case NEXTPAYMENTDATE:
                return "String";
            case LOCALCURRENCYOFPOSITION:
                return "String";
            case CURRENTMARKETVALUELOCALCURRENCY:
                return "String";
            case CURRENTMARKETVALUEUSD:
                return "String";
            case ASSETCLASSIFICATION:
                return "String";
            case NOTIONALPRINCIPALAMOUNTLOCALCURRENCY:
                return "String";
            case NOTIONALPRINCIPALAMOUNTUSD:
                return "String";
            case RECORDENTITYTHIRDPARTYCREDITENHANCEMENTCOVER:
                return "String";
            case RETHIRDPARTYCREDITENHANCEMENTPROVIDERIDENTIFIER:
                return "String";
            case RETHIRDPARTYCREDITENHANCEMENTAGREEMENTIDENTIFIER:
                return "String";
            case RELATEDPOSITIONOFRECORDSENTITY:
                return "String";
            case REFERENCENUMBERFORANYRELATEDLOAN:
                return "String";
            case IDENTIFIEROFTHELENDEROFTHERELATEDLOAN:
                return "String";
            case COUNTERPARTYTHIRDPARTYCOVER:
                return "String";
            case COUNTERPARTYTHIRDPARTYENHANCEMENTPROVIDERIDENTIFIER:
                return "String";
            case COUNTERPARTYTHIRDPARTYENHANCEMENTAGREEMENTIDENTIFIER:
                return "String";
            case NETTINGAGREEMENTCOUNTERPARTYIDENTIFIER:
                return "String";
            case ID:
                return "Integer";
        }
        return null;
    }


}
