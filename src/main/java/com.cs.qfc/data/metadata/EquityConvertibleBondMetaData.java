package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class EquityConvertibleBondMetaData implements MetaData {

    public static final String TABLE = "EQUITYCONVERTIBLEBOND";
    private static final String CUSIPID = "cusipId";
    private static final String ISINID = "isinId";
    private static final String NEXTCALLDATE = "nextCallDate";
    private static final String NEXTPUTDATE = "nextPutDate";
    private static final String NXTPAYDATE = "nxtPayDate";
    private static final String SECURITYDESCRIPTION = "securityDescription";
    private static final String A3C7IND = "a3c7Ind";
    private static final String BBYKET = "bbYKet";
    private static final String COMPEXCHCODEDER = "compExchCodeDer";
    private static final String CONVPRICE = "convPrice";
    private static final String CONRATIO = "conRatio";
    private static final String CSMANAGED = "csManaged";
    private static final String EQUITYFLOAT = "equityFloat";
    private static final String EXCHANGE = "exchange";
    private static final String FIDESACODE = "fidesaCode";
    private static final String FUNDOBJECTIVE = "fundObjective";
    private static final String GLOSSG5 = "glossG5";
    private static final String IDBBGLOBAL = "idBbGlobal";
    private static final String IDBBUNIQUE = "idBbUnique";
    private static final String ITALYID = "italyId";
    private static final String JAPANID = "japanId";
    private static final String RICID = "ricId";
    private static final String SINGID = "singId";
    private static final String VALORENID = "valorenId";
    private static final String WERTPID = "wertpId";
    private static final String HUGOID = "hugoId";
    private static final String NTPALSEQYPODT = "ntpaLsEqyPoDt";
    private static final String LONDONIMSDESCRIPTION = "londonImsDescription";
    private static final String MATURITYDATE = "maturityDate";
    private static final String NASDAQREPORTCODEDER = "nasdaqReportCodeDer";
    private static final String PRIMARYRIC = "primaryRic";
    private static final String QTYPERUNDLYSHR = "qtyPerUndlyShr";
    private static final String UNDLYPERADR = "undlyPerAdr";
    private static final String REGS = "regs";
    private static final String REGIONPRIM = "regionPrim";
    private static final String RTRTICKETSYMBOL = "rtrTicketSymbol";
    private static final String TRADECNTRY = "tradeCntry";
    private static final String UNDLYSECISIN = "undlySecIsin";
    private static final String VOTINGRIGHTPERSHARE = "votingRightPerShare";
    private static final String CMUID = "cmuId";
    private static final String ULTPRNTTICKETEXCHANGE = "ultPrntTicketExchange";
    private static final String POETSID = "poetsId";
    private static final String NTPASOI = "ntpaSoi";
    private static final String CPN = "cpn";
    private static final String SSRLIQUIDITYINDICATOR = "ssrLiquidityIndicator";
    private static final String REEXCHANGE = "reExchange";
    private static final String GLOSSK3 = "glossK3";
    private static final String SECTYPE = "secType";
    private static final String FUNDEURODIRECTUCITS = "fundEuroDirectUcits";
    private static final String FUNDASSETCLASSFOCUS = "fundAssetClassFocus";
    private static final String BBCOMPANYID = "bbCompanyId";
    private static final String SPISSUEID = "spIssueId";
    private static final String FUNDCLOSEDNEWINV = "fundClosedNewInv";
    private static final String LEGALNAME = "legalName";
    private static final String TRADINGLOTSIZE = "tradingLotSize";
    private static final String READRUNDLYRIC = "reAdrUndlyRic";
    private static final String RELATIVEINDEX = "relativeIndex";
    private static final String PRIMTRAD = "primTrad";
    private static final String CUMULATIVEFLAG = "cumulativeFlag";
    private static final String FUNDCUSTODIANNAME = "fundCustodianName";
    private static final String INITIALPUBOFFER = "initialPubOffer";
    private static final String GICSSECTORISR = "gicsSectorIsr";
    private static final String GICSSECTORNAMEISR = "gicsSectorNameIsr";
    private static final String GICSINDUSTRYGROUPISR = "gicsIndustryGroupIsr";
    private static final String GICSINDUSTRYGROUPNAMEISR = "gicsIndustryGroupNameIsr";
    private static final String GICSINDUSTRYSR = "gicsIndustrySr";
    private static final String GICSINDUSTRYNAMEISR = "gicsIndustryNameIsr";
    private static final String NAICSIDISR = "naicsIdIsr";
    private static final String NTPACOUNTRYOFSETTLEMENT = "ntpaCountryOfSettlement";
    private static final String GLIRP = "gliRp";
    private static final String CDDSLASTUPDATEDTIME = "cddsLastUpdatedTime";
    private static final String PUBLICLYTRADED = "publiclyTraded";
    private static final String CSSETTLEDAYS = "csSettleDays";
    private static final String BBUNDERLYINGSECURITY = "bbUnderlyingSecurity";
    private static final String BBUNDERLYINGTYPECODE = "bbUnderlyingTypeCode";
    private static final String TICKSIZEPILOTGROUP = "tickSizePilotGroup";
    private static final String CALLEDDATE = "calledDate";
    private static final String CALLEDPRICE = "calledPrice";
    private static final String COUPONTYPE = "couponType";
    private static final String RECLASSSCHEME = "reClassScheme";
    private static final String LEGALENTITYIDENTIFIER = "legalEntityIdentifier";
    private static final String COMPANYCORPTICKER = "companyCorpTicker";
    private static final String COMPANYTOPARENT = "companyToParent";
    private static final String BBISSUERTYPE = "bbIssuerType";
    private static final String FISECISSUERID = "fisecIssuerId";
    private static final String ULTPRNTCOMPID = "ultPrntCompId";
    private static final String ISARCHIVED = "isArchived";
    private static final String LOCALEXCHANGESYMBOL = "localExchangeSymbol";
    private static final String CFICODE = "cfiCode";
    private static final String FISN = "fisn";
    private static final String ISSDATE = "issDate";
    private static final String FIRSTSETTLEMENTDATE = "firstSettlementDate";
    private static final String WHENISSUEDFLAG = "whenIssuedFlag";
    private static final String MINORTRADINGCURRENCYIND = "minorTradingCurrencyInd";
    private static final String RISKALERT = "riskAlert";
    private static final String MDSID = "mdsId";
    private static final String FUNDGEOGRAPHICFOCUS = "fundGeographicFocus";
    private static final String FUNDLEVERAGEAMOUNT = "fundLeverageAmount";
    private static final String CMSCODE = "cmsCode";
    private static final String IDSPI = "idSpi";
    private static final String STATICLISTINGID = "staticListingId";
    private static final String CLIENTTYPE = "clientType";
    private static final String MARKETINGDESK = "marketingDesk";
    private static final String EXTERNALID = "externalId";
    private static final String ISSUEBOOK = "issueBook";
    private static final String DUTCHID = "dutchId";
    private static final String BELGIUMID = "belgiumId";
    private static final String ISTARID = "istarId";
    private static final String IDBBSECURITY = "idBbSecurity";
    private static final String OPERATINGMIC = "operatingMic";
    private static final String BBINDUSTRYSECTORCD = "bbIndustrySectorCd";
    private static final String BBINDUSTRYGROUPCD = "bbIndustryGroupCd";
    private static final String BBINDUSTRYSUBGROUPCD = "bbIndustrySubGroupCd";
    private static final String CDDSRECCREATIONTIME = "cddsRecCreationTime";
    private static final String LISTEDEXCHIND = "listedExchInd";
    private static final String IDMALAYSIAN = "idMalaysian";
    private static final String REDEMPTIONPRICE = "redemptionPrice";
    private static final String SERIES = "series";
    private static final String CPNFREQ = "cpnFreq";
    private static final String GMSOI = "gmSoi";
    private static final String CSPVTPLACEMENTIND = "csPvtPlacementInd";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case CUSIPID:
                return "cusip_id";
            case ISINID:
                return "isin_id";
            case NEXTCALLDATE:
                return "next_call_date";
            case NEXTPUTDATE:
                return "next_put_date";
            case NXTPAYDATE:
                return "nxt_pay_date";
            case SECURITYDESCRIPTION:
                return "security_description";
            case A3C7IND:
                return "a_3c7_ind";
            case BBYKET:
                return "bb_y_ket";
            case COMPEXCHCODEDER:
                return "comp_exch_code_der";
            case CONVPRICE:
                return "conv_price";
            case CONRATIO:
                return "con_ratio";
            case CSMANAGED:
                return "cs_managed";
            case EQUITYFLOAT:
                return "equity_float";
            case EXCHANGE:
                return "exchange";
            case FIDESACODE:
                return "fidesa_code";
            case FUNDOBJECTIVE:
                return "fund_objective";
            case GLOSSG5:
                return "gloss_g5";
            case IDBBGLOBAL:
                return "id_bb_global";
            case IDBBUNIQUE:
                return "id_bb_unique";
            case ITALYID:
                return "italy_id";
            case JAPANID:
                return "japan_id";
            case RICID:
                return "ric_id";
            case SINGID:
                return "sing_id";
            case VALORENID:
                return "valoren_id";
            case WERTPID:
                return "wertp_id";
            case HUGOID:
                return "hugo_id";
            case NTPALSEQYPODT:
                return "ntpa_ls_eqy_po_dt";
            case LONDONIMSDESCRIPTION:
                return "london_ims_description";
            case MATURITYDATE:
                return "maturity_date";
            case NASDAQREPORTCODEDER:
                return "nasdaq_report_code_der";
            case PRIMARYRIC:
                return "primary_ric";
            case QTYPERUNDLYSHR:
                return "qty_per_undly_shr";
            case UNDLYPERADR:
                return "undly_per_adr";
            case REGS:
                return "regs";
            case REGIONPRIM:
                return "region_prim";
            case RTRTICKETSYMBOL:
                return "rtr_ticket_symbol";
            case TRADECNTRY:
                return "trade_cntry";
            case UNDLYSECISIN:
                return "undly_sec_isin";
            case VOTINGRIGHTPERSHARE:
                return "voting_right_per_share";
            case CMUID:
                return "cmu_id";
            case ULTPRNTTICKETEXCHANGE:
                return "ult_prnt_ticket_exchange";
            case POETSID:
                return "poets_id";
            case NTPASOI:
                return "ntpa_soi";
            case CPN:
                return "cpn";
            case SSRLIQUIDITYINDICATOR:
                return "ssr_liquidity_indicator";
            case REEXCHANGE:
                return "re_exchange";
            case GLOSSK3:
                return "gloss_k3";
            case SECTYPE:
                return "sec_type";
            case FUNDEURODIRECTUCITS:
                return "fund_euro_direct_ucits";
            case FUNDASSETCLASSFOCUS:
                return "fund_asset_class_focus";
            case BBCOMPANYID:
                return "bb_company_id";
            case SPISSUEID:
                return "sp_issue_id";
            case FUNDCLOSEDNEWINV:
                return "fund_closed_new_inv";
            case LEGALNAME:
                return "legal_name";
            case TRADINGLOTSIZE:
                return "trading_lot_size";
            case READRUNDLYRIC:
                return "re_adr_undly_ric";
            case RELATIVEINDEX:
                return "relative_index";
            case PRIMTRAD:
                return "prim_trad";
            case CUMULATIVEFLAG:
                return "cumulative_flag";
            case FUNDCUSTODIANNAME:
                return "fund_custodian_name";
            case INITIALPUBOFFER:
                return "initial_pub_offer";
            case GICSSECTORISR:
                return "gics_sector_isr";
            case GICSSECTORNAMEISR:
                return "gics_sector_name_isr";
            case GICSINDUSTRYGROUPISR:
                return "gics_industry_group_isr";
            case GICSINDUSTRYGROUPNAMEISR:
                return "gics_industry_group_name_isr";
            case GICSINDUSTRYSR:
                return "gics_industry_sr";
            case GICSINDUSTRYNAMEISR:
                return "gics_industry_name_isr";
            case NAICSIDISR:
                return "naics_id_isr";
            case NTPACOUNTRYOFSETTLEMENT:
                return "ntpa_country_of_settlement";
            case GLIRP:
                return "gli_rp";
            case CDDSLASTUPDATEDTIME:
                return "cdds_last_updated_time";
            case PUBLICLYTRADED:
                return "publicly_traded";
            case CSSETTLEDAYS:
                return "cs_settle_days";
            case BBUNDERLYINGSECURITY:
                return "bb_underlying_security";
            case BBUNDERLYINGTYPECODE:
                return "bb_underlying_type_code";
            case TICKSIZEPILOTGROUP:
                return "tick_size_pilot_group";
            case CALLEDDATE:
                return "called_date";
            case CALLEDPRICE:
                return "called_price";
            case COUPONTYPE:
                return "coupon_type";
            case RECLASSSCHEME:
                return "re_class_scheme";
            case LEGALENTITYIDENTIFIER:
                return "legal_entity_identifier";
            case COMPANYCORPTICKER:
                return "company_corp_ticker";
            case COMPANYTOPARENT:
                return "company_to_parent";
            case BBISSUERTYPE:
                return "bb_issuer_type";
            case FISECISSUERID:
                return "fisec_issuer_id";
            case ULTPRNTCOMPID:
                return "ult_prnt_comp_id";
            case ISARCHIVED:
                return "is_archived";
            case LOCALEXCHANGESYMBOL:
                return "local_exchange_symbol";
            case CFICODE:
                return "cfi_code";
            case FISN:
                return "fisn";
            case ISSDATE:
                return "iss_date";
            case FIRSTSETTLEMENTDATE:
                return "first_settlement_date";
            case WHENISSUEDFLAG:
                return "when_issued_flag";
            case MINORTRADINGCURRENCYIND:
                return "minor_trading_currency_ind";
            case RISKALERT:
                return "risk_alert";
            case MDSID:
                return "mds_id";
            case FUNDGEOGRAPHICFOCUS:
                return "fund_geographic_focus";
            case FUNDLEVERAGEAMOUNT:
                return "fund_leverage_amount";
            case CMSCODE:
                return "cms_code";
            case IDSPI:
                return "id_spi";
            case STATICLISTINGID:
                return "static_listing_id";
            case CLIENTTYPE:
                return "client_type";
            case MARKETINGDESK:
                return "marketing_desk";
            case EXTERNALID:
                return "external_id";
            case ISSUEBOOK:
                return "issue_book";
            case DUTCHID:
                return "dutch_id";
            case BELGIUMID:
                return "belgium_id";
            case ISTARID:
                return "istar_id";
            case IDBBSECURITY:
                return "id_bb_security";
            case OPERATINGMIC:
                return "operating_mic";
            case BBINDUSTRYSECTORCD:
                return "bb_industry_sector_cd";
            case BBINDUSTRYGROUPCD:
                return "bb_industry_group_cd";
            case BBINDUSTRYSUBGROUPCD:
                return "bb_industry_sub_group_cd";
            case CDDSRECCREATIONTIME:
                return "cdds_rec_creation_time";
            case LISTEDEXCHIND:
                return "listed_exch_ind";
            case IDMALAYSIAN:
                return "id_malaysian";
            case REDEMPTIONPRICE:
                return "redemption_price";
            case SERIES:
                return "series";
            case CPNFREQ:
                return "cpn_freq";
            case GMSOI:
                return "gm_soi";
            case CSPVTPLACEMENTIND:
                return "cs_pvt_placement_ind";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case CUSIPID:
                return "String";
            case ISINID:
                return "String";
            case NEXTCALLDATE:
                return "java.sql.Date";
            case NEXTPUTDATE:
                return "String";
            case NXTPAYDATE:
                return "java.sql.Date";
            case SECURITYDESCRIPTION:
                return "String";
            case A3C7IND:
                return "String";
            case BBYKET:
                return "String";
            case COMPEXCHCODEDER:
                return "String";
            case CONVPRICE:
                return "Float";
            case CONRATIO:
                return "Float";
            case CSMANAGED:
                return "String";
            case EQUITYFLOAT:
                return "Float";
            case EXCHANGE:
                return "String";
            case FIDESACODE:
                return "String";
            case FUNDOBJECTIVE:
                return "String";
            case GLOSSG5:
                return "String";
            case IDBBGLOBAL:
                return "String";
            case IDBBUNIQUE:
                return "String";
            case ITALYID:
                return "String";
            case JAPANID:
                return "String";
            case RICID:
                return "String";
            case SINGID:
                return "String";
            case VALORENID:
                return "String";
            case WERTPID:
                return "String";
            case HUGOID:
                return "String";
            case NTPALSEQYPODT:
                return "String";
            case LONDONIMSDESCRIPTION:
                return "String";
            case MATURITYDATE:
                return "java.sql.Date";
            case NASDAQREPORTCODEDER:
                return "String";
            case PRIMARYRIC:
                return "String";
            case QTYPERUNDLYSHR:
                return "Float";
            case UNDLYPERADR:
                return "String";
            case REGS:
                return "String";
            case REGIONPRIM:
                return "String";
            case RTRTICKETSYMBOL:
                return "String";
            case TRADECNTRY:
                return "String";
            case UNDLYSECISIN:
                return "String";
            case VOTINGRIGHTPERSHARE:
                return "Float";
            case CMUID:
                return "String";
            case ULTPRNTTICKETEXCHANGE:
                return "String";
            case POETSID:
                return "String";
            case NTPASOI:
                return "String";
            case CPN:
                return "Float";
            case SSRLIQUIDITYINDICATOR:
                return "String";
            case REEXCHANGE:
                return "String";
            case GLOSSK3:
                return "String";
            case SECTYPE:
                return "String";
            case FUNDEURODIRECTUCITS:
                return "String";
            case FUNDASSETCLASSFOCUS:
                return "String";
            case BBCOMPANYID:
                return "String";
            case SPISSUEID:
                return "String";
            case FUNDCLOSEDNEWINV:
                return "String";
            case LEGALNAME:
                return "String";
            case TRADINGLOTSIZE:
                return "Float";
            case READRUNDLYRIC:
                return "String";
            case RELATIVEINDEX:
                return "String";
            case PRIMTRAD:
                return "String";
            case CUMULATIVEFLAG:
                return "String";
            case FUNDCUSTODIANNAME:
                return "String";
            case INITIALPUBOFFER:
                return "java.sql.Date";
            case GICSSECTORISR:
                return "String";
            case GICSSECTORNAMEISR:
                return "String";
            case GICSINDUSTRYGROUPISR:
                return "String";
            case GICSINDUSTRYGROUPNAMEISR:
                return "String";
            case GICSINDUSTRYSR:
                return "String";
            case GICSINDUSTRYNAMEISR:
                return "String";
            case NAICSIDISR:
                return "String";
            case NTPACOUNTRYOFSETTLEMENT:
                return "String";
            case GLIRP:
                return "String";
            case CDDSLASTUPDATEDTIME:
                return "String";
            case PUBLICLYTRADED:
                return "String";
            case CSSETTLEDAYS:
                return "String";
            case BBUNDERLYINGSECURITY:
                return "String";
            case BBUNDERLYINGTYPECODE:
                return "String";
            case TICKSIZEPILOTGROUP:
                return "String";
            case CALLEDDATE:
                return "java.sql.Date";
            case CALLEDPRICE:
                return "Float";
            case COUPONTYPE:
                return "String";
            case RECLASSSCHEME:
                return "String";
            case LEGALENTITYIDENTIFIER:
                return "String";
            case COMPANYCORPTICKER:
                return "String";
            case COMPANYTOPARENT:
                return "String";
            case BBISSUERTYPE:
                return "String";
            case FISECISSUERID:
                return "String";
            case ULTPRNTCOMPID:
                return "String";
            case ISARCHIVED:
                return "Integer";
            case LOCALEXCHANGESYMBOL:
                return "String";
            case CFICODE:
                return "String";
            case FISN:
                return "String";
            case ISSDATE:
                return "java.sql.Date";
            case FIRSTSETTLEMENTDATE:
                return "java.sql.Date";
            case WHENISSUEDFLAG:
                return "String";
            case MINORTRADINGCURRENCYIND:
                return "String";
            case RISKALERT:
                return "String";
            case MDSID:
                return "String";
            case FUNDGEOGRAPHICFOCUS:
                return "String";
            case FUNDLEVERAGEAMOUNT:
                return "String";
            case CMSCODE:
                return "String";
            case IDSPI:
                return "String";
            case STATICLISTINGID:
                return "String";
            case CLIENTTYPE:
                return "String";
            case MARKETINGDESK:
                return "String";
            case EXTERNALID:
                return "String";
            case ISSUEBOOK:
                return "String";
            case DUTCHID:
                return "String";
            case BELGIUMID:
                return "String";
            case ISTARID:
                return "String";
            case IDBBSECURITY:
                return "Float";
            case OPERATINGMIC:
                return "String";
            case BBINDUSTRYSECTORCD:
                return "Integer";
            case BBINDUSTRYGROUPCD:
                return "Integer";
            case BBINDUSTRYSUBGROUPCD:
                return "Integer";
            case CDDSRECCREATIONTIME:
                return "java.sql.Timestamp";
            case LISTEDEXCHIND:
                return "String";
            case IDMALAYSIAN:
                return "String";
            case REDEMPTIONPRICE:
                return "Float";
            case SERIES:
                return "String";
            case CPNFREQ:
                return "String";
            case GMSOI:
                return "String";
            case CSPVTPLACEMENTIND:
                return "String";
        }
        return null;
    }


}
