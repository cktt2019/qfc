package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class LoanIQMetaData implements MetaData {

    public static final String TABLE = "LOANIQ";
    private static final String ID = "id";
    private static final String CPTYCSID = "cptyCsid";
    private static final String SDSID = "sdsId";
    private static final String PORTFOLIOCODE = "portfolioCode";
    private static final String AGREEMENTID = "agreementId";
    private static final String TRADEDATE = "tradeDate";
    private static final String MATURITYDATE = "maturityDate";
    private static final String CURRENCY = "currency";
    private static final String PRESENTVALUE = "presentValue";
    private static final String TRADEAMOUNT = "tradeAmount";
    private static final String RECORDTYPE = "recordType";
    private static final String SWAPID = "swapId";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "id";
            case CPTYCSID:
                return "cpty_csid";
            case SDSID:
                return "sds_id";
            case PORTFOLIOCODE:
                return "portfolio_code";
            case AGREEMENTID:
                return "agreement_id";
            case TRADEDATE:
                return "trade_date";
            case MATURITYDATE:
                return "maturity_date";
            case CURRENCY:
                return "currency";
            case PRESENTVALUE:
                return "present_value";
            case TRADEAMOUNT:
                return "trade_amount";
            case RECORDTYPE:
                return "record_type";
            case SWAPID:
                return "swap_id";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (property) {
            case ID:
                return "Integer";
            case CPTYCSID:
                return "String";
            case SDSID:
                return "String";
            case PORTFOLIOCODE:
                return "String";
            case AGREEMENTID:
                return "String";
            case TRADEDATE:
                return "java.sql.Date";
            case MATURITYDATE:
                return "String";
            case CURRENCY:
                return "String";
            case PRESENTVALUE:
                return "Float";
            case TRADEAMOUNT:
                return "Float";
            case RECORDTYPE:
                return "String";
            case SWAPID:
                return "String";
        }
        return "String";
    }


}
