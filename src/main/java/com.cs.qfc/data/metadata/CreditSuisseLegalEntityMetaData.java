package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class CreditSuisseLegalEntityMetaData implements MetaData {

    public static final String TABLE = "CREDITSUISSELEGALENTITY";
    private static final String COUNT = "count";
    private static final String ENTERPRISEDNUMBER = "enterpriseDNumber";
    private static final String BOOKINGENTITYNM = "bookingEntityNm";
    private static final String PEOPLESOFTID = "peopleSoftId";
    private static final String CSIDID = "csidId";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case COUNT:
                return "Count";
            case ENTERPRISEDNUMBER:
                return "enterprise_d_number";
            case BOOKINGENTITYNM:
                return "booking_entity_nm";
            case PEOPLESOFTID:
                return "people_soft_id";
            case CSIDID:
                return "csid_id";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property + "a")) {
            case COUNT:
                return "Integer";
            case ENTERPRISEDNUMBER:
                return "Integer";
            case BOOKINGENTITYNM:
                return "String";
            case PEOPLESOFTID:
                return "String";
            case CSIDID:
                return "String";
        }
        return "String";
    }


}
