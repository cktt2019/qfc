package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class DrdTradeUnderlyingMetaData implements MetaData {

    public static final String TABLE = "DRDTRADEUNDERLYING";
    private static final String ID = "gpiIssue";
    private static final String COBDATE = "cobDate";
    private static final String LOADCOBDATE = "loadCobDate";
    private static final String LOADBOOK = "loadBook";
    private static final String BOOKNAME = "bookName";
    private static final String TRADELOADID = "tradeLoadId";
    private static final String LOADTIMESTAMP = "loadTimestamp";
    private static final String SNAPSHOTNAME = "snapshotName";
    private static final String SNAPSHOTID = "snapshotId";
    private static final String SNAPSHOTCREATIONDATETIME = "snapshotCreationDatetime";
    private static final String INTERFACEVERSION = "interfaceVersion";
    private static final String TRADEID = "tradeId";
    private static final String TFTTRADEVERSION = "tftTradeVersion";
    private static final String TRADELOCATIONID = "tradeLocationId";
    private static final String PRIMARYTCN = "primaryTcn";
    private static final String TCN = "tcn";
    private static final String TRADEVERSIONUPDATETIME = "tradeVersionUpdateTime";
    private static final String CANCELLATIONDATE = "cancellationDate";
    private static final String GBMENTITY = "gbmEntity";
    private static final String APPROVALSTATUS = "approvalStatus";
    private static final String EVENTREASON = "eventReason";
    private static final String EXTERNALID = "externalId";
    private static final String PRIMARYMATURITYDATE = "primaryMaturityDate";
    private static final String PRODUCTSUBTYPE = "productSubType";
    private static final String PRODUCTTYPE = "productType";
    private static final String PRODUCTVALUATIONTYPE = "productValuationType";
    private static final String TRADESTATUS = "tradeStatus";
    private static final String UNDERLYINGID = "underlyingId";
    private static final String UNDERLYINGIDTYPE = "underlyingIdType";
    private static final String ACCOUNTCCY = "accountCcy";
    private static final String BASKETSTRIKE = "basketStrike";
    private static final String BASKETWEIGHTING = "basketWeighting";
    private static final String COUPON = "coupon";
    private static final String COUPONFREQ = "couponFreq";
    private static final String FINALPRICE = "finalPrice";
    private static final String FLOATINGRATEINDEX = "floatingRateIndex";
    private static final String FLOATINGRATEINDEXTENOR = "floatingRateIndexTenor";
    private static final String FLOATINGRATEINDEXTYPE = "floatingRateIndexType";
    private static final String INITIALPRICE = "initialPrice";
    private static final String REFOBLIGCATEGORY = "refObligCategory";
    private static final String REFPOOLLONGNAME = "refPoolLongName";
    private static final String REFPOOLSHORTNAME = "refPoolShortName";
    private static final String SEQUENCEID = "sequenceId";
    private static final String UNDERLYINGASSETCLASS = "underlyingAssetClass";
    private static final String UNDERLYINGCCY = "underlyingCcy";
    private static final String UNDERLYINGDESC = "underlyingDesc";
    private static final String UNDERLYINGEXTID = "underlyingExtId";
    private static final String UNDERLYINGEXTIDTYPE = "underlyingExtIdType";
    private static final String UNDERLYINGMATDATE = "underlyingMatDate";
    private static final String UNDERLYINGRISKID = "underlyingRiskId";
    private static final String PAYREC = "payRec";
    private static final String SETTLEMENTTYPE = "settlementType";
    private static final String BUSINESSGROUP = "businessGroup";
    private static final String VALUATIONCLASS = "valuationClass";
    private static final String VALUATIONTYPE = "valuationType";
    private static final String TRADESOURCESYSTEM = "tradeSourceSystem";
    private static final String UNDERLYINGPRICESOURCE = "underlyingPriceSource";
    private static final String CDUMASTERAGREEMENTLLK = "cduMasterAgreementLlk";
    private static final String CDUCOLLATERALANNEXLLK = "cduCollateralAnnexLlk";
    private static final String FRAMESOFTMASTERAGREEMENTLLK = "framesoftMasterAgreementLlk";
    private static final String ALGOCOLLATERALANNEXLLK = "algoCollateralAnnexLlk";
    private static final String GBMBOOKREF = "gbmBookRef";
    private static final String UNDERLYINGPRODUCTID = "underlyingProductId";
    private static final String UNDERLYINGPRODUCTTYPE = "underlyingProductType";
    private static final String UNDERLYINGRISKTYPE = "underlyingRiskType";
    private static final String BANKRUPTCY = "bankruptcy";
    private static final String DELIVOBLIGACCELERATEDMATURED = "delivObligAcceleratedMatured";
    private static final String DELIVOBLIGACCRUEDINTEREST = "delivObligAccruedInterest";
    private static final String DELIVOBLIGASSIGNABLELOAN = "delivObligAssignableLoan";
    private static final String DELIVOBLIGCATEGORY = "delivObligCategory";
    private static final String DELIVOBLIGCONSENTREQUIREDLOAN = "delivObligConsentRequiredLoan";
    private static final String DELIVOBLGDIRECTLOANPRTCPTN = "delivOblgDirectLoanPrtCptn";
    private static final String DELIVOBLIGLISTED = "delivObligListed";
    private static final String DELIVOBLIGMAXIMUMMATURITY = "delivObligMaximumMaturity";
    private static final String DELIVOBLIGMAXMATURITYSPECIFIED = "delivObligMaxMaturitySpecified";
    private static final String DELIVOBLIGNOTBEARER = "delivObligNotBearer";
    private static final String DELIVOBLIGNOTCONTINGENT = "delivObligNotContingent";
    private static final String DELIVOBLIGNOTDOMESTICCURRENCY = "delivObligNotDomesticCurrency";
    private static final String DELIVOBLIGNOTDOMESTICISSUANCE = "delivObligNotDomesticIssuance";
    private static final String DELIVOBLIGNOTDOMESTICLAW = "delivObligNotDomesticLaw";
    private static final String DELIVOBLIGNOTSOVEREIGNLENDER = "delivObligNotSovereignLender";
    private static final String DELIVOBLIGPHYPARIPASSU = "delivObligPhyParipassu";
    private static final String DELIVOBLIGSPECIFIEDCCYLIST = "delivObligSpecifiedCcyList";
    private static final String DELIVOBLIGSPECIFIEDCCYTYPE = "delivObligSpecifiedCcyType";
    private static final String DELIVOBLIGTRANSFERABLE = "delivObligTransferable";
    private static final String ENTITYTYPE = "entityType";
    private static final String PRIMARYOBLIGOR = "primaryObligor";
    private static final String RECOVERYVALUE = "recoveryValue";
    private static final String REFERENCEENTITYREDCODE = "referenceEntityRedCode";
    private static final String REFERENCEENTITYUNIQUEENTITYID = "referenceEntityUniqueEntityId";
    private static final String REFERENCEOBLIGATIONBLOOMBERGID = "referenceObligationBloombergId";
    private static final String REFERENCEOBLIGATIONCUSIP = "referenceObligationCusip";
    private static final String REFERENCEOBLIGATIONGUARANTOR = "referenceObligationGuarantor";
    private static final String REFERENCEOBLIGATIONISIN = "referenceObligationIsin";
    private static final String REFERENCEPRICE = "referencePrice";
    private static final String REFOBLIGORCOUPONRATE = "refObligorCouponRate";
    private static final String REFOBLIGORMATURITYDATE = "refObligorMaturityDate";
    private static final String REFOBLIGORNOTIONALAMT = "refObligorNotionalAmt";
    private static final String REFOBLIGORNOTIONALAMTCCY = "refObligorNotionalAmtCcy";
    private static final String REFOBLIGOROBLIGATIONCATEGORY = "refObligorObligationCategory";
    private static final String VALUNWIND = "valUnwind";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "id";
            case COBDATE:
                return "cob_date";
            case LOADCOBDATE:
                return "load_cob_date";
            case LOADBOOK:
                return "load_book";
            case BOOKNAME:
                return "book_name";
            case TRADELOADID:
                return "trade_load_id";
            case LOADTIMESTAMP:
                return "load_timestamp";
            case SNAPSHOTNAME:
                return "snapshot_name";
            case SNAPSHOTID:
                return "snapshot_id";
            case SNAPSHOTCREATIONDATETIME:
                return "snapshot_creation_datetime";
            case INTERFACEVERSION:
                return "interface_version";
            case TRADEID:
                return "trade_id";
            case TFTTRADEVERSION:
                return "tft_trade_version";
            case TRADELOCATIONID:
                return "trade_location_id";
            case PRIMARYTCN:
                return "primary_tcn";
            case TCN:
                return "tcn";
            case TRADEVERSIONUPDATETIME:
                return "trade_version_update_time";
            case CANCELLATIONDATE:
                return "cancellation_date";
            case GBMENTITY:
                return "gbm_entity";
            case APPROVALSTATUS:
                return "approval_status";
            case EVENTREASON:
                return "event_reason";
            case EXTERNALID:
                return "external_id";
            case PRIMARYMATURITYDATE:
                return "primary_maturity_date";
            case PRODUCTSUBTYPE:
                return "product_sub_type";
            case PRODUCTTYPE:
                return "product_type";
            case PRODUCTVALUATIONTYPE:
                return "product_valuation_type";
            case TRADESTATUS:
                return "trade_status";
            case UNDERLYINGID:
                return "underlying_id";
            case UNDERLYINGIDTYPE:
                return "underlying_id_type";
            case ACCOUNTCCY:
                return "account_ccy";
            case BASKETSTRIKE:
                return "basket_strike";
            case BASKETWEIGHTING:
                return "basket_weighting";
            case COUPON:
                return "coupon";
            case COUPONFREQ:
                return "coupon_freq";
            case FINALPRICE:
                return "final_price";
            case FLOATINGRATEINDEX:
                return "floating_rate_index";
            case FLOATINGRATEINDEXTENOR:
                return "floating_rate_index_tenor";
            case FLOATINGRATEINDEXTYPE:
                return "floating_rate_index_type";
            case INITIALPRICE:
                return "initial_price";
            case REFOBLIGCATEGORY:
                return "ref_oblig_category";
            case REFPOOLLONGNAME:
                return "ref_pool_long_name";
            case REFPOOLSHORTNAME:
                return "ref_pool_short_name";
            case SEQUENCEID:
                return "sequence_id";
            case UNDERLYINGASSETCLASS:
                return "underlying_asset_class";
            case UNDERLYINGCCY:
                return "underlying_ccy";
            case UNDERLYINGDESC:
                return "underlying_desc";
            case UNDERLYINGEXTID:
                return "underlying_ext_id";
            case UNDERLYINGEXTIDTYPE:
                return "underlying_ext_id_type";
            case UNDERLYINGMATDATE:
                return "underlying_mat_date";
            case UNDERLYINGRISKID:
                return "underlying_risk_id";
            case PAYREC:
                return "pay_rec";
            case SETTLEMENTTYPE:
                return "settlement_type";
            case BUSINESSGROUP:
                return "business_group";
            case VALUATIONCLASS:
                return "valuation_class";
            case VALUATIONTYPE:
                return "valuation_type";
            case TRADESOURCESYSTEM:
                return "trade_source_system";
            case UNDERLYINGPRICESOURCE:
                return "underlying_price_source";
            case CDUMASTERAGREEMENTLLK:
                return "cdu_master_agreement_llk";
            case CDUCOLLATERALANNEXLLK:
                return "cdu_collateral_annex_llk";
            case FRAMESOFTMASTERAGREEMENTLLK:
                return "framesoft_master_agreement_llk";
            case ALGOCOLLATERALANNEXLLK:
                return "algo_collateral_annex_llk";
            case GBMBOOKREF:
                return "gbm_book_ref";
            case UNDERLYINGPRODUCTID:
                return "underlying_product_id";
            case UNDERLYINGPRODUCTTYPE:
                return "underlying_product_type";
            case UNDERLYINGRISKTYPE:
                return "underlying_risk_type";
            case BANKRUPTCY:
                return "bankruptcy";
            case DELIVOBLIGACCELERATEDMATURED:
                return "deliv_oblig_accelerated_or_matured";
            case DELIVOBLIGACCRUEDINTEREST:
                return "deliv_oblig_accrued_interest";
            case DELIVOBLIGASSIGNABLELOAN:
                return "deliv_oblig_assignable_loan";
            case DELIVOBLIGCATEGORY:
                return "deliv_oblig_category";
            case DELIVOBLIGCONSENTREQUIREDLOAN:
                return "deliv_oblig_consent_required_loan";
            case DELIVOBLGDIRECTLOANPRTCPTN:
                return "deliv_oblg_direct_loan_prt_cptn";
            case DELIVOBLIGLISTED:
                return "deliv_oblig_listed";
            case DELIVOBLIGMAXIMUMMATURITY:
                return "deliv_oblig_maximum_maturity";
            case DELIVOBLIGMAXMATURITYSPECIFIED:
                return "deliv_oblig_max_maturity_specified";
            case DELIVOBLIGNOTBEARER:
                return "deliv_oblig_not_bearer";
            case DELIVOBLIGNOTCONTINGENT:
                return "deliv_oblig_not_contingent";
            case DELIVOBLIGNOTDOMESTICCURRENCY:
                return "deliv_oblig_not_domestic_currency";
            case DELIVOBLIGNOTDOMESTICISSUANCE:
                return "deliv_oblig_not_domestic_issuance";
            case DELIVOBLIGNOTDOMESTICLAW:
                return "deliv_oblig_not_domestic_law";
            case DELIVOBLIGNOTSOVEREIGNLENDER:
                return "deliv_oblig_not_sovereign_lender";
            case DELIVOBLIGPHYPARIPASSU:
                return "deliv_oblig_phy_paripassu";
            case DELIVOBLIGSPECIFIEDCCYLIST:
                return "deliv_oblig_specified_ccy_list";
            case DELIVOBLIGSPECIFIEDCCYTYPE:
                return "deliv_oblig_specified_ccy_type";
            case DELIVOBLIGTRANSFERABLE:
                return "deliv_oblig_transferable";
            case ENTITYTYPE:
                return "entity_type";
            case PRIMARYOBLIGOR:
                return "primary_obligor";
            case RECOVERYVALUE:
                return "recovery_value";
            case REFERENCEENTITYREDCODE:
                return "reference_entity_red_code";
            case REFERENCEENTITYUNIQUEENTITYID:
                return "reference_entity_unique_entity_id";
            case REFERENCEOBLIGATIONBLOOMBERGID:
                return "reference_obligation_bloomberg_id";
            case REFERENCEOBLIGATIONCUSIP:
                return "reference_obligation_cusip";
            case REFERENCEOBLIGATIONGUARANTOR:
                return "reference_obligation_guarantor";
            case REFERENCEOBLIGATIONISIN:
                return "reference_obligation_isin";
            case REFERENCEPRICE:
                return "reference_price";
            case REFOBLIGORCOUPONRATE:
                return "ref_obligor_coupon_rate";
            case REFOBLIGORMATURITYDATE:
                return "ref_obligor_maturity_date";
            case REFOBLIGORNOTIONALAMT:
                return "ref_obligor_notional_amt";
            case REFOBLIGORNOTIONALAMTCCY:
                return "ref_obligor_notional_amt_ccy";
            case REFOBLIGOROBLIGATIONCATEGORY:
                return "ref_obligor_obligation_category";
            case VALUNWIND:
                return "val_unwind";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "Integer";
            case COBDATE:
                return "java.sql.Date";
            case LOADCOBDATE:
                return "java.sql.Date";
            case LOADBOOK:
                return "String";
            case BOOKNAME:
                return "String";
            case TRADELOADID:
                return "Integer";
            case LOADTIMESTAMP:
                return "Date";
            case SNAPSHOTNAME:
                return "String";
            case SNAPSHOTID:
                return "Integer";
            case SNAPSHOTCREATIONDATETIME:
                return "Date";
            case INTERFACEVERSION:
                return "String";
            case TRADEID:
                return "Integer";
            case TFTTRADEVERSION:
                return "Integer";
            case TRADELOCATIONID:
                return "Integer";
            case PRIMARYTCN:
                return "String";
            case TCN:
                return "String";
            case TRADEVERSIONUPDATETIME:
                return "Date";
            case CANCELLATIONDATE:
                return "String";
            case GBMENTITY:
                return "String";
            case APPROVALSTATUS:
                return "String";
            case EVENTREASON:
                return "String";
            case EXTERNALID:
                return "String";
            case PRIMARYMATURITYDATE:
                return "java.sql.Date";
            case PRODUCTSUBTYPE:
                return "String";
            case PRODUCTTYPE:
                return "String";
            case PRODUCTVALUATIONTYPE:
                return "String";
            case TRADESTATUS:
                return "String";
            case UNDERLYINGID:
                return "String";
            case UNDERLYINGIDTYPE:
                return "String";
            case ACCOUNTCCY:
                return "String";
            case BASKETSTRIKE:
                return "Float";
            case BASKETWEIGHTING:
                return "String";
            case COUPON:
                return "Float";
            case COUPONFREQ:
                return "String";
            case FINALPRICE:
                return "Float";
            case FLOATINGRATEINDEX:
                return "String";
            case FLOATINGRATEINDEXTENOR:
                return "String";
            case FLOATINGRATEINDEXTYPE:
                return "String";
            case INITIALPRICE:
                return "Float";
            case REFOBLIGCATEGORY:
                return "String";
            case REFPOOLLONGNAME:
                return "String";
            case REFPOOLSHORTNAME:
                return "String";
            case SEQUENCEID:
                return "Integer";
            case UNDERLYINGASSETCLASS:
                return "String";
            case UNDERLYINGCCY:
                return "String";
            case UNDERLYINGDESC:
                return "String";
            case UNDERLYINGEXTID:
                return "String";
            case UNDERLYINGEXTIDTYPE:
                return "String";
            case UNDERLYINGMATDATE:
                return "java.sql.Date";
            case UNDERLYINGRISKID:
                return "String";
            case PAYREC:
                return "String";
            case SETTLEMENTTYPE:
                return "String";
            case BUSINESSGROUP:
                return "String";
            case VALUATIONCLASS:
                return "String";
            case VALUATIONTYPE:
                return "String";
            case TRADESOURCESYSTEM:
                return "String";
            case UNDERLYINGPRICESOURCE:
                return "String";
            case CDUMASTERAGREEMENTLLK:
                return "String";
            case CDUCOLLATERALANNEXLLK:
                return "String";
            case FRAMESOFTMASTERAGREEMENTLLK:
                return "Integer";
            case ALGOCOLLATERALANNEXLLK:
                return "Integer";
            case GBMBOOKREF:
                return "String";
            case UNDERLYINGPRODUCTID:
                return "String";
            case UNDERLYINGPRODUCTTYPE:
                return "String";
            case UNDERLYINGRISKTYPE:
                return "String";
            case BANKRUPTCY:
                return "String";
            case DELIVOBLIGACCELERATEDMATURED:
                return "String";
            case DELIVOBLIGACCRUEDINTEREST:
                return "String";
            case DELIVOBLIGASSIGNABLELOAN:
                return "String";
            case DELIVOBLIGCATEGORY:
                return "String";
            case DELIVOBLIGCONSENTREQUIREDLOAN:
                return "String";
            case DELIVOBLGDIRECTLOANPRTCPTN:
                return "String";
            case DELIVOBLIGLISTED:
                return "String";
            case DELIVOBLIGMAXIMUMMATURITY:
                return "Integer";
            case DELIVOBLIGMAXMATURITYSPECIFIED:
                return "String";
            case DELIVOBLIGNOTBEARER:
                return "String";
            case DELIVOBLIGNOTCONTINGENT:
                return "String";
            case DELIVOBLIGNOTDOMESTICCURRENCY:
                return "String";
            case DELIVOBLIGNOTDOMESTICISSUANCE:
                return "String";
            case DELIVOBLIGNOTDOMESTICLAW:
                return "String";
            case DELIVOBLIGNOTSOVEREIGNLENDER:
                return "String";
            case DELIVOBLIGPHYPARIPASSU:
                return "String";
            case DELIVOBLIGSPECIFIEDCCYLIST:
                return "String";
            case DELIVOBLIGSPECIFIEDCCYTYPE:
                return "String";
            case DELIVOBLIGTRANSFERABLE:
                return "String";
            case ENTITYTYPE:
                return "String";
            case PRIMARYOBLIGOR:
                return "String";
            case RECOVERYVALUE:
                return "Float";
            case REFERENCEENTITYREDCODE:
                return "String";
            case REFERENCEENTITYUNIQUEENTITYID:
                return "String";
            case REFERENCEOBLIGATIONBLOOMBERGID:
                return "String";
            case REFERENCEOBLIGATIONCUSIP:
                return "String";
            case REFERENCEOBLIGATIONGUARANTOR:
                return "String";
            case REFERENCEOBLIGATIONISIN:
                return "String";
            case REFERENCEPRICE:
                return "Float";
            case REFOBLIGORCOUPONRATE:
                return "String";
            case REFOBLIGORMATURITYDATE:
                return "java.sql.Date";
            case REFOBLIGORNOTIONALAMT:
                return "Float";
            case REFOBLIGORNOTIONALAMTCCY:
                return "String";
            case REFOBLIGOROBLIGATIONCATEGORY:
                return "String";
            case VALUNWIND:
                return "Float";
        }
        return null;
    }


}
