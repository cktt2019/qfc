package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class PortfolioMetaData implements MetaData {

    public static final String TABLE = "PORTFOLIO";
    private static final String PORTFOLIOID = "portfolioId";
    private static final String GLOBALPARTYID = "globalPartyId";
    private static final String PARTYTYPEDESCRIPTION = "partytypeDescription";
    private static final String LEGALNAME = "legalName";
    private static final String PARTYALIASNAME = "partyAliasName";
    private static final String LEGALROLEDEFINITIONID = "legalRoleDefinitionId";
    private static final String LRDROLETYPE = "lrdRoleType";
    private static final String LRDROLEDESCRIPTION = "lrdRoleDescription";
    private static final String LRDROLECODE = "lrdRoleCode";
    private static final String PARTYLEGALROLEID = "partyLegalRoleId";
    private static final String SOURCEID = "sourceId";
    private static final String SOURCENAME = "sourceName";
    private static final String INTERNALPARTYALTID = "internalPartyAltId";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case PORTFOLIOID:
                return "portfolio_id";
            case GLOBALPARTYID:
                return "global_party_id";
            case PARTYTYPEDESCRIPTION:
                return "partytype_description";
            case LEGALNAME:
                return "legal_name";
            case PARTYALIASNAME:
                return "party_alias_name";
            case LEGALROLEDEFINITIONID:
                return "legal_role_definition_id";
            case LRDROLETYPE:
                return "lrd_role_type";
            case LRDROLEDESCRIPTION:
                return "lrd_role_description";
            case LRDROLECODE:
                return "lrd_role_code";
            case PARTYLEGALROLEID:
                return "party_legal_role_id";
            case SOURCEID:
                return "source_id";
            case SOURCENAME:
                return "source_name";
            case INTERNALPARTYALTID:
                return "internal_party_alt_id";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case PORTFOLIOID:
                return "String";
            case GLOBALPARTYID:
                return "String";
            case PARTYTYPEDESCRIPTION:
                return "String";
            case LEGALNAME:
                return "String";
            case PARTYALIASNAME:
                return "String";
            case LEGALROLEDEFINITIONID:
                return "String";
            case LRDROLETYPE:
                return "String";
            case LRDROLEDESCRIPTION:
                return "String";
            case LRDROLECODE:
                return "String";
            case PARTYLEGALROLEID:
                return "String";
            case SOURCEID:
                return "String";
            case SOURCENAME:
                return "String";
            case INTERNALPARTYALTID:
                return "String";
        }
        return null;
    }


}
