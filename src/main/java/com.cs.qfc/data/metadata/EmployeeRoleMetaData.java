package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class EmployeeRoleMetaData implements MetaData {

    public static final String TABLE = "EMPLOYEEROLE";
    private static final String EMPLOYEEID = "employeeId";
    private static final String EMPLOYEEACTIVEINDICATOR = "employeeActiveIndicator";
    private static final String EMPLOYEEEFFECTIVEDATE = "employeeEffectiveDate";
    private static final String EMPLOYEEEMAILID = "employeeEmailId";
    private static final String EMPLOYEEFIRSTNAME = "employeeFirstName";
    private static final String EMPLOYEELASTNAME = "employeeLastName";
    private static final String EMPLOYEELASTUPDATEDBY = "employeeLastUpdatedBy";
    private static final String EMPLOYEELASTUPDATEDDATETIME = "employeeLastUpdatedDatetime";
    private static final String EMPLOYEELOGONDOMAIN = "employeeLogonDomain";
    private static final String EMPLOYEELOGONID = "employeeLogonId";
    private static final String EMPLOYEEPIDID = "employeePidId";
    private static final String CITY = "city";
    private static final String REGION = "region";
    private static final String COUNTRYLONGDESC = "countryLongDesc";
    private static final String DELTAFLAG = "deltaFlag";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case EMPLOYEEID:
                return "employee_id";
            case EMPLOYEEACTIVEINDICATOR:
                return "employee_active_indicator";
            case EMPLOYEEEFFECTIVEDATE:
                return "employee_effective_date";
            case EMPLOYEEEMAILID:
                return "employee_email_id";
            case EMPLOYEEFIRSTNAME:
                return "employee_first_name";
            case EMPLOYEELASTNAME:
                return "employee_last_name";
            case EMPLOYEELASTUPDATEDBY:
                return "employee_last_updated_by";
            case EMPLOYEELASTUPDATEDDATETIME:
                return "employee_last_updated_datetime";
            case EMPLOYEELOGONDOMAIN:
                return "employee_logon_domain";
            case EMPLOYEELOGONID:
                return "employee_logon_id";
            case EMPLOYEEPIDID:
                return "employee_pid_id";
            case CITY:
                return "city";
            case REGION:
                return "region";
            case COUNTRYLONGDESC:
                return "country_long_desc";
            case DELTAFLAG:
                return "delta_flag";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case EMPLOYEEID:
                return "String";
            case EMPLOYEEACTIVEINDICATOR:
                return "String";
            case EMPLOYEEEFFECTIVEDATE:
                return "java.sql.Date";
            case EMPLOYEEEMAILID:
                return "String";
            case EMPLOYEEFIRSTNAME:
                return "String";
            case EMPLOYEELASTNAME:
                return "String";
            case EMPLOYEELASTUPDATEDBY:
                return "String";
            case EMPLOYEELASTUPDATEDDATETIME:
                return "String";
            case EMPLOYEELOGONDOMAIN:
                return "String";
            case EMPLOYEELOGONID:
                return "String";
            case EMPLOYEEPIDID:
                return "String";
            case CITY:
                return "String";
            case REGION:
                return "String";
            case COUNTRYLONGDESC:
                return "String";
            case DELTAFLAG:
                return "String";
        }
        return null;
    }


}
