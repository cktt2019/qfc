package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class DrdTradeInstrumentMetaData implements MetaData {

    public static final String TABLE = "DRDTRADEINSTRUMENT";
    private static final String ID = "id";
    private static final String COBDATE = "cobDate";
    private static final String GBMENTITY = "gbmEntity";
    private static final String TRADEID = "tradeId";
    private static final String COUNTERPARTYID = "counterPartyId";
    private static final String BOOKNAME = "bookName";
    private static final String ASSETCLASS = "assetClass";
    private static final String CDUMASTERAGREEMENTLLK = "cduMasterAgreementLlk";
    private static final String CDUCOLLATERALANNEXLLK = "cduCollateralAnnexLlk";
    private static final String FRAMESOFTMASTERAGREEMENTLLK = "framesoftMasterAgreementLlk";
    private static final String ALGOCOLLATERALANNEXLLK = "algoCollateralAnnexLlk";
    private static final String TRADEDATE = "tradeDate";
    private static final String PRIMARYMATURITYDATE = "primaryMaturityDate";
    private static final String LOCALPVCCYCODE = "localPvCcyCode";
    private static final String LOCALPV = "localPv";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "id";
            case COBDATE:
                return "cob_date";
            case GBMENTITY:
                return "gbm_entity";
            case TRADEID:
                return "trade_id";
            case COUNTERPARTYID:
                return "counter_party_id";
            case BOOKNAME:
                return "book_name";
            case ASSETCLASS:
                return "asset_class";
            case CDUMASTERAGREEMENTLLK:
                return "cdu_master_agreement_llk";
            case CDUCOLLATERALANNEXLLK:
                return "cdu_collateral_annex_llk";
            case FRAMESOFTMASTERAGREEMENTLLK:
                return "framesoft_master_agreement_llk";
            case ALGOCOLLATERALANNEXLLK:
                return "algo_collateral_annex_llk";
            case TRADEDATE:
                return "trade_date";
            case PRIMARYMATURITYDATE:
                return "primary_maturity_date";
            case LOCALPVCCYCODE:
                return "local_pv_ccy_code";
            case LOCALPV:
                return "local_pv";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case ID:
                return "Integer";
            case COBDATE:
                return "java.sql.Date";
            case GBMENTITY:
                return "String";
            case TRADEID:
                return "Integer";
            case COUNTERPARTYID:
                return "String";
            case BOOKNAME:
                return "String";
            case ASSETCLASS:
                return "String";
            case CDUMASTERAGREEMENTLLK:
                return "String";
            case CDUCOLLATERALANNEXLLK:
                return "String";
            case FRAMESOFTMASTERAGREEMENTLLK:
                return "String";
            case ALGOCOLLATERALANNEXLLK:
                return "String";
            case TRADEDATE:
                return "java.sql.Date";
            case PRIMARYMATURITYDATE:
                return "java.sql.Date";
            case LOCALPVCCYCODE:
                return "String";
            case LOCALPV:
                return "Integer";
        }
        return null;
    }


}
