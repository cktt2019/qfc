package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class DTPMISGBLDYTREEMetaData implements MetaData {

    public static final String TABLE = "DTPMISGBLDYTREE";
    private static final String MISMEMBERCODE = "misMemberCode";
    private static final String AMERICASPRODUCTLINEHEADID = "americasProductLineHeadId";
    private static final String AMERICASCLUSTERHEADID = "americasClusterHeadId";
    private static final String APACCLUSTERHEADID = "apacClusterHeadId";
    private static final String APACPRODUCTLINEHEADID = "apacProductLineHeadId";
    private static final String BUSINESSCONTROLLERL2ID = "businessControllerL2Id";
    private static final String CLUSTER = "cluster";
    private static final String CLUSTERCODE = "clusterCode";
    private static final String CLUSTERHEADID = "clusterHeadId";
    private static final String DESK = "desk";
    private static final String DESKCODE = "deskCode";
    private static final String DIVISION = "division";
    private static final String DIVISIONCODE = "divisionCode";
    private static final String EUROCLUSTERHEADID = "euroClusterHeadId";
    private static final String EUROPRODUCTLINEHEADID = "euroProductLineHeadId";
    private static final String GLOBALPRODUCTLINEHEADID = "globalProductLineHeadId";
    private static final String HIERARCHYTRADERID = "hierarchyTraderId";
    private static final String MISHIERARCHYSORTCODE = "misHierarchySortCode";
    private static final String MISMEMBERNAME = "misMemberName";
    private static final String MISMEMBERTYPECODE = "misMemberTypeCode";
    private static final String MISPRODUCT = "misProduct";
    private static final String MISREGIONCODE = "misRegionCode";
    private static final String MISTREECODE = "misTreeCode";
    private static final String PARENTMISMEMBERCODE = "parentMisMemberCode";
    private static final String PRODUCTCONTROLSECTIONMGRID = "productControlSectionMgrId";
    private static final String PRODUCTLINECODE = "productLineCode";
    private static final String PRODUCTLINENAME = "productLineName";
    private static final String SUB3PRODUCTLINE = "sub3ProductLine";
    private static final String SUB3PRODUCTLINEDESCRIPTION = "sub3ProductLineDescription";
    private static final String SUBCLUSTERCODE = "subClusterCode";
    private static final String SUBCLUSTERNAME = "subClusterName";
    private static final String SUBDIVISIONCODE = "subDivisionCode";
    private static final String SUBDIVISIONDESCRIPTION = "subDivisionDescription";
    private static final String SUBPRODUCTDESCRIPTION = "subProductDescription";
    private static final String SUBPRODUCTLINE = "subProductLine";
    private static final String SWISSPRODUCTLINEHEADID = "swissProductLineHeadId";
    private static final String TRADINGSUPERVISORID = "tradingSupervisorId";
    private static final String TRADINGSUPERVISORDELEGATEID = "tradingSupervisorDelegateId";
    private static final String MISPRODUCTCODE = "misProductCode";
    private static final String MISUNITPERCENTOWNED = "misUnitPercentOwned";
    private static final String MISUNITPERCENTDESCRIPTION = "misUnitPercentDescription";
    private static final String DELTAFLAG = "deltaFlag";
    private static final String CDDSTREENODESTATUS = "cddsTreeNodeStatus";
    private static final String TRADINGSUPPORT = "tradingSupport";
    private static final String TRADESUPPORTMANAGEREMAIL = "tradeSupportManagerEmail";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case MISMEMBERCODE:
                return "mis_member_code";
            case AMERICASPRODUCTLINEHEADID:
                return "americas_product_line_head_id";
            case AMERICASCLUSTERHEADID:
                return "americas_cluster_head_id";
            case APACCLUSTERHEADID:
                return "apac_cluster_head_id";
            case APACPRODUCTLINEHEADID:
                return "apac_product_line_head_id";
            case BUSINESSCONTROLLERL2ID:
                return "business_controller_l2_id";
            case CLUSTER:
                return "cluster";
            case CLUSTERCODE:
                return "cluster_code";
            case CLUSTERHEADID:
                return "cluster_head_id";
            case DESK:
                return "desk";
            case DESKCODE:
                return "desk_code";
            case DIVISION:
                return "division";
            case DIVISIONCODE:
                return "division_code";
            case EUROCLUSTERHEADID:
                return "euro_cluster_head_id";
            case EUROPRODUCTLINEHEADID:
                return "euro_product_line_head_id";
            case GLOBALPRODUCTLINEHEADID:
                return "global_product_line_head_id";
            case HIERARCHYTRADERID:
                return "hierarchy_trader_id";
            case MISHIERARCHYSORTCODE:
                return "mis_hierarchy_sort_code";
            case MISMEMBERNAME:
                return "mis_member_name";
            case MISMEMBERTYPECODE:
                return "mis_member_type_code";
            case MISPRODUCT:
                return "mis_product";
            case MISREGIONCODE:
                return "mis_region_code";
            case MISTREECODE:
                return "mis_tree_code";
            case PARENTMISMEMBERCODE:
                return "parent_mis_member_code";
            case PRODUCTCONTROLSECTIONMGRID:
                return "product_control_section_mgr_id";
            case PRODUCTLINECODE:
                return "product_line_code";
            case PRODUCTLINENAME:
                return "product_line_name";
            case SUB3PRODUCTLINE:
                return "sub_3_product_line";
            case SUB3PRODUCTLINEDESCRIPTION:
                return "sub_3_product_line_description";
            case SUBCLUSTERCODE:
                return "sub_cluster_code";
            case SUBCLUSTERNAME:
                return "sub_cluster_name";
            case SUBDIVISIONCODE:
                return "sub_division_code";
            case SUBDIVISIONDESCRIPTION:
                return "sub_division_description";
            case SUBPRODUCTDESCRIPTION:
                return "sub_product_description";
            case SUBPRODUCTLINE:
                return "sub_product_line";
            case SWISSPRODUCTLINEHEADID:
                return "swiss_product_line_head_id";
            case TRADINGSUPERVISORID:
                return "trading_supervisor_id";
            case TRADINGSUPERVISORDELEGATEID:
                return "trading_supervisor_delegate_id";
            case MISPRODUCTCODE:
                return "mis_product_code";
            case MISUNITPERCENTOWNED:
                return "mis_unit_percent_owned";
            case MISUNITPERCENTDESCRIPTION:
                return "mis_unit_percent_description";
            case DELTAFLAG:
                return "delta_flag";
            case CDDSTREENODESTATUS:
                return "cdds_tree_node_status";
            case TRADINGSUPPORT:
                return "trading_support";
            case TRADESUPPORTMANAGEREMAIL:
                return "trade_support_manager_email";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case MISMEMBERCODE:
                return "String";
            case AMERICASPRODUCTLINEHEADID:
                return "String";
            case AMERICASCLUSTERHEADID:
                return "String";
            case APACCLUSTERHEADID:
                return "String";
            case APACPRODUCTLINEHEADID:
                return "String";
            case BUSINESSCONTROLLERL2ID:
                return "String";
            case CLUSTER:
                return "String";
            case CLUSTERCODE:
                return "String";
            case CLUSTERHEADID:
                return "String";
            case DESK:
                return "String";
            case DESKCODE:
                return "String";
            case DIVISION:
                return "String";
            case DIVISIONCODE:
                return "String";
            case EUROCLUSTERHEADID:
                return "String";
            case EUROPRODUCTLINEHEADID:
                return "String";
            case GLOBALPRODUCTLINEHEADID:
                return "String";
            case HIERARCHYTRADERID:
                return "String";
            case MISHIERARCHYSORTCODE:
                return "String";
            case MISMEMBERNAME:
                return "String";
            case MISMEMBERTYPECODE:
                return "String";
            case MISPRODUCT:
                return "String";
            case MISREGIONCODE:
                return "String";
            case MISTREECODE:
                return "String";
            case PARENTMISMEMBERCODE:
                return "String";
            case PRODUCTCONTROLSECTIONMGRID:
                return "String";
            case PRODUCTLINECODE:
                return "String";
            case PRODUCTLINENAME:
                return "String";
            case SUB3PRODUCTLINE:
                return "String";
            case SUB3PRODUCTLINEDESCRIPTION:
                return "String";
            case SUBCLUSTERCODE:
                return "String";
            case SUBCLUSTERNAME:
                return "String";
            case SUBDIVISIONCODE:
                return "String";
            case SUBDIVISIONDESCRIPTION:
                return "String";
            case SUBPRODUCTDESCRIPTION:
                return "String";
            case SUBPRODUCTLINE:
                return "String";
            case SWISSPRODUCTLINEHEADID:
                return "String";
            case TRADINGSUPERVISORID:
                return "String";
            case TRADINGSUPERVISORDELEGATEID:
                return "String";
            case MISPRODUCTCODE:
                return "String";
            case MISUNITPERCENTOWNED:
                return "String";
            case MISUNITPERCENTDESCRIPTION:
                return "String";
            case DELTAFLAG:
                return "String";
            case CDDSTREENODESTATUS:
                return "String";
            case TRADINGSUPPORT:
                return "String";
            case TRADESUPPORTMANAGEREMAIL:
                return "String";
        }
        return null;
    }


}
