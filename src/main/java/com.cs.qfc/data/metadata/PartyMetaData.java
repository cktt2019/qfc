package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class PartyMetaData implements MetaData {

    public static final String TABLE = "PARTY";
    private static final String GLOBALPARTYID = "globalPartyId";
    private static final String CSID = "csid";
    private static final String CSIDSOURCE = "csidSource";
    private static final String GSID = "gsid";
    private static final String GSIDSOURCE = "gsidSource";
    private static final String COUNTRYOFDOMICILEISOCOUNTRYCODE = "countryOfDomicileIsoCountryCode";
    private static final String COUNTRYOFINCORPORATIONISOCOUNTRYCODE = "countryOfIncorporationIsoCountryCode";
    private static final String PARTYLEGALNAME = "partyLegalName";
    private static final String ADDRESSTYPECODE = "addressTypeCode";
    private static final String ADDRESSADDRESSLINE1 = "addressAddressLine1";
    private static final String ADDRESSADDRESSLINE2 = "addressAddressLine2";
    private static final String ADDRESSADDRESSLINE3 = "addressAddressLine3";
    private static final String ADDRESSADDRESSLINE4 = "addressAddressLine4";
    private static final String ADDRESSPOSTALCODE = "addressPostalCode";
    private static final String ADDRESSCOUNTRYISOCOUNTRYNAME = "addressCountryIsoCountryName";
    private static final String ADDRESSADDRESSFULLPHONENUMBER = "addressAddressFullPhoneNumber";
    private static final String ADDRESSELCTRONICADDRESSID = "addressElctronicAddressId";
    private static final String PARTYLEGALROLEDEFINITIONID = "partyLegalRoleDefinitionId";
    private static final String PARTYLEGALROLEID = "partyLegalRoleId";
    private static final String INTERNALPARTYIDALTSOURCENAME = "internalPartyIdAltSourceName";
    private static final String PARTYLEGALROLEDESCRIPTION = "partyLegalRoleDescription";
    private static final String PARTYLEGALSTATUSTYPE = "partyLegalStatusType";
    private static final String PARTYLEGALSTATUSCODE = "partyLegalStatusCode";
    private static final String ADDRESSTYPEDESCRIPTION = "addressTypeDescription";
    private static final String ADDRESSCITYNAME = "addressCityName";
    private static final String ADDRESSPOBOXCITY = "addressPoBoxCity";
    private static final String EXTERNALPARTYALTIDSOURCEID = "externalPartyAltIdSourceId";
    private static final String EXTERNALPARTYALTIDSOURCENAME = "externalPartyAltIdSourceName";
    private static final String EXTERNALPARTYALTIDSOURCEIDENTIFICATIONNUMBER = "externalPartyAltIdSourceIdentificationNumber";

    @Override
    public String getColumnName(String property) {
        switch (snakeToCamel(property)) {
            case GLOBALPARTYID:
                return "global_party_id";
            case CSID:
                return "csid";
            case CSIDSOURCE:
                return "csid_source";
            case GSID:
                return "gsid";
            case GSIDSOURCE:
                return "gsid_source";
            case COUNTRYOFDOMICILEISOCOUNTRYCODE:
                return "country_of_domicile_iso_country_code";
            case COUNTRYOFINCORPORATIONISOCOUNTRYCODE:
                return "country_of_incorporation_iso_country_code";
            case PARTYLEGALNAME:
                return "party_legal_name";
            case ADDRESSTYPECODE:
                return "address_type_code";
            case ADDRESSADDRESSLINE1:
                return "address_address_line1";
            case ADDRESSADDRESSLINE2:
                return "address_address_line2";
            case ADDRESSADDRESSLINE3:
                return "address_address_line3";
            case ADDRESSADDRESSLINE4:
                return "address_address_line4";
            case ADDRESSPOSTALCODE:
                return "address_postal_code";
            case ADDRESSCOUNTRYISOCOUNTRYNAME:
                return "address_country_iso_country_name";
            case ADDRESSADDRESSFULLPHONENUMBER:
                return "address_address_full_phone_number";
            case ADDRESSELCTRONICADDRESSID:
                return "address_elctronic_address_id";
            case PARTYLEGALROLEDEFINITIONID:
                return "party_legal_role_definition_id";
            case PARTYLEGALROLEID:
                return "party_legal_role_id";
            case INTERNALPARTYIDALTSOURCENAME:
                return "internal_party_id_alt_source_name";
            case PARTYLEGALROLEDESCRIPTION:
                return "party_legal_role_description";
            case PARTYLEGALSTATUSTYPE:
                return "party_legal_status_type";
            case PARTYLEGALSTATUSCODE:
                return "party_legal_status_code";
            case ADDRESSTYPEDESCRIPTION:
                return "address_type_description";
            case ADDRESSCITYNAME:
                return "address_city_name";
            case ADDRESSPOBOXCITY:
                return "address_po_box_city";
            case EXTERNALPARTYALTIDSOURCEID:
                return "external_party_alt_id_source_id";
            case EXTERNALPARTYALTIDSOURCENAME:
                return "external_party_alt_id_source_name";
            case EXTERNALPARTYALTIDSOURCEIDENTIFICATIONNUMBER:
                return "external_party_alt_id_source_identification_number";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (snakeToCamel(property)) {
            case GLOBALPARTYID:
                return "Long";
            case CSID:
                return "String";
            case CSIDSOURCE:
                return "String";
            case GSID:
                return "String";
            case GSIDSOURCE:
                return "String";
            case COUNTRYOFDOMICILEISOCOUNTRYCODE:
                return "String";
            case COUNTRYOFINCORPORATIONISOCOUNTRYCODE:
                return "String";
            case PARTYLEGALNAME:
                return "String";
            case ADDRESSTYPECODE:
                return "String";
            case ADDRESSADDRESSLINE1:
                return "String";
            case ADDRESSADDRESSLINE2:
                return "String";
            case ADDRESSADDRESSLINE3:
                return "String";
            case ADDRESSADDRESSLINE4:
                return "String";
            case ADDRESSPOSTALCODE:
                return "String";
            case ADDRESSCOUNTRYISOCOUNTRYNAME:
                return "String";
            case ADDRESSADDRESSFULLPHONENUMBER:
                return "String";
            case ADDRESSELCTRONICADDRESSID:
                return "String";
            case PARTYLEGALROLEDEFINITIONID:
                return "String";
            case PARTYLEGALROLEID:
                return "String";
            case INTERNALPARTYIDALTSOURCENAME:
                return "String";
            case PARTYLEGALROLEDESCRIPTION:
                return "String";
            case PARTYLEGALSTATUSTYPE:
                return "String";
            case PARTYLEGALSTATUSCODE:
                return "String";
            case ADDRESSTYPEDESCRIPTION:
                return "String";
            case ADDRESSCITYNAME:
                return "String";
            case ADDRESSPOBOXCITY:
                return "String";
            case EXTERNALPARTYALTIDSOURCEID:
                return "String";
            case EXTERNALPARTYALTIDSOURCENAME:
                return "String";
            case EXTERNALPARTYALTIDSOURCEIDENTIFICATIONNUMBER:
                return "String";
        }
        return null;
    }


}
