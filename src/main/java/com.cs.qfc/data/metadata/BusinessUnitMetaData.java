package com.cs.qfc.data.metadata;


/**
 * @author ctanyitang
 */


public class BusinessUnitMetaData implements MetaData {

    public static final String TABLE = "BusinessUnit";
    private static final String FINANCIALBUSINESSUNITCODE = "financialBusinessUnitCode";
    private static final String BASECURRENCY = "baseCurrency";
    private static final String BUSINESSUNITACTIVEINDICATOR = "businessUnitActiveIndicator";
    private static final String BUSINESSUNITDESCRIPTION = "businessUnitDescription";
    private static final String BUSINESSUNITEFFECTIVEDATE = "businessUnitEffectiveDate";
    private static final String BUSINESSUNITGERMANDESCRIPTION = "businessUnitGermanDescription";
    private static final String BUSINESSUNITGERMANNAME = "businessUnitGermanName";
    private static final String BUSINESSUNITID = "businessUnitId";
    private static final String BUSINESSUNITLASTUPDATEDATETIME = "businessUnitLastUpdateDatetime";
    private static final String BUSINESSUNITLASTUPDATEDBY = "businessUnitLastUpdatedBy";
    private static final String BUSINESSUNITNAME = "businessUnitName";
    private static final String BUSINESSUNITSHORTDESCRIPTION = "businessUnitShortDescription";
    private static final String BUSINESSUNITSHORTGERMANDESCRIPTION = "businessUnitShortGermanDescription";
    private static final String CHBUSINESSUNITLASTUPDATEDBY = "chBusinessUnitLastUpdatedBy";
    private static final String CHBUSINESSUNITLASTUPDATEDDATETIME = "chBusinessUnitLastUpdatedDatetime";
    private static final String CUSTOMERID = "customerId";
    private static final String CUSTOMERVENDORAFFILIATEINDICATOR = "customerVendorAffiliateIndicator";
    private static final String FINANCIALBUSINESSUNITACTIVEINDICATOR = "financialBusinessUnitActiveIndicator";
    private static final String FINANCIALBUSINESSUNITEFFECTIVEDATE = "financialBusinessUnitEffectiveDate";
    private static final String FINANCIALBUSINESSUNITGROUPID = "financialBusinessUnitGroupId";
    private static final String FINANCIALBUSINESSUNITGROUPNAME = "financialBusinessUnitGroupName";
    private static final String FINANCIALBUSINESSUNITNAME = "financialBusinessUnitName";
    private static final String FIRMTYPE = "firmType";
    private static final String ISOCURRENCYCODE = "isoCurrencyCode";
    private static final String LCDREFNO = "lcdRefNo";
    private static final String LEGALENTITYSHORTDESCRIPTION = "legalEntityShortDescription";
    private static final String LEGALENTITYACTIVEINDICATOR = "legalEntityActiveIndicator";
    private static final String LEGALENTITYCODE = "legalEntityCode";
    private static final String LEGALENTITYDESCRIPTION = "legalEntityDescription";
    private static final String LEGALENTITYEFFECTIVEDATE = "legalEntityEffectiveDate";
    private static final String LEGALENTITYGERMANDESCRIPTION = "legalEntityGermanDescription";
    private static final String LEGALENTITYLASTUPDATEDBY = "legalEntityLastUpdatedBy";
    private static final String LEGALENTITYLASTUPDATEDDATETIME = "legalEntityLastUpdatedDatetime";
    private static final String LOCALGAAPCURRENCY = "localGaapCurrency";
    private static final String MISMEMBERCODE = "misMemberCode";
    private static final String PROCESSINGLOCATIONCODE = "processingLocationCode";
    private static final String SOURCEID = "sourceId";
    private static final String DELTAFLAG = "deltaFlag";
    private static final String BUDOMICILE = "buDomicile";

    @Override
    public String getColumnName(String property) {
        switch (property) {
            case FINANCIALBUSINESSUNITCODE:
                return "financial_business_unit_code";
            case "financial_business_unit_code":
                return "financial_business_unit_code";
            case BASECURRENCY:
                return "base_currency";
            case BUSINESSUNITACTIVEINDICATOR:
                return "business_unit_active_indicator";
            case BUSINESSUNITDESCRIPTION:
                return "business_unit_description";
            case BUSINESSUNITEFFECTIVEDATE:
                return "business_unit_effective_date";
            case BUSINESSUNITGERMANDESCRIPTION:
                return "business_unit_german_description";
            case BUSINESSUNITGERMANNAME:
                return "business_unit_german_name";
            case BUSINESSUNITID:
                return "business_unit_id";
            case "business_unit_id":
                return "business_unit_id";
            case BUSINESSUNITLASTUPDATEDATETIME:
                return "business_unit_last_update_datetime";
            case BUSINESSUNITLASTUPDATEDBY:
                return "business_unit_last_updated_by";
            case BUSINESSUNITNAME:
                return "business_unit_name";
            case "business_unit_name":
                return "business_unit_name";
            case BUSINESSUNITSHORTDESCRIPTION:
                return "business_unit_short_description";
            case BUSINESSUNITSHORTGERMANDESCRIPTION:
                return "business_unit_short_german_description";
            case CHBUSINESSUNITLASTUPDATEDBY:
                return "ch_business_unit_last_updated_by";
            case CHBUSINESSUNITLASTUPDATEDDATETIME:
                return "ch_business_unit_last_updated_datetime";
            case CUSTOMERID:
                return "customer_id";
            case CUSTOMERVENDORAFFILIATEINDICATOR:
                return "customer_vendor_affiliate_indicator";
            case FINANCIALBUSINESSUNITACTIVEINDICATOR:
                return "financial_business_unit_active_indicator";
            case FINANCIALBUSINESSUNITEFFECTIVEDATE:
                return "financial_business_unit_effective_date";
            case FINANCIALBUSINESSUNITGROUPID:
                return "financial_business_unit_group_id";
            case FINANCIALBUSINESSUNITGROUPNAME:
                return "financial_business_unit_group_name";
            case FINANCIALBUSINESSUNITNAME:
                return "financial_business_unit_name";
            case FIRMTYPE:
                return "firm_type";
            case ISOCURRENCYCODE:
                return "iso_currency_code";
            case LCDREFNO:
                return "lcd_ref_no";
            case LEGALENTITYSHORTDESCRIPTION:
                return "legal_entity_short_description";
            case LEGALENTITYACTIVEINDICATOR:
                return "legal_entity_active_indicator";
            case LEGALENTITYCODE:
                return "legal_entity_code";
            case "legal_entity_code":
                return "legal_entity_code";
            case "legal_entity_description":
                return "legal_entity_description";
            case LEGALENTITYDESCRIPTION:
                return "legal_entity_description";
            case LEGALENTITYEFFECTIVEDATE:
                return "legal_entity_effective_date";
            case LEGALENTITYGERMANDESCRIPTION:
                return "legal_entity_german_description";
            case LEGALENTITYLASTUPDATEDBY:
                return "legal_entity_last_updated_by";
            case LEGALENTITYLASTUPDATEDDATETIME:
                return "legal_entity_last_updated_datetime";
            case LOCALGAAPCURRENCY:
                return "local_gaap_currency";
            case MISMEMBERCODE:
                return "mis_member_code";
            case PROCESSINGLOCATIONCODE:
                return "processing_location_code";
            case SOURCEID:
                return "source_id";
            case DELTAFLAG:
                return "delta_flag";
            case BUDOMICILE:
                return "bu_domicile";
        }
        return property;
    }


    @Override
    public String getColumnDataType(String property) {

        switch (property + "a") {
            case FINANCIALBUSINESSUNITCODE:
                return "String";
            case BASECURRENCY:
                return "String";
            case BUSINESSUNITACTIVEINDICATOR:
                return "String";
            case BUSINESSUNITDESCRIPTION:
                return "String";
            case BUSINESSUNITEFFECTIVEDATE:
                return "java.sql.Date";
            case BUSINESSUNITGERMANDESCRIPTION:
                return "String";
            case BUSINESSUNITGERMANNAME:
                return "String";
            case BUSINESSUNITID:
                return "String";
            case BUSINESSUNITLASTUPDATEDATETIME:
                return "java.sql.Timestamp";
            case BUSINESSUNITLASTUPDATEDBY:
                return "String";
            case BUSINESSUNITNAME:
                return "String";
            case BUSINESSUNITSHORTDESCRIPTION:
                return "String";
            case BUSINESSUNITSHORTGERMANDESCRIPTION:
                return "String";
            case CHBUSINESSUNITLASTUPDATEDBY:
                return "String";
            case CHBUSINESSUNITLASTUPDATEDDATETIME:
                return "java.sql.Timestamp";
            case CUSTOMERID:
                return "String";
            case CUSTOMERVENDORAFFILIATEINDICATOR:
                return "String";
            case FINANCIALBUSINESSUNITACTIVEINDICATOR:
                return "String";
            case FINANCIALBUSINESSUNITEFFECTIVEDATE:
                return "java.sql.Date";
            case FINANCIALBUSINESSUNITGROUPID:
                return "String";
            case FINANCIALBUSINESSUNITGROUPNAME:
                return "String";
            case FINANCIALBUSINESSUNITNAME:
                return "String";
            case FIRMTYPE:
                return "String";
            case ISOCURRENCYCODE:
                return "String";
            case LCDREFNO:
                return "String";
            case LEGALENTITYSHORTDESCRIPTION:
                return "String";
            case LEGALENTITYACTIVEINDICATOR:
                return "String";
            case LEGALENTITYCODE:
                return "String";
            case LEGALENTITYDESCRIPTION:
                return "String";
            case LEGALENTITYEFFECTIVEDATE:
                return "java.sql.Date";
            case LEGALENTITYGERMANDESCRIPTION:
                return "String";
            case LEGALENTITYLASTUPDATEDBY:
                return "String";
            case LEGALENTITYLASTUPDATEDDATETIME:
                return "java.sql.Timestamp";
            case LOCALGAAPCURRENCY:
                return "String";
            case MISMEMBERCODE:
                return "String";
            case PROCESSINGLOCATIONCODE:
                return "String";
            case SOURCEID:
                return "String";
            case DELTAFLAG:
                return "String";
            case BUDOMICILE:
                return "String";
        }
        return "String";
    }


}
