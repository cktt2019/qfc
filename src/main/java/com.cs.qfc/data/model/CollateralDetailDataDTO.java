package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CollateralDetailDataDTO {

    private Integer id;

    private String sourceTxnId;

    private String a41AsOfDate;

    private String a42RecordsEntityIdentifier;

    private String a43collateraoPostedReceivedFlag;

    private String a44CounterpartyIdentifier;

    private String a45NettingAgreementIdentifier;

    private String a46UniqueCollateralItemIdentifier;

    private String a47OriginalFaceAmtCollateralItemLocalCurr;

    private String a48LocalCurrencyOfCollateralItem;

    private String a49MarketValueAmountOfCollateralItemInUsd;

    private String a410DescriptionOfCollateralItem;

    private String a411AssetClassification;

    private String a412CollateralPortfolioSegregationStatus;

    private String a413CollateralLocation;

    private String a414CollateralJurisdiction;

    private String a415CollateralRehypoAllowed;


}
