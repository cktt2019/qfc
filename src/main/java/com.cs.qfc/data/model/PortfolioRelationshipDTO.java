package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class PortfolioRelationshipDTO  {
   
	    private String portfolioRelationshipId ;
   
	    private String globalPartyId ;
   
	    private String partytypeDescription ;
   
	    private String partyTypeCode ;
   
	    private String partyLegalName ;
   
	    private String partyAliasName ;
   
	    private String legalRoleDefinitionId ;
   
	    private String roleType ;
   
	    private String roleDescription ;
   
	    private String roleCode ;
   
	    private String partyLegalRoleId ;
   
	    private String portfolioName ;
   
	    private String portfolioNumber ;
   
	    private String legalEntityName ;
   
	    private String cmsBookingId ;
   
	    private String entityJurisdiction ;
   
	    private String legalEntityCode ;
   
	    private String parentTrxnCovrg ;
   
	    private String trxnCoverage ;
   
	    private String trxnCoverageDescr ;
   
	    private String productId ;
   
	    private String csidSourceId ;
   
	    private String csidSourceName ;
   
	    private String csidSourceAltid ;
   
	    private String insightSourceId ;
   
	    private String insightSourceName ;
   
	    private String insightSourceAltid ;
   
	    private String sdsfbAccountNumber ;
   
	    private String sdsfbSystemId ;
   
	    private String sdsfbSystemName ;
   
	    private String sdsfbMnemonicValue ;
   
	    private String sdsfbMnemonicCode ;
   
	    private String ntpaAccountNumber ;
   
	    private String ntpaSystemId ;
   
	    private String ntpaSystemName ;
   
	    private String ntpaMnemonicValue ;
   
	    private String ntpaMnemonicCode ;
   
	    private String ntpanyAccountNumber ;
   
	    private String ntpanySystemId ;
   
	    private String ntpanySystemName ;
   
	    private String ntpanyMnemonicValue ;
   
	    private String ntpanyMnemonicCode ;
   
	    private String gmiAccountNumber ;
   
	    private String gmiSystemId ;
   
	    private String gmiSystemName ;
   
	    private String gmiMnemonicValue ;
   
	    private String gmiMnemonicCode ;
   
	    private String gmipacAccountNumber ;
   
	    private String gmipacSystemId ;
   
	    private String gmipacSystemName ;
   
	    private String gmipacMnemonicValue ;
   
	    private String gmipacMnemonicCode ;
   
	    private String loaniqAccountNumber ;
   
	    private String loaniqSystemId ;
   
	    private String loaniqSystemName ;
   
	    private String loaniqMnemonicValue ;
   
	    private String loaniqMnemonicCode ;
   
	    private String loannetAccountNumber ;
   
	    private String loannetSystemId ;
   
	    private String loannetSystemName ;
   
	    private String loannetMnemonicValue ;
   
	    private String loannetMnemonicCode ;
   
	    private String ftsintAccountNumber ;
   
	    private String ftsintSystemId ;
   
	    private String ftsintSystemName ;
   
	    private String ftsintMnemonicValue ;
   
	    private String ftsintMnemonicCode ;
   
	    private String globl1bdlAccountNumber ;
   
	    private String globl1bdlSystemId ;
   
	    private String globl1bdlSystemName ;
   
	    private String globl1bdlMnemonicValue ;
   
	    private String globl1bdlMnemonicCode ;
   
	    private String globl1nyAccountNumber ;
   
	    private String globl1nySystemId ;
   
	    private String globl1nySystemName ;
   
	    private String globl1nyMnemonicValue ;
   
	    private String globl1nyMnemonicCode ;
   
	    private String sourceEndTime ;
   
	    private String partySourceSystem ;
   
	    private String portfolioId ;
    


}
