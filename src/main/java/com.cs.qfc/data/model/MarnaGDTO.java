package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MarnaGDTO {

    private Integer cnid;

    private String agreementNumber;

    private String status;

    private String supersededByCnid;

    private String supersededByAgreement;

    private String legalAgreementDescription;

    private String csEntityBookinggroupId;

    private String csEntityBookinggroupName;

    private String negotiator;

    private String negotiatorLocation;

    private String counterParty;

    private String counterPartyRole;

    private String csid;

    private String legalId;

    private String formerlyKnownAs;

    private String countryOfInc;

    private String contractType;

    private String requestType;

    private String agreementTitle;

    private java.sql.Date contractDate;

    private String agency;

    private String collateralAgreementFlag;

    private String csaEnforceableFlag;

    private String transactionSpecificFlag;

    private String csEntityAgencyFlag;

    private String desk;

    private String iconid;

    private String csaRegFlag;


}
