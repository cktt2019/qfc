package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PrimeSwap {

    private java.sql.Date businessDate;

    private String legalEntity;

    private String swapId;

    private String swapVersion;

    private String counterPartyCode;

    private String counterPartyName;

    private String book;

    private String trader;

    private String swapType;

    private String framesoftMasterAgreementLlk;

    private String algoCollateralAgreementLlk;

    private String cduMasterAgreementLlk;

    private String cduCollateralAnnexLlk;

    private java.sql.Date swapTradeDate;

    private java.sql.Date swapTerminationDate;

    private java.sql.Date nextInterestPayDate;

    private String equityCurrency;

    private Float totalSwapValue;

    private String currencyEquityNotional;

    private Float initMargin;

    private String initialMarginDirection;

    private String csid;

    private String gsid;

    private Integer id;


}
