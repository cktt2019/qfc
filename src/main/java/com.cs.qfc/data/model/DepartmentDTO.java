package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DepartmentDTO {

    private String departmentId;

    private String businessControllerId;

    private String controllerId;

    private String departmentCategoryCode;

    private String departmentCategoryName;

    private String departmentDormantIndicator;

    private String departmentLongName;

    private String departmentName;

    private String departmentReconcilerGroupName;

    private String departmentStatus;

    private String departmentTraderId;

    private java.sql.Date departmentValidFromDate;

    private java.sql.Date departmentValidToDate;

    private String financialBusinessUnitCode;

    private String ivrGroup;

    private String l1SupervisorId;

    private String misOfficeCode;

    private String misOfficeName;

    private String ownerL2Id;

    private String recAndAdjControllerId;

    private String recAndAdjSupervisorId;

    private String reconcilerId;

    private String regionCode;

    private String regionDescription;

    private String revenueType;

    private String revenueTypeDescription;

    private String riskPortfolioId;

    private String specialisationIndicator;

    private String taxCategory;

    private String volkerRuleDescription;

    private String volkerRuleIndicator;

    private String fxCentrallyMaangedName;

    private String volckerSubDivision;

    private String volckerSubDivisionDescription;

    private String ihcHcdFlag;

    private String coveredDeptFlag;

    private String deltaFlag;

    private String remittancePolicyCode;

    private String firstInstanceDeptActivated;

    private String firstInstanceDeptInactivated;

    private String lastInstanceDeptActivated;

    private String lastInstanceDeptInactivated;

    private String rbpnlFlag;

    private String ivrGroupLongName;

    private String frtbDeptCategory;


}
