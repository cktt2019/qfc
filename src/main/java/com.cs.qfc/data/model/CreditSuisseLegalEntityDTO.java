package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreditSuisseLegalEntityDTO {

    private Integer count;

    private Integer enterpriseDNumber;

    private String bookingEntityNm;

    private String peopleSoftId;

    private String csidId;


}
