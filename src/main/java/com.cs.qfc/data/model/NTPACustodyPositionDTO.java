package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NTPACustodyPositionDTO {

    private java.sql.Date businessDate;

    private String compIdC;

    private String pIdC;

    private String cAcIdC;

    private String cAcSetlmtTypC;

    private String curIdC;

    private String pnbPosD;

    private String tNatvCurIdC;

    private String tBaseCurIdC;

    private String tConCurIdC;

    private String pnbMktValM;

    private String pnbMktValBaseM;

    private String pnbMktValConM;

    private String pPxClM;

    private String pnbActvyLastId;

    private String cIdC;

    private String cN;

    private String cAcTypC;

    private String cAcEfD;

    private String pMatyD;

    private String pStdT;

    private String pIsinIdC;

    private String pSedolIdC;

    private String pFileTypC;

    private String pnbAcctTypC;

    private Integer id;


}
