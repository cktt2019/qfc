package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PortfolioDTO {

    private String portfolioId;

    private String globalPartyId;

    private String partytypeDescription;

    private String legalName;

    private String partyAliasName;

    private String legalRoleDefinitionId;

    private String lrdRoleType;

    private String lrdRoleDescription;

    private String lrdRoleCode;

    private String partyLegalRoleId;

    private String sourceId;

    private String sourceName;

    private String internalPartyAltId;


}
