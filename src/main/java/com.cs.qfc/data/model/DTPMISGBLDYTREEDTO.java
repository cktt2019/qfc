package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DTPMISGBLDYTREEDTO {

    private String misMemberCode;

    private String americasProductLineHeadId;

    private String americasClusterHeadId;

    private String apacClusterHeadId;

    private String apacProductLineHeadId;

    private String businessControllerL2Id;

    private String cluster;

    private String clusterCode;

    private String clusterHeadId;

    private String desk;

    private String deskCode;

    private String division;

    private String divisionCode;

    private String euroClusterHeadId;

    private String euroProductLineHeadId;

    private String globalProductLineHeadId;

    private String hierarchyTraderId;

    private String misHierarchySortCode;

    private String misMemberName;

    private String misMemberTypeCode;

    private String misProduct;

    private String misRegionCode;

    private String misTreeCode;

    private String parentMisMemberCode;

    private String productControlSectionMgrId;

    private String productLineCode;

    private String productLineName;

    private String sub3ProductLine;

    private String sub3ProductLineDescription;

    private String subClusterCode;

    private String subClusterName;

    private String subDivisionCode;

    private String subDivisionDescription;

    private String subProductDescription;

    private String subProductLine;

    private String swissProductLineHeadId;

    private String tradingSupervisorId;

    private String tradingSupervisorDelegateId;

    private String misProductCode;

    private String misUnitPercentOwned;

    private String misUnitPercentDescription;

    private String deltaFlag;

    private String cddsTreeNodeStatus;

    private String tradingSupport;

    private String tradeSupportManagerEmail;


}
