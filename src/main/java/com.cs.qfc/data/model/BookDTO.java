package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookDTO {

    private String globalBookId;

    private String accountMethod;

    private String accountingBasisIndicator;

    private String bookCategoryCode;

    private String bookCategoryDescription;

    private String bookControllerId;

    private String bookDormantIndicator;

    private String bookName;

    private String bookPnlSystemName;

    private String bookRef;

    private String bookStatus;

    private String bookSubCategoryCode;

    private String bookSubCategoryDescription;

    private String bookTraderId;

    private String cnyDeliverableOutsideChinaIndicator;

    private String departmentId;

    private String frontOfficeRepresentativeId;

    private String gplAtomCode;

    private String nhfsIndicator;

    private String remoteBookingPolicyIndicator;

    private String splitHedgeComments;

    private String splitHedgeIndicator;

    private String usTaxCategoryCode;

    private java.sql.Timestamp bookLastUpdatedDatetime;

    private String financialBusinessUnitCode;

    private String deltaFlag;

    private String bookRequestStatusCode;

    private java.sql.Date firstInstanceBookActivated;

    private java.sql.Date lastInstanceBookActivated;

    private java.sql.Date firstInstanceBookInactivated;

    private java.sql.Date lastInstanceBook;

    private String emailAddress;

    private String cavFlag;

    private String fosFlag;

    private String bookingIntent;

    private String bookRequestId;

    private String iparEmplIdEmailId;

    private String bookTraderEmailId;

    private String pcSupervisorEmailId;

    private String bookEmpIdEmailId;

    private String pctnlEmplIdEmailId;


}
