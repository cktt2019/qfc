package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessUnitDTO {

    private String financialBusinessUnitCode;

    private String baseCurrency;

    private String businessUnitActiveIndicator;

    private String businessUnitDescription;

    private java.sql.Date businessUnitEffectiveDate;

    private String businessUnitGermanDescription;

    private String businessUnitGermanName;

    private String businessUnitId;

    private java.sql.Timestamp businessUnitLastUpdateDatetime;

    private String businessUnitLastUpdatedBy;

    private String businessUnitName;

    private String businessUnitShortDescription;

    private String businessUnitShortGermanDescription;

    private String chBusinessUnitLastUpdatedBy;

    private java.sql.Timestamp chBusinessUnitLastUpdatedDatetime;

    private String customerId;

    private String customerVendorAffiliateIndicator;

    private String financialBusinessUnitActiveIndicator;

    private java.sql.Date financialBusinessUnitEffectiveDate;

    private String financialBusinessUnitGroupId;

    private String financialBusinessUnitGroupName;

    private String financialBusinessUnitName;

    private String firmType;

    private String isoCurrencyCode;

    private String lcdRefNo;

    private String legalEntityShortDescription;

    private String legalEntityActiveIndicator;

    private String legalEntityCode;

    private String legalEntityDescription;

    private java.sql.Date legalEntityEffectiveDate;

    private String legalEntityGermanDescription;

    private String legalEntityLastUpdatedBy;

    private java.sql.Timestamp legalEntityLastUpdatedDatetime;

    private String localGaapCurrency;

    private String misMemberCode;

    private String processingLocationCode;

    private String sourceId;

    private String deltaFlag;

    private String buDomicile;


}
