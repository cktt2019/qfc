package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Party {

    private Long globalPartyId;

    private String csid;

    private String csidSource;

    private String gsid;

    private String gsidSource;

    private String countryOfDomicileIsoCountryCode;

    private String countryOfIncorporationIsoCountryCode;

    private String partyLegalName;

    private String addressTypeCode;

    private String addressAddressLine1;

    private String addressAddressLine2;

    private String addressAddressLine3;

    private String addressAddressLine4;

    private String addressPostalCode;

    private String addressCountryIsoCountryName;

    private String addressAddressFullPhoneNumber;

    private String addressElctronicAddressId;

    private String partyLegalRoleDefinitionId;

    private String partyLegalRoleId;

    private String internalPartyIdAltSourceName;

    private String partyLegalRoleDescription;

    private String partyLegalStatusType;

    private String partyLegalStatusCode;

    private String addressTypeDescription;

    private String addressCityName;

    private String addressPoBoxCity;

    private String externalPartyAltIdSourceId;

    private String externalPartyAltIdSourceName;

    private String externalPartyAltIdSourceIdentificationNumber;


}
