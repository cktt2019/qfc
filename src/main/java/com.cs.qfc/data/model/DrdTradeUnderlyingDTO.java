package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DrdTradeUnderlyingDTO {

    private Integer id;

    private java.sql.Date cobDate;

    private java.sql.Date loadCobDate;

    private String loadBook;

    private String bookName;

    private Integer tradeLoadId;

    private Date loadTimestamp;

    private String snapshotName;

    private Integer snapshotId;

    private Date snapshotCreationDatetime;

    private String interfaceVersion;

    private Integer tradeId;

    private Integer tftTradeVersion;

    private Integer tradeLocationId;

    private String primaryTcn;

    private String tcn;

    private Date tradeVersionUpdateTime;

    private String cancellationDate;

    private String gbmEntity;

    private String approvalStatus;

    private String eventReason;

    private String externalId;

    private java.sql.Date primaryMaturityDate;

    private String productSubType;

    private String productType;

    private String productValuationType;

    private String tradeStatus;

    private String underlyingId;

    private String underlyingIdType;

    private String accountCcy;

    private Float basketStrike;

    private String basketWeighting;

    private Float coupon;

    private String couponFreq;

    private Float finalPrice;

    private String floatingRateIndex;

    private String floatingRateIndexTenor;

    private String floatingRateIndexType;

    private Float initialPrice;

    private String refObligCategory;

    private String refPoolLongName;

    private String refPoolShortName;

    private Integer sequenceId;

    private String underlyingAssetClass;

    private String underlyingCcy;

    private String underlyingDesc;

    private String underlyingExtId;

    private String underlyingExtIdType;

    private java.sql.Date underlyingMatDate;

    private String underlyingRiskId;

    private String payRec;

    private String settlementType;

    private String businessGroup;

    private String valuationClass;

    private String valuationType;

    private String tradeSourceSystem;

    private String underlyingPriceSource;

    private String cduMasterAgreementLlk;

    private String cduCollateralAnnexLlk;

    private Integer framesoftMasterAgreementLlk;

    private Integer algoCollateralAnnexLlk;

    private String gbmBookRef;

    private String underlyingProductId;

    private String underlyingProductType;

    private String underlyingRiskType;

    private String bankruptcy;

    private String delivObligAcceleratedMatured;

    private String delivObligAccruedInterest;

    private String delivObligAssignableLoan;

    private String delivObligCategory;

    private String delivObligConsentRequiredLoan;

    private String delivOblgDirectLoanPrtCptn;

    private String delivObligListed;

    private Integer delivObligMaximumMaturity;

    private String delivObligMaxMaturitySpecified;

    private String delivObligNotBearer;

    private String delivObligNotContingent;

    private String delivObligNotDomesticCurrency;

    private String delivObligNotDomesticIssuance;

    private String delivObligNotDomesticLaw;

    private String delivObligNotSovereignLender;

    private String delivObligPhyParipassu;

    private String delivObligSpecifiedCcyList;

    private String delivObligSpecifiedCcyType;

    private String delivObligTransferable;

    private String entityType;

    private String primaryObligor;

    private Float recoveryValue;

    private String referenceEntityRedCode;

    private String referenceEntityUniqueEntityId;

    private String referenceObligationBloombergId;

    private String referenceObligationCusip;

    private String referenceObligationGuarantor;

    private String referenceObligationIsin;

    private Float referencePrice;

    private String refObligorCouponRate;

    private java.sql.Date refObligorMaturityDate;

    private Float refObligorNotionalAmt;

    private String refObligorNotionalAmtCcy;

    private String refObligorObligationCategory;

    private Float valUnwind;


}
