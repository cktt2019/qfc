package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DebtInstrumentDTO {

    private String gpiIssue;

    private String isinId;

    private String sedolId;

    private String securityDescriptionLong;

    private java.sql.Date nxtPayDate;

    private java.sql.Date nextCallDate;

    private java.sql.Date nextPutDate;


}
