package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DrdTradeHeaderDTO {

    private Integer id;

    private java.sql.Date cobDate;

    private java.sql.Date loadCodDate;

    private String bookName;

    private String tradeLoadId;

    private Date loadTimestamp;

    private String snapshotName;

    private Integer snapshotId;

    private Date snapshotCreationDatetime;

    private String interfaceVersion;

    private Integer tradeId;

    private Integer tfTradeVersion;

    private Integer tradeLocationId;

    private String primaryTcn;

    private Date tradeVersionUpdateTime;

    private String cancellationDate;

    private String xccyIndicator;

    private String gbmEntity;

    private Integer totalLocalPv;

    private String totalLocalPvCcyCode;

    private String totalPvStatus;

    private Integer totalRpTpv;

    private java.sql.Date tradePvRegDate;

    private Integer tradePvRegDays;

    private String confirmationStatus;

    private Integer defaultFairValue;

    private String proveCluster;

    private String proveFamily;

    private String proveId;

    private String proveProduct;

    private String approvalStatus;

    private String eventReason;

    private String externalId;

    private java.sql.Date primaryMaturityDate;

    private String productSubType;

    private String productType;

    private String productValuationType;

    private String rootTcn;

    private String tradeStatus;

    private String cduMasterAgreementLlk;

    private String cduCollateralAnnexLlk;

    private Integer freamesoftMasterAgreementLlk;

    private Integer algoCollateralAnnexLlk;

    private String accountArea;

    private String brokerId;

    private Float collIndepAmount;

    private String collIndepCcy;

    private String collIndepOtherMargQuote;

    private Float collIndepPercent;

    private String collIndepType;

    private String marketerId;

    private String riskManagementOnlyFlag;

    private String structuredTradeFlag;

    private java.sql.Date tradeDate;

    private String traderName;

    private String tradeSourceSystem;

    private String tradingPartyId;

    private String tradingPartyIdType;

    private String city;

    private String deal;

    private String stripId;

    private String customerNumber;

    private String displayId;

    private String assetClass1;

    private String assetClass2;

    private String assetClass3;

    private String assetClass4;

    private String linkedSetType;

    private String sysLinkId;

    private Integer leadTradeId;

    private Integer leadTradeVersion;

    private Integer leadTradeLocationId;

    private String leadExternalId;

    private String leadTradeBook;

    private Integer washTradeId;

    private Integer washTradeVersion;

    private Integer washTradeLocationId;

    private String washExternalId;

    private String washTradeBook;

    private Integer riskTradeId;

    private Integer riskTradeVersion;

    private Integer riskTradeLocationId;

    private String riskExternalId;

    private String riskTradeBook;

    private String syntheticTradeIndicator;

    private String linkageStatus;

    private String linkageStatusDescription;

    private Integer linkedTradeId;

    private String linkedPrimaryTcn;

    private Integer linkedTradeVersion;

    private String linkedTradeLocationId;

    private String linkedTradeExternalId;

    private String linkedTradeBook;

    private String valuationClass;

    private String valuationType;

    private Float premiumAmount;

    private String swapsWireId;

    private String approxBooking;

    private java.sql.Date approxBookingReviewDate;

    private String docsCounterPartyId;

    private String cloIndicator;

    private String loanMitigationFlag;

    private String usi;

    private String cssi;

    private Integer gmbBookId;

    private String area;

    private String structuredId;

    private String pvLevel;

    private String counterPartyCsid;

    private String creeTradeIdentifier;

    private String creeTradeIdentifierVersion;

    private String lchOrigCounterParty;

    private String gbmCluster;

    private String gbmDivision;

    private String affirmPlatformStatus;

    private String docsActualProcessingMethod;

    private String uti;

    private String swireManualConfirmedReq;

    private java.sql.Date entryDateTime;

    private String fundingIndicator;

    private Float leverageFactor;

    private String linkTransId;

    private String linkType;

    private String masterTradeTradeId;

    private String gbmBookRef;

    private String lastAmmendBy;

    private String structureType;

    private String garantiaSysId;

    private String earlyTmTermParty;

    private java.sql.Date earlyTmOptStartDate;

    private String counterPartyGsid;

    private String entityType;

    private String event;

    private java.sql.Date eventTradeDate;

    private String fidcPmInitialMargin;

    private String fidcPmNettingOnly;

    private String fidcPmVariationMargin;

    private String lateTrdNotificationReason;

    private Integer masterAgreementType;

    private String notesFrontOffice;

    private String riskAdvisor;

    private String riskTrader;

    private String tradingPartyGsid;

    private String fidcPmInd;

    private String agent;

    private String paymentIndicator;

    private java.sql.Date renegotiationDate;

    private Integer alternativeVersionId;

    private String cifbu;

    private String deskBook;

    private String metalInkTradeId;

    private java.sql.Date linkedStartDate;

    private java.sql.Date linkedEndDate;

    private String salespersonId;

    private String sourceSystemId;

    private Integer sourceSystemVersion;

    private String earlyTmExerciseFreq;

    private String earlyTmOptStyle;

    private String paymentPackageSysLinkId;

    private String tfMatchStatus;

    private Integer bestMatchTfTradeVersion;


}
