package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EquityConvertibleBondDTO {

    private String cusipId;

    private String isinId;

    private java.sql.Date nextCallDate;

    private String nextPutDate;

    private java.sql.Date nxtPayDate;

    private String securityDescription;
    private String securityDescriptionLong;
    private String a3c7Ind;

    private String bbYKet;

    private String compExchCodeDer;

    private Float convPrice;

    private Float conRatio;

    private String csManaged;

    private Float equityFloat;

    private String exchange;

    private String fidesaCode;

    private String fundObjective;

    private String glossG5;

    private String idBbGlobal;

    private String idBbUnique;

    private String italyId;

    private String japanId;

    private String ricId;

    private String singId;

    private String valorenId;

    private String wertpId;

    private String hugoId;

    private String ntpaLsEqyPoDt;

    private String londonImsDescription;

    private java.sql.Date maturityDate;

    private String nasdaqReportCodeDer;

    private String primaryRic;

    private Float qtyPerUndlyShr;

    private String undlyPerAdr;

    private String regs;

    private String regionPrim;

    private String rtrTicketSymbol;

    private String tradeCntry;

    private String undlySecIsin;

    private Float votingRightPerShare;

    private String cmuId;

    private String ultPrntTicketExchange;

    private String poetsId;

    private String ntpaSoi;

    private Float cpn;

    private String ssrLiquidityIndicator;

    private String reExchange;

    private String glossK3;

    private String secType;

    private String fundEuroDirectUcits;

    private String fundAssetClassFocus;

    private String bbCompanyId;

    private String spIssueId;

    private String fundClosedNewInv;

    private String legalName;

    private Float tradingLotSize;

    private String reAdrUndlyRic;

    private String relativeIndex;

    private String primTrad;

    private String cumulativeFlag;

    private String fundCustodianName;

    private java.sql.Date initialPubOffer;

    private String gicsSectorIsr;

    private String gicsSectorNameIsr;

    private String gicsIndustryGroupIsr;

    private String gicsIndustryGroupNameIsr;

    private String gicsIndustrySr;

    private String gicsIndustryNameIsr;

    private String naicsIdIsr;

    private String ntpaCountryOfSettlement;

    private String gliRp;

    private String cddsLastUpdatedTime;

    private String publiclyTraded;

    private String csSettleDays;

    private String bbUnderlyingSecurity;

    private String bbUnderlyingTypeCode;

    private String tickSizePilotGroup;

    private java.sql.Date calledDate;

    private Float calledPrice;

    private String couponType;

    private String reClassScheme;

    private String legalEntityIdentifier;

    private String companyCorpTicker;

    private String companyToParent;

    private String bbIssuerType;

    private String fisecIssuerId;

    private String ultPrntCompId;

    private Integer isArchived;

    private String localExchangeSymbol;

    private String cfiCode;

    private String fisn;

    private java.sql.Date issDate;

    private java.sql.Date firstSettlementDate;

    private String whenIssuedFlag;

    private String minorTradingCurrencyInd;

    private String riskAlert;

    private String mdsId;

    private String fundGeographicFocus;

    private String fundLeverageAmount;

    private String cmsCode;

    private String idSpi;

    private String staticListingId;

    private String clientType;

    private String marketingDesk;

    private String externalId;

    private String issueBook;

    private String dutchId;

    private String belgiumId;

    private String istarId;

    private Float idBbSecurity;

    private String operatingMic;

    private Integer bbIndustrySectorCd;

    private Integer bbIndustryGroupCd;

    private Integer bbIndustrySubGroupCd;

    private java.sql.Timestamp cddsRecCreationTime;

    private String listedExchInd;

    private String idMalaysian;

    private Float redemptionPrice;

    private String series;

    private String cpnFreq;

    private String gmSoi;

    private String csPvtPlacementInd;


}
