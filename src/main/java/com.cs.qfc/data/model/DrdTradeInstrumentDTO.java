package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DrdTradeInstrumentDTO {

    private Integer id;

    private java.sql.Date cobDate;

    private String gbmEntity;

    private Integer tradeId;

    private String counterPartyId;

    private String bookName;

    private String assetClass;

    private String cduMasterAgreementLlk;

    private String cduCollateralAnnexLlk;

    private String framesoftMasterAgreementLlk;

    private String algoCollateralAnnexLlk;

    private java.sql.Date tradeDate;

    private java.sql.Date primaryMaturityDate;

    private String localPvCcyCode;

    private Integer localPv;


}
