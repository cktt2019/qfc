package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DebtConvertibleBondDTO {

    private String sedolId;

    private String securityDescriptionLong;

    private java.sql.Date nxtPayDate;

    private String nextCallDate;

    private String nextPutDate;

    private String accrualCount;

    private String aggteIssue;

    private Float baseCpi;

    private String clearingHouseDer;

    private String bbTicker;

    private String bookEntry;

    private String bradyBusDayConvention;

    private String businessCalendar;

    private String calcTypeDes;

    private String callDiscrete;

    private String callFeature;

    private String callNotice;

    private java.sql.Date callDate;

    private Float callPrice;

    private String cpnFre;

    private Float cpnFreqDays;

    private String couponType;

    private String couponTypeReset;

    private String cpnFreqYld;

    private String csManaged;

    private java.sql.Date issDate;

    private String debtStatus;

    private String defSecurity;

    private Float daysToSettle;

    private String fwEntry;

    private String fidessaZone;

    private String fidessaCode;

    private java.sql.Date firstCallDate;

    private java.sql.Date firstCoupon;

    private Float fixFltDayCount;

    private String floatRst;

    private Float floatRstIndx;

    private Float fltDaysPrior;

    private String glossK3;

    private String glossL9;

    private String glossM3;

    private String idBb;

    private String idBbUnique;

    private String epicId;

    private String cmuId;

    private String isinId;

    private String japanId;

    private String miscDomesticId;

    private String poetsId;

    private String ricId;

    private String sedol2Id;

    private String singId;

    private String incomeCurrency;

    private String industrySbgprIssue;

    private Float intRateCap;

    private String intRateFl;

    private String bojId;

    private String fisecId;

    private String hugoId;

    private String istarId;

    private String nriId;

    private String smuId;

    private String nrtIssuerId;

    private Float marginPerc;

    private java.sql.Date maturityDate;

    private String muniMinTax;

    private String muniFedTax;

    private String stateCode;

    private java.sql.Date nextReset;

    private String nriIntPayClass;

    private String nriCode;

    private String nriSecSub;

    private String nriSicc;

    private String offeringTyp;

    private java.sql.Date datedDate;

    private String physical;

    private java.sql.Date prevPayDate;

    private java.sql.Date prvRstDate;

    private Float princFac;

    private String privatePlacement;

    private String proRataCall;

    private Float pubBalOut;

    private String putDisc;

    private String putFeature;

    private Integer putNotice;

    private String ratingsTrigger;

    private String refixFreq;

    private String regs;

    private String regionCode;

    private String resetIdx;

    private String restrictCode;

    private String secType;

    private String sinkFreq;

    private Integer benchmarkSpread;

    private String tickerSymbol;

    private String tradingCurrency;

    private Float mnPiece;

    private String traxReport;

    private java.sql.Date ultMatDate;

    private String desNotes;

    private String ultPrntCompId;

    private String fisecIssuerId;

    private String ultPrntTickerExchange;

    private String couponFrequency;

    private String londonImsDescription;

    private Integer conRatio;

    private String undlySecIsin;

    private Float convPrice;

    private String perSecSrc01;

    private java.sql.Date issueDate;

    private String spIssuerId;

    private String ntpaInPosition;

    private String isInternationalSukuk;

    private String isDomesticSukuk;

    private String syntheticId;

    private String poolCusipId;

    private String auctRatePfdInd;

    private String repoclearFlg;

    private String muniLetterCreditIssuer;

    private String muniCorpObligator2;

    private String insuranceStatus;

    private String obligator;

    private Float cpn;

    private String tradeCntry;

    private String commonId;

    private String series;

    private String classCode;


}
