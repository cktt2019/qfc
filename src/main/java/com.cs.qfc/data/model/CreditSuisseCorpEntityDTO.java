package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreditSuisseCorpEntityDTO {

    private Integer count;

    private String legalNameOfEntity;

    private String lei;

    private String immediateParentName;

    private String immediateParentLei;

    private String percentageOwnership;

    private String entityType;

    private String domicile;

    private String jurisdictionIncorp;


}
