package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PorfolioRelationshipDTO {

    private String portfolioId;

    private String globalPartyId;

    private String partytypeDescription;

    private String partyTypeCode;

    private String partyLegalName;

    private String partyAliasName;

    private String legalRoleDefinitionId;

    private String roleType;

    private String roleDescription;

    private String roleCode;

    private String partyLegalRoleId;

    private String sourceId;

    private String sourceName;

    private String internalPartyAltId;

    private String portfolioName;

    private String portfolioNumber;

    private String legalEntityName;

    private String cmsBookingId;

    private String entityJurisdiction;

    private String legalEntityCode;

    private String parentTrxnCovrg;

    private String trxnCoverage;

    private String trxnCoverageDescr;

    private String productId;

    private String accountNumber;

    private String systemId;

    private String systemName;

    private String mnemonicValue;

    private String mnemonicCode;


}
