package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PrimeSwapPositionDTO {

    private Integer id;

    private java.sql.Date businessDate;

    private String swapId;

    private String swapVersion;

    private String positionId;

    private String positionVersion;

    private String swapType;

    private String counterpartyCode;

    private String counterpartyName;

    private String settledLtdCostBase;

    private String swapCcyToUsdFx;


}
