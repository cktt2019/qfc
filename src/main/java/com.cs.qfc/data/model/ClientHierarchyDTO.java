package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientHierarchyDTO {

    private String csid;

    private String gsid;

    private String counterPartyName;

    private String immediateParentCsid;

    private String immediateParentGsid;

    private String ultimateParentCsid;

    private String ultimateParentGsid;

    private String level;


}
