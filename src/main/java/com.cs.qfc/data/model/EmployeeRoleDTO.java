package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeRoleDTO {

    private String employeeId;

    private String employeeActiveIndicator;

    private java.sql.Date employeeEffectiveDate;

    private String employeeEmailId;

    private String employeeFirstName;

    private String employeeLastName;

    private String employeeLastUpdatedBy;

    private String employeeLastUpdatedDatetime;

    private String employeeLogonDomain;

    private String employeeLogonId;

    private String employeePidId;

    private String city;

    private String region;

    private String countryLongDesc;

    private String deltaFlag;


}
