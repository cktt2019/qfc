package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VisionDTO {

    private java.sql.Date businessDate;

    private String entity;

    private String cpty;

    private String book;

    private String proveNameProduct;

    private String agreementId1;

    private java.sql.Date tradeDate;

    private java.sql.Date endDate;

    private String bondNextCoup;

    private String cashCurr;

    private String pvUsd;

    private String defaultFvLevel;

    private String face;

    private String valueOn;

    private String rehypothecationIndicator;

    private String custodianId;

    private Integer id;


}
