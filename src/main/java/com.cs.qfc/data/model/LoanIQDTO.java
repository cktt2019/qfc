package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoanIQDTO {

    private Integer id;

    private String cptyCsid;

    private String sdsId;

    private String portfolioCode;

    private String agreementId;

    private java.sql.Date tradeDate;

    private String maturityDate;

    private String currency;

    private Float presentValue;

    private Float tradeAmount;

    private String recordType;

    private String swapId;


}
