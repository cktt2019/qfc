import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {CreditSuisseCorpEntity} from './CreditSuisseCorpEntity.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class CreditSuisseCorpEntityService {

    constructor(
    private http: HttpClient
    ) {}

	private CreditSuisseCorpEntityUrl = 'http://localhost:8081/api/api/creditsuissecorpentitys';

	private selectedCreditSuisseCorpEntity : CreditSuisseCorpEntity;

    creditSuisseCorpEntityDetailViewComponent : any;

      setSelectedCreditSuisseCorpEntity(selectedItem : CreditSuisseCorpEntity){
        this.selectedCreditSuisseCorpEntity = selectedItem;
        console.log('setSelectedCreditSuisseCorpEntity' + selectedItem);
      }

    getSelectedCreditSuisseCorpEntity () : CreditSuisseCorpEntity{
        return this.selectedCreditSuisseCorpEntity ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 saveCreditSuisseCorpEntity (creditSuisseCorpEntity : CreditSuisseCorpEntity ){
	              this.http.post<CreditSuisseCorpEntity>(this.CreditSuisseCorpEntityUrl, creditSuisseCorpEntity , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}


customSearch (searchCritera : SearchCriteria) : Observable<CreditSuisseCorpEntity[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<CreditSuisseCorpEntity[]>(this.CreditSuisseCorpEntityUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched CreditSuisseCorpEntity(s)')),
                   catchError(this.handleError<CreditSuisseCorpEntity[]>('findAll', []))
                   );
	}

   findAll () : Observable<CreditSuisseCorpEntity[]>{

	   const url = `this.CreditSuisseCorpEntityUrl"}`;
	   return this.http.get<CreditSuisseCorpEntity[]>(this.CreditSuisseCorpEntityUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched CreditSuisseCorpEntity(s)')),
                   catchError(this.handleError<CreditSuisseCorpEntity[]>('findAll', []))
                   );
	}
     

    findById (count : number): Observable<CreditSuisseCorpEntity>{
	  const url = `${this.CreditSuisseCorpEntityUrl}/${count}`;
      return this.http.get<CreditSuisseCorpEntity>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched CreditSuisseCorpEntity with count=${count}`)),
        catchError(this.handleError<CreditSuisseCorpEntity>(`CreditSuisseCorpEntity count=${count}`))
      );
	 }
	

   updateCreditSuisseCorpEntity (creditSuisseCorpEntity : CreditSuisseCorpEntity ) : Observable<any> {
         return this.http.put<CreditSuisseCorpEntity>(this.CreditSuisseCorpEntityUrl, creditSuisseCorpEntity , this.httpOptions).pipe(
           tap(_ => console.log(`updated CreditSuisseCorpEntity ${creditSuisseCorpEntity}`)),
           catchError(this.handleError<CreditSuisseCorpEntity>(`CreditSuisseCorpEntity =${creditSuisseCorpEntity}`))
         );
	}
	

   deleteById (count : number){
	     const url = `${this.CreditSuisseCorpEntityUrl}/${count}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
