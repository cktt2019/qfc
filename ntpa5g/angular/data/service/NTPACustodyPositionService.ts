import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {NTPACustodyPosition} from './NTPACustodyPosition.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class NTPACustodyPositionService {

    constructor(
    private http: HttpClient
    ) {}

	private NTPACustodyPositionUrl = 'http://localhost:8081/api/api/ntpacustodypositions';

	private selectedNTPACustodyPosition : NTPACustodyPosition;

    nTPACustodyPositionDetailViewComponent : any;

      setSelectedNTPACustodyPosition(selectedItem : NTPACustodyPosition){
        this.selectedNTPACustodyPosition = selectedItem;
        console.log('setSelectedNTPACustodyPosition' + selectedItem);
      }

    getSelectedNTPACustodyPosition () : NTPACustodyPosition{
        return this.selectedNTPACustodyPosition ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 saveNTPACustodyPosition (nTPACustodyPosition : NTPACustodyPosition ){
	              this.http.post<NTPACustodyPosition>(this.NTPACustodyPositionUrl, nTPACustodyPosition , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}


customSearch (searchCritera : SearchCriteria) : Observable<NTPACustodyPosition[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<NTPACustodyPosition[]>(this.NTPACustodyPositionUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched NTPACustodyPosition(s)')),
                   catchError(this.handleError<NTPACustodyPosition[]>('findAll', []))
                   );
	}

   findAll () : Observable<NTPACustodyPosition[]>{

	   const url = `this.NTPACustodyPositionUrl"}`;
	   return this.http.get<NTPACustodyPosition[]>(this.NTPACustodyPositionUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched NTPACustodyPosition(s)')),
                   catchError(this.handleError<NTPACustodyPosition[]>('findAll', []))
                   );
	}
     

    findById (id : number): Observable<NTPACustodyPosition>{
	  const url = `${this.NTPACustodyPositionUrl}/${id}`;
      return this.http.get<NTPACustodyPosition>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched NTPACustodyPosition with id=${id}`)),
        catchError(this.handleError<NTPACustodyPosition>(`NTPACustodyPosition id=${id}`))
      );
	 }
	

   updateNTPACustodyPosition (nTPACustodyPosition : NTPACustodyPosition ) : Observable<any> {
         return this.http.put<NTPACustodyPosition>(this.NTPACustodyPositionUrl, nTPACustodyPosition , this.httpOptions).pipe(
           tap(_ => console.log(`updated NTPACustodyPosition ${nTPACustodyPosition}`)),
           catchError(this.handleError<NTPACustodyPosition>(`NTPACustodyPosition =${nTPACustodyPosition}`))
         );
	}
	

   deleteById (id : number){
	     const url = `${this.NTPACustodyPositionUrl}/${id}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
