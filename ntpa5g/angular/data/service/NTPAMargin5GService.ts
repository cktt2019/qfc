import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {NTPAMargin5G} from './NTPAMargin5G.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class NTPAMargin5GService {

    constructor(
    private http: HttpClient
    ) {}

	private NTPAMargin5GUrl = 'http://localhost:8081/api/api/ntpamargin5gs';

	private selectedNTPAMargin5G : NTPAMargin5G;

    nTPAMargin5GDetailViewComponent : any;

      setSelectedNTPAMargin5G(selectedItem : NTPAMargin5G){
        this.selectedNTPAMargin5G = selectedItem;
        console.log('setSelectedNTPAMargin5G' + selectedItem);
      }

    getSelectedNTPAMargin5G () : NTPAMargin5G{
        return this.selectedNTPAMargin5G ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 saveNTPAMargin5G (nTPAMargin5G : NTPAMargin5G ){
	              this.http.post<NTPAMargin5G>(this.NTPAMargin5GUrl, nTPAMargin5G , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}


customSearch (searchCritera : SearchCriteria) : Observable<NTPAMargin5G[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<NTPAMargin5G[]>(this.NTPAMargin5GUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched NTPAMargin5G(s)')),
                   catchError(this.handleError<NTPAMargin5G[]>('findAll', []))
                   );
	}

   findAll () : Observable<NTPAMargin5G[]>{

	   const url = `this.NTPAMargin5GUrl"}`;
	   return this.http.get<NTPAMargin5G[]>(this.NTPAMargin5GUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched NTPAMargin5G(s)')),
                   catchError(this.handleError<NTPAMargin5G[]>('findAll', []))
                   );
	}
     

    findById (id : number): Observable<NTPAMargin5G>{
	  const url = `${this.NTPAMargin5GUrl}/${id}`;
      return this.http.get<NTPAMargin5G>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched NTPAMargin5G with id=${id}`)),
        catchError(this.handleError<NTPAMargin5G>(`NTPAMargin5G id=${id}`))
      );
	 }
	

   updateNTPAMargin5G (nTPAMargin5G : NTPAMargin5G ) : Observable<any> {
         return this.http.put<NTPAMargin5G>(this.NTPAMargin5GUrl, nTPAMargin5G , this.httpOptions).pipe(
           tap(_ => console.log(`updated NTPAMargin5G ${nTPAMargin5G}`)),
           catchError(this.handleError<NTPAMargin5G>(`NTPAMargin5G =${nTPAMargin5G}`))
         );
	}
	

   deleteById (id : number){
	     const url = `${this.NTPAMargin5GUrl}/${id}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
