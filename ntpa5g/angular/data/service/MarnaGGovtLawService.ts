import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {MarnaGGovtLaw} from './MarnaGGovtLaw.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class MarnaGGovtLawService {

    constructor(
    private http: HttpClient
    ) {}

	private MarnaGGovtLawUrl = 'http://localhost:8081/api/api/marnaggovtlaws';

	private selectedMarnaGGovtLaw : MarnaGGovtLaw;

    marnaGGovtLawDetailViewComponent : any;

      setSelectedMarnaGGovtLaw(selectedItem : MarnaGGovtLaw){
        this.selectedMarnaGGovtLaw = selectedItem;
        console.log('setSelectedMarnaGGovtLaw' + selectedItem);
      }

    getSelectedMarnaGGovtLaw () : MarnaGGovtLaw{
        return this.selectedMarnaGGovtLaw ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 saveMarnaGGovtLaw (marnaGGovtLaw : MarnaGGovtLaw ){
	              this.http.post<MarnaGGovtLaw>(this.MarnaGGovtLawUrl, marnaGGovtLaw , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}


customSearch (searchCritera : SearchCriteria) : Observable<MarnaGGovtLaw[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<MarnaGGovtLaw[]>(this.MarnaGGovtLawUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched MarnaGGovtLaw(s)')),
                   catchError(this.handleError<MarnaGGovtLaw[]>('findAll', []))
                   );
	}

   findAll () : Observable<MarnaGGovtLaw[]>{

	   const url = `this.MarnaGGovtLawUrl"}`;
	   return this.http.get<MarnaGGovtLaw[]>(this.MarnaGGovtLawUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched MarnaGGovtLaw(s)')),
                   catchError(this.handleError<MarnaGGovtLaw[]>('findAll', []))
                   );
	}
     

    findById (id : number): Observable<MarnaGGovtLaw>{
	  const url = `${this.MarnaGGovtLawUrl}/${id}`;
      return this.http.get<MarnaGGovtLaw>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched MarnaGGovtLaw with id=${id}`)),
        catchError(this.handleError<MarnaGGovtLaw>(`MarnaGGovtLaw id=${id}`))
      );
	 }
	

   updateMarnaGGovtLaw (marnaGGovtLaw : MarnaGGovtLaw ) : Observable<any> {
         return this.http.put<MarnaGGovtLaw>(this.MarnaGGovtLawUrl, marnaGGovtLaw , this.httpOptions).pipe(
           tap(_ => console.log(`updated MarnaGGovtLaw ${marnaGGovtLaw}`)),
           catchError(this.handleError<MarnaGGovtLaw>(`MarnaGGovtLaw =${marnaGGovtLaw}`))
         );
	}
	

   deleteById (id : number){
	     const url = `${this.MarnaGGovtLawUrl}/${id}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
