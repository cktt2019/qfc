import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {AgreementTitlesScope} from './AgreementTitlesScope.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class AgreementTitlesScopeService {

    constructor(
    private http: HttpClient
    ) {}

	private AgreementTitlesScopeUrl = 'http://localhost:8081/api/api/agreementtitlesscopes';

	private selectedAgreementTitlesScope : AgreementTitlesScope;

    agreementTitlesScopeDetailViewComponent : any;

      setSelectedAgreementTitlesScope(selectedItem : AgreementTitlesScope){
        this.selectedAgreementTitlesScope = selectedItem;
        console.log('setSelectedAgreementTitlesScope' + selectedItem);
      }

    getSelectedAgreementTitlesScope () : AgreementTitlesScope{
        return this.selectedAgreementTitlesScope ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 saveAgreementTitlesScope (agreementTitlesScope : AgreementTitlesScope ){
	              this.http.post<AgreementTitlesScope>(this.AgreementTitlesScopeUrl, agreementTitlesScope , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}


customSearch (searchCritera : SearchCriteria) : Observable<AgreementTitlesScope[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<AgreementTitlesScope[]>(this.AgreementTitlesScopeUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched AgreementTitlesScope(s)')),
                   catchError(this.handleError<AgreementTitlesScope[]>('findAll', []))
                   );
	}

   findAll () : Observable<AgreementTitlesScope[]>{

	   const url = `this.AgreementTitlesScopeUrl"}`;
	   return this.http.get<AgreementTitlesScope[]>(this.AgreementTitlesScopeUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched AgreementTitlesScope(s)')),
                   catchError(this.handleError<AgreementTitlesScope[]>('findAll', []))
                   );
	}
     

    findById (count : number): Observable<AgreementTitlesScope>{
	  const url = `${this.AgreementTitlesScopeUrl}/${count}`;
      return this.http.get<AgreementTitlesScope>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched AgreementTitlesScope with count=${count}`)),
        catchError(this.handleError<AgreementTitlesScope>(`AgreementTitlesScope count=${count}`))
      );
	 }
	

   updateAgreementTitlesScope (agreementTitlesScope : AgreementTitlesScope ) : Observable<any> {
         return this.http.put<AgreementTitlesScope>(this.AgreementTitlesScopeUrl, agreementTitlesScope , this.httpOptions).pipe(
           tap(_ => console.log(`updated AgreementTitlesScope ${agreementTitlesScope}`)),
           catchError(this.handleError<AgreementTitlesScope>(`AgreementTitlesScope =${agreementTitlesScope}`))
         );
	}
	

   deleteById (count : number){
	     const url = `${this.AgreementTitlesScopeUrl}/${count}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
