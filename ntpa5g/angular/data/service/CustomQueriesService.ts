import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {CustomQueries} from './CustomQueries.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SearchCriteria} from './SearchCriteria.model';
import {SearchInfo} from './SearchInfo.model';

@Injectable({
  providedIn: 'root',
})
export class CustomQueriesService {

    constructor(
    private http: HttpClient
    ) {}

	private CustomQueriesUrl = 'http://localhost:8081/api/api/customqueriess';
	//Optional
	private CustomQueriesListUrl = 'http://localhost:8081/api/api/customquerieslist';

	private selectedCustomQueries : CustomQueries;

    customQueriesDetailViewComponent : any;

      setSelectedCustomQueries(selectedItem : CustomQueries){
        this.selectedCustomQueries = selectedItem;
        console.log('setSelectedCustomQueries' + selectedItem);
      }

    getSelectedCustomQueries () : CustomQueries{
        return this.selectedCustomQueries ;
      }


	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };



       
       


	 saveCustomQueries (customQueries : CustomQueries ){
	              this.http.post<CustomQueries>(this.CustomQueriesUrl, customQueries , this.httpOptions).subscribe(
                      res => {
                        console.log(res);
                      });

	}

  findQueriesList(entityName : string) : Observable<string[]>{
        const url = `${this.CustomQueriesListUrl}/${entityName}`;
        return this.http.get<string[]>(url, this.httpOptions).pipe(
        tap(_ => console.log('fetched findQueriesList')),
                      catchError(this.handleError<string[]>('findAll', []))
                      );
      }

customSearch (searchCritera : SearchCriteria) : Observable<CustomQueries[]>{

	   var info = new SearchInfo();
       info.criteria = searchCritera;
	   return this.http.post<CustomQueries[]>(this.CustomQueriesUrl+"/custom",info, this.httpOptions).pipe(
	   tap(_ => console.log('fetched CustomQueries(s)')),
                   catchError(this.handleError<CustomQueries[]>('findAll', []))
                   );
	}

   findAll () : Observable<CustomQueries[]>{

	   const url = `this.CustomQueriesUrl"}`;
	   return this.http.get<CustomQueries[]>(this.CustomQueriesUrl, this.httpOptions).pipe(
	   tap(_ => console.log('fetched CustomQueries(s)')),
                   catchError(this.handleError<CustomQueries[]>('findAll', []))
                   );
	}
     

    findById (criteriaName : string): Observable<CustomQueries>{
	  const url = `${this.CustomQueriesUrl}/${criteriaName}`;
      return this.http.get<CustomQueries>(url, this.httpOptions).pipe(
        tap(_ => console.log(`fetched CustomQueries with criteriaName=${criteriaName}`)),
        catchError(this.handleError<CustomQueries>(`CustomQueries criteriaName=${criteriaName}`))
      );
	 }
	

   updateCustomQueries (customQueries : CustomQueries ) : Observable<any> {
         return this.http.put<CustomQueries>(this.CustomQueriesUrl, customQueries , this.httpOptions).pipe(
           tap(_ => console.log(`updated CustomQueries ${customQueries}`)),
           catchError(this.handleError<CustomQueries>(`CustomQueries =${customQueries}`))
         );
	}
	

   deleteById (criteriaName : string){
	     const url = `${this.CustomQueriesUrl}/${criteriaName}`;
             return this.http.delete(url).subscribe(res => {
           console.log(res);});


	}

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }


}
