

	export interface CreditSuisseCorpEntity  {

 	    count : number;

 	    legalNameOfEntity : string;

 	    lei : string;

 	    immediateParentName : string;

 	    immediateParentLei : string;

 	    percentageOwnership : string;

 	    entityType : string;

 	    domicile : string;

 	    jurisdictionIncorp : string;

    

}
