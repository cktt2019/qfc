

	export interface NTPAMargin5G  {

 	    businessDate : any;

 	    runDate : string;

 	    account : string;

 	    currId : string;

 	    liquidatingEquity : string;

 	    marginEquity : string;

 	    regTRequirements : string;

 	    maintRequirements : string;

 	    nyseRequirements : string;

 	    regtExcessDeficit : string;

 	    maintExcessDeficit : string;

 	    nyseExcessDeficit : string;

 	    sma : string;

 	    cashAvailable : string;

 	    totalTdBalance : string;

 	    totalTdLongMktValue : string;

 	    totalTdShortMktValue : string;

 	    totalOptionLongMktValue : string;

 	    totalOptionShortMktValue : string;

 	    totalSdBalance : string;

 	    totalSdLongMktValue : string;

 	    totalSdShortMktValue : string;

 	    totalAssessedBalance : string;

 	    totalInterestAccrual : string;

 	    type : string;

 	    tdBalance : string;

 	    tdMarketValLng : string;

 	    tdMarketValShrt : string;

 	    sdBalance : string;

 	    sdMarketValue : string;

 	    legalEntityId : string;

 	    ntpaProductId : string;

 	    id : number;

    

}
