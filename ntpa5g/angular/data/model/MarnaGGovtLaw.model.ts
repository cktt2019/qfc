

	export interface MarnaGGovtLaw  {

 	    businessDate : any;

 	    agmtNo : string;

 	    cnid : string;

 	    agmtDate : string;

 	    counterparty : string;

 	    csid : string;

 	    agmtType : string;

 	    agmtTitle : string;

 	    coi : string;

 	    governingLaw : string;

 	    id : number;

    

}
