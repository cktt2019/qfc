

	export interface NTPACustodyPosition  {

 	    businessDate : any;

 	    compIdC : string;

 	    pIdC : string;

 	    cAcIdC : string;

 	    cAcSetlmtTypC : string;

 	    curIdC : string;

 	    pnbPosD : string;

 	    tNatvCurIdC : string;

 	    tBaseCurIdC : string;

 	    tConCurIdC : string;

 	    pnbMktValM : string;

 	    pnbMktValBaseM : string;

 	    pnbMktValConM : string;

 	    pPxClM : string;

 	    pnbActvyLastId : string;

 	    cIdC : string;

 	    cN : string;

 	    cAcTypC : string;

 	    cAcEfD : string;

 	    pMatyD : string;

 	    pStdT : string;

 	    pIsinIdC : string;

 	    pSedolIdC : string;

 	    pFileTypC : string;

 	    pnbAcctTypC : string;

 	    id : number;

    

}
