= Solocal-DTS REST API
v1.0.0
:doctype: book
:icons: font
:source-highlighter: highlightjs
:highlightjs-theme: github
:toc: left
:toclevels: 7
:sectlinks:
:hardbreaks:
:sectnums:


[introduction]
= Introduction
To enable the transition to a less monolithic approach of managing the digital advertisement platform developed by DTS,
 a REST API will be used. +
This gives internal application developers a programmatic interface into the  DTS back-end systems. +
The result should be smaller custom applications build using a well defined versioned and documented API.


[[dco]]
= DCO
 DCO stands for Dynamic Creative Optimisation.
 The following DCO end-points provide the API to configure the DCO system, covering the setup of reference data like available algorithms and customer specific configuration. 
 The end result of this is the runtime delivery of highly optimized creatives.
[[dco-metadata]]
== DCO Metadata

This section consists of the endpoints to manage the DCO-Algorithms registry.
Before any new algorithm is used by the DCO runtime engine, it MUST be registered in the algorithms registry.
The registry holds the name of the algorithm and it's input/output parameters

=== Add Algo no I/O Params




[[example_curl_request]]
==== Curl request
include::{snippets}/np-add-algos/curl-request.adoc[]

==== HTTPie request
include::{snippets}/np-add-algos/httpie-request.adoc[]
 HTTPie is a command line HTTP client with an intuitive UI (https://httpie.org/)

==== HTTP request
include::{snippets}/np-add-algos/http-request.adoc[]

==== Request body
include::{snippets}/np-add-algos/request-body.adoc[]



==== Response

include::{snippets}/np-add-algos/http-response.adoc[]


