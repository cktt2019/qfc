package com.cs.qfc.rest.api.service;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class MarnaGGovtLawService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(MarnaGGovtLawService.class);
 
    @Autowired
	MarnaGGovtLawRepository marnaGGovtLawRepository;

	@Autowired
    CustomizedMarnaGGovtLawRepository customizedMarnaGGovtLawRepository;

	
	@Transactional
	public MarnaGGovtLaw saveMarnaGGovtLaw (MarnaGGovtLawDTO marnaGGovtLawDTO){
	 MarnaGGovtLaw marnaGGovtLaw  = DTOToMarnaGGovtLawConverter.convert(marnaGGovtLawDTO);
	 return marnaGGovtLawRepository.save(marnaGGovtLaw);
	}
	
	@Transactional
	public MarnaGGovtLaw save (MarnaGGovtLaw marnaGGovtLaw){
	 return marnaGGovtLawRepository.save(marnaGGovtLaw);
	}

       
       
       
  
    @Transactional
   public Collection<MarnaGGovtLaw> findAll (){
	   return marnaGGovtLawRepository.findAll();
	}

	@Transactional
    public Collection<MarnaGGovtLawDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedMarnaGGovtLawRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public MarnaGGovtLaw findById (Integer id){
	   Optional<MarnaGGovtLaw> marnaGGovtLaw  = marnaGGovtLawRepository.findById(id);
	  return (marnaGGovtLaw.isPresent() ? marnaGGovtLaw.get() : null);
	}
	
   @Transactional
   public MarnaGGovtLaw updateMarnaGGovtLaw (MarnaGGovtLaw marnaGGovtLaw,MarnaGGovtLawDTO marnaGGovtLawDto){
	
	marnaGGovtLaw.setBusinessDate(marnaGGovtLawDto.getBusinessDate());
	marnaGGovtLaw.setAgmtNo(marnaGGovtLawDto.getAgmtNo());
	marnaGGovtLaw.setCnid(marnaGGovtLawDto.getCnid());
	marnaGGovtLaw.setAgmtDate(marnaGGovtLawDto.getAgmtDate());
	marnaGGovtLaw.setCounterparty(marnaGGovtLawDto.getCounterparty());
	marnaGGovtLaw.setCsid(marnaGGovtLawDto.getCsid());
	marnaGGovtLaw.setAgmtType(marnaGGovtLawDto.getAgmtType());
	marnaGGovtLaw.setAgmtTitle(marnaGGovtLawDto.getAgmtTitle());
	marnaGGovtLaw.setCoi(marnaGGovtLawDto.getCoi());
	marnaGGovtLaw.setGoverningLaw(marnaGGovtLawDto.getGoverningLaw());
	 return marnaGGovtLawRepository.save(marnaGGovtLaw);
	}
	
   @Transactional
   public void deleteById (Integer id){
	   marnaGGovtLawRepository.deleteById(id);

	}
	
   
   

}
