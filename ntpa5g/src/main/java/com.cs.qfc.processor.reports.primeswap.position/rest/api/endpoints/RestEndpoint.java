package com.cs.qfc.rest.api.endpoints;

import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *  Rest enpoint for data retrieval and modifications
 *
 *
 * @author ctanyitang
 */
 	@Controller
    @Path("/api")
    @Transactional
	public class RestEndpoint {
	
     @Autowired
     NTPACustodyPositionService nTPACustodyPositionService;
     
     @Autowired
     CustomQueriesService customQueriesService;
     
     @Autowired
     AgreementTitlesScopeService agreementTitlesScopeService;
     
     @Autowired
     CreditSuisseCorpEntityService creditSuisseCorpEntityService;
     
     @Autowired
     NTPAMargin5GService nTPAMargin5GService;
     
     @Autowired
     MarnaGGovtLawService marnaGGovtLawService;
     



    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/agreementtitlesscopes")
	public List<AgreementTitlesScopeDTO> listAgreementtitlesscopes () {

		return agreementTitlesScopeService.findAll().stream().map(e -> AgreementTitlesScopeToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/agreementtitlesscopes/custom")
    	public List<AgreementTitlesScopeDTO> createAgreementTitlesScope ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(agreementTitlesScopeService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/agreementtitlesscopes")
	public Response createAgreementTitlesScope ( @RequestBody AgreementTitlesScopeDTO agreementTitlesScope ) {
		AgreementTitlesScope createdAgreementTitlesScope = agreementTitlesScopeService.saveAgreementTitlesScope(agreementTitlesScope);
		return Response.status(Response.Status.CREATED).header("Location","/agreementtitlesscopes/"+createdAgreementTitlesScope.getCount()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/creditsuissecorpentitys")
	public List<CreditSuisseCorpEntityDTO> listCreditsuissecorpentitys () {

		return creditSuisseCorpEntityService.findAll().stream().map(e -> CreditSuisseCorpEntityToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/creditsuissecorpentitys/custom")
    	public List<CreditSuisseCorpEntityDTO> createCreditSuisseCorpEntity ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(creditSuisseCorpEntityService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/creditsuissecorpentitys")
	public Response createCreditSuisseCorpEntity ( @RequestBody CreditSuisseCorpEntityDTO creditSuisseCorpEntity ) {
		CreditSuisseCorpEntity createdCreditSuisseCorpEntity = creditSuisseCorpEntityService.saveCreditSuisseCorpEntity(creditSuisseCorpEntity);
		return Response.status(Response.Status.CREATED).header("Location","/creditsuissecorpentitys/"+createdCreditSuisseCorpEntity.getCount()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ntpamargin5gs")
	public List<NTPAMargin5GDTO> listNtpamargin5gs () {

		return nTPAMargin5GService.findAll().stream().map(e -> NTPAMargin5GToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/ntpamargin5gs/custom")
    	public List<NTPAMargin5GDTO> createNTPAMargin5G ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(nTPAMargin5GService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/ntpamargin5gs")
	public Response createNTPAMargin5G ( @RequestBody NTPAMargin5GDTO nTPAMargin5G ) {
		NTPAMargin5G createdNTPAMargin5G = nTPAMargin5GService.saveNTPAMargin5G(nTPAMargin5G);
		return Response.status(Response.Status.CREATED).header("Location","/ntpamargin5gs/"+createdNTPAMargin5G.getId()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ntpamargin5gs/{id}")
	public NTPAMargin5GDTO getNTPAMargin5GById (@PathParam("id") Integer  id    ) {

		NTPAMargin5G nTPAMargin5G =	nTPAMargin5GService.findById(id    );
		if(nTPAMargin5G != null ) 
		   return NTPAMargin5GToDTOConverter.convert(nTPAMargin5G);
		throw new NotFoundException("No NTPAMargin5G with  " +" id = " + id    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/ntpamargin5gs/{id}")
	public Response updateNTPAMargin5GById (@RequestBody NTPAMargin5GDTO nTPAMargin5G ) {
	    NTPAMargin5G existingNTPAMargin5G =	nTPAMargin5GService.findById(nTPAMargin5G.getId()    );
		if(existingNTPAMargin5G == null ) 
		throw new NotFoundException("No NTPAMargin5G with  " +" nTPAMargin5G.getId() = " + nTPAMargin5G.getId()    + " exists");
		
	    nTPAMargin5GService.updateNTPAMargin5G(existingNTPAMargin5G,nTPAMargin5G);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/ntpamargin5gs/{id}")
	public Response deleteNTPAMargin5GById (@PathParam("id") Integer  id    ) 
	  {
	        NTPAMargin5G existingNTPAMargin5G =	nTPAMargin5GService.findById(id    );
		if(existingNTPAMargin5G == null ) 
		throw new NotFoundException("No NTPAMargin5G with  " +" id = " + id    + " exists");
	
		nTPAMargin5GService.deleteById(id    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/marnaggovtlaws/{id}")
	public MarnaGGovtLawDTO getMarnaGGovtLawById (@PathParam("id") Integer  id    ) {

		MarnaGGovtLaw marnaGGovtLaw =	marnaGGovtLawService.findById(id    );
		if(marnaGGovtLaw != null ) 
		   return MarnaGGovtLawToDTOConverter.convert(marnaGGovtLaw);
		throw new NotFoundException("No MarnaGGovtLaw with  " +" id = " + id    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/marnaggovtlaws/{id}")
	public Response updateMarnaGGovtLawById (@RequestBody MarnaGGovtLawDTO marnaGGovtLaw ) {
	    MarnaGGovtLaw existingMarnaGGovtLaw =	marnaGGovtLawService.findById(marnaGGovtLaw.getId()    );
		if(existingMarnaGGovtLaw == null ) 
		throw new NotFoundException("No MarnaGGovtLaw with  " +" marnaGGovtLaw.getId() = " + marnaGGovtLaw.getId()    + " exists");
		
	    marnaGGovtLawService.updateMarnaGGovtLaw(existingMarnaGGovtLaw,marnaGGovtLaw);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/marnaggovtlaws/{id}")
	public Response deleteMarnaGGovtLawById (@PathParam("id") Integer  id    ) 
	  {
	        MarnaGGovtLaw existingMarnaGGovtLaw =	marnaGGovtLawService.findById(id    );
		if(existingMarnaGGovtLaw == null ) 
		throw new NotFoundException("No MarnaGGovtLaw with  " +" id = " + id    + " exists");
	
		marnaGGovtLawService.deleteById(id    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ntpacustodypositions")
	public List<NTPACustodyPositionDTO> listNtpacustodypositions () {

		return nTPACustodyPositionService.findAll().stream().map(e -> NTPACustodyPositionToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/ntpacustodypositions/custom")
    	public List<NTPACustodyPositionDTO> createNTPACustodyPosition ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(nTPACustodyPositionService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/ntpacustodypositions")
	public Response createNTPACustodyPosition ( @RequestBody NTPACustodyPositionDTO nTPACustodyPosition ) {
		NTPACustodyPosition createdNTPACustodyPosition = nTPACustodyPositionService.saveNTPACustodyPosition(nTPACustodyPosition);
		return Response.status(Response.Status.CREATED).header("Location","/ntpacustodypositions/"+createdNTPACustodyPosition.getId()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/customqueries")
	public List<CustomQueriesDTO> listCustomqueries () {

		return customQueriesService.findAll().stream().map(e -> CustomQueriesToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/customqueries/custom")
    	public List<CustomQueriesDTO> createCustomQueries ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(customQueriesService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/customqueries")
	public Response createCustomQueries ( @RequestBody CustomQueriesDTO customQueries ) {
		CustomQueries createdCustomQueries = customQueriesService.saveCustomQueries(customQueries);
		return Response.status(Response.Status.CREATED).header("Location","/customqueries/"+createdCustomQueries.getCriteriaName()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/creditsuissecorpentitys/{count}")
	public CreditSuisseCorpEntityDTO getCreditSuisseCorpEntityById (@PathParam("count") Integer  count    ) {

		CreditSuisseCorpEntity creditSuisseCorpEntity =	creditSuisseCorpEntityService.findById(count    );
		if(creditSuisseCorpEntity != null ) 
		   return CreditSuisseCorpEntityToDTOConverter.convert(creditSuisseCorpEntity);
		throw new NotFoundException("No CreditSuisseCorpEntity with  " +" count = " + count    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/creditsuissecorpentitys/{count}")
	public Response updateCreditSuisseCorpEntityById (@RequestBody CreditSuisseCorpEntityDTO creditSuisseCorpEntity ) {
	    CreditSuisseCorpEntity existingCreditSuisseCorpEntity =	creditSuisseCorpEntityService.findById(creditSuisseCorpEntity.getCount()    );
		if(existingCreditSuisseCorpEntity == null ) 
		throw new NotFoundException("No CreditSuisseCorpEntity with  " +" creditSuisseCorpEntity.getCount() = " + creditSuisseCorpEntity.getCount()    + " exists");
		
	    creditSuisseCorpEntityService.updateCreditSuisseCorpEntity(existingCreditSuisseCorpEntity,creditSuisseCorpEntity);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/creditsuissecorpentitys/{count}")
	public Response deleteCreditSuisseCorpEntityById (@PathParam("count") Integer  count    ) 
	  {
	        CreditSuisseCorpEntity existingCreditSuisseCorpEntity =	creditSuisseCorpEntityService.findById(count    );
		if(existingCreditSuisseCorpEntity == null ) 
		throw new NotFoundException("No CreditSuisseCorpEntity with  " +" count = " + count    + " exists");
	
		creditSuisseCorpEntityService.deleteById(count    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/agreementtitlesscopes/{count}")
	public AgreementTitlesScopeDTO getAgreementTitlesScopeById (@PathParam("count") Integer  count    ) {

		AgreementTitlesScope agreementTitlesScope =	agreementTitlesScopeService.findById(count    );
		if(agreementTitlesScope != null ) 
		   return AgreementTitlesScopeToDTOConverter.convert(agreementTitlesScope);
		throw new NotFoundException("No AgreementTitlesScope with  " +" count = " + count    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/agreementtitlesscopes/{count}")
	public Response updateAgreementTitlesScopeById (@RequestBody AgreementTitlesScopeDTO agreementTitlesScope ) {
	    AgreementTitlesScope existingAgreementTitlesScope =	agreementTitlesScopeService.findById(agreementTitlesScope.getCount()    );
		if(existingAgreementTitlesScope == null ) 
		throw new NotFoundException("No AgreementTitlesScope with  " +" agreementTitlesScope.getCount() = " + agreementTitlesScope.getCount()    + " exists");
		
	    agreementTitlesScopeService.updateAgreementTitlesScope(existingAgreementTitlesScope,agreementTitlesScope);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/agreementtitlesscopes/{count}")
	public Response deleteAgreementTitlesScopeById (@PathParam("count") Integer  count    ) 
	  {
	        AgreementTitlesScope existingAgreementTitlesScope =	agreementTitlesScopeService.findById(count    );
		if(existingAgreementTitlesScope == null ) 
		throw new NotFoundException("No AgreementTitlesScope with  " +" count = " + count    + " exists");
	
		agreementTitlesScopeService.deleteById(count    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/marnaggovtlaws")
	public List<MarnaGGovtLawDTO> listMarnaggovtlaws () {

		return marnaGGovtLawService.findAll().stream().map(e -> MarnaGGovtLawToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/marnaggovtlaws/custom")
    	public List<MarnaGGovtLawDTO> createMarnaGGovtLaw ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(marnaGGovtLawService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/marnaggovtlaws")
	public Response createMarnaGGovtLaw ( @RequestBody MarnaGGovtLawDTO marnaGGovtLaw ) {
		MarnaGGovtLaw createdMarnaGGovtLaw = marnaGGovtLawService.saveMarnaGGovtLaw(marnaGGovtLaw);
		return Response.status(Response.Status.CREATED).header("Location","/marnaggovtlaws/"+createdMarnaGGovtLaw.getId()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ntpacustodypositions/{id}")
	public NTPACustodyPositionDTO getNTPACustodyPositionById (@PathParam("id") Integer  id    ) {

		NTPACustodyPosition nTPACustodyPosition =	nTPACustodyPositionService.findById(id    );
		if(nTPACustodyPosition != null ) 
		   return NTPACustodyPositionToDTOConverter.convert(nTPACustodyPosition);
		throw new NotFoundException("No NTPACustodyPosition with  " +" id = " + id    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/ntpacustodypositions/{id}")
	public Response updateNTPACustodyPositionById (@RequestBody NTPACustodyPositionDTO nTPACustodyPosition ) {
	    NTPACustodyPosition existingNTPACustodyPosition =	nTPACustodyPositionService.findById(nTPACustodyPosition.getId()    );
		if(existingNTPACustodyPosition == null ) 
		throw new NotFoundException("No NTPACustodyPosition with  " +" nTPACustodyPosition.getId() = " + nTPACustodyPosition.getId()    + " exists");
		
	    nTPACustodyPositionService.updateNTPACustodyPosition(existingNTPACustodyPosition,nTPACustodyPosition);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/ntpacustodypositions/{id}")
	public Response deleteNTPACustodyPositionById (@PathParam("id") Integer  id    ) 
	  {
	        NTPACustodyPosition existingNTPACustodyPosition =	nTPACustodyPositionService.findById(id    );
		if(existingNTPACustodyPosition == null ) 
		throw new NotFoundException("No NTPACustodyPosition with  " +" id = " + id    + " exists");
	
		nTPACustodyPositionService.deleteById(id    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/customqueries/{criteriaName}")
	public CustomQueriesDTO getCustomQueriesById (@PathParam("criteriaName") String  criteriaName    ) {

		CustomQueries customQueries =	customQueriesService.findById(criteriaName    );
		if(customQueries != null ) 
		   return CustomQueriesToDTOConverter.convert(customQueries);
		throw new NotFoundException("No CustomQueries with  " +" criteriaName = " + criteriaName    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/customqueries/{criteriaName}")
	public Response updateCustomQueriesById (@RequestBody CustomQueriesDTO customQueries ) {
	    CustomQueries existingCustomQueries =	customQueriesService.findById(customQueries.getCriteriaName()    );
		if(existingCustomQueries == null ) 
		throw new NotFoundException("No CustomQueries with  " +" customQueries.getCriteriaName() = " + customQueries.getCriteriaName()    + " exists");
		
	    customQueriesService.updateCustomQueries(existingCustomQueries,customQueries);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/customqueries/{criteriaName}")
	public Response deleteCustomQueriesById (@PathParam("criteriaName") String  criteriaName    ) 
	  {
	        CustomQueries existingCustomQueries =	customQueriesService.findById(criteriaName    );
		if(existingCustomQueries == null ) 
		throw new NotFoundException("No CustomQueries with  " +" criteriaName = " + criteriaName    + " exists");
	
		customQueriesService.deleteById(criteriaName    );

		return Response.status(Response.Status.OK).build();
		
	}


        //Extra method required for custom search (Remove if not needed)
	    @GET
        @Produces(MediaType.APPLICATION_JSON)
        @Path("/customquerieslist/{entityName}")
        public List<String> listCustomqueriesForEntity (@PathParam("entityName") String  entityName) {

            List<CustomQueries> filtered = customQueriesService.findAll().stream().filter(x -> x.getEntityName().equals(entityName)).collect(Collectors.toList());
            return
                    filtered.stream().map(x->x.getCriteriaName()).collect(Collectors.toList());

        }
	}







