package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.NTPAMargin5G;
import com.cs.qfc.data.model.NTPAMargin5GDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class NTPAMargin5GToDTOConverter  {
	

	

public static NTPAMargin5GDTO convert(NTPAMargin5G nTPAMargin5G) {
 if (nTPAMargin5G != null)
  return NTPAMargin5GDTO.builder().businessDate(nTPAMargin5G.getBusinessDate())
.runDate(nTPAMargin5G.getRunDate())
.account(nTPAMargin5G.getAccount())
.currId(nTPAMargin5G.getCurrId())
.liquidatingEquity(nTPAMargin5G.getLiquidatingEquity())
.marginEquity(nTPAMargin5G.getMarginEquity())
.regTRequirements(nTPAMargin5G.getRegTRequirements())
.maintRequirements(nTPAMargin5G.getMaintRequirements())
.nyseRequirements(nTPAMargin5G.getNyseRequirements())
.regtExcessDeficit(nTPAMargin5G.getRegtExcessDeficit())
.maintExcessDeficit(nTPAMargin5G.getMaintExcessDeficit())
.nyseExcessDeficit(nTPAMargin5G.getNyseExcessDeficit())
.sma(nTPAMargin5G.getSma())
.cashAvailable(nTPAMargin5G.getCashAvailable())
.totalTdBalance(nTPAMargin5G.getTotalTdBalance())
.totalTdLongMktValue(nTPAMargin5G.getTotalTdLongMktValue())
.totalTdShortMktValue(nTPAMargin5G.getTotalTdShortMktValue())
.totalOptionLongMktValue(nTPAMargin5G.getTotalOptionLongMktValue())
.totalOptionShortMktValue(nTPAMargin5G.getTotalOptionShortMktValue())
.totalSdBalance(nTPAMargin5G.getTotalSdBalance())
.totalSdLongMktValue(nTPAMargin5G.getTotalSdLongMktValue())
.totalSdShortMktValue(nTPAMargin5G.getTotalSdShortMktValue())
.totalAssessedBalance(nTPAMargin5G.getTotalAssessedBalance())
.totalInterestAccrual(nTPAMargin5G.getTotalInterestAccrual())
.type(nTPAMargin5G.getType())
.tdBalance(nTPAMargin5G.getTdBalance())
.tdMarketValLng(nTPAMargin5G.getTdMarketValLng())
.tdMarketValShrt(nTPAMargin5G.getTdMarketValShrt())
.sdBalance(nTPAMargin5G.getSdBalance())
.sdMarketValue(nTPAMargin5G.getSdMarketValue())
.legalEntityId(nTPAMargin5G.getLegalEntityId())
.ntpaProductId(nTPAMargin5G.getNtpaProductId())
.id(nTPAMargin5G.getId())
.build();
 return null;
	}
    
}
