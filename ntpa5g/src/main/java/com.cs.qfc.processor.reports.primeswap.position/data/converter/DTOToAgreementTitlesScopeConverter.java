package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.AgreementTitlesScope;
import com.cs.qfc.data.model.AgreementTitlesScopeDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToAgreementTitlesScopeConverter  {
	

	

public static AgreementTitlesScope convert(AgreementTitlesScopeDTO agreementTitlesScopeDto) {
 if (agreementTitlesScopeDto != null)
  return AgreementTitlesScope.builder().count(agreementTitlesScopeDto.getCount())
.agreementTitle(agreementTitlesScopeDto.getAgreementTitle())
.agreementType(agreementTitlesScopeDto.getAgreementType())
.inScope(agreementTitlesScopeDto.getInScope())
 
.build();
 return null;
	}
    
}
