package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.MarnaGGovtLaw;
import com.cs.qfc.data.model.MarnaGGovtLawDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToMarnaGGovtLawConverter  {
	

	

public static MarnaGGovtLaw convert(MarnaGGovtLawDTO marnaGGovtLawDto) {
 if (marnaGGovtLawDto != null)
  return MarnaGGovtLaw.builder().businessDate(marnaGGovtLawDto.getBusinessDate())
.agmtNo(marnaGGovtLawDto.getAgmtNo())
.cnid(marnaGGovtLawDto.getCnid())
.agmtDate(marnaGGovtLawDto.getAgmtDate())
.counterparty(marnaGGovtLawDto.getCounterparty())
.csid(marnaGGovtLawDto.getCsid())
.agmtType(marnaGGovtLawDto.getAgmtType())
.agmtTitle(marnaGGovtLawDto.getAgmtTitle())
.coi(marnaGGovtLawDto.getCoi())
.governingLaw(marnaGGovtLawDto.getGoverningLaw())
.id(marnaGGovtLawDto.getId())
 
.build();
 return null;
	}
    
}
