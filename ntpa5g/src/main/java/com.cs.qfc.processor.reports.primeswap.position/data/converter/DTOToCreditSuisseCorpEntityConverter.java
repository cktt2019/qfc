package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.CreditSuisseCorpEntity;
import com.cs.qfc.data.model.CreditSuisseCorpEntityDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToCreditSuisseCorpEntityConverter  {
	

	

public static CreditSuisseCorpEntity convert(CreditSuisseCorpEntityDTO creditSuisseCorpEntityDto) {
 if (creditSuisseCorpEntityDto != null)
  return CreditSuisseCorpEntity.builder().count(creditSuisseCorpEntityDto.getCount())
.legalNameOfEntity(creditSuisseCorpEntityDto.getLegalNameOfEntity())
.lei(creditSuisseCorpEntityDto.getLei())
.immediateParentName(creditSuisseCorpEntityDto.getImmediateParentName())
.immediateParentLei(creditSuisseCorpEntityDto.getImmediateParentLei())
.percentageOwnership(creditSuisseCorpEntityDto.getPercentageOwnership())
.entityType(creditSuisseCorpEntityDto.getEntityType())
.domicile(creditSuisseCorpEntityDto.getDomicile())
.jurisdictionIncorp(creditSuisseCorpEntityDto.getJurisdictionIncorp())
 
.build();
 return null;
	}
    
}
