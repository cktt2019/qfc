package com.cs.qfc.data.converter;


import com.cs.qfc.data.jpa.entities.NTPAMargin5G;
import com.cs.qfc.data.model.NTPAMargin5GDTO;


/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToNTPAMargin5GConverter  {
	

	

public static NTPAMargin5G convert(NTPAMargin5GDTO nTPAMargin5GDto) {
 if (nTPAMargin5GDto != null)
  return NTPAMargin5G.builder().businessDate(nTPAMargin5GDto.getBusinessDate())
.runDate(nTPAMargin5GDto.getRunDate())
.account(nTPAMargin5GDto.getAccount())
.currId(nTPAMargin5GDto.getCurrId())
.liquidatingEquity(nTPAMargin5GDto.getLiquidatingEquity())
.marginEquity(nTPAMargin5GDto.getMarginEquity())
.regTRequirements(nTPAMargin5GDto.getRegTRequirements())
.maintRequirements(nTPAMargin5GDto.getMaintRequirements())
.nyseRequirements(nTPAMargin5GDto.getNyseRequirements())
.regtExcessDeficit(nTPAMargin5GDto.getRegtExcessDeficit())
.maintExcessDeficit(nTPAMargin5GDto.getMaintExcessDeficit())
.nyseExcessDeficit(nTPAMargin5GDto.getNyseExcessDeficit())
.sma(nTPAMargin5GDto.getSma())
.cashAvailable(nTPAMargin5GDto.getCashAvailable())
.totalTdBalance(nTPAMargin5GDto.getTotalTdBalance())
.totalTdLongMktValue(nTPAMargin5GDto.getTotalTdLongMktValue())
.totalTdShortMktValue(nTPAMargin5GDto.getTotalTdShortMktValue())
.totalOptionLongMktValue(nTPAMargin5GDto.getTotalOptionLongMktValue())
.totalOptionShortMktValue(nTPAMargin5GDto.getTotalOptionShortMktValue())
.totalSdBalance(nTPAMargin5GDto.getTotalSdBalance())
.totalSdLongMktValue(nTPAMargin5GDto.getTotalSdLongMktValue())
.totalSdShortMktValue(nTPAMargin5GDto.getTotalSdShortMktValue())
.totalAssessedBalance(nTPAMargin5GDto.getTotalAssessedBalance())
.totalInterestAccrual(nTPAMargin5GDto.getTotalInterestAccrual())
.type(nTPAMargin5GDto.getType())
.tdBalance(nTPAMargin5GDto.getTdBalance())
.tdMarketValLng(nTPAMargin5GDto.getTdMarketValLng())
.tdMarketValShrt(nTPAMargin5GDto.getTdMarketValShrt())
.sdBalance(nTPAMargin5GDto.getSdBalance())
.sdMarketValue(nTPAMargin5GDto.getSdMarketValue())
.legalEntityId(nTPAMargin5GDto.getLegalEntityId())
.ntpaProductId(nTPAMargin5GDto.getNtpaProductId())
.id(nTPAMargin5GDto.getId())
 
.build();
 return null;
	}
    
}
