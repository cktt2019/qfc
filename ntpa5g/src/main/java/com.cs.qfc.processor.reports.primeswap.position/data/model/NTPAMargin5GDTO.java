package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class NTPAMargin5GDTO  {
   
	    private java.sql.Date businessDate ;
   
	    private String runDate ;
   
	    private String account ;
   
	    private String currId ;
   
	    private String liquidatingEquity ;
   
	    private String marginEquity ;
   
	    private String regTRequirements ;
   
	    private String maintRequirements ;
   
	    private String nyseRequirements ;
   
	    private String regtExcessDeficit ;
   
	    private String maintExcessDeficit ;
   
	    private String nyseExcessDeficit ;
   
	    private String sma ;
   
	    private String cashAvailable ;
   
	    private String totalTdBalance ;
   
	    private String totalTdLongMktValue ;
   
	    private String totalTdShortMktValue ;
   
	    private String totalOptionLongMktValue ;
   
	    private String totalOptionShortMktValue ;
   
	    private String totalSdBalance ;
   
	    private String totalSdLongMktValue ;
   
	    private String totalSdShortMktValue ;
   
	    private String totalAssessedBalance ;
   
	    private String totalInterestAccrual ;
   
	    private String type ;
   
	    private String tdBalance ;
   
	    private String tdMarketValLng ;
   
	    private String tdMarketValShrt ;
   
	    private String sdBalance ;
   
	    private String sdMarketValue ;
   
	    private String legalEntityId ;
   
	    private String ntpaProductId ;
   
	    private Integer id ;
    


}
