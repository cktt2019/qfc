package com.cs.qfc.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class MarnaGGovtLawDTO  {
   
	    private java.sql.Date businessDate ;
   
	    private String agmtNo ;
   
	    private String cnid ;
   
	    private String agmtDate ;
   
	    private String counterparty ;
   
	    private String csid ;
   
	    private String agmtType ;
   
	    private String agmtTitle ;
   
	    private String coi ;
   
	    private String governingLaw ;
   
	    private Integer id ;
    


}
