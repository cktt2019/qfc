package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.AgreementTitlesScope;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class AgreementTitlesScopeCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, AgreementTitlesScope> cacheConfiguration (){
     CacheConfiguration<Integer, AgreementTitlesScope> cacheCfg = new CacheConfiguration<Integer, AgreementTitlesScope> ();

     cacheCfg.setName("AgreementTitlesScopeCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     AgreementTitlesScopeCacheJdbcPojoStoreFactory factory = new AgreementTitlesScopeCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("AgreementTitlesScopeCache");
     jdbcType.setDatabaseTable("AgreementTitlesScope");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(AgreementTitlesScope.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"count",Integer.class,"count"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.INTEGER,"count",Integer.class,"count"),
   	  new JdbcTypeField(Types.VARCHAR,"agreement_title",String.class,"agreementTitle"),
   	  new JdbcTypeField(Types.VARCHAR,"agreement_type",String.class,"agreementType"),
   	  new JdbcTypeField(Types.VARCHAR,"in_scope",Boolean.class,"inScope")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class AgreementTitlesScopeCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, AgreementTitlesScope> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, AgreementTitlesScope> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
