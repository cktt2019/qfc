package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.NTPACustodyPosition;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class NTPACustodyPositionCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, NTPACustodyPosition> cacheConfiguration (){
     CacheConfiguration<Integer, NTPACustodyPosition> cacheCfg = new CacheConfiguration<Integer, NTPACustodyPosition> ();

     cacheCfg.setName("NTPACustodyPositionCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     NTPACustodyPositionCacheJdbcPojoStoreFactory factory = new NTPACustodyPositionCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("NTPACustodyPositionCache");
     jdbcType.setDatabaseTable("NTPACustodyPosition");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(NTPACustodyPosition.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.DATE,"business_date",java.sql.Date.class,"businessDate"),
   	  new JdbcTypeField(Types.VARCHAR,"comp_id_c",String.class,"compIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"p_id_c",String.class,"pIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"c_ac_id_c",String.class,"cAcIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"c_ac_setlmt_typ_c",String.class,"cAcSetlmtTypC"),
   	  new JdbcTypeField(Types.VARCHAR,"cur_id_c",String.class,"curIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"pnb_pos_d",String.class,"pnbPosD"),
   	  new JdbcTypeField(Types.VARCHAR,"t_natv_cur_id_c",String.class,"tNatvCurIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"t_base_cur_id_c",String.class,"tBaseCurIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"t_con_cur_id_c",String.class,"tConCurIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"pnb_mkt_val_m",String.class,"pnbMktValM"),
   	  new JdbcTypeField(Types.VARCHAR,"pnb_mkt_val_base_m",String.class,"pnbMktValBaseM"),
   	  new JdbcTypeField(Types.VARCHAR,"pnb_mkt_val_con_m",String.class,"pnbMktValConM"),
   	  new JdbcTypeField(Types.VARCHAR,"p_px_cl_m",String.class,"pPxClM"),
   	  new JdbcTypeField(Types.VARCHAR,"pnb_actvy_last_id",String.class,"pnbActvyLastId"),
   	  new JdbcTypeField(Types.VARCHAR,"c_id_c",String.class,"cIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"c_n",String.class,"cN"),
   	  new JdbcTypeField(Types.VARCHAR,"c_ac_typ_c",String.class,"cAcTypC"),
   	  new JdbcTypeField(Types.VARCHAR,"c_ac_ef_d",String.class,"cAcEfD"),
   	  new JdbcTypeField(Types.VARCHAR,"p_maty_d",String.class,"pMatyD"),
   	  new JdbcTypeField(Types.VARCHAR,"p_std_t",String.class,"pStdT"),
   	  new JdbcTypeField(Types.VARCHAR,"p_isin_id_c",String.class,"pIsinIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"p_sedol_id_c",String.class,"pSedolIdC"),
   	  new JdbcTypeField(Types.VARCHAR,"p_file_typ_c",String.class,"pFileTypC"),
   	  new JdbcTypeField(Types.VARCHAR,"pnb_acct_typ_c",String.class,"pnbAcctTypC"),
   	  new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class NTPACustodyPositionCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, NTPACustodyPosition> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, NTPACustodyPosition> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
