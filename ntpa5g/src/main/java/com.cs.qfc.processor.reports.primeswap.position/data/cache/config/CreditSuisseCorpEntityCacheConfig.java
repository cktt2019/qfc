package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.CreditSuisseCorpEntity;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class CreditSuisseCorpEntityCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, CreditSuisseCorpEntity> cacheConfiguration (){
     CacheConfiguration<Integer, CreditSuisseCorpEntity> cacheCfg = new CacheConfiguration<Integer, CreditSuisseCorpEntity> ();

     cacheCfg.setName("CreditSuisseCorpEntityCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     CreditSuisseCorpEntityCacheJdbcPojoStoreFactory factory = new CreditSuisseCorpEntityCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("CreditSuisseCorpEntityCache");
     jdbcType.setDatabaseTable("CreditSuisseCorpEntity");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(CreditSuisseCorpEntity.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"count",Integer.class,"count"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.INTEGER,"count",Integer.class,"count"),
   	  new JdbcTypeField(Types.VARCHAR,"legal_name_of_entity",String.class,"legalNameOfEntity"),
   	  new JdbcTypeField(Types.VARCHAR,"lei",String.class,"lei"),
   	  new JdbcTypeField(Types.VARCHAR,"immediate_parent_name",String.class,"immediateParentName"),
   	  new JdbcTypeField(Types.VARCHAR,"immediate_parent_lei",String.class,"immediateParentLei"),
   	  new JdbcTypeField(Types.VARCHAR,"percentage_ownership",String.class,"percentageOwnership"),
   	  new JdbcTypeField(Types.VARCHAR,"entity_type",String.class,"entityType"),
   	  new JdbcTypeField(Types.VARCHAR,"domicile",String.class,"domicile"),
   	  new JdbcTypeField(Types.VARCHAR,"jurisdiction_incorp",String.class,"jurisdictionIncorp")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class CreditSuisseCorpEntityCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, CreditSuisseCorpEntity> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, CreditSuisseCorpEntity> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
