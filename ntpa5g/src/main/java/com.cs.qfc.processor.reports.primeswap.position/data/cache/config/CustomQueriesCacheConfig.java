package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.CustomQueries;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class CustomQueriesCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<String, CustomQueries> cacheConfiguration (){
     CacheConfiguration<String, CustomQueries> cacheCfg = new CacheConfiguration<String, CustomQueries> ();

     cacheCfg.setName("CustomQueriesCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     CustomQueriesCacheJdbcPojoStoreFactory factory = new CustomQueriesCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("CustomQueriesCache");
     jdbcType.setDatabaseTable("CustomQueries");
     jdbcType.setKeyType(String.class);
     jdbcType.setValueType(CustomQueries.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.VARCHAR,"criteria_name",String.class,"criteriaName"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.VARCHAR,"entity_name",String.class,"entityName"),
   	  new JdbcTypeField(Types.VARCHAR,"criteria_name",String.class,"criteriaName"),
   	  new JdbcTypeField(Types.VARCHAR,"criteria_description",String.class,"criteriaDescription"),
   	  new JdbcTypeField(Types.VARCHAR,"criteria_content",String.class,"criteriaContent")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class CustomQueriesCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<String, CustomQueries> implements Serializable {
           @Override
           public CacheJdbcPojoStore<String, CustomQueries> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
