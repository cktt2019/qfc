package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.NTPAMargin5G;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class NTPAMargin5GCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, NTPAMargin5G> cacheConfiguration (){
     CacheConfiguration<Integer, NTPAMargin5G> cacheCfg = new CacheConfiguration<Integer, NTPAMargin5G> ();

     cacheCfg.setName("NTPAMargin5GCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     NTPAMargin5GCacheJdbcPojoStoreFactory factory = new NTPAMargin5GCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("NTPAMargin5GCache");
     jdbcType.setDatabaseTable("NTPAMargin5G");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(NTPAMargin5G.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.DATE,"business_date",java.sql.Date.class,"businessDate"),
   	  new JdbcTypeField(Types.VARCHAR,"run_date",String.class,"runDate"),
   	  new JdbcTypeField(Types.VARCHAR,"account",String.class,"account"),
   	  new JdbcTypeField(Types.VARCHAR,"curr_id",String.class,"currId"),
   	  new JdbcTypeField(Types.VARCHAR,"liquidating_equity",String.class,"liquidatingEquity"),
   	  new JdbcTypeField(Types.VARCHAR,"margin_equity",String.class,"marginEquity"),
   	  new JdbcTypeField(Types.VARCHAR,"reg_t_requirements",String.class,"regTRequirements"),
   	  new JdbcTypeField(Types.VARCHAR,"maint_requirements",String.class,"maintRequirements"),
   	  new JdbcTypeField(Types.VARCHAR,"nyse_requirements",String.class,"nyseRequirements"),
   	  new JdbcTypeField(Types.VARCHAR,"regt_excess_deficit",String.class,"regtExcessDeficit"),
   	  new JdbcTypeField(Types.VARCHAR,"maint_excess_deficit",String.class,"maintExcessDeficit"),
   	  new JdbcTypeField(Types.VARCHAR,"nyse_excess_deficit",String.class,"nyseExcessDeficit"),
   	  new JdbcTypeField(Types.VARCHAR,"sma",String.class,"sma"),
   	  new JdbcTypeField(Types.VARCHAR,"cash_available",String.class,"cashAvailable"),
   	  new JdbcTypeField(Types.VARCHAR,"total_td_balance",String.class,"totalTdBalance"),
   	  new JdbcTypeField(Types.VARCHAR,"total_td_long_mkt_value",String.class,"totalTdLongMktValue"),
   	  new JdbcTypeField(Types.VARCHAR,"total_td_short_mkt_value",String.class,"totalTdShortMktValue"),
   	  new JdbcTypeField(Types.VARCHAR,"total_option_long_mkt_value",String.class,"totalOptionLongMktValue"),
   	  new JdbcTypeField(Types.VARCHAR,"total_option_short_mkt_value",String.class,"totalOptionShortMktValue"),
   	  new JdbcTypeField(Types.VARCHAR,"total_sd_balance",String.class,"totalSdBalance"),
   	  new JdbcTypeField(Types.VARCHAR,"total_sd_long_mkt_value",String.class,"totalSdLongMktValue"),
   	  new JdbcTypeField(Types.VARCHAR,"total_sd_short_mkt_value",String.class,"totalSdShortMktValue"),
   	  new JdbcTypeField(Types.VARCHAR,"total_assessed_balance",String.class,"totalAssessedBalance"),
   	  new JdbcTypeField(Types.VARCHAR,"total_interest_accrual",String.class,"totalInterestAccrual"),
   	  new JdbcTypeField(Types.VARCHAR,"type",String.class,"type"),
   	  new JdbcTypeField(Types.VARCHAR,"td_balance",String.class,"tdBalance"),
   	  new JdbcTypeField(Types.VARCHAR,"td_market_val_lng",String.class,"tdMarketValLng"),
   	  new JdbcTypeField(Types.VARCHAR,"td_market_val_shrt",String.class,"tdMarketValShrt"),
   	  new JdbcTypeField(Types.VARCHAR,"sd_balance",String.class,"sdBalance"),
   	  new JdbcTypeField(Types.VARCHAR,"sd_market_value",String.class,"sdMarketValue"),
   	  new JdbcTypeField(Types.VARCHAR,"legal_entity_id",String.class,"legalEntityId"),
   	  new JdbcTypeField(Types.VARCHAR,"ntpa_product_id",String.class,"ntpaProductId"),
   	  new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class NTPAMargin5GCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, NTPAMargin5G> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, NTPAMargin5G> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
