package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.MarnaGGovtLaw;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class MarnaGGovtLawCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, MarnaGGovtLaw> cacheConfiguration (){
     CacheConfiguration<Integer, MarnaGGovtLaw> cacheCfg = new CacheConfiguration<Integer, MarnaGGovtLaw> ();

     cacheCfg.setName("MarnaGGovtLawCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     MarnaGGovtLawCacheJdbcPojoStoreFactory factory = new MarnaGGovtLawCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("MarnaGGovtLawCache");
     jdbcType.setDatabaseTable("MarnaGGovtLaw");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(MarnaGGovtLaw.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.DATE,"business_date",java.sql.Date.class,"businessDate"),
   	  new JdbcTypeField(Types.VARCHAR,"agmt_no",String.class,"agmtNo"),
   	  new JdbcTypeField(Types.VARCHAR,"cnid",String.class,"cnid"),
   	  new JdbcTypeField(Types.VARCHAR,"agmt_date",String.class,"agmtDate"),
   	  new JdbcTypeField(Types.VARCHAR,"counterparty",String.class,"counterparty"),
   	  new JdbcTypeField(Types.VARCHAR,"csid",String.class,"csid"),
   	  new JdbcTypeField(Types.VARCHAR,"agmt_type",String.class,"agmtType"),
   	  new JdbcTypeField(Types.VARCHAR,"agmt_title",String.class,"agmtTitle"),
   	  new JdbcTypeField(Types.VARCHAR,"coi",String.class,"coi"),
   	  new JdbcTypeField(Types.VARCHAR,"governing_law",String.class,"governingLaw"),
   	  new JdbcTypeField(Types.INTEGER,"id",Integer.class,"id")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class MarnaGGovtLawCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, MarnaGGovtLaw> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, MarnaGGovtLaw> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
