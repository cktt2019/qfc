package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "MarnaGGovtLaw" )
	@Entity
	public class MarnaGGovtLaw implements Serializable {
	
	  @Column(name = "business_date"  )
	private java.sql.Date businessDate ;
 
	
	  @Column(name = "agmt_no"  )
	private String agmtNo ;
 
	
	  @Column(name = "cnid"  )
	private String cnid ;
 
	
	  @Column(name = "agmt_date"  )
	private String agmtDate ;
 
	
	  @Column(name = "counterparty"  )
	private String counterparty ;
 
	
	  @Column(name = "csid"  )
	private String csid ;
 
	
	  @Column(name = "agmt_type"  )
	private String agmtType ;
 
	
	  @Column(name = "agmt_title"  )
	private String agmtTitle ;
 
	
	  @Column(name = "coi"  )
	private String coi ;
 
	
	  @Column(name = "governing_law"  )
	private String governingLaw ;
 
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marnaggovtlaw_seq")
	    @SequenceGenerator(name = "marnaggovtlaw_seq", sequenceName = "marnaggovtlaw_seq")
	  @Column(name = "id"  )
	private Integer id ;
 
   
   
    


      
      
}
