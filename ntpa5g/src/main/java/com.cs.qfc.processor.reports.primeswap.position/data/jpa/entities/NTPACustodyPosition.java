package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "NTPACustodyPosition" )
	@Entity
	public class NTPACustodyPosition implements Serializable {
	
	  @Column(name = "business_date"  )
	private java.sql.Date businessDate ;
 
	
	  @Column(name = "comp_id_c"  )
	private String compIdC ;
 
	
	  @Column(name = "p_id_c"  )
	private String pIdC ;
 
	
	  @Column(name = "c_ac_id_c"  )
	private String cAcIdC ;
 
	
	  @Column(name = "c_ac_setlmt_typ_c"  )
	private String cAcSetlmtTypC ;
 
	
	  @Column(name = "cur_id_c"  )
	private String curIdC ;
 
	
	  @Column(name = "pnb_pos_d"  )
	private String pnbPosD ;
 
	
	  @Column(name = "t_natv_cur_id_c"  )
	private String tNatvCurIdC ;
 
	
	  @Column(name = "t_base_cur_id_c"  )
	private String tBaseCurIdC ;
 
	
	  @Column(name = "t_con_cur_id_c"  )
	private String tConCurIdC ;
 
	
	  @Column(name = "pnb_mkt_val_m"  )
	private String pnbMktValM ;
 
	
	  @Column(name = "pnb_mkt_val_base_m"  )
	private String pnbMktValBaseM ;
 
	
	  @Column(name = "pnb_mkt_val_con_m"  )
	private String pnbMktValConM ;
 
	
	  @Column(name = "p_px_cl_m"  )
	private String pPxClM ;
 
	
	  @Column(name = "pnb_actvy_last_id"  )
	private String pnbActvyLastId ;
 
	
	  @Column(name = "c_id_c"  )
	private String cIdC ;
 
	
	  @Column(name = "c_n"  )
	private String cN ;
 
	
	  @Column(name = "c_ac_typ_c"  )
	private String cAcTypC ;
 
	
	  @Column(name = "c_ac_ef_d"  )
	private String cAcEfD ;
 
	
	  @Column(name = "p_maty_d"  )
	private String pMatyD ;
 
	
	  @Column(name = "p_std_t"  )
	private String pStdT ;
 
	
	  @Column(name = "p_isin_id_c"  )
	private String pIsinIdC ;
 
	
	  @Column(name = "p_sedol_id_c"  )
	private String pSedolIdC ;
 
	
	  @Column(name = "p_file_typ_c"  )
	private String pFileTypC ;
 
	
	  @Column(name = "pnb_acct_typ_c"  )
	private String pnbAcctTypC ;
 
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ntpacustodyposition_seq")
	    @SequenceGenerator(name = "ntpacustodyposition_seq", sequenceName = "ntpacustodyposition_seq")
	  @Column(name = "id"  )
	private Integer id ;
 
   
   
    


      
      
}
