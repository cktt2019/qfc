package com.cs.qfc.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@Table(name = "CustomQueries" )
	@Entity
	public class CustomQueries implements Serializable {
	
	  @Column(name = "entity_name"  )
	private String entityName ;
 
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customqueries_seq")
	    @SequenceGenerator(name = "customqueries_seq", sequenceName = "customqueries_seq")
	  @Column(name = "criteria_name"  )
	private String criteriaName ;
 
	
	  @Column(name = "criteria_description"  )
	private String criteriaDescription ;
 
	
	  @Column(name = "criteria_content"  )
	private String criteriaContent ;
 
   
   
    


      
      
}
