package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.CreditSuisseCorpEntity;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CreditSuisseCorpEntityDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link CreditSuisseCorpEntity} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedCreditSuisseCorpEntityRepository {
	
	public Collection<CreditSuisseCorpEntityDTO> findByCustomCriteria (SearchInfoDTO info);

	}







