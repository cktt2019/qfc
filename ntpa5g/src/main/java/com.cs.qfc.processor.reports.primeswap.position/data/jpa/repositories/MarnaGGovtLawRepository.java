package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.MarnaGGovtLaw;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 *  Service interface with operations for {@link MarnaGGovtLaw} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface MarnaGGovtLawRepository extends JpaRepository<MarnaGGovtLaw, Integer>{


	public List<MarnaGGovtLaw> findByBusinessDate (java.sql.Date businessDate);
	public List<MarnaGGovtLaw> findByAgmtNo (String agmtNo);
	public List<MarnaGGovtLaw> findByCnid (String cnid);
	public List<MarnaGGovtLaw> findByAgmtDate (String agmtDate);
	public List<MarnaGGovtLaw> findByCounterparty (String counterparty);
	public List<MarnaGGovtLaw> findByCsid (String csid);
	public List<MarnaGGovtLaw> findByAgmtType (String agmtType);
	public List<MarnaGGovtLaw> findByAgmtTitle (String agmtTitle);
	public List<MarnaGGovtLaw> findByCoi (String coi);
	public List<MarnaGGovtLaw> findByGoverningLaw (String governingLaw);
	

	
	public Page<MarnaGGovtLaw> findAll (Pageable page);
	
	

	

	
	}







