package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.CustomQueries;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link CustomQueries} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface CustomQueriesRepository extends JpaRepository<CustomQueries, String>{


	public List<CustomQueries> findByEntityName (String entityName);
	public List<CustomQueries> findByCriteriaDescription (String criteriaDescription);
	public List<CustomQueries> findByCriteriaContent (String criteriaContent);
	

	
	public Page<CustomQueries> findAll (Pageable page);
	
	

	

	
	}







