package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.CustomQueries;
import com.cs.qfc.data.metadata.CustomQueriesMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.CustomQueriesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link CustomQueries} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedCustomQueriesRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedCustomQueriesRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<CustomQueriesDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + CustomQueriesMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new CustomQueriesMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new CustomQueriesMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new CustomQueriesMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<CustomQueriesDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new CustomQueriesRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			CustomQueriesMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class CustomQueriesRowMapper implements RowMapper<CustomQueriesDTO> {
		@Override
		public CustomQueriesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			CustomQueriesDTO ret = new CustomQueriesDTO();


			
           ret.setEntityName (rs.getString("entity_name"));
           ret.setCriteriaName (rs.getString("criteria_name"));
           ret.setCriteriaDescription (rs.getString("criteria_description"));
           ret.setCriteriaContent (rs.getString("criteria_content"));

			return ret;
		}
	}

}
