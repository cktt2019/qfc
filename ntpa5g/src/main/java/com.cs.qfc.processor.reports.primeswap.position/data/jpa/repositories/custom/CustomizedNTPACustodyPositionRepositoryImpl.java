package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.NTPACustodyPosition;
import com.cs.qfc.data.metadata.NTPACustodyPositionMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.NTPACustodyPositionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link NTPACustodyPosition} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedNTPACustodyPositionRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedNTPACustodyPositionRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<NTPACustodyPositionDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + NTPACustodyPositionMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new NTPACustodyPositionMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new NTPACustodyPositionMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new NTPACustodyPositionMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<NTPACustodyPositionDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new NTPACustodyPositionRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			NTPACustodyPositionMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class NTPACustodyPositionRowMapper implements RowMapper<NTPACustodyPositionDTO> {
		@Override
		public NTPACustodyPositionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			NTPACustodyPositionDTO ret = new NTPACustodyPositionDTO();


			
           ret.setBusinessDate (rs.getDate("business_date"));
           ret.setCompIdC (rs.getString("comp_id_c"));
           ret.setPIdC (rs.getString("p_id_c"));
           ret.setCAcIdC (rs.getString("c_ac_id_c"));
           ret.setCAcSetlmtTypC (rs.getString("c_ac_setlmt_typ_c"));
           ret.setCurIdC (rs.getString("cur_id_c"));
           ret.setPnbPosD (rs.getString("pnb_pos_d"));
           ret.setTNatvCurIdC (rs.getString("t_natv_cur_id_c"));
           ret.setTBaseCurIdC (rs.getString("t_base_cur_id_c"));
           ret.setTConCurIdC (rs.getString("t_con_cur_id_c"));
           ret.setPnbMktValM (rs.getString("pnb_mkt_val_m"));
           ret.setPnbMktValBaseM (rs.getString("pnb_mkt_val_base_m"));
           ret.setPnbMktValConM (rs.getString("pnb_mkt_val_con_m"));
           ret.setPPxClM (rs.getString("p_px_cl_m"));
           ret.setPnbActvyLastId (rs.getString("pnb_actvy_last_id"));
           ret.setCIdC (rs.getString("c_id_c"));
           ret.setCN (rs.getString("c_n"));
           ret.setCAcTypC (rs.getString("c_ac_typ_c"));
           ret.setCAcEfD (rs.getString("c_ac_ef_d"));
           ret.setPMatyD (rs.getString("p_maty_d"));
           ret.setPStdT (rs.getString("p_std_t"));
           ret.setPIsinIdC (rs.getString("p_isin_id_c"));
           ret.setPSedolIdC (rs.getString("p_sedol_id_c"));
           ret.setPFileTypC (rs.getString("p_file_typ_c"));
           ret.setPnbAcctTypC (rs.getString("pnb_acct_typ_c"));
           ret.setId (rs.getInt("id"));

			return ret;
		}
	}

}
