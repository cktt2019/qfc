package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.CreditSuisseCorpEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link CreditSuisseCorpEntity} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface CreditSuisseCorpEntityRepository extends JpaRepository<CreditSuisseCorpEntity, Integer>{


	public List<CreditSuisseCorpEntity> findByLegalNameOfEntity (String legalNameOfEntity);
	public List<CreditSuisseCorpEntity> findByLei (String lei);
	public List<CreditSuisseCorpEntity> findByImmediateParentName (String immediateParentName);
	public List<CreditSuisseCorpEntity> findByImmediateParentLei (String immediateParentLei);
	public List<CreditSuisseCorpEntity> findByPercentageOwnership (String percentageOwnership);
	public List<CreditSuisseCorpEntity> findByEntityType (String entityType);
	public List<CreditSuisseCorpEntity> findByDomicile (String domicile);
	public List<CreditSuisseCorpEntity> findByJurisdictionIncorp (String jurisdictionIncorp);
	

	
	public Page<CreditSuisseCorpEntity> findAll (Pageable page);
	
	

	

	
	}







