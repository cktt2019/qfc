package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.NTPACustodyPosition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 *  Service interface with operations for {@link NTPACustodyPosition} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface NTPACustodyPositionRepository extends JpaRepository<NTPACustodyPosition, Integer>{


	public List<NTPACustodyPosition> findByBusinessDate (java.sql.Date businessDate);
	public List<NTPACustodyPosition> findByCompIdC (String compIdC);
	public List<NTPACustodyPosition> findByPIdC (String pIdC);
	public List<NTPACustodyPosition> findByCAcIdC (String cAcIdC);
	public List<NTPACustodyPosition> findByCAcSetlmtTypC (String cAcSetlmtTypC);
	public List<NTPACustodyPosition> findByCurIdC (String curIdC);
	public List<NTPACustodyPosition> findByPnbPosD (String pnbPosD);
	public List<NTPACustodyPosition> findByTNatvCurIdC (String tNatvCurIdC);
	public List<NTPACustodyPosition> findByTBaseCurIdC (String tBaseCurIdC);
	public List<NTPACustodyPosition> findByTConCurIdC (String tConCurIdC);
	public List<NTPACustodyPosition> findByPnbMktValM (String pnbMktValM);
	public List<NTPACustodyPosition> findByPnbMktValBaseM (String pnbMktValBaseM);
	public List<NTPACustodyPosition> findByPnbMktValConM (String pnbMktValConM);
	public List<NTPACustodyPosition> findByPPxClM (String pPxClM);
	public List<NTPACustodyPosition> findByPnbActvyLastId (String pnbActvyLastId);
	public List<NTPACustodyPosition> findByCIdC (String cIdC);
	public List<NTPACustodyPosition> findByCN (String cN);
	public List<NTPACustodyPosition> findByCAcTypC (String cAcTypC);
	public List<NTPACustodyPosition> findByCAcEfD (String cAcEfD);
	public List<NTPACustodyPosition> findByPMatyD (String pMatyD);
	public List<NTPACustodyPosition> findByPStdT (String pStdT);
	public List<NTPACustodyPosition> findByPIsinIdC (String pIsinIdC);
	public List<NTPACustodyPosition> findByPSedolIdC (String pSedolIdC);
	public List<NTPACustodyPosition> findByPFileTypC (String pFileTypC);
	public List<NTPACustodyPosition> findByPnbAcctTypC (String pnbAcctTypC);
	

	
	public Page<NTPACustodyPosition> findAll (Pageable page);
	
	

	

	
	}







