package com.cs.qfc.data.jpa.repositories.custom;


import com.cs.qfc.data.jpa.entities.AgreementTitlesScope;
import com.cs.qfc.data.metadata.AgreementTitlesScopeMetaData;
import com.cs.qfc.data.metadata.core.PaginationInstructionDTO;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.metadata.core.SortInstructionDTO;
import com.cs.qfc.data.model.AgreementTitlesScopeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link AgreementTitlesScope} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedAgreementTitlesScopeRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedAgreementTitlesScopeRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<AgreementTitlesScopeDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + AgreementTitlesScopeMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new AgreementTitlesScopeMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new AgreementTitlesScopeMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new AgreementTitlesScopeMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<AgreementTitlesScopeDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new AgreementTitlesScopeRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			AgreementTitlesScopeMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class AgreementTitlesScopeRowMapper implements RowMapper<AgreementTitlesScopeDTO> {
		@Override
		public AgreementTitlesScopeDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			AgreementTitlesScopeDTO ret = new AgreementTitlesScopeDTO();


			
           ret.setCount (rs.getInt("count"));
           ret.setAgreementTitle (rs.getString("agreement_title"));
           ret.setAgreementType (rs.getString("agreement_type"));
           ret.setInScope (rs.getBoolean("in_scope"));

			return ret;
		}
	}

}
