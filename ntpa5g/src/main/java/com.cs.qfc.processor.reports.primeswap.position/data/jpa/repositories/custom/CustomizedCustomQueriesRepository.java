package com.cs.qfc.data.jpa.repositories.custom;

import com.cs.qfc.data.jpa.entities.CustomQueries;
import com.cs.qfc.data.metadata.core.SearchInfoDTO;
import com.cs.qfc.data.model.CustomQueriesDTO;

import java.util.Collection;

/**
 *  Service interface with operations for {@link CustomQueries} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedCustomQueriesRepository {
	
	public Collection<CustomQueriesDTO> findByCustomCriteria (SearchInfoDTO info);

	}







