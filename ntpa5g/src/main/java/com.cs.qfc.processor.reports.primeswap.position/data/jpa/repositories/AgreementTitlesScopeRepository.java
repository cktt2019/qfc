package com.cs.qfc.data.jpa.repositories;

import com.cs.qfc.data.jpa.entities.AgreementTitlesScope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  Service interface with operations for {@link AgreementTitlesScope} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface AgreementTitlesScopeRepository extends JpaRepository<AgreementTitlesScope, Integer>{


	public List<AgreementTitlesScope> findByAgreementTitle (String agreementTitle);
	public List<AgreementTitlesScope> findByAgreementType (String agreementType);
	public List<AgreementTitlesScope> findByInScope (Boolean inScope);
	

	
	public Page<AgreementTitlesScope> findAll (Pageable page);
	
	

	

	
	}







