package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class NTPACustodyPositionMetaData  implements MetaData {
	
	private static final String BUSINESSDATE = "businessDate";
	private static final String COMPIDC = "compIdC";
	private static final String PIDC = "pIdC";
	private static final String CACIDC = "cAcIdC";
	private static final String CACSETLMTTYPC = "cAcSetlmtTypC";
	private static final String CURIDC = "curIdC";
	private static final String PNBPOSD = "pnbPosD";
	private static final String TNATVCURIDC = "tNatvCurIdC";
	private static final String TBASECURIDC = "tBaseCurIdC";
	private static final String TCONCURIDC = "tConCurIdC";
	private static final String PNBMKTVALM = "pnbMktValM";
	private static final String PNBMKTVALBASEM = "pnbMktValBaseM";
	private static final String PNBMKTVALCONM = "pnbMktValConM";
	private static final String PPXCLM = "pPxClM";
	private static final String PNBACTVYLASTID = "pnbActvyLastId";
	private static final String CIDC = "cIdC";
	private static final String CN = "cN";
	private static final String CACTYPC = "cAcTypC";
	private static final String CACEFD = "cAcEfD";
	private static final String PMATYD = "pMatyD";
	private static final String PSTDT = "pStdT";
	private static final String PISINIDC = "pIsinIdC";
	private static final String PSEDOLIDC = "pSedolIdC";
	private static final String PFILETYPC = "pFileTypC";
	private static final String PNBACCTTYPC = "pnbAcctTypC";
	private static final String ID = "id";

   
     public static final String TABLE = "NTPACustodyPosition";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case BUSINESSDATE : return "business_date";
	case COMPIDC : return "comp_id_c";
	case PIDC : return "p_id_c";
	case CACIDC : return "c_ac_id_c";
	case CACSETLMTTYPC : return "c_ac_setlmt_typ_c";
	case CURIDC : return "cur_id_c";
	case PNBPOSD : return "pnb_pos_d";
	case TNATVCURIDC : return "t_natv_cur_id_c";
	case TBASECURIDC : return "t_base_cur_id_c";
	case TCONCURIDC : return "t_con_cur_id_c";
	case PNBMKTVALM : return "pnb_mkt_val_m";
	case PNBMKTVALBASEM : return "pnb_mkt_val_base_m";
	case PNBMKTVALCONM : return "pnb_mkt_val_con_m";
	case PPXCLM : return "p_px_cl_m";
	case PNBACTVYLASTID : return "pnb_actvy_last_id";
	case CIDC : return "c_id_c";
	case CN : return "c_n";
	case CACTYPC : return "c_ac_typ_c";
	case CACEFD : return "c_ac_ef_d";
	case PMATYD : return "p_maty_d";
	case PSTDT : return "p_std_t";
	case PISINIDC : return "p_isin_id_c";
	case PSEDOLIDC : return "p_sedol_id_c";
	case PFILETYPC : return "p_file_typ_c";
	case PNBACCTTYPC : return "pnb_acct_typ_c";
	case ID : return "id";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case BUSINESSDATE : return "java.sql.Date";
	case COMPIDC : return "String";
	case PIDC : return "String";
	case CACIDC : return "String";
	case CACSETLMTTYPC : return "String";
	case CURIDC : return "String";
	case PNBPOSD : return "String";
	case TNATVCURIDC : return "String";
	case TBASECURIDC : return "String";
	case TCONCURIDC : return "String";
	case PNBMKTVALM : return "String";
	case PNBMKTVALBASEM : return "String";
	case PNBMKTVALCONM : return "String";
	case PPXCLM : return "String";
	case PNBACTVYLASTID : return "String";
	case CIDC : return "String";
	case CN : return "String";
	case CACTYPC : return "String";
	case CACEFD : return "String";
	case PMATYD : return "String";
	case PSTDT : return "String";
	case PISINIDC : return "String";
	case PSEDOLIDC : return "String";
	case PFILETYPC : return "String";
	case PNBACCTTYPC : return "String";
	case ID : return "Integer";
		}
		return null;
	}
	

}
