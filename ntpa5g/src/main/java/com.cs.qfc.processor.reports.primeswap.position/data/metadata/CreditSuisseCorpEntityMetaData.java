package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class CreditSuisseCorpEntityMetaData  implements MetaData {
	
	private static final String COUNT = "count";
	private static final String LEGALNAMEOFENTITY = "legalNameOfEntity";
	private static final String LEI = "lei";
	private static final String IMMEDIATEPARENTNAME = "immediateParentName";
	private static final String IMMEDIATEPARENTLEI = "immediateParentLei";
	private static final String PERCENTAGEOWNERSHIP = "percentageOwnership";
	private static final String ENTITYTYPE = "entityType";
	private static final String DOMICILE = "domicile";
	private static final String JURISDICTIONINCORP = "jurisdictionIncorp";

   
     public static final String TABLE = "CreditSuisseCorpEntity";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case COUNT : return "count";
	case LEGALNAMEOFENTITY : return "legal_name_of_entity";
	case LEI : return "lei";
	case IMMEDIATEPARENTNAME : return "immediate_parent_name";
	case IMMEDIATEPARENTLEI : return "immediate_parent_lei";
	case PERCENTAGEOWNERSHIP : return "percentage_ownership";
	case ENTITYTYPE : return "entity_type";
	case DOMICILE : return "domicile";
	case JURISDICTIONINCORP : return "jurisdiction_incorp";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case COUNT : return "Integer";
	case LEGALNAMEOFENTITY : return "String";
	case LEI : return "String";
	case IMMEDIATEPARENTNAME : return "String";
	case IMMEDIATEPARENTLEI : return "String";
	case PERCENTAGEOWNERSHIP : return "String";
	case ENTITYTYPE : return "String";
	case DOMICILE : return "String";
	case JURISDICTIONINCORP : return "String";
		}
		return null;
	}
	

}
