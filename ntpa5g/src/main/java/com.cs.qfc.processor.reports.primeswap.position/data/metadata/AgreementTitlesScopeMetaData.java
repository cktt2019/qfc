package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class AgreementTitlesScopeMetaData  implements MetaData {
	
	private static final String COUNT = "count";
	private static final String AGREEMENTTITLE = "agreementTitle";
	private static final String AGREEMENTTYPE = "agreementType";
	private static final String INSCOPE = "inScope";

   
     public static final String TABLE = "AgreementTitlesScope";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case COUNT : return "count";
	case AGREEMENTTITLE : return "agreement_title";
	case AGREEMENTTYPE : return "agreement_type";
	case INSCOPE : return "in_scope";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case COUNT : return "Integer";
	case AGREEMENTTITLE : return "String";
	case AGREEMENTTYPE : return "String";
	case INSCOPE : return "Boolean";
		}
		return null;
	}
	

}
