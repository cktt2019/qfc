package com.cs.qfc.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class CustomQueriesMetaData  implements MetaData {
	
	private static final String ENTITYNAME = "entityName";
	private static final String CRITERIANAME = "criteriaName";
	private static final String CRITERIADESCRIPTION = "criteriaDescription";
	private static final String CRITERIACONTENT = "criteriaContent";

   
     public static final String TABLE = "CustomQueries";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case ENTITYNAME : return "entity_name";
	case CRITERIANAME : return "criteria_name";
	case CRITERIADESCRIPTION : return "criteria_description";
	case CRITERIACONTENT : return "criteria_content";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case ENTITYNAME : return "String";
	case CRITERIANAME : return "String";
	case CRITERIADESCRIPTION : return "String";
	case CRITERIACONTENT : return "String";
		}
		return null;
	}
	

}
