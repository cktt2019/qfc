package com.cs.qfc.data.cache.config;

import com.cs.qfc.data.model.Department;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








    @Component
	public class DepartmentCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<String, Department> cacheConfiguration (){
     CacheConfiguration<String, Department> cacheCfg = new CacheConfiguration<String, Department> ();

     cacheCfg.setName("DepartmentCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     DepartmentCacheJdbcPojoStoreFactory factory = new DepartmentCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("DepartmentCache");
     jdbcType.setDatabaseTable("Department");
     jdbcType.setKeyType(String.class);
     jdbcType.setValueType(Department.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.VARCHAR,"department_id ",String.class,"departmentId "));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.VARCHAR,"department_id",String.class,"departmentId"),
   	  new JdbcTypeField(Types.VARCHAR,"business_controller_id",String.class,"businessControllerId"),
   	  new JdbcTypeField(Types.VARCHAR,"controller_id",String.class,"controllerId"),
   	  new JdbcTypeField(Types.VARCHAR,"department_category_code",String.class,"departmentCategoryCode"),
   	  new JdbcTypeField(Types.VARCHAR,"department_category_name",String.class,"departmentCategoryName"),
   	  new JdbcTypeField(Types.VARCHAR,"department_dormant_indicator",String.class,"departmentDormantIndicator"),
   	  new JdbcTypeField(Types.VARCHAR,"department_long_name",String.class,"departmentLongName"),
   	  new JdbcTypeField(Types.VARCHAR,"department_name",String.class,"departmentName"),
   	  new JdbcTypeField(Types.VARCHAR,"department_reconciler_group_name",String.class,"departmentReconcilerGroupName"),
   	  new JdbcTypeField(Types.VARCHAR,"department_status",String.class,"departmentStatus"),
   	  new JdbcTypeField(Types.VARCHAR,"department_trader_id",String.class,"departmentTraderId"),
   	  new JdbcTypeField(Types.DATE,"department_valid_from_date",java.sql.Date.class,"departmentValidFromDate"),
   	  new JdbcTypeField(Types.DATE,"department_valid_to_date",java.sql.Date.class,"departmentValidToDate"),
   	  new JdbcTypeField(Types.VARCHAR,"financial_business_unit_code",String.class,"financialBusinessUnitCode"),
   	  new JdbcTypeField(Types.VARCHAR,"ivr_group",String.class,"ivrGroup"),
   	  new JdbcTypeField(Types.VARCHAR,"l1_supervisor_id",String.class,"l1SupervisorId"),
   	  new JdbcTypeField(Types.VARCHAR,"mis_office_code",String.class,"misOfficeCode"),
   	  new JdbcTypeField(Types.VARCHAR,"mis_office_name",String.class,"misOfficeName"),
   	  new JdbcTypeField(Types.VARCHAR,"owner_l2_id",String.class,"ownerL2Id"),
   	  new JdbcTypeField(Types.VARCHAR,"rec_and_adj_controller_id",String.class,"recAndAdjControllerId"),
   	  new JdbcTypeField(Types.VARCHAR,"rec_and_adj_supervisor_id",String.class,"recAndAdjSupervisorId"),
   	  new JdbcTypeField(Types.VARCHAR,"reconciler_id",String.class,"reconcilerId"),
   	  new JdbcTypeField(Types.VARCHAR,"region_code",String.class,"regionCode"),
   	  new JdbcTypeField(Types.VARCHAR,"region_description",String.class,"regionDescription"),
   	  new JdbcTypeField(Types.VARCHAR,"revenue_type",String.class,"revenueType"),
   	  new JdbcTypeField(Types.VARCHAR,"revenue_type_description",String.class,"revenueTypeDescription"),
   	  new JdbcTypeField(Types.VARCHAR,"risk_portfolio_id",String.class,"riskPortfolioId"),
   	  new JdbcTypeField(Types.VARCHAR,"specialisation_indicator",String.class,"specialisationIndicator"),
   	  new JdbcTypeField(Types.VARCHAR,"tax_category",String.class,"taxCategory"),
   	  new JdbcTypeField(Types.VARCHAR,"volker_rule_description",String.class,"volkerRuleDescription"),
   	  new JdbcTypeField(Types.VARCHAR,"volker_rule_indicator",String.class,"volkerRuleIndicator"),
   	  new JdbcTypeField(Types.VARCHAR,"fx_centrally_maanged_name",String.class,"fxCentrallyMaangedName"),
   	  new JdbcTypeField(Types.VARCHAR,"volcker_sub_division",String.class,"volckerSubDivision"),
   	  new JdbcTypeField(Types.VARCHAR,"volcker_sub_division_description",String.class,"volckerSubDivisionDescription"),
   	  new JdbcTypeField(Types.VARCHAR,"ihc_hcd_flag",String.class,"ihcHcdFlag"),
   	  new JdbcTypeField(Types.VARCHAR,"covered_dept_flag",String.class,"coveredDeptFlag"),
   	  new JdbcTypeField(Types.VARCHAR,"delta_flag",String.class,"deltaFlag"),
   	  new JdbcTypeField(Types.VARCHAR,"remittance_policy_code",String.class,"remittancePolicyCode"),
   	  new JdbcTypeField(Types.VARCHAR,"first_instance_dept_activated",String.class,"firstInstanceDeptActivated"),
   	  new JdbcTypeField(Types.VARCHAR,"first_instance_dept_inactivated",String.class,"firstInstanceDeptInactivated"),
   	  new JdbcTypeField(Types.VARCHAR,"last_instance_dept_activated",String.class,"lastInstanceDeptActivated"),
   	  new JdbcTypeField(Types.VARCHAR,"last_instance_dept_inactivated",String.class,"lastInstanceDeptInactivated"),
   	  new JdbcTypeField(Types.VARCHAR,"rbpnl_flag",String.class,"rbpnlFlag"),
   	  new JdbcTypeField(Types.VARCHAR,"ivr_group_long_name",String.class,"ivrGroupLongName"),
   	  new JdbcTypeField(Types.VARCHAR,"frtb_dept_category",String.class,"frtbDeptCategory")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class DepartmentCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<String, Department> implements Serializable {
           @Override
           public CacheJdbcPojoStore<String, Department> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
