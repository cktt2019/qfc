package idm.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;


import idm.rest.api.endpoints.RestEndpoint;

@Component
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
	    register(MultiPartFeature.class);
		register(RestEndpoint.class);
		register(CorsFilter.class);
	}

}
