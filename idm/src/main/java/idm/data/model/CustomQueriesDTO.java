package idm.data.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;


import java.util.*;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class CustomQueriesDTO  {
	    private String entityName ;
	    private String criteriaName ;
	    private String criteriaDescription ;
	    private String criteriaContent ;
    


  



}
