package idm.data.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;


import java.util.*;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class IdmDocumentVersionDTO  {
	    private Integer idmDocumentId ;
	    private String idmDocumentVersion ;
    


  private IdmDocumentDTO idmDocument1;





}
