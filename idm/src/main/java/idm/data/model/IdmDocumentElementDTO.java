package idm.data.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;


import java.util.*;




/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public class IdmDocumentElementDTO  {
	    private String tradeType ;
	    private String region ;
	    private String underlying ;
	    private String valuationDate ;
	    private String elementType ;
	    private String payload ;
	    private Integer idmDocumentId ;
	    private Integer idmDocumentElementId ;
    


  private IdmDocumentDTO idmDocument0;





}
