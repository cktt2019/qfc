package idm.data.converter;




import java.util.*;
import  idm.data.model.IdmDocumentVersionDTO;
import  idm.data.jpa.entities.IdmDocumentVersion;
import java.math.BigDecimal;

/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class IdmDocumentVersionToDTOConverter  {
	

	

public static IdmDocumentVersionDTO convert(IdmDocumentVersion idmDocumentVersion) {
 if (idmDocumentVersion != null)
  return IdmDocumentVersionDTO.builder().idmDocumentId(idmDocumentVersion.getIdmDocumentId())
.idmDocumentVersion(idmDocumentVersion.getIdmDocumentVersion())
.build();
 return null;
	}
    
}
