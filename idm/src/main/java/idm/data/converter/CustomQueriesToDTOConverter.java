package idm.data.converter;




import java.util.*;
import  idm.data.model.CustomQueriesDTO;
import  idm.data.jpa.entities.CustomQueries;
import java.math.BigDecimal;

/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class CustomQueriesToDTOConverter  {
	

	

public static CustomQueriesDTO convert(CustomQueries customQueries) {
 if (customQueries != null)
  return CustomQueriesDTO.builder().entityName(customQueries.getEntityName())
.criteriaName(customQueries.getCriteriaName())
.criteriaDescription(customQueries.getCriteriaDescription())
.criteriaContent(customQueries.getCriteriaContent())
.build();
 return null;
	}
    
}
