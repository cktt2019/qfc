package idm.data.converter;




import java.util.*;
import  idm.data.model.IdmDocumentDTO;
import  idm.data.jpa.entities.IdmDocument;
import java.math.BigDecimal;

/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class IdmDocumentToDTOConverter  {
	

	

public static IdmDocumentDTO convert(IdmDocument idmDocument) {
 if (idmDocument != null)
  return IdmDocumentDTO.builder().documentId(idmDocument.getDocumentId())
.region(idmDocument.getRegion())
.underlying(idmDocument.getUnderlying())
.tradeType(idmDocument.getTradeType())
.valuationDate(idmDocument.getValuationDate())
.version(idmDocument.getVersion())
.calibrator(idmDocument.getCalibrator())
.extractedUnderlying(idmDocument.getExtractedUnderlying())
.payload(idmDocument.getPayload())
.status(idmDocument.getStatus())
.build();
 return null;
	}
    
}
