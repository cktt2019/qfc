package idm.data.converter;



import java.util.*;
import  idm.data.jpa.entities.IdmDocumentElement;
import  idm.data.model.IdmDocumentElementDTO;
import java.math.BigDecimal;

/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToIdmDocumentElementConverter  {
	

	

public static IdmDocumentElement convert(IdmDocumentElementDTO idmDocumentElementDto) {
 if (idmDocumentElementDto != null)
  return IdmDocumentElement.builder().tradeType(idmDocumentElementDto.getTradeType())
.region(idmDocumentElementDto.getRegion())
.underlying(idmDocumentElementDto.getUnderlying())
.valuationDate(idmDocumentElementDto.getValuationDate())
.elementType(idmDocumentElementDto.getElementType())
.payload(idmDocumentElementDto.getPayload())
.idmDocumentId(idmDocumentElementDto.getIdmDocumentId())
.idmDocumentElementId(idmDocumentElementDto.getIdmDocumentElementId())
 
.build();
 return null;
	}
    
}
