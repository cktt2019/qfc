package idm.data.converter;



import java.util.*;
import  idm.data.jpa.entities.IdmDocumentVersion;
import  idm.data.model.IdmDocumentVersionDTO;
import java.math.BigDecimal;

/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToIdmDocumentVersionConverter  {
	

	

public static IdmDocumentVersion convert(IdmDocumentVersionDTO idmDocumentVersionDto) {
 if (idmDocumentVersionDto != null)
  return IdmDocumentVersion.builder().idmDocumentId(idmDocumentVersionDto.getIdmDocumentId())
.idmDocumentVersion(idmDocumentVersionDto.getIdmDocumentVersion())
 
.build();
 return null;
	}
    
}
