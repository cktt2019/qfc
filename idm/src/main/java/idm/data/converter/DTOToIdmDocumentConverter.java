package idm.data.converter;



import java.util.*;
import  idm.data.jpa.entities.IdmDocument;
import  idm.data.model.IdmDocumentDTO;
import java.math.BigDecimal;

/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToIdmDocumentConverter  {
	

	

public static IdmDocument convert(IdmDocumentDTO idmDocumentDto) {
 if (idmDocumentDto != null)
  return IdmDocument.builder().documentId(idmDocumentDto.getDocumentId())
.region(idmDocumentDto.getRegion())
.underlying(idmDocumentDto.getUnderlying())
.tradeType(idmDocumentDto.getTradeType())
.valuationDate(idmDocumentDto.getValuationDate())
.version(idmDocumentDto.getVersion())
.calibrator(idmDocumentDto.getCalibrator())
.extractedUnderlying(idmDocumentDto.getExtractedUnderlying())
.payload(idmDocumentDto.getPayload())
.status(idmDocumentDto.getStatus())
 
.build();
 return null;
	}
    
}
