package idm.data.converter;




import java.util.*;
import  idm.data.model.IdmDocumentElementDTO;
import  idm.data.jpa.entities.IdmDocumentElement;
import java.math.BigDecimal;

/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	
	public class IdmDocumentElementToDTOConverter  {
	

	

public static IdmDocumentElementDTO convert(IdmDocumentElement idmDocumentElement) {
 if (idmDocumentElement != null)
  return IdmDocumentElementDTO.builder().tradeType(idmDocumentElement.getTradeType())
.region(idmDocumentElement.getRegion())
.underlying(idmDocumentElement.getUnderlying())
.valuationDate(idmDocumentElement.getValuationDate())
.elementType(idmDocumentElement.getElementType())
.payload(idmDocumentElement.getPayload())
.idmDocumentId(idmDocumentElement.getIdmDocumentId())
.idmDocumentElementId(idmDocumentElement.getIdmDocumentElementId())
.build();
 return null;
	}
    
}
