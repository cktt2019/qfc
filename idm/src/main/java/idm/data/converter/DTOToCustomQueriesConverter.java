package idm.data.converter;



import java.util.*;
import  idm.data.jpa.entities.CustomQueries;
import  idm.data.model.CustomQueriesDTO;
import java.math.BigDecimal;

/**
 * 
 * 
 * @author london-databases
 *
 */
 

	public class DTOToCustomQueriesConverter  {
	

	

public static CustomQueries convert(CustomQueriesDTO customQueriesDto) {
 if (customQueriesDto != null)
  return CustomQueries.builder().entityName(customQueriesDto.getEntityName())
.criteriaName(customQueriesDto.getCriteriaName())
.criteriaDescription(customQueriesDto.getCriteriaDescription())
.criteriaContent(customQueriesDto.getCriteriaContent())
 
.build();
 return null;
	}
    
}
