package idm.data.jpa.repositories.custom;

import java.util.Collection;
import  idm.data.model.IdmDocumentVersionDTO;
import java.math.BigDecimal;
import idm.data.jpa.entities.IdmDocumentVersion;
import idm.data.metadata.core.SearchInfoDTO;

/**
 *  Service interface with operations for {@link IdmDocumentVersion} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedIdmDocumentVersionRepository {
	
	public Collection<IdmDocumentVersionDTO> findByCustomCriteria (SearchInfoDTO info);

	}







