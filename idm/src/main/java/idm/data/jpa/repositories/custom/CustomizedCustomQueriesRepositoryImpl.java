package idm.data.jpa.repositories.custom;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;

import  idm.data.jpa.entities.CustomQueries;
import  idm.data.model.CustomQueriesDTO;
import  idm.data.metadata.CustomQueriesMetaData;
import  idm.data.metadata.core.SearchCriteriaDTO;
import  idm.data.metadata.core.SearchInfoDTO;
import  idm.data.metadata.core.SortInstructionDTO;
import  idm.data.metadata.core.PaginationInstructionDTO;

/**
 * Service interface with operations for {@link CustomQueries} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedCustomQueriesRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedCustomQueriesRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<CustomQueriesDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + CustomQueriesMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new CustomQueriesMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new CustomQueriesMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new CustomQueriesMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<CustomQueriesDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new CustomQueriesRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			CustomQueriesMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class CustomQueriesRowMapper implements RowMapper<CustomQueriesDTO> {
		@Override
		public CustomQueriesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			CustomQueriesDTO ret = new CustomQueriesDTO();


			
           ret.setEntityName (rs.getString("entityName"));
           ret.setCriteriaName (rs.getString("criteriaName"));
           ret.setCriteriaDescription (rs.getString("criteriaDescription"));
           ret.setCriteriaContent (rs.getString("criteriaContent"));

			return ret;
		}
	}

}
