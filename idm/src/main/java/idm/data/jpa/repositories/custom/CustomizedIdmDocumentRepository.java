package idm.data.jpa.repositories.custom;

import java.util.Collection;
import  idm.data.model.IdmDocumentDTO;
import java.math.BigDecimal;
import idm.data.jpa.entities.IdmDocument;
import idm.data.metadata.core.SearchInfoDTO;

/**
 *  Service interface with operations for {@link IdmDocument} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedIdmDocumentRepository {
	
	public Collection<IdmDocumentDTO> findByCustomCriteria (SearchInfoDTO info);

	}







