package idm.data.jpa.repositories.custom;

import java.util.Collection;
import  idm.data.model.IdmDocumentElementDTO;
import java.math.BigDecimal;
import idm.data.jpa.entities.IdmDocumentElement;
import idm.data.metadata.core.SearchInfoDTO;

/**
 *  Service interface with operations for {@link IdmDocumentElement} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedIdmDocumentElementRepository {
	
	public Collection<IdmDocumentElementDTO> findByCustomCriteria (SearchInfoDTO info);

	}







