package idm.data.jpa.repositories.custom;

import java.util.Collection;
import  idm.data.model.CustomQueriesDTO;
import java.math.BigDecimal;
import idm.data.jpa.entities.CustomQueries;
import idm.data.metadata.core.SearchInfoDTO;

/**
 *  Service interface with operations for {@link CustomQueries} persistence.
 *
 *
 * @author ctanyitang
 */
 
	public interface CustomizedCustomQueriesRepository {
	
	public Collection<CustomQueriesDTO> findByCustomCriteria (SearchInfoDTO info);

	}







