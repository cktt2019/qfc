package idm.data.jpa.repositories;

import java.util.List;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import java.math.BigDecimal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import idm.data.jpa.entities.IdmDocumentVersion;
import idm.data.jpa.entities.IdmDocumentVersionKey;

/**
 *  Service interface with operations for {@link IdmDocumentVersion} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface IdmDocumentVersionRepository extends JpaRepository<IdmDocumentVersion, IdmDocumentVersionKey>{


	

	
	public Page<IdmDocumentVersion> findAll (Pageable page);
	
	

	

	
	}







