package idm.data.jpa.repositories;

import java.util.List;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import java.math.BigDecimal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import idm.data.jpa.entities.IdmDocumentElement;

/**
 *  Service interface with operations for {@link IdmDocumentElement} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface IdmDocumentElementRepository extends JpaRepository<IdmDocumentElement, Integer>{


	public List<IdmDocumentElement> findByTradeType (String tradeType);
	public List<IdmDocumentElement> findByRegion (String region);
	public List<IdmDocumentElement> findByUnderlying (String underlying);
	public List<IdmDocumentElement> findByValuationDate (String valuationDate);
	public List<IdmDocumentElement> findByElementType (String elementType);
	public List<IdmDocumentElement> findByPayload (String payload);
	public List<IdmDocumentElement> findByIdmDocumentId (Integer idmDocumentId);
	

	
	public Page<IdmDocumentElement> findAll (Pageable page);
	
	

	

	
	}







