package idm.data.jpa.repositories.custom;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;

import  idm.data.jpa.entities.IdmDocumentElement;
import  idm.data.model.IdmDocumentElementDTO;
import  idm.data.metadata.IdmDocumentElementMetaData;
import  idm.data.metadata.core.SearchCriteriaDTO;
import  idm.data.metadata.core.SearchInfoDTO;
import  idm.data.metadata.core.SortInstructionDTO;
import  idm.data.metadata.core.PaginationInstructionDTO;

/**
 * Service interface with operations for {@link IdmDocumentElement} .
 *
 *
 * @author ctanyitang
 */

@Component
public class CustomizedIdmDocumentElementRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedIdmDocumentElementRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Collection<IdmDocumentElementDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM " + IdmDocumentElementMetaData.TABLE + " WHERE ");
		createQuery(searchInfo.getCriteria(), sb , new IdmDocumentElementMetaData());
		
		if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
			sb.append(" ORDER BY ");
			createSortingClause(searchInfo.getSorting(), sb, new IdmDocumentElementMetaData());
		}

		if (searchInfo.getPaging() != null) {
			createPagingClause(searchInfo.getPaging(), sb);
		}
		
		String sql = sb.toString();

		Map<String, Object> paramValues = new HashMap<String, Object>();
		createParamValueMap(searchInfo.getCriteria(), paramValues, new IdmDocumentElementMetaData());
		SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);

		
		List<IdmDocumentElementDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
				new IdmDocumentElementRowMapper());

		return resp;

	}
	
	private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
		if (pagination.getType().equalsIgnoreCase("offset")) {
			if (pagination.getOffset() != null && pagination.getLimit() != null) {
				sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
						.append(Integer.valueOf(pagination.getLimit()));
			}
		}

	}

	private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
			IdmDocumentElementMetaData metaData) {
		for (SortInstructionDTO sort : sorting) {
			if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
				sb.append(metaData.getColumnName(sort.getField())).append("  ")
						.append(sort.getDirection().toUpperCase()).append("  ");
		}

	}
	static class IdmDocumentElementRowMapper implements RowMapper<IdmDocumentElementDTO> {
		@Override
		public IdmDocumentElementDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			IdmDocumentElementDTO ret = new IdmDocumentElementDTO();


			
           ret.setTradeType (rs.getString("trade_type"));
           ret.setRegion (rs.getString("region"));
           ret.setUnderlying (rs.getString("underlying"));
           ret.setValuationDate (rs.getString("valuation_date"));
           ret.setElementType (rs.getString("element_type"));
           ret.setPayload (rs.getString("payload"));
           ret.setIdmDocumentId (rs.getInt("idm_document_id"));
           ret.setIdmDocumentElementId (rs.getInt("idm_document_element_id"));

			return ret;
		}
	}

}
