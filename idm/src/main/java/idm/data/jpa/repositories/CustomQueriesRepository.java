package idm.data.jpa.repositories;

import java.util.List;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import java.math.BigDecimal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import idm.data.jpa.entities.CustomQueries;

/**
 *  Service interface with operations for {@link CustomQueries} persistence.
 *
 *
 * @author ctanyitang
 */
 	@Repository
	public interface CustomQueriesRepository extends JpaRepository<CustomQueries, String>{


	public List<CustomQueries> findByEntityName (String entityName);
	public List<CustomQueries> findByCriteriaDescription (String criteriaDescription);
	public List<CustomQueries> findByCriteriaContent (String criteriaContent);
	

	
	public Page<CustomQueries> findAll (Pageable page);
	
	

	

	
	}







