package idm.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;



import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.lang.*;
import javax.persistence.*;
import java.sql.*;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode(exclude = {"idmDocumentElement","idmDocumentVersion"})
	@Table(name = "IdmDocument" )
	@Entity
	public class IdmDocument implements Serializable {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idmdocument_seq")
	    @SequenceGenerator(name = "idmdocument_seq", sequenceName = "idmdocument_seq")
	  @Column(name = "document_id"  )
	private Integer documentId ;
 
	
	  @Column(name = "region"  )
	private String region ;
 
	
	  @Column(name = "underlying"  )
	private String underlying ;
 
	
	  @Column(name = "trade_type"  )
	private String tradeType ;
 
	
	  @Column(name = "valuation_date"  )
	private java.sql.Timestamp valuationDate ;
 
	
	  @Column(name = "version"  )
	private String version ;
 
	
	  @Column(name = "calibrator"  )
	private String calibrator ;
 
	
	  @Column(name = "extracted_underlying"  )
	private String extractedUnderlying ;
 
	
	  @Column(name = "payload"  )
	private java.sql.Clob payload ;
 
	
	  @Column(name = "status"  )
	private String status ;
 
   
   
    


      @OneToMany(mappedBy="idmDocument0")
private Set<IdmDocumentElement> idmDocumentElement;

@OneToMany(mappedBy="idmDocument1")
private Set<Idm_document_version> idmDocumentVersion;


      
}
