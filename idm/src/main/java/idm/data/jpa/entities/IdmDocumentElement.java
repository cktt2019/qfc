package idm.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;



import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.lang.*;
import javax.persistence.*;
import java.sql.*;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode(exclude = {"idmDocument0"})
	@Table(name = "IdmDocumentElement" )
	@Entity
	public class IdmDocumentElement implements Serializable {
	
	  @Column(name = "trade_type"  )
	private String tradeType ;
 
	
	  @Column(name = "region"  )
	private String region ;
 
	
	  @Column(name = "underlying"  )
	private String underlying ;
 
	
	  @Column(name = "valuation_date"  )
	private String valuationDate ;
 
	
	  @Column(name = "element_type"  )
	private String elementType ;
 
	
	  @Column(name = "payload"  )
	private String payload ;
 
	
	  @Column(name = "idm_document_id" ,  insertable= false , updatable =false)
	private Integer idmDocumentId ;
 
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idmdocumentelement_seq")
	    @SequenceGenerator(name = "idmdocumentelement_seq", sequenceName = "idmdocumentelement_seq")
	  @Column(name = "idm_document_element_id"  )
	private Integer idmDocumentElementId ;
 
   
   
    


      @ManyToOne
@JoinColumn(name="idm_document_id", nullable=false)
private IdmDocument idmDocument0;


      
}
