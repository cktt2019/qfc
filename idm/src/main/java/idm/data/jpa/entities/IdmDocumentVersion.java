package idm.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;



import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.lang.*;
import javax.persistence.*;
import java.sql.*;



/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode(exclude = {"idmDocument1"})
	@Table(name = "Idm_document_version" )
	@Entity
    @IdClass(IdmDocumentVersionKey.class)
	public class IdmDocumentVersion implements Serializable {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idm_document_version_seq")
	    @SequenceGenerator(name = "idm_document_version_seq", sequenceName = "idm_document_version_seq")
	  @Column(name = "idm_document_id" ,  insertable= false , updatable =false)
	private Integer idmDocumentId ;
 
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idm_document_version_seq")
	    @SequenceGenerator(name = "idm_document_version_seq", sequenceName = "idm_document_version_seq")
	  @Column(name = "idm_document_version"  )
	private String idmDocumentVersion ;
 
   
   
    


      @ManyToOne
@JoinColumn(name="idm_document_id", nullable=false)
private IdmDocument idmDocument1;


      
}
