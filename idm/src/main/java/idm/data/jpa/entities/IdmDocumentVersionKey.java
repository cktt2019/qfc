package idm.data.jpa.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * 
 * 
 * @author london-databases
 *
 */
 
 	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public class IdmDocumentVersionKey implements Serializable {

	    private Integer idmDocumentId ;
 
	    private String idmDocumentVersion ;
 
}