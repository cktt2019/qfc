package idm.data.cache.config;

import idm.data.model.IdmDocumentDTO;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.Serializable;
import java.sql.Types;
import org.apache.ignite.cache.store.jdbc.dialect.JdbcDialect;






    @Component
	public class IdmDocumentCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, IdmDocumentDTO> cacheConfiguration (){
     CacheConfiguration<Integer, IdmDocumentDTO> cacheCfg = new CacheConfiguration<Integer, IdmDocumentDTO> ();

     cacheCfg.setName("IdmDocumentCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     IdmDocumentCacheJdbcPojoStoreFactory factory = new IdmDocumentCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("IdmDocumentCache");
     jdbcType.setDatabaseTable("IdmDocument");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(IdmDocumentDTO.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"document_id",Integer.class,"documentId"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.INTEGER,"document_id",Integer.class,"documentId"),
   	  new JdbcTypeField(Types.VARCHAR,"region",String.class,"region"),
   	  new JdbcTypeField(Types.VARCHAR,"underlying",String.class,"underlying"),
   	  new JdbcTypeField(Types.VARCHAR,"trade_type",String.class,"tradeType"),
   	  new JdbcTypeField(Types.VARCHAR,"valuation_date",java.sql.Timestamp.class,"valuationDate"),
   	  new JdbcTypeField(Types.VARCHAR,"version",String.class,"version"),
   	  new JdbcTypeField(Types.VARCHAR,"calibrator",String.class,"calibrator"),
   	  new JdbcTypeField(Types.VARCHAR,"extracted_underlying",String.class,"extractedUnderlying"),
   	  new JdbcTypeField(Types.VARCHAR,"payload",java.sql.Clob.class,"payload"),
   	  new JdbcTypeField(Types.VARCHAR,"status",String.class,"status")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class IdmDocumentCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, IdmDocumentDTO> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, IdmDocumentDTO> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
