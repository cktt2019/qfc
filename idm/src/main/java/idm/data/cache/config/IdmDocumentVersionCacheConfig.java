package idm.data.cache.config;

import idm.data.model.Idm_document_versionDTO;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.Serializable;
import java.sql.Types;
import org.apache.ignite.cache.store.jdbc.dialect.JdbcDialect;






    @Component
	public class IdmDocumentVersionCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<IdmDocumentVersionKey.class, IdmDocumentVersionDTO> cacheConfiguration (){
     CacheConfiguration<IdmDocumentVersionKey.class, IdmDocumentVersionDTO> cacheCfg = new CacheConfiguration<IdmDocumentVersionKey.class, IdmDocumentVersionDTO> ();

     cacheCfg.setName("IdmDocumentVersionCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     IdmDocumentVersionCacheJdbcPojoStoreFactory factory = new IdmDocumentVersionCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("IdmDocumentVersionCache");
     jdbcType.setDatabaseTable("Idm_document_version");
     jdbcType.setKeyType(IdmDocumentVersionKey.class.class);
     jdbcType.setValueType(IdmDocumentVersionDTO.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"idm_document_id",Integer.class,"idmDocumentId"),
    	  new JdbcTypeField(Types.VARCHAR,"idm_document_version",String.class,"idmDocumentVersion"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.INTEGER,"idm_document_id",Integer.class,"idmDocumentId"),
   	  new JdbcTypeField(Types.VARCHAR,"idm_document_version",String.class,"idmDocumentVersion")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class IdmDocumentVersionCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<IdmDocumentVersionKey.class, IdmDocumentVersionDTO> implements Serializable {
           @Override
           public CacheJdbcPojoStore<IdmDocumentVersionKey.class, IdmDocumentVersionDTO> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
