package idm.data.cache.config;

import idm.data.model.IdmDocumentElementDTO;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.Serializable;
import java.sql.Types;
import org.apache.ignite.cache.store.jdbc.dialect.JdbcDialect;






    @Component
	public class IdmDocumentElementCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;



    public CacheConfiguration<Integer, IdmDocumentElementDTO> cacheConfiguration (){
     CacheConfiguration<Integer, IdmDocumentElementDTO> cacheCfg = new CacheConfiguration<Integer, IdmDocumentElementDTO> ();

     cacheCfg.setName("IdmDocumentElementCache");
     cacheCfg.setCacheMode(CacheMode.PARTITIONED);
     cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
     cacheCfg.setReadThrough(true);
     cacheCfg.setWriteThrough(false);

     IdmDocumentElementCacheJdbcPojoStoreFactory factory = new IdmDocumentElementCacheJdbcPojoStoreFactory ();
     factory.setDialect(jdbcDialect);

     JdbcType jdbcType = new JdbcType();
     jdbcType.setCacheName("IdmDocumentElementCache");
     jdbcType.setDatabaseTable("IdmDocumentElement");
     jdbcType.setKeyType(Integer.class);
     jdbcType.setValueType(IdmDocumentElementDTO.class);

     jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER,"idm_document_element_id",Integer.class,"idmDocumentElementId"));

     jdbcType.setValueFields(
      new JdbcTypeField(Types.VARCHAR,"trade_type",String.class,"tradeType"),
   	  new JdbcTypeField(Types.VARCHAR,"region",String.class,"region"),
   	  new JdbcTypeField(Types.VARCHAR,"underlying",String.class,"underlying"),
   	  new JdbcTypeField(Types.VARCHAR,"valuation_date",String.class,"valuationDate"),
   	  new JdbcTypeField(Types.VARCHAR,"element_type",String.class,"elementType"),
   	  new JdbcTypeField(Types.VARCHAR,"payload",String.class,"payload"),
   	  new JdbcTypeField(Types.INTEGER,"idm_document_id",Integer.class,"idmDocumentId"),
   	  new JdbcTypeField(Types.INTEGER,"idm_document_element_id",Integer.class,"idmDocumentElementId")
      );

      factory.setTypes(jdbcType);
      cacheCfg.setCacheStoreFactory(factory);

      return cacheCfg;

    }

      private class IdmDocumentElementCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<Integer, IdmDocumentElementDTO> implements Serializable {
           @Override
           public CacheJdbcPojoStore<Integer, IdmDocumentElementDTO> create() {
            setDataSourceBean("dataSource");
            return super.create();
           }
        }

}
