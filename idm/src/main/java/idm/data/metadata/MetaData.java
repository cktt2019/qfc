package idm.data.metadata;

public interface MetaData {

	public String getColumnName(String property);

	public String getColumnDataType(String property);

}
