package idm.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class IdmDocumentElementMetaData  implements MetaData {
	
	private static final String TRADETYPE = "tradeType";
	private static final String REGION = "region";
	private static final String UNDERLYING = "underlying";
	private static final String VALUATIONDATE = "valuationDate";
	private static final String ELEMENTTYPE = "elementType";
	private static final String PAYLOAD = "payload";
	private static final String IDMDOCUMENTID = "idmDocumentId";
	private static final String IDMDOCUMENTELEMENTID = "idmDocumentElementId";

   
     public static final String TABLE = "IdmDocumentElement";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case TRADETYPE : return "trade_type";
	case REGION : return "region";
	case UNDERLYING : return "underlying";
	case VALUATIONDATE : return "valuation_date";
	case ELEMENTTYPE : return "element_type";
	case PAYLOAD : return "payload";
	case IDMDOCUMENTID : return "idm_document_id";
	case IDMDOCUMENTELEMENTID : return "idm_document_element_id";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case TRADETYPE : return "String";
	case REGION : return "String";
	case UNDERLYING : return "String";
	case VALUATIONDATE : return "String";
	case ELEMENTTYPE : return "String";
	case PAYLOAD : return "String";
	case IDMDOCUMENTID : return "Integer";
	case IDMDOCUMENTELEMENTID : return "Integer";
		}
		return null;
	}
	

}
