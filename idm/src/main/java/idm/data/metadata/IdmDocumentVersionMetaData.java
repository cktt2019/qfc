package idm.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class IdmDocumentVersionMetaData  implements MetaData {
	
	private static final String IDMDOCUMENTID = "idmDocumentId";
	private static final String IDMDOCUMENTVERSION = "idmDocumentVersion";

   
     public static final String TABLE = "Idm_document_version";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case IDMDOCUMENTID : return "idm_document_id";
	case IDMDOCUMENTVERSION : return "idm_document_version";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case IDMDOCUMENTID : return "Integer";
	case IDMDOCUMENTVERSION : return "String";
		}
		return null;
	}
	

}
