package idm.data.metadata;


/**
 * 
 * 
 * @author ctanyitang
 *
 */
 

	public class IdmDocumentMetaData  implements MetaData {
	
	private static final String DOCUMENTID = "documentId";
	private static final String REGION = "region";
	private static final String UNDERLYING = "underlying";
	private static final String TRADETYPE = "tradeType";
	private static final String VALUATIONDATE = "valuationDate";
	private static final String VERSION = "version";
	private static final String CALIBRATOR = "calibrator";
	private static final String EXTRACTEDUNDERLYING = "extractedUnderlying";
	private static final String PAYLOAD = "payload";
	private static final String STATUS = "status";

   
     public static final String TABLE = "IdmDocument";
   
    
	    @Override
		public  String getColumnName(String property) {
		switch (property) {
	case DOCUMENTID : return "document_id";
	case REGION : return "region";
	case UNDERLYING : return "underlying";
	case TRADETYPE : return "trade_type";
	case VALUATIONDATE : return "valuation_date";
	case VERSION : return "version";
	case CALIBRATOR : return "calibrator";
	case EXTRACTEDUNDERLYING : return "extracted_underlying";
	case PAYLOAD : return "payload";
	case STATUS : return "status";
		}
		return null;
	}
	

	
	    @Override
		public  String getColumnDataType(String property) {
		switch (property) {
	case DOCUMENTID : return "Integer";
	case REGION : return "String";
	case UNDERLYING : return "String";
	case TRADETYPE : return "String";
	case VALUATIONDATE : return "java.sql.Timestamp";
	case VERSION : return "String";
	case CALIBRATOR : return "String";
	case EXTRACTEDUNDERLYING : return "String";
	case PAYLOAD : return "java.sql.Clob";
	case STATUS : return "String";
		}
		return null;
	}
	

}
