package idm.data.metadata.core;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchCriteriaDTO {

	String field;
	String comparator;
	String value;

	// simple expressions : EXAMPLE (A AND B AND C OR D )
	List<SearchCriteriaDTO> exp;

	// logical groups of expressions : Example (A AND B) OR (C AND D )
	SearchCriteriaDTO left;

	SearchCriteriaDTO right;

	// Logical AND/OR chaining expressions together , in case of a list it specifies
	// how to link with the successor. the last
	// element in the list should have none
	String logicalOperator;

}
