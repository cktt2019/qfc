package idm.data.metadata.core;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchInfoDTO {
	
	SearchCriteriaDTO criteria;
	List<SortInstructionDTO> sorting;
	PaginationInstructionDTO paging;

}
