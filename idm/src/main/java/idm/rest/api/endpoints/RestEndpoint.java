package idm.rest.api.endpoints;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.stream.Collectors;
import java.math.BigDecimal;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import idm.data.model.*;
import idm.data.converter.*;
import idm.rest.api.service.*;
import idm.data.jpa.entities.*;
import idm.data.metadata.core.SearchInfoDTO;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 *  Rest enpoint for data retrieval and modifications
 *
 *
 * @author ctanyitang
 */
 	@Controller
    @Path("/api")
    @Transactional
	public class RestEndpoint {
	
     @Autowired
     CustomQueriesService customQueriesService;
     
     @Autowired
     IdmDocumentService idmDocumentService;
     
     @Autowired
     IdmDocumentElementService idmDocumentElementService;
     
     @Autowired
     IdmDocumentVersionService idmDocumentVersionService;
     



    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentversions")
	public List<IdmDocumentVersionDTO> listIdmdocumentversions () {

		return idmDocumentVersionService.findAll().stream().map(e -> IdmDocumentVersionToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/idmdocumentversions/custom")
    	public List<IdmDocumentVersionDTO> createIdmDocumentVersion ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(idmDocumentVersionService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentversions")
	public Response createIdmDocumentVersion ( @RequestBody IdmDocumentVersionDTO idmDocumentVersion ) {
		IdmDocumentVersion createdIdmDocumentVersion = idmDocumentVersionService.saveIdmDocumentVersion(idmDocumentVersion);
		return Response.status(Response.Status.CREATED).header("Location","/idmdocumentversions/"+createdIdmDocumentVersion.getIdmDocumentId()+"/"+createdIdmDocumentVersion.getIdmDocumentVersion()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}/idmdocumentversions")
	public List<IdmDocumentVersionDTO> getIdmdocumentsRelatedIdmdocumentversions (@PathParam("documentId") Integer  documentId    ) {
		
        //ONE_TO_MANY
		IdmDocument idmDocument =	idmDocumentService.findById(documentId    );
		if(idmDocument != null ) {
		Set<Idm_document_version>  relatedIdm_document_version =  idmDocument.getIdm_document_version();
		 if (relatedIdm_document_version != null)
		  return relatedIdm_document_version.stream().map(e -> Idm_document_versionToDTOConverter.convert(e)).collect(Collectors.toList());
		 else
		 throw new NotFoundException("No related relatedIdm_document_version found for IdmDocument with  " +" documentId = " + documentId     );
		}
		else throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");
	}



    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}/idmdocumentversions")
	public Response createIdmdocumentsRelatedIdmdocumentversions (@PathParam("documentId") Integer  documentId   ,    @RequestBody List<IdmDocumentVersionDTO> idmDocument ) {
		

		//ONE_TO_MANY
		IdmDocument idmDocumentVariable =	idmDocumentService.findById(documentId  );
		if(idmDocumentVariable != null ) {
		  for (IdmDocumentVersionDTO  idmDocumentVersionDto  : idmDocument){
		   IdmDocumentVersion idmDocumentVersionTmp = DTOToIdmDocumentVersionConverter.convert(idmDocumentVersionDto);
		   idmDocumentVersionTmp.setIdmDocument1(idmDocumentVariable);
		   idmDocumentVersionTmp = idmDocumentVersionService.save(idmDocumentVersionTmp);

		   idmDocumentVariable.getIdm_document_version().add( idmDocumentVersionTmp);
		  }
		idmDocumentService.save(idmDocumentVariable);
		}
		else throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");









		return Response.status(Response.Status.CREATED).build();
	}

    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}/idmdocumentversions")
	public Response putIdmdocumentsRelatedIdmdocumentversions (@PathParam("documentId") Integer  documentId   ,    @RequestBody List<IdmDocumentVersionDTO> idmDocument ) {
		



		//ONE_TO_MANY
		IdmDocument idmDocumentVariable =	idmDocumentService.findById(documentId    );
		if(idmDocumentVariable != null ) {
		  for (IdmDocumentVersionDTO  idmDocumentVersionDto  : idmDocument){
		   IdmDocumentVersion idmDocumentVersionTmp = DTOToIdmDocumentVersionConverter.convert(idmDocumentVersionDto);
		   idmDocumentVersionTmp.setIdmDocument1(idmDocumentVariable);
		   idmDocumentVersionTmp = idmDocumentVersionService.save(idmDocumentVersionTmp);

		   idmDocumentVariable.getIdm_document_version().add( idmDocumentVersionTmp);
		  }
		idmDocumentService.save(idmDocumentVariable);
		}
		else throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");



		return Response.status(Response.Status.CREATED).build();
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}/idmdocumentversions")
	public Response deleteIdmdocumentsRelatedIdmdocumentversions (@PathParam("documentId") Integer  documentId     , @RequestBody List<IdmDocumentVersionDTO> idmDocument)
	  {



		//ONE_TO_MANY
		IdmDocument idmDocumentVariable =	idmDocumentService.findById(documentId    );
		if(idmDocumentVariable != null ) {
		  for (IdmDocumentVersionDTO  idmDocumentVersionDto  : idmDocument){
		   IdmDocumentVersion idmDocumentVersionTmp = DTOToIdmDocumentVersionConverter.convert(idmDocumentVersionDto);
		   idmDocumentVersionTmp.setIdmDocument1(null);
		   idmDocumentVersionTmp = idmDocumentVersionService.save(idmDocumentVersionTmp);

		   idmDocumentVariable.getIdm_document_version().remove( idmDocumentVersionTmp);
		  }
		idmDocumentService.save(idmDocumentVariable);
		}
		else throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");







		return Response.status(Response.Status.CREATED).build();
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments")
	public List<IdmDocumentDTO> listIdmdocuments () {

		return idmDocumentService.findAll().stream().map(e -> IdmDocumentToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/idmdocuments/custom")
    	public List<IdmDocumentDTO> createIdmDocument ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(idmDocumentService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments")
	public Response createIdmDocument ( @RequestBody IdmDocumentDTO idmDocument ) {
		IdmDocument createdIdmDocument = idmDocumentService.saveIdmDocument(idmDocument);
		return Response.status(Response.Status.CREATED).header("Location","/idmdocuments/"+createdIdmDocument.getDocumentId()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}/idmdocumentelements")
	public List<IdmDocumentElementDTO> getIdmdocumentsRelatedIdmdocumentelements (@PathParam("documentId") Integer  documentId    ) {
		
        //ONE_TO_MANY
		IdmDocument idmDocument =	idmDocumentService.findById(documentId    );
		if(idmDocument != null ) {
		Set<IdmDocumentElement>  relatedIdmDocumentElement =  idmDocument.getIdmDocumentElement();
		 if (relatedIdmDocumentElement != null)
		  return relatedIdmDocumentElement.stream().map(e -> IdmDocumentElementToDTOConverter.convert(e)).collect(Collectors.toList());
		 else
		 throw new NotFoundException("No related relatedIdmDocumentElement found for IdmDocument with  " +" documentId = " + documentId     );
		}
		else throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");
	}



    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}/idmdocumentelements")
	public Response createIdmdocumentsRelatedIdmdocumentelements (@PathParam("documentId") Integer  documentId   ,    @RequestBody List<IdmDocumentElementDTO> idmDocument ) {
		

		//ONE_TO_MANY
		IdmDocument idmDocumentVariable =	idmDocumentService.findById(documentId  );
		if(idmDocumentVariable != null ) {
		  for (IdmDocumentElementDTO  idmDocumentElementDto  : idmDocument){
		   IdmDocumentElement idmDocumentElementTmp = DTOToIdmDocumentElementConverter.convert(idmDocumentElementDto);
		   idmDocumentElementTmp.setIdmDocument0(idmDocumentVariable);
		   idmDocumentElementTmp = idmDocumentElementService.save(idmDocumentElementTmp);

		   idmDocumentVariable.getIdmDocumentElement().add( idmDocumentElementTmp);
		  }
		idmDocumentService.save(idmDocumentVariable);
		}
		else throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");









		return Response.status(Response.Status.CREATED).build();
	}

    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}/idmdocumentelements")
	public Response putIdmdocumentsRelatedIdmdocumentelements (@PathParam("documentId") Integer  documentId   ,    @RequestBody List<IdmDocumentElementDTO> idmDocument ) {
		



		//ONE_TO_MANY
		IdmDocument idmDocumentVariable =	idmDocumentService.findById(documentId    );
		if(idmDocumentVariable != null ) {
		  for (IdmDocumentElementDTO  idmDocumentElementDto  : idmDocument){
		   IdmDocumentElement idmDocumentElementTmp = DTOToIdmDocumentElementConverter.convert(idmDocumentElementDto);
		   idmDocumentElementTmp.setIdmDocument0(idmDocumentVariable);
		   idmDocumentElementTmp = idmDocumentElementService.save(idmDocumentElementTmp);

		   idmDocumentVariable.getIdmDocumentElement().add( idmDocumentElementTmp);
		  }
		idmDocumentService.save(idmDocumentVariable);
		}
		else throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");



		return Response.status(Response.Status.CREATED).build();
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}/idmdocumentelements")
	public Response deleteIdmdocumentsRelatedIdmdocumentelements (@PathParam("documentId") Integer  documentId     , @RequestBody List<IdmDocumentElementDTO> idmDocument)
	  {



		//ONE_TO_MANY
		IdmDocument idmDocumentVariable =	idmDocumentService.findById(documentId    );
		if(idmDocumentVariable != null ) {
		  for (IdmDocumentElementDTO  idmDocumentElementDto  : idmDocument){
		   IdmDocumentElement idmDocumentElementTmp = DTOToIdmDocumentElementConverter.convert(idmDocumentElementDto);
		   idmDocumentElementTmp.setIdmDocument0(null);
		   idmDocumentElementTmp = idmDocumentElementService.save(idmDocumentElementTmp);

		   idmDocumentVariable.getIdmDocumentElement().remove( idmDocumentElementTmp);
		  }
		idmDocumentService.save(idmDocumentVariable);
		}
		else throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");







		return Response.status(Response.Status.CREATED).build();
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/customqueries")
	public List<CustomQueriesDTO> listCustomqueries () {

		return customQueriesService.findAll().stream().map(e -> CustomQueriesToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/customqueries/custom")
    	public List<CustomQueriesDTO> createCustomQueries ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(customQueriesService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/customqueries")
	public Response createCustomQueries ( @RequestBody CustomQueriesDTO customQueries ) {
		CustomQueries createdCustomQueries = customQueriesService.saveCustomQueries(customQueries);
		return Response.status(Response.Status.CREATED).header("Location","/customqueries/"+createdCustomQueries.getCriteriaName()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentelements")
	public List<IdmDocumentElementDTO> listIdmdocumentelements () {

		return idmDocumentElementService.findAll().stream().map(e -> IdmDocumentElementToDTOConverter.convert(e)).collect(Collectors.toList());
		
	}


    //Custom Search

     @POST
    	@Consumes(MediaType.APPLICATION_JSON)
    	@Path("/idmdocumentelements/custom")
    	public List<IdmDocumentElementDTO> createIdmDocumentElement ( @RequestBody SearchInfoDTO searchInfo ) {
        return new ArrayList<>(idmDocumentElementService.findAllCustom(searchInfo));
        }

    @POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentelements")
	public Response createIdmDocumentElement ( @RequestBody IdmDocumentElementDTO idmDocumentElement ) {
		IdmDocumentElement createdIdmDocumentElement = idmDocumentElementService.saveIdmDocumentElement(idmDocumentElement);
		return Response.status(Response.Status.CREATED).header("Location","/idmdocumentelements/"+createdIdmDocumentElement.getIdmDocumentElementId()).build();
		
	}






    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}")
	public IdmDocumentDTO getIdmDocumentById (@PathParam("documentId") Integer  documentId    ) {

		IdmDocument idmDocument =	idmDocumentService.findById(documentId    );
		if(idmDocument != null ) 
		   return IdmDocumentToDTOConverter.convert(idmDocument);
		throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}")
	public Response updateIdmDocumentById (@RequestBody IdmDocumentDTO idmDocument ) {
	    IdmDocument existingIdmDocument =	idmDocumentService.findById(idmDocument.getDocumentId()    );
		if(existingIdmDocument == null ) 
		throw new NotFoundException("No IdmDocument with  " +" idmDocument.getDocumentId() = " + idmDocument.getDocumentId()    + " exists");
		
	    idmDocumentService.updateIdmDocument(existingIdmDocument,idmDocument);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocuments/{documentId}")
	public Response deleteIdmDocumentById (@PathParam("documentId") Integer  documentId    ) 
	  {
	        IdmDocument existingIdmDocument =	idmDocumentService.findById(documentId    );
		if(existingIdmDocument == null ) 
		throw new NotFoundException("No IdmDocument with  " +" documentId = " + documentId    + " exists");
	
		idmDocumentService.deleteById(documentId    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentelements/{idmDocumentElementId}")
	public IdmDocumentElementDTO getIdmDocumentElementById (@PathParam("idmDocumentElementId") Integer  idmDocumentElementId    ) {

		IdmDocumentElement idmDocumentElement =	idmDocumentElementService.findById(idmDocumentElementId    );
		if(idmDocumentElement != null ) 
		   return IdmDocumentElementToDTOConverter.convert(idmDocumentElement);
		throw new NotFoundException("No IdmDocumentElement with  " +" idmDocumentElementId = " + idmDocumentElementId    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentelements/{idmDocumentElementId}")
	public Response updateIdmDocumentElementById (@RequestBody IdmDocumentElementDTO idmDocumentElement ) {
	    IdmDocumentElement existingIdmDocumentElement =	idmDocumentElementService.findById(idmDocumentElement.getIdmDocumentElementId()    );
		if(existingIdmDocumentElement == null ) 
		throw new NotFoundException("No IdmDocumentElement with  " +" idmDocumentElement.getIdmDocumentElementId() = " + idmDocumentElement.getIdmDocumentElementId()    + " exists");
		
	    idmDocumentElementService.updateIdmDocumentElement(existingIdmDocumentElement,idmDocumentElement);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentelements/{idmDocumentElementId}")
	public Response deleteIdmDocumentElementById (@PathParam("idmDocumentElementId") Integer  idmDocumentElementId    ) 
	  {
	        IdmDocumentElement existingIdmDocumentElement =	idmDocumentElementService.findById(idmDocumentElementId    );
		if(existingIdmDocumentElement == null ) 
		throw new NotFoundException("No IdmDocumentElement with  " +" idmDocumentElementId = " + idmDocumentElementId    + " exists");
	
		idmDocumentElementService.deleteById(idmDocumentElementId    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/customqueries/{criteriaName}")
	public CustomQueriesDTO getCustomQueriesById (@PathParam("criteriaName") String  criteriaName    ) {

		CustomQueries customQueries =	customQueriesService.findById(criteriaName    );
		if(customQueries != null ) 
		   return CustomQueriesToDTOConverter.convert(customQueries);
		throw new NotFoundException("No CustomQueries with  " +" criteriaName = " + criteriaName    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/customqueries/{criteriaName}")
	public Response updateCustomQueriesById (@RequestBody CustomQueriesDTO customQueries ) {
	    CustomQueries existingCustomQueries =	customQueriesService.findById(customQueries.getCriteriaName()    );
		if(existingCustomQueries == null ) 
		throw new NotFoundException("No CustomQueries with  " +" customQueries.getCriteriaName() = " + customQueries.getCriteriaName()    + " exists");
		
	    customQueriesService.updateCustomQueries(existingCustomQueries,customQueries);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/customqueries/{criteriaName}")
	public Response deleteCustomQueriesById (@PathParam("criteriaName") String  criteriaName    ) 
	  {
	        CustomQueries existingCustomQueries =	customQueriesService.findById(criteriaName    );
		if(existingCustomQueries == null ) 
		throw new NotFoundException("No CustomQueries with  " +" criteriaName = " + criteriaName    + " exists");
	
		customQueriesService.deleteById(criteriaName    );

		return Response.status(Response.Status.OK).build();
		
	}




    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentversions/{idmDocumentId}/{idmDocumentVersion}")
	public IdmDocumentVersionDTO getIdmDocumentVersionById (@PathParam("idmDocumentId") Integer  idmDocumentId   ,   @PathParam("idmDocumentVersion") String  idmDocumentVersion    ) {

		IdmDocumentVersion idmDocumentVersion =	idmDocumentVersionService.findById(idmDocumentId   ,   idmDocumentVersion    );
		if(idmDocumentVersion != null ) 
		   return IdmDocumentVersionToDTOConverter.convert(idmDocumentVersion);
		throw new NotFoundException("No IdmDocumentVersion with  " +" idmDocumentId = " + idmDocumentId   +" idmDocumentVersion = " + idmDocumentVersion    + " exists");
		
	}


    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentversions/{idmDocumentId}/{idmDocumentVersion}")
	public Response updateIdmDocumentVersionById (@RequestBody IdmDocumentVersionDTO idmDocumentVersion ) {
	    IdmDocumentVersion existingIdmDocumentVersion =	idmDocumentVersionService.findById(idmDocumentVersion.getIdmDocumentId()   ,   idmDocumentVersion.getIdmDocumentVersion()    );
		if(existingIdmDocumentVersion == null ) 
		throw new NotFoundException("No IdmDocumentVersion with  " +" idmDocumentVersion.getIdmDocumentId() = " + idmDocumentVersion.getIdmDocumentId()   +" idmDocumentVersion.getIdmDocumentVersion() = " + idmDocumentVersion.getIdmDocumentVersion()    + " exists");
		
	    idmDocumentVersionService.updateIdmDocumentVersion(existingIdmDocumentVersion,idmDocumentVersion);

		return Response.status(Response.Status.OK).build();
		
		
	}


    @DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/idmdocumentversions/{idmDocumentId}/{idmDocumentVersion}")
	public Response deleteIdmDocumentVersionById (@PathParam("idmDocumentId") Integer  idmDocumentId   ,   @PathParam("idmDocumentVersion") String  idmDocumentVersion    ) 
	  {
	        IdmDocumentVersion existingIdmDocumentVersion =	idmDocumentVersionService.findById(idmDocumentId   ,   idmDocumentVersion    );
		if(existingIdmDocumentVersion == null ) 
		throw new NotFoundException("No IdmDocumentVersion with  " +" idmDocumentId = " + idmDocumentId   +" idmDocumentVersion = " + idmDocumentVersion    + " exists");
	
		idmDocumentVersionService.deleteById(idmDocumentId   ,   idmDocumentVersion    );

		return Response.status(Response.Status.OK).build();
		
	}



       @GET
           @Produces(MediaType.APPLICATION_JSON)
           @Path("/customqueries")
           public List<CustomQueriesDTO> listCustomqueries() {

               return customQueriesService.findAll().stream().map(e -> CustomQueriesToDTOConverter.convert(e)).collect(Collectors.toList());

           }

           @POST
           @Consumes(MediaType.APPLICATION_JSON)
           @Path("/customqueries")
           public Response createCustomQueries(@RequestBody CustomQueriesDTO customQueries) {
               CustomQueries createdCustomQueries = customQueriesService.saveCustomQueries(customQueries);
               return Response.status(Response.Status.CREATED).header("Location", "/customqueries/" + createdCustomQueries.getCriteriaName()).build();

           }


           @GET
           @Produces(MediaType.APPLICATION_JSON)
           @Path("/customquerieslist/{entityName}")
           public List<String> listCustomqueriesForEntity(@PathParam("entityName") String entityName) {
               Collection<CustomQueries> all = customQueriesService.findAll();
               List<CustomQueries> filtered = all.stream().filter(x -> x.getEntityName().equals(entityName)).collect(Collectors.toList());
               return
                       filtered.stream().map(x -> x.getCriteriaName().replace(entityName + "_", "")).collect(Collectors.toList());

           }


           @GET
           @Produces(MediaType.APPLICATION_JSON)
           @Path("/customqueries/{criteriaName}")
           public CustomQueriesDTO getCustomQueriesById(@PathParam("criteriaName") String criteriaName) {

               CustomQueries customQueries = customQueriesService.findById(criteriaName);
               if (customQueries != null)
                   return CustomQueriesToDTOConverter.convert(customQueries);
               throw new NotFoundException("No CustomQueries with  " + " criteriaName = " + criteriaName + " exists");

           }


           @PUT
           @Consumes(MediaType.APPLICATION_JSON)
           @Path("/customqueries/{criteriaName}")
           public Response updateCustomQueriesById(@RequestBody CustomQueriesDTO customQueries) {
               CustomQueries existingCustomQueries = customQueriesService.findById(customQueries.getCriteriaName());
               if (existingCustomQueries == null)
                   throw new NotFoundException("No CustomQueries with  " + " customQueries.getCriteriaName() = " + customQueries.getCriteriaName() + " exists");

               customQueriesService.updateCustomQueries(existingCustomQueries, customQueries);

               return Response.status(Response.Status.OK).build();


           }


           @DELETE
           @Consumes(MediaType.APPLICATION_JSON)
           @Path("/customqueries/{criteriaName}")
           public Response deleteCustomQueriesById(@PathParam("criteriaName") String criteriaName) {
               CustomQueries existingCustomQueries = customQueriesService.findById(criteriaName);
               if (existingCustomQueries == null)
                   throw new NotFoundException("No CustomQueries with  " + " criteriaName = " + criteriaName + " exists");

               customQueriesService.deleteById(criteriaName);

               return Response.status(Response.Status.OK).build();

           }


    @POST
    @Consumes(MediaType.WILDCARD)
    @Path("/uploadfile/{entityName}/{entityId}/{fieldName}")
    public Response uploadMultiple(@PathParam("entityName") String entityName, @PathParam("entityId") String entityId,
                                   @PathParam("fieldName") String fieldName,
                                   @FormDataParam("fileName") String fileName, @FormDataParam("file") FormDataBodyPart body) {

        System.out.println("entityName: " + entityName);
        System.out.println("entityId: " + entityId);
        System.out.println("fieldName: " + fieldName);
        System.out.println("fileName: " + fileName);
        try {
            for (BodyPart part : body.getParent().getBodyParts()) {
                InputStream is = part.getEntityAs(InputStream.class);
                ContentDisposition meta = part.getContentDisposition();
                // System.out.println(meta);
                if (meta.getFileName() != null && meta.getFileName().length() > 2) {
                    copyInputStreamToFileLocal(is, UPLOADS_LOCATION + SEPARATOR + entityName + SEPARATOR + entityId  + SEPARATOR + fieldName + SEPARATOR, meta.getFileName());

                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return Response.status(Response.Status.OK).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getBase64ForFile/{mediaType}/{fileName}")
    public ResponseEntity getBase64ForFile(@PathParam("mediaType") String mediaType , @PathParam("fileName") String filePath) {
        System.out.println(" getBase64ForFile "+ filePath);
        String base64EncodedFile = "";
        base64EncodedFile = "iVBORw0KGgoAAAANSUhEUgAAAV4AAAEUCAMAAABUNq4iAAAC1lBMVEXm5uYAAAD/tgD/lgD/lwD/lgD/lwD/lwD/lgD/lgD/lgD/lwD/mAD/mwD/vwD/lgD/lwD/lgD/mQD/lwD/lwD/mgD/lwD/lwD/qgD/lwD//wD/lgD/nQD/lgD/lgD/lwD/lwD/lwD/lwD/lgD/lgD/lwD/lgD/lwD/lwD/lgD/lwD/qgD/lgD/lwD/mQD/qgD/lgD/nQD/mAD/lwD/mgD/lwD/ogD/oQD/lwD/lwD/mQD/lwD/lgD/mQD/mAD/lwD/lgD/mAD/lgD/mAD/lgD/lwD/lwD/lwD/lwD/pAD/lgD/lwD/lgD/lgD/lwD/lwD/mAD/lwD/lgD/mQD/lwD/lwD/mQD/mQD/nwD/lgD/lwD/lgD/mgD/lgD/lwD/lwD/lgD/nwD/ngD/lgD/lwD/lgD/lwD/lgD//wD/lgD/lgD/mAD/mQD/qgD/lgD/mwD/mAD/mgD/mQD/lwD/lgD/lwD/mQD/lwD/lwD/mAD/lgD/nAD/lwD/mwD/lwD/mAD/lgD/lgD/lgD/lwD/lwD/mQD/lgD/lwD/lwD/lwD/lwD/lgD/mQD/mAD/lwD/nAD/lgD/lwD/mAD/mwD/lwD/lwD/mAD/lgD/lwD/lgD/lgD/lwD/lwD/mgD/ngD/mAD/mQD/mAD/mQD/lwD/mAD/lgD/lwD/mAD/lwD/lwD/lwD/lgD/lwD/lwD/lwD/lwD/mgD/lgD/lgD/lwD/mAD/lwD/lwD/lgD/lwD/lgD/lwD/mAD/lwD/lgD/lwD/lwD/mAD/mAD/lwD/lwD/lgD/lwD/mAD/lgD/lgD/lgD/lgD/lgD/mAD/nwD/lwD/lwD/lwD/lwD/lwD/lgD/lwD/lwD/lwD/mwD/lwD/lwD/lgD/mAD/lwD/lgD/lgD/lgD/lwD/lgD/mAD/lwD/lwD/lwD/mAD/nAD/lwD/mAD/lgD/lwD/mQD/mAD/lwD/lgD/lgD/lwB92HTLAAAA8nRSTlP/AAdJmMzm/P/23cF+LgRmwjgezpM16bwMsAF8GpfuQnaQ8qqSldH+pvt4CTPJQQa7DXmpMJELE9BHMrXIBWG68y/UTREbYOGsDrKATvH5iUhA7yjY4y0jEFWF6j/ibn+hCB09W/1dcAKGz1dGA4spKitQnbRYN8vVWWQSFhezXvjZJ72OD0Sf64QgIjxSViSZZXQcZ1NPqKJ6vr+aJhU0CiUU1z7l32P3oO3gUZbk0zrFt/VqjNp1Yq9sVMaN3MSDXHM7rWlo3sD0o9tvGDGCNky41qul8CH6p1+Ksbl3WnvDOejHLG0ffZTnzUtKpOyeh1ZAATMAAAkrSURBVHgB7NDFAYIAAABAW7rB3n9NJ6B56d0ItwOAX7Rnc3r16kWvXr3o1asXvXr16tWrF7169aJXr1706tWLXr3o1asXvXr1olevXvTq1atXr1706tWLXr160atXL3r1olevXvTq1YtevXrRq1evXr160atXL3r16kWvXr38U+/heDpfrsFUYRQnqd6JsvwSzFeUeqeo6mCZJtM7qu2CpW53vSOqLlju8dQ7KKuDNV56B+XBKuFb74DDJVjno3fAlz17YO4jiMM4Pr/wGdS2G9u2bdtG3bB2+5LDa3Yuk70/djPK830Jn7t1KAy7f568+sJg2gPy6guHaQ/Jqy8Cpj0ir75IuHv85Omz51EIoGjy6oOrmNg42Ss+Af73RPyLvDGJclhSMnnt88bK/1Kuk9c27+M4OSqVvLZ500SVTl7bvBmiyiSvbd5nosqyzUvebFHl2OYlb1SuHJVnm5e8yC8Qp0JY5yUvilIc3WLyWgnurqeWZGbl5AHktRICqbSsvKKy6nR4yVsdInvV1J4GL3mrz8tBt+uiyOtngek61TeY8ZK3sam5JbXVratqazfhJW9Hp+x1N/xkXTUBk9c7jW6XHNQdfrKumoDJ65leV/m6dd0TMHk90+o6lYdrdEW6eg66RF59Wl31/7p0y8Uo8ipdx7dX6UpvX78YRN6BLtHXC8QMktf/huqO5UN3r+ER8pqm1wXCR8lr3Ni4RhcwnoDJO1Y8ManRBWKmpslrpgu4fGfgajZ1rof7XhNdl28vXyvs6rp8e0Fea80Xw2niguy1sAjyBtXS8vFWVl9EQfXy1evCN/Aojbz67sO0MPLqewvThsir7xZMe0defe9hWMwaefWtx8CsZCGvR89gVj95vdp4DJM2hbyerUYh+KrWyeujhwi61nQhr6+2thFcHz4KeX2X/glBtP35i5DXr75++47AKv3xU4S8/rbw6/efv/5WFr/B14qzEHnJS15GXvKSl5GXvORl5LUfecnLyEte8jLykpe8jLzkJS8jL3kZeclLXkZe8pKXkZe85GX/pqamunbZtQcgOfIFjuP1i+a7mtuKVXHS2bNj27Y9Z3N5iG0s3xpn27ZtG2XX6+7/6+XM7F0482q/hbY+Y0QIb/KOHTt26qi3y97t7qrRZf90G689Ovz2AvsihLc5sF9HvUTggNzigLNUt20HD66ss43XWNVTA6/VPyxvGhxq4D18XjL/PW+z/5X1/8mb3VxOZ5y6KSAnM5V9bY6qdUqumawsNTcvGC8Ta/P6Z/+nfVjeQapZfqtxCttZrfqoqsDs5pHJu6ygoKBwcREUlygnLQZKyzrKrX+7VUD5hLYyXdwiCSom3mZv4F5Z29vvAN+dg4PwzvTX4L3r7lLodk+2pJEFBUPhXnsf94XmXXb/IrAeeDAgu2x73YdkN8Ie2SItc6ZbneaD4itlSn14KDR75NEI5M0DHsPt8Sdwm+Q46MokTBXJsgtcYOG06kkgVdK5N2E6kFeHl0eq8Qae8uF270lSPl5LQvLOHo4pc5SkBOBB2T0N9JM54QzcnpHTs81wGx6hvLVr2l7KngxMHwPwnOyex8vwnn8HwGSArrV4M2HD2ireF6js9H/Ce8qLeL0Ugtfr5d2S1i7AK0J5X5l6cW+AV187t8uLwHZpB1jbpNefA5ZL+Z3AuvSK5H7DMbwXAD1S228fD0mX1+R9wwdTKnlzOkHMnlFz3rSgYJB/xYrr4aYVK1Y0D8W7ETjzreTOBcDqELwDL97+9quYR8njwC3Xzn3rnUjlfVd2TYG3JV0JzJPm3F9e5j7jAVPNNXeQXWq5y3tWBrwnu+R74X3VpGoCGZs83vnAo5IZSZc0EJrWeb7eYOoltZ0MfS+RlP8BvBic1/pQUjrQUlr7ERT4Jfn7RijvRtntghjnqhqVwkmyO2WtPTFiJrBGGg/Tz5dTb5c3F9h7vtNVMLwWb8ok+NjwulvGtpddcgZcGoLXa5bUH/hETp8CbYPyJspuOfCZ9BZwn5yeiUxec7csgUly6oThVfPFn8fg9IX0ADwpt/td3pVUKyNQk1dfgPWW4XW2bCe3mZBWP++XwLlyugY4NSjvDjlNgjQzu1BOj0YT7+Vn4rTI8H4EH1fn/Yrq1X4ebVQMXyca3o+gidwyYXgo3keed1std8+vy2kh8GgY3g8gzayfLae9UcR77mQg5rOSPoZ3EWRW5x0M3DLW6/VavLoIKDW8i+A9ud0JZ4biHSSvh4ArKrmerZd3J/BN1N17D8HLg/OkZYa3DXzrl1OZy3sX8Kjc/MGo7sbtLHfLO+XUaCj0lPQdzAzDuwXYK6evgBx9D3wlu8XBeTtXPlf/EEW80+Eq2X1oeHuCeUY+f5bL28eC3gbhlrVBqK61DK/ZcoXsdgKvSfoMrD6heVOAW2TnHwOdpHygTHZXB+ddDnwnp8wo4i2HOwdJKQMM72wfxNy4TD++g8urq4BdATWfAHcuD0L1vsc7OwmKVrT3PzoUOuVLagk0nbIkFK/OAXqOU2oa8KakbpDx0+xtYwnOq3uArLlaeztRxOswxjzWNwPDq58BPmoNGN6UD4B7nwCwJgahal6K4VVXgA9iAJ733kmDFZJ398uA9S1AxSmS9mMKxXuqBVi/WEQTb0IGpm8N78hETBWGV293wuR7RMGoNnq8a88EU1lATpnheXXRUEy/bJFd9ke4xYbg1U9GlmZRxKsVvwJMX9zT8CrQIQa495P7MbxafqYF8NizCsrb9hcMr9ov7uZi7ZXpkh/ixvjC8GrzSxbg695Hbm85sFx1bShe/dYasNJaRRBv/bW/+e3RhY6Plz/h6W8GqQcknSEj+Ozgfdv1Dwpkb9v3o/5Faws/uTZPXv7vb/g9R+HaPK/xyOj+pbjPVQboHShQlBX5vL/9wq+nSHrWB+818B7t9gOd3vvpj48gKbeB92j3egVeT6mB96iXOgS3P3cGGniPRQnxfz33d3pyw3/M/tsOHRMBAAAgEOrf2g6OfxAB9OrVi1696NWrF7169aJXr1706tWrV69e9OrVi169etGrVy969aJXr1706tWLXr160av3oRe9evWiV69e9OpFr1696NWrF7169aJXL3r16kWvXr3o1asXvXr1AkDIAJeoFje3HAjXAAAAAElFTkSuQmCC";
        String prefix = "data:image/png;base64,";
        if("video".equals(mediaType))
            prefix = "data:video/mp4;base64,";
        String fileUrl = "";
        try {

            filePath = filePath.replace("__",SEPARATOR);
            byte[] downloadFileContent = Files.readAllBytes(Paths.get(UPLOADS_LOCATION +SEPARATOR+filePath));

            base64EncodedFile = encodeFileToBase64(downloadFileContent);

        } catch (Exception e) {
             base64EncodedFile = "iVBORw0KGgoAAAANSUhEUgAAAV4AAAEUCAMAAABUNq4iAAAC1lBMVEXm5uYAAAD/tgD/lgD/lwD/lgD/lwD/lwD/lgD/lgD/lgD/lwD/mAD/mwD/vwD/lgD/lwD/lgD/mQD/lwD/lwD/mgD/lwD/lwD/qgD/lwD//wD/lgD/nQD/lgD/lgD/lwD/lwD/lwD/lwD/lgD/lgD/lwD/lgD/lwD/lwD/lgD/lwD/qgD/lgD/lwD/mQD/qgD/lgD/nQD/mAD/lwD/mgD/lwD/ogD/oQD/lwD/lwD/mQD/lwD/lgD/mQD/mAD/lwD/lgD/mAD/lgD/mAD/lgD/lwD/lwD/lwD/lwD/pAD/lgD/lwD/lgD/lgD/lwD/lwD/mAD/lwD/lgD/mQD/lwD/lwD/mQD/mQD/nwD/lgD/lwD/lgD/mgD/lgD/lwD/lwD/lgD/nwD/ngD/lgD/lwD/lgD/lwD/lgD//wD/lgD/lgD/mAD/mQD/qgD/lgD/mwD/mAD/mgD/mQD/lwD/lgD/lwD/mQD/lwD/lwD/mAD/lgD/nAD/lwD/mwD/lwD/mAD/lgD/lgD/lgD/lwD/lwD/mQD/lgD/lwD/lwD/lwD/lwD/lgD/mQD/mAD/lwD/nAD/lgD/lwD/mAD/mwD/lwD/lwD/mAD/lgD/lwD/lgD/lgD/lwD/lwD/mgD/ngD/mAD/mQD/mAD/mQD/lwD/mAD/lgD/lwD/mAD/lwD/lwD/lwD/lgD/lwD/lwD/lwD/lwD/mgD/lgD/lgD/lwD/mAD/lwD/lwD/lgD/lwD/lgD/lwD/mAD/lwD/lgD/lwD/lwD/mAD/mAD/lwD/lwD/lgD/lwD/mAD/lgD/lgD/lgD/lgD/lgD/mAD/nwD/lwD/lwD/lwD/lwD/lwD/lgD/lwD/lwD/lwD/mwD/lwD/lwD/lgD/mAD/lwD/lgD/lgD/lgD/lwD/lgD/mAD/lwD/lwD/lwD/mAD/nAD/lwD/mAD/lgD/lwD/mQD/mAD/lwD/lgD/lgD/lwB92HTLAAAA8nRSTlP/AAdJmMzm/P/23cF+LgRmwjgezpM16bwMsAF8GpfuQnaQ8qqSldH+pvt4CTPJQQa7DXmpMJELE9BHMrXIBWG68y/UTREbYOGsDrKATvH5iUhA7yjY4y0jEFWF6j/ibn+hCB09W/1dcAKGz1dGA4spKitQnbRYN8vVWWQSFhezXvjZJ72OD0Sf64QgIjxSViSZZXQcZ1NPqKJ6vr+aJhU0CiUU1z7l32P3oO3gUZbk0zrFt/VqjNp1Yq9sVMaN3MSDXHM7rWlo3sD0o9tvGDGCNky41qul8CH6p1+Ksbl3WnvDOejHLG0ffZTnzUtKpOyeh1ZAATMAAAkrSURBVHgB7NDFAYIAAABAW7rB3n9NJ6B56d0ItwOAX7Rnc3r16kWvXr3o1asXvXr16tWrF7169aJXr1706tWLXr3o1asXvXr1olevXvTq1atXr1706tWLXr160atXL3r1olevXvTq1YtevXrRq1evXr160atXL3r16kWvXr38U+/heDpfrsFUYRQnqd6JsvwSzFeUeqeo6mCZJtM7qu2CpW53vSOqLlju8dQ7KKuDNV56B+XBKuFb74DDJVjno3fAlz17YO4jiMM4Pr/wGdS2G9u2bdtG3bB2+5LDa3Yuk70/djPK830Jn7t1KAy7f568+sJg2gPy6guHaQ/Jqy8Cpj0ir75IuHv85Omz51EIoGjy6oOrmNg42Ss+Af73RPyLvDGJclhSMnnt88bK/1Kuk9c27+M4OSqVvLZ500SVTl7bvBmiyiSvbd5nosqyzUvebFHl2OYlb1SuHJVnm5e8yC8Qp0JY5yUvilIc3WLyWgnurqeWZGbl5AHktRICqbSsvKKy6nR4yVsdInvV1J4GL3mrz8tBt+uiyOtngek61TeY8ZK3sam5JbXVratqazfhJW9Hp+x1N/xkXTUBk9c7jW6XHNQdfrKumoDJ65leV/m6dd0TMHk90+o6lYdrdEW6eg66RF59Wl31/7p0y8Uo8ipdx7dX6UpvX78YRN6BLtHXC8QMktf/huqO5UN3r+ER8pqm1wXCR8lr3Ni4RhcwnoDJO1Y8ManRBWKmpslrpgu4fGfgajZ1rof7XhNdl28vXyvs6rp8e0Fea80Xw2niguy1sAjyBtXS8vFWVl9EQfXy1evCN/Aojbz67sO0MPLqewvThsir7xZMe0defe9hWMwaefWtx8CsZCGvR89gVj95vdp4DJM2hbyerUYh+KrWyeujhwi61nQhr6+2thFcHz4KeX2X/glBtP35i5DXr75++47AKv3xU4S8/rbw6/efv/5WFr/B14qzEHnJS15GXvKSl5GXvORl5LUfecnLyEte8jLykpe8jLzkJS8jL3kZeclLXkZe8pKXkZe85GX/pqamunbZtQcgOfIFjuP1i+a7mtuKVXHS2bNj27Y9Z3N5iG0s3xpn27ZtG2XX6+7/6+XM7F0482q/hbY+Y0QIb/KOHTt26qi3y97t7qrRZf90G689Ovz2AvsihLc5sF9HvUTggNzigLNUt20HD66ss43XWNVTA6/VPyxvGhxq4D18XjL/PW+z/5X1/8mb3VxOZ5y6KSAnM5V9bY6qdUqumawsNTcvGC8Ta/P6Z/+nfVjeQapZfqtxCttZrfqoqsDs5pHJu6ygoKBwcREUlygnLQZKyzrKrX+7VUD5hLYyXdwiCSom3mZv4F5Z29vvAN+dg4PwzvTX4L3r7lLodk+2pJEFBUPhXnsf94XmXXb/IrAeeDAgu2x73YdkN8Ie2SItc6ZbneaD4itlSn14KDR75NEI5M0DHsPt8Sdwm+Q46MokTBXJsgtcYOG06kkgVdK5N2E6kFeHl0eq8Qae8uF270lSPl5LQvLOHo4pc5SkBOBB2T0N9JM54QzcnpHTs81wGx6hvLVr2l7KngxMHwPwnOyex8vwnn8HwGSArrV4M2HD2ireF6js9H/Ce8qLeL0Ugtfr5d2S1i7AK0J5X5l6cW+AV187t8uLwHZpB1jbpNefA5ZL+Z3AuvSK5H7DMbwXAD1S228fD0mX1+R9wwdTKnlzOkHMnlFz3rSgYJB/xYrr4aYVK1Y0D8W7ETjzreTOBcDqELwDL97+9quYR8njwC3Xzn3rnUjlfVd2TYG3JV0JzJPm3F9e5j7jAVPNNXeQXWq5y3tWBrwnu+R74X3VpGoCGZs83vnAo5IZSZc0EJrWeb7eYOoltZ0MfS+RlP8BvBic1/pQUjrQUlr7ERT4Jfn7RijvRtntghjnqhqVwkmyO2WtPTFiJrBGGg/Tz5dTb5c3F9h7vtNVMLwWb8ok+NjwulvGtpddcgZcGoLXa5bUH/hETp8CbYPyJspuOfCZ9BZwn5yeiUxec7csgUly6oThVfPFn8fg9IX0ADwpt/td3pVUKyNQk1dfgPWW4XW2bCe3mZBWP++XwLlyugY4NSjvDjlNgjQzu1BOj0YT7+Vn4rTI8H4EH1fn/Yrq1X4ebVQMXyca3o+gidwyYXgo3keed1std8+vy2kh8GgY3g8gzayfLae9UcR77mQg5rOSPoZ3EWRW5x0M3DLW6/VavLoIKDW8i+A9ud0JZ4biHSSvh4ArKrmerZd3J/BN1N17D8HLg/OkZYa3DXzrl1OZy3sX8Kjc/MGo7sbtLHfLO+XUaCj0lPQdzAzDuwXYK6evgBx9D3wlu8XBeTtXPlf/EEW80+Eq2X1oeHuCeUY+f5bL28eC3gbhlrVBqK61DK/ZcoXsdgKvSfoMrD6heVOAW2TnHwOdpHygTHZXB+ddDnwnp8wo4i2HOwdJKQMM72wfxNy4TD++g8urq4BdATWfAHcuD0L1vsc7OwmKVrT3PzoUOuVLagk0nbIkFK/OAXqOU2oa8KakbpDx0+xtYwnOq3uArLlaeztRxOswxjzWNwPDq58BPmoNGN6UD4B7nwCwJgahal6K4VVXgA9iAJ733kmDFZJ398uA9S1AxSmS9mMKxXuqBVi/WEQTb0IGpm8N78hETBWGV293wuR7RMGoNnq8a88EU1lATpnheXXRUEy/bJFd9ke4xYbg1U9GlmZRxKsVvwJMX9zT8CrQIQa495P7MbxafqYF8NizCsrb9hcMr9ov7uZi7ZXpkh/ixvjC8GrzSxbg695Hbm85sFx1bShe/dYasNJaRRBv/bW/+e3RhY6Plz/h6W8GqQcknSEj+Ozgfdv1Dwpkb9v3o/5Faws/uTZPXv7vb/g9R+HaPK/xyOj+pbjPVQboHShQlBX5vL/9wq+nSHrWB+818B7t9gOd3vvpj48gKbeB92j3egVeT6mB96iXOgS3P3cGGniPRQnxfz33d3pyw3/M/tsOHRMBAAAgEOrf2g6OfxAB9OrVi1696NWrF7169aJXr1706tWrV69e9OrVi169etGrVy969aJXr1706tWLXr160av3oRe9evWiV69e9OpFr1696NWrF7169aJXL3r16kWvXr3o1asXvXr1AkDIAJeoFje3HAjXAAAAAElFTkSuQmCC";
        }
        return new ResponseEntity<String>(prefix + base64EncodedFile, HttpStatus.OK);
        //return "data:image/png;base64, " + base64EncodedFile;
    }

    private static String encodeFileToBase64(byte[] fileContent) {
        return Base64.getEncoder().encodeToString(fileContent);

    }

    // InputStream -> File
    private static void copyInputStreamToFileLocal(InputStream inputStream, String path, String fileName) throws IOException {
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        try (FileOutputStream outputStream = new FileOutputStream(path + SEPARATOR + fileName)) {

            int read;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

            // commons-io
            // IOUtils.copy(inputStream, outputStream);

        }

    }


        @GET
        @Produces(MediaType.APPLICATION_OCTET_STREAM)
        @Path("/downloadFile/{fileName}")
            public Response downloadFile(@PathParam("fileName") String filePath) {
            byte[] downloadFileContent = new byte[0];
            System.out.println(filePath);
            String[] split = filePath.split("___");
            String fileName = split[split.length-1];

            filePath = filePath.replace("___",SEPARATOR);
            try{
                downloadFileContent = Files.readAllBytes(Paths.get(UPLOADS_LOCATION + SEPARATOR + filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            //return Response.ok().build();
            try {
                return Response.ok(downloadFileContent, MediaType.APPLICATION_OCTET_STREAM)
                            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName).build();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return Response.ok().build();
        }

	}







