package idm.rest.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import idm.data.model.*;
import idm.data.converter.*;
import idm.data.jpa.entities.*;
import idm.data.jpa.repositories.*;
import idm.data.jpa.repositories.custom.*;
import idm.data.metadata.core.SearchInfoDTO;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class IdmDocumentVersionService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(IdmDocumentVersionService.class);
 
    @Autowired
	IdmDocumentVersionRepository idmDocumentVersionRepository;

	@Autowired
    CustomizedIdmDocumentVersionRepository customizedIdmDocumentVersionRepository;

	
	@Transactional
	public IdmDocumentVersion saveIdmDocumentVersion (IdmDocumentVersionDTO idmDocumentVersionDTO){
	 IdmDocumentVersion idmDocumentVersion  = DTOToIdmDocumentVersionConverter.convert(idmDocumentVersionDTO);
	 return idmDocumentVersionRepository.save(idmDocumentVersion);
	}
	
	@Transactional
	public IdmDocumentVersion save (IdmDocumentVersion idmDocumentVersion){
	 return idmDocumentVersionRepository.save(idmDocumentVersion);
	}

       
       
       
  
    @Transactional
   public Collection<IdmDocumentVersion> findAll (){
	   return idmDocumentVersionRepository.findAll();
	}

	@Transactional
    public Collection<IdmDocumentVersionDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedIdmDocumentVersionRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public IdmDocumentVersion findById (Integer idmDocumentId , String idmDocumentVersion){
	   Optional<IdmDocumentVersion> idmDocumentVersion  = idmDocumentVersionRepository.findById(IdmDocumentVersionKey.builder().idmDocumentId(idmDocumentId).idmDocumentVersion(idmDocumentVersion).build());
	  return (idmDocumentVersion.isPresent() ? idmDocumentVersion.get() : null);
	}
	
   @Transactional
   public IdmDocumentVersion updateIdmDocumentVersion (IdmDocumentVersion idmDocumentVersion,IdmDocumentVersionDTO idmDocumentVersionDto){
	
	 return idmDocumentVersionRepository.save(idmDocumentVersion);
	}
	
   @Transactional
   public void deleteById (Integer idmDocumentId , String idmDocumentVersion){
	   idmDocumentVersionRepository.deleteById(IdmDocumentVersionKey.builder().idmDocumentId(idmDocumentId).idmDocumentVersion(idmDocumentVersion).build());

	}
	
   
   

}
