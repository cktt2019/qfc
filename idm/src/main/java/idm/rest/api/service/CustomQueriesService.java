package idm.rest.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import idm.data.model.*;
import idm.data.converter.*;
import idm.data.jpa.entities.*;
import idm.data.jpa.repositories.*;
import idm.data.jpa.repositories.custom.*;
import idm.data.metadata.core.SearchInfoDTO;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class CustomQueriesService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(CustomQueriesService.class);
 
    @Autowired
	CustomQueriesRepository customQueriesRepository;

	@Autowired
    CustomizedCustomQueriesRepository customizedCustomQueriesRepository;

	
	@Transactional
	public CustomQueries saveCustomQueries (CustomQueriesDTO customQueriesDTO){
	 CustomQueries customQueries  = DTOToCustomQueriesConverter.convert(customQueriesDTO);
	 return customQueriesRepository.save(customQueries);
	}
	
	@Transactional
	public CustomQueries save (CustomQueries customQueries){
	 return customQueriesRepository.save(customQueries);
	}

       
       
       
  
    @Transactional
   public Collection<CustomQueries> findAll (){
	   return customQueriesRepository.findAll();
	}

	@Transactional
    public Collection<CustomQueriesDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedCustomQueriesRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public CustomQueries findById (String criteriaName){
	   Optional<CustomQueries> customQueries  = customQueriesRepository.findById(criteriaName);
	  return (customQueries.isPresent() ? customQueries.get() : null);
	}
	
   @Transactional
   public CustomQueries updateCustomQueries (CustomQueries customQueries,CustomQueriesDTO customQueriesDto){
	
	customQueries.setEntityName(customQueriesDto.getEntityName());
	customQueries.setCriteriaDescription(customQueriesDto.getCriteriaDescription());
	customQueries.setCriteriaContent(customQueriesDto.getCriteriaContent());
	 return customQueriesRepository.save(customQueries);
	}
	
   @Transactional
   public void deleteById (String criteriaName){
	   customQueriesRepository.deleteById(criteriaName);

	}
	
   
   

}
