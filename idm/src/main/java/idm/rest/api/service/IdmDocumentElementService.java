package idm.rest.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import idm.data.model.*;
import idm.data.converter.*;
import idm.data.jpa.entities.*;
import idm.data.jpa.repositories.*;
import idm.data.jpa.repositories.custom.*;
import idm.data.metadata.core.SearchInfoDTO;
/**
 * 
 * 
 * @author london-databases
 *
 */
 

    @Component
	public class IdmDocumentElementService {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(IdmDocumentElementService.class);
 
    @Autowired
	IdmDocumentElementRepository idmDocumentElementRepository;

	@Autowired
    CustomizedIdmDocumentElementRepository customizedIdmDocumentElementRepository;

	
	@Transactional
	public IdmDocumentElement saveIdmDocumentElement (IdmDocumentElementDTO idmDocumentElementDTO){
	 IdmDocumentElement idmDocumentElement  = DTOToIdmDocumentElementConverter.convert(idmDocumentElementDTO);
	 return idmDocumentElementRepository.save(idmDocumentElement);
	}
	
	@Transactional
	public IdmDocumentElement save (IdmDocumentElement idmDocumentElement){
	 return idmDocumentElementRepository.save(idmDocumentElement);
	}

       
       
       
  
    @Transactional
   public Collection<IdmDocumentElement> findAll (){
	   return idmDocumentElementRepository.findAll();
	}

	@Transactional
    public Collection<IdmDocumentElementDTO> findAllCustom (SearchInfoDTO searchInfo){
    		return customizedIdmDocumentElementRepository.findByCustomCriteria(searchInfo);
    }
     
   @Transactional
   public IdmDocumentElement findById (Integer idmDocumentElementId){
	   Optional<IdmDocumentElement> idmDocumentElement  = idmDocumentElementRepository.findById(idmDocumentElementId);
	  return (idmDocumentElement.isPresent() ? idmDocumentElement.get() : null);
	}
	
   @Transactional
   public IdmDocumentElement updateIdmDocumentElement (IdmDocumentElement idmDocumentElement,IdmDocumentElementDTO idmDocumentElementDto){
	
	idmDocumentElement.setTradeType(idmDocumentElementDto.getTradeType());
	idmDocumentElement.setRegion(idmDocumentElementDto.getRegion());
	idmDocumentElement.setUnderlying(idmDocumentElementDto.getUnderlying());
	idmDocumentElement.setValuationDate(idmDocumentElementDto.getValuationDate());
	idmDocumentElement.setElementType(idmDocumentElementDto.getElementType());
	idmDocumentElement.setPayload(idmDocumentElementDto.getPayload());
	idmDocumentElement.setIdmDocumentId(idmDocumentElementDto.getIdmDocumentId());
	 return idmDocumentElementRepository.save(idmDocumentElement);
	}
	
   @Transactional
   public void deleteById (Integer idmDocumentElementId){
	   idmDocumentElementRepository.deleteById(idmDocumentElementId);

	}
	
   
   

}
