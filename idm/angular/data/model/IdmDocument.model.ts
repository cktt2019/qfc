
 

import {IdmDocumentElement} from '../model/IdmDocumentElement.model';
import {IdmDocumentVersion} from '../model/IdmDocumentVersion.model';

	export class IdmDocument  {

 	    documentId : number;

 	    region : string;

 	    underlying : string;

 	    tradeType : string;

 	    valuationDate : any;

 	    version : string;

 	    calibrator : string;

 	    extractedUnderlying : string;

 	    payload : any;

 	    status : string;


    

idmDocumentElement :  Array<IdmDocumentElement> ;

idmDocumentVersion :  Array<Idm_document_version> ;



}
