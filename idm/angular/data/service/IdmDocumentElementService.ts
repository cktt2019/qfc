import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { firstValueFrom, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { SearchCriteria } from '../model/SearchCriteria.model';
import { SearchInfo } from '../model/SearchInfo.model';
import { SystemParams } from '../model/SystemParams.model';
import { IdmDocumentElement } from '../model/IdmDocumentElement.model';
import {IdmDocument} from '../model/IdmDocument.model';




@Injectable({
  providedIn: 'root',
})
export class IdmDocumentElementService {

    constructor(
    private http: HttpClient
    ) {}

	private IdmDocumentElementUrl = 'http://localhost:8081/api/api/idmdocumentelements';

	private selectedIdmDocumentElement : IdmDocumentElement;

    idmDocumentElementDetailViewComponent : any;

      setSelectedIdmDocumentElement(selectedItem : IdmDocumentElement){
        this.selectedIdmDocumentElement = selectedItem;
        console.log('setSelectedIdmDocumentElement' + selectedItem);
      }

    getSelectedIdmDocumentElement () : IdmDocumentElement{
        return this.selectedIdmDocumentElement ;
      }
	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

       
       



    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the Promise result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }

            //GetAll Entity Level
             findAll () : Promise<IdmDocumentElement[]>{
            	   const url = SystemParams.baseUrl + '/idmdocumentelements';
            	   return firstValueFrom(this.http.get<IdmDocumentElement[]>(this.IdmDocumentElementUrl, this.httpOptions).pipe(
            	   tap(_ => console.log('fetched IdmDocumentElement(s)')),
                               catchError(this.handleError<IdmDocumentElement[]>('findAll', []))
                               ));
    	}


    		//GetOne Entity Level
    		   findById (idmDocumentElementId : number): Promise<IdmDocumentElement >{

            	    const url = SystemParams.baseUrl + `/idmdocumentelements/${idmDocumentElementId}`;
                  return firstValueFrom(this.http.get<IdmDocumentElement>(url, this.httpOptions).pipe(
                    tap(_ => console.log(`fetched IdmDocumentElement with idmDocumentElementId=${idmDocumentElementId}`)),
                    catchError(this.handleError<IdmDocumentElement>(`IdmDocumentElement idmDocumentElementId=${idmDocumentElementId}`))
                  ));

    	}

        //Custom Search
         findByCustomSearch (criteria : SearchInfo) : Promise<IdmDocumentElement[]>{
            	   const url = SystemParams.baseUrl +  '/IdmDocumentElement/custom';
            	   return firstValueFrom(this.http.post<IdmDocumentElement[]>(this.IdmDocumentElementUrl, criteria,  this.httpOptions).pipe(
            	   tap(_ => console.log('fetched IdmDocumentElement(s)')),
                               catchError(this.handleError<IdmDocumentElement[]>('findAll', []))
                               ));
                }
             //POST ONE Entity Level
              createIdmDocumentElement (idmDocumentElement : IdmDocumentElement  ) {
            const url = this.IdmDocumentElementUrl;
            this.http.post<IdmDocumentElement>(url, idmDocumentElement , this.httpOptions).subscribe(
            		res => {
            			console.log(res);
            		});
    	}
    	      //PUT ONE Entity Level
                    updateIdmDocumentElementById (idmDocumentElement : IdmDocumentElement ,idmDocumentElementId : number ) {
                                    const url = SystemParams.baseUrl + `/idmdocumentelements/${idmDocumentElementId}`;
                                    this.http.put<IdmDocumentElement>(url, idmDocumentElement , this.httpOptions).subscribe(
                                    		res => {
                                    			console.log(res);
                                    		});
    	}
    	     //DeleteOne Entity Level
    	     deleteById (idmDocumentElementId : number){

                                 	    const url = SystemParams.baseUrl + `/idmdocumentelements/${idmDocumentElementId}`;
                                       return firstValueFrom(this.http.delete(url, this.httpOptions).pipe(
                                         tap(_ => console.log(`fetched IdmDocumentElement with idmDocumentElementId=${idmDocumentElementId}`)),
                                         catchError(this.handleError<IdmDocumentElement>(`IdmDocumentElement idmDocumentElementId=${idmDocumentElementId}`))
                                       ));

            }
     }
