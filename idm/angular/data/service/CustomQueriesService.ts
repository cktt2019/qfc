import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { firstValueFrom, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { SearchCriteria } from '../model/SearchCriteria.model';
import { SearchInfo } from '../model/SearchInfo.model';
import { SystemParams } from '../model/SystemParams.model';
import { CustomQueries } from '../model/CustomQueries.model';




@Injectable({
  providedIn: 'root',
})
export class CustomQueriesService {

    constructor(
    private http: HttpClient
    ) {}

	private CustomQueriesUrl = 'http://localhost:8081/api/api/customqueriess';
	//Optional
	private CustomQueriesListUrl = 'http://localhost:8081/api/api/customquerieslist';

	private selectedCustomQueries : CustomQueries;

    customQueriesDetailViewComponent : any;

      setSelectedCustomQueries(selectedItem : CustomQueries){
        this.selectedCustomQueries = selectedItem;
        console.log('setSelectedCustomQueries' + selectedItem);
      }

    getSelectedCustomQueries () : CustomQueries{
        return this.selectedCustomQueries ;
      }
	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

       
       

  findQueriesList(entityName : string) : Promise<string[]>{
        const url = `${this.CustomQueriesListUrl}/${entityName}`;
        return firstValueFrom(this.http.get<string[]>(url, this.httpOptions).pipe(
        tap(_ => console.log('fetched findQueriesList')),
                      catchError(this.handleError<string[]>('findAll', []))
                      ));
      }


    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the Promise result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }

            //GetAll Entity Level
             findAll () : Promise<CustomQueries[]>{
            	   const url = SystemParams.baseUrl + '/customqueries';
            	   return firstValueFrom(this.http.get<CustomQueries[]>(this.CustomQueriesUrl, this.httpOptions).pipe(
            	   tap(_ => console.log('fetched CustomQueries(s)')),
                               catchError(this.handleError<CustomQueries[]>('findAll', []))
                               ));
    	}


    		//GetOne Entity Level
    		   findById (criteriaName : String): Promise<CustomQueries >{

            	    const url = SystemParams.baseUrl + `/customqueries/${criteriaName}`;
                  return firstValueFrom(this.http.get<CustomQueries>(url, this.httpOptions).pipe(
                    tap(_ => console.log(`fetched CustomQueries with criteriaName=${criteriaName}`)),
                    catchError(this.handleError<CustomQueries>(`CustomQueries criteriaName=${criteriaName}`))
                  ));

    	}

        //Custom Search
         findByCustomSearch (criteria : SearchInfo) : Promise<CustomQueries[]>{
            	   const url = SystemParams.baseUrl +  '/CustomQueries/custom';
            	   return firstValueFrom(this.http.post<CustomQueries[]>(this.CustomQueriesUrl, criteria,  this.httpOptions).pipe(
            	   tap(_ => console.log('fetched CustomQueries(s)')),
                               catchError(this.handleError<CustomQueries[]>('findAll', []))
                               ));
                }
             //POST ONE Entity Level
              createCustomQueries (customQueries : CustomQueries  ) {
            const url = this.CustomQueriesUrl;
            this.http.post<CustomQueries>(url, customQueries , this.httpOptions).subscribe(
            		res => {
            			console.log(res);
            		});
    	}
    	      //PUT ONE Entity Level
                    updateCustomQueriesById (customQueries : CustomQueries ,criteriaName : String ) {
                                    const url = SystemParams.baseUrl + `/customqueries/${criteriaName}`;
                                    this.http.put<CustomQueries>(url, customQueries , this.httpOptions).subscribe(
                                    		res => {
                                    			console.log(res);
                                    		});
    	}
    	     //DeleteOne Entity Level
    	     deleteById (criteriaName : String){

                                 	    const url = SystemParams.baseUrl + `/customqueries/${criteriaName}`;
                                       return firstValueFrom(this.http.delete(url, this.httpOptions).pipe(
                                         tap(_ => console.log(`fetched CustomQueries with criteriaName=${criteriaName}`)),
                                         catchError(this.handleError<CustomQueries>(`CustomQueries criteriaName=${criteriaName}`))
                                       ));

            }
     }
