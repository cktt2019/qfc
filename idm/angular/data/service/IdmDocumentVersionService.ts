import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { firstValueFrom, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { SearchCriteria } from '../model/SearchCriteria.model';
import { SearchInfo } from '../model/SearchInfo.model';
import { SystemParams } from '../model/SystemParams.model';
import { IdmDocumentVersion } from '../model/IdmDocumentVersion.model';
import {IdmDocument} from '../model/IdmDocument.model';




@Injectable({
  providedIn: 'root',
})
export class IdmDocumentVersionService {

    constructor(
    private http: HttpClient
    ) {}

	private IdmDocumentVersionUrl = 'http://localhost:8081/api/api/idmdocumentversions';

	private selectedIdmDocumentVersion : IdmDocumentVersion;

    idmDocumentVersionDetailViewComponent : any;

      setSelectedIdmDocumentVersion(selectedItem : IdmDocumentVersion){
        this.selectedIdmDocumentVersion = selectedItem;
        console.log('setSelectedIdmDocumentVersion' + selectedItem);
      }

    getSelectedIdmDocumentVersion () : IdmDocumentVersion{
        return this.selectedIdmDocumentVersion ;
      }
	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

       
       



    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the Promise result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }

            //GetAll Entity Level
             findAll () : Promise<IdmDocumentVersion[]>{
            	   const url = SystemParams.baseUrl + '/idmdocumentversions';
            	   return firstValueFrom(this.http.get<IdmDocumentVersion[]>(this.IdmDocumentVersionUrl, this.httpOptions).pipe(
            	   tap(_ => console.log('fetched IdmDocumentVersion(s)')),
                               catchError(this.handleError<IdmDocumentVersion[]>('findAll', []))
                               ));
    	}


    		//GetOne Entity Level
    		   findById (idmDocumentId : number,idmDocumentVersion : String): Promise<IdmDocumentVersion >{

            	    const url = SystemParams.baseUrl + `/idmdocumentversions/${idmDocumentId}/${idmDocumentVersion}`;
                  return firstValueFrom(this.http.get<IdmDocumentVersion>(url, this.httpOptions).pipe(
                    tap(_ => console.log(`fetched IdmDocumentVersion with idmDocumentId        , idmDocumentVersion=${idmDocumentId        , idmDocumentVersion}`)),
                    catchError(this.handleError<IdmDocumentVersion>(`IdmDocumentVersion idmDocumentId        , idmDocumentVersion=${idmDocumentId        , idmDocumentVersion}`))
                  ));

    	}

        //Custom Search
         findByCustomSearch (criteria : SearchInfo) : Promise<IdmDocumentVersion[]>{
            	   const url = SystemParams.baseUrl +  '/IdmDocumentVersion/custom';
            	   return firstValueFrom(this.http.post<IdmDocumentVersion[]>(this.IdmDocumentVersionUrl, criteria,  this.httpOptions).pipe(
            	   tap(_ => console.log('fetched IdmDocumentVersion(s)')),
                               catchError(this.handleError<IdmDocumentVersion[]>('findAll', []))
                               ));
                }
             //POST ONE Entity Level
              createIdmDocumentVersion (idmDocumentVersion : IdmDocumentVersion  ) {
            const url = this.IdmDocumentVersionUrl;
            this.http.post<IdmDocumentVersion>(url, idmDocumentVersion , this.httpOptions).subscribe(
            		res => {
            			console.log(res);
            		});
    	}
    	      //PUT ONE Entity Level
                    updateIdmDocumentVersionById (idmDocumentVersion : IdmDocumentVersion ,idmDocumentId : number,idmDocumentVersion : String ) {
                                    const url = SystemParams.baseUrl + `/idmdocumentversions/${idmDocumentId}/${idmDocumentVersion}`;
                                    this.http.put<IdmDocumentVersion>(url, idmDocumentVersion , this.httpOptions).subscribe(
                                    		res => {
                                    			console.log(res);
                                    		});
    	}
    	     //DeleteOne Entity Level
    	     deleteById (idmDocumentId : number,idmDocumentVersion : String){

                                 	    const url = SystemParams.baseUrl + `/idmdocumentversions/${idmDocumentId}/${idmDocumentVersion}`;
                                       return firstValueFrom(this.http.delete(url, this.httpOptions).pipe(
                                         tap(_ => console.log(`fetched IdmDocumentVersion with idmDocumentId        , idmDocumentVersion=${idmDocumentId        , idmDocumentVersion}`)),
                                         catchError(this.handleError<IdmDocumentVersion>(`IdmDocumentVersion idmDocumentId        , idmDocumentVersion=${idmDocumentId        , idmDocumentVersion}`))
                                       ));

            }
     }
