import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { firstValueFrom, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { SearchCriteria } from '../model/SearchCriteria.model';
import { SearchInfo } from '../model/SearchInfo.model';
import { SystemParams } from '../model/SystemParams.model';
import { IdmDocument } from '../model/IdmDocument.model';


import {IdmDocumentElement} from '../model/IdmDocumentElement.model';
import {IdmDocumentVersion} from '../model/IdmDocumentVersion.model';


@Injectable({
  providedIn: 'root',
})
export class IdmDocumentService {

    constructor(
    private http: HttpClient
    ) {}

	private IdmDocumentUrl = 'http://localhost:8081/api/api/idmdocuments';

	private selectedIdmDocument : IdmDocument;

    idmDocumentDetailViewComponent : any;

      setSelectedIdmDocument(selectedItem : IdmDocument){
        this.selectedIdmDocument = selectedItem;
        console.log('setSelectedIdmDocument' + selectedItem);
      }

    getSelectedIdmDocument () : IdmDocument{
        return this.selectedIdmDocument ;
      }
	httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

       
       



    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the Promise result
     */
    private handleError<T>(operation = 'operation', result?: T) {

      return (error: any): Observable<T> => {
      console.error(error); // log to console instead
        console.error(operation + "  failed: " + error.message);
        return of(result as T);
      };

    }

            //ONE_TO_MANY RELL GET
             getIdmdocumentsRelatedIdmdocumentversions (documentId : number) : Promise<Array<IdmDocumentVersion>>{
                        	   const url = SystemParams.baseUrl + `/idmdocuments/${documentId}/idmdocumentversions`;
                        	   return firstValueFrom(this.http.get<Array<IdmDocumentVersion>>(url, this.httpOptions).pipe(
                        	   tap(_ => console.log('fetched IdmDocument(s)')),
                                           catchError(this.handleError<Array<IdmDocumentVersion>>('findAll', []))
                                           ));
    	}

    		//ONE_TO_MANY RELL POST
    		createIdmdocumentsRelatedIdmdocumentversions (documentId : number  , idmDocumentVersion : Array<IdmDocumentVersion>){
                                                    	   const url = SystemParams.baseUrl + `/idmdocuments/${documentId}/idmdocumentversions`;
                                                    	   return firstValueFrom(this.http.post<Array<IdmDocumentVersion>>(url,idmDocumentVersion, this.httpOptions).pipe(
                                                    	   tap(_ => console.log('fetched IdmDocument(s)')),
                                                                       catchError(this.handleError<Array<IdmDocumentVersion>>('findAll', []))
                                                                       ));
    	}
    		//ONE_TO_MANY RELL PUT
    			putIdmdocumentsRelatedIdmdocumentversions (documentId : number  , idmDocumentVersion : Array<IdmDocumentVersion>){
                                                                	   const url = SystemParams.baseUrl + `/idmdocuments/${documentId}/idmdocumentversions`;
                                                                	   return firstValueFrom(this.http.put<Array<IdmDocumentVersion>>(url,idmDocumentVersion, this.httpOptions).pipe(
                                                                	   tap(_ => console.log('fetched IdmDocument(s)')),
                                                                                   catchError(this.handleError<IdmDocument[]>('findAll', []))
                                                                                   ));

    	}

    		//ONE_TO_MANY RELL DELETE
    		deleteIdmdocumentsRelatedIdmdocumentversions (documentId : number  , idmDocumentVersion : Array<IdmDocumentVersion>){
                                                                	   const url = SystemParams.baseUrl + `/idmdocuments/${documentId}/idmdocumentversions`;
                                                                	   return firstValueFrom(this.http.delete(url,idmDocumentVersion, this.httpOptions).pipe(
                                                                	   tap(_ => console.log('fetched IdmDocument(s)')),
                                                                                   catchError(this.handleError<IdmDocument[]>('findAll', []))
                                                                                   ));

            }

            //GetAll Entity Level
             findAll () : Promise<IdmDocument[]>{
            	   const url = SystemParams.baseUrl + '/idmdocuments';
            	   return firstValueFrom(this.http.get<IdmDocument[]>(this.IdmDocumentUrl, this.httpOptions).pipe(
            	   tap(_ => console.log('fetched IdmDocument(s)')),
                               catchError(this.handleError<IdmDocument[]>('findAll', []))
                               ));
    	}

            //ONE_TO_MANY RELL GET
             getIdmdocumentsRelatedIdmdocumentelements (documentId : number) : Promise<Array<IdmDocumentElement>>{
                        	   const url = SystemParams.baseUrl + `/idmdocuments/${documentId}/idmdocumentelements`;
                        	   return firstValueFrom(this.http.get<Array<IdmDocumentElement>>(url, this.httpOptions).pipe(
                        	   tap(_ => console.log('fetched IdmDocument(s)')),
                                           catchError(this.handleError<Array<IdmDocumentElement>>('findAll', []))
                                           ));
    	}

    		//ONE_TO_MANY RELL POST
    		createIdmdocumentsRelatedIdmdocumentelements (documentId : number  , idmDocumentElement : Array<IdmDocumentElement>){
                                                    	   const url = SystemParams.baseUrl + `/idmdocuments/${documentId}/idmdocumentelements`;
                                                    	   return firstValueFrom(this.http.post<Array<IdmDocumentElement>>(url,idmDocumentElement, this.httpOptions).pipe(
                                                    	   tap(_ => console.log('fetched IdmDocument(s)')),
                                                                       catchError(this.handleError<Array<IdmDocumentElement>>('findAll', []))
                                                                       ));
    	}
    		//ONE_TO_MANY RELL PUT
    			putIdmdocumentsRelatedIdmdocumentelements (documentId : number  , idmDocumentElement : Array<IdmDocumentElement>){
                                                                	   const url = SystemParams.baseUrl + `/idmdocuments/${documentId}/idmdocumentelements`;
                                                                	   return firstValueFrom(this.http.put<Array<IdmDocumentElement>>(url,idmDocumentElement, this.httpOptions).pipe(
                                                                	   tap(_ => console.log('fetched IdmDocument(s)')),
                                                                                   catchError(this.handleError<IdmDocument[]>('findAll', []))
                                                                                   ));

    	}

    		//ONE_TO_MANY RELL DELETE
    		deleteIdmdocumentsRelatedIdmdocumentelements (documentId : number  , idmDocumentElement : Array<IdmDocumentElement>){
                                                                	   const url = SystemParams.baseUrl + `/idmdocuments/${documentId}/idmdocumentelements`;
                                                                	   return firstValueFrom(this.http.delete(url,idmDocumentElement, this.httpOptions).pipe(
                                                                	   tap(_ => console.log('fetched IdmDocument(s)')),
                                                                                   catchError(this.handleError<IdmDocument[]>('findAll', []))
                                                                                   ));

            }


    		//GetOne Entity Level
    		   findById (documentId : number): Promise<IdmDocument >{

            	    const url = SystemParams.baseUrl + `/idmdocuments/${documentId}`;
                  return firstValueFrom(this.http.get<IdmDocument>(url, this.httpOptions).pipe(
                    tap(_ => console.log(`fetched IdmDocument with documentId=${documentId}`)),
                    catchError(this.handleError<IdmDocument>(`IdmDocument documentId=${documentId}`))
                  ));

    	}

        //Custom Search
         findByCustomSearch (criteria : SearchInfo) : Promise<IdmDocument[]>{
            	   const url = SystemParams.baseUrl +  '/IdmDocument/custom';
            	   return firstValueFrom(this.http.post<IdmDocument[]>(this.IdmDocumentUrl, criteria,  this.httpOptions).pipe(
            	   tap(_ => console.log('fetched IdmDocument(s)')),
                               catchError(this.handleError<IdmDocument[]>('findAll', []))
                               ));
                }
             //POST ONE Entity Level
              createIdmDocument (idmDocument : IdmDocument  ) {
            const url = this.IdmDocumentUrl;
            this.http.post<IdmDocument>(url, idmDocument , this.httpOptions).subscribe(
            		res => {
            			console.log(res);
            		});
    	}
    	      //PUT ONE Entity Level
                    updateIdmDocumentById (idmDocument : IdmDocument ,documentId : number ) {
                                    const url = SystemParams.baseUrl + `/idmdocuments/${documentId}`;
                                    this.http.put<IdmDocument>(url, idmDocument , this.httpOptions).subscribe(
                                    		res => {
                                    			console.log(res);
                                    		});
    	}
    	     //DeleteOne Entity Level
    	     deleteById (documentId : number){

                                 	    const url = SystemParams.baseUrl + `/idmdocuments/${documentId}`;
                                       return firstValueFrom(this.http.delete(url, this.httpOptions).pipe(
                                         tap(_ => console.log(`fetched IdmDocument with documentId=${documentId}`)),
                                         catchError(this.handleError<IdmDocument>(`IdmDocument documentId=${documentId}`))
                                       ));

            }
     }
