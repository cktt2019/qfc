export class IdmDocument {

    documentId: number;

    region: string;

    underlying: string;

    tradeType: string;

    valuationDate: any;

    version: string;

    calibrator: string;

    extractedUnderlying: string;

    payload: any;


}
