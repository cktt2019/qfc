package idm.rest.api.service;

import idm.data.converter.*;
import idm.data.jpa.entities.*;
import idm.data.jpa.repositories.*;
import idm.data.jpa.repositories.custom.*;
import idm.data.metadata.core.SearchInfoDTO;
import idm.data.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * @author london-databases
 */


@Component
public class IdmDocumentService {


    private static final Logger LOG = LoggerFactory.getLogger(IdmDocumentService.class);

    @Autowired
    IdmDocumentRepository idmDocumentRepository;

    @Autowired
    CustomizedIdmDocumentRepository customizedIdmDocumentRepository;


    @Transactional
    public IdmDocument saveIdmDocument(IdmDocumentDTO idmDocumentDTO) {
        IdmDocument idmDocument = DTOToIdmDocumentConverter.convert(idmDocumentDTO);
        return idmDocumentRepository.save(idmDocument);
    }

    @Transactional
    public IdmDocument save(IdmDocument idmDocument) {
        return idmDocumentRepository.save(idmDocument);
    }


    @Transactional
    public Collection<IdmDocument> findAll() {
        return idmDocumentRepository.findAll();
    }

    @Transactional
    public Collection<IdmDocumentDTO> findAllCustom(SearchInfoDTO searchInfo) {
        return customizedIdmDocumentRepository.findByCustomCriteria(searchInfo);
    }

    @Transactional
    public IdmDocument findById(Integer documentId) {
        Optional<IdmDocument> idmDocument = idmDocumentRepository.findById(documentId);
        return (idmDocument.isPresent() ? idmDocument.get() : null);
    }

    @Transactional
    public IdmDocument updateIdmDocument(IdmDocument idmDocument, IdmDocumentDTO idmDocumentDto) {

        idmDocument.setRegion(idmDocumentDto.getRegion());
        idmDocument.setUnderlying(idmDocumentDto.getUnderlying());
        idmDocument.setTradeType(idmDocumentDto.getTradeType());
        idmDocument.setValuationDate(idmDocumentDto.getValuationDate());
        idmDocument.setVersion(idmDocumentDto.getVersion());
        idmDocument.setCalibrator(idmDocumentDto.getCalibrator());
        idmDocument.setExtractedUnderlying(idmDocumentDto.getExtractedUnderlying());
        idmDocument.setPayload(idmDocumentDto.getPayload());
        return idmDocumentRepository.save(idmDocument);
    }

    @Transactional
    public void deleteById(Integer documentId) {
        idmDocumentRepository.deleteById(documentId);

    }


}
