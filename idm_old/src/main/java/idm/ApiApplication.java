package idm;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class ApiApplication {

    public static final String PROPERTIES_FILE_PATH = "properties.file.path";
    public static final String INT_CONFIG_LOCATION = "config.yaml";
    public static final String PORT_WEB = "PORT_WEB";
    private static final Logger LOG = LoggerFactory.getLogger(ApiApplication.class);
    private static final String SPRING_CONFIG_LOCATION = "spring.config.location";
    private static final String SPRING_SERVER_PORT = "server.port";
    private static final String FILE = "file:";

    public static void main(String[] args) {

        intialiseConfigLocation();
        SpringApplication.run(ApiApplication.class, args);
    }

    private static void intialiseConfigLocation() {

        Integer serverPort = NumberUtils.toInt(System.getProperty(PORT_WEB));
        if (serverPort == null || serverPort <= 0) {
            serverPort = 8081;
        }
        LOG.info(PORT_WEB + " = " + serverPort);
        System.setProperty(SPRING_SERVER_PORT, serverPort.toString());

        LOG.info(PROPERTIES_FILE_PATH + " = " + System.getProperty(PROPERTIES_FILE_PATH));
        if (!StringUtils.isBlank(System.getProperty(PROPERTIES_FILE_PATH))) {
            System.setProperty(SPRING_CONFIG_LOCATION, FILE + System.getProperty(PROPERTIES_FILE_PATH));
            LOG.info("Set " + SPRING_CONFIG_LOCATION + " to " + FILE + System.getProperty(PROPERTIES_FILE_PATH));
        } else if (StringUtils.isBlank(System.getProperty(SPRING_CONFIG_LOCATION))) {
            System.setProperty(SPRING_CONFIG_LOCATION, FILE + INT_CONFIG_LOCATION);
            LOG.info("Set " + SPRING_CONFIG_LOCATION + " ********************* to " + FILE + INT_CONFIG_LOCATION);
        }
    }

}
