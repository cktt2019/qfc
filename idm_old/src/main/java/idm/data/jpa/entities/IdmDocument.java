package idm.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "IdmDocument")
@Entity
public class IdmDocument implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idmdocument_seq")
    @SequenceGenerator(name = "idmdocument_seq", sequenceName = "idmdocument_seq")
    @Column(name = "document_id")
    private Integer documentId;


    @Column(name = "region")
    private String region;


    @Column(name = "underlying")
    private String underlying;


    @Column(name = "trade_type")
    private String tradeType;


    @Column(name = "valuation_date")
    private java.sql.Timestamp valuationDate;


    @Column(name = "version")
    private String version;


    @Column(name = "calibrator")
    private String calibrator;


    @Column(name = "extracted_underlying")
    private String extractedUnderlying;


    @Column(name = "payload")
    private java.sql.Clob payload;


}
