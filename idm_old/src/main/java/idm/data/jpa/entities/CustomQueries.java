package idm.data.jpa.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "CustomQueries")
@Entity
public class CustomQueries implements Serializable {

    @Column(name = "entityName")
    private String entityName;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customqueries_seq")
    @SequenceGenerator(name = "customqueries_seq", sequenceName = "customqueries_seq")
    @Column(name = "criteriaName")
    private String criteriaName;


    @Column(name = "criteriaDescription")
    private String criteriaDescription;


    @Column(name = "criteriaContent")
    private String criteriaContent;


}
