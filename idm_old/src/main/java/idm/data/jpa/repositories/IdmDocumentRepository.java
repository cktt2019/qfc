package idm.data.jpa.repositories;

import idm.data.jpa.entities.IdmDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Service interface with operations for {@link IdmDocument} persistence.
 *
 * @author ctanyitang
 */
@Repository
public interface IdmDocumentRepository extends JpaRepository<IdmDocument, Integer> {


    public List<IdmDocument> findByRegion(String region);

    public List<IdmDocument> findByUnderlying(String underlying);

    public List<IdmDocument> findByTradeType(String tradeType);

    public List<IdmDocument> findByValuationDate(java.sql.Timestamp valuationDate);

    public List<IdmDocument> findByVersion(String version);

    public List<IdmDocument> findByCalibrator(String calibrator);

    public List<IdmDocument> findByExtractedUnderlying(String extractedUnderlying);

    public List<IdmDocument> findByPayload(java.sql.Clob payload);


    public Page<IdmDocument> findAll(Pageable page);


}







