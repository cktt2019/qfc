package idm.data.jpa.repositories.custom;

import idm.data.jpa.entities.IdmDocument;
import idm.data.metadata.core.SearchInfoDTO;
import idm.data.model.IdmDocumentDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link IdmDocument} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedIdmDocumentRepository {

    public Collection<IdmDocumentDTO> findByCustomCriteria(SearchInfoDTO info);

}







