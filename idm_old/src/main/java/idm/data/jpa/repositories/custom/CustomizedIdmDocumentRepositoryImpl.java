package idm.data.jpa.repositories.custom;


import idm.data.jpa.entities.IdmDocument;
import idm.data.metadata.IdmDocumentMetaData;
import idm.data.metadata.core.PaginationInstructionDTO;
import idm.data.metadata.core.SearchInfoDTO;
import idm.data.metadata.core.SortInstructionDTO;
import idm.data.model.IdmDocumentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service interface with operations for {@link IdmDocument} .
 *
 * @author ctanyitang
 */

@Component
public class CustomizedIdmDocumentRepositoryImpl extends BaseCustomizedSearchRepository implements CustomizedIdmDocumentRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection<IdmDocumentDTO> findByCustomCriteria(SearchInfoDTO searchInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM " + IdmDocumentMetaData.TABLE + " WHERE ");
        createQuery(searchInfo.getCriteria(), sb, new IdmDocumentMetaData());

        if (searchInfo.getSorting() != null && searchInfo.getSorting().size() > 0) {
            sb.append(" ORDER BY ");
            createSortingClause(searchInfo.getSorting(), sb, new IdmDocumentMetaData());
        }

        if (searchInfo.getPaging() != null) {
            createPagingClause(searchInfo.getPaging(), sb);
        }

        String sql = sb.toString();

        Map<String, Object> paramValues = new HashMap<String, Object>();
        createParamValueMap(searchInfo.getCriteria(), paramValues, new IdmDocumentMetaData());
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValues(paramValues);


        List<IdmDocumentDTO> resp = namedParameterJdbcTemplate.query(sql, namedParameters,
                new IdmDocumentRowMapper());

        return resp;

    }

    private void createPagingClause(PaginationInstructionDTO pagination, StringBuilder sb) {
        if (pagination.getType().equalsIgnoreCase("offset")) {
            if (pagination.getOffset() != null && pagination.getLimit() != null) {
                sb.append("  OFFSET ").append(Integer.valueOf(pagination.getOffset())).append("  LIMIT  ")
                        .append(Integer.valueOf(pagination.getLimit()));
            }
        }

    }

    private void createSortingClause(List<SortInstructionDTO> sorting, StringBuilder sb,
                                     IdmDocumentMetaData metaData) {
        for (SortInstructionDTO sort : sorting) {
            if (sort.getDirection().equalsIgnoreCase("asc") || sort.getDirection().equalsIgnoreCase("desc"))
                sb.append(metaData.getColumnName(sort.getField())).append("  ")
                        .append(sort.getDirection().toUpperCase()).append("  ");
        }

    }

    static class IdmDocumentRowMapper implements RowMapper<IdmDocumentDTO> {
        @Override
        public IdmDocumentDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            IdmDocumentDTO ret = new IdmDocumentDTO();


            ret.setDocumentId(rs.getInt("document_id"));
            ret.setRegion(rs.getString("region"));
            ret.setUnderlying(rs.getString("underlying"));
            ret.setTradeType(rs.getString("trade_type"));
            ret.setValuationDate(rs.getTimestamp("valuation_date"));
            ret.setVersion(rs.getString("version"));
            ret.setCalibrator(rs.getString("calibrator"));
            ret.setExtractedUnderlying(rs.getString("extracted_underlying"));
            ret.setPayload(rs.getjava.sql.Clob("payload"));

            return ret;
        }
    }

}
