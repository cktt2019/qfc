package idm.data.jpa.repositories.custom;

import idm.data.jpa.entities.CustomQueries;
import idm.data.metadata.core.SearchInfoDTO;
import idm.data.model.CustomQueriesDTO;

import java.util.Collection;

/**
 * Service interface with operations for {@link CustomQueries} persistence.
 *
 * @author ctanyitang
 */

public interface CustomizedCustomQueriesRepository {

    public Collection<CustomQueriesDTO> findByCustomCriteria(SearchInfoDTO info);

}







