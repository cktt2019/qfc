package idm.data.jpa.repositories.custom;

import idm.data.metadata.MetaData;
import idm.data.metadata.core.SearchCriteriaDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

public class BaseCustomizedSearchRepository {

    public BaseCustomizedSearchRepository() {
        super();
    }

    protected void createQuery(SearchCriteriaDTO criteria, StringBuilder sb, MetaData meta) {
        if (criteria.getField() != null) {

            sb.append(meta.getColumnName(criteria.getField())).append(" ")
                    .append(mapComparator(criteria.getComparator())).append(" ").append(":")
                    .append(buildParameterName(criteria)).append(" ");
        } else {
            if (criteria.getExp() != null) {
                sb.append("( ");
                for (SearchCriteriaDTO c : criteria.getExp()) {
                    createQuery(c, sb, meta);
                }
                sb.append(" )");
            }
        }

        if (criteria.getLogicalOperator() != null && !criteria.getLogicalOperator().equals("END")) {
            // used to chain up to the next expressions, example when using a list, or just
            // chaining to another sub-expression
            sb.append(" ").append(criteria.getLogicalOperator()).append(" ");
        }
    }

    protected void createParamValueMap(SearchCriteriaDTO criteria, Map<String, Object> valueMap, MetaData meta) {
        if (criteria.getField() != null) {

            switch (meta.getColumnDataType(criteria.getField())) {
                case "String":
                    valueMap.put(buildParameterName(criteria), ((criteria.getValue())));
                    break;
                case "Float":
                    valueMap.put(buildParameterName(criteria), (Float.valueOf(criteria.getValue())));
                    break;
                case "Integer":
                    valueMap.put(buildParameterName(criteria), (Integer.valueOf(criteria.getValue())));
                    break;
                case "Long":
                    valueMap.put(buildParameterName(criteria), (Long.valueOf(criteria.getValue())));
                    break;
                case "Date":
                    // Note this assumes date format is YYYY-MM-DD
                    valueMap.put(buildParameterName(criteria), (LocalDate.parse(criteria.getValue())));
                    break;
                case "Boolean":
                    valueMap.put(buildParameterName(criteria), (Boolean.valueOf(criteria.getValue())));
                    break;
                case "BigDecimal":
                    valueMap.put(buildParameterName(criteria), (BigDecimal.valueOf(Integer.valueOf(criteria.getValue()))));
                    break;
                case "Double":
                    valueMap.put(buildParameterName(criteria), (Double.valueOf(criteria.getValue())));
                    break;

            }

        } else {
            if (criteria.getExp() != null) {

                for (SearchCriteriaDTO c : criteria.getExp()) {
                    createParamValueMap(c, valueMap, meta);
                }
            } else {
                createParamValueMap(criteria.getLeft(), valueMap, meta);
                createParamValueMap(criteria.getRight(), valueMap, meta);
            }

        }
    }

    private String buildParameterName(SearchCriteriaDTO criteria) {
        return criteria.getField() + "_" + criteria.getComparator() + "_" + criteria.getValue().hashCode();
    }

    private Object mapComparator(String comparator) {
        switch (comparator) {

            case "eq":
                return "=";
            case "neq":
                return "!=";
            case "lt":
                return "<";
            case "lte":
                return "<=";
            case "gt":
                return ">";
            case "gte":
                return ">=";
            case "bw":
                return "LIKE";
            case "ew":
                return "LIKE";
            case "ct":
                return "LIKE";
            case "in":
                return "IN";
            case "nin":
                return "NOT IN";

            default:
                return "=";
        }
    }

}
