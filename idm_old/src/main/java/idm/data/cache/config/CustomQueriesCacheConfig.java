package idm.data.cache.config;

import idm.data.model.CustomQueriesDTO;
import org.apache.ignite.cache.*;
import org.apache.ignite.cache.store.jdbc.*;
import org.apache.ignite.cache.store.jdbc.dialect.JdbcDialect;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.sql.Types;


@Component
public class CustomQueriesCacheConfig implements Serializable {

    @Autowired
    JdbcDialect jdbcDialect;


    public CacheConfiguration<String, CustomQueriesDTO> cacheConfiguration() {
        CacheConfiguration<String, CustomQueriesDTO> cacheCfg = new CacheConfiguration<String, CustomQueriesDTO>();

        cacheCfg.setName("CustomQueriesCache");
        cacheCfg.setCacheMode(CacheMode.PARTITIONED);
        cacheCfg.setAtomicityMode(CacheAtomicityMode.ATOMIC);
        cacheCfg.setReadThrough(true);
        cacheCfg.setWriteThrough(false);

        CustomQueriesCacheJdbcPojoStoreFactory factory = new CustomQueriesCacheJdbcPojoStoreFactory();
        factory.setDialect(jdbcDialect);

        JdbcType jdbcType = new JdbcType();
        jdbcType.setCacheName("CustomQueriesCache");
        jdbcType.setDatabaseTable("CustomQueries");
        jdbcType.setKeyType(String.class);
        jdbcType.setValueType(CustomQueriesDTO.class);

        jdbcType.setKeyFields(new JdbcTypeField(Types.VARCHAR, "criteriaName", String.class, "criteriaName"));

        jdbcType.setValueFields(
                new JdbcTypeField(Types.VARCHAR, "entityName", String.class, "entityName"),
                new JdbcTypeField(Types.VARCHAR, "criteriaName", String.class, "criteriaName"),
                new JdbcTypeField(Types.VARCHAR, "criteriaDescription", String.class, "criteriaDescription"),
                new JdbcTypeField(Types.VARCHAR, "criteriaContent", String.class, "criteriaContent")
        );

        factory.setTypes(jdbcType);
        cacheCfg.setCacheStoreFactory(factory);

        return cacheCfg;

    }

    private class CustomQueriesCacheJdbcPojoStoreFactory extends CacheJdbcPojoStoreFactory<String, CustomQueriesDTO> implements Serializable {
        @Override
        public CacheJdbcPojoStore<String, CustomQueriesDTO> create() {
            setDataSourceBean("dataSource");
            return super.create();
        }
    }

}
