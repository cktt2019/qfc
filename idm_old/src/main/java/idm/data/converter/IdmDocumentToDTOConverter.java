package idm.data.converter;


import idm.data.jpa.entities.IdmDocument;
import idm.data.model.IdmDocumentDTO;

/**
 * @author london-databases
 */


public class IdmDocumentToDTOConverter {


    public static IdmDocumentDTO convert(IdmDocument idmDocument) {
        if (idmDocument != null)
            return IdmDocumentDTO.builder().documentId(idmDocument.getDocumentId())
                    .region(idmDocument.getRegion())
                    .underlying(idmDocument.getUnderlying())
                    .tradeType(idmDocument.getTradeType())
                    .valuationDate(idmDocument.getValuationDate())
                    .version(idmDocument.getVersion())
                    .calibrator(idmDocument.getCalibrator())
                    .extractedUnderlying(idmDocument.getExtractedUnderlying())
                    .payload(idmDocument.getPayload())
                    .build();
        return null;
    }

}
