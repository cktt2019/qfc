package idm.data.converter;


import idm.data.jpa.entities.CustomQueries;
import idm.data.model.CustomQueriesDTO;

/**
 * @author london-databases
 */


public class CustomQueriesToDTOConverter {


    public static CustomQueriesDTO convert(CustomQueries customQueries) {
        if (customQueries != null)
            return CustomQueriesDTO.builder().entityName(customQueries.getEntityName())
                    .criteriaName(customQueries.getCriteriaName())
                    .criteriaDescription(customQueries.getCriteriaDescription())
                    .criteriaContent(customQueries.getCriteriaContent())
                    .build();
        return null;
    }

}
