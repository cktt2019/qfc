package idm.data.converter;


import idm.data.jpa.entities.CustomQueries;
import idm.data.model.CustomQueriesDTO;

/**
 * @author london-databases
 */


public class DTOToCustomQueriesConverter {


    public static CustomQueries convert(CustomQueriesDTO customQueriesDto) {
        if (customQueriesDto != null)
            return CustomQueries.builder().entityName(customQueriesDto.getEntityName())
                    .criteriaName(customQueriesDto.getCriteriaName())
                    .criteriaDescription(customQueriesDto.getCriteriaDescription())
                    .criteriaContent(customQueriesDto.getCriteriaContent())

                    .build();
        return null;
    }

}
