package idm.data.converter;


import idm.data.jpa.entities.IdmDocument;
import idm.data.model.IdmDocumentDTO;

/**
 * @author london-databases
 */


public class DTOToIdmDocumentConverter {


    public static IdmDocument convert(IdmDocumentDTO idmDocumentDto) {
        if (idmDocumentDto != null)
            return IdmDocument.builder().documentId(idmDocumentDto.getDocumentId())
                    .region(idmDocumentDto.getRegion())
                    .underlying(idmDocumentDto.getUnderlying())
                    .tradeType(idmDocumentDto.getTradeType())
                    .valuationDate(idmDocumentDto.getValuationDate())
                    .version(idmDocumentDto.getVersion())
                    .calibrator(idmDocumentDto.getCalibrator())
                    .extractedUnderlying(idmDocumentDto.getExtractedUnderlying())
                    .payload(idmDocumentDto.getPayload())

                    .build();
        return null;
    }

}
