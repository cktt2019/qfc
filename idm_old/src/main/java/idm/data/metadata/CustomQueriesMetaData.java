package idm.data.metadata;


/**
 * @author ctanyitang
 */


public class CustomQueriesMetaData implements MetaData {

    public static final String TABLE = "CustomQueries";
    private static final String ENTITYNAME = "entityName";
    private static final String CRITERIANAME = "criteriaName";
    private static final String CRITERIADESCRIPTION = "criteriaDescription";
    private static final String CRITERIACONTENT = "criteriaContent";

    @Override
    public String getColumnName(String property) {
        switch (property) {
            case ENTITYNAME:
                return "entityName";
            case CRITERIANAME:
                return "criteriaName";
            case CRITERIADESCRIPTION:
                return "criteriaDescription";
            case CRITERIACONTENT:
                return "criteriaContent";
        }
        return null;
    }


    @Override
    public String getColumnDataType(String property) {
        switch (property) {
            case ENTITYNAME:
                return "String";
            case CRITERIANAME:
                return "String";
            case CRITERIADESCRIPTION:
                return "String";
            case CRITERIACONTENT:
                return "String";
        }
        return null;
    }


}
