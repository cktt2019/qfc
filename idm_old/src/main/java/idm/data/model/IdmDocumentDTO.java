package idm.data.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author london-databases
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IdmDocumentDTO {
    private Integer documentId;
    private String region;
    private String underlying;
    private String tradeType;
    private java.sql.Timestamp valuationDate;
    private String version;
    private String calibrator;
    private String extractedUnderlying;
    private java.sql.Clob payload;


}
