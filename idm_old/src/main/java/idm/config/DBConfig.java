package idm.config;

import org.apache.ignite.cache.store.jdbc.dialect.JdbcDialect;
import org.apache.ignite.cache.store.jdbc.dialect.OracleDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DBConfig {

    @Bean
    public JdbcDialect getJdbcDialect() {
        OracleDialect dialect = new OracleDialect();
        dialect.setFetchSize(100000);
        return dialect;
    }

    /*
	@Bean
	@Primary
	@Qualifier("postgres")
	@ConfigurationProperties(prefix = "postgres.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean
	@Qualifier("embedded")
	public DataSource embeddedDataSource() {
		return DataSourceBuilder.create().driverClassName("org.h2.Driver").url("jdbc:h2:mem:restapi;DB_CLOSE_DELAY=-1").username("sa").build();
	}
	*/

}
