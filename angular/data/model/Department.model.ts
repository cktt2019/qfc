export interface Department {

    departmentId: string;

    businessControllerId: string;

    controllerId: string;

    departmentCategoryCode: string;

    departmentCategoryName: string;

    departmentDormantIndicator: string;

    departmentLongName: string;

    departmentName: string;

    departmentReconcilerGroupName: string;

    departmentStatus: string;

    departmentTraderId: string;

    departmentValidFromDate: any;

    departmentValidToDate: any;

    financialBusinessUnitCode: string;

    ivrGroup: string;

    l1SupervisorId: string;

    misOfficeCode: string;

    misOfficeName: string;

    ownerL2Id: string;

    recAndAdjControllerId: string;

    recAndAdjSupervisorId: string;

    reconcilerId: string;

    regionCode: string;

    regionDescription: string;

    revenueType: string;

    revenueTypeDescription: string;

    riskPortfolioId: string;

    specialisationIndicator: string;

    taxCategory: string;

    volkerRuleDescription: string;

    volkerRuleIndicator: string;

    fxCentrallyMaangedName: string;

    volckerSubDivision: string;

    volckerSubDivisionDescription: string;

    ihcHcdFlag: string;

    coveredDeptFlag: string;

    deltaFlag: string;

    remittancePolicyCode: string;

    firstInstanceDeptActivated: string;

    firstInstanceDeptInactivated: string;

    lastInstanceDeptActivated: string;

    lastInstanceDeptInactivated: string;

    rbpnlFlag: string;

    ivrGroupLongName: string;

    frtbDeptCategory: string;


}
