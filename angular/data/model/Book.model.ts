export interface Book {

    globalBookId: string;

    accountMethod: string;

    accountingBasisIndicator: string;

    bookCategoryCode: string;

    bookCategoryDescription: string;

    bookControllerId: string;

    bookDormantIndicator: string;

    bookName: string;

    bookPnlSystemName: string;

    bookRef: string;

    bookStatus: string;

    bookSubCategoryCode: string;

    bookSubCategoryDescription: string;

    bookTraderId: string;

    cnyDeliverableOutsideChinaIndicator: string;

    departmentId: string;

    frontOfficeRepresentativeId: string;

    gplAtomCode: string;

    nhfsIndicator: string;

    remoteBookingPolicyIndicator: string;

    splitHedgeComments: string;

    splitHedgeIndicator: string;

    usTaxCategoryCode: string;

    bookLastUpdatedDatetime: any;

    financialBusinessUnitCode: string;

    deltaFlag: string;

    bookRequestStatusCode: string;

    firstInstanceBookActivated: any;

    lastInstanceBookActivated: any;

    firstInstanceBookInactivated: any;

    lastInstanceBook: any;

    emailAddress: string;

    cavFlag: string;

    fosFlag: string;

    bookingIntent: string;

    bookRequestId: string;

    iparEmplIdEmailId: string;

    bookTraderEmailId: string;

    pcSupervisorEmailId: string;

    bookEmpIdEmailId: string;

    pctnlEmplIdEmailId: string;


}
