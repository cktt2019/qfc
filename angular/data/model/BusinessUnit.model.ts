export interface BusinessUnit {

    financialBusinessUnitCode: string;

    baseCurrency: string;

    businessUnitActiveIndicator: string;

    businessUnitDescription: string;

    businessUnitEffectiveDate: any;

    businessUnitGermanDescription: string;

    businessUnitGermanName: string;

    businessUnitId: string;

    businessUnitLastUpdateDatetime: any;

    businessUnitLastUpdatedBy: string;

    businessUnitName: string;

    businessUnitShortDescription: string;

    businessUnitShortGermanDescription: string;

    chBusinessUnitLastUpdatedBy: string;

    chBusinessUnitLastUpdatedDatetime: any;

    customerId: string;

    customerVendorAffiliateIndicator: string;

    financialBusinessUnitActiveIndicator: string;

    financialBusinessUnitEffectiveDate: any;

    financialBusinessUnitGroupId: string;

    financialBusinessUnitGroupName: string;

    financialBusinessUnitName: string;

    firmType: string;

    isoCurrencyCode: string;

    lcdRefNo: string;

    legalEntityShortDescription: string;

    legalEntityActiveIndicator: string;

    legalEntityCode: string;

    legalEntityDescription: string;

    legalEntityEffectiveDate: any;

    legalEntityGermanDescription: string;

    legalEntityLastUpdatedBy: string;

    legalEntityLastUpdatedDatetime: any;

    localGaapCurrency: string;

    misMemberCode: string;

    processingLocationCode: string;

    sourceId: string;

    deltaFlag: string;

    buDomicile: string;


}
