export interface EmployeeRole {

    employeeId: string;

    employeeActiveIndicator: string;

    employeeEffectiveDate: any;

    employeeEmailId: string;

    employeeFirstName: string;

    employeeLastName: string;

    employeeLastUpdatedBy: string;

    employeeLastUpdatedDatetime: string;

    employeeLogonDomain: string;

    employeeLogonId: string;

    employeePidId: string;

    city: string;

    region: string;

    countryLongDesc: string;

    deltaFlag: string;


}
