export interface DTPMISGBLDYTREE {

    misMemberCode: string;

    americasProductLineHeadId: string;

    americasClusterHeadId: string;

    apacClusterHeadId: string;

    apacProductLineHeadId: string;

    businessControllerL2Id: string;

    cluster: string;

    clusterCode: string;

    clusterHeadId: string;

    desk: string;

    deskCode: string;

    division: string;

    divisionCode: string;

    euroClusterHeadId: string;

    euroProductLineHeadId: string;

    globalProductLineHeadId: string;

    hierarchyTraderId: string;

    misHierarchySortCode: string;

    misMemberName: string;

    misMemberTypeCode: string;

    misProduct: string;

    misRegionCode: string;

    misTreeCode: string;

    parentMisMemberCode: string;

    productControlSectionMgrId: string;

    productLineCode: string;

    productLineName: string;

    sub3ProductLine: string;

    sub3ProductLineDescription: string;

    subClusterCode: string;

    subClusterName: string;

    subDivisionCode: string;

    subDivisionDescription: string;

    subProductDescription: string;

    subProductLine: string;

    swissProductLineHeadId: string;

    tradingSupervisorId: string;

    tradingSupervisorDelegateId: string;

    misProductCode: string;

    misUnitPercentOwned: string;

    misUnitPercentDescription: string;

    deltaFlag: string;

    cddsTreeNodeStatus: string;

    tradingSupport: string;

    tradeSupportManagerEmail: string;


}
