import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {Department} from './Department.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class DepartmentService {

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    private DepartmentUrl = 'http://localhost:8081/api/api/department';

    constructor(
        private http: HttpClient
    ) {
    }

    saveDepartment(department: Department): Observable<Department> {
        return this.http.post<Department>(this.DepartmentUrl, department, this.httpOptions).pipe(
            tap(_ => console.log(`saved Department ${department}`)),
            catchError(this.handleError<Department>('saveDepartment', department))
        );
    }

    findAll(): Observable<Department[]> {

        const url = `this.DepartmentUrl"}`;
        return this.http.get<Department[]>(this.DepartmentUrl, this.httpOptions).pipe(
            tap(_ => console.log('fetched Department(s)')),
            catchError(this.handleError<Department[]>('findAll', []))
        );
    }


    findById(departmentId: string): Observable<Department> {
        const url = `${this.DepartmentUrl}/${departmentId}`;
        return this.http.get<Department>(url, this.httpOptions).pipe(
            tap(_ => console.log(`fetched Department with departmentId=${departmentId}`)),
            catchError(this.handleError<Department>(`Department departmentId=${departmentId}`))
        );
    }


    updateDepartment(department: Department): Observable<any> {
        return this.http.put<Department>(this.DepartmentUrl, department, this.httpOptions).pipe(
            tap(_ => console.log(`updated Department ${department}`)),
            catchError(this.handleError<Department>(`Department =${department}`))
        );
    }


    deleteById(departmentId: string): Observable<any> {
        const url = `${this.DepartmentUrl}/${departmentId}`;
        return this.http.delete<Department>(url).pipe(
            tap(_ => console.log(`fetched Department with departmentId=${departmentId}`)),
            catchError(this.handleError<Department>(`Department departmentId=${departmentId}`))
        );


    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

        return (error: any): Observable<T> => {
            console.error(error); // log to console instead
            console.error(operation + "  failed: " + error.message);
            return of(result as T);
        };

    }


}
