import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {DTPMISGBLDYTREE} from './DTPMISGBLDYTREE.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class DTPMISGBLDYTREEService {

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    private DTPMISGBLDYTREEUrl = 'http://localhost:8081/api/api/dtpmisgbldytree';

    constructor(
        private http: HttpClient
    ) {
    }

    saveDTPMISGBLDYTREE(dTPMISGBLDYTREE: DTPMISGBLDYTREE): Observable<DTPMISGBLDYTREE> {
        return this.http.post<DTPMISGBLDYTREE>(this.DTPMISGBLDYTREEUrl, dTPMISGBLDYTREE, this.httpOptions).pipe(
            tap(_ => console.log(`saved DTPMISGBLDYTREE ${dTPMISGBLDYTREE}`)),
            catchError(this.handleError<DTPMISGBLDYTREE>('saveDTPMISGBLDYTREE', dTPMISGBLDYTREE))
        );
    }

    findAll(): Observable<DTPMISGBLDYTREE[]> {

        const url = `this.DTPMISGBLDYTREEUrl"}`;
        return this.http.get<DTPMISGBLDYTREE[]>(this.DTPMISGBLDYTREEUrl, this.httpOptions).pipe(
            tap(_ => console.log('fetched DTPMISGBLDYTREE(s)')),
            catchError(this.handleError<DTPMISGBLDYTREE[]>('findAll', []))
        );
    }


    findById(misMemberCode: string): Observable<DTPMISGBLDYTREE> {
        const url = `${this.DTPMISGBLDYTREEUrl}/${misMemberCode}`;
        return this.http.get<DTPMISGBLDYTREE>(url, this.httpOptions).pipe(
            tap(_ => console.log(`fetched DTPMISGBLDYTREE with misMemberCode=${misMemberCode}`)),
            catchError(this.handleError<DTPMISGBLDYTREE>(`DTPMISGBLDYTREE misMemberCode=${misMemberCode}`))
        );
    }


    updateDTPMISGBLDYTREE(dTPMISGBLDYTREE: DTPMISGBLDYTREE): Observable<any> {
        return this.http.put<DTPMISGBLDYTREE>(this.DTPMISGBLDYTREEUrl, dTPMISGBLDYTREE, this.httpOptions).pipe(
            tap(_ => console.log(`updated DTPMISGBLDYTREE ${dTPMISGBLDYTREE}`)),
            catchError(this.handleError<DTPMISGBLDYTREE>(`DTPMISGBLDYTREE =${dTPMISGBLDYTREE}`))
        );
    }


    deleteById(misMemberCode: string): Observable<any> {
        const url = `${this.DTPMISGBLDYTREEUrl}/${misMemberCode}`;
        return this.http.delete<DTPMISGBLDYTREE>(url).pipe(
            tap(_ => console.log(`fetched DTPMISGBLDYTREE with misMemberCode=${misMemberCode}`)),
            catchError(this.handleError<DTPMISGBLDYTREE>(`DTPMISGBLDYTREE misMemberCode=${misMemberCode}`))
        );


    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

        return (error: any): Observable<T> => {
            console.error(error); // log to console instead
            console.error(operation + "  failed: " + error.message);
            return of(result as T);
        };

    }


}
