import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {Book} from './Book.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class BookService {

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    private BookUrl = 'http://localhost:8081/api/api/book';

    constructor(
        private http: HttpClient
    ) {
    }

    saveBook(book: Book): Observable<Book> {
        return this.http.post<Book>(this.BookUrl, book, this.httpOptions).pipe(
            tap(_ => console.log(`saved Book ${book}`)),
            catchError(this.handleError<Book>('saveBook', book))
        );
    }

    findAll(): Observable<Book[]> {

        const url = `this.BookUrl"}`;
        return this.http.get<Book[]>(this.BookUrl, this.httpOptions).pipe(
            tap(_ => console.log('fetched Book(s)')),
            catchError(this.handleError<Book[]>('findAll', []))
        );
    }


    findById(globalBookId: string): Observable<Book> {
        const url = `${this.BookUrl}/${globalBookId}`;
        return this.http.get<Book>(url, this.httpOptions).pipe(
            tap(_ => console.log(`fetched Book with globalBookId=${globalBookId}`)),
            catchError(this.handleError<Book>(`Book globalBookId=${globalBookId}`))
        );
    }


    updateBook(book: Book): Observable<any> {
        return this.http.put<Book>(this.BookUrl, book, this.httpOptions).pipe(
            tap(_ => console.log(`updated Book ${book}`)),
            catchError(this.handleError<Book>(`Book =${book}`))
        );
    }


    deleteById(globalBookId: string): Observable<any> {
        const url = `${this.BookUrl}/${globalBookId}`;
        return this.http.delete<Book>(url).pipe(
            tap(_ => console.log(`fetched Book with globalBookId=${globalBookId}`)),
            catchError(this.handleError<Book>(`Book globalBookId=${globalBookId}`))
        );


    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

        return (error: any): Observable<T> => {
            console.error(error); // log to console instead
            console.error(operation + "  failed: " + error.message);
            return of(result as T);
        };

    }


}
