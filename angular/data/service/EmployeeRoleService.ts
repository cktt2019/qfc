import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {EmployeeRole} from './EmployeeRole.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class EmployeeRoleService {

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    private EmployeeRoleUrl = 'http://localhost:8081/api/api/employeerole';

    constructor(
        private http: HttpClient
    ) {
    }

    saveEmployeeRole(employeeRole: EmployeeRole): Observable<EmployeeRole> {
        return this.http.post<EmployeeRole>(this.EmployeeRoleUrl, employeeRole, this.httpOptions).pipe(
            tap(_ => console.log(`saved EmployeeRole ${employeeRole}`)),
            catchError(this.handleError<EmployeeRole>('saveEmployeeRole', employeeRole))
        );
    }

    findAll(): Observable<EmployeeRole[]> {

        const url = `this.EmployeeRoleUrl"}`;
        return this.http.get<EmployeeRole[]>(this.EmployeeRoleUrl, this.httpOptions).pipe(
            tap(_ => console.log('fetched EmployeeRole(s)')),
            catchError(this.handleError<EmployeeRole[]>('findAll', []))
        );
    }


    findById(employeeId: string): Observable<EmployeeRole> {
        const url = `${this.EmployeeRoleUrl}/${employeeId}`;
        return this.http.get<EmployeeRole>(url, this.httpOptions).pipe(
            tap(_ => console.log(`fetched EmployeeRole with employeeId=${employeeId}`)),
            catchError(this.handleError<EmployeeRole>(`EmployeeRole employeeId=${employeeId}`))
        );
    }


    updateEmployeeRole(employeeRole: EmployeeRole): Observable<any> {
        return this.http.put<EmployeeRole>(this.EmployeeRoleUrl, employeeRole, this.httpOptions).pipe(
            tap(_ => console.log(`updated EmployeeRole ${employeeRole}`)),
            catchError(this.handleError<EmployeeRole>(`EmployeeRole =${employeeRole}`))
        );
    }


    deleteById(employeeId: string): Observable<any> {
        const url = `${this.EmployeeRoleUrl}/${employeeId}`;
        return this.http.delete<EmployeeRole>(url).pipe(
            tap(_ => console.log(`fetched EmployeeRole with employeeId=${employeeId}`)),
            catchError(this.handleError<EmployeeRole>(`EmployeeRole employeeId=${employeeId}`))
        );


    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

        return (error: any): Observable<T> => {
            console.error(error); // log to console instead
            console.error(operation + "  failed: " + error.message);
            return of(result as T);
        };

    }


}
