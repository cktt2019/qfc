import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {BusinessUnit} from './BusinessUnit.model';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class BusinessUnitService {

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    private BusinessUnitUrl = 'http://localhost:8081/api/api/businessunit';

    constructor(
        private http: HttpClient
    ) {
    }

    saveBusinessUnit(businessUnit: BusinessUnit): Observable<BusinessUnit> {
        return this.http.post<BusinessUnit>(this.BusinessUnitUrl, businessUnit, this.httpOptions).pipe(
            tap(_ => console.log(`saved BusinessUnit ${businessUnit}`)),
            catchError(this.handleError<BusinessUnit>('saveBusinessUnit', businessUnit))
        );
    }

    findAll(): Observable<BusinessUnit[]> {

        const url = `this.BusinessUnitUrl"}`;
        return this.http.get<BusinessUnit[]>(this.BusinessUnitUrl, this.httpOptions).pipe(
            tap(_ => console.log('fetched BusinessUnit(s)')),
            catchError(this.handleError<BusinessUnit[]>('findAll', []))
        );
    }


    findById(financialBusinessUnitCode: string, businessUnitId: string): Observable<BusinessUnit> {
        Optional < BusinessUnit > businessUnit = businessUnitRepository.findById(BusinessUnitKey.builder().financialBusinessUnitCode(financialBusinessUnitCode).businessUnitId(businessUnitId).build());
    }


    updateBusinessUnit(businessUnit: BusinessUnit): Observable<any> {
        return this.http.put<BusinessUnit>(this.BusinessUnitUrl, businessUnit, this.httpOptions).pipe(
            tap(_ => console.log(`updated BusinessUnit ${businessUnit}`)),
            catchError(this.handleError<BusinessUnit>(`BusinessUnit =${businessUnit}`))
        );
    }


    deleteById(financialBusinessUnitCode: string, businessUnitId: string): Observable<any> {
        businessUnitRepository.deleteById(BusinessUnitKey.builder().financialBusinessUnitCode(financialBusinessUnitCode).businessUnitId(businessUnitId).build());

    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {

        return (error: any): Observable<T> => {
            console.error(error); // log to console instead
            console.error(operation + "  failed: " + error.message);
            return of(result as T);
        };

    }


}
